#!/bin/bash

git pull

docker-compose -f production.yml run --rm frontend npm run build
docker-compose -f production.yml run --rm backend rails db:migrate
docker-compose -f production.yml stop backend && docker-compose -f production.yml up -d backend
