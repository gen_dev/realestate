const WebSocket = require('ws');

let wss;

const initWebSocketServer = (server) => {
  wss = new WebSocket.Server({ server });

  wss.on('connection', (ws, req) => {
    const userId = req.url.split('/').pop();

    console.log(`New WebSocket connection established for user ${userId}`);
    ws.userId = userId;

    ws.on('message', (message) => {
      console.log(`Received from ${userId}:`, message);
    });

    ws.on('close', () => {
      console.log(`WebSocket connection closed for user ${userId}`);
    });
  });
};

const broadcastToUser = (userId, message) => {
  wss.clients.forEach((client) => {
    if (client.readyState === WebSocket.OPEN && client.userId === userId) {
      client.send(JSON.stringify(message));
    }
  });
};

module.exports = {
  initWebSocketServer,
  broadcastToUser,
};
