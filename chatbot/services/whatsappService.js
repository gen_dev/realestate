const { Client, LocalAuth } = require('whatsapp-web.js');
const { broadcastToUser } = require('./websocketService');

const clients = {}; // Store clients by user ID
const qrCodeData = {}; // Store QR code data by user ID

// Initialize a new client for a specific user
const initializeClient = (userId) => {
  const client = new Client({
    puppeteer: {
      headless: true,
      args: ['--no-sandbox', '--disable-setuid-sandbox'],
    },
    authStrategy: new LocalAuth({
      dataPath: `sessions/${userId}`,
    }),
  });

  client.initialize();

  client.on('message', (message) => {
    console.log("New message:", message);
  });

  client.on('loading_screen', (percent, message) => {
    broadcastToUser(userId, { loading: percent });
  });

  client.on('disconnected', (reason) => {
    console.log(`Client ${userId} was logged out:`, reason);
    broadcastToUser(userId, { disconnected: true });
  });

  client.on('qr', (qr) => {
    qrCodeData[userId] = qr;
    broadcastToUser(userId, { qrCode: qr });
  });

  client.on('authenticated', () => {
    console.log(`AUTHENTICATED for user ${userId}`);
    broadcastToUser(userId, { connected: true });
  });

  client.on('ready', async () => {
    console.log(`READY for user ${userId}`);
    const picUrl = await client.getProfilePicUrl(client.info.me._serialized);
    broadcastToUser(userId, { info: { avatar: picUrl, ...client.info } });
  });

  clients[userId] = client;
  return client;
};

const getClient = (userId) => {
  return clients[userId] || initializeClient(userId);
};

const getQrCodeData = (userId) => {
  return qrCodeData[userId];
};

const removeClient = (userId) => {
  delete clients[userId];
};

module.exports = {
  initializeClient,
  getClient,
  getQrCodeData,
  removeClient,
};
