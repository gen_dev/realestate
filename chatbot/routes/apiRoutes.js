const express = require('express');
const { getClient, removeClient } = require('../services/whatsappService');
const { calculateMessageDelay } = require('../utils/messageUtils');

const router = express.Router();

// API endpoint to send a message for a specific user
router.post('/send-message', async (req, res) => {
  const { userId, number, message } = req.body;

  if (!userId || !number || !message) {
    return res.status(400).send('User ID, number, and message are required');
  }

  try {
    const client = getClient(userId);
    const chatId = `${number}@c.us`;
    const chat = await client.getChatById(chatId);

    await client.sendPresenceAvailable();
    await chat.sendSeen();
    await chat.sendStateTyping();

    const delay = calculateMessageDelay(message);
    console.log(`Delaying message for ${delay}ms to simulate typing...`);
    await new Promise((resolve) => setTimeout(resolve, delay));

    await chat.clearState();
    await chat.sendMessage(message);

    await client.sendPresenceUnavailable();
    res.status(200).send('Message sent');
  } catch (error) {
    console.error(`Error sending message for user ${userId}:`, error);
    res.status(500).send('Failed to send message');
  }
});

// API endpoint to get the current user info
router.get('/:userId', async (req, res) => {
  const { userId } = req.params;
  const client = getClient(userId);

  const state = client.pupPage ? await client.getState() : null;

  if (!client || state !== 'CONNECTED') {
    return res.status(404).send('Client not available');
  }

  const picUrl = await client.getProfilePicUrl(client.info.me._serialized);

  const populatedInfo = {
    avatar: picUrl,
    ...client.info
  };

  res.status(200).send({ info: populatedInfo });
});

// API endpoint to get chats
router.get('/:userId/chats', async (req, res) => {
  const { userId } = req.params;
  const client = getClient(userId);

  const chats = await client.getChats();

  res.status(200).send({ data: chats });
});

// API endpoint to get messages for a chat
router.get('/:userId/chats/:chatId/messages', async (req, res) => {
  const { userId, chatId } = req.params;
  const client = getClient(userId);

  const chat = await client.getChatById(chatId);
  const messages = await chat.fetchMessages({ limit: Number.MAX_VALUE });

  res.status(200).send({ data: messages });
});

// API endpoint to logout a specific user
router.post('/logout', (req, res) => {
  const { userId } = req.body;

  if (!userId || !getClient(userId)) {
    return res.status(404).send('Client not available');
  }

  getClient(userId).logout().then(() => {
    removeClient(userId);
    res.status(200).send({ success: true });
  }).catch((error) => {
    console.error(`Error logging out user ${userId}:`, error);
    res.status(500).send('Failed to logout');
  });
});

module.exports = router;
