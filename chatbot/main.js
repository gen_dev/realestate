const express = require('express');
const { Client, LocalAuth } = require('whatsapp-web.js');
const QRCode = require('qrcode');
const WebSocket = require('ws');
const cors = require('cors');

const app = express();
const port = 3001;

let qrCodeData = {}; // Store QR code data by user ID

app.use(express.json());
app.use(cors());

// Store clients by user ID
const clients = {};

// Initialize a new client for a specific user
const initializeClient = (userId) => {
  const client = new Client({
    puppeteer: {
      headless: true,
      args: ['--no-sandbox', '--disable-setuid-sandbox'],
    },
    authStrategy: new LocalAuth({
      dataPath: `sessions/${userId}`,
    }),
    webVersionCache: {
      type: "remote",
      remotePath: "https://raw.githubusercontent.com/wppconnect-team/wa-version/main/html/2.2412.54.html",
    },
  });

  client.initialize();

  client.on('loading_screen', (percent, message) => {
    broadcastToUser(userId, { loading: percent });
  });

  client.on('disconnected', (reason) => {
    console.log(`Client ${userId} was logged out:`, reason);
    broadcastToUser(userId, { disconnected: true });
  });

  client.on('qr', (qr) => {
    qrCodeData[userId] = qr;
    broadcastToUser(userId, { qrCode: qr });
  });

  client.on('authenticated', () => {
    console.log(`AUTHENTICATED for user ${userId}`);
    broadcastToUser(userId, { connected: true });
  });

  client.on('ready', async () => {
    console.log(`READY for user ${userId}`);
    const picUrl = await client.getProfilePicUrl(client.info.me._serialized);
    broadcastToUser(userId, { info: { avatar: picUrl, ...client.info } });
  });

  clients[userId] = client;
  return client;
};

// Broadcast a message to all WebSocket clients for a specific user
const broadcastToUser = (userId, message) => {
  wss.clients.forEach(client => {
    if (client.readyState === WebSocket.OPEN && client.userId === userId) {
      client.send(JSON.stringify(message));
    }
  });
};

// API endpoint to send a message for a specific user
app.post('/send-message', async (req, res) => {
  const { userId, number, message } = req.body;

  if (!userId || !number || !message) {
    return res.status(400).send('User ID, number, and message are required');
  }

  try {
    const client = clients[userId] || initializeClient(userId);
    const chatId = `${number}@c.us`;
    const chat = await client.getChatById(chatId)

    // Set client as ONLINE
    await client.sendPresenceAvailable();

    await chat.sendSeen();
    await chat.sendStateTyping();

    // Calculate the delay based on message length
    const typingSpeed = 13; // Average typing speed in ms per character
    const randomExtraDelay = Math.random() * 1000; // Random extra delay between 0 and 1000ms
    const delay = message.length * typingSpeed + randomExtraDelay;

    console.log(`Delaying message for ${delay}ms to simulate typing...`);

    // Delay the message sending
    await new Promise(resolve => setTimeout(resolve, delay));

    // Remove state typing
    await chat.clearState();

    await chat.sendMessage(message);

    // Set client as OFFLINE
    await client.sendPresenceUnavailable();
    res.status(200).send('Message sent');
  } catch (error) {
    console.error(`Error sending message for user ${userId}:`, error);
    res.status(500).send('Failed to send message');
  }
});

// API endpoint to get the current user info
app.get('/:userId', async (req, res) => {
  const { userId } = req.params;
  const client = clients[userId] || initializeClient(userId);

  const state = client.pupPage ? await client.getState() : null;

  if (!client || state !== 'CONNECTED') {
    return res.status(404).send('Client not available');
  }

  const picUrl = await client.getProfilePicUrl(client.info.me._serialized);

  const populatedInfo = {
    avatar: picUrl,
    ...client.info
  };

  res.status(200).send({ info: populatedInfo });
});

// API endpoint to get chats
app.get('/:userId/chats', async (req, res) => {
  const { userId } = req.params;
  const client = clients[userId] || initializeClient(userId);

  const chats = await client.getChats();

  res.status(200).send({ data: chats });
});

// API endpoint to get messages for a chat
app.get('/:userId/chats/:chatId/messages', async (req, res) => {
  const { userId, chatId } = req.params;
  const client = clients[userId] || initializeClient(userId);

  const chat = await client.getChatById(chatId);

  const messages = await chat.fetchMessages({limit: Number.MAX_VALUE});

  res.status(200).send({ data: messages });
});

// API endpoint to logout a specific user
app.post('/logout', (req, res) => {
  const { userId } = req.body;

  if (!userId || !clients[userId]) {
    return res.status(404).send('Client not available');
  }

  clients[userId].logout().then(() => {
    delete clients[userId];
    res.status(200).send({ success: true });
  }).catch(error => {
    console.error(`Error logging out user ${userId}:`, error);
    res.status(500).send('Failed to logout');
  });
});

// Create WebSocket server
const server = app.listen(port, () => {
  console.log(`Server running on port ${port}`);
});

const wss = new WebSocket.Server({ server });

wss.on('connection', (ws, req) => {
  const userId = req.url.split('/').pop(); // Assume user ID is passed in the WebSocket URL

  console.log(`New WebSocket connection established for user ${userId}`);

  ws.userId = userId;

  // Send the current QR code to the new client
  const qrCode = qrCodeData[userId];
  if (qrCode) {
    ws.send(JSON.stringify({ qrCode }));
  }

  ws.on('message', message => {
    console.log(`Received from ${userId}:`, message);
  });

  ws.on('close', () => {
    console.log(`WebSocket connection closed for user ${userId}`);
  });
});
