namespace :create_payments do
  task :taxes => :environment do
    CashRegister.where(state: 'pending').update_all(state: 'closed')

    Contract.where(state: 'ongoing').map do |contract|
      if contract.ends_at && contract.ends_at < Date.today
        contract.update(state: 'finished')
        contract.property.update(state: 'published') if contract.property
      end
    end

    Tax.all.map { |x| x.create_payment() }

    Payment.where('state = ? AND due_date < ?', 'pending', Date.today).each do |p|
      next unless p.tax
      next if p.tax.chargeback.zero?

      times = ((Date.today - p.due_date).to_i - 1)

      next if times.zero?

      next if p.chargebacks.where(charged_at: Date.today.beginning_of_day..Date.today.end_of_day).any?
      # next if p.concept == 'Alquiler' && p.tax.taxable_type == 'Contract' && p.referral_id == p.tax.taxable.owner.last.id
      chargeback = p.chargebacks.last || p.chargebacks.new(amount: 0)
      chargeback.amount = (p.amount * (p.tax.chargeback / 100)) * times
      chargeback.charged_at = Date.today
      chargeback.save!
      # p.chargebacks.create(amount: p.amount * (p.tax.chargeback / 100))
    end
  end
end
