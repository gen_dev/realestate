require 'test_helper'

class AbilityTest < ActiveSupport::TestCase
  context 'when there isnt current user' do 
    setup do 
      @ability = Ability.new(nil)
    end 

    should_have_public_permissions
  end 

  context 'when there is current user' do 
    context 'when hasnt got any role' do 
      setup do 
        @user = create(:user)
        @ability = Ability.new(@user)
      end

      should_have_public_permissions
    end 

    context 'when has role :gendev' do 
      setup do 
        @user = create(:user_with_role_gendev)
      end

      should 'be able to manage all' do 
        assert @user.can?(:manage, :all)
      end 
    end 

    context 'when has role :admin' do 
      setup do 
        @user = create(:user_with_role_admin)
      end
      
      should 'not be able to manage all' do 
        assert @user.cannot?(:manage, :all)
      end
      
      context 'when user have agencies' do 
        setup do 
          @agency = create(:agency)
          @another_agency = create(:agency)
          create(:user_agency, user: @user, agency: @agency)
        end 
        
        should_be_able_to_manage_only_related do 
          @cannot_manage = [Agency.all, @another_agency] 
          @can_manage = [@agency]
        end

        context 'when agency have addresses' do 
          setup do 
            @address = create(:address, addressable: @agency)
            @address_from_other_agency = create(:address)
          end 
          
          should_be_able_to_manage_only_related do 
            @cannot_manage = [Address.all, @address_from_other_agency] 
            @can_manage = [@address]
          end
        end

        context 'when user have addresses' do 
          setup do 
            @address = create(:address, addressable: @user)
            @user_from_same_agency = create(:user)
            create(:user_agency, user: @user_from_same_agency, agency: @agency)
            @address_from_other_user_same_agency = create(:address, addressable: @user_from_same_agency)
            @address_from_other_user_other_agency = create(:address)
          end 
          
          should_be_able_to_manage_only_related do 
            @cannot_manage = [Address.all, @address_from_other_user_other_agency] 
            @can_manage = [@address, @address_from_other_user_same_agency]
          end
        end

        context 'when agency have accounts' do 
          setup do 
            @account = create(:account, agency: @agency)
            @account_from_other_agency = create(:account)
          end 
          
          should_be_able_to_manage_only_related do 
            @cannot_manage = [Account.all, @account_from_other_agency] 
            @can_manage = [@account]
          end

          context 'when agency have sub accounts' do 
            setup do 
              @sub_account = create(:sub_account, account: @account, agency: @account.agency)
              @sub_account_from_other_agency = create(:sub_account)
            end 
            
            should_be_able_to_manage_only_related do 
              @cannot_manage = [SubAccount.all, @sub_account_from_other_agency] 
              @can_manage = [@sub_account]
            end

            context 'when sub_account have payments' do 
              setup do 
                @payment = create(:payment, sub_account: @sub_account)
                @payment_from_other_agency = create(:payment)
              end 
              
              should_be_able_to_manage_only_related do 
                @cannot_manage = [Payment.all, @payment_from_other_agency] 
                @can_manage = [@payment]
              end

              context 'when payment have partial payments' do 
                setup do 
                  @partial_payment = create(:partial_payment, payment: @payment)
                  @partial_payment_from_other_agency = create(:partial_payment)
                end 
                
                should_be_able_to_manage_only_related do 
                  @cannot_manage = [PartialPayment.all, @partial_payment_from_other_agency] 
                  @can_manage = [@partial_payment]
                end
              end
            end
          end
        end

        context 'when agency have properties' do 
          setup do 
            @property = create(:property, agency: @agency)
            @property_from_other_agency = create(:property)
          end 
          
          should_be_able_to_manage_only_related do 
            @cannot_manage = [Property.all, @property_from_other_agency] 
            @can_manage = [@property]
          end

          context 'when property have characteristics' do 
            setup do 
              @property_characteristic = create(:simple_data, datable: @property)
              @property_characteristic_from_other_agency = create(:simple_data)
            end 

            should_be_able_to_manage_only_related do 
              @cannot_manage = [SimpleData.all, @property_characteristic_from_other_agency] 
              @can_manage = [@property_characteristic]
            end
          end

          context 'when property have contracts' do 
            setup do 
              @contract = create(:contract, property: @property)
              @contract_from_other_agency = create(:contract)
            end

            should_be_able_to_manage_only_related do 
              @cannot_manage = [Contract.all, @contract_from_other_agency] 
              @can_manage = [@contract]
            end

            context 'when contract have contract plans' do 
              setup do 
                @contract_plan = create(:contract_plan, contract: @contract, property: @property)
                @contract_plan_from_other_agency = create(:contract_plan)
              end

              should_be_able_to_manage_only_related do 
                @cannot_manage = [ContractPlan.all, @contract_plan_from_other_agency] 
                @can_manage = [@contract_plan]
              end

              context 'when contract plan have contract expenses' do 
                setup do 
                  @contract_expense = create(:contract_expense, contract_plan: @contract_plan)
                  @contract_expense_from_other_agency = create(:contract_expense)
                end
  
                should_be_able_to_manage_only_related do 
                  @cannot_manage = [ContractExpense.all, @contract_expense_from_other_agency] 
                  @can_manage = [@contract_expense]
                end
              end
            end 

            context 'when contract have documents' do 
              setup do 
                @document = create(:document, documentable: @contract)
                @document_from_other_agency = create(:document)
              end

              should_be_able_to_manage_only_related do 
                @cannot_manage = [Document.all, @document_from_other_agency] 
                @can_manage = [@document]
              end
            end

            context 'when contract have payment plans' do 
              setup do 
                @payment_plan = create(:payment_plan, contract: @contract)
                @payment_plan_from_other_agency = create(:payment_plan)
              end

              should_be_able_to_manage_only_related do 
                @cannot_manage = [PaymentPlan.all, @payment_plan_from_other_agency] 
                @can_manage = [@payment_plan]
              end
            end

            context 'when contract have taxes' do 
              setup do 
                @tax = create(:tax, taxable: @contract)
                @tax_from_other_agency = create(:tax)
              end

              should_be_able_to_manage_only_related do 
                @cannot_manage = [Tax.all, @tax_from_other_agency] 
                @can_manage = [@tax]
              end
            end
          end 

          context 'when property have images' do 
            setup do 
              @image = create(:image, imageable: @property)
              @image_from_other_agency = create(:image)
            end

            should_be_able_to_manage_only_related do 
              @cannot_manage = [Image.all, @image_from_other_agency] 
              @can_manage = [@image]
            end
          end
        end

        context 'when agency have other users' do 
          setup do 
            @another_user = create(:user)
            @user_agency = create(:user_agency, user: @another_user, agency: @agency)
            @user_agency_from_other_agency = create(:user_agency, user: @another_user)
          end 
          
          should_be_able_to_manage_only_related do 
            @cannot_manage = [UserAgency.all, @user_agency_from_other_agency] 
            @can_manage = [@user_agency]
          end
        end
      end
    end 

    context 'when has role :owner' do 
      setup do 
        @user = create(:user_with_role_owner)
        @ability = Ability.new(@user)
      end
      
      should_have_owner_permissions
    end 
    
    context 'when has role :tenant' do 
      setup do 
        @user = create(:user_with_role_tenant)
        @ability = Ability.new(@user)
      end
      
      should_have_tenant_permissions
    end 
  end 
end