require 'test_helper'

class ContractPlanTest < ActiveSupport::TestCase
  context '#create' do 
    context 'when required fields are missing' do 
      should 'raise invalid record validation' do 
        assert_raise ActiveRecord::RecordInvalid do
          create(:contract_plan, duration: nil)
        end
      end
    end

    context 'when required fields are present' do 
      setup do 
        @contract_plan = create(:contract_plan)
      end 

      should 'create contract_plan' do 
        assert @contract_plan.persisted?
      end
    end 
  end 
  
  context '#total_amount' do 
    setup do 
      @contract_plan = create(:contract_plan, rent_value: { "0": 1000, "1": 2000 }, rent_update_period: 6)
    end 
    
    should 'return sum of rent value from each period * rent update period' do 
      assert_equal @contract_plan.total_amount, ((1000 * 6) + (2000 * 6))
    end
  end
  
  context '#current_period' do 
    setup do 
      contract_plan_params = { start_date: Time.now.months_ago(4), due_date: Time.now.months_ago(-8), rent_value: { "0": 1000, "1": 2000 }, rent_update_period: 6 }
      @contract_plan1 = create(:contract_plan, contract_plan_params)
      
      contract_plan2_params = contract_plan_params.merge(start_date: Time.now.months_ago(8), due_date: Time.now.months_ago(-4))
      @contract_plan2 = create(:contract_plan, contract_plan2_params)
    end 

    should 'return current period number' do 
      assert_equal @contract_plan1.current_period, "0"
      assert_equal @contract_plan2.current_period, "1"
    end

    context '#current_period_value' do
      should 'return current period value based on current period number' do 
        assert_equal @contract_plan1.current_period_value, @contract_plan1.rent_value["0"] 
        assert_equal @contract_plan1.current_period_value, 1000 
        assert_equal @contract_plan2.current_period_value, @contract_plan2.rent_value["1"] 
        assert_equal @contract_plan2.current_period_value, 2000
      end
    end 
  end
end 