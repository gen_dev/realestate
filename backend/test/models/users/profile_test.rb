require 'test_helper'

class Users::ProfileTest < ActiveSupport::TestCase
  context '#create' do 
    context 'when required fields are missing' do 
      should 'raise invalid record validation' do 
        assert_raise ActiveRecord::RecordInvalid do
          create(:user_profile, name: nil)
        end
      end
    end

    context 'when required fields are present' do 
      setup do 
        @user_profile = create(:user_profile)
      end 

      should 'create user profile ' do 
        assert @user_profile.persisted?
      end
    end 
  end 
end
