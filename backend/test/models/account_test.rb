require 'test_helper'

class AccountTest < ActiveSupport::TestCase
  context '#create' do 
    setup do 
      @account = create(:account)
    end 

    should 'create account' do 
      assert @account.persisted?
    end
  end 
end
