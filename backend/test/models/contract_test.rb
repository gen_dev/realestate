require 'test_helper'

class ContractTest < ActiveSupport::TestCase
  context '#create' do 
    context 'when required fields are missing' do 
      should 'raise invalid record validation' do 
        assert_raise ActiveRecord::RecordInvalid do
          create(:contract, state: nil)
        end
      end
    end

    context 'when required fields are present' do 
      setup do 
        @contract = create(:contract)
      end 

      should 'create contract' do 
        assert @contract.persisted?
      end
    end 
  end 

  context '#owner' do 
    setup do 
      @contract = create(:contract)
    end 

    context 'when contract doesnt have owner' do 
      should 'return empty array' do 
        assert_empty @contract.owner
      end 
    end 

    context 'when contract have owner' do 
      setup do 
        @owner = create(:user)
        @owner.add_role :owner, @contract
      end 
      
      should 'return owner' do 
        assert @contract.owner
        assert @owner.has_role?(:owner, @contract)
        assert_equal @contract.owner[0], @owner
      end 
    end 
  end

  context '#tenant' do 
    setup do 
      @contract = create(:contract)
    end 

    context 'when contract doesnt have tenant' do 
      should 'return empty array' do 
        assert_empty @contract.tenant
      end 
    end 

    context 'when contract have tenant' do 
      setup do 
        @tenant = create(:user)
        @tenant.add_role :tenant, @contract
      end 
      
      should 'return tenant' do 
        assert @contract.tenant
        assert @tenant.has_role?(:tenant, @contract)
        assert_equal @contract.tenant[0], @tenant
      end 
    end 
  end

  context '#overdue_payments' do 
    setup do 
      @contract = create(:contract)
    end

    context 'when there arent payments' do 
      should 'return empty array' do 
        assert_empty @contract.overdue_payments
      end 
    end

    context 'when there are payments' do 
      context 'when due date > today' do 
        setup do 
          @payment = create(:payment, due_date: Time.now.days_ago(-2), contract: @contract)
        end 

        should 'return empty array' do 
          assert_empty @contract.overdue_payments
        end 
      end 

      context 'when due date < today' do 
        context 'when state != pending' do 
          setup do 
            @payment = create(:payment, state: 'paid', due_date: Time.now.days_ago(2), contract: @contract)
          end 

          should 'return empty array' do 
            assert_empty @contract.overdue_payments
          end 
        end 

        context 'when state == pending' do 
          setup do 
            @payment = create(:payment, state: 'pending', due_date: Time.now.days_ago(2), contract: @contract)
          end 
  
          should 'return as overdue_payments' do 
            assert @contract.overdue_payments
            assert_equal @contract.overdue_payments[0], @payment
          end 
        end 
      end 
    end
  end

  context '#documents' do 
    context 'when documents are nil' do
      setup do
        @contract = create(:contract)
      end 

      should 'return empty array' do 
        assert_empty @contract.documents
      end  
    end 

    context 'when one document is present' do
      setup do
        @contract = create(:contract_with_one_document)
      end 

      should 'return document' do 
        assert @contract.documents
        assert_equal @contract.documents.count, 1
        assert_equal @contract.documents[0].class, Document
        assert_equal @contract.documents[0].name.class, String
        assert_equal @contract.documents[0].size.class, Integer
        assert_equal @contract.documents[0].document.class, DocumentUploader
        assert_equal @contract.documents[0].document.url.class, String
        assert File.exists?(Rails.root.join(fixture_path + @contract.documents[0].document.url))
      end  
    end 

    context 'when multiple documents are present' do
      setup do
        @contract = create(:contract_with_documents)
      end 

      should 'return documents' do 
        assert @contract.documents
        assert_operator @contract.documents.count, :>, 1
        assert_equal @contract.documents[0].class, Document
        assert_equal @contract.documents[0].name.class, String
        assert_equal @contract.documents[0].size.class, Integer
        assert_equal @contract.documents[0].document.class, DocumentUploader
        assert_equal @contract.documents[0].document.url.class, String
        assert File.exists?(Rails.root.join(fixture_path + @contract.documents[0].document.url))

        assert_equal @contract.documents[1].class, Document
        assert_equal @contract.documents[1].name.class, String
        assert_equal @contract.documents[1].size.class, Integer
        assert_equal @contract.documents[1].document.class, DocumentUploader
        assert_equal @contract.documents[1].document.url.class, String
        assert File.exists?(Rails.root.join(fixture_path + @contract.documents[1].document.url))
      end  
    end  
  end
end 