require 'test_helper'

class SubAccountTest < ActiveSupport::TestCase
  context '#create' do 
    context 'when required fields are missing' do 
      should 'not raise invalid record validation' do 
        assert_raise ActiveRecord::RecordInvalid do
          create(:sub_account, balance: nil)
        end
      end
    end

    context 'when required fields are present' do 
      setup do 
        @sub_account = create(:sub_account)
      end 

      should 'create sub account' do 
        assert @sub_account.persisted?
      end
    end 
  end 

  context '#update_balance' do 
    setup do 
      @sub_account = create(:sub_account, balance: 3000)
    end 

    context 'when arguments are missing' do 
      should 'raise ArgumentError' do 
        assert_raise ArgumentError do
          @sub_account.update_balance(1)
        end
      end
    end 

    context 'when arguments are present' do
      context 'when action is addition' do
        setup do 
          @sub_account.update_balance(200, :+)
        end 

        should 'update balance' do 
          assert_equal @sub_account.balance, 3000 + 200  
        end 
      end
      context 'when action is substraction' do 
        setup do 
          @sub_account.update_balance(200, :-)
        end 
        
        should 'update balance' do 
          assert_equal @sub_account.balance, 3000 - 200  
        end 
      end
    end
  end
end 