require 'test_helper'

class AddressTest < ActiveSupport::TestCase
  context '#create' do 
    context 'when required fields are missing' do 
      should 'raise invalid record validation' do 
        assert_raise ActiveRecord::RecordInvalid do
          create(:address, street: nil)
        end
      end
    end

    context 'when required fields are present' do 
      context 'when addressable is a Agency' do 
        setup do 
          @address = create(:address, addressable: create(:agency))
        end 

        should 'create address' do 
          assert @address.persisted?
          assert_equal @address.addressable.class, Agency 
        end
      end

      context 'when addressable is a Property' do 
        setup do 
          @address = create(:address, addressable: create(:property))
        end 

        should 'create address' do 
          assert @address.persisted?
          assert_equal @address.addressable.class, Property
        end

        context 'when Property already have an address' do 
          should 'raise callback validation' do 
            error = assert_raise ActiveRecord::RecordInvalid do
              create(:address, addressable: @address.addressable)
            end
            assert_match /Address already exists for that Property./, error.message
          end
        end
      end

      context 'when addressable is a User' do 
        setup do 
          @address = create(:address, addressable: create(:user))
        end 

        should 'create address' do 
          assert @address.persisted?
          assert_equal @address.addressable.class, User 
        end
      end
    end 
  end 
end 