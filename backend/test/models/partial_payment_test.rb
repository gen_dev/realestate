require 'test_helper'

class PartialPaymentTest < ActiveSupport::TestCase
  context '#create' do 
    setup do 
      @partial_payment = create(:partial_payment)
    end 

    should 'create partial payment' do 
      assert @partial_payment.persisted?
    end
  end 
end 