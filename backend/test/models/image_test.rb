require 'test_helper'

class ImageTest < ActiveSupport::TestCase
  context '#create' do 
    setup do 
      @image = create(:image)
    end 

    should 'create image' do 
      assert @image.persisted?
    end
  end 

  context '#url' do 
    setup do 
      @image = create(:image)
    end 

    should 'return path of attached image' do 
      assert @image.url
      assert_equal @image.url, @image.image.url
    end
  end
end 