require 'test_helper'

class PropertyTest < ActiveSupport::TestCase
  context '#create' do 
    context 'when required fields are missing' do 
      should 'raise invalid record validation' do 
        assert_raise ActiveRecord::RecordInvalid do
          create(:property, state: nil)
        end
      end
    end

    context 'when required fields are present' do 
      setup do 
        @property = create(:property)
      end 

      should 'create property' do 
        assert @property.persisted?
      end
    end 
  end 

  context '#active_tenant' do 
    context 'when property doesnt have tenant' do 
      setup do 
        @property = create(:property)
      end 
      
      should 'return empty array' do 
        assert_empty @property.active_tenant
      end 
    end 
    
    context 'when property have active tenant' do 
      setup do 
        @property = create(:property_with_tenant)
      end 
      
      should 'return tenant' do 
        assert @property.active_tenant
        assert @property.active_tenant.first.has_role?(:tenant, @property.contracts[0])
      end 
    end 
  end

  context '#owner' do 
    context 'when property doesnt have owner' do 
      setup do 
        @property = create(:property)
      end 
      
      should 'return empty array' do 
        assert_empty @property.owner
      end 
    end 
    
    context 'when property have owner' do 
      setup do 
        @property = create(:property_with_owner)
      end 
      
      should 'return owner' do 
        assert @property.owner
        assert @property.owner.first.has_role?(:owner, @property)
      end 
    end 
  end

  context '#active_contract' do 
    setup do 
      @property = create(:property)
    end 

    context 'when property hasnt got contract' do 
      should 'return nil' do 
        assert_nil @property.active_contract
      end
    end

    context 'when property has contracts' do 
      context 'when contract state != ongoing' do
        setup do 
          @contract = create(:contract, state: 'pending', property: @property)
        end 

        should 'return empty array' do 
          assert_nil @property.active_contract
        end
      end     
      
      context 'when contract state == ongoing' do
        setup do 
          @contract = create(:contract, state: 'ongoing', property: @property)
        end 

        should 'return active contract' do 
          assert_equal @property.active_contract, @contract
        end
      end 
    end 
  end 

  context '#images' do 
    context 'when images are nil' do
      setup do
        @property = create(:property)
      end 

      should 'return empty array' do 
        assert_empty @property.images
      end  
    end 

    context 'when one image is present' do
      setup do
        @property = create(:property_with_one_image)
      end 

      should 'return image' do 
        assert @property.images
        assert_equal @property.images.count, 1
        assert_equal @property.images[0].class, Image
        assert_equal @property.images[0].name.class, String
        assert_equal @property.images[0].size.class, Integer
        assert_equal @property.images[0].image.class, ImageUploader
        assert_equal @property.images[0].image.url.class, String
        assert File.exists?(Rails.root.join(fixture_path + @property.images[0].image.url))
      end  
    end 

    context 'when multiple images are present' do
      setup do
        @property = create(:property_with_images)
      end 

      should 'return images' do 
        assert @property.images
        assert_operator @property.images.count, :>, 1
        assert_equal @property.images[0].class, Image
        assert_equal @property.images[0].name.class, String
        assert_equal @property.images[0].size.class, Integer
        assert_equal @property.images[0].image.class, ImageUploader
        assert_equal @property.images[0].image.url.class, String
        assert File.exists?(Rails.root.join(fixture_path + @property.images[0].image.url))

        assert_equal @property.images[1].class, Image
        assert_equal @property.images[1].name.class, String
        assert_equal @property.images[1].size.class, Integer
        assert_equal @property.images[1].image.class, ImageUploader
        assert_equal @property.images[1].image.url.class, String
        assert File.exists?(Rails.root.join(fixture_path + @property.images[1].image.url))
      end  
    end  
  end
end 