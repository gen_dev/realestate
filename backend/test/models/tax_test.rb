require 'test_helper'

class TaxTest < ActiveSupport::TestCase
  context '#create' do 
    setup do 
      @tax = create(:tax)
    end 

    should 'create tax' do 
      assert @tax.persisted?
    end
  end 

  context '#set_payment_date' do 
    setup do 
      @tax = create(:tax, last_payment_date: nil)
    end

    context 'when payment state transitions to paid' do 
      setup do 
        # TODO: add transition logic
        @tax.set_payment_date
      end 

      should 'set payment date' do 
        assert @tax.last_payment_date
      end
    end
  end 

  context '#create_payment' do 
    should 'create a new payment once the interval edge is reached' do 
      # TODO: add test when method exists
    end
  end 
end 