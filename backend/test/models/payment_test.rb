require 'test_helper'

class PaymentTest < ActiveSupport::TestCase
  context '#create' do 
    context 'when required fields are missing' do 
      should 'raise invalid record validation' do 
        assert_raise ActiveRecord::RecordInvalid do
          create(:payment, concept: nil)
        end
      end
    end

    context 'when required fields are present' do 
      setup do 
        @payment = create(:payment)
      end 

      should 'create payment' do 
        assert @payment.persisted?
      end
    end 
  end 

  context '#lasting_amount' do 
    should 'return diff between amount and sum of partial payments amount' do 
      # TODO: add test when method exists
    end
  end 
end 