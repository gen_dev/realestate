require 'test_helper'

class Price::PeriodTest < ActiveSupport::TestCase
  context '#create' do 
    context 'when required fields are missing' do 
      should 'raise invalid record validation' do 
        assert_raise ActiveRecord::RecordInvalid do
          create(:price_period, priceable: nil)
        end
      end
    end

    context 'when required fields are present' do 
      setup do 
        @price_period = create(:price_period)
      end 

      should 'create price period' do 
        assert @price_period.persisted?
      end
    end 
  end 
end
