require 'test_helper'

class SimpleDataTest < ActiveSupport::TestCase
  context '#create' do 
    setup do 
      @simple_data = create(:simple_data)
    end 

    should 'create simple data' do 
      assert @simple_data.persisted?
    end

    context 'when datable is a Address' do 
      setup do 
        @simple_data = create(:simple_data, datable: create(:address))
      end 

      should 'create simple data' do 
        assert @simple_data.persisted?
        assert_equal @simple_data.datable.class, Address 
      end
    end

    context 'when datable is a Agency' do 
      setup do 
        @simple_data = create(:simple_data, datable: create(:agency))
      end 

      should 'create simple data' do 
        assert @simple_data.persisted?
        assert_equal @simple_data.datable.class, Agency 
      end
    end

    context 'when datable is a Cash' do 
      setup do 
        @simple_data = create(:simple_data, datable: create(:sub_account))
      end 

      should 'create simple data' do 
        assert @simple_data.persisted?
        assert_equal @simple_data.datable.class, SubAccount
      end
    end

    context 'when datable is a Contract' do 
      setup do 
        @simple_data = create(:simple_data, datable: create(:contract))
      end 

      should 'create simple data' do 
        assert @simple_data.persisted?
        assert_equal @simple_data.datable.class, Contract
      end
    end

    context 'when datable is a ContractExpense' do 
      setup do 
        @simple_data = create(:simple_data, datable: create(:contract_expense))
      end 

      should 'create simple data' do 
        assert @simple_data.persisted?
        assert_equal @simple_data.datable.class, ContractExpense
      end
    end

    context 'when datable is a ContractPlan' do 
      setup do 
        @simple_data = create(:simple_data, datable: create(:contract_plan))
      end 

      should 'create simple data' do 
        assert @simple_data.persisted?
        assert_equal @simple_data.datable.class, ContractPlan
      end
    end
    
    context 'when datable is a Payment' do 
      setup do 
        @simple_data = create(:simple_data, datable: create(:payment))
      end 

      should 'create simple data' do 
        assert @simple_data.persisted?
        assert_equal @simple_data.datable.class, Payment
      end
    end

    context 'when datable is a PartialPayment' do 
      setup do 
        @simple_data = create(:simple_data, datable: create(:partial_payment))
      end 

      should 'create simple data' do 
        assert @simple_data.persisted?
        assert_equal @simple_data.datable.class, PartialPayment
      end
    end

    context 'when datable is a Property' do 
      setup do 
        @property = create(:property)
        @simple_data = create(:simple_data, datable: @property)
      end 

      should 'create simple data' do 
        assert @simple_data.persisted?
        assert_equal @simple_data.datable.class, Property
      end

      context 'property' do 
        should 'return simple data as characteristics' do 
          assert @property.characteristics
          assert_equal @property.characteristics.last, @simple_data
        end 
      end
    end

    context 'when datable is a Tax' do 
      setup do 
        @simple_data = create(:simple_data, datable: create(:tax))
      end 

      should 'create simple data' do 
        assert @simple_data.persisted?
        assert_equal @simple_data.datable.class, Tax
      end
    end

    context 'when datable is a User' do 
      setup do 
        @simple_data = create(:simple_data, datable: create(:user))
      end 

      should 'create simple data' do 
        assert @simple_data.persisted?
        assert_equal @simple_data.datable.class, User
      end
    end
  end 
end 