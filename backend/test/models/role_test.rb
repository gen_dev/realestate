require 'test_helper'

class RoleTest < ActiveSupport::TestCase
  context 'when added to object' do 
    setup do 
      @user = create(:user)
      @user.add_role :developer
    end 

    should 'create role' do 
      assert @user.roles
      assert @user.has_role? :developer
    end
  end 

  context 'when added to resource' do 
    setup do 
      @user = create(:user)
      @contract = create(:contract)
      @user.add_role :developer, @contract
    end 

    should 'create role for specific resource' do 
      assert @user.roles
      assert_equal @user.has_role?(:developer), false
      assert_equal @user.has_role?(:developer, @contract), true
    end
  end 
end 