require 'test_helper'

class UserTest < ActiveSupport::TestCase
  context '#create' do 
    context 'when required fields are missing' do 
      should 'raise invalid record validation' do 
        assert_raise ActiveRecord::RecordInvalid do
          create(:user, email: nil)
        end
      end
    end

    context 'when required fields are present' do 
      setup do 
        @user = create(:user)
      end 

      should 'create user' do 
        assert @user.persisted?
      end

      context 'when user email already exist' do 
        should 'raise invalid record validation' do 
          assert_raise ActiveRecord::RecordInvalid do
            create(:user, email: @user.email)
          end
        end
      end
    end 
  end 

  context '#active_contracts' do 
    setup do 
      @user = create(:user)
    end 

    context 'when user hasnt got contracts' do 
      should 'return empty array' do 
        assert_empty @user.active_contracts
      end
    end

    context 'when user has contracts' do 
      context 'when contract state != ongoing' do
        setup do 
          @contract = create(:contract, state: 'pending')
        end 

        context 'when user role is owner' do
          setup do 
            @user.add_role :owner, @contract
          end 
          
          should 'return empty array' do 
            assert_empty @user.active_contracts
          end
        end
         
        context 'when user role is tenant' do
          setup do 
            @user.add_role :tenant, @contract
          end 
          
          should 'return empty array' do 
            assert_empty @user.active_contracts
          end
        end
      end     
      
      context 'when contract state == ongoing' do
        setup do 
          @contract = create(:contract, state: 'ongoing')
        end 

        context 'when user role is owner' do
          setup do 
            @user.add_role :owner, @contract
          end 
          
          should 'return active contracts' do 
            assert @user.active_contracts
            assert_equal @user.active_contracts[0], @contract
          end
        end
        
        context 'when user role is tenant' do
          setup do 
            @user.add_role :tenant, @contract
          end 

          should 'return active contracts' do 
            assert @user.active_contracts
            assert_equal @user.active_contracts[0], @contract
          end
        end 
      end 
    end 
  end 

  context '#selected_agency' do 
    setup do 
      @user = create(:user)
      @agency = create(:agency)
    end
    
    context 'when isnt selected' do 
      setup do 
        @user_agency = create(:user_agency, selected: false, user: @user, agency: @agency)
      end 
      
      should 'return nil' do 
        assert_nil @user.selected_agency
      end
    end
    
    context 'when is selected' do 
      setup do 
        @user_agency = create(:user_agency, selected: true, user: @user, agency: @agency)
      end 
      
      should 'return agency' do 
        assert @user.selected_agency
        assert_equal @user.selected_agency, @agency
      end
    end
  end

  context '#properties' do 
    setup do 
      @user = create(:user)
    end 

    context 'when user hasnt got properties' do 
      should 'return empty array' do 
        assert_empty @user.properties
      end
    end

    context 'when user has properties' do 
      setup do 
        @property = create(:property)
      end 

      context 'when user role is owner' do
        setup do 
          @user.add_role :owner, @property
        end 
        
        should 'return active properties' do 
          assert @user.properties
          assert_equal @user.properties[0], @property
        end
      end
      
      context 'when user role is tenant' do
        setup do 
          @user.add_role :tenant, @property
        end 

        should 'return active properties' do 
          assert @user.properties
          assert_equal @user.properties[0], @property
        end
      end 
    end 
  end 

  context '#permissions' do 
    setup do
      @user = create(:user)
    end 

    should 'return hash with permissions' do 
      assert @user.permissions
      assert_equal @user.permissions.class, Hash
      assert_equal @user.permissions[:can].class, Hash
      assert_equal @user.permissions[:cannot].class, Hash
    end
  end

  context '#overdue_payments' do 
    should 'return overdue payments (being rent, taxes, duty, etc)' do 
      # TODO: add test when method exists
    end
  end
  
  context '#next_rent_expiration' do 
    should 'return the closest payment by due date' do 
      # TODO: add test when method exists
    end
  end

  context '#up_to_date' do 
    should 'return the closest payment by due date' do 
      # TODO: add test when method exists
    end
  end
end
