require 'test_helper'

class AgencyTest < ActiveSupport::TestCase
  context '#create' do 
    context 'when required fields are missing' do 
      should 'raise invalid record validation' do 
        assert_raise ActiveRecord::RecordInvalid do
          create(:agency, name: nil)
        end
      end
    end

    context 'when required fields are present' do 
      setup do 
        @agency = create(:agency)
      end 

      should 'create agency' do 
        assert @agency.persisted?
      end

      context 'when agency name already exist' do 
        should 'raise invalid record validation' do 
          assert_raise ActiveRecord::RecordInvalid do
            create(:agency, name: @agency.name)
          end
        end
      end
    end 
  end 
end 