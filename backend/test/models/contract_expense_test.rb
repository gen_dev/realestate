require 'test_helper'

class ContractExpenseTest < ActiveSupport::TestCase
  context '#create' do 
    context 'when required fields are missing' do 
      should 'raise invalid record validation' do 
        assert_raise ActiveRecord::RecordInvalid do
          create(:contract_expense, name: nil)
        end
      end
    end

    context 'when required fields are present' do 
      setup do 
        @contract_expense = create(:contract_expense)
      end 

      should 'create contract_expense' do 
        assert @contract_expense.persisted?
      end
    end 
  end 
end 