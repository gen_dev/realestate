require 'test_helper'

class DocumentTest < ActiveSupport::TestCase
  context '#create' do 
    setup do 
      @document = create(:document)
    end 

    should 'create document' do 
      assert @document.persisted?
    end
  end 

  context '#url' do 
    setup do 
      @document = create(:document)
    end 

    should 'return path of attached document' do 
      assert @document.url
      assert_equal @document.url, @document.document.url
    end
  end
end 