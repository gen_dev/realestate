require 'test_helper'

class PaymentPlanTest < ActiveSupport::TestCase
  context '#create' do 
    setup do 
      @payment_plan = create(:payment_plan)
    end 

    should 'create payment plan' do 
      assert @payment_plan.persisted?
    end
  end 
end 