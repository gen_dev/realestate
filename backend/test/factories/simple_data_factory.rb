FactoryBot.define do
  factory :simple_data, class: 'SimpleData' do
    name { 'Mt2' }
    value { '30' }
    category { 'Studio Apartment' }
    datable { create(:property) }
  end
end