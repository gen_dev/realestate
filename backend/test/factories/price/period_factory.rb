
FactoryBot.define do
  factory :price_period, class: 'Price::Period' do
    amount { 3000 }
    starts_at { nil }
    ends_at { nil }
    priceable { create(:payment) }
  end
end