FactoryBot.define do
  factory :image, class: 'Image' do
    sequence(:name, 'A') { |n| "Some File #{n}" }
    size { 20040 }
    imageable { create(:property) }
    after :create do |image|
      image.image = File.open(Rails.root.join('test/fixtures/image.jpeg'))
      image.save
    end
  end

  factory :image_without_attachment, class: 'Image' do
    sequence(:name, 'A') { |n| "Some File #{n}" }
    size { 20040 }
    imageable { create(:property) }
  end
end