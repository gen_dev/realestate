FactoryBot.define do
  factory :user, class: 'User' do
    sequence(:email, 'A') { |n| "email_#{n}@example.com"}
    password { "[omitted]" }
    
    after :create do |user|
      create(:user_profile, user: user)
    end

    factory :user_with_role_gendev do 
      after :create do |user|
        user.add_role(:gendev)
      end
    end 

    factory :user_with_role_admin do 
      after :create do |user|
        user.add_role(:admin)
      end
    end 

    factory :user_with_role_owner do 
      after :create do |user|
        user.add_role(:owner)
      end
    end 

    factory :user_with_role_tenant do 
      after :create do |user|
        user.add_role(:tenant)
      end
    end 
  end

  factory :user_profile, class: 'Users::Profile' do
    sequence(:name, 'A') { |n| "Test User #{n}" }
    sequence(:email, 'A') { |n| "email_#{n}@example.com"}
    address { 'Some address 666'}
    phone { '543418234234'}
    user { create(:user) }
  end
end