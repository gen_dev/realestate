FactoryBot.define do
  factory :partial_payment, class: 'PartialPayment' do
    value { 300 }
    payment { create(:payment) }
  end
end