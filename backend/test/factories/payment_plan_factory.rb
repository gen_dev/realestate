FactoryBot.define do
  factory :payment_plan, class: 'PaymentPlan' do
    payment_methods { ['Check','CreditCard','DebitCard']}
    contract { create(:contract) }
  end
end