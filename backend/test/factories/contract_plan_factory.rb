FactoryBot.define do
  factory :contract_plan, class: 'ContractPlan' do
    duration { 12 }  
    rent_update_period { 6 } 
    rent_value { { first_period: 1000, second_period: 2000 } }
    property { create(:property) }
    contract { create(:contract) }
  end
end