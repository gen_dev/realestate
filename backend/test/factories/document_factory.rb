FactoryBot.define do
  factory :document, class: 'Document' do
    sequence(:name, 'A') { |n| "Some File #{n}" }
    size { 20040 }
    documentable { create(:contract) }
    after :create do |document|
      document.document = File.open(Rails.root.join('test/fixtures/document.pdf'))
      document.save
    end
  end

  factory :document_without_attachment, class: 'Document' do
    sequence(:name, 'A') { |n| "Some File #{n}" }
    size { 20040 }
    documentable { create(:contract) }
  end
end