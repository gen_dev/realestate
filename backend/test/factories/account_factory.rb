FactoryBot.define do
  factory :account, class: 'Account' do
    sequence(:name, 'A') { |n| "Account #{n}" }
    agency { create(:agency) }
  end
end