FactoryBot.define do
    factory :guarantee, class: 'Contracts::Guarantee' do
      guarantor { create(:user) }
      contract { create(:contract) }
    end
  end