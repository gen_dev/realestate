FactoryBot.define do
  factory :payment, class: 'Payment' do
    payment_type { 'Check' }
    period { '1' }
    concept { 'Rent' }
    state { 'pending' }
    due_date { Time.now.days_ago(-2) }
    amount { 2000 }
    contract { create(:contract) }
    sub_account { create(:sub_account) }
    referral { create(:user) }

    after :create do |payment|
      create(:price_period, priceable: payment) if payment.price.nil?
    end
  end
end