FactoryBot.define do
  factory :contract_expense, class: 'ContractExpense' do
    name { 'Sealed' } 
    value { 3000 }
    contract_plan { create(:contract_plan) }
  end
end