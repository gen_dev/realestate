FactoryBot.define do
  factory :property, class: 'Property' do
    state { 'Rented' }
    property_type { 'Apartment' }
    description { 'Apartment description.' }
    price { 21020.50 }
    agency { create(:agency) }

    factory :property_with_address do 
      after :create do |property|
        create(:address, addressable: property)
      end
    end

    factory :property_with_owner do 
      after :create do |property|
        create(:user).add_role(:owner, property)
      end
    end

    factory :property_with_tenant do 
      after :create do |property|
        create(:user).add_role(:tenant, create(:contract, property: property))
      end
    end

    factory :property_with_owner_and_tenant do 
      after :create do |property|
        create(:user).add_role(:owner, property)
        create(:user).add_role(:tenant, create(:contract, property: property))
      end
    end

    factory :property_with_one_image do 
      after :create do |property|
        create(:image, imageable: property)
      end
    end 

    factory :property_with_images do 
      after :create do |property|
        create(:image, imageable: property)
        create(:image, imageable: property)
      end
    end 
  end
end