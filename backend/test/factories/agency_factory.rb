FactoryBot.define do
  factory :agency, class: 'Agency' do
    sequence(:name, 'A') { |n| "Test Agency #{n}" }
  end
end