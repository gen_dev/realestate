FactoryBot.define do
  factory :contract, class: 'Contract' do
    state { 'ongoing' }
    contract_type { 'renting' }
    property { create(:property) }

    factory :contract_with_one_document do 
      after :create do |contract|
        create(:document, documentable: contract)
      end
    end 

    factory :contract_with_documents do 
      after :create do |contract|
        create(:document, documentable: contract)
        create(:document, documentable: contract)
      end
    end 
  end
end
