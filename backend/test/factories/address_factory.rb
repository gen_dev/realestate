FactoryBot.define do
  factory :address, class: 'Address' do
    street { 'Ocassion Avenue' }
    number { '666' }
    city { 'New Jersey' }
    state { 'New Jersey' }
    country { 'USA' }
    unit { '3' }
    neighborhood { 'Hells Kitchen' }
    addressable { create(:agency) }
  end
end