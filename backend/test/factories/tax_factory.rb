FactoryBot.define do
  factory :tax, class: 'Tax' do
    taxable { create(:contract) }
    debtor { create(:user) }
    sequence(:name) { |n| "Tax #{n}" }
    state { 'Pending' }
    interval { 2 }
    last_payment_date { Time.now }
  end
end