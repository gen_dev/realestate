FactoryBot.define do
  factory :sub_account, class: 'SubAccount' do
    sequence(:name, 'A') { |n| "SubAccount #{n}" }
    balance { 3000 }
    agency { create(:agency) }
    account { create(:account, agency: agency) }
  end
end