FactoryBot.define do
  factory :user_agency, class: 'UserAgency' do
    user { create(:user) }
    agency { create(:agency) }
    selected { true }
  end
end