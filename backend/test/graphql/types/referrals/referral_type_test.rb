require 'test_helper'

class Types::Referrals::ReferralTypeTest < ActiveSupport::TestCase
  test 'return possible types of Referral' do 
    assert_equal Types::Referrals::ReferralType.possible_types, [Types::Users::UserType, Types::Agencies::AgencyType]
  end 
end
