require 'test_helper'

class Types::Documentables::DocumentableTypeTest < ActiveSupport::TestCase
  test 'return possible types of Document' do 
    assert_equal Types::Documentables::DocumentableType.possible_types, [Types::Contracts::ContractType]
  end 
end
