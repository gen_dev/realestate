require 'test_helper'

class Types::Agencies::AgencyInputTypeTest < ActiveSupport::TestCase
  test 'returns correct types for each argument' do 
    klass = Types::Agencies::AgencyInputType
    assert klass.arguments
    assert_nil klass.arguments["id"]
    assert_equal klass.arguments["name"].type.to_type_signature, "String!"
  end 
end
