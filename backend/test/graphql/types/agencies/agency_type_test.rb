require 'test_helper'

class Types::Agencies::AgencyTypeTest < ActiveSupport::TestCase
  test 'returns correct types for each argument' do 
    klass = Types::Agencies::AgencyType
    assert klass.fields
    assert_equal klass.fields["id"].type.to_type_signature, "ID!"
    assert_equal klass.fields["name"].type.to_type_signature, "String!"
    assert_equal klass.fields["address"].type.to_type_signature, "Address"
    assert_equal klass.fields["properties"].type.to_type_signature, "[Property!]"
    assert_equal klass.fields["users"].type.to_type_signature, "[User!]"
    assert_equal klass.fields["userAgencies"].type.to_type_signature, "[UserAgency!]"
  end 
end
