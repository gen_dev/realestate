require 'test_helper'

class Types::Addressables::AddressableTypeTest < ActiveSupport::TestCase
  test 'return possible types of Address' do 
    assert_equal Types::Addressables::AddressableType.possible_types, [Types::Users::UserType, Types::Agencies::AgencyType, Types::Properties::PropertyType]
  end 
end
