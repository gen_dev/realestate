require 'test_helper'

class Types::Images::ImageTypeTest < ActiveSupport::TestCase
  test 'returns correct types for each argument' do 
    klass = Types::Images::ImageType
    assert klass.fields
    assert_equal klass.fields["id"].type.to_type_signature, "ID!"
    assert_equal klass.fields["name"].type.to_type_signature, "String!"
    assert_equal klass.fields["size"].type.to_type_signature, "Int!"
    assert_equal klass.fields["url"].type.to_type_signature, "String!"
    assert_equal klass.fields["imageable"].type.to_type_signature, "Imageable!"
  end 
end
