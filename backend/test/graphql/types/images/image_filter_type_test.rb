require 'test_helper'

class Types::Images::ImageFilterTypeTest < ActiveSupport::TestCase
  test 'returns correct types for each argument' do 
    klass = Types::Images::ImageFilterType
    assert klass.arguments
    assert_nil klass.arguments["id"]
    assert_equal klass.arguments["name"].type.to_type_signature, "String"
    assert_equal klass.arguments["size"].type.to_type_signature, "Int"
  end 
end
