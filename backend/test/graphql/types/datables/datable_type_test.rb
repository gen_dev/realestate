require 'test_helper'

class Types::Datables::DatableTypeTest < ActiveSupport::TestCase
  test 'return possible types of Datable' do 
    assert_equal Types::Datables::DatableType.possible_types, [Types::Addresses::AddressType, Types::Agencies::AgencyType, Types::SubAccounts::SubAccountType, Types::Contracts::ContractType, Types::Contracts::ContractExpenseType, Types::Contracts::ContractPlanType, Types::Payments::PaymentType, Types::Payments::PaymentPlanType, Types::Payments::PartialPaymentType, Types::Properties::PropertyType, Types::Taxes::TaxType, Types::Users::UserType]
  end 
end