require 'test_helper'

class Types::Contracts::ContractInputTypeTest < ActiveSupport::TestCase
  test 'returns correct types for each argument' do 
    klass = Types::Contracts::ContractInputType
    assert klass.arguments
    assert_equal klass.arguments["id"].type.to_type_signature, "ID"
    assert_equal klass.arguments["state"].type.to_type_signature, "String!"
    assert_equal klass.arguments["contractType"].type.to_type_signature, "String!"
  end 
end
