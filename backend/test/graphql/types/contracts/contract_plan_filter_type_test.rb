require 'test_helper'

class Types::Contracts::ContractPlanFilterTypeTest < ActiveSupport::TestCase
  test 'returns correct types for each argument' do 
    klass = Types::Contracts::ContractPlanFilterType
    assert klass.arguments
    assert_nil klass.arguments["id"]
    assert_equal klass.arguments["duration"].type.to_type_signature, "Int"
    assert_equal klass.arguments["rentUpdatePeriod"].type.to_type_signature, "Int"
    assert_equal klass.arguments["rentValue"].type.to_type_signature, "JSON"
  end 
end