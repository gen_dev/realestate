require 'test_helper'

class Types::Contracts::ContractTypeTest < ActiveSupport::TestCase
  test 'returns correct types for each argument' do 
    klass = Types::Contracts::ContractType
    assert klass.fields
    assert_equal klass.fields["id"].type.to_type_signature, "ID!"
    assert_equal klass.fields["state"].type.to_type_signature, "String!"
    assert_equal klass.fields["contractType"].type.to_type_signature, "String!"
    assert_equal klass.fields["documents"].type.to_type_signature, "[Document!]"
    assert_equal klass.fields["property"].type.to_type_signature, "Property!"
    assert_equal klass.fields["contractPlan"].type.to_type_signature, "ContractPlan"
    assert_equal klass.fields["paymentPlan"].type.to_type_signature, "PaymentPlan"
    assert_equal klass.fields["taxes"].type.to_type_signature, "[Tax!]"
    assert_equal klass.fields["payments"].type.to_type_signature, "[Payment!]"
    assert_equal klass.fields["owner"].type.to_type_signature, "[User!]"
    assert_equal klass.fields["tenant"].type.to_type_signature, "[User!]"
    assert_equal klass.fields["overduePayments"].type.to_type_signature, "[Payment!]"
  end 
end
