require 'test_helper'

class Types::Contracts::ContractFilterTypeTest < ActiveSupport::TestCase
  test 'returns correct types for each argument' do 
    klass = Types::Contracts::ContractFilterType
    assert klass.arguments
    assert_nil klass.arguments["id"]
    assert_equal klass.arguments["state"].type.to_type_signature, "[String!]"
    assert_equal klass.arguments["contractType"].type.to_type_signature, "String"
    assert_equal klass.arguments["owner"].type.to_type_signature, "String"
    assert_equal klass.arguments["tenant"].type.to_type_signature, "String"
  end 
end
