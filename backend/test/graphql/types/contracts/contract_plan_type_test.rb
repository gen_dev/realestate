require 'test_helper'

class Types::Contracts::ContractPlanTypeTest < ActiveSupport::TestCase
  test 'returns correct types for each argument' do 
    klass = Types::Contracts::ContractPlanType
    assert klass.fields
    assert_equal klass.fields["id"].type.to_type_signature, "ID!"
    assert_equal klass.fields["duration"].type.to_type_signature, "Int!"
    assert_equal klass.fields["rentUpdatePeriod"].type.to_type_signature, "Int!"
    assert_equal klass.fields["rentValue"].type.to_type_signature, "JSON!"
    assert_equal klass.fields["property"].type.to_type_signature, "Property!"
    assert_equal klass.fields["contract"].type.to_type_signature, "Contract!"
    assert_equal klass.fields["contractExpenses"].type.to_type_signature, "[ContractExpense!]"
    assert_equal klass.fields["totalAmount"].type.to_type_signature, "Float"
  end 
end