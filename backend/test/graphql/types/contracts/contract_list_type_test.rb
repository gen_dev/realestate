require 'test_helper'

class Types::Contracts::ContractListTypeTest < ActiveSupport::TestCase
  test 'returns correct types for each argument' do 
    klass = Types::Contracts::ContractListType
    assert klass.fields
    assert_nil klass.fields["id"]
    assert_equal klass.fields["pages"].type.to_type_signature, "Int"
    assert_equal klass.fields["contracts"].type.to_type_signature, "[Contract!]"
  end 
end
