require 'test_helper'

class Types::Contracts::ContractExpenseFilterTypeTest < ActiveSupport::TestCase
  test 'returns correct types for each argument' do 
    klass = Types::Contracts::ContractExpenseFilterType
    assert klass.arguments
    assert_nil klass.arguments["id"]
    assert_equal klass.arguments["name"].type.to_type_signature, "String"
    assert_equal klass.arguments["value"].type.to_type_signature, "Float"
  end 
end
