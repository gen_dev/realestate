require 'test_helper'

class Types::Contracts::ContractExpenseTypeTest < ActiveSupport::TestCase
  test 'returns correct types for each argument' do 
    klass = Types::Contracts::ContractExpenseType
    assert klass.fields
    assert_equal klass.fields["id"].type.to_type_signature, "ID!"
    assert_equal klass.fields["name"].type.to_type_signature, "String!"
    assert_equal klass.fields["value"].type.to_type_signature, "Float!"
    assert_equal klass.fields["contractPlan"].type.to_type_signature, "ContractPlan!"
  end 
end
