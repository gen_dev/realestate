require 'test_helper'

class Types::SimpleDatas::SimpleDataInputTypeTest < ActiveSupport::TestCase
  test 'returns correct types for each argument' do 
    klass = Types::SimpleDatas::SimpleDataInputType
    assert klass.arguments
    assert_equal klass.arguments["id"].type.to_type_signature, "ID"
    assert_equal klass.arguments["name"].type.to_type_signature, "String!"
    assert_equal klass.arguments["value"].type.to_type_signature, "String!"
    assert_equal klass.arguments["category"].type.to_type_signature, "String"
  end 
end
