require 'test_helper'

class Types::SimpleDatas::SimpleDataTypeTest < ActiveSupport::TestCase
  test 'returns correct types for each argument' do 
    klass = Types::SimpleDatas::SimpleDataType
    assert klass.fields
    assert_equal klass.fields["id"].type.to_type_signature, "ID!"
    assert_equal klass.fields["name"].type.to_type_signature, "String!"
    assert_equal klass.fields["value"].type.to_type_signature, "String!"
    assert_equal klass.fields["category"].type.to_type_signature, "String"
    assert_equal klass.fields["datable"].type.to_type_signature, "Datable!"
  end 
end
