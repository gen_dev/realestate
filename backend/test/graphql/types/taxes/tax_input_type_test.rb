require 'test_helper'

class Types::Taxes::TaxInputTypeTest < ActiveSupport::TestCase
  test 'returns correct types for each argument' do 
    klass = Types::Taxes::TaxInputType
    assert klass.arguments
    assert_equal klass.arguments["id"].type.to_type_signature, "ID"
    assert_equal klass.arguments["name"].type.to_type_signature, "String!"
    assert_equal klass.arguments["state"].type.to_type_signature, "String"
    assert_equal klass.arguments["duration"].type.to_type_signature, "Int"
  end 
end
