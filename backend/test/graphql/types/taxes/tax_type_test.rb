require 'test_helper'

class Types::Taxes::TaxTypeTest < ActiveSupport::TestCase
  test 'returns correct types for each argument' do 
    klass = Types::Taxes::TaxType
    assert klass.fields
    assert_equal klass.fields["id"].type.to_type_signature, "ID!"
    assert_equal klass.fields["name"].type.to_type_signature, "String!"
    assert_equal klass.fields["state"].type.to_type_signature, "String!"
    assert_equal klass.fields["interval"].type.to_type_signature, "Int"
    assert_equal klass.fields["duration"].type.to_type_signature, "Int"
    assert_equal klass.fields["lastPaymentDate"].type.to_type_signature, "ISO8601Date"
    assert_equal klass.fields["contract"].type.to_type_signature, "Contract!"
    assert_equal klass.fields["debtor"].type.to_type_signature, "Debtor!"
  end 
end