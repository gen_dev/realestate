require 'test_helper'

class Types::Taxes::TaxFilterTypeTest < ActiveSupport::TestCase
  test 'returns correct types for each argument' do 
    klass = Types::Taxes::TaxFilterType
    assert klass.arguments
    assert_nil klass.arguments["id"]
    assert_equal klass.arguments["name"].type.to_type_signature, "String"
    assert_equal klass.arguments["state"].type.to_type_signature, "String"
    assert_equal klass.arguments["interval"].type.to_type_signature, "Int"
    assert_equal klass.arguments["duration"].type.to_type_signature, "Int"
  end 
end
