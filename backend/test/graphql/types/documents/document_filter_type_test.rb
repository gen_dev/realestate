require 'test_helper'

class Types::Documents::DocumentFilterTypeTest < ActiveSupport::TestCase
  test 'returns correct types for each argument' do 
    klass = Types::Documents::DocumentFilterType
    assert klass.arguments
    assert_nil klass.arguments["id"]
    assert_equal klass.arguments["name"].type.to_type_signature, "String"
    assert_equal klass.arguments["size"].type.to_type_signature, "Int"
  end 
end
