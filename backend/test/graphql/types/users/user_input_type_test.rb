require 'test_helper'

class Types::Users::UserInputTypeTest < ActiveSupport::TestCase
  test 'returns correct types for each argument' do 
    klass = Types::Users::UserInputType
    assert klass.arguments
    assert_nil klass.arguments["id"]
    assert_equal klass.arguments["email"].type.to_type_signature, "String!"
  end 
end
