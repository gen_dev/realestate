require 'test_helper'

class Types::Users::ContactListTypeTest < ActiveSupport::TestCase
  test 'returns correct types for each argument' do 
    klass = Types::Users::ContactListType
    assert klass.fields
    assert_nil klass.fields["id"]
    assert_equal klass.fields["pages"].type.to_type_signature, "Int"
    assert_equal klass.fields["contacts"].type.to_type_signature, "[User!]"
  end 
end
