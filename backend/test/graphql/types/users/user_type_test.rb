require 'test_helper'

class Types::Users::UserTypeTest < ActiveSupport::TestCase
  test 'returns correct types for each argument' do 
    klass = Types::Users::UserType
    assert klass.fields
    assert_equal klass.fields["id"].type.to_type_signature, "ID!"
    assert_equal klass.fields["email"].type.to_type_signature, "String!"
    assert_equal klass.fields["address"].type.to_type_signature, "Address"
    assert_equal klass.fields["userAgencies"].type.to_type_signature, "[UserAgency!]"
    assert_equal klass.fields["agencies"].type.to_type_signature, "[Agency!]"
    assert_equal klass.fields["activeContracts"].type.to_type_signature, "[Contract!]"
    assert_equal klass.fields["properties"].type.to_type_signature, "[Property!]"
    assert_equal klass.fields["permissions"].type.to_type_signature, "JSON"
  end 
end
