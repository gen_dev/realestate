require 'test_helper'

class Types::Users::UserFilterTypeTest < ActiveSupport::TestCase
  test 'returns correct types for each argument' do 
    klass = Types::Users::UserFilterType
    assert klass.arguments
    assert_nil klass.arguments["id"]
    assert_equal klass.arguments["email"].type.to_type_signature, "String"
  end 
end
