require 'test_helper'

class Types::UserAgencies::UserAgencyInputTypeTest < ActiveSupport::TestCase
  test 'returns correct types for each argument' do 
    klass = Types::UserAgencies::UserAgencyInputType
    assert klass.arguments
    assert_nil klass.arguments["id"]
    assert_equal klass.arguments["selected"].type.to_type_signature, "Boolean"
  end 
end
