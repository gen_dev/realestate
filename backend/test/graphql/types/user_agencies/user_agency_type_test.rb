require 'test_helper'

class Types::UserAgencies::UserAgencyTypeTest < ActiveSupport::TestCase
  test 'returns correct types for each argument' do 
    klass = Types::UserAgencies::UserAgencyType
    assert klass.fields
    assert_equal klass.fields["id"].type.to_type_signature, "ID!"
    assert_equal klass.fields["selected"].type.to_type_signature, "Boolean"
    assert_equal klass.fields["user"].type.to_type_signature, "User!"
    assert_equal klass.fields["agency"].type.to_type_signature, "Agency!"
  end 
end