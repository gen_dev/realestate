require 'test_helper'

class Types::Debtors::DebtorTypeTest < ActiveSupport::TestCase
  test 'return possible types of Debtors' do 
    assert_equal Types::Debtors::DebtorType.possible_types, [Types::Users::UserType, Types::Agencies::AgencyType]
  end 
end
