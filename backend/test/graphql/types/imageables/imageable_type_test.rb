require 'test_helper'

class Types::Imageables::ImageableTypeTest < ActiveSupport::TestCase
  test 'return possible types of Image' do 
    assert_equal Types::Imageables::ImageableType.possible_types, [Types::Properties::PropertyType]
  end 
end
