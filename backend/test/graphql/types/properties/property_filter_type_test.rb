require 'test_helper'

class Types::Properties::PropertyFilterTypeTest < ActiveSupport::TestCase
  test 'returns correct types for each argument' do 
    klass = Types::Properties::PropertyFilterType
    assert klass.arguments
    assert_nil klass.arguments["id"]
    assert_equal klass.arguments["propertyType"].type.to_type_signature, "[String!]"
    assert_equal klass.arguments["state"].type.to_type_signature, "[String!]"
    assert_equal klass.arguments["description"].type.to_type_signature, "String"
  end 
end
