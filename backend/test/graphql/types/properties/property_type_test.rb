require 'test_helper'

class Types::Properties::PropertyTypeTest < ActiveSupport::TestCase
  test 'returns correct types for each argument' do 
    klass = Types::Properties::PropertyType
    assert klass.fields
    assert_equal klass.fields["id"].type.to_type_signature, "ID!"
    assert_equal klass.fields["propertyType"].type.to_type_signature, "String!"
    assert_equal klass.fields["state"].type.to_type_signature, "String!"
    assert_equal klass.fields["description"].type.to_type_signature, "String"
    assert_equal klass.fields["price"].type.to_type_signature, "String"
    assert_equal klass.fields["agency"].type.to_type_signature, "Agency!"
    assert_equal klass.fields["images"].type.to_type_signature, "[Image!]"
    assert_equal klass.fields["activeTenant"].type.to_type_signature, "[User!]"
    assert_equal klass.fields["activeContract"].type.to_type_signature, "Contract"
    assert_equal klass.fields["address"].type.to_type_signature, "Address"
    assert_equal klass.fields["contracts"].type.to_type_signature, "[Contract!]"
    assert_equal klass.fields["owner"].type.to_type_signature, "[User!]"
    assert_equal klass.fields["characteristics"].type.to_type_signature, "[SimpleData!]"
    assert_equal klass.fields["features"].type.to_type_signature, "JSON"
  end 
end
