require 'test_helper'

class Types::Properties::PropertyInputTypeTest < ActiveSupport::TestCase
  test 'returns correct types for each argument' do 
    klass = Types::Properties::PropertyInputType
    assert klass.arguments
    assert_nil klass.arguments["id"]
    assert_equal klass.arguments["propertyType"].type.to_type_signature, "String!"
    assert_equal klass.arguments["state"].type.to_type_signature, "String!"
    assert_equal klass.arguments["description"].type.to_type_signature, "String"
    assert_equal klass.arguments["price"].type.to_type_signature, "ID"
    assert_equal klass.arguments["features"].type.to_type_signature, "FeatureInput!"
    assert_equal klass.arguments["addressAttributes"].type.to_type_signature, "AddressInput!"
  end 
end
