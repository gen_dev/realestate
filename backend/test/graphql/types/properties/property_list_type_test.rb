require 'test_helper'

class Types::Properties::PropertyListTypeTest < ActiveSupport::TestCase
  test 'returns correct types for each argument' do 
    klass = Types::Properties::PropertyListType
    assert klass.fields
    assert_nil klass.fields["id"]
    assert_equal klass.fields["pages"].type.to_type_signature, "Int"
    assert_equal klass.fields["properties"].type.to_type_signature, "[Property!]"
  end 
end
