require 'test_helper'

class Types::Addresses::AddressFilterTypeTest < ActiveSupport::TestCase
  test 'returns correct types for each argument' do 
    klass = Types::Addresses::AddressFilterType
    assert klass.arguments
    assert_nil klass.arguments["id"]
    assert_equal klass.arguments["street"].type.to_type_signature, "String"
    assert_equal klass.arguments["number"].type.to_type_signature, "String"
    assert_equal klass.arguments["unit"].type.to_type_signature, "String"
    assert_equal klass.arguments["city"].type.to_type_signature, "String"
    assert_equal klass.arguments["state"].type.to_type_signature, "String"
    assert_equal klass.arguments["country"].type.to_type_signature, "String"
    assert_equal klass.arguments["neighborhood"].type.to_type_signature, "String"
  end 
end
