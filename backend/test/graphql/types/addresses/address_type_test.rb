require 'test_helper'

class Types::Addresses::AddressTypeTest < ActiveSupport::TestCase
  test 'returns correct types for each argument' do 
    klass = Types::Addresses::AddressType
    assert klass.fields
    assert_equal klass.fields["id"].type.to_type_signature, "ID!"
    assert_equal klass.fields["street"].type.to_type_signature, "String!"
    assert_equal klass.fields["number"].type.to_type_signature, "String!"
    assert_equal klass.fields["unit"].type.to_type_signature, "String"
    assert_equal klass.fields["city"].type.to_type_signature, "String!"
    assert_equal klass.fields["state"].type.to_type_signature, "String!"
    assert_equal klass.fields["country"].type.to_type_signature, "String!"
    assert_equal klass.fields["neighborhood"].type.to_type_signature, "String"
    assert_equal klass.fields["addressable"].type.to_type_signature, "Addressable!"
  end 
end
