require 'test_helper'

class Types::Payments::PartialPaymentTypeTest < ActiveSupport::TestCase
  test 'returns correct types for each argument' do 
    klass = Types::Payments::PartialPaymentType
    assert klass.fields
    assert_equal klass.fields["id"].type.to_type_signature, "ID!"
    assert_equal klass.fields["value"].type.to_type_signature, "Float!"
    assert_equal klass.fields["payment"].type.to_type_signature, "Payment!"
  end 
end
