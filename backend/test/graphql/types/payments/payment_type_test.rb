require 'test_helper'

class Types::Payments::PaymentTypeTest < ActiveSupport::TestCase
  test 'returns correct types for each argument' do 
    klass = Types::Payments::PaymentType
    assert klass.fields
    assert_equal klass.fields["id"].type.to_type_signature, "ID!"
    assert_equal klass.fields["paymentType"].type.to_type_signature, "String"
    assert_equal klass.fields["period"].type.to_type_signature, "String!"
    assert_equal klass.fields["concept"].type.to_type_signature, "String!"
    assert_equal klass.fields["state"].type.to_type_signature, "String!"
    assert_equal klass.fields["dueDate"].type.to_type_signature, "ISO8601Date!"
    assert_equal klass.fields["amount"].type.to_type_signature, "Float!"
    assert_equal klass.fields["observations"].type.to_type_signature, "String"
    assert_equal klass.fields["contract"].type.to_type_signature, "Contract"
    assert_equal klass.fields["referral"].type.to_type_signature, "Referral!"
    assert_equal klass.fields["partialPayments"].type.to_type_signature, "[PartialPayment!]"
  end 
end
