require 'test_helper'

class Types::Payments::PaymentPlanTypeTest < ActiveSupport::TestCase
  test 'returns correct types for each argument' do 
    klass = Types::Payments::PaymentPlanType
    assert klass.fields
    assert_equal klass.fields["id"].type.to_type_signature, "ID!"
    assert_equal klass.fields["tenant"].type.to_type_signature, "String!"
    assert_equal klass.fields["owner"].type.to_type_signature, "String!"
    assert_equal klass.fields["paymentMethods"].type.to_type_signature, "JSON!"
    assert_equal klass.fields["contract"].type.to_type_signature, "Contract!"
  end 
end
