require 'test_helper'

class Types::Payments::PaymentPlanInputTypeTest < ActiveSupport::TestCase
  test 'returns correct types for each argument' do 
    klass = Types::Payments::PaymentPlanInputType
    assert klass.arguments
    assert_equal klass.arguments["id"].type.to_type_signature, "ID"
    assert_equal klass.arguments["paymentMethods"].type.to_type_signature, "[String!]"
  end 
end
