require 'test_helper'

class Types::Payments::PartialPaymentFilterTypeTest < ActiveSupport::TestCase
  test 'returns correct types for each argument' do 
    klass = Types::Payments::PartialPaymentFilterType
    assert klass.arguments
    assert_nil klass.arguments["id"]
    assert_equal klass.arguments["value"].type.to_type_signature, "Float"
  end 
end
