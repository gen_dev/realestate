require 'test_helper'

class Types::Payments::PaymentPlanFilterTypeTest < ActiveSupport::TestCase
  test 'returns correct types for each argument' do 
    klass = Types::Payments::PaymentPlanFilterType
    assert klass.arguments
    assert_nil klass.arguments["id"]
    assert_equal klass.arguments["tenant"].type.to_type_signature, "String"
    assert_equal klass.arguments["owner"].type.to_type_signature, "String"
    assert_equal klass.arguments["paymentMethods"].type.to_type_signature, "JSON"
  end 
end
