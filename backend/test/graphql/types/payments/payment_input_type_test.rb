require 'test_helper'

class Types::Payments::PaymentInputTypeTest < ActiveSupport::TestCase
  test 'returns correct types for each argument' do 
    klass = Types::Payments::PaymentInputType
    assert klass.arguments
    assert_nil klass.arguments["id"]
    assert_equal klass.arguments["paymentType"].type.to_type_signature, "String"
    assert_equal klass.arguments["period"].type.to_type_signature, "String"
    assert_equal klass.arguments["concept"].type.to_type_signature, "String!"
    assert_equal klass.arguments["state"].type.to_type_signature, "String!"
    assert_equal klass.arguments["dueDate"].type.to_type_signature, "String"
    assert_equal klass.arguments["amount"].type.to_type_signature, "ID!"
    assert_equal klass.arguments["observations"].type.to_type_signature, "String"
  end 
end
