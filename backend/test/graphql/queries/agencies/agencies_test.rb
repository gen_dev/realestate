require 'test_helper'

class Queries::AgenciesTest < ActiveSupport::TestCase
  def perform(query, context = {})
    BackendSchema.execute(query, context: context).to_h
  end

  context 'when current user has gendev permissions' do
    setup do 
      @current_user = create(:user_with_role_gendev)
      @context = { 
        current_user: @current_user,
        current_ability: Ability.new(@current_user)
      }
    end 

    context 'when ask for multiples attrs' do 
      setup do 
        @query_string = <<-GRAPHQL
          query {
            agencies {
              id
              name
            }
          }
        GRAPHQL
      end 

      context 'when there arent agencies' do 
        should 'return empty array' do 
          response = perform(@query_string, @context)
          assert_empty response["data"]["agencies"]
        end
      end
      
      context 'when there are agencies' do
        setup do 
          @agency1 = create(:agency)
          @agency2 = create(:agency)
        end
        
        should 'return multiple agencies' do 
          response = perform(@query_string, @context)
          assert response["data"]["agencies"]
          assert_operator response["data"]["agencies"].length, :>, 1
          assert_equal response["data"]["agencies"].length, Agency.all.length
        end 

        should 'return attrs by agency' do 
          response = perform(@query_string, @context)
          assert_equal response["data"]["agencies"][0]["name"], @agency1.name
          assert_equal response["data"]["agencies"][0]["id"].to_i, @agency1.id
          assert_equal response["data"]["agencies"][1]["name"], @agency2.name
          assert_equal response["data"]["agencies"][1]["id"].to_i, @agency2.id
        end
      end  
    end 

    context 'when ask for only 1 attr' do 
      setup do 
        @query_string = <<-GRAPHQL
          query {
            agencies {
              name
            }
          }
        GRAPHQL
      end
      
      context 'when there arent agencies' do 
        should 'return empty array' do 
          response = perform(@query_string, @context)
          assert_empty response["data"]["agencies"]
        end
      end
      
      context 'when there are agencies' do
        setup do 
          create(:agency)
          create(:agency)
        end

        should 'return only 1 attr by agency' do 
          response = perform(@query_string, @context)
          assert response["data"]["agencies"][0]["name"]
          assert_nil response["data"]["agencies"][0]["id"]
          assert response["data"]["agencies"][1]["name"]
          assert_nil response["data"]["agencies"][1]["id"]
        end
      end
    end 

    context 'when use filters' do 
      setup do 
        @name = 'GenDev'
        @query_string = <<-GRAPHQL
          query {
            agencies(filters: { name: "#{@name}" } ) {
              id
              name
            }
          }
        GRAPHQL
      end 

      context 'when agencies dont match filter' do 
        setup do 
          create(:agency, name: "Random")
        end 

        should 'return empty array' do 
          response = perform(@query_string, @context)
          assert_empty response["data"]["agencies"]
        end
      end

      context 'when there are agencies matching filter' do 
        setup do 
          create(:agency, name: @name)
          create(:agency, name: "Random Agency")
          create(:agency, name: "Another Agency")
        end 
        
        should 'return only correct agencies' do 
          response = perform(@query_string, @context)
          assert response["data"]["agencies"]
          assert_equal response["data"]["agencies"].length, 1
          assert response["data"]["agencies"][0]["id"]
          assert_equal response["data"]["agencies"][0]["name"], @name
        end
      end
    end 
  end

  context 'when current user has admin permissions' do
    setup do 
      @current_user = create(:user_with_role_admin)
      @context = { 
        current_user: @current_user,
        current_ability: Ability.new(@current_user)
      }
    end 

    context 'when ask for multiples attrs' do 
      setup do 
        @query_string = <<-GRAPHQL
          query {
            agencies {
              id
              name
            }
          }
        GRAPHQL
      end 

      context 'when there arent agencies' do 
        should 'return empty array' do 
          response = perform(@query_string, @context)
          assert_empty response["data"]["agencies"]
        end
      end
      
      context 'when there are agencies' do
        setup do 
          @agency1 = create(:agency)
          @agency2 = create(:agency)
        end
        
        context 'when there arent agencies related to user' do 
          should 'return empty array' do 
            response = perform(@query_string, @context)
            assert_empty response["data"]["agencies"]
          end
        end 

        context 'when there are agencies related to user' do 
          setup do 
            create(:user_agency, agency: @agency1, user: @current_user)
            @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
          end 
        
          should 'return only related agencies' do 
            response = perform(@query_string, @context)
            assert response["data"]["agencies"]
            assert_equal response["data"]["agencies"].length, 1
          end
        end 

        context 'when all agencies are related to user' do 
          setup do 
            create(:user_agency, agency: @agency1, user: @current_user)
            create(:user_agency, agency: @agency2, user: @current_user)
            @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
          end 

          should 'return multiple agencies' do 
            response = perform(@query_string, @context)
            assert response["data"]["agencies"]
            assert_operator response["data"]["agencies"].length, :>, 1
            assert_equal response["data"]["agencies"].length, Agency.all.length
          end 

          should 'return attrs by agency' do 
            response = perform(@query_string, @context)
            agencies = response["data"]["agencies"].sort_by { |x| x["id"].to_i }

            assert_equal agencies[0]["name"], @agency1.name
            assert_equal agencies[0]["id"].to_i, @agency1.id

            assert_equal agencies[1]["name"], @agency2.name
            assert_equal agencies[1]["id"].to_i, @agency2.id
          end
        end  
      end  
    end 

    context 'when ask for only 1 attr' do 
      setup do 
        @query_string = <<-GRAPHQL
          query {
            agencies {
              name
            }
          }
        GRAPHQL
      end
      
      context 'when there arent agencies' do 
        should 'return empty array' do 
          response = perform(@query_string, @context)
          assert_empty response["data"]["agencies"]
        end
      end
      
      context 'when there are agencies' do
        setup do 
          @agency1 = create(:agency)
          @agency2 = create(:agency)
        end

        context 'when there arent agencies related to user' do 
          should 'return empty array' do 
            response = perform(@query_string, @context)
            assert_empty response["data"]["agencies"]
          end
        end 

        context 'when there are agencies related to user' do 
          setup do 
            create(:user_agency, agency: @agency1, user: @current_user)
            @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
          end 
        
          should 'return only 1 attr by each related agency' do 
            response = perform(@query_string, @context)
            assert response["data"]["agencies"]
            assert_equal response["data"]["agencies"].length, 1
            assert response["data"]["agencies"][0]["name"]
            assert_nil response["data"]["agencies"][0]["id"]
          end
        end 

        context 'when all agencies are related to user' do 
          setup do 
            create(:user_agency, agency: @agency1, user: @current_user)
            create(:user_agency, agency: @agency2, user: @current_user)
            @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
          end 

          should 'return only 1 attr by agency' do 
            response = perform(@query_string, @context)
            assert response["data"]["agencies"][0]["name"]
            assert_nil response["data"]["agencies"][0]["id"]
            assert response["data"]["agencies"][1]["name"]
            assert_nil response["data"]["agencies"][1]["id"]
          end
        end
      end
    end 

    context 'when use filters' do 
      setup do 
        @name = 'GenDev'
        @query_string = <<-GRAPHQL
          query {
            agencies(filters: { name: "#{@name}" } ) {
              id
              name
            }
          }
        GRAPHQL
      end 

      context 'when agencies dont match filter' do 
        setup do 
          create(:agency, name: "Random")
        end 

        should 'return empty array' do 
          response = perform(@query_string, @context)
          assert_empty response["data"]["agencies"]
        end
      end

      context 'when there are agencies matching filter' do 
        setup do 
          @agency1 = create(:agency, name: @name)
          @agency2 = create(:agency, name: "Random Agency")
          @agency3 = create(:agency, name: "Another Agency")
        end 

        context 'when there arent related to user' do 
          should 'return empty array' do 
            response = perform(@query_string, @context)
            assert_empty response["data"]["agencies"]
          end
        end 

        context 'when there are agencies related to user' do 
          setup do 
            create(:user_agency, agency: @agency1, user: @current_user)
            @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
          end 
        
          should 'return only related and filtered agencies' do 
            response = perform(@query_string, @context)
            assert response["data"]["agencies"]
            assert_equal response["data"]["agencies"].length, 1
            assert response["data"]["agencies"][0]["id"]
            assert_equal response["data"]["agencies"][0]["name"], @name
          end
        end
      end
    end 
  end
end