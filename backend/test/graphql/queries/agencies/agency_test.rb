require 'test_helper'

class Queries::AgencyTest < ActiveSupport::TestCase
  def perform(query, context = {})
    BackendSchema.execute(query, context: context).to_h
  end

  context 'when current user has gendev permissions' do
    setup do 
      @current_user = create(:user_with_role_gendev)
      @context = { 
        current_user: @current_user,
        current_ability: Ability.new(@current_user)
      }
    end 

    context 'when there arent agencies' do 
      setup do 
        @query_string = <<-GRAPHQL
          query {
            agency(id: 1) {
              id
              name
            }
          }
        GRAPHQL
      end 

      should 'return nil' do 
        response = perform(@query_string, @context)
        assert_nil response["data"]["agency"]
      end
    end


    context 'when there are agencies' do
      setup do 
        @agency1 = create(:agency)
      end  

      context 'when ask for multiple attrs' do 
        context 'when agency doesnt exist' do 
          setup do 
            @query_string = <<-GRAPHQL
              query {
                agency(id: #{@agency1.id + 1}) {
                  id
                  name
                }
              }
            GRAPHQL
          end

          should 'return nil' do 
            response = perform(@query_string, @context)
            assert_nil response["data"]["agency"]
          end
        end

        context 'when agency exists' do
          setup do 
            @query_string = <<-GRAPHQL
              query {
                agency(id: #{@agency1.id}) {
                  id
                  name
                }
              }
            GRAPHQL
          end

          should 'return attrs by agency' do 
            response = perform(@query_string, @context)
            assert_equal response["data"]["agency"]["name"], @agency1.name
            assert_equal response["data"]["agency"]["id"].to_i, @agency1.id
          end
        end 
      end 

      context 'when ask for only 1 attr' do 
        context 'when agency doesnt exist' do 
          setup do 
            @query_string = <<-GRAPHQL
              query {
                agency(id: #{@agency1.id + 1}) {
                  name
                }
              }
            GRAPHQL
          end

          should 'return nil' do 
            response = perform(@query_string, @context)
            assert_nil response["data"]["agency"]
          end
        end

        context 'when agency exists' do
          setup do 
            @query_string = <<-GRAPHQL
              query {
                agency(id: #{@agency1.id}) {
                  name
                }
              }
            GRAPHQL
          end

          should 'return only 1 attr for specific agency' do 
            response = perform(@query_string, @context)
            assert_equal response["data"]["agency"]["name"], @agency1.name
            assert_nil response["data"]["agency"]["id"]
          end
        end 
      end 
    end
  end

  context 'when current user has admin permissions' do
    setup do 
      @current_user = create(:user_with_role_admin)
      @context = { 
        current_user: @current_user,
        current_ability: Ability.new(@current_user)
      }
    end 

    context 'when there arent agencies' do 
      setup do 
        @query_string = <<-GRAPHQL
          query {
            agency(id: 1) {
              id
              name
            }
          }
        GRAPHQL
      end 

      should 'return nil' do 
        response = perform(@query_string, @context)
        assert_nil response["data"]["agency"]
      end
    end


    context 'when there are agencies' do
      setup do 
        @agency1 = create(:agency)
      end  

      context 'when ask for multiple attrs' do 
        context 'when agency doesnt exist' do 
          setup do 
            @query_string = <<-GRAPHQL
              query {
                agency(id: #{@agency1.id + 1}) {
                  id
                  name
                }
              }
            GRAPHQL
          end

          should 'return nil' do 
            response = perform(@query_string, @context)
            assert_nil response["data"]["agency"]
          end
        end

        context 'when agency exists' do
          setup do 
            @query_string = <<-GRAPHQL
              query {
                agency(id: #{@agency1.id}) {
                  id
                  name
                }
              }
            GRAPHQL
          end

          context 'when isnt related to user' do 
            should 'return "unauthorized" message' do 
              response = perform(@query_string, @context)
              assert_nil response['data']['address']
              assert_equal response['errors'][0]['message'], 'user unauthorized'
            end
          end 

          context 'when is related to user' do 
            setup do 
              create(:user_agency, agency: @agency1, user: @current_user)
              @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
            end 

            should 'return attrs by agency' do 
              response = perform(@query_string, @context)
              assert_equal response["data"]["agency"]["name"], @agency1.name
              assert_equal response["data"]["agency"]["id"].to_i, @agency1.id
            end
          end 
        end 
      end 

      context 'when ask for only 1 attr' do 
        context 'when agency doesnt exist' do 
          setup do 
            @query_string = <<-GRAPHQL
              query {
                agency(id: #{@agency1.id + 1}) {
                  name
                }
              }
            GRAPHQL
          end

          should 'return nil' do 
            response = perform(@query_string, @context)
            assert_nil response["data"]["agency"]
          end
        end

        context 'when agency exists' do
          setup do 
            @query_string = <<-GRAPHQL
              query {
                agency(id: #{@agency1.id}) {
                  name
                }
              }
            GRAPHQL
          end

          context 'when isnt related to user' do 
            should 'return "unauthorized" message' do 
              response = perform(@query_string, @context)
              assert_nil response['data']['address']
              assert_equal response['errors'][0]['message'], 'user unauthorized'
            end
          end 

          context 'when is related to user' do 
            setup do 
              create(:user_agency, agency: @agency1, user: @current_user)
              @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
            end 

            should 'return only 1 attr for specific agency' do 
              response = perform(@query_string, @context)
              assert_equal response["data"]["agency"]["name"], @agency1.name
              assert_nil response["data"]["agency"]["id"]
            end
          end
        end 
      end 
    end
  end
end