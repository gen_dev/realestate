require 'test_helper'

class Queries::ImageTest < ActiveSupport::TestCase
  def perform(query, context = {})
    BackendSchema.execute(query, context: context).to_h
  end

  context 'when current user has gendev permissions' do
    setup do 
      @current_user = create(:user_with_role_gendev)
      @context = { 
        current_user: @current_user,
        current_ability: Ability.new(@current_user)
      }
    end 

    context 'when there arent images' do 
      setup do 
        @query_string = <<-GRAPHQL
          query {
            image(id: 1) {
              id
              name
              size
              url
            }
          }
        GRAPHQL
      end 

      should 'return nil' do 
        response = perform(@query_string, @context)
        assert_nil response["data"]["image"]
      end
    end


    context 'when there are images' do
      setup do 
        @image1 = create(:image)
      end  

      context 'when ask for multiple attrs' do 
        context 'when image doesnt exist' do 
          setup do 
            @query_string = <<-GRAPHQL
              query {
                image(id: #{@image1.id + 1}) {
                  id
                  name
                  size
                  url
                }
              }
            GRAPHQL
          end

          should 'return nil' do 
            response = perform(@query_string, @context)
            assert_nil response["data"]["image"]
          end
        end

        context 'when image exists' do
          setup do 
            @query_string = <<-GRAPHQL
              query {
                image(id: #{@image1.id}) {
                  id
                  name
                  size
                  url
                }
              }
            GRAPHQL
          end

          should 'return only attrs by image' do 
            response = perform(@query_string, @context)
            assert_equal response["data"]["image"]["id"].to_i, @image1.id
            assert_equal response["data"]["image"]["name"], @image1.name
            assert_equal response["data"]["image"]["size"], @image1.size
            assert_equal response["data"]["image"]["url"], @image1.url
          end
        end 
      end 

      context 'when ask for only 1 attr' do 
        context 'when image doesnt exist' do 
          setup do 
            @query_string = <<-GRAPHQL
              query {
                image(id: #{@image1.id + 1}) {
                  name
                }
              }
            GRAPHQL
          end

          should 'return nil' do 
            response = perform(@query_string, @context)
            assert_nil response["data"]["image"]
          end
        end

        context 'when image exists' do
          setup do 
            @query_string = <<-GRAPHQL
              query {
                image(id: #{@image1.id}) {
                  name
                }
              }
            GRAPHQL
          end

          should 'return only 1 attr for specific image' do 
            response = perform(@query_string, @context)
            assert_equal response["data"]["image"]["name"], @image1.name
            assert_nil response["data"]["image"]["id"]
          end
        end 
      end 
    end
  end

  context 'when current user has admin permissions' do
    setup do 
      @current_user = create(:user_with_role_admin)
      @context = { 
        current_user: @current_user,
        current_ability: Ability.new(@current_user)
      }
    end 

    context 'when there arent images' do 
      setup do 
        @query_string = <<-GRAPHQL
          query {
            image(id: 1) {
              id
              name
              size
              url
            }
          }
        GRAPHQL
      end 

      should 'return nil' do 
        response = perform(@query_string, @context)
        assert_nil response["data"]["image"]
      end
    end


    context 'when there are images' do
      setup do 
        @image1 = create(:image)
      end  

      context 'when ask for multiple attrs' do 
        context 'when image doesnt exist' do 
          setup do 
            @query_string = <<-GRAPHQL
              query {
                image(id: #{@image1.id + 1}) {
                  id
                  name
                  size
                  url
                }
              }
            GRAPHQL
          end

          should 'return nil' do 
            response = perform(@query_string, @context)
            assert_nil response["data"]["image"]
          end
        end

        context 'when image exists' do
          setup do 
            @query_string = <<-GRAPHQL
              query {
                image(id: #{@image1.id}) {
                  id
                  name
                  size
                  url
                }
              }
            GRAPHQL
          end

          context 'when isnt related to user' do 
            should 'return "unauthorized" message' do 
              response = perform(@query_string, @context)
              assert_nil response['data']['image']
              assert_equal response['errors'][0]['message'], 'user unauthorized'
            end
          end 
          
          context 'when is related to user' do 
            setup do 
              create(:user_agency, agency: @image1.imageable.agency, user: @current_user)
              @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
            end

            should 'return only attrs by image' do 
              response = perform(@query_string, @context)
              assert_equal response["data"]["image"]["id"].to_i, @image1.id
              assert_equal response["data"]["image"]["name"], @image1.name
              assert_equal response["data"]["image"]["size"], @image1.size
              assert_equal response["data"]["image"]["url"], @image1.url
            end
          end
        end 
      end 

      context 'when ask for only 1 attr' do 
        context 'when image doesnt exist' do 
          setup do 
            @query_string = <<-GRAPHQL
              query {
                image(id: #{@image1.id + 1}) {
                  name
                }
              }
            GRAPHQL
          end

          should 'return nil' do 
            response = perform(@query_string, @context)
            assert_nil response["data"]["image"]
          end
        end

        context 'when image exists' do
          setup do 
            @query_string = <<-GRAPHQL
              query {
                image(id: #{@image1.id}) {
                  name
                }
              }
            GRAPHQL
          end

          context 'when isnt related to user' do 
            should 'return "unauthorized" message' do 
              response = perform(@query_string, @context)
              assert_nil response['data']['image']
              assert_equal response['errors'][0]['message'], 'user unauthorized'
            end
          end 
          
          context 'when is related to user' do 
            setup do 
              create(:user_agency, agency: @image1.imageable.agency, user: @current_user)
              @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
            end

            should 'return only 1 attr for specific image' do 
              response = perform(@query_string, @context)
              assert_equal response["data"]["image"]["name"], @image1.name
              assert_nil response["data"]["image"]["id"]
            end
          end
        end 
      end 
    end
  end
end
