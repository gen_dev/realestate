require 'test_helper'

class Queries::ImagesTest < ActiveSupport::TestCase
  def perform(query, context = {})
    BackendSchema.execute(query, context: context).to_h
  end

  context 'when current user has gendev permissions' do
    setup do 
      @current_user = create(:user_with_role_gendev)
      @context = { 
        current_user: @current_user,
        current_ability: Ability.new(@current_user)
      }
    end 

    context 'when ask for multiples attrs' do 
      setup do 
        @query_string = <<-GRAPHQL
          query {
            images {
              id
              name
              size
              url
            }
          }
        GRAPHQL
      end 

      context 'when there arent images' do 
        should 'return empty array' do 
          response = perform(@query_string, @context)
          assert_empty response["data"]["images"]
        end
      end
      
      context 'when there are images' do
        setup do 
          @image1 = create(:image)
          @image2 = create(:image)
        end
        
        should 'return multiple images' do 
          response = perform(@query_string, @context)
          assert response["data"]["images"]
          assert_operator response["data"]["images"].length, :>, 1
          assert_equal response["data"]["images"].length, Image.all.length
        end 

        should 'return attrs by image' do 
          response = perform(@query_string, @context)
          images = response["data"]["images"].sort_by { |x| x["id"].to_i }

          assert_equal images[0]["id"].to_i, @image1.id
          assert_equal images[0]["name"], @image1.name
          assert_equal images[0]["size"], @image1.size
          assert_equal images[0]["url"], @image1.url

          assert_equal images[1]["id"].to_i, @image2.id
          assert_equal images[1]["name"], @image2.name
          assert_equal images[1]["size"], @image2.size
          assert_equal images[1]["url"], @image2.url
        end
      end  
    end 

    context 'when ask for only 1 attr' do 
      setup do 
        @query_string = <<-GRAPHQL
          query {
            images {
              name
            }
          }
        GRAPHQL
      end
      
      context 'when there arent images' do 
        should 'return empty array' do 
          response = perform(@query_string, @context)
          assert_empty response["data"]["images"]
        end
      end
      
      context 'when there are images' do
        setup do 
          create(:image)
          create(:image)
        end

        should 'return only 1 attr by image' do 
          response = perform(@query_string, @context)
          assert response["data"]["images"][0]["name"]
          assert_nil response["data"]["images"][0]["id"]
          assert response["data"]["images"][1]["name"]
          assert_nil response["data"]["images"][1]["id"]
        end
      end
    end 

    context 'when use filters' do 
      setup do 
        @name = "Imagen123"
        @query_string = <<-GRAPHQL
          query {
            images(filters: { name: "#{@name}" } ) {
              id
              name
              size
              url
            }
          }
        GRAPHQL
      end 

      context 'when images dont match filter' do 
        setup do 
          create(:image, name: 'Image45')
        end 

        should 'return empty array' do 
          response = perform(@query_string, @context)
          assert_empty response["data"]["images"]
        end
      end

      context 'when there are images matching filter' do 
        setup do 
          create(:image, name: @name)
          create(:image, name: 'Image45')
          create(:image, name: 'Image45')
        end 
        
        should 'return only correct images' do 
          response = perform(@query_string, @context)
          assert response["data"]["images"]
          assert_equal response["data"]["images"].length, 1
          assert response["data"]["images"][0]["id"]
          assert_equal response["data"]["images"][0]["name"], @name
          assert response["data"]["images"][0]["size"]
          assert response["data"]["images"][0]["url"]
        end
      end
    end
  end

  context 'when current user has admin permissions' do
    setup do 
      @current_user = create(:user_with_role_admin)
      @context = { 
        current_user: @current_user,
        current_ability: Ability.new(@current_user)
      }
    end 

    context 'when ask for multiples attrs' do 
      setup do 
        @query_string = <<-GRAPHQL
          query {
            images {
              id
              name
              size
              url
            }
          }
        GRAPHQL
      end 

      context 'when there arent images' do 
        should 'return empty array' do 
          response = perform(@query_string, @context)
          assert_empty response["data"]["images"]
        end
      end
      
      context 'when there are images' do
        setup do 
          @image1 = create(:image)
          @image2 = create(:image)
        end
        
        context 'when there arent images related to user' do 
          should 'return empty array' do 
            response = perform(@query_string, @context)
            assert_empty response["data"]["images"]
          end
        end 
      
        context 'when there are images related to user' do 
          setup do 
            create(:user_agency, agency: @image1.imageable.agency, user: @current_user)
            @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
          end 
        
          should 'return only related images' do 
            response = perform(@query_string, @context)
            assert response["data"]["images"]
            assert_equal response["data"]["images"].length, 1
          end
        end 
      
        context 'when all images are related to user' do 
          setup do 
            create(:user_agency, agency: @image1.imageable.agency, user: @current_user)
            create(:user_agency, agency: @image2.imageable.agency, user: @current_user)
            @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
          end 
      
          should 'return multiple images' do 
            response = perform(@query_string, @context)
            assert response["data"]["images"]
            assert_operator response["data"]["images"].length, :>, 1
            assert_equal response["data"]["images"].length, Image.all.length
          end 

          should 'return attrs by image' do 
            response = perform(@query_string, @context)
            images = response["data"]["images"].sort_by { |x| x["id"].to_i }

            assert_equal images[0]["id"].to_i, @image1.id
            assert_equal images[0]["name"], @image1.name
            assert_equal images[0]["size"], @image1.size
            assert_equal images[0]["url"], @image1.url

            assert_equal images[1]["id"].to_i, @image2.id
            assert_equal images[1]["name"], @image2.name
            assert_equal images[1]["size"], @image2.size
            assert_equal images[1]["url"], @image2.url
          end
        end
      end  
    end 

    context 'when ask for only 1 attr' do 
      setup do 
        @query_string = <<-GRAPHQL
          query {
            images {
              name
            }
          }
        GRAPHQL
      end
      
      context 'when there arent images' do 
        should 'return empty array' do 
          response = perform(@query_string, @context)
          assert_empty response["data"]["images"]
        end
      end
      
      context 'when there are images' do
        setup do 
          @image1 = create(:image)
          @image2 = create(:image)
        end

        context 'when there are images related to user' do 
          setup do 
            create(:user_agency, agency: @image1.imageable.agency, user: @current_user)
            @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
          end 
        
          should 'return only 1 attr by each related agency' do 
            response = perform(@query_string, @context)
            assert response["data"]["images"]
            assert_equal response["data"]["images"].length, 1
            assert response["data"]["images"][0]["name"]
            assert_nil response["data"]["images"][0]["id"]
          end
        end 
  
        context 'when all images are related to user' do 
          setup do 
            create(:user_agency, agency: @image1.imageable.agency, user: @current_user)
            create(:user_agency, agency: @image2.imageable.agency, user: @current_user)
            @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
          end 

          should 'return only 1 attr by image' do 
            response = perform(@query_string, @context)
            assert response["data"]["images"][0]["name"]
            assert_nil response["data"]["images"][0]["id"]
            assert response["data"]["images"][1]["name"]
            assert_nil response["data"]["images"][1]["id"]
          end
        end
      end
    end 

    context 'when use filters' do 
      setup do 
        @name = "Imagen666"
        @query_string = <<-GRAPHQL
          query {
            images(filters: { name: "#{@name}" } ) {
              id
              name
              size
              url
            }
          }
        GRAPHQL
      end 

      context 'when images dont match filter' do 
        setup do 
          create(:image, name: 'Imagen234')
        end 

        should 'return empty array' do 
          response = perform(@query_string, @context)
          assert_empty response["data"]["images"]
        end
      end

      context 'when there are images matching filter' do 
        setup do 
          @image1 = create(:image, name: @name)
          @image2 = create(:image, name: 'Imagen234')
          @image3 = create(:image, name: @name)
        end 
        
        context 'when there arent related to user' do 
          should 'return empty array' do 
            response = perform(@query_string, @context)
            assert_empty response["data"]["images"]
          end
        end 

        context 'when there are images related to user' do 
          setup do 
            create(:user_agency, agency: @image1.imageable.agency, user: @current_user)
            @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
          end 
        
          should 'return only related and filtered images' do 
            response = perform(@query_string, @context)
            assert response["data"]["images"]
            assert_equal response["data"]["images"].length, 1
            assert response["data"]["images"][0]["id"]
            assert_equal response["data"]["images"][0]["name"], @name
            assert response["data"]["images"][0]["size"]
            assert response["data"]["images"][0]["url"]
          end
        end
      end
    end
  end
end
