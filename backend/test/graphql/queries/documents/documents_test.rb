require 'test_helper'

class Queries::DocumentsTest < ActiveSupport::TestCase
  def perform(query, context = {})
    BackendSchema.execute(query, context: context).to_h
  end

  context 'when current user has gendev permissions' do
    setup do 
      @current_user = create(:user_with_role_gendev)
      @context = { 
        current_user: @current_user,
        current_ability: Ability.new(@current_user)
      }
    end 

    context 'when ask for multiples attrs' do 
      setup do 
        @query_string = <<-GRAPHQL
          query {
            documents {
              id
              name
              size
              url
            }
          }
        GRAPHQL
      end 

      context 'when there arent documents' do 
        should 'return empty array' do 
          response = perform(@query_string, @context)
          assert_empty response["data"]["documents"]
        end
      end
      
      context 'when there are documents' do
        setup do 
          @document1 = create(:document)
          @document2 = create(:document)
        end
        
        should 'return multiple documents' do 
          response = perform(@query_string, @context)
          assert response["data"]["documents"]
          assert_operator response["data"]["documents"].length, :>, 1
          assert_equal response["data"]["documents"].length, Document.all.length
        end 

        should 'return attrs by document' do 
          response = perform(@query_string, @context)
          documents = response["data"]["documents"].sort_by { |x| x["id"].to_i }

          assert_equal documents[0]["id"].to_i, @document1.id
          assert_equal documents[0]["name"], @document1.name
          assert_equal documents[0]["size"], @document1.size
          assert_equal documents[0]["url"], @document1.url

          assert_equal documents[1]["id"].to_i, @document2.id
          assert_equal documents[1]["name"], @document2.name
          assert_equal documents[1]["size"], @document2.size
          assert_equal documents[1]["url"], @document2.url
        end
      end  
    end 

    context 'when ask for only 1 attr' do 
      setup do 
        @query_string = <<-GRAPHQL
          query {
            documents {
              name
            }
          }
        GRAPHQL
      end
      
      context 'when there arent documents' do 
        should 'return empty array' do 
          response = perform(@query_string, @context)
          assert_empty response["data"]["documents"]
        end
      end
      
      context 'when there are documents' do
        setup do 
          create(:document)
          create(:document)
        end

        should 'return only 1 attr by document' do 
          response = perform(@query_string, @context)
          assert response["data"]["documents"][0]["name"]
          assert_nil response["data"]["documents"][0]["id"]
          assert response["data"]["documents"][1]["name"]
          assert_nil response["data"]["documents"][1]["id"]
        end
      end
    end 

    context 'when use filters' do 
      setup do 
        @name = "Documentn123"
        @query_string = <<-GRAPHQL
          query {
            documents(filters: { name: "#{@name}" } ) {
              id
              name
              size
              url
            }
          }
        GRAPHQL
      end 

      context 'when documents dont match filter' do 
        setup do 
          create(:document, name: 'Document45')
        end 

        should 'return empty array' do 
          response = perform(@query_string, @context)
          assert_empty response["data"]["documents"]
        end
      end

      context 'when there are documents matching filter' do 
        setup do 
          create(:document, name: @name)
          create(:document, name: 'Document45')
          create(:document, name: 'Document45')
        end 
        
        should 'return only correct documents' do 
          response = perform(@query_string, @context)
          assert response["data"]["documents"]
          assert_equal response["data"]["documents"].length, 1
          assert response["data"]["documents"][0]["id"]
          assert_equal response["data"]["documents"][0]["name"], @name
          assert response["data"]["documents"][0]["size"]
          assert response["data"]["documents"][0]["url"]
        end
      end
    end
  end

  context 'when current user has admin permissions' do
    setup do 
      @current_user = create(:user_with_role_admin)
      @context = { 
        current_user: @current_user,
        current_ability: Ability.new(@current_user)
      }
    end 

    context 'when ask for multiples attrs' do 
      setup do 
        @query_string = <<-GRAPHQL
          query {
            documents {
              id
              name
              size
              url
            }
          }
        GRAPHQL
      end 

      context 'when there arent documents' do 
        should 'return empty array' do 
          response = perform(@query_string, @context)
          assert_empty response["data"]["documents"]
        end
      end
      
      context 'when there are documents' do
        setup do 
          @document1 = create(:document)
          @document2 = create(:document)
        end
        
        context 'when there arent documents related to user' do 
          should 'return empty array' do 
            response = perform(@query_string, @context)
            assert_empty response["data"]["documents"]
          end
        end 
      
        context 'when there are documents related to user' do 
          setup do 
            create(:user_agency, agency: @document1.documentable.property.agency, user: @current_user)
            @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
          end 
        
          should 'return only related documents' do 
            response = perform(@query_string, @context)
            assert response["data"]["documents"]
            assert_equal response["data"]["documents"].length, 1
          end
        end 
      
        context 'when all documents are related to user' do 
          setup do 
            create(:user_agency, agency: @document1.documentable.property.agency, user: @current_user)
            create(:user_agency, agency: @document2.documentable.property.agency, user: @current_user)
            @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
          end 
      
          should 'return multiple documents' do 
            response = perform(@query_string, @context)
            assert response["data"]["documents"]
            assert_operator response["data"]["documents"].length, :>, 1
            assert_equal response["data"]["documents"].length, Document.all.length
          end 

          should 'return attrs by document' do 
            response = perform(@query_string, @context)
            documents = response["data"]["documents"].sort_by { |x| x["id"].to_i }

            assert_equal documents[0]["id"].to_i, @document1.id
            assert_equal documents[0]["name"], @document1.name
            assert_equal documents[0]["size"], @document1.size
            assert_equal documents[0]["url"], @document1.url

            assert_equal documents[1]["id"].to_i, @document2.id
            assert_equal documents[1]["name"], @document2.name
            assert_equal documents[1]["size"], @document2.size
            assert_equal documents[1]["url"], @document2.url
          end
        end
      end  
    end 

    context 'when ask for only 1 attr' do 
      setup do 
        @query_string = <<-GRAPHQL
          query {
            documents {
              name
            }
          }
        GRAPHQL
      end
      
      context 'when there arent documents' do 
        should 'return empty array' do 
          response = perform(@query_string, @context)
          assert_empty response["data"]["documents"]
        end
      end
      
      context 'when there are documents' do
        setup do 
          @document1 = create(:document)
          @document2 = create(:document)
        end

        context 'when there are documents related to user' do 
          setup do 
            create(:user_agency, agency: @document1.documentable.property.agency, user: @current_user)
            @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
          end 
        
          should 'return only 1 attr by each related agency' do 
            response = perform(@query_string, @context)
            assert response["data"]["documents"]
            assert_equal response["data"]["documents"].length, 1
            assert response["data"]["documents"][0]["name"]
            assert_nil response["data"]["documents"][0]["id"]
          end
        end 
  
        context 'when all documents are related to user' do 
          setup do 
            create(:user_agency, agency: @document1.documentable.property.agency, user: @current_user)
            create(:user_agency, agency: @document2.documentable.property.agency, user: @current_user)
            @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
          end 

          should 'return only 1 attr by document' do 
            response = perform(@query_string, @context)
            assert response["data"]["documents"][0]["name"]
            assert_nil response["data"]["documents"][0]["id"]
            assert response["data"]["documents"][1]["name"]
            assert_nil response["data"]["documents"][1]["id"]
          end
        end
      end
    end 

    context 'when use filters' do 
      setup do 
        @name = "Documentn666"
        @query_string = <<-GRAPHQL
          query {
            documents(filters: { name: "#{@name}" } ) {
              id
              name
              size
              url
            }
          }
        GRAPHQL
      end 

      context 'when documents dont match filter' do 
        setup do 
          create(:document, name: 'Documentn234')
        end 

        should 'return empty array' do 
          response = perform(@query_string, @context)
          assert_empty response["data"]["documents"]
        end
      end

      context 'when there are documents matching filter' do 
        setup do 
          @document1 = create(:document, name: @name)
          @document2 = create(:document, name: 'Documentn234')
          @document3 = create(:document, name: @name)
        end 
        
        context 'when there arent related to user' do 
          should 'return empty array' do 
            response = perform(@query_string, @context)
            assert_empty response["data"]["documents"]
          end
        end 

        context 'when there are documents related to user' do 
          setup do 
            create(:user_agency, agency: @document1.documentable.property.agency, user: @current_user)
            @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
          end 
        
          should 'return only related and filtered documents' do 
            response = perform(@query_string, @context)
            assert response["data"]["documents"]
            assert_equal response["data"]["documents"].length, 1
            assert response["data"]["documents"][0]["id"]
            assert_equal response["data"]["documents"][0]["name"], @name
            assert response["data"]["documents"][0]["size"]
            assert response["data"]["documents"][0]["url"]
          end
        end
      end
    end
  end
end
