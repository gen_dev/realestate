require 'test_helper'

class Queries::DocumentTest < ActiveSupport::TestCase
  def perform(query, context = {})
    BackendSchema.execute(query, context: context).to_h
  end

  context 'when current user has gendev permissions' do
    setup do 
      @current_user = create(:user_with_role_gendev)
      @context = { 
        current_user: @current_user,
        current_ability: Ability.new(@current_user)
      }
    end 

    context 'when there arent documents' do 
      setup do 
        @query_string = <<-GRAPHQL
          query {
            document(id: 1) {
              id
              name
              size
              url
            }
          }
        GRAPHQL
      end 

      should 'return nil' do 
        response = perform(@query_string, @context)
        assert_nil response["data"]["document"]
      end
    end


    context 'when there are documents' do
      setup do 
        @document1 = create(:document)
      end  

      context 'when ask for multiple attrs' do 
        context 'when document doesnt exist' do 
          setup do 
            @query_string = <<-GRAPHQL
              query {
                document(id: #{@document1.id + 1}) {
                  id
                  name
                  size
                  url
                }
              }
            GRAPHQL
          end

          should 'return nil' do 
            response = perform(@query_string, @context)
            assert_nil response["data"]["document"]
          end
        end

        context 'when document exists' do
          setup do 
            @query_string = <<-GRAPHQL
              query {
                document(id: #{@document1.id}) {
                  id
                  name
                  size
                  url
                }
              }
            GRAPHQL
          end

          should 'return only attrs by document' do 
            response = perform(@query_string, @context)
            assert_equal response["data"]["document"]["id"].to_i, @document1.id
            assert_equal response["data"]["document"]["name"], @document1.name
            assert_equal response["data"]["document"]["size"], @document1.size
            assert_equal response["data"]["document"]["url"], @document1.url
          end
        end 
      end 

      context 'when ask for only 1 attr' do 
        context 'when document doesnt exist' do 
          setup do 
            @query_string = <<-GRAPHQL
              query {
                document(id: #{@document1.id + 1}) {
                  name
                }
              }
            GRAPHQL
          end

          should 'return nil' do 
            response = perform(@query_string, @context)
            assert_nil response["data"]["document"]
          end
        end

        context 'when document exists' do
          setup do 
            @query_string = <<-GRAPHQL
              query {
                document(id: #{@document1.id}) {
                  name
                }
              }
            GRAPHQL
          end

          should 'return only 1 attr for specific document' do 
            response = perform(@query_string, @context)
            assert_equal response["data"]["document"]["name"], @document1.name
            assert_nil response["data"]["document"]["id"]
          end
        end 
      end 
    end
  end

  context 'when current user has admin permissions' do
    setup do 
      @current_user = create(:user_with_role_admin)
      @context = { 
        current_user: @current_user,
        current_ability: Ability.new(@current_user)
      }
    end 

    context 'when there arent documents' do 
      setup do 
        @query_string = <<-GRAPHQL
          query {
            document(id: 1) {
              id
              name
              size
              url
            }
          }
        GRAPHQL
      end 

      should 'return nil' do 
        response = perform(@query_string, @context)
        assert_nil response["data"]["document"]
      end
    end


    context 'when there are documents' do
      setup do 
        @document1 = create(:document)
      end  

      context 'when ask for multiple attrs' do 
        context 'when document doesnt exist' do 
          setup do 
            @query_string = <<-GRAPHQL
              query {
                document(id: #{@document1.id + 1}) {
                  id
                  name
                  size
                  url
                }
              }
            GRAPHQL
          end

          should 'return nil' do 
            response = perform(@query_string, @context)
            assert_nil response["data"]["document"]
          end
        end

        context 'when document exists' do
          setup do 
            @query_string = <<-GRAPHQL
              query {
                document(id: #{@document1.id}) {
                  id
                  name
                  size
                  url
                }
              }
            GRAPHQL
          end

          context 'when isnt related to user' do 
            should 'return "unauthorized" message' do 
              response = perform(@query_string, @context)
              assert_nil response['data']['document']
              assert_equal response['errors'][0]['message'], 'user unauthorized'
            end
          end 
          
          context 'when is related to user' do 
            setup do 
              create(:user_agency, agency: @document1.documentable.property.agency, user: @current_user)
              @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
            end

            should 'return only attrs by document' do 
              response = perform(@query_string, @context)
              assert_equal response["data"]["document"]["id"].to_i, @document1.id
              assert_equal response["data"]["document"]["name"], @document1.name
              assert_equal response["data"]["document"]["size"], @document1.size
              assert_equal response["data"]["document"]["url"], @document1.url
            end
          end
        end 
      end 

      context 'when ask for only 1 attr' do 
        context 'when document doesnt exist' do 
          setup do 
            @query_string = <<-GRAPHQL
              query {
                document(id: #{@document1.id + 1}) {
                  name
                }
              }
            GRAPHQL
          end

          should 'return nil' do 
            response = perform(@query_string, @context)
            assert_nil response["data"]["document"]
          end
        end

        context 'when document exists' do
          setup do 
            @query_string = <<-GRAPHQL
              query {
                document(id: #{@document1.id}) {
                  name
                }
              }
            GRAPHQL
          end

          context 'when isnt related to user' do 
            should 'return "unauthorized" message' do 
              response = perform(@query_string, @context)
              assert_nil response['data']['document']
              assert_equal response['errors'][0]['message'], 'user unauthorized'
            end
          end 
          
          context 'when is related to user' do 
            setup do 
              create(:user_agency, agency: @document1.documentable.property.agency, user: @current_user)
              @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
            end

            should 'return only 1 attr for specific document' do 
              response = perform(@query_string, @context)
              assert_equal response["data"]["document"]["name"], @document1.name
              assert_nil response["data"]["document"]["id"]
            end
          end
        end 
      end 
    end
  end
end
