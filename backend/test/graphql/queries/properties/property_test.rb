require 'test_helper'

class Queries::PropertyTest < ActiveSupport::TestCase
  def perform(query, context = {})
    BackendSchema.execute(query, context: context).to_h
  end

  context 'when current user has gendev permissions' do
    setup do 
      @current_user = create(:user_with_role_gendev)
      @context = { 
        current_user: @current_user,
        current_ability: Ability.new(@current_user)
      }
    end 

    context 'when there arent properties' do 
      setup do 
        @query_string = <<-GRAPHQL
          query {
            property(id: 1) {
              id
              propertyType
              state
              description
            }
          }
        GRAPHQL
      end 

      should 'return nil' do 
        response = perform(@query_string, @context)
        assert_nil response["data"]["property"]
      end
    end


    context 'when there are properties' do
      setup do 
        @property1 = create(:property)
      end  

      context 'when ask for multiple attrs' do 
        context 'when property doesnt exist' do 
          setup do 
            @query_string = <<-GRAPHQL
              query {
                property(id: #{@property1.id + 1}) {
                  id
                  propertyType
                  state
                  description
                  price
                }
              }
            GRAPHQL
          end

          should 'return nil' do 
            response = perform(@query_string, @context)
            assert_nil response["data"]["property"]
          end
        end

        context 'when property exists' do
          setup do 
            @query_string = <<-GRAPHQL
              query {
                property(id: #{@property1.id}) {
                  id
                  propertyType
                  state
                  description
                  price
                }
              }
            GRAPHQL
          end

          should 'return only attrs by property' do 
            response = perform(@query_string, @context)
            assert_equal response["data"]["property"]["id"].to_i, @property1.id
            assert_equal response["data"]["property"]["state"], @property1.state
            assert_equal response["data"]["property"]["propertyType"], @property1.property_type
            assert_equal response["data"]["property"]["description"], @property1.description
            assert_equal response["data"]["property"]["price"], @property1.price
          end
        end 
      end 

      context 'when ask for only 1 attr' do 
        context 'when property doesnt exist' do 
          setup do 
            @query_string = <<-GRAPHQL
              query {
                property(id: #{@property1.id + 1}) {
                  propertyType
                }
              }
            GRAPHQL
          end

          should 'return nil' do 
            response = perform(@query_string, @context)
            assert_nil response["data"]["property"]
          end
        end

        context 'when property exists' do
          setup do 
            @query_string = <<-GRAPHQL
              query {
                property(id: #{@property1.id}) {
                  propertyType
                }
              }
            GRAPHQL
          end

          should 'return only 1 attr for specific property' do 
            response = perform(@query_string, @context)
            assert_equal response["data"]["property"]["propertyType"], @property1.property_type
            assert_nil response["data"]["property"]["id"]
          end
        end 
      end 
    end
  end

  context 'when current user has admin permissions' do
    setup do 
      @current_user = create(:user_with_role_admin)
      @context = { 
        current_user: @current_user,
        current_ability: Ability.new(@current_user)
      }
    end 

    context 'when there arent properties' do 
      setup do 
        @query_string = <<-GRAPHQL
          query {
            property(id: 1) {
              id
              propertyType
              state
              description
            }
          }
        GRAPHQL
      end 

      should 'return nil' do 
        response = perform(@query_string, @context)
        assert_nil response["data"]["property"]
      end
    end


    context 'when there are properties' do
      setup do 
        @property1 = create(:property)
      end  

      context 'when ask for multiple attrs' do 
        context 'when property doesnt exist' do 
          setup do 
            @query_string = <<-GRAPHQL
              query {
                property(id: #{@property1.id + 1}) {
                  id
                  propertyType
                  state
                  description
                  price
                }
              }
            GRAPHQL
          end

          should 'return nil' do 
            response = perform(@query_string, @context)
            assert_nil response["data"]["property"]
          end
        end

        context 'when property exists' do
          setup do 
            @query_string = <<-GRAPHQL
              query {
                property(id: #{@property1.id}) {
                  id
                  propertyType
                  state
                  description
                  price
                }
              }
            GRAPHQL
          end

          context 'when isnt related to user' do 
            should 'return "unauthorized" message' do 
              response = perform(@query_string, @context)
              assert_nil response['data']['property']
              assert_equal response['errors'][0]['message'], 'user unauthorized'
            end
          end 
          
          context 'when is related to user' do 
            setup do 
              create(:user_agency, agency: @property1.agency, user: @current_user)
              @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
            end

            should 'return only attrs by property' do 
              response = perform(@query_string, @context)
              assert_equal response["data"]["property"]["id"].to_i, @property1.id
              assert_equal response["data"]["property"]["state"], @property1.state
              assert_equal response["data"]["property"]["propertyType"], @property1.property_type
              assert_equal response["data"]["property"]["description"], @property1.description
              assert_equal response["data"]["property"]["price"], @property1.price
            end
          end
        end 
      end 

      context 'when ask for only 1 attr' do 
        context 'when property doesnt exist' do 
          setup do 
            @query_string = <<-GRAPHQL
              query {
                property(id: #{@property1.id + 1}) {
                  propertyType
                }
              }
            GRAPHQL
          end

          should 'return nil' do 
            response = perform(@query_string, @context)
            assert_nil response["data"]["property"]
          end
        end

        context 'when property exists' do
          setup do 
            @query_string = <<-GRAPHQL
              query {
                property(id: #{@property1.id}) {
                  propertyType
                }
              }
            GRAPHQL
          end

          context 'when isnt related to user' do 
            should 'return "unauthorized" message' do 
              response = perform(@query_string, @context)
              assert_nil response['data']['property']
              assert_equal response['errors'][0]['message'], 'user unauthorized'
            end
          end 
          
          context 'when is related to user' do 
            setup do 
              create(:user_agency, agency: @property1.agency, user: @current_user)
              @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
            end

            should 'return only 1 attr for specific property' do 
              response = perform(@query_string, @context)
              assert_equal response["data"]["property"]["propertyType"], @property1.property_type
              assert_nil response["data"]["property"]["id"]
            end
          end
        end 
      end 
    end
  end
end
