require 'test_helper'

class Queries::PropertiesTest < ActiveSupport::TestCase
  def perform(query, context = {})
    BackendSchema.execute(query, context: context).to_h
  end

  context 'when current user has gendev permissions' do
    setup do 
      @current_user = create(:user_with_role_gendev)
      @agency = create(:user_agency, user: @current_user).agency
      @context = { 
        current_user: @current_user,
        current_ability: Ability.new(@current_user)
      }
    end 

    context 'when ask for multiples attrs' do 
      setup do 
        @query_string = <<-GRAPHQL
          query {
            properties {
              properties {
                id
                propertyType
                state
                description
                price
              }
              pages
            }
          }
        GRAPHQL
      end 

      context 'when there arent properties' do 
        should 'return empty array' do 
          response = perform(@query_string, @context)
          assert_empty response["data"]["properties"]["properties"]
        end
      end
      
      context 'when there are properties' do
        setup do 
          @property1 = create(:property, agency: @agency)
          @property2 = create(:property, agency: @agency)
        end
        
        should 'return multiple properties' do 
          response = perform(@query_string, @context)
          assert response["data"]["properties"]["properties"]
          assert_operator response["data"]["properties"]["properties"].length, :>, 1
          assert_equal response["data"]["properties"]["properties"].length, Property.all.length
        end 

        should 'return attrs by property' do 
          response = perform(@query_string, @context)

          properties = response["data"]["properties"]["properties"].sort_by { |x| x["id"].to_i }

          assert_equal properties[0]["id"].to_i, @property1.id
          assert_equal properties[0]["propertyType"], @property1.property_type
          assert_equal properties[0]["state"], @property1.state
          assert_equal properties[0]["description"], @property1.description
          assert_equal properties[0]["price"], @property1.price
          assert_equal properties[1]["id"].to_i, @property2.id
          assert_equal properties[1]["propertyType"], @property2.property_type
          assert_equal properties[1]["state"], @property2.state
          assert_equal properties[1]["description"], @property2.description
          assert_equal properties[1]["price"], @property2.price
        end
      end  
    end 

    context 'when ask for only 1 attr' do 
      setup do 
        @query_string = <<-GRAPHQL
          query {
            properties {
              properties {
                propertyType
              }
              pages
            }
          }
        GRAPHQL
      end
      
      context 'when there arent properties' do 
        should 'return empty array' do 
          response = perform(@query_string, @context)
          assert_empty response["data"]["properties"]["properties"]
        end
      end
      
      context 'when there are properties' do
        setup do 
          create(:property, agency: @agency)
          create(:property, agency: @agency)
        end

        should 'return only 1 attr by property' do 
          response = perform(@query_string, @context)
          assert response["data"]["properties"]["properties"][0]["propertyType"]
          assert_nil response["data"]["properties"]["properties"][0]["id"]
          assert response["data"]["properties"]["properties"][1]["propertyType"]
          assert_nil response["data"]["properties"]["properties"][1]["id"]
        end
      end
    end 

    context 'when use filters' do 
      setup do 
        @state = "Rented"
        @query_string = <<-GRAPHQL
          query {
            properties(filters: { state: "#{@state}" } ) {
              properties {
                id
                propertyType
                state
                description
                price
              }
              pages
            }
          }
        GRAPHQL
      end 

      context 'when properties dont match filter' do 
        setup do 
          create(:property, state: 'For sale', agency: @agency)
        end 

        should 'return empty array' do 
          response = perform(@query_string, @context)
          assert_empty response["data"]["properties"]["properties"]
        end
      end

      context 'when there are properties matching filter' do 
        setup do 
          create(:property, state: @state, agency: @agency)
          create(:property, state: 'For sale', agency: @agency)
          create(:property, state: 'For sale', agency: @agency)
        end 
        
        should 'return only correct properties' do 
          response = perform(@query_string, @context)
          assert response["data"]["properties"]["properties"]
          assert_equal response["data"]["properties"]["properties"].length, 1
          assert response["data"]["properties"]["properties"][0]["id"]
          assert response["data"]["properties"]["properties"][0]["propertyType"]
          assert_equal response["data"]["properties"]["properties"][0]["state"], @state
          assert response["data"]["properties"]["properties"][0]["description"]
          assert response["data"]["properties"]["properties"][0]["price"]
        end
      end
    end
  end

  context 'when current user has admin permissions' do
    setup do 
      @current_user = create(:user_with_role_admin)
      @context = { 
        current_user: @current_user,
        current_ability: Ability.new(@current_user)
      }
    end 

    context 'when ask for multiples attrs' do 
      setup do 
        @query_string = <<-GRAPHQL
          query {
            properties {
              properties {
                id
                propertyType
                state
                description
                price
              }
              pages
            }
          }
        GRAPHQL
      end 

      context 'when there arent properties' do 
        should 'return empty array' do 
          response = perform(@query_string, @context)
          assert_empty response["data"]["properties"]["properties"]
        end
      end
      
      context 'when there are properties' do
        setup do 
          @property1 = create(:property)
          @property2 = create(:property)
        end
        
        context 'when there arent properties related to user' do 
          should 'return empty array' do 
            response = perform(@query_string, @context)
            assert_empty response["data"]["properties"]["properties"]
          end
        end 
      
        context 'when there are properties related to user' do 
          setup do 
            create(:user_agency, agency: @property1.agency, user: @current_user)
            @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
          end 
        
          should 'return only related properties' do 
            response = perform(@query_string, @context)
            assert response["data"]["properties"]["properties"]
            assert_equal response["data"]["properties"]["properties"].length, 1
          end
        end 
      
        context 'when all properties are related to user' do 
          setup do 
            create(:user_agency, agency: @property1.agency, user: @current_user)
            create(:user_agency, agency: @property2.agency, user: @current_user)
            @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
          end 
      
          should 'return multiple properties' do 
            response = perform(@query_string, @context)
            assert response["data"]["properties"]["properties"]
            assert_operator response["data"]["properties"]["properties"].length, :>, 1
            assert_equal response["data"]["properties"]["properties"].length, Property.all.length
          end 

          should 'return attrs by property' do 
            response = perform(@query_string, @context)
            properties = response["data"]["properties"]["properties"].sort_by { |x| x["id"].to_i }

            assert_equal properties[0]["id"].to_i, @property1.id
            assert_equal properties[0]["propertyType"], @property1.property_type
            assert_equal properties[0]["state"], @property1.state
            assert_equal properties[0]["description"], @property1.description
            assert_equal properties[0]["price"], @property1.price

            assert_equal properties[1]["id"].to_i, @property2.id
            assert_equal properties[1]["propertyType"], @property2.property_type
            assert_equal properties[1]["state"], @property2.state
            assert_equal properties[1]["description"], @property2.description
            assert_equal properties[1]["price"], @property2.price
          end
        end
      end  
    end 

    context 'when ask for only 1 attr' do 
      setup do 
        @query_string = <<-GRAPHQL
          query {
            properties {
              properties {
                propertyType
              }
              pages
            }
          }
        GRAPHQL
      end
      
      context 'when there arent properties' do 
        should 'return empty array' do 
          response = perform(@query_string, @context)
          assert_empty response["data"]["properties"]["properties"]
        end
      end
      
      context 'when there are properties' do
        setup do 
          @property1 = create(:property)
          @property2 = create(:property)
        end

        context 'when there are properties related to user' do 
          setup do 
            create(:user_agency, agency: @property1.agency, user: @current_user)
            @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
          end 
        
          should 'return only 1 attr by each related agency' do 
            response = perform(@query_string, @context)
            assert response["data"]["properties"]["properties"]
            assert_equal response["data"]["properties"]["properties"].length, 1
            assert response["data"]["properties"]["properties"][0]["propertyType"]
            assert_nil response["data"]["properties"]["properties"][0]["id"]
          end
        end 
  
        context 'when all properties are related to user' do 
          setup do 
            create(:user_agency, agency: @property1.agency, user: @current_user)
            create(:user_agency, agency: @property2.agency, user: @current_user)
            @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
          end 

          should 'return only 1 attr by property' do 
            response = perform(@query_string, @context)
            assert response["data"]["properties"]["properties"][0]["propertyType"]
            assert_nil response["data"]["properties"]["properties"][0]["id"]
            assert response["data"]["properties"]["properties"][1]["propertyType"]
            assert_nil response["data"]["properties"]["properties"][1]["id"]
          end
        end
      end
    end 

    context 'when use filters' do 
      setup do 
        @state = "Rented"
        @query_string = <<-GRAPHQL
          query {
            properties(filters: { state: "#{@state}" } ) {
              properties {
                id
                propertyType
                state
                description
                price
              }
              pages
            }
          }
        GRAPHQL
      end 

      context 'when properties dont match filter' do 
        setup do 
          create(:property, state: 'For sale')
        end 

        should 'return empty array' do 
          response = perform(@query_string, @context)
          assert_empty response["data"]["properties"]["properties"]
        end
      end

      context 'when there are properties matching filter' do 
        setup do 
          @property1 = create(:property, state: @state)
          @property2 = create(:property, state: 'For sale')
          @property3 = create(:property, state: @state)
        end 
        
        context 'when there arent related to user' do 
          should 'return empty array' do 
            response = perform(@query_string, @context)
            assert_empty response["data"]["properties"]["properties"]
          end
        end 

        context 'when there are properties related to user' do 
          setup do 
            create(:user_agency, agency: @property1.agency, user: @current_user)
            @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
          end 
        
          should 'return only related and filtered properties' do 
            response = perform(@query_string, @context)
            assert response["data"]["properties"]["properties"]
            assert_equal response["data"]["properties"]["properties"].length, 1
            assert response["data"]["properties"]["properties"][0]["id"]
            assert response["data"]["properties"]["properties"][0]["propertyType"]
            assert_equal response["data"]["properties"]["properties"][0]["state"], @state
            assert response["data"]["properties"]["properties"][0]["description"]
            assert response["data"]["properties"]["properties"][0]["price"]
          end
        end
      end
    end
  end
end
