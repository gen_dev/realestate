require 'test_helper'

class Queries::UserTest < ActiveSupport::TestCase
  def perform(query, context = {})
    BackendSchema.execute(query, context: context).to_h
  end

  context 'when current user has gendev permissions' do
    setup do 
      @current_user = create(:user_with_role_gendev)
      @context = { 
        current_user: @current_user,
        current_ability: Ability.new(@current_user)
      }
    end 

    context 'when there are users' do
      setup do 
        @user1 = create(:user)
      end  

      context 'when ask for multiple attrs' do 
        context 'when user doesnt exist' do 
          setup do 
            @query_string = <<-GRAPHQL
              query {
                user(id: #{@user1.id + 1}) {
                  id
                  email
                }
              }
            GRAPHQL
          end

          should 'return nil' do 
            response = perform(@query_string, @context)
            assert_nil response["data"]["user"]
          end
        end

        context 'when user exists' do
          setup do 
            @query_string = <<-GRAPHQL
              query {
                user(id: #{@user1.id}) {
                  id
                  email
                }
              }
            GRAPHQL
          end

          should 'return multiple attrs for specific User' do 
            response = perform(@query_string, @context)
            assert_equal response["data"]["user"]["id"].to_i, @user1.id
            assert_equal response["data"]["user"]["email"], @user1.email
          end
        end 
      end 

      context 'when ask for only 1 attr' do 
        context 'when user doesnt exist' do 
          setup do 
            @query_string = <<-GRAPHQL
              query {
                user(id: #{@user1.id + 1}) {
                  email
                }
              }
            GRAPHQL
          end

          should 'return nil' do 
            response = perform(@query_string, @context)
            assert_nil response["data"]["user"]
          end
        end

        context 'when user exists' do
          setup do 
            @query_string = <<-GRAPHQL
              query {
                user(id: #{@user1.id}) {
                  email
                }
              }
            GRAPHQL
          end

          should 'return only 1 attr for specific User' do 
            response = perform(@query_string, @context)
            assert_equal response["data"]["user"]["email"], @user1.email
            assert_nil response["data"]["user"]["id"]
          end
        end 
      end 
    end
  end

  context 'when current user has admin permissions' do
    setup do 
      @current_user = create(:user_with_role_admin)
      @context = { 
        current_user: @current_user,
        current_ability: Ability.new(@current_user)
      }
    end 

    context 'when there are users' do
      setup do 
        @user1 = create(:user)
      end  

      context 'when ask for multiple attrs' do 
        context 'when user doesnt exist' do 
          setup do 
            @query_string = <<-GRAPHQL
              query {
                user(id: #{@user1.id + 1}) {
                  id
                  email
                }
              }
            GRAPHQL
          end

          should 'return nil' do 
            response = perform(@query_string, @context)
            assert_nil response["data"]["user"]
          end
        end

        context 'when user exists' do
          setup do 
            @query_string = <<-GRAPHQL
              query {
                user(id: #{@user1.id}) {
                  id
                  email
                }
              }
            GRAPHQL
          end

          context 'when isnt related to current user' do 
            should 'return "unauthorized" message' do 
              response = perform(@query_string, @context)
              assert_nil response['data']['user']
              assert_equal response['errors'][0]['message'], 'user unauthorized'
            end
          end 
          
          context 'when is related to current user' do 
            setup do 
              create(:user_agency, agency: create(:user_agency, user: @user1).agency, user: @current_user)
              @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
            end

            should 'return multiple attrs for specific User' do 
              response = perform(@query_string, @context)
              assert_equal response["data"]["user"]["id"].to_i, @user1.id
              assert_equal response["data"]["user"]["email"], @user1.email
            end
          end
        end 
      end 

      context 'when ask for only 1 attr' do 
        context 'when user doesnt exist' do 
          setup do 
            @query_string = <<-GRAPHQL
              query {
                user(id: #{@user1.id + 1}) {
                  email
                }
              }
            GRAPHQL
          end

          should 'return nil' do 
            response = perform(@query_string, @context)
            assert_nil response["data"]["user"]
          end
        end

        context 'when user exists' do
          setup do 
            @query_string = <<-GRAPHQL
              query {
                user(id: #{@user1.id}) {
                  email
                }
              }
            GRAPHQL
          end

          context 'when isnt related to current user' do 
            should 'return "unauthorized" message' do 
              response = perform(@query_string, @context)
              assert_equal response['errors'][0]['message'], 'user unauthorized'
            end
          end 
          
          context 'when is related to current user' do 
            setup do 
              create(:user_agency, agency: create(:user_agency, user: @user1).agency, user: @current_user)
              @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
            end

            should 'return only 1 attr for specific User' do 
              response = perform(@query_string, @context)
              assert_equal response["data"]["user"]["email"], @user1.email
              assert_nil response["data"]["user"]["id"]
            end
          end
        end 
      end 
    end
  end
end