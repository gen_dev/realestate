require 'test_helper'

class Queries::UsersTest < ActiveSupport::TestCase
  def perform(query, context = {})
    BackendSchema.execute(query, context: context).to_h
  end

  context 'when current user has gendev permissions' do
    setup do 
      @current_user = create(:user)
      create(:user_agency, user: @current_user, selected: true)
      @current_user.add_role(:gendev, @current_user.selected_agency)
      @context = { 
        current_user: @current_user,
        current_ability: Ability.new(@current_user)
      }
    end 

    context 'when ask for multiples attrs' do 
      setup do 
        @query_string = <<-GRAPHQL
          query {
            users {
              id
              email
            }
          }
        GRAPHQL
      end 

      context 'when there are users' do
        setup do 
          @user1 = create(:user)
          @user2 = create(:user)
          @user1.add_role(:tenant, @current_user.selected_agency)
          @user2.add_role(:tenant, @current_user.selected_agency)
        end
        
        should 'return multiple users' do 
          response = perform(@query_string, @context)
          assert response["data"]["users"]
          assert_operator response["data"]["users"].length, :>, 1
          assert_equal response["data"]["users"].length, User.all.length
        end 

        should 'return attrs by User' do 
          response = perform(@query_string, @context)
          assert_equal response["data"]["users"][0]["id"].to_i, @current_user.id
          assert_equal response["data"]["users"][0]["email"], @current_user.email
          assert_equal response["data"]["users"][1]["id"].to_i, @user1.id
          assert_equal response["data"]["users"][1]["email"], @user1.email
          assert_equal response["data"]["users"][2]["id"].to_i, @user2.id
          assert_equal response["data"]["users"][2]["email"], @user2.email
        end
      end  
    end 

    context 'when ask for only 1 attr' do 
      setup do 
        @query_string = <<-GRAPHQL
          query {
            users {
              email
            }
          }
        GRAPHQL
      end
      
      context 'when there are users' do
        setup do 
          @user1 = create(:user)
          @user2 = create(:user)
          @user1.add_role(:tenant, @current_user.selected_agency)
          @user2.add_role(:tenant, @current_user.selected_agency)
        end

        should 'return only 1 attr by User' do 
          response = perform(@query_string, @context)
          assert response["data"]["users"][0]["email"]
          assert_nil response["data"]["users"][0]["id"]
          assert_nil response["data"]["users"][1]["id"]
        end
      end
    end 

    context 'when use filters' do 
      setup do 
        @email = 'Jack@test.com'
        @query_string = <<-GRAPHQL
          query {
            users(filters: { email: "#{@email}" } ) {
              id
              email
            }
          }
        GRAPHQL
      end 

      context 'when users dont match filter' do 
        setup do 
          @user1 = create(:user, email: "John@test.com")
          @user1.add_role(:tenant, @current_user.selected_agency)
        end 

        should 'return empty array' do 
          response = perform(@query_string, @context)
          assert_empty response["data"]["users"]
        end
      end
    end 
  end

  context 'when current user has admin permissions' do
    setup do 
      @current_user = create(:user)
      create(:user_agency, user: @current_user, selected: true)
      @current_user.add_role(:admin, @current_user.selected_agency)
      @context = { 
        current_user: @current_user,
        current_ability: Ability.new(@current_user)
      }
    end 

    context 'when ask for multiples attrs' do 
      setup do 
        @query_string = <<-GRAPHQL
          query {
            users {
              id
              email
            }
          }
        GRAPHQL
      end 

      context 'when there are users' do
        context 'when there arent users related to user' do 
          setup do 
            @user1 = create(:user)
            @user2 = create(:user)
          end
          
          should 'return empty array' do 
            @current_user.remove_role :admin
            response = perform(@query_string, @context)
            assert_empty response["data"]["users"]
          end
        end 
      
        context 'when there are users related to user' do 
          setup do 
            @user1 = create(:user)
            @user1.add_role(:tenant, @current_user.selected_agency)
            @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
          end 
        
          should 'return only related users' do 
            response = perform(@query_string, @context)
            assert response["data"]["users"]
            assert_equal response["data"]["users"].length, 2 # 2 because of current_user
          end
        end 
      
        context 'when all users are related to user' do 
          setup do 
            @user1 = create(:user)
            @user2 = create(:user)
            @user1.add_role(:tenant, @current_user.selected_agency)
            @user2.add_role(:tenant, @current_user.selected_agency)
            @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
          end 
        
          should 'return multiple users' do 
            response = perform(@query_string, @context)
            assert response["data"]["users"]
            assert_operator response["data"]["users"].length, :>, 1
            assert_equal response["data"]["users"].length, User.all.length
          end 

          should 'return attrs by User' do 
            response = perform(@query_string, @context)
            users = response["data"]["users"].sort_by { |x| x["id"].to_i }

            assert_equal users[0]["id"].to_i, @current_user.id
            assert_equal users[0]["email"], @current_user.email

            assert_equal users[1]["id"].to_i, @user1.id
            assert_equal users[1]["email"], @user1.email

            assert_equal users[2]["id"].to_i, @user2.id
            assert_equal users[2]["email"], @user2.email
          end
        end
      end  
    end 

    context 'when ask for only 1 attr' do 
      setup do 
        @query_string = <<-GRAPHQL
          query {
            users {
              email
            }
          }
        GRAPHQL
      end
      
      context 'when there are users' do
        setup do 
          @user1 = create(:user)
          @user2 = create(:user)
          @user1.add_role(:tenant, @current_user.selected_agency)
        end

        context 'when there are users related to user' do 
          should 'return only 1 attr by each related agency' do 
            response = perform(@query_string, @context)
            assert response["data"]["users"]
            assert_equal response["data"]["users"].length, 2 # 2 because of current_user
            assert response["data"]["users"][0]["email"]
            assert_nil response["data"]["users"][0]["id"]
          end
        end 
  
        context 'when all users are related to user' do 
          setup do         
            @user2.add_role(:tenant, @current_user.selected_agency)
          end 

          should 'return only 1 attr by User' do 
            response = perform(@query_string, @context)
            assert response["data"]["users"][0]["email"]
            assert_nil response["data"]["users"][0]["id"]

            assert response["data"]["users"][1]["email"]
            assert_nil response["data"]["users"][1]["id"]
          end
        end
      end
    end 

    context 'when use filters' do 
      setup do 
        @email = 'Jack@test.com'
        @query_string = <<-GRAPHQL
          query {
            users(filters: { email: "#{@email}" } ) {
              id
              email
            }
          }
        GRAPHQL
      end 

      context 'when users dont match filter' do 
        setup do 
          create(:user, email: "John@test.com")
        end 

        should 'return empty array' do 
          response = perform(@query_string, @context)
          assert_empty response["data"]["users"]
        end
      end
    end 
  end
end