require 'test_helper'

class Queries::TaxTest < ActiveSupport::TestCase
  def perform(query, context = {})
    BackendSchema.execute(query, context: context).to_h
  end

  context 'when current user has gendev permissions' do
    setup do 
      @current_user = create(:user_with_role_gendev)
      @context = { 
        current_user: @current_user,
        current_ability: Ability.new(@current_user)
      }
    end 

    context 'when there arent taxes' do 
      setup do 
        @query_string = <<-GRAPHQL
          query {
            tax(id: 1) {
              id
              name 
              state
              interval
              lastPaymentDate
              debtor {
                ... on User {
                  id
                  email
                }
                ... on Agency {
                  id
                  name
                }
              }
            }
          }
        GRAPHQL
      end 

      should 'return nil' do 
        response = perform(@query_string, @context)
        assert_nil response["data"]["tax"]
      end
    end


    context 'when there are taxes' do
      setup do 
        @tax1 = create(:tax)
      end  

      context 'when ask for multiple attrs' do 
        context 'when tax doesnt exist' do 
          setup do 
            @query_string = <<-GRAPHQL
              query {
                tax(id: #{@tax1.id + 1}) {
                  id
                  name 
                  state
                  interval
                  lastPaymentDate
                  debtor {
                    ... on User {
                      id
                      email
                    }
                    ... on Agency {
                      id
                      name
                    }
                  }
                }
              }
            GRAPHQL
          end

          should 'return nil' do 
            response = perform(@query_string, @context)
            assert_nil response["data"]["tax"]
          end
        end

        context 'when tax exists' do
          setup do 
            @query_string = <<-GRAPHQL
              query {
                tax(id: #{@tax1.id}) {
                  id
                  name 
                  state
                  interval
                  lastPaymentDate
                  debtor {
                    ... on User {
                      id
                      email
                    }
                    ... on Agency {
                      id
                      name
                    }
                  }
                }
              }
            GRAPHQL
          end

          should 'return attrs by taxes' do 
            response = perform(@query_string, @context)
            assert_equal response["data"]["tax"]["id"].to_i, @tax1.id
            assert_equal response["data"]["tax"]["name"], @tax1.name
            assert_equal response["data"]["tax"]["state"], @tax1.state
            assert_equal response["data"]["tax"]["interval"], @tax1.interval
            assert_equal response["data"]["tax"]["lastPaymentDate"], @tax1.last_payment_date.to_s
            assert_equal response["data"]["tax"]["debtor"]["id"].to_i, @tax1.debtor.id
            assert_equal response["data"]["tax"]["debtor"]["email"], @tax1.debtor.email
          end
        end 
      end 

      context 'when ask for only 1 attr' do 
        context 'when tax doesnt exist' do 
          setup do 
            @query_string = <<-GRAPHQL
              query {
                tax(id: #{@tax1.id + 1}) {
                  name
                }
              }
            GRAPHQL
          end

          should 'return nil' do 
            response = perform(@query_string, @context)
            assert_nil response["data"]["tax"]
          end
        end

        context 'when tax exists' do
          setup do 
            @query_string = <<-GRAPHQL
              query {
                tax(id: #{@tax1.id}) {
                  name
                }
              }
            GRAPHQL
          end

          should 'return only 1 attr for specific tax' do 
            response = perform(@query_string, @context)
            assert_equal response["data"]["tax"]["name"], @tax1.name
            assert_nil response["data"]["tax"]["id"]
          end
        end 
      end 
    end
  end

  context 'when current user has admin permissions' do
    setup do 
      @current_user = create(:user_with_role_admin)
      @context = { 
        current_user: @current_user,
        current_ability: Ability.new(@current_user)
      }
    end 

    context 'when there arent taxes' do 
      setup do 
        @query_string = <<-GRAPHQL
          query {
            tax(id: 1) {
              id
              name 
              state
              interval
              lastPaymentDate
              debtor {
                ... on User {
                  id
                  email
                }
                ... on Agency {
                  id
                  name
                }
              }
            }
          }
        GRAPHQL
      end 

      should 'return nil' do 
        response = perform(@query_string, @context)
        assert_nil response["data"]["tax"]
      end
    end


    context 'when there are taxes' do
      setup do 
        @tax1 = create(:tax)
      end  

      context 'when ask for multiple attrs' do 
        context 'when tax doesnt exist' do 
          setup do 
            @query_string = <<-GRAPHQL
              query {
                tax(id: #{@tax1.id + 1}) {
                  id
                  name 
                  state
                  interval
                  lastPaymentDate
                  debtor {
                    ... on User {
                      id
                      email
                    }
                    ... on Agency {
                      id
                      name
                    }
                  }
                }
              }
            GRAPHQL
          end

          should 'return nil' do 
            response = perform(@query_string, @context)
            assert_nil response["data"]["tax"]
          end
        end

        context 'when tax exists' do
          setup do 
            @query_string = <<-GRAPHQL
              query {
                tax(id: #{@tax1.id}) {
                  id
                  name 
                  state
                  interval
                  lastPaymentDate
                  debtor {
                    ... on User {
                      id
                      email
                    }
                    ... on Agency {
                      id
                      name
                    }
                  }
                }
              }
            GRAPHQL
          end

          context 'when isnt related to user' do 
            should 'return "unauthorized" message' do 
              response = perform(@query_string, @context)
              assert_nil response['data']['tax']
              assert_equal response['errors'][0]['message'], 'user unauthorized'
            end
          end 
          
          context 'when is related to user' do 
            setup do 
              create(:user_agency, agency: @tax1.taxable.property.agency, user: @current_user)
              @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
            end

            should 'return attrs by taxes' do 
              response = perform(@query_string, @context)
              assert_equal response["data"]["tax"]["id"].to_i, @tax1.id
              assert_equal response["data"]["tax"]["name"], @tax1.name
              assert_equal response["data"]["tax"]["state"], @tax1.state
              assert_equal response["data"]["tax"]["interval"], @tax1.interval
              assert_equal response["data"]["tax"]["lastPaymentDate"], @tax1.last_payment_date.to_s
              assert_equal response["data"]["tax"]["debtor"]["id"].to_i, @tax1.debtor.id
              assert_equal response["data"]["tax"]["debtor"]["email"], @tax1.debtor.email
            end
          end 
        end 
      end 

      context 'when ask for only 1 attr' do 
        context 'when tax doesnt exist' do 
          setup do 
            @query_string = <<-GRAPHQL
              query {
                tax(id: #{@tax1.id + 1}) {
                  name
                }
              }
            GRAPHQL
          end

          should 'return nil' do 
            response = perform(@query_string, @context)
            assert_nil response["data"]["tax"]
          end
        end

        context 'when tax exists' do
          setup do 
            @query_string = <<-GRAPHQL
              query {
                tax(id: #{@tax1.id}) {
                  name
                }
              }
            GRAPHQL
          end

          context 'when isnt related to user' do 
            should 'return "unauthorized" message' do 
              response = perform(@query_string, @context)
              assert_nil response['data']['tax']
              assert_equal response['errors'][0]['message'], 'user unauthorized'
            end
          end 
          
          context 'when is related to user' do 
            setup do 
              create(:user_agency, agency: @tax1.taxable.property.agency, user: @current_user)
              @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
            end

            should 'return only 1 attr for specific tax' do 
              response = perform(@query_string, @context)
              assert_equal response["data"]["tax"]["name"], @tax1.name
              assert_nil response["data"]["tax"]["id"]
            end
          end
        end 
      end 
    end
  end
end