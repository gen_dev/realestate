require 'test_helper'

class Queries::TaxesTest < ActiveSupport::TestCase
  def perform(query, context = {})
    BackendSchema.execute(query, context: context).to_h
  end

  context 'when current user has gendev permissions' do
    setup do 
      @current_user = create(:user_with_role_gendev)
      @context = { 
        current_user: @current_user,
        current_ability: Ability.new(@current_user)
      }
    end 

    context 'when ask for multiples attrs' do 
      setup do 
        @query_string = <<-GRAPHQL
          query {
            taxes {
              id
              name 
              state
              interval
              lastPaymentDate
              debtor {
                ... on User {
                  id
                  email
                }
                ... on Agency {
                  id
                  name
                }
              }
            }
          }
        GRAPHQL
      end 

      context 'when there arent taxes' do 
        should 'return empty array' do 
          response = perform(@query_string, @context)
          assert_empty response["data"]["taxes"]
        end
      end
      
      context 'when there are taxes' do
        setup do 
          @tax1 = create(:tax, debtor: create(:user))
          @tax2 = create(:tax, debtor: create(:agency))
        end
        
        should 'return multiple taxes' do 
          response = perform(@query_string, @context)
          assert response["data"]["taxes"]
          assert_operator response["data"]["taxes"].length, :>, 1
          assert_equal response["data"]["taxes"].length, Tax.all.length
        end 

        should 'return attrs by tax' do 
          response = perform(@query_string, @context)
          assert_equal response["data"]["taxes"][0]["id"].to_i, @tax1.id
          assert_equal response["data"]["taxes"][0]["name"], @tax1.name
          assert_equal response["data"]["taxes"][0]["state"], @tax1.state
          assert_equal response["data"]["taxes"][0]["interval"], @tax1.interval
          assert_equal response["data"]["taxes"][0]["lastPaymentDate"], @tax1.last_payment_date.to_s
          assert_equal response["data"]["taxes"][0]["debtor"]["id"].to_i, @tax1.debtor.id
          assert_equal response["data"]["taxes"][0]["debtor"]["email"], @tax1.debtor.email

          assert_equal response["data"]["taxes"][1]["id"].to_i, @tax2.id
          assert_equal response["data"]["taxes"][1]["name"], @tax2.name
          assert_equal response["data"]["taxes"][1]["state"], @tax2.state
          assert_equal response["data"]["taxes"][1]["interval"], @tax2.interval
          assert_equal response["data"]["taxes"][1]["lastPaymentDate"], @tax2.last_payment_date.to_s
          assert_equal response["data"]["taxes"][1]["debtor"]["id"].to_i, @tax2.debtor.id
        end
      end  
    end 

    context 'when ask for only 1 attr' do 
      setup do 
        @query_string = <<-GRAPHQL
          query {
            taxes {
              name 
            }
          }
        GRAPHQL
      end
      
      context 'when there arent taxes' do 
        should 'return empty array' do 
          response = perform(@query_string, @context)
          assert_empty response["data"]["taxes"]
        end
      end
      
      context 'when there are taxes' do
        setup do 
          create(:tax)
          create(:tax)
        end

        should 'return only 1 attr by tax' do 
          response = perform(@query_string, @context)
          assert response["data"]["taxes"][0]["name"]
          assert_nil response["data"]["taxes"][0]["id"]
          assert response["data"]["taxes"][1]["name"]
          assert_nil response["data"]["taxes"][1]["id"]
        end
      end
    end 

    context 'when use filters' do 
      setup do 
        @name = "EPE"
        @query_string = <<-GRAPHQL
          query {
            taxes(filters: { name: "#{@name}" } ) {
              id
              name 
              state
              interval
              lastPaymentDate
            }
          }
        GRAPHQL
      end 

      context 'when taxes dont match filter' do 
        setup do 
          create(:tax, name: "TGI")
        end 

        should 'return empty array' do 
          response = perform(@query_string, @context)
          assert_empty response["data"]["taxes"]
        end
      end

      context 'when there are taxes matching filter' do 
        setup do 
          create(:tax, name: @name)
          create(:tax, name: "TGI")
          create(:tax, name: "API")
        end 
        
        should 'return only correct taxes' do 
          response = perform(@query_string, @context)
          assert response["data"]["taxes"]
          assert_equal response["data"]["taxes"].length, 1
          assert response["data"]["taxes"][0]["id"]
          assert_equal response["data"]["taxes"][0]["name"], @name
          assert response["data"]["taxes"][0]["state"]
          assert response["data"]["taxes"][0]["interval"]
          assert response["data"]["taxes"][0]["lastPaymentDate"]
        end
      end
    end
  end

  context 'when current user has admin permissions' do
    setup do 
      @current_user = create(:user_with_role_admin)
      @context = { 
        current_user: @current_user,
        current_ability: Ability.new(@current_user)
      }
    end 

    context 'when ask for multiples attrs' do 
      setup do 
        @query_string = <<-GRAPHQL
          query {
            taxes {
              id
              name 
              state
              interval
              lastPaymentDate
              debtor {
                ... on User {
                  id
                  email
                }
                ... on Agency {
                  id
                  name
                }
              }
            }
          }
        GRAPHQL
      end 

      context 'when there arent taxes' do 
        should 'return empty array' do 
          response = perform(@query_string, @context)
          assert_empty response["data"]["taxes"]
        end
      end
      
      context 'when there are taxes' do
        setup do 
          @tax1 = create(:tax, debtor: create(:user))
          @tax2 = create(:tax, debtor: create(:agency))
        end

        context 'when there arent taxes related to user' do 
          should 'return empty array' do 
            response = perform(@query_string, @context)
            assert_empty response["data"]["taxes"]
          end
        end 
      
        context 'when there are taxes related to user' do 
          setup do 
            create(:user_agency, agency: @tax1.taxable.property.agency, user: @current_user)
            @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
          end 
        
          should 'return only related taxes' do 
            response = perform(@query_string, @context)
            assert response["data"]["taxes"]
            assert_equal response["data"]["taxes"].length, 1
          end
        end 
      
        context 'when all taxes are related to user' do 
          setup do 
            create(:user_agency, agency: @tax1.taxable.property.agency, user: @current_user)
            create(:user_agency, agency: @tax2.taxable.property.agency, user: @current_user)
            @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
          end 
        
          should 'return multiple taxes' do 
            response = perform(@query_string, @context)
            assert response["data"]["taxes"]
            assert_operator response["data"]["taxes"].length, :>, 1
            assert_equal response["data"]["taxes"].length, Tax.all.length
          end 

          should 'return attrs by tax' do 
            response = perform(@query_string, @context)
            taxes = response["data"]["taxes"].sort_by { |x| x["id"].to_i }

            assert_equal taxes[0]["id"].to_i, @tax1.id
            assert_equal taxes[0]["name"], @tax1.name
            assert_equal taxes[0]["state"], @tax1.state
            assert_equal taxes[0]["interval"], @tax1.interval
            assert_equal taxes[0]["lastPaymentDate"], @tax1.last_payment_date.to_s
            assert_equal taxes[0]["debtor"]["id"].to_i, @tax1.debtor.id
            assert_equal taxes[0]["debtor"]["email"], @tax1.debtor.email

            assert_equal taxes[1]["id"].to_i, @tax2.id
            assert_equal taxes[1]["name"], @tax2.name
            assert_equal taxes[1]["state"], @tax2.state
            assert_equal taxes[1]["interval"], @tax2.interval
            assert_equal taxes[1]["lastPaymentDate"], @tax2.last_payment_date.to_s
            assert_equal taxes[1]["debtor"]["id"].to_i, @tax2.debtor.id
          end
        end
      end  
    end 

    context 'when ask for only 1 attr' do 
      setup do 
        @query_string = <<-GRAPHQL
          query {
            taxes {
              name 
            }
          }
        GRAPHQL
      end
      
      context 'when there arent taxes' do 
        should 'return empty array' do 
          response = perform(@query_string, @context)
          assert_empty response["data"]["taxes"]
        end
      end
      
      context 'when there are taxes' do
        setup do 
          @tax1 = create(:tax)
          @tax2 = create(:tax)
        end

        context 'when there are taxes related to user' do 
          setup do 
            create(:user_agency, agency: @tax1.taxable.property.agency, user: @current_user)
            @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
          end 
        
          should 'return only 1 attr by each related agency' do 
            response = perform(@query_string, @context)
            assert response["data"]["taxes"]
            assert_equal response["data"]["taxes"].length, 1
            assert response["data"]["taxes"][0]["name"]
            assert_nil response["data"]["taxes"][0]["id"]
          end
        end 
  
        context 'when all taxes are related to user' do 
          setup do 
            create(:user_agency, agency: @tax1.taxable.property.agency, user: @current_user)
            create(:user_agency, agency: @tax2.taxable.property.agency, user: @current_user)
            @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
          end 

          should 'return only 1 attr by tax' do 
            response = perform(@query_string, @context)
            assert response["data"]["taxes"][0]["name"]
            assert_nil response["data"]["taxes"][0]["id"]
            assert response["data"]["taxes"][1]["name"]
            assert_nil response["data"]["taxes"][1]["id"]
          end
        end
      end
    end 

    context 'when use filters' do 
      setup do 
        @name = "EPE"
        @query_string = <<-GRAPHQL
          query {
            taxes(filters: { name: "#{@name}" } ) {
              id
              name 
              state
              interval
              lastPaymentDate
            }
          }
        GRAPHQL
      end 

      context 'when taxes dont match filter' do 
        setup do 
          create(:tax, name: "TGI")
        end 

        should 'return empty array' do 
          response = perform(@query_string, @context)
          assert_empty response["data"]["taxes"]
        end
      end

      context 'when there are taxes matching filter' do 
        setup do 
          @tax1 = create(:tax, name: @name)
          @tax2 = create(:tax, name: "TGI")
        end 

        context 'when there arent related to user' do 
          should 'return empty array' do 
            response = perform(@query_string, @context)
            assert_empty response["data"]["taxes"]
          end
        end 

        context 'when there are taxes related to user' do 
          setup do 
            create(:user_agency, agency: @tax1.taxable.property.agency, user: @current_user)
            @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
          end 
        
          should 'return only related and filtered taxes' do 
            response = perform(@query_string, @context)
            assert response["data"]["taxes"]
            assert_equal response["data"]["taxes"].length, 1
            assert response["data"]["taxes"][0]["id"]
            assert_equal response["data"]["taxes"][0]["name"], @name
            assert response["data"]["taxes"][0]["state"]
            assert response["data"]["taxes"][0]["interval"]
            assert response["data"]["taxes"][0]["lastPaymentDate"]
          end
        end
      end
    end
  end
end