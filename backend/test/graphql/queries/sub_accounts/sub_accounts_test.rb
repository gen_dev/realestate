require 'test_helper'

class Queries::CashesTest < ActiveSupport::TestCase
  DEFAULT_SUBACCOUNTS = ["Alquileres cobrados", "Alquileres pagados", "Impuestos y servicios cobrados", "Impuestos y servicios pagados", "Honorarios alquiler", "Honorarios venta", "Comisiones"]

  def perform(query, context = {})
    BackendSchema.execute(query, context: context).to_h
  end

  context 'when current user has gendev permissions' do
    setup do 
      @current_user = create(:user_with_role_gendev)
      create(:user_agency, selected: true, user: @current_user)
      @context = { 
        current_user: @current_user,
        current_ability: Ability.new(@current_user)
      }
    end 

    context 'when ask for multiples attrs' do 
      setup do 
        @query_string = <<-GRAPHQL
          query {
            subAccounts {
              id
              name
              balance
            }
          }
        GRAPHQL
      end 

      context 'when there arent subAccounts' do 
        should 'return empty array' do 
          response = perform(@query_string, @context)
          assert_empty response["data"]["subAccounts"]
        end
      end
      
      context 'when there are subAccounts' do
        setup do 
          @sub_account1 = create(:sub_account, agency: @current_user.selected_agency, account: nil)
          @sub_account2 = create(:sub_account, agency: @current_user.selected_agency, account: nil)
        end
        
        should 'return multiple subAccounts' do 
          response = perform(@query_string, @context)
          assert response["data"]["subAccounts"]
          assert_operator response["data"]["subAccounts"].length, :>, 1
          assert_equal response["data"]["subAccounts"].count + (DEFAULT_SUBACCOUNTS.count * Agency.all.count), SubAccount.all.length
        end 

        should 'return attrs by subAccounts' do 
          response = perform(@query_string, @context)
          subAccounts = response["data"]["subAccounts"].sort_by { |x| x["id"].to_i }

          assert_equal subAccounts[0]["name"], @sub_account1.name
          assert_equal subAccounts[0]["balance"], @sub_account1.balance
          assert_equal subAccounts[0]["id"].to_i, @sub_account1.id
          assert_equal subAccounts[1]["name"], @sub_account2.name
          assert_equal subAccounts[1]["balance"], @sub_account2.balance
          assert_equal subAccounts[1]["id"].to_i, @sub_account2.id
        end
      end  
    end 

    context 'when ask for only 1 attr' do 
      setup do 
        @query_string = <<-GRAPHQL
          query {
            subAccounts {
              name
            }
          }
        GRAPHQL
      end
      
      context 'when there arent subAccounts' do 
        should 'return empty array' do 
          response = perform(@query_string, @context)
          assert_empty response["data"]["subAccounts"]
        end
      end
      
      context 'when there are subAccounts' do
        setup do 
          @sub_account1 = create(:sub_account, agency: @current_user.selected_agency, account: nil)
          @sub_account2 = create(:sub_account, agency: @current_user.selected_agency, account: nil)
        end

        should 'return only 1 attr by subAccount' do 
          response = perform(@query_string, @context)
          assert response["data"]["subAccounts"][0]["name"]
          assert_nil response["data"]["subAccounts"][0]["id"]
          assert response["data"]["subAccounts"][1]["name"]
          assert_nil response["data"]["subAccounts"][1]["id"]
        end
      end
    end 

    context 'when use filters' do 
      setup do 
        @balance = 3500
        @query_string = <<-GRAPHQL
          query {
            subAccounts(filters: { balance: #{@balance} } ) {
              id
              name
              balance
            }
          }
        GRAPHQL
      end 

      context 'when subAccounts dont match filter' do 
        setup do 
          create(:sub_account, balance: "Random", agency: @current_user.selected_agency, account: nil)
        end 

        should 'return empty array' do 
          response = perform(@query_string, @context)
          assert_empty response["data"]["subAccounts"]
        end
      end

      context 'when there are subAccounts matching filter' do 
        setup do 
          create(:sub_account, balance: @balance, agency: @current_user.selected_agency, account: nil)
          create(:sub_account, balance: 1630, agency: @current_user.selected_agency, account: nil)
          create(:sub_account, balance: 2300, agency: @current_user.selected_agency, account: nil)
        end 
        
        should 'return only correct subAccounts' do 
          response = perform(@query_string, @context)
          assert response["data"]["subAccounts"]
          assert_equal response["data"]["subAccounts"].length, 1
          assert response["data"]["subAccounts"][0]["id"]
          assert response["data"]["subAccounts"][0]["name"]
          assert_equal response["data"]["subAccounts"][0]["balance"], @balance
        end
      end
    end 
  end

  context 'when current user has admin permissions' do
    setup do 
      @current_user = create(:user_with_role_admin)
      create(:user_agency, selected: true, user: @current_user)
      @context = { 
        current_user: @current_user,
        current_ability: Ability.new(@current_user)
      }
    end 

    context 'when ask for multiples attrs' do 
      setup do 
        @query_string = <<-GRAPHQL
          query {
            subAccounts {
              id
              name
              balance
            }
          }
        GRAPHQL
      end 

      context 'when there arent subAccounts' do 
        should 'return empty array' do 
          response = perform(@query_string, @context)
          assert_empty response["data"]["subAccounts"]
        end
      end

      context 'when there are subAccounts' do
        setup do 
          @sub_account1 = create(:sub_account, account: nil)
          @sub_account2 = create(:sub_account, account: nil)
        end

        context 'when there arent subAccounts related to user' do 
          should 'return empty array' do 
            response = perform(@query_string, @context)
            assert_empty response["data"]["subAccounts"]
          end
        end 

        context 'when there are subAccounts related to user' do 
          setup do 
            @sub_account1.update agency: @current_user.selected_agency
            @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
          end 
          
          should 'return only related subAccounts' do 
            response = perform(@query_string, @context)
            assert response["data"]["subAccounts"]
            assert_equal response["data"]["subAccounts"].length, 1
          end
        end 

        context 'when all subAccounts are related to user' do 
          setup do 
            @sub_account1.update agency: @current_user.selected_agency
            @sub_account2.update agency: @current_user.selected_agency
            @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
          end 

          should 'return multiple subAccounts' do 
            response = perform(@query_string, @context)
            assert response["data"]["subAccounts"]
            assert_operator response["data"]["subAccounts"].length, :>, 1
            assert_equal response["data"]["subAccounts"].count + (DEFAULT_SUBACCOUNTS.count * Agency.all.count), SubAccount.all.length
          end 

          should 'return attrs by subAccounts' do 
            response = perform(@query_string, @context)
            subAccounts = response["data"]["subAccounts"].sort_by { |x| x["id"].to_i }
            
            assert_equal subAccounts[0]["id"].to_i, @sub_account1.id
            assert_equal subAccounts[0]["name"], @sub_account1.name
            assert_equal subAccounts[0]["balance"], @sub_account1.balance

            assert_equal subAccounts[1]["id"].to_i, @sub_account2.id
            assert_equal subAccounts[1]["name"], @sub_account2.name
            assert_equal subAccounts[1]["balance"], @sub_account2.balance
          end
        end
      end  
    end 

    context 'when ask for only 1 attr' do 
      setup do 
        @query_string = <<-GRAPHQL
          query {
            subAccounts {
              name
            }
          }
        GRAPHQL
      end
      
      context 'when there arent subAccounts' do 
        should 'return empty array' do 
          response = perform(@query_string, @context)
          assert_empty response["data"]["subAccounts"]
        end
      end
      
      context 'when there are subAccounts' do
        setup do 
          @sub_account1 = create(:sub_account, account: nil)
          @sub_account2 = create(:sub_account, account: nil)
        end

        context 'when there are subAccounts related to user' do 
          setup do 
            @sub_account1.update agency: @current_user.selected_agency
            @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
          end 
        
          should 'return only 1 attr by each related agency' do 
            response = perform(@query_string, @context)
            assert response["data"]["subAccounts"]
            assert_equal response["data"]["subAccounts"].length, 1
            assert response["data"]["subAccounts"][0]["name"]
            assert_nil response["data"]["subAccounts"][0]["id"]
          end
        end 

        context 'when all subAccounts are related to user' do 
          setup do 
            @sub_account1.update agency: @current_user.selected_agency
            @sub_account2.update agency: @current_user.selected_agency
            @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
          end 

          should 'return only 1 attr by subAccount' do 
            response = perform(@query_string, @context)
            assert response["data"]["subAccounts"][0]["name"]
            assert_nil response["data"]["subAccounts"][0]["id"]
            assert response["data"]["subAccounts"][1]["name"]
            assert_nil response["data"]["subAccounts"][1]["id"]
          end
        end
      end
    end 

    context 'when use filters' do 
      setup do 
        @balance = 3500
        @query_string = <<-GRAPHQL
          query {
            subAccounts(filters: { balance: #{@balance} } ) {
              id
              name
              balance
            }
          }
        GRAPHQL
      end 

      context 'when subAccounts dont match filter' do 
        setup do 
          create(:sub_account, balance: "Random", account: nil)
        end 

        should 'return empty array' do 
          response = perform(@query_string, @context)
          assert_empty response["data"]["subAccounts"]
        end
      end

      context 'when there are subAccounts matching filter' do 
        setup do 
          @sub_account1 = create(:sub_account, balance: @balance, account: nil)
          @sub_account2 = create(:sub_account, balance: 1630, account: nil)
          @sub_account3 = create(:sub_account, balance: @balance, account: nil)
        end 

        context 'when there arent related to user' do 
          should 'return empty array' do 
            response = perform(@query_string, @context)
            assert_empty response["data"]["subAccounts"]
          end
        end 

        context 'when there are subAccounts related to user' do 
          setup do 
            @sub_account1.update agency: @current_user.selected_agency
            @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
          end 
        
          should 'return only related and filtered subAccounts' do 
            response = perform(@query_string, @context)
            assert response["data"]["subAccounts"]
            assert_equal response["data"]["subAccounts"].length, 1
            assert response["data"]["subAccounts"][0]["id"]
            assert response["data"]["subAccounts"][0]["name"]
            assert_equal response["data"]["subAccounts"][0]["balance"], @balance
          end
        end
      end
    end 
  end
end