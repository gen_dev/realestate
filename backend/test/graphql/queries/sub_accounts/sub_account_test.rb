require 'test_helper'

class Queries::SubAccountTest < ActiveSupport::TestCase
  def perform(query, context = {})
    BackendSchema.execute(query, context: context).to_h
  end

  context 'when current user has gendev permissions' do
    setup do 
      @current_user = create(:user_with_role_gendev)
      @context = { 
        current_user: @current_user,
        current_ability: Ability.new(@current_user)
      }
    end 

    context 'when there arent agencies' do 
      setup do 
        @query_string = <<-GRAPHQL
          query {
            subAccount(id: 1) {
              id
              name
              balance
            }
          }
        GRAPHQL
      end 

      should 'return nil' do 
        response = perform(@query_string, @context)
        assert_nil response["data"]["subAccount"]
      end
    end


    context 'when there are agencies' do
      setup do 
        @sub_account1 = create(:sub_account)
      end  

      context 'when ask for multiple attrs' do 
        context 'when subAccount doesnt exist' do 
          setup do 
            @query_string = <<-GRAPHQL
              query {
                subAccount(id: #{@sub_account1.id + 1}) {
                  id
                  name
                  balance
                }
              }
            GRAPHQL
          end

          should 'return nil' do 
            response = perform(@query_string, @context)
            assert_nil response["data"]["subAccount"]
          end
        end

        context 'when subAccount exists' do
          setup do 
            @query_string = <<-GRAPHQL
              query {
                subAccount(id: #{@sub_account1.id}) {
                  id
                  name
                  balance
                }
              }
            GRAPHQL
          end

          should 'return only 1 attr for specific subAccount' do 
            response = perform(@query_string, @context)
            assert_equal response["data"]["subAccount"]["name"], @sub_account1.name
            assert_equal response["data"]["subAccount"]["balance"], @sub_account1.balance
            assert_equal response["data"]["subAccount"]["id"].to_i, @sub_account1.id
          end
        end 
      end 

      context 'when ask for only 1 attr' do 
        context 'when subAccount doesnt exist' do 
          setup do 
            @query_string = <<-GRAPHQL
              query {
                subAccount(id: #{@sub_account1.id + 1}) {
                  name
                }
              }
            GRAPHQL
          end

          should 'return nil' do 
            response = perform(@query_string, @context)
            assert_nil response["data"]["subAccount"]
          end
        end

        context 'when subAccount exists' do
          setup do 
            @query_string = <<-GRAPHQL
              query {
                subAccount(id: #{@sub_account1.id}) {
                  name
                }
              }
            GRAPHQL
          end

          should 'return only 1 attr for specific subAccount' do 
            response = perform(@query_string, @context)
            assert_equal response["data"]["subAccount"]["name"], @sub_account1.name
            assert_nil response["data"]["subAccount"]["id"]
          end
        end 
      end 
    end
  end

  context 'when current user has admin permissions' do
    setup do 
      @current_user = create(:user_with_role_admin)
      @context = { 
        current_user: @current_user,
        current_ability: Ability.new(@current_user)
      }
    end 

    context 'when there arent agencies' do 
      setup do 
        @query_string = <<-GRAPHQL
          query {
            subAccount(id: 1) {
              id
              name
              balance
            }
          }
        GRAPHQL
      end 

      should 'return nil' do 
        response = perform(@query_string, @context)
        assert_nil response["data"]["subAccount"]
      end
    end


    context 'when there are agencies' do
      setup do 
        @sub_account1 = create(:sub_account)
      end  

      context 'when ask for multiple attrs' do 
        context 'when subAccount doesnt exist' do 
          setup do 
            @query_string = <<-GRAPHQL
              query {
                subAccount(id: #{@sub_account1.id + 1}) {
                  id
                  name
                  balance
                }
              }
            GRAPHQL
          end

          should 'return nil' do 
            response = perform(@query_string, @context)
            assert_nil response["data"]["subAccount"]
          end
        end

        context 'when subAccount exists' do
          setup do 
            @query_string = <<-GRAPHQL
              query {
                subAccount(id: #{@sub_account1.id}) {
                  id
                  name
                  balance
                }
              }
            GRAPHQL
          end

          context 'when isnt related to user' do 
            should 'return "unauthorized" message' do 
              response = perform(@query_string, @context)
              assert_nil response['data']['subAccount']
              assert_equal response['errors'][0]['message'], 'user unauthorized'
            end
          end 

          context 'when is related to user' do 
            setup do 
              create(:user_agency, agency: @sub_account1.agency, user: @current_user)
              @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
            end 

            should 'return attrs for specific subAccount' do 
              response = perform(@query_string, @context)
              assert_equal response["data"]["subAccount"]["name"], @sub_account1.name
              assert_equal response["data"]["subAccount"]["balance"], @sub_account1.balance
              assert_equal response["data"]["subAccount"]["id"].to_i, @sub_account1.id
            end
          end
        end 
      end 

      context 'when ask for only 1 attr' do 
        context 'when subAccount doesnt exist' do 
          setup do 
            @query_string = <<-GRAPHQL
              query {
                subAccount(id: #{@sub_account1.id + 1}) {
                  name
                }
              }
            GRAPHQL
          end

          should 'return nil' do 
            response = perform(@query_string, @context)
            assert_nil response["data"]["subAccount"]
          end
        end

        context 'when subAccount exists' do
          setup do 
            @query_string = <<-GRAPHQL
              query {
                subAccount(id: #{@sub_account1.id}) {
                  name
                }
              }
            GRAPHQL
          end

          context 'when isnt related to user' do 
            should 'return "unauthorized" message' do 
              response = perform(@query_string, @context)
              assert_nil response['data']['subAccount']
              assert_equal response['errors'][0]['message'], 'user unauthorized'
            end
          end 

          context 'when is related to user' do 
            setup do 
              create(:user_agency, agency: @sub_account1.agency, user: @current_user)
              @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
            end 

            should 'return only 1 attr for specific subAccount' do 
              response = perform(@query_string, @context)
              assert_equal response["data"]["subAccount"]["name"], @sub_account1.name
              assert_nil response["data"]["subAccount"]["id"]
            end
          end 
        end 
      end 
    end
  end
end