require 'test_helper'

class Queries::ContractTest < ActiveSupport::TestCase
  def perform(query, context = {})
    BackendSchema.execute(query, context: context).to_h
  end

  context 'when current user has gendev permissions' do
    setup do 
      @current_user = create(:user_with_role_gendev)
      @context = { 
        current_user: @current_user,
        current_ability: Ability.new(@current_user)
      }
    end 

    context 'when there arent contracts' do 
      setup do 
        @query_string = <<-GRAPHQL
          query {
            contract(id: 1) {
              id
              contractType
              state
              property {
                id
                propertyType
                state
                description
              }
            }
          }
        GRAPHQL
      end 

      should 'return nil' do 
        response = perform(@query_string, @context)
        assert_nil response["data"]["contract"]
      end
    end


    context 'when there are contracts' do
      setup do 
        @contract1 = create(:contract)
      end  

      context 'when ask for multiple attrs' do 
        context 'when contract doesnt exist' do 
          setup do 
            @query_string = <<-GRAPHQL
              query {
                contract(id: #{@contract1.id + 1}) {
                  id
                  contractType
                  state
                  property {
                    id
                    propertyType
                    state
                    description
                  }
                }
              }
            GRAPHQL
          end

          should 'return nil' do 
            response = perform(@query_string, @context)
            assert_nil response["data"]["contract"]
          end
        end

        context 'when contract exists' do
          setup do 
            @query_string = <<-GRAPHQL
              query {
                contract(id: #{@contract1.id}) {
                  id
                  contractType
                  state
                  property {
                    id
                    propertyType
                    state
                    description
                  }
                }
              }
            GRAPHQL
          end

          should 'return attrs by contract' do 
            response = perform(@query_string, @context)
            assert_equal response["data"]["contract"]["state"], @contract1.state
            assert_equal response["data"]["contract"]["contractType"], @contract1.contract_type
            assert_equal response["data"]["contract"]["property"]["propertyType"], @contract1.property.property_type
            assert_equal response["data"]["contract"]["property"]["state"], @contract1.property.state
            assert_equal response["data"]["contract"]["property"]["description"], @contract1.property.description
            assert_equal response["data"]["contract"]["id"].to_i, @contract1.id
          end
        end 
      end 

      context 'when ask for only 1 attr' do 
        context 'when contract doesnt exist' do 
          setup do 
            @query_string = <<-GRAPHQL
              query {
                contract(id: #{@contract1.id + 1}) {
                  contractType
                }
              }
            GRAPHQL
          end

          should 'return nil' do 
            response = perform(@query_string, @context)
            assert_nil response["data"]["contract"]
          end
        end

        context 'when contract exists' do
          setup do 
            @query_string = <<-GRAPHQL
              query {
                contract(id: #{@contract1.id}) {
                  contractType
                }
              }
            GRAPHQL
          end

          should 'return only 1 attr for specific contract' do 
            response = perform(@query_string, @context)
            assert_equal response["data"]["contract"]["contractType"], @contract1.contract_type
            assert_nil response["data"]["contract"]["id"]
          end
        end 
      end 
    end
  end

  context 'when current user has admin permissions' do
    setup do 
      @current_user = create(:user_with_role_admin)
      @context = { 
        current_user: @current_user,
        current_ability: Ability.new(@current_user)
      }
    end 

    context 'when there arent contracts' do 
      setup do 
        @query_string = <<-GRAPHQL
          query {
            contract(id: 1) {
              id
              contractType
              state
              property {
                id
                propertyType
                state
                description
              }
            }
          }
        GRAPHQL
      end 

      should 'return nil' do 
        response = perform(@query_string, @context)
        assert_nil response["data"]["contract"]
      end
    end


    context 'when there are contracts' do
      setup do 
        @contract1 = create(:contract)
      end  

      context 'when ask for multiple attrs' do 
        context 'when contract doesnt exist' do 
          setup do 
            @query_string = <<-GRAPHQL
              query {
                contract(id: #{@contract1.id + 1}) {
                  id
                  contractType
                  state
                  property {
                    id
                    propertyType
                    state
                    description
                  }
                }
              }
            GRAPHQL
          end

          should 'return nil' do 
            response = perform(@query_string, @context)
            assert_nil response["data"]["contract"]
          end
        end

        context 'when contract exists' do
          setup do 
            @query_string = <<-GRAPHQL
              query {
                contract(id: #{@contract1.id}) {
                  id
                  contractType
                  state
                  property {
                    id
                    propertyType
                    state
                    description
                  }
                }
              }
            GRAPHQL
          end

          context 'when isnt related to user' do 
            should 'return "unauthorized" message' do 
              response = perform(@query_string, @context)
              assert_nil response['data']['contract']
              assert_equal response['errors'][0]['message'], 'user unauthorized'
            end
          end 
          
          context 'when is related to user' do 
            setup do 
              create(:user_agency, agency: @contract1.property.agency, user: @current_user)
              @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
            end 

            should 'return attrs by contract' do 
              response = perform(@query_string, @context)
              assert_equal response["data"]["contract"]["state"], @contract1.state
              assert_equal response["data"]["contract"]["contractType"], @contract1.contract_type
              assert_equal response["data"]["contract"]["property"]["propertyType"], @contract1.property.property_type
              assert_equal response["data"]["contract"]["property"]["state"], @contract1.property.state
              assert_equal response["data"]["contract"]["property"]["description"], @contract1.property.description
              assert_equal response["data"]["contract"]["id"].to_i, @contract1.id
            end
          end
        end 
      end 

      context 'when ask for only 1 attr' do 
        context 'when contract doesnt exist' do 
          setup do 
            @query_string = <<-GRAPHQL
              query {
                contract(id: #{@contract1.id + 1}) {
                  contractType
                }
              }
            GRAPHQL
          end

          should 'return nil' do 
            response = perform(@query_string, @context)
            assert_nil response["data"]["contract"]
          end
        end

        context 'when contract exists' do
          setup do 
            @query_string = <<-GRAPHQL
              query {
                contract(id: #{@contract1.id}) {
                  contractType
                }
              }
            GRAPHQL
          end

          context 'when isnt related to user' do 
            should 'return "unauthorized" message' do 
              response = perform(@query_string, @context)
              assert_nil response['data']['contract']
              assert_equal response['errors'][0]['message'], 'user unauthorized'
            end
          end 
          
          context 'when is related to user' do 
            setup do 
              create(:user_agency, agency: @contract1.property.agency, user: @current_user)
              @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
            end 

            should 'return only 1 attr for specific contract' do 
              response = perform(@query_string, @context)
              assert_equal response["data"]["contract"]["contractType"], @contract1.contract_type
              assert_nil response["data"]["contract"]["id"]
            end
          end 
        end 
      end 
    end
  end
end