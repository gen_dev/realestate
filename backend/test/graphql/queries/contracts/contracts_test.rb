require 'test_helper'

class Queries::ContractsTest < ActiveSupport::TestCase
  def perform(query, context = {})
    BackendSchema.execute(query, context: context).to_h
  end

  context 'when current user has gendev permissions' do
    setup do 
      @current_user = create(:user_with_role_gendev)
      create(:user_agency, selected: true, user: @current_user)
      @context = { 
        current_user: @current_user,
        current_ability: Ability.new(@current_user)
      }
    end 

    context 'when ask for multiples attrs' do 
      setup do 
        @query_string = <<-GRAPHQL
          query {
            contracts {
              contracts {
                id
                contractType
                state
                property {
                  id
                  propertyType
                  state
                  description
                }
              }
              pages
            }
          }
        GRAPHQL
      end 

      context 'when there arent contracts' do 
        should 'return empty array' do 
          response = perform(@query_string, @context)
          assert_empty response["data"]["contracts"]["contracts"]
        end
      end
      
      context 'when there are contracts' do
        setup do 
          @contract1 = create(:contract, property: create(:property, agency: @current_user.selected_agency))
          @contract2 = create(:contract, property: create(:property, agency: @current_user.selected_agency))
        end
        
        should 'return multiple contracts' do 
          response = perform(@query_string, @context)
          assert response["data"]["contracts"]["contracts"]
          assert_operator response["data"]["contracts"]["contracts"].length, :>, 1
          assert_equal response["data"]["contracts"]["contracts"].length, Contract.all.length
        end 

        should 'return attrs by contract' do 
          response = perform(@query_string, @context)
          contracts = response["data"]["contracts"]["contracts"].sort_by { |x| x["id"].to_i }

          assert_equal contracts[0]["contractType"], @contract1.contract_type
          assert_equal contracts[0]["state"], @contract1.state
          assert_equal contracts[0]["id"].to_i, @contract1.id
          assert_equal contracts[0]["property"]["propertyType"], @contract1.property.property_type
          assert_equal contracts[0]["property"]["state"], @contract1.property.state
          assert_equal contracts[0]["property"]["description"], @contract1.property.description
          assert_equal contracts[1]["contractType"], @contract2.contract_type
          assert_equal contracts[1]["state"], @contract2.state
          assert_equal contracts[1]["id"].to_i, @contract2.id
          assert_equal contracts[1]["property"]["propertyType"], @contract2.property.property_type
          assert_equal contracts[1]["property"]["state"], @contract2.property.state
          assert_equal contracts[1]["property"]["description"], @contract2.property.description
        end
      end  
    end 

    context 'when ask for only 1 attr' do 
      setup do 
        @query_string = <<-GRAPHQL
          query {
            contracts {
              contracts {
                contractType
              }
              pages
            }
          }
        GRAPHQL
      end
      
      context 'when there arent contracts' do 
        should 'return empty array' do 
          response = perform(@query_string, @context)
          assert_empty response["data"]["contracts"]["contracts"]
        end
      end
      
      context 'when there are contracts' do
        setup do 
          create(:contract, property: create(:property, agency: @current_user.selected_agency))
          create(:contract, property: create(:property, agency: @current_user.selected_agency))
        end

        should 'return only 1 attr by contract' do 
          response = perform(@query_string, @context)
          assert response["data"]["contracts"]["contracts"][0]["contractType"]
          assert_nil response["data"]["contracts"]["contracts"][0]["id"]
          assert response["data"]["contracts"]["contracts"][1]["contractType"]
          assert_nil response["data"]["contracts"]["contracts"][1]["id"]
        end
      end
    end 

    context 'when use filters' do 
      setup do 
        @state = 'active'
        @query_string = <<-GRAPHQL
          query {
            contracts(filters: { state: "#{@state}" } ) {
              contracts {
                id
                state
                contractType
              }
              pages
            }
          }
        GRAPHQL
      end 

      context 'when contracts dont match filter' do 
        setup do 
          create(:contract, state: "Random", property: create(:property, agency: @current_user.selected_agency))
        end 

        should 'return empty array' do 
          response = perform(@query_string, @context)
          assert_empty response["data"]["contracts"]["contracts"]
        end
      end

      context 'when there are contracts matching filter' do 
        setup do 
          create(:contract, state: @state, property: create(:property, agency: @current_user.selected_agency))
          create(:contract, state: "pending", property: create(:property, agency: @current_user.selected_agency))
          create(:contract, state: "finished", property: create(:property, agency: @current_user.selected_agency))
        end 
        
        should 'return only correct contracts' do 
          response = perform(@query_string, @context)
          assert response["data"]["contracts"]["contracts"]
          assert_equal response["data"]["contracts"]["contracts"].length, 1
          assert response["data"]["contracts"]["contracts"][0]["id"]
          assert_equal response["data"]["contracts"]["contracts"][0]["state"], @state
          assert response["data"]["contracts"]["contracts"][0]["contractType"]
        end
      end
    end
  end

  context 'when current user has admin permissions' do
    setup do 
      @current_user = create(:user_with_role_admin)
      create(:user_agency, selected: true, user: @current_user)
      @context = { 
        current_user: @current_user,
        current_ability: Ability.new(@current_user)
      }
    end 

    context 'when ask for multiples attrs' do 
      setup do 
        @query_string = <<-GRAPHQL
          query {
            contracts {
              contracts {
                id
                contractType
                state
                property {
                  id
                  propertyType
                  state
                  description
                }
              }
              pages
            }
          }
        GRAPHQL
      end 

      context 'when there arent contracts' do 
        should 'return empty array' do 
          response = perform(@query_string, @context)
          assert_empty response["data"]["contracts"]["contracts"]
        end
      end
      
      context 'when there are contracts' do
        setup do 
          @contract1 = create(:contract)
          @contract2 = create(:contract)
        end

        context 'when there arent contracts related to user' do 
          should 'return empty array' do 
            response = perform(@query_string, @context)
            assert_empty response["data"]["contracts"]["contracts"]
          end
        end 
      
        context 'when there are contracts related to user' do 
          setup do 
            @contract1.update(property: create(:property, agency: @current_user.selected_agency))
            @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
          end 
        
          should 'return only related contracts' do 
            response = perform(@query_string, @context)
            assert response["data"]["contracts"]["contracts"]
            assert_equal response["data"]["contracts"]["contracts"].length, 1
          end
        end 
      
        context 'when all contracts are related to user' do 
          setup do 
            @contract1.update(property: create(:property, agency: @current_user.selected_agency))
            @contract2.update(property: create(:property, agency: @current_user.selected_agency))
            @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
          end 

          should 'return multiple contracts' do 
            response = perform(@query_string, @context)
            assert response["data"]["contracts"]["contracts"]
            assert_operator response["data"]["contracts"]["contracts"].length, :>, 1
            assert_equal response["data"]["contracts"]["contracts"].length, Contract.all.length
          end 

          should 'return attrs by contract' do 
            response = perform(@query_string, @context)
            contracts = response["data"]["contracts"]["contracts"].sort_by { |x| x["id"].to_i }

            assert_equal contracts[0]["contractType"], @contract1.contract_type
            assert_equal contracts[0]["state"], @contract1.state
            assert_equal contracts[0]["id"].to_i, @contract1.id
            assert_equal contracts[0]["property"]["propertyType"], @contract1.property.property_type
            assert_equal contracts[0]["property"]["state"], @contract1.property.state
            assert_equal contracts[0]["property"]["description"], @contract1.property.description
            
            assert_equal contracts[1]["contractType"], @contract2.contract_type
            assert_equal contracts[1]["state"], @contract2.state
            assert_equal contracts[1]["id"].to_i, @contract2.id
            assert_equal contracts[1]["property"]["propertyType"], @contract2.property.property_type
            assert_equal contracts[1]["property"]["state"], @contract2.property.state
            assert_equal contracts[1]["property"]["description"], @contract2.property.description
          end
        end
      end  
    end 

    context 'when ask for only 1 attr' do 
      setup do 
        @query_string = <<-GRAPHQL
          query {
            contracts {
              contracts {
                contractType
              }
              pages
            }
          }
        GRAPHQL
      end
      
      context 'when there arent contracts' do 
        should 'return empty array' do 
          response = perform(@query_string, @context)
          assert_empty response["data"]["contracts"]["contracts"]
        end
      end
      
      context 'when there are contracts' do
        setup do 
          @contract1 = create(:contract)
          @contract2 = create(:contract)
        end

        context 'when there are contracts related to user' do 
          setup do 
            @contract1.update(property: create(:property, agency: @current_user.selected_agency))
            @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
          end 
        
          should 'return only 1 attr by each related contract' do 
            response = perform(@query_string, @context)
            assert response["data"]["contracts"]["contracts"]
            assert_equal response["data"]["contracts"]["contracts"].length, 1
            assert response["data"]["contracts"]["contracts"][0]["contractType"]
            assert_nil response["data"]["contracts"]["contracts"][0]["id"]
          end
        end 
  
        context 'when all contracts are related to user' do 
          setup do 
            @contract1.update(property: create(:property, agency: @current_user.selected_agency))
            @contract2.update(property: create(:property, agency: @current_user.selected_agency))
            @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
          end 

          should 'return only 1 attr by contract' do 
            response = perform(@query_string, @context)
            assert response["data"]["contracts"]["contracts"][0]["contractType"]
            assert_nil response["data"]["contracts"]["contracts"][0]["id"]
            assert response["data"]["contracts"]["contracts"][1]["contractType"]
            assert_nil response["data"]["contracts"]["contracts"][1]["id"]
          end
        end
      end
    end 

    context 'when use filters' do 
      setup do 
        @state = 'active'
        @query_string = <<-GRAPHQL
          query {
            contracts(filters: { state: "#{@state}" } ) {
              contracts {
                id
                state
                contractType
              }
              pages
            }
          }
        GRAPHQL
      end 

      context 'when contracts dont match filter' do 
        setup do 
          create(:contract, state: "Random")
        end 

        should 'return empty array' do 
          response = perform(@query_string, @context)
          assert_empty response["data"]["contracts"]["contracts"]
        end
      end

      context 'when there are contracts matching filter' do 
        setup do 
          @contract1 = create(:contract, state: @state)
          @contract2 = create(:contract, state: "pending")
          @contract3 = create(:contract, state: @state)
        end 

        context 'when there arent related to user' do 
          should 'return empty array' do 
            response = perform(@query_string, @context)
            assert_empty response["data"]["contracts"]["contracts"]
          end
        end 

        context 'when there are contracts related to user' do 
          setup do 
            @contract1.update(property: create(:property, agency: @current_user.selected_agency))
            @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
          end 
        
          should 'return only related and filtered contracts' do 
            response = perform(@query_string, @context)
            assert response["data"]["contracts"]["contracts"]
            assert_equal response["data"]["contracts"]["contracts"].length, 1
            assert response["data"]["contracts"]["contracts"][0]["id"]
            assert_equal response["data"]["contracts"]["contracts"][0]["state"], @state
            assert response["data"]["contracts"]["contracts"][0]["contractType"]
          end
        end
      end
    end
  end
end