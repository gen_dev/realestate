require 'test_helper'

class Queries::ContractPlanTest < ActiveSupport::TestCase
  def perform(query, context = {})
    BackendSchema.execute(query, context: context).to_h
  end

  context 'when current user has gendev permissions' do
    setup do 
      @current_user = create(:user_with_role_gendev)
      @context = { 
        current_user: @current_user,
        current_ability: Ability.new(@current_user)
      }
    end 

    context 'when there arent contract plans' do 
      setup do 
        @query_string = <<-GRAPHQL
          query {
            contractPlan(id: 1) {
              id
              duration
              rentUpdatePeriod
              rentValue
              property {
                id
                propertyType
                state
                description
              }
            }
          }
        GRAPHQL
      end 

      should 'return nil' do 
        response = perform(@query_string, @context)
        assert_nil response["data"]["contractPlan"]
      end
    end


    context 'when there are contract plans' do
      setup do 
        @contract_plan1 = create(:contract_plan)
      end  

      context 'when ask for multiple attrs' do 
        context 'when contract plan doesnt exist' do 
          setup do 
            @query_string = <<-GRAPHQL
              query {
                contractPlan(id: #{@contract_plan1.id + 1}) {
                  id
                  duration
                  rentUpdatePeriod
                  rentValue
                  property {
                    id
                    propertyType
                    state
                    description
                  }
                }
              }
            GRAPHQL
          end

          should 'return nil' do 
            response = perform(@query_string, @context)
            assert_nil response["data"]["contract"]
          end
        end

        context 'when contract plan exists' do
          setup do 
            @query_string = <<-GRAPHQL
              query {
                contractPlan(id: #{@contract_plan1.id}) {
                  id
                  duration
                  rentUpdatePeriod
                  rentValue
                  property {
                    id
                    propertyType
                    state
                    description
                  }
                }
              }
            GRAPHQL
          end

          should 'return attrs by contract plan' do 
            response = perform(@query_string, @context)
            assert_equal response["data"]["contractPlan"]["id"].to_i, @contract_plan1.id
            assert_equal response["data"]["contractPlan"]["duration"], @contract_plan1.duration
            assert_equal response["data"]["contractPlan"]["rentUpdatePeriod"], @contract_plan1.rent_update_period
            assert_equal response["data"]["contractPlan"]["rentValue"], @contract_plan1.rent_value
            assert_equal response["data"]["contractPlan"]["property"]["id"].to_i, @contract_plan1.property.id
            assert_equal response["data"]["contractPlan"]["property"]["propertyType"], @contract_plan1.property.property_type
            assert_equal response["data"]["contractPlan"]["property"]["state"], @contract_plan1.property.state
            assert_equal response["data"]["contractPlan"]["property"]["description"], @contract_plan1.property.description
          end
        end 
      end 

      context 'when ask for only 1 attr' do 
        context 'when contract plan doesnt exist' do 
          setup do 
            @query_string = <<-GRAPHQL
              query {
                contractPlan(id: #{@contract_plan1.id + 1}) {
                  duration
                }
              }
            GRAPHQL
          end

          should 'return nil' do 
            response = perform(@query_string, @context)
            assert_nil response["data"]["contractPlan"]
          end
        end

        context 'when contract plan exists' do
          setup do 
            @query_string = <<-GRAPHQL
              query {
                contractPlan(id: #{@contract_plan1.id}) {
                  duration
                }
              }
            GRAPHQL
          end

          should 'return only 1 attr for specific contractPlan' do 
            response = perform(@query_string, @context)
            assert_equal response["data"]["contractPlan"]["duration"], @contract_plan1.duration
            assert_nil response["data"]["contractPlan"]["id"]
          end
        end 
      end 
    end
  end

  context 'when current user has admin permissions' do
    setup do 
      @current_user = create(:user_with_role_admin)
      @context = { 
        current_user: @current_user,
        current_ability: Ability.new(@current_user)
      }
    end 

    context 'when there arent contract plans' do 
      setup do 
        @query_string = <<-GRAPHQL
          query {
            contractPlan(id: 1) {
              id
              duration
              rentUpdatePeriod
              rentValue
              property {
                id
                propertyType
                state
                description
              }
            }
          }
        GRAPHQL
      end 

      should 'return nil' do 
        response = perform(@query_string, @context)
        assert_nil response["data"]["contractPlan"]
      end
    end


    context 'when there are contract plans' do
      setup do 
        @contract_plan1 = create(:contract_plan)
      end  

      context 'when ask for multiple attrs' do 
        context 'when contract plan doesnt exist' do 
          setup do 
            @query_string = <<-GRAPHQL
              query {
                contractPlan(id: #{@contract_plan1.id + 1}) {
                  id
                  duration
                  rentUpdatePeriod
                  rentValue
                  property {
                    id
                    propertyType
                    state
                    description
                  }
                }
              }
            GRAPHQL
          end

          should 'return nil' do 
            response = perform(@query_string, @context)
            assert_nil response["data"]["contract"]
          end
        end

        context 'when contract plan exists' do
          setup do 
            @query_string = <<-GRAPHQL
              query {
                contractPlan(id: #{@contract_plan1.id}) {
                  id
                  duration
                  rentUpdatePeriod
                  rentValue
                  property {
                    id
                    propertyType
                    state
                    description
                  }
                }
              }
            GRAPHQL
          end

          context 'when isnt related to user' do 
            should 'return "unauthorized" message' do 
              response = perform(@query_string, @context)
              assert_nil response['data']['contractPlan']
              assert_equal response['errors'][0]['message'], 'user unauthorized'
            end
          end 
          
          context 'when is related to user' do 
            setup do 
              create(:user_agency, agency: @contract_plan1.property.agency, user: @current_user)
              @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
            end 

            should 'return attrs by contract plan' do 
              response = perform(@query_string, @context)
              assert_equal response["data"]["contractPlan"]["id"].to_i, @contract_plan1.id
              assert_equal response["data"]["contractPlan"]["duration"], @contract_plan1.duration
              assert_equal response["data"]["contractPlan"]["rentUpdatePeriod"], @contract_plan1.rent_update_period
              assert_equal response["data"]["contractPlan"]["rentValue"], @contract_plan1.rent_value
              assert_equal response["data"]["contractPlan"]["property"]["id"].to_i, @contract_plan1.property.id
              assert_equal response["data"]["contractPlan"]["property"]["propertyType"], @contract_plan1.property.property_type
              assert_equal response["data"]["contractPlan"]["property"]["state"], @contract_plan1.property.state
              assert_equal response["data"]["contractPlan"]["property"]["description"], @contract_plan1.property.description
            end
          end
        end 
      end 

      context 'when ask for only 1 attr' do 
        context 'when contract plan doesnt exist' do 
          setup do 
            @query_string = <<-GRAPHQL
              query {
                contractPlan(id: #{@contract_plan1.id + 1}) {
                  duration
                }
              }
            GRAPHQL
          end

          should 'return nil' do 
            response = perform(@query_string, @context)
            assert_nil response["data"]["contractPlan"]
          end
        end

        context 'when contract plan exists' do
          setup do 
            @query_string = <<-GRAPHQL
              query {
                contractPlan(id: #{@contract_plan1.id}) {
                  duration
                }
              }
            GRAPHQL
          end

          context 'when isnt related to user' do 
            should 'return "unauthorized" message' do 
              response = perform(@query_string, @context)
              assert_nil response['data']['contractPlan']
              assert_equal response['errors'][0]['message'], 'user unauthorized'
            end
          end 
          
          context 'when is related to user' do 
            setup do 
              create(:user_agency, agency: @contract_plan1.property.agency, user: @current_user)
              @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
            end 

            should 'return only 1 attr for specific contractPlan' do 
              response = perform(@query_string, @context)
              assert_equal response["data"]["contractPlan"]["duration"], @contract_plan1.duration
              assert_nil response["data"]["contractPlan"]["id"]
            end
          end 
        end 
      end 
    end
  end
end