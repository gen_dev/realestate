require 'test_helper'

class Queries::ContractExpensesTest < ActiveSupport::TestCase
  def perform(query, context = {})
    BackendSchema.execute(query, context: context).to_h
  end

  context 'when current user has gendev permissions' do
    setup do 
      @current_user = create(:user_with_role_gendev)
      @context = { 
        current_user: @current_user,
        current_ability: Ability.new(@current_user)
      }
    end 

    context 'when ask for multiples attrs' do 
      setup do 
        @query_string = <<-GRAPHQL
          query {
            contractExpenses {
              id
              name
              value
              contractPlan {
                id
                duration
                rentUpdatePeriod
                rentValue
                property {
                  id
                  propertyType
                  state
                  description
                }
              }
            }
          }
        GRAPHQL
      end 

      context 'when there arent contract expenses' do 
        should 'return empty array' do 
          response = perform(@query_string, @context)
          assert_empty response["data"]["contractExpenses"]
        end
      end
      
      context 'when there are contract expenses' do
        setup do 
          @contract_expense1 = create(:contract_expense)
          @contract_expense2 = create(:contract_expense)
        end
        
        should 'return multiple contract expenses' do 
          response = perform(@query_string, @context)
          assert response["data"]["contractExpenses"]
          assert_operator response["data"]["contractExpenses"].length, :>, 1
          assert_equal response["data"]["contractExpenses"].length, ContractExpense.all.length
        end 

        should 'return attrs by contract expense' do 
          response = perform(@query_string, @context)
          assert_equal response["data"]["contractExpenses"][0]["name"], @contract_expense1.name
          assert_equal response["data"]["contractExpenses"][0]["value"], @contract_expense1.value
          assert_equal response["data"]["contractExpenses"][0]["id"].to_i, @contract_expense1.id
          assert_equal response["data"]["contractExpenses"][0]["contractPlan"]["id"].to_i, @contract_expense1.contract_plan.id
          assert_equal response["data"]["contractExpenses"][0]["contractPlan"]["duration"], @contract_expense1.contract_plan.duration
          assert_equal response["data"]["contractExpenses"][0]["contractPlan"]["rentUpdatePeriod"], @contract_expense1.contract_plan.rent_update_period
          assert_equal response["data"]["contractExpenses"][0]["contractPlan"]["rentValue"], @contract_expense1.contract_plan.rent_value
          assert_equal response["data"]["contractExpenses"][0]["contractPlan"]["property"]["propertyType"], @contract_expense1.contract_plan.property.property_type
          assert_equal response["data"]["contractExpenses"][0]["contractPlan"]["property"]["state"], @contract_expense1.contract_plan.property.state
          assert_equal response["data"]["contractExpenses"][0]["contractPlan"]["property"]["description"], @contract_expense1.contract_plan.property.description
          assert_equal response["data"]["contractExpenses"][1]["name"], @contract_expense2.name
          assert_equal response["data"]["contractExpenses"][1]["value"], @contract_expense2.value
          assert_equal response["data"]["contractExpenses"][1]["id"].to_i, @contract_expense2.id
          assert_equal response["data"]["contractExpenses"][1]["contractPlan"]["id"].to_i, @contract_expense2.contract_plan.id
          assert_equal response["data"]["contractExpenses"][1]["contractPlan"]["duration"], @contract_expense2.contract_plan.duration
          assert_equal response["data"]["contractExpenses"][1]["contractPlan"]["rentUpdatePeriod"], @contract_expense2.contract_plan.rent_update_period
          assert_equal response["data"]["contractExpenses"][1]["contractPlan"]["rentValue"], @contract_expense2.contract_plan.rent_value
          assert_equal response["data"]["contractExpenses"][1]["contractPlan"]["property"]["propertyType"], @contract_expense2.contract_plan.property.property_type
          assert_equal response["data"]["contractExpenses"][1]["contractPlan"]["property"]["state"], @contract_expense2.contract_plan.property.state
          assert_equal response["data"]["contractExpenses"][1]["contractPlan"]["property"]["description"], @contract_expense2.contract_plan.property.description
        end
      end  
    end 

    context 'when ask for only 1 attr' do 
      setup do 
        @query_string = <<-GRAPHQL
          query {
            contractExpenses {
              name
            }
          }
        GRAPHQL
      end
      
      context 'when there arent contract expenses' do 
        should 'return empty array' do 
          response = perform(@query_string, @context)
          assert_empty response["data"]["contractExpenses"]
        end
      end
      
      context 'when there are contract expenses' do
        setup do 
          create(:contract_expense)
          create(:contract_expense)
        end

        should 'return only 1 attr by contract' do 
          response = perform(@query_string, @context)
          assert response["data"]["contractExpenses"][0]["name"]
          assert_nil response["data"]["contractExpenses"][0]["id"]
          assert response["data"]["contractExpenses"][1]["name"]
          assert_nil response["data"]["contractExpenses"][1]["id"]
        end
      end
    end 

    context 'when use filters' do 
      setup do 
        @name = 'Some contract expense'
        @query_string = <<-GRAPHQL
          query {
            contractExpenses(filters: { name: "#{@name}" } ) {
              id
              name
              value
            }
          }
        GRAPHQL
      end 

      context 'when contract expenses dont match filter' do 
        setup do 
          create(:contract_expense, name: "Random")
        end 

        should 'return empty array' do 
          response = perform(@query_string, @context)
          assert_empty response["data"]["contractExpenses"]
        end
      end

      context 'when there are contract expenses matching filter' do 
        setup do 
          create(:contract_expense, name: @name)
          create(:contract_expense, name: "Asd")
          create(:contract_expense, name: "Qwe")
        end 
        
        should 'return only correct contract expenses' do 
          response = perform(@query_string, @context)
          assert response["data"]["contractExpenses"]
          assert_equal response["data"]["contractExpenses"].length, 1
          assert response["data"]["contractExpenses"][0]["id"]
          assert_equal response["data"]["contractExpenses"][0]["name"], @name
          assert response["data"]["contractExpenses"][0]["value"]
        end
      end
    end
  end

  context 'when current user has admin permissions' do
    setup do 
      @current_user = create(:user_with_role_admin)
      @context = { 
        current_user: @current_user,
        current_ability: Ability.new(@current_user)
      }
    end 

    context 'when ask for multiples attrs' do 
      setup do 
        @query_string = <<-GRAPHQL
          query {
            contractExpenses {
              id
              name
              value
              contractPlan {
                id
                duration
                rentUpdatePeriod
                rentValue
                property {
                  id
                  propertyType
                  state
                  description
                }
              }
            }
          }
        GRAPHQL
      end 

      context 'when there arent contract expenses' do 
        should 'return empty array' do 
          response = perform(@query_string, @context)
          assert_empty response["data"]["contractExpenses"]
        end
      end
      
      context 'when there are contract expenses' do
        setup do 
          @contract_expense1 = create(:contract_expense)
          @contract_expense2 = create(:contract_expense)
        end

        context 'when there arent contract expenses related to user' do 
          should 'return empty array' do 
            response = perform(@query_string, @context)
            assert_empty response["data"]["contractExpenses"]
          end
        end 
      
        context 'when there are contract expenses related to user' do 
          setup do 
            create(:user_agency, agency: @contract_expense1.contract_plan.property.agency, user: @current_user)
            @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
          end 
        
          should 'return only related contract expenses' do 
            response = perform(@query_string, @context)
            assert response["data"]["contractExpenses"]
            assert_equal response["data"]["contractExpenses"].length, 1
          end
        end 
      
        context 'when all contract expenses are related to user' do 
          setup do 
            create(:user_agency, agency: @contract_expense1.contract_plan.property.agency, user: @current_user)
            create(:user_agency, agency: @contract_expense2.contract_plan.property.agency, user: @current_user)
            @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
          end 

          should 'return multiple contract expenses' do 
            response = perform(@query_string, @context)
            assert response["data"]["contractExpenses"]
            assert_operator response["data"]["contractExpenses"].length, :>, 1
            assert_equal response["data"]["contractExpenses"].length, ContractExpense.all.length
          end 

          should 'return attrs by contract expense' do 
            response = perform(@query_string, @context)
            contract_expenses = response["data"]["contractExpenses"].sort_by { |x| x["id"].to_i }

            assert_equal contract_expenses[0]["name"], @contract_expense1.name
            assert_equal contract_expenses[0]["value"], @contract_expense1.value
            assert_equal contract_expenses[0]["id"].to_i, @contract_expense1.id
            assert_equal contract_expenses[0]["contractPlan"]["id"].to_i, @contract_expense1.contract_plan.id
            assert_equal contract_expenses[0]["contractPlan"]["duration"], @contract_expense1.contract_plan.duration
            assert_equal contract_expenses[0]["contractPlan"]["rentUpdatePeriod"], @contract_expense1.contract_plan.rent_update_period
            assert_equal contract_expenses[0]["contractPlan"]["rentValue"], @contract_expense1.contract_plan.rent_value
            assert_equal contract_expenses[0]["contractPlan"]["property"]["propertyType"], @contract_expense1.contract_plan.property.property_type
            assert_equal contract_expenses[0]["contractPlan"]["property"]["state"], @contract_expense1.contract_plan.property.state
            assert_equal contract_expenses[0]["contractPlan"]["property"]["description"], @contract_expense1.contract_plan.property.description

            assert_equal contract_expenses[1]["name"], @contract_expense2.name
            assert_equal contract_expenses[1]["value"], @contract_expense2.value
            assert_equal contract_expenses[1]["id"].to_i, @contract_expense2.id
            assert_equal contract_expenses[1]["contractPlan"]["id"].to_i, @contract_expense2.contract_plan.id
            assert_equal contract_expenses[1]["contractPlan"]["duration"], @contract_expense2.contract_plan.duration
            assert_equal contract_expenses[1]["contractPlan"]["rentUpdatePeriod"], @contract_expense2.contract_plan.rent_update_period
            assert_equal contract_expenses[1]["contractPlan"]["rentValue"], @contract_expense2.contract_plan.rent_value
            assert_equal contract_expenses[1]["contractPlan"]["property"]["propertyType"], @contract_expense2.contract_plan.property.property_type
            assert_equal contract_expenses[1]["contractPlan"]["property"]["state"], @contract_expense2.contract_plan.property.state
            assert_equal contract_expenses[1]["contractPlan"]["property"]["description"], @contract_expense2.contract_plan.property.description
          end
        end
      end  
    end 

    context 'when ask for only 1 attr' do 
      setup do 
        @query_string = <<-GRAPHQL
          query {
            contractExpenses {
              name
            }
          }
        GRAPHQL
      end
      
      context 'when there arent contract expenses' do 
        should 'return empty array' do 
          response = perform(@query_string, @context)
          assert_empty response["data"]["contractExpenses"]
        end
      end
      
      context 'when there are contract expenses' do
        setup do 
          @contract_expense1 = create(:contract_expense)
          @contract_expense2 = create(:contract_expense)
        end

        context 'when there are contract expenses related to user' do 
          setup do 
            create(:user_agency, agency: @contract_expense1.contract_plan.property.agency, user: @current_user)
            @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
          end 
        
          should 'return only 1 attr by each related contract expense' do 
            response = perform(@query_string, @context)
            assert response["data"]["contractExpenses"]
            assert_equal response["data"]["contractExpenses"].length, 1
            assert response["data"]["contractExpenses"][0]["name"]
            assert_nil response["data"]["contractExpenses"][0]["id"]
          end
        end 
  
        context 'when all contract expenses are related to user' do 
          setup do 
            create(:user_agency, agency: @contract_expense1.contract_plan.property.agency, user: @current_user)
            create(:user_agency, agency: @contract_expense2.contract_plan.property.agency, user: @current_user)
            @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
          end 

          should 'return only 1 attr by contract expense' do 
            response = perform(@query_string, @context)
            assert response["data"]["contractExpenses"][0]["name"]
            assert_nil response["data"]["contractExpenses"][0]["id"]
            assert response["data"]["contractExpenses"][1]["name"]
            assert_nil response["data"]["contractExpenses"][1]["id"]
          end
        end
      end
    end 

    context 'when use filters' do 
      setup do 
        @name = 'Some contract expense'
        @query_string = <<-GRAPHQL
          query {
            contractExpenses(filters: { name: "#{@name}" } ) {
              id
              name
              value
            }
          }
        GRAPHQL
      end 

      context 'when contract expenses dont match filter' do 
        setup do 
          create(:contract_expense, name: "Random")
        end 

        should 'return empty array' do 
          response = perform(@query_string, @context)
          assert_empty response["data"]["contractExpenses"]
        end
      end

      context 'when there are contract expenses matching filter' do 
        setup do 
          @contract_expense1 = create(:contract_expense, name: @name)
          @contract_expense2 = create(:contract_expense, name: "Asd")
          @contract_expense3 = create(:contract_expense, name: @name)
        end 

        context 'when there arent related to user' do 
          should 'return empty array' do 
            response = perform(@query_string, @context)
            assert_empty response["data"]["contractExpenses"]
          end
        end 

        context 'when there are contract expenses related to user' do 
          setup do 
            create(:user_agency, agency: @contract_expense1.contract_plan.property.agency, user: @current_user)
            @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
          end 
        
          should 'return only related and filtered contract expenses' do 
            response = perform(@query_string, @context)
            assert response["data"]["contractExpenses"]
            assert_equal response["data"]["contractExpenses"].length, 1
            assert response["data"]["contractExpenses"][0]["id"]
            assert_equal response["data"]["contractExpenses"][0]["name"], @name
            assert response["data"]["contractExpenses"][0]["value"]
          end
        end
      end
    end
  end
end