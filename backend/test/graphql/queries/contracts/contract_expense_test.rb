require 'test_helper'

class Queries::ContractExpenseTest < ActiveSupport::TestCase
  def perform(query, context = {})
    BackendSchema.execute(query, context: context).to_h
  end

  context 'when current user has gendev permissions' do
    setup do 
      @current_user = create(:user_with_role_gendev)
      @context = { 
        current_user: @current_user,
        current_ability: Ability.new(@current_user)
      }
    end 

    context 'when there arent contract expenses' do 
      setup do 
        @query_string = <<-GRAPHQL
          query {
            contractExpense(id: 1) {
              id
              name
              value
              contractPlan {
                id
                duration
                rentUpdatePeriod
                rentValue
                property {
                  id
                  propertyType
                  state
                  description
                }
              }
            }
          }
        GRAPHQL
      end 

      should 'return nil' do 
        response = perform(@query_string, @context)
        assert_nil response["data"]["contractExpense"]
      end
    end


    context 'when there are contract expenses' do
      setup do 
        @contract_expense1 = create(:contract_expense)
      end  

      context 'when ask for multiple attrs' do 
        context 'when contract expense doesnt exist' do 
          setup do 
            @query_string = <<-GRAPHQL
              query {
                contractExpense(id: #{@contract_expense1.id + 1}) {
                  id
                  name
                  value
                  contractPlan {
                    id
                    duration
                    rentUpdatePeriod
                    rentValue
                    property {
                      id
                      propertyType
                      state
                      description
                    }
                  }
                }
              }
            GRAPHQL
          end

          should 'return nil' do 
            response = perform(@query_string, @context)
            assert_nil response["data"]["contract"]
          end
        end

        context 'when contract expense exists' do
          setup do 
            @query_string = <<-GRAPHQL
              query {
                contractExpense(id: #{@contract_expense1.id}) {
                  id
                  name
                  value
                  contractPlan {
                    id
                    duration
                    rentUpdatePeriod
                    rentValue
                    property {
                      id
                      propertyType
                      state
                      description
                    }
                  }
                }
              }
            GRAPHQL
          end

          should 'return attrs by contract expense' do 
            response = perform(@query_string, @context)
            assert_equal response["data"]["contractExpense"]["name"], @contract_expense1.name
            assert_equal response["data"]["contractExpense"]["value"], @contract_expense1.value
            assert_equal response["data"]["contractExpense"]["id"].to_i, @contract_expense1.id
            assert_equal response["data"]["contractExpense"]["contractPlan"]["id"].to_i, @contract_expense1.contract_plan.id
            assert_equal response["data"]["contractExpense"]["contractPlan"]["duration"], @contract_expense1.contract_plan.duration
            assert_equal response["data"]["contractExpense"]["contractPlan"]["rentUpdatePeriod"], @contract_expense1.contract_plan.rent_update_period
            assert_equal response["data"]["contractExpense"]["contractPlan"]["rentValue"], @contract_expense1.contract_plan.rent_value
            assert_equal response["data"]["contractExpense"]["contractPlan"]["property"]["id"].to_i, @contract_expense1.contract_plan.property.id
            assert_equal response["data"]["contractExpense"]["contractPlan"]["property"]["propertyType"], @contract_expense1.contract_plan.property.property_type
            assert_equal response["data"]["contractExpense"]["contractPlan"]["property"]["state"], @contract_expense1.contract_plan.property.state
            assert_equal response["data"]["contractExpense"]["contractPlan"]["property"]["description"], @contract_expense1.contract_plan.property.description
          end
        end 
      end 

      context 'when ask for only 1 attr' do 
        context 'when contract expense doesnt exist' do 
          setup do 
            @query_string = <<-GRAPHQL
              query {
                contractExpense(id: #{@contract_expense1.id + 1}) {
                  name
                }
              }
            GRAPHQL
          end

          should 'return nil' do 
            response = perform(@query_string, @context)
            assert_nil response["data"]["contractExpense"]
          end
        end

        context 'when contract expense exists' do
          setup do 
            @query_string = <<-GRAPHQL
              query {
                contractExpense(id: #{@contract_expense1.id}) {
                  name
                }
              }
            GRAPHQL
          end

          should 'return only 1 attr for specific contractExpense' do 
            response = perform(@query_string, @context)
            assert_equal response["data"]["contractExpense"]["name"], @contract_expense1.name
            assert_nil response["data"]["contractExpense"]["id"]
          end
        end 
      end 
    end
  end

  context 'when current user has admin permissions' do
    setup do 
      @current_user = create(:user_with_role_admin)
      @context = { 
        current_user: @current_user,
        current_ability: Ability.new(@current_user)
      }
    end 

    context 'when there arent contract expenses' do 
      setup do 
        @query_string = <<-GRAPHQL
          query {
            contractExpense(id: 1) {
              id
              name
              value
              contractPlan {
                id
                duration
                rentUpdatePeriod
                rentValue
                property {
                  id
                  propertyType
                  state
                  description
                }
              }
            }
          }
        GRAPHQL
      end 

      should 'return nil' do 
        response = perform(@query_string, @context)
        assert_nil response["data"]["contractExpense"]
      end
    end


    context 'when there are contract expenses' do
      setup do 
        @contract_expense1 = create(:contract_expense)
      end  

      context 'when ask for multiple attrs' do 
        context 'when contract expense doesnt exist' do 
          setup do 
            @query_string = <<-GRAPHQL
              query {
                contractExpense(id: #{@contract_expense1.id + 1}) {
                  id
                  name
                  value
                  contractPlan {
                    id
                    duration
                    rentUpdatePeriod
                    rentValue
                    property {
                      id
                      propertyType
                      state
                      description
                    }
                  }
                }
              }
            GRAPHQL
          end

          should 'return nil' do 
            response = perform(@query_string, @context)
            assert_nil response["data"]["contract"]
          end
        end

        context 'when contract expense exists' do
          setup do 
            @query_string = <<-GRAPHQL
              query {
                contractExpense(id: #{@contract_expense1.id}) {
                  id
                  name
                  value
                  contractPlan {
                    id
                    duration
                    rentUpdatePeriod
                    rentValue
                    property {
                      id
                      propertyType
                      state
                      description
                    }
                  }
                }
              }
            GRAPHQL
          end

          context 'when isnt related to user' do 
            should 'return "unauthorized" message' do 
              response = perform(@query_string, @context)
              assert_nil response['data']['contractExpense']
              assert_equal response['errors'][0]['message'], 'user unauthorized'
            end
          end 
        
          context 'when is related to user' do 
            setup do 
              create(:user_agency, agency: @contract_expense1.contract_plan.property.agency, user: @current_user)
              @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
            end 

            should 'return attrs by contract expense' do 
              response = perform(@query_string, @context)
              assert_equal response["data"]["contractExpense"]["name"], @contract_expense1.name
              assert_equal response["data"]["contractExpense"]["value"], @contract_expense1.value
              assert_equal response["data"]["contractExpense"]["id"].to_i, @contract_expense1.id
              assert_equal response["data"]["contractExpense"]["contractPlan"]["id"].to_i, @contract_expense1.contract_plan.id
              assert_equal response["data"]["contractExpense"]["contractPlan"]["duration"], @contract_expense1.contract_plan.duration
              assert_equal response["data"]["contractExpense"]["contractPlan"]["rentUpdatePeriod"], @contract_expense1.contract_plan.rent_update_period
              assert_equal response["data"]["contractExpense"]["contractPlan"]["rentValue"], @contract_expense1.contract_plan.rent_value
              assert_equal response["data"]["contractExpense"]["contractPlan"]["property"]["id"].to_i, @contract_expense1.contract_plan.property.id
              assert_equal response["data"]["contractExpense"]["contractPlan"]["property"]["propertyType"], @contract_expense1.contract_plan.property.property_type
              assert_equal response["data"]["contractExpense"]["contractPlan"]["property"]["state"], @contract_expense1.contract_plan.property.state
              assert_equal response["data"]["contractExpense"]["contractPlan"]["property"]["description"], @contract_expense1.contract_plan.property.description
            end
          end
        end 
      end 

      context 'when ask for only 1 attr' do 
        context 'when contract expense doesnt exist' do 
          setup do 
            @query_string = <<-GRAPHQL
              query {
                contractExpense(id: #{@contract_expense1.id + 1}) {
                  name
                }
              }
            GRAPHQL
          end

          should 'return nil' do 
            response = perform(@query_string, @context)
            assert_nil response["data"]["contractExpense"]
          end
        end

        context 'when contract expense exists' do
          setup do 
            @query_string = <<-GRAPHQL
              query {
                contractExpense(id: #{@contract_expense1.id}) {
                  name
                }
              }
            GRAPHQL
          end

          context 'when isnt related to user' do 
            should 'return "unauthorized" message' do 
              response = perform(@query_string, @context)
              assert_nil response['data']['contractExpense']
              assert_equal response['errors'][0]['message'], 'user unauthorized'
            end
          end 
        
          context 'when is related to user' do 
            setup do 
              create(:user_agency, agency: @contract_expense1.contract_plan.property.agency, user: @current_user)
              @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
            end 

            should 'return only 1 attr for specific contractExpense' do 
              response = perform(@query_string, @context)
              assert_equal response["data"]["contractExpense"]["name"], @contract_expense1.name
              assert_nil response["data"]["contractExpense"]["id"]
            end
          end 
        end 
      end 
    end
  end
end