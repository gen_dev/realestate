require 'test_helper'

class Queries::ContractPlansTest < ActiveSupport::TestCase
  def perform(query, context = {})
    BackendSchema.execute(query, context: context).to_h
  end

  context 'when current user has gendev permissions' do
    setup do 
      @current_user = create(:user_with_role_gendev)
      @context = { 
        current_user: @current_user,
        current_ability: Ability.new(@current_user)
      }
    end 

    context 'when ask for multiples attrs' do 
      setup do 
        @query_string = <<-GRAPHQL
          query {
            contractPlans {
              id
              duration
              rentUpdatePeriod
              rentValue
              property {
                id
                propertyType
                state
                description
              }
            }
          }
        GRAPHQL
      end 

      context 'when there arent contract plans' do 
        should 'return empty array' do 
          response = perform(@query_string, @context)
          assert_empty response["data"]["contractPlans"]
        end
      end
      
      context 'when there are contract plans' do
        setup do 
          @contract_plan1 = create(:contract_plan)
          @contract_plan2 = create(:contract_plan)
        end
        
        should 'return multiple contract plans' do 
          response = perform(@query_string, @context)
          assert response["data"]["contractPlans"]
          assert_operator response["data"]["contractPlans"].length, :>, 1
          assert_equal response["data"]["contractPlans"].length, ContractPlan.all.length
        end 

        should 'return attrs by contract plan' do 
          response = perform(@query_string, @context)
          assert_equal response["data"]["contractPlans"][0]["id"].to_i, @contract_plan1.id
          assert_equal response["data"]["contractPlans"][0]["duration"], @contract_plan1.duration
          assert_equal response["data"]["contractPlans"][0]["rentUpdatePeriod"], @contract_plan1.rent_update_period
          assert_equal response["data"]["contractPlans"][0]["rentValue"], @contract_plan1.rent_value
          assert_equal response["data"]["contractPlans"][0]["property"]["propertyType"], @contract_plan1.property.property_type
          assert_equal response["data"]["contractPlans"][0]["property"]["state"], @contract_plan1.property.state
          assert_equal response["data"]["contractPlans"][0]["property"]["description"], @contract_plan1.property.description
          assert_equal response["data"]["contractPlans"][1]["id"].to_i, @contract_plan2.id
          assert_equal response["data"]["contractPlans"][1]["duration"], @contract_plan2.duration
          assert_equal response["data"]["contractPlans"][1]["rentUpdatePeriod"], @contract_plan2.rent_update_period
          assert_equal response["data"]["contractPlans"][1]["rentValue"], @contract_plan2.rent_value
          assert_equal response["data"]["contractPlans"][1]["property"]["propertyType"], @contract_plan2.property.property_type
          assert_equal response["data"]["contractPlans"][1]["property"]["state"], @contract_plan2.property.state
          assert_equal response["data"]["contractPlans"][1]["property"]["description"], @contract_plan2.property.description
        end
      end  
    end 

    context 'when ask for only 1 attr' do 
      setup do 
        @query_string = <<-GRAPHQL
          query {
            contractPlans {
              duration
            }
          }
        GRAPHQL
      end
      
      context 'when there arent contract plans' do 
        should 'return empty array' do 
          response = perform(@query_string, @context)
          assert_empty response["data"]["contractPlans"]
        end
      end
      
      context 'when there are contract plans' do
        setup do 
          create(:contract_plan)
          create(:contract_plan)
        end

        should 'return only 1 attr by contract' do 
          response = perform(@query_string, @context)
          assert response["data"]["contractPlans"][0]["duration"]
          assert_nil response["data"]["contractPlans"][0]["id"]
          assert response["data"]["contractPlans"][1]["duration"]
          assert_nil response["data"]["contractPlans"][1]["id"]
        end
      end
    end 

    context 'when use filters' do 
      setup do 
        @duration = 12
        @query_string = <<-GRAPHQL
          query {
            contractPlans(filters: { duration: #{@duration} } ) {
              id
              duration
              rentUpdatePeriod
              rentValue
            }
          }
        GRAPHQL
      end 

      context 'when contract plans dont match filter' do 
        setup do 
          create(:contract_plan, duration: 36)
        end 

        should 'return empty array' do 
          response = perform(@query_string, @context)
          assert_empty response["data"]["contractPlans"]
        end
      end

      context 'when there are contract plans matching filter' do 
        setup do 
          create(:contract_plan, duration: @duration)
          create(:contract_plan, duration: 24)
          create(:contract_plan, duration: 6)
        end 
        
        should 'return only correct contract plans' do 
          response = perform(@query_string, @context)
          assert response["data"]["contractPlans"]
          assert_equal response["data"]["contractPlans"].length, 1
          assert response["data"]["contractPlans"][0]["id"]
          assert_equal response["data"]["contractPlans"][0]["duration"], @duration
          assert response["data"]["contractPlans"][0]["rentUpdatePeriod"]
          assert response["data"]["contractPlans"][0]["rentValue"]
        end
      end
    end
  end

  context 'when current user has admin permissions' do
    setup do 
      @current_user = create(:user_with_role_admin)
      @context = { 
        current_user: @current_user,
        current_ability: Ability.new(@current_user)
      }
    end 

    context 'when ask for multiples attrs' do 
      setup do 
        @query_string = <<-GRAPHQL
          query {
            contractPlans {
              id
              duration
              rentUpdatePeriod
              rentValue
              property {
                id
                propertyType
                state
                description
              }
            }
          }
        GRAPHQL
      end 

      context 'when there arent contract plans' do 
        should 'return empty array' do 
          response = perform(@query_string, @context)
          assert_empty response["data"]["contractPlans"]
        end
      end
      
      context 'when there are contract plans' do
        setup do 
          @contract_plan1 = create(:contract_plan)
          @contract_plan2 = create(:contract_plan)
        end

        context 'when there are contract plans related to user' do 
          setup do 
            create(:user_agency, agency: @contract_plan1.property.agency, user: @current_user)
            @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
          end 
        
          should 'return only related contract plans' do 
            response = perform(@query_string, @context)
            assert response["data"]["contractPlans"]
            assert_equal response["data"]["contractPlans"].length, 1
          end
        end 
      
        context 'when all contract plans are related to user' do 
          setup do 
            create(:user_agency, agency: @contract_plan1.property.agency, user: @current_user)
            create(:user_agency, agency: @contract_plan2.property.agency, user: @current_user)
            @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
          end 
          
          should 'return multiple contract plans' do 
            response = perform(@query_string, @context)
            assert response["data"]["contractPlans"]
            assert_operator response["data"]["contractPlans"].length, :>, 1
            assert_equal response["data"]["contractPlans"].length, ContractPlan.all.length
          end 

          should 'return attrs by contract plan' do 
            response = perform(@query_string, @context)
            contract_plans = response["data"]["contractPlans"].sort_by { |x| x["id"].to_i }
            
            assert_equal contract_plans[0]["id"].to_i, @contract_plan1.id
            assert_equal contract_plans[0]["duration"], @contract_plan1.duration
            assert_equal contract_plans[0]["rentUpdatePeriod"], @contract_plan1.rent_update_period
            assert_equal contract_plans[0]["rentValue"], @contract_plan1.rent_value
            assert_equal contract_plans[0]["property"]["propertyType"], @contract_plan1.property.property_type
            assert_equal contract_plans[0]["property"]["state"], @contract_plan1.property.state
            assert_equal contract_plans[0]["property"]["description"], @contract_plan1.property.description

            assert_equal contract_plans[1]["id"].to_i, @contract_plan2.id
            assert_equal contract_plans[1]["duration"], @contract_plan2.duration
            assert_equal contract_plans[1]["rentUpdatePeriod"], @contract_plan2.rent_update_period
            assert_equal contract_plans[1]["rentValue"], @contract_plan2.rent_value
            assert_equal contract_plans[1]["property"]["propertyType"], @contract_plan2.property.property_type
            assert_equal contract_plans[1]["property"]["state"], @contract_plan2.property.state
            assert_equal contract_plans[1]["property"]["description"], @contract_plan2.property.description
          end
        end  
      end  
    end 

    context 'when ask for only 1 attr' do 
      setup do 
        @query_string = <<-GRAPHQL
          query {
            contractPlans {
              duration
            }
          }
        GRAPHQL
      end
      
      context 'when there arent contract plans' do 
        should 'return empty array' do 
          response = perform(@query_string, @context)
          assert_empty response["data"]["contractPlans"]
        end
      end
      
      context 'when there are contract plans' do
        setup do 
          @contract_plan1 = create(:contract_plan)
          @contract_plan2 = create(:contract_plan)
        end

        context 'when there are contract plans related to user' do 
          setup do 
            create(:user_agency, agency: @contract_plan1.property.agency, user: @current_user)
            @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
          end 
        
          should 'return only 1 attr by each related contract plan' do 
            response = perform(@query_string, @context)
            assert response["data"]["contractPlans"]
            assert_equal response["data"]["contractPlans"].length, 1
            assert response["data"]["contractPlans"][0]["duration"]
            assert_nil response["data"]["contractPlans"][0]["id"]
          end
        end 
  
        context 'when all contract plans are related to user' do 
          setup do 
            create(:user_agency, agency: @contract_plan1.property.agency, user: @current_user)
            create(:user_agency, agency: @contract_plan2.property.agency, user: @current_user)
            @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
          end 

          should 'return only 1 attr by contract' do 
            response = perform(@query_string, @context)
            assert response["data"]["contractPlans"][0]["duration"]
            assert_nil response["data"]["contractPlans"][0]["id"]
            assert response["data"]["contractPlans"][1]["duration"]
            assert_nil response["data"]["contractPlans"][1]["id"]
          end
        end
      end
    end 

    context 'when use filters' do 
      setup do 
        @duration = 12
        @query_string = <<-GRAPHQL
          query {
            contractPlans(filters: { duration: #{@duration} } ) {
              id
              duration
              rentUpdatePeriod
              rentValue
            }
          }
        GRAPHQL
      end 

      context 'when contract plans dont match filter' do 
        setup do 
          create(:contract_plan, duration: 36)
        end 

        should 'return empty array' do 
          response = perform(@query_string, @context)
          assert_empty response["data"]["contractPlans"]
        end
      end

      context 'when there are contract plans matching filter' do 
        setup do 
          @contract_plan1 = create(:contract_plan, duration: @duration)
          @contract_plan2 = create(:contract_plan, duration: 24)
          @contract_plan3 = create(:contract_plan, duration: @duration)
        end 
        
        context 'when there arent related to user' do 
          should 'return empty array' do 
            response = perform(@query_string, @context)
            assert_empty response["data"]["contractPlans"]
          end
        end 

        context 'when there are contract plans related to user' do 
          setup do 
            create(:user_agency, agency: @contract_plan1.property.agency, user: @current_user)
            @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
          end 
        
          should 'return only related and filtered contract plans' do 
            response = perform(@query_string, @context)
            assert response["data"]["contractPlans"]
            assert_equal response["data"]["contractPlans"].length, 1
            assert response["data"]["contractPlans"][0]["id"]
            assert_equal response["data"]["contractPlans"][0]["duration"], @duration
            assert response["data"]["contractPlans"][0]["rentUpdatePeriod"]
            assert response["data"]["contractPlans"][0]["rentValue"]
          end
        end
      end
    end
  end
end