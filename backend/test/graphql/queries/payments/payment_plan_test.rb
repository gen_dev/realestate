require 'test_helper'

class Queries::PaymentPlanTest < ActiveSupport::TestCase
  def perform(query, context = {})
    BackendSchema.execute(query, context: context).to_h
  end

  context 'when current user has gendev permissions' do
    setup do 
      @current_user = create(:user_with_role_gendev)
      @context = { 
        current_user: @current_user,
        current_ability: Ability.new(@current_user)
      }
    end 

    context 'when there arent payment plans' do 
      setup do 
        @query_string = <<-GRAPHQL
          query {
            paymentPlan(id: 1) {
              paymentMethods
              contract {
                id
                state
                contractType
                property {
                  id
                  propertyType
                  state
                  description
                }
              }
            }
          }
        GRAPHQL
      end 

      should 'return nil' do 
        response = perform(@query_string, @context)
        assert_nil response["data"]["paymentPlan"]
      end
    end


    context 'when there are payment plans' do
      setup do 
        @payment_plan1 = create(:payment_plan)
      end  

      context 'when ask for multiple attrs' do 
        context 'when payment plan doesnt exist' do 
          setup do 
            @query_string = <<-GRAPHQL
              query {
                paymentPlan(id: #{@payment_plan1.id + 1}) {
                  paymentMethods
                  contract {
                    id
                    state
                    contractType
                    property {
                      id
                      propertyType
                      state
                      description
                    }
                  }
                }
              }
            GRAPHQL
          end

          should 'return nil' do 
            response = perform(@query_string, @context)
            assert_nil response["data"]["paymentPlan"]
          end
        end

        context 'when payment plan exists' do
          setup do 
            @query_string = <<-GRAPHQL
              query {
                paymentPlan(id: #{@payment_plan1.id}) {
                  id
                  paymentMethods
                  contract {
                    id
                    state
                    contractType
                    property {
                      id
                      propertyType
                      state
                      description
                    }
                  }
                }
              }
            GRAPHQL
          end

          should 'return only attrs by payment plan' do 
            response = perform(@query_string, @context)
            assert_equal response["data"]["paymentPlan"]["id"].to_i, @payment_plan1.id
            assert_equal response["data"]["paymentPlan"]["paymentMethods"], @payment_plan1.payment_methods
            assert_equal response["data"]["paymentPlan"]["contract"]["id"].to_i, @payment_plan1.contract.id
            assert_equal response["data"]["paymentPlan"]["contract"]["state"], @payment_plan1.contract.state
            assert_equal response["data"]["paymentPlan"]["contract"]["contractType"], @payment_plan1.contract.contract_type
            assert_equal response["data"]["paymentPlan"]["contract"]["property"]["id"].to_i, @payment_plan1.contract.property.id
            assert_equal response["data"]["paymentPlan"]["contract"]["property"]["propertyType"], @payment_plan1.contract.property.property_type
            assert_equal response["data"]["paymentPlan"]["contract"]["property"]["state"], @payment_plan1.contract.property.state
            assert_equal response["data"]["paymentPlan"]["contract"]["property"]["description"], @payment_plan1.contract.property.description
          end
        end 
      end 

      context 'when ask for only 1 attr' do 
        context 'when payment plan doesnt exist' do 
          setup do 
            @query_string = <<-GRAPHQL
              query {
                paymentPlan(id: #{@payment_plan1.id + 1}) {
                  paymentMethods
                  contract {
                    id
                    state
                    contractType
                    property {
                      id
                      propertyType
                      state
                      description
                    }
                  }
                }
              }
            GRAPHQL
          end

          should 'return nil' do 
            response = perform(@query_string, @context)
            assert_nil response["data"]["paymentPlan"]
          end
        end

        context 'when payment plan exists' do
          setup do 
            @query_string = <<-GRAPHQL
              query {
                paymentPlan(id: #{@payment_plan1.id}) {
                  paymentMethods
                }
              }
            GRAPHQL
          end

          should 'return only 1 attr for specific payment plan' do 
            response = perform(@query_string, @context)
            assert_nil response["data"]["paymentPlan"]["id"]
            assert_equal response["data"]["paymentPlan"]["paymentMethods"], @payment_plan1.payment_methods
          end
        end 
      end 
    end
  end

  context 'when current user has admin permissions' do
    setup do 
      @current_user = create(:user_with_role_admin)
      @context = { 
        current_user: @current_user,
        current_ability: Ability.new(@current_user)
      }
    end 

    context 'when there arent payment plans' do 
      setup do 
        @query_string = <<-GRAPHQL
          query {
            paymentPlan(id: 1) {
              paymentMethods
              contract {
                id
                state
                contractType
                property {
                  id
                  propertyType
                  state
                  description
                }
              }
            }
          }
        GRAPHQL
      end 

      should 'return nil' do 
        response = perform(@query_string, @context)
        assert_nil response["data"]["paymentPlan"]
      end
    end

    context 'when there are payment plans' do
      setup do 
        @payment_plan1 = create(:payment_plan)
      end  

      context 'when ask for multiple attrs' do 
        context 'when payment plan doesnt exist' do 
          setup do 
            @query_string = <<-GRAPHQL
              query {
                paymentPlan(id: #{@payment_plan1.id + 1}) {
                  paymentMethods
                  contract {
                    id
                    state
                    contractType
                    property {
                      id
                      propertyType
                      state
                      description
                    }
                  }
                }
              }
            GRAPHQL
          end

          should 'return nil' do 
            response = perform(@query_string, @context)
            assert_nil response["data"]["paymentPlan"]
          end
        end

        context 'when payment plan exists' do
          setup do 
            @query_string = <<-GRAPHQL
              query {
                paymentPlan(id: #{@payment_plan1.id}) {
                  id
                  paymentMethods
                  contract {
                    id
                    state
                    contractType
                    property {
                      id
                      propertyType
                      state
                      description
                    }
                  }
                }
              }
            GRAPHQL
          end

          context 'when isnt related to user' do 
            should 'return "unauthorized" message' do 
              response = perform(@query_string, @context)
              assert_nil response['data']['paymentPlan']
              assert_equal response['errors'][0]['message'], 'user unauthorized'
            end
          end 
          
          context 'when is related to user' do 
            setup do 
              create(:user_agency, agency: @payment_plan1.contract.property.agency, user: @current_user)
              @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
            end

            should 'return only attrs by payment plan' do 
              response = perform(@query_string, @context)
              assert_equal response["data"]["paymentPlan"]["id"].to_i, @payment_plan1.id
              assert_equal response["data"]["paymentPlan"]["paymentMethods"], @payment_plan1.payment_methods
              assert_equal response["data"]["paymentPlan"]["contract"]["id"].to_i, @payment_plan1.contract.id
              assert_equal response["data"]["paymentPlan"]["contract"]["state"], @payment_plan1.contract.state
              assert_equal response["data"]["paymentPlan"]["contract"]["contractType"], @payment_plan1.contract.contract_type
              assert_equal response["data"]["paymentPlan"]["contract"]["property"]["id"].to_i, @payment_plan1.contract.property.id
              assert_equal response["data"]["paymentPlan"]["contract"]["property"]["propertyType"], @payment_plan1.contract.property.property_type
              assert_equal response["data"]["paymentPlan"]["contract"]["property"]["state"], @payment_plan1.contract.property.state
              assert_equal response["data"]["paymentPlan"]["contract"]["property"]["description"], @payment_plan1.contract.property.description
            end
          end
        end 
      end 

      context 'when ask for only 1 attr' do 
        context 'when payment plan doesnt exist' do 
          setup do 
            @query_string = <<-GRAPHQL
              query {
                paymentPlan(id: #{@payment_plan1.id + 1}) {
                  paymentMethods
                }
              }
            GRAPHQL
          end

          should 'return nil' do 
            response = perform(@query_string, @context)
            assert_nil response["data"]["paymentPlan"]
          end
        end

        context 'when payment plan exists' do
          setup do 
            @query_string = <<-GRAPHQL
              query {
                paymentPlan(id: #{@payment_plan1.id}) {
                  paymentMethods
                }
              }
            GRAPHQL
          end

          context 'when isnt related to user' do 
            should 'return "unauthorized" message' do 
              response = perform(@query_string, @context)
              assert_nil response['data']['paymentPlan']
              assert_equal response['errors'][0]['message'], 'user unauthorized'
            end
          end 
          
          context 'when is related to user' do 
            setup do 
              create(:user_agency, agency: @payment_plan1.contract.property.agency, user: @current_user)
              @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
            end

            should 'return only 1 attr for specific payment plan' do 
              response = perform(@query_string, @context)
              assert_nil response["data"]["paymentPlan"]["id"]
              assert_equal response["data"]["paymentPlan"]["paymentMethods"], @payment_plan1.payment_methods
            end
          end
        end 
      end 
    end
  end
end