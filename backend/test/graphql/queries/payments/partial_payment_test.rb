require 'test_helper'

class Queries::PartialPaymentTest < ActiveSupport::TestCase
  def perform(query, context = {})
    BackendSchema.execute(query, context: context).to_h
  end

  context 'when current user has gendev permissions' do
    setup do 
      @current_user = create(:user_with_role_gendev)
      @context = { 
        current_user: @current_user,
        current_ability: Ability.new(@current_user)
      }
    end 
      
    context 'when there arent partial payments' do 
      setup do 
        @query_string = <<-GRAPHQL
          query {
            partialPayment(id: 1) {
              id
              value
              payment {
                id
                paymentType
                period
                concept
                state
                dueDate
                amount
                contract {
                  id
                  state
                  contractType
                  property {
                    id
                    propertyType
                    state
                    description
                  }
                }
                subAccount {
                  id
                  name
                  balance
                }
                referral {
                  ... on User {
                    id
                    email
                    name
                  }
                  ... on Agency {
                    id
                    name
                  }
                }
              }
            }
          }
        GRAPHQL
      end 

      should 'return nil' do 
        response = perform(@query_string, @context)
        assert_nil response["data"]["partialPayment"]
      end
    end


    context 'when there are partial payments' do
      setup do 
        @partial_payment1 = create(:partial_payment)
      end  

      context 'when ask for multiple attrs' do 
        context 'when partial payment doesnt exist' do 
          setup do 
            @query_string = <<-GRAPHQL
              query {
                partialPayment(id: #{@partial_payment1.id + 1}) {
                  id
                  value
                  payment {
                    id
                    paymentType
                    period
                    concept
                    state
                    dueDate
                    amount
                    contract {
                      id
                      state
                      contractType
                      property {
                        id
                        propertyType
                        state
                        description
                      }
                    }
                    subAccount {
                      id
                      name
                      balance
                    }
                    referral {
                      ... on User {
                        id
                        email
                        profile {
                          name
                        }
                      }
                      ... on Agency {
                        id
                        name
                      }
                    }
                  }
                }
              }
            GRAPHQL
          end

          should 'return nil' do 
            response = perform(@query_string, @context)
            assert_nil response["data"]["partialPayment"]
          end
        end

        context 'when payment exists' do
          setup do 
            @query_string = <<-GRAPHQL
              query {
                partialPayment(id: #{@partial_payment1.id}) {
                  id 
                  value
                  payment {
                    id
                    paymentType
                    period
                    concept
                    state
                    dueDate
                    amount
                    contract {
                      id
                      state
                      contractType
                      property {
                        id
                        propertyType
                        state
                        description
                      }
                    }
                    subAccount {
                      id
                      name
                      balance
                    }
                    referral {
                      ... on User {
                        id
                        email
                        profile {
                          name
                        }
                      }
                      ... on Agency {
                        id
                        name
                      }
                    }
                  }
                }
              }
            GRAPHQL
          end

          should 'return attrs by partial payment' do 
            response = perform(@query_string, @context)
            assert_equal response["data"]["partialPayment"]["id"].to_i, @partial_payment1.id
            assert_equal response["data"]["partialPayment"]["value"].to_f, @partial_payment1.value.to_f
            assert_equal response["data"]["partialPayment"]["payment"]["id"].to_i, @partial_payment1.payment.id
            assert_equal response["data"]["partialPayment"]["payment"]["paymentType"], @partial_payment1.payment.payment_type
            assert_equal response["data"]["partialPayment"]["payment"]["period"], @partial_payment1.payment.period
            assert_equal response["data"]["partialPayment"]["payment"]["concept"], @partial_payment1.payment.concept
            assert_equal response["data"]["partialPayment"]["payment"]["state"], @partial_payment1.payment.state
            assert_equal response["data"]["partialPayment"]["payment"]["dueDate"], @partial_payment1.payment.due_date.to_s
            assert_equal response["data"]["partialPayment"]["payment"]["amount"], @partial_payment1.payment.amount
            assert_equal response["data"]["partialPayment"]["payment"]["contract"]["id"].to_i, @partial_payment1.payment.contract.id
            assert_equal response["data"]["partialPayment"]["payment"]["contract"]["state"], @partial_payment1.payment.contract.state
            assert_equal response["data"]["partialPayment"]["payment"]["contract"]["contractType"], @partial_payment1.payment.contract.contract_type
            assert_equal response["data"]["partialPayment"]["payment"]["contract"]["property"]["id"].to_i, @partial_payment1.payment.contract.property.id
            assert_equal response["data"]["partialPayment"]["payment"]["contract"]["property"]["propertyType"], @partial_payment1.payment.contract.property.property_type
            assert_equal response["data"]["partialPayment"]["payment"]["contract"]["property"]["state"], @partial_payment1.payment.contract.property.state
            assert_equal response["data"]["partialPayment"]["payment"]["contract"]["property"]["description"], @partial_payment1.payment.contract.property.description
            assert_equal response["data"]["partialPayment"]["payment"]["subAccount"]["id"].to_i, @partial_payment1.payment.sub_account.id
            assert_equal response["data"]["partialPayment"]["payment"]["subAccount"]["name"], @partial_payment1.payment.sub_account.name
            assert_equal response["data"]["partialPayment"]["payment"]["subAccount"]["balance"], @partial_payment1.payment.sub_account.balance
            assert_equal response["data"]["partialPayment"]["payment"]["referral"]["id"].to_i, @partial_payment1.payment.referral.id
            assert_equal response["data"]["partialPayment"]["payment"]["referral"]["profile"]["name"], @partial_payment1.payment.referral.profile.name
          end
        end 
      end 

      context 'when ask for only 1 attr' do 
        context 'when partial payment doesnt exist' do 
          setup do 
            @query_string = <<-GRAPHQL
              query {
                partialPayment(id: #{@partial_payment1.id + 1}) {
                  value
                }
              }
            GRAPHQL
          end

          should 'return nil' do 
            response = perform(@query_string, @context)
            assert_nil response["data"]["partialPayment"]
          end
        end

        context 'when partial payment exists' do
          setup do 
            @query_string = <<-GRAPHQL
              query {
                partialPayment(id: #{@partial_payment1.id}) {
                  value
                }
              }
            GRAPHQL
          end

          should 'return only 1 attr for specific partial payment' do 
            response = perform(@query_string, @context)
            assert_equal response["data"]["partialPayment"]["value"].to_f, @partial_payment1.value.to_f
            assert_nil response["data"]["partialPayment"]["id"]
          end
        end 
      end 
    end
  end

  context 'when current user has admin permissions' do
    setup do 
      @current_user = create(:user_with_role_admin)
      @context = { 
        current_user: @current_user,
        current_ability: Ability.new(@current_user)
      }
    end 
      
    context 'when there arent partial payments' do 
      setup do 
        @query_string = <<-GRAPHQL
          query {
            partialPayment(id: 1) {
              id
              value
              payment {
                id
                paymentType
                period
                concept
                state
                dueDate
                amount
                contract {
                  id
                  state
                  contractType
                  property {
                    id
                    propertyType
                    state
                    description
                  }
                }
                subAccount {
                  id
                  name
                  balance
                }
                referral {
                  ... on User {
                    id
                    email
                    name
                  }
                  ... on Agency {
                    id
                    name
                  }
                }
              }
            }
          }
        GRAPHQL
      end 

      should 'return nil' do 
        response = perform(@query_string, @context)
        assert_nil response["data"]["partialPayment"]
      end
    end

    context 'when there are partial payments' do
      setup do 
        @partial_payment1 = create(:partial_payment)
      end  

      context 'when ask for multiple attrs' do 
        context 'when partial payment doesnt exist' do 
          setup do 
            @query_string = <<-GRAPHQL
              query {
                partialPayment(id: #{@partial_payment1.id + 1}) {
                  id
                  value
                  payment {
                    id
                    paymentType
                    period
                    concept
                    state
                    dueDate
                    amount
                    contract {
                      id
                      state
                      contractType
                      property {
                        id
                        propertyType
                        state
                        description
                      }
                    }
                    subAccount {
                      id
                      name
                      balance
                    }
                    referral {
                      ... on User {
                        id
                        email
                        profile {
                          name
                        }
                      }
                      ... on Agency {
                        id
                        name
                      }
                    }
                  }
                }
              }
            GRAPHQL
          end

          should 'return nil' do 
            response = perform(@query_string, @context)
            assert_nil response["data"]["partialPayment"]
          end
        end

        context 'when payment exists' do
          setup do 
            @query_string = <<-GRAPHQL
              query {
                partialPayment(id: #{@partial_payment1.id}) {
                  id 
                  value
                  payment {
                    id
                    paymentType
                    period
                    concept
                    state
                    dueDate
                    amount
                    contract {
                      id
                      state
                      contractType
                      property {
                        id
                        propertyType
                        state
                        description
                      }
                    }
                    subAccount {
                      id
                      name
                      balance
                    }
                    referral {
                      ... on User {
                        id
                        email
                        profile {
                          name
                        }
                      }
                      ... on Agency {
                        id
                        name
                      }
                    }
                  }
                }
              }
            GRAPHQL
          end

          context 'when isnt related to user' do 
            should 'return "unauthorized" message' do 
              response = perform(@query_string, @context)
              assert_nil response['data']['partialPayment']
              assert_equal response['errors'][0]['message'], 'user unauthorized'
            end
          end 
          
          context 'when is related to user' do 
            setup do 
              create(:user_agency, agency: @partial_payment1.payment.sub_account.agency, user: @current_user)
              @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
            end

            should 'return attrs by partial payment' do 
              response = perform(@query_string, @context)
              assert_equal response["data"]["partialPayment"]["id"].to_i, @partial_payment1.id
              assert_equal response["data"]["partialPayment"]["value"].to_f, @partial_payment1.value.to_f
              assert_equal response["data"]["partialPayment"]["payment"]["id"].to_i, @partial_payment1.payment.id
              assert_equal response["data"]["partialPayment"]["payment"]["paymentType"], @partial_payment1.payment.payment_type
              assert_equal response["data"]["partialPayment"]["payment"]["period"], @partial_payment1.payment.period
              assert_equal response["data"]["partialPayment"]["payment"]["concept"], @partial_payment1.payment.concept
              assert_equal response["data"]["partialPayment"]["payment"]["state"], @partial_payment1.payment.state
              assert_equal response["data"]["partialPayment"]["payment"]["dueDate"], @partial_payment1.payment.due_date.to_s
              assert_equal response["data"]["partialPayment"]["payment"]["amount"], @partial_payment1.payment.amount
              assert_equal response["data"]["partialPayment"]["payment"]["contract"]["id"].to_i, @partial_payment1.payment.contract.id
              assert_equal response["data"]["partialPayment"]["payment"]["contract"]["state"], @partial_payment1.payment.contract.state
              assert_equal response["data"]["partialPayment"]["payment"]["contract"]["contractType"], @partial_payment1.payment.contract.contract_type
              assert_equal response["data"]["partialPayment"]["payment"]["contract"]["property"]["id"].to_i, @partial_payment1.payment.contract.property.id
              assert_equal response["data"]["partialPayment"]["payment"]["contract"]["property"]["propertyType"], @partial_payment1.payment.contract.property.property_type
              assert_equal response["data"]["partialPayment"]["payment"]["contract"]["property"]["state"], @partial_payment1.payment.contract.property.state
              assert_equal response["data"]["partialPayment"]["payment"]["contract"]["property"]["description"], @partial_payment1.payment.contract.property.description
              assert_equal response["data"]["partialPayment"]["payment"]["subAccount"]["id"].to_i, @partial_payment1.payment.sub_account.id
              assert_equal response["data"]["partialPayment"]["payment"]["subAccount"]["name"], @partial_payment1.payment.sub_account.name
              assert_equal response["data"]["partialPayment"]["payment"]["subAccount"]["balance"], @partial_payment1.payment.sub_account.balance
              assert_equal response["data"]["partialPayment"]["payment"]["referral"]["id"].to_i, @partial_payment1.payment.referral.id
              assert_equal response["data"]["partialPayment"]["payment"]["referral"]["profile"]["name"], @partial_payment1.payment.referral.profile.name
            end
          end
        end 
      end 

      context 'when ask for only 1 attr' do 
        context 'when partial payment doesnt exist' do 
          setup do 
            @query_string = <<-GRAPHQL
              query {
                partialPayment(id: #{@partial_payment1.id + 1}) {
                  value
                }
              }
            GRAPHQL
          end

          should 'return nil' do 
            response = perform(@query_string, @context)
            assert_nil response["data"]["partialPayment"]
          end
        end

        context 'when partial payment exists' do
          setup do 
            @query_string = <<-GRAPHQL
              query {
                partialPayment(id: #{@partial_payment1.id}) {
                  value
                }
              }
            GRAPHQL
          end

          context 'when isnt related to user' do 
            should 'return "unauthorized" message' do 
              response = perform(@query_string, @context)
              assert_nil response['data']['partialPayment']
              assert_equal response['errors'][0]['message'], 'user unauthorized'
            end
          end 
          
          context 'when is related to user' do 
            setup do 
              create(:user_agency, agency: @partial_payment1.payment.sub_account.agency, user: @current_user)
              @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
            end

            should 'return only 1 attr for specific partial payment' do 
              response = perform(@query_string, @context)
              assert_equal response["data"]["partialPayment"]["value"].to_f, @partial_payment1.value.to_f
              assert_nil response["data"]["partialPayment"]["id"]
            end
          end
        end 
      end 
    end
  end 
end