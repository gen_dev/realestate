require 'test_helper'

class Queries::PaymentTest < ActiveSupport::TestCase
  def perform(query, context = {})
    BackendSchema.execute(query, context: context).to_h
  end

  context 'when current user has gendev permissions' do
    setup do 
      @current_user = create(:user_with_role_gendev)
      @context = { 
        current_user: @current_user,
        current_ability: Ability.new(@current_user)
      }
    end 
      
    context 'when there arent payments' do 
      setup do 
        @query_string = <<-GRAPHQL
          query {
            payment(id: 1) {
              paymentType
              period
              concept
              state
              dueDate
              amount
              contract {
                id
                state
                contractType
                property {
                  id
                  propertyType
                  state
                  description
                }
              }
              subAccount {
                id
                name
                balance
              }
              referral {
                ... on User {
                  id
                  email
                  name
                }
                ... on Agency {
                  id
                  name
                }
              }
            }
          }
        GRAPHQL
      end 

      should 'return nil' do 
        response = perform(@query_string, @context)
        assert_nil response["data"]["payment"]
      end
    end


    context 'when there are payments' do
      setup do 
        @payment1 = create(:payment)
      end  

      context 'when ask for multiple attrs' do 
        context 'when payment doesnt exist' do 
          setup do 
            @query_string = <<-GRAPHQL
              query {
                payment(id: #{@payment1.id + 1}) {
                  paymentType
                  period
                  concept
                  state
                  dueDate
                  amount
                  contract {
                    id
                    state
                    contractType
                    property {
                      id
                      propertyType
                      state
                      description
                    }
                  }
                  subAccount {
                    id
                    name
                    balance
                  }
                  referral {
                    ... on User {
                      id
                      email
                      profile { 
                        name
                      }
                    }
                    ... on Agency {
                      id
                      name
                    }
                  }
                }
              }
            GRAPHQL
          end

          should 'return nil' do 
            response = perform(@query_string, @context)
            assert_nil response["data"]["payment"]
          end
        end

        context 'when payment exists' do
          setup do 
            @query_string = <<-GRAPHQL
              query {
                payment(id: #{@payment1.id}) {
                  id
                  paymentType
                  period
                  concept
                  state
                  dueDate
                  amount
                  contract {
                    id
                    state
                    contractType
                    property {
                      id
                      propertyType
                      state
                      description
                    }
                  }
                  subAccount {
                    id
                    name
                    balance
                  }
                  referral {
                    ... on User {
                      id
                      email
                      profile { 
                        name
                      }
                    }
                    ... on Agency {
                      id
                      name
                    }
                  }
                }
              }
            GRAPHQL
          end

          should 'return only attrs by payment' do 
            response = perform(@query_string, @context)
            assert_equal response["data"]["payment"]["id"].to_i, @payment1.id
            assert_equal response["data"]["payment"]["paymentType"], @payment1.payment_type
            assert_equal response["data"]["payment"]["period"], @payment1.period
            assert_equal response["data"]["payment"]["concept"], @payment1.concept
            assert_equal response["data"]["payment"]["state"], @payment1.state
            assert_equal response["data"]["payment"]["dueDate"], @payment1.due_date.to_s
            assert_equal response["data"]["payment"]["amount"], @payment1.amount
            assert_equal response["data"]["payment"]["contract"]["id"].to_i, @payment1.contract.id
            assert_equal response["data"]["payment"]["contract"]["state"], @payment1.contract.state
            assert_equal response["data"]["payment"]["contract"]["contractType"], @payment1.contract.contract_type
            assert_equal response["data"]["payment"]["contract"]["property"]["id"].to_i, @payment1.contract.property.id
            assert_equal response["data"]["payment"]["contract"]["property"]["propertyType"], @payment1.contract.property.property_type
            assert_equal response["data"]["payment"]["contract"]["property"]["state"], @payment1.contract.property.state
            assert_equal response["data"]["payment"]["contract"]["property"]["description"], @payment1.contract.property.description
            assert_equal response["data"]["payment"]["subAccount"]["id"].to_i, @payment1.sub_account.id
            assert_equal response["data"]["payment"]["subAccount"]["name"], @payment1.sub_account.name
            assert_equal response["data"]["payment"]["subAccount"]["balance"], @payment1.sub_account.balance
            assert_equal response["data"]["payment"]["referral"]["id"].to_i, @payment1.referral.id
            assert_equal response["data"]["payment"]["referral"]["profile"]["name"], @payment1.referral.profile.name
          end
        end 
      end 

      context 'when ask for only 1 attr' do 
        context 'when payment doesnt exist' do 
          setup do 
            @query_string = <<-GRAPHQL
              query {
                payment(id: #{@payment1.id + 1}) {
                  paymentType
                  period
                  concept
                  state
                  dueDate
                  amount
                  contract {
                    id
                    state
                    contractType
                    property {
                      id
                      propertyType
                      state
                      description
                    }
                  }
                  subAccount {
                    id
                    name
                    balance
                  }
                  referral {
                    ... on User {
                      id
                      email
                      profile { 
                        name
                      }
                    }
                    ... on Agency {
                      id
                      name
                    }
                  }
                }
              }
            GRAPHQL
          end

          should 'return nil' do 
            response = perform(@query_string, @context)
            assert_nil response["data"]["payment"]
          end
        end

        context 'when payment exists' do
          setup do 
            @query_string = <<-GRAPHQL
              query {
                payment(id: #{@payment1.id}) {
                  period
                }
              }
            GRAPHQL
          end

          should 'return only 1 attr for specific payment' do 
            response = perform(@query_string, @context)
            assert_equal response["data"]["payment"]["period"], @payment1.period
            assert_nil response["data"]["payment"]["id"]
          end
        end 
      end 
    end
  end

  context 'when current user has admin permissions' do
    setup do 
      @current_user = create(:user_with_role_admin)
      @context = { 
        current_user: @current_user,
        current_ability: Ability.new(@current_user)
      }
    end 
      
    context 'when there arent payments' do 
      setup do 
        @query_string = <<-GRAPHQL
          query {
            payment(id: 1) {
              paymentType
              period
              concept
              state
              dueDate
              amount
              contract {
                id
                state
                contractType
                property {
                  id
                  propertyType
                  state
                  description
                }
              }
              subAccount {
                id
                name
                balance
              }
              referral {
                ... on User {
                  id
                  email
                  name
                }
                ... on Agency {
                  id
                  name
                }
              }
            }
          }
        GRAPHQL
      end 

      should 'return nil' do 
        response = perform(@query_string, @context)
        assert_nil response["data"]["payment"]
      end
    end

    context 'when there are payments' do
      setup do 
        @payment1 = create(:payment)
      end  

      context 'when ask for multiple attrs' do 
        context 'when payment doesnt exist' do 
          setup do 
            @query_string = <<-GRAPHQL
              query {
                payment(id: #{@payment1.id + 1}) {
                  paymentType
                  period
                  concept
                  state
                  dueDate
                  amount
                  contract {
                    id
                    state
                    contractType
                    property {
                      id
                      propertyType
                      state
                      description
                    }
                  }
                  subAccount {
                    id
                    name
                    balance
                  }
                  referral {
                    ... on User {
                      id
                      email
                      profile { 
                        name
                      }
                    }
                    ... on Agency {
                      id
                      name
                    }
                  }
                }
              }
            GRAPHQL
          end

          should 'return nil' do 
            response = perform(@query_string, @context)
            assert_nil response["data"]["payment"]
          end
        end

        context 'when payment exists' do
          setup do 
            @query_string = <<-GRAPHQL
              query {
                payment(id: #{@payment1.id}) {
                  id
                  paymentType
                  period
                  concept
                  state
                  dueDate
                  amount
                  contract {
                    id
                    state
                    contractType
                    property {
                      id
                      propertyType
                      state
                      description
                    }
                  }
                  subAccount {
                    id
                    name
                    balance
                  }
                  referral {
                    ... on User {
                      id
                      email
                      profile {
                        name
                      }
                    }
                    ... on Agency {
                      id
                      name
                    }
                  }
                }
              }
            GRAPHQL
          end

          context 'when isnt related to user' do 
            should 'return "unauthorized" message' do 
              response = perform(@query_string, @context)
              assert_nil response['data']['payment']
              assert_equal response['errors'][0]['message'], 'user unauthorized'
            end
          end 
          
          context 'when is related to user' do 
            setup do 
              create(:user_agency, agency: @payment1.sub_account.agency, user: @current_user)
              @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
            end

            should 'return only attrs by payment' do 
              response = perform(@query_string, @context)
              assert_equal response["data"]["payment"]["id"].to_i, @payment1.id
              assert_equal response["data"]["payment"]["paymentType"], @payment1.payment_type
              assert_equal response["data"]["payment"]["period"], @payment1.period
              assert_equal response["data"]["payment"]["concept"], @payment1.concept
              assert_equal response["data"]["payment"]["state"], @payment1.state
              assert_equal response["data"]["payment"]["dueDate"], @payment1.due_date.to_s
              assert_equal response["data"]["payment"]["amount"], @payment1.amount
              assert_equal response["data"]["payment"]["contract"]["id"].to_i, @payment1.contract.id
              assert_equal response["data"]["payment"]["contract"]["state"], @payment1.contract.state
              assert_equal response["data"]["payment"]["contract"]["contractType"], @payment1.contract.contract_type
              assert_equal response["data"]["payment"]["contract"]["property"]["id"].to_i, @payment1.contract.property.id
              assert_equal response["data"]["payment"]["contract"]["property"]["propertyType"], @payment1.contract.property.property_type
              assert_equal response["data"]["payment"]["contract"]["property"]["state"], @payment1.contract.property.state
              assert_equal response["data"]["payment"]["contract"]["property"]["description"], @payment1.contract.property.description
              assert_equal response["data"]["payment"]["subAccount"]["id"].to_i, @payment1.sub_account.id
              assert_equal response["data"]["payment"]["subAccount"]["name"], @payment1.sub_account.name
              assert_equal response["data"]["payment"]["subAccount"]["balance"], @payment1.sub_account.balance
              assert_equal response["data"]["payment"]["referral"]["id"].to_i, @payment1.referral.id
              assert_equal response["data"]["payment"]["referral"]["profile"]["name"], @payment1.referral.profile.name
            end
          end
        end 
      end 

      context 'when ask for only 1 attr' do 
        context 'when payment doesnt exist' do 
          setup do 
            @query_string = <<-GRAPHQL
              query {
                payment(id: #{@payment1.id + 1}) {
                  paymentType
                  period
                  concept
                  state
                  dueDate
                  amount
                  contract {
                    id
                    state
                    contractType
                    property {
                      id
                      propertyType
                      state
                      description
                    }
                  }
                  subAccount {
                    id
                    name
                    balance
                  }
                  referral {
                    ... on User {
                      id
                      email
                      profile { 
                        name
                      }
                    }
                    ... on Agency {
                      id
                      name
                    }
                  }
                }
              }
            GRAPHQL
          end

          should 'return nil' do 
            response = perform(@query_string, @context)
            assert_nil response["data"]["payment"]
          end
        end

        context 'when payment exists' do
          setup do 
            @query_string = <<-GRAPHQL
              query {
                payment(id: #{@payment1.id}) {
                  period
                }
              }
            GRAPHQL
          end

          context 'when isnt related to user' do 
            should 'return "unauthorized" message' do 
              response = perform(@query_string, @context)
              assert_nil response['data']['payment']
              assert_equal response['errors'][0]['message'], 'user unauthorized'
            end
          end 
          
          context 'when is related to user' do 
            setup do 
              create(:user_agency, agency: @payment1.sub_account.agency, user: @current_user)
              @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
            end
          
            should 'return only 1 attr for specific payment' do 
              response = perform(@query_string, @context)
              assert_equal response["data"]["payment"]["period"], @payment1.period
              assert_nil response["data"]["payment"]["id"]
            end
          end
        end 
      end 
    end
  end
end