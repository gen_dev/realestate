require 'test_helper'

class Queries::PaymentsTest < ActiveSupport::TestCase
  def perform(query, context = {})
    BackendSchema.execute(query, context: context).to_h
  end

  context 'when current user has gendev permissions' do
    setup do 
      @current_user = create(:user_with_role_gendev)
      create(:user_agency, user: @current_user, selected: true)
      @context = { 
        current_user: @current_user,
        current_ability: Ability.new(@current_user)
      }
    end 
      
    context 'when ask for multiples attrs' do 
      setup do 
        @query_string = <<-GRAPHQL
          query {
            payments {
              payments {
                id
                paymentType
                period
                concept
                state
                dueDate
                amount
                contract {
                  id
                  state
                  contractType
                  property {
                    id
                    propertyType
                    state
                    description
                  }
                }
                subAccount {
                  id
                  name
                  balance
                }
                referral {
                  ... on User {
                    id
                    email
                    profile {
                      name
                    }
                  }
                  ... on Agency {
                    id
                    name
                  }
                }
              }
              pages
            }
          }
        GRAPHQL
      end 

      context 'when there arent payments' do 
        should 'return empty array' do 
          response = perform(@query_string, @context)
          assert_empty response["data"]["payments"]["payments"]
        end
      end
      
      context 'when there are payments' do
        setup do 
          @payment1 = create(:payment, referral: create(:user), sub_account: create(:sub_account, agency: @current_user.selected_agency))
          @payment2 = create(:payment, referral: create(:agency), sub_account: create(:sub_account, agency: @current_user.selected_agency))
        end
        
        should 'return multiple payments' do 
          response = perform(@query_string, @context)
          payments = response["data"]["payments"]["payments"]
          
          assert payments
          assert_operator payments.length, :>, 1
          assert_equal payments.length, Payment.all.length
        end 

        should 'return attrs by payment' do 
          response = perform(@query_string, @context)
          payments = response["data"]["payments"]["payments"].sort_by { |x| x["id"].to_i }

          assert_equal payments[0]["id"].to_i, @payment1.id
          assert_equal payments[0]["paymentType"], @payment1.payment_type
          assert_equal payments[0]["period"], @payment1.period
          assert_equal payments[0]["concept"], @payment1.concept
          assert_equal payments[0]["state"], @payment1.state
          assert_equal payments[0]["dueDate"], @payment1.due_date.to_s
          assert_equal payments[0]["amount"], @payment1.amount
          assert_equal payments[0]["contract"]["id"].to_i, @payment1.contract.id
          assert_equal payments[0]["contract"]["state"], @payment1.contract.state
          assert_equal payments[0]["contract"]["contractType"], @payment1.contract.contract_type
          assert_equal payments[0]["contract"]["property"]["id"].to_i, @payment1.contract.property.id
          assert_equal payments[0]["contract"]["property"]["propertyType"], @payment1.contract.property.property_type
          assert_equal payments[0]["contract"]["property"]["state"], @payment1.contract.property.state
          assert_equal payments[0]["contract"]["property"]["description"], @payment1.contract.property.description
          assert_equal payments[0]["subAccount"]["id"].to_i, @payment1.sub_account.id
          assert_equal payments[0]["subAccount"]["name"], @payment1.sub_account.name
          assert_equal payments[0]["subAccount"]["balance"], @payment1.sub_account.balance
          assert_equal payments[0]["referral"]["id"].to_i, @payment1.referral.id
          assert_equal payments[0]["referral"]["profile"]["name"], @payment1.referral.profile.name
          assert_equal payments[0]["referral"]["email"], @payment1.referral.email
          
          assert_equal payments[1]["id"].to_i, @payment2.id
          assert_equal payments[1]["paymentType"], @payment2.payment_type
          assert_equal payments[1]["period"], @payment2.period
          assert_equal payments[1]["concept"], @payment2.concept
          assert_equal payments[1]["state"], @payment2.state
          assert_equal payments[1]["dueDate"], @payment2.due_date.to_s
          assert_equal payments[1]["amount"], @payment2.amount
          assert_equal payments[1]["contract"]["id"].to_i, @payment2.contract.id
          assert_equal payments[1]["contract"]["state"], @payment2.contract.state
          assert_equal payments[1]["contract"]["contractType"], @payment2.contract.contract_type
          assert_equal payments[1]["contract"]["property"]["id"].to_i, @payment2.contract.property.id
          assert_equal payments[1]["contract"]["property"]["propertyType"], @payment2.contract.property.property_type
          assert_equal payments[1]["contract"]["property"]["state"], @payment2.contract.property.state
          assert_equal payments[1]["contract"]["property"]["description"], @payment2.contract.property.description
          assert_equal payments[1]["subAccount"]["id"].to_i, @payment2.sub_account.id
          assert_equal payments[1]["subAccount"]["name"], @payment2.sub_account.name
          assert_equal payments[1]["subAccount"]["balance"], @payment2.sub_account.balance
          assert_equal payments[1]["referral"]["id"].to_i, @payment2.referral.id
          assert_equal payments[1]["referral"]["name"], @payment2.referral.name
          assert_nil payments[1]["referral"]["email"]
        end
      end  
    end 

    context 'when ask for only 1 attr' do 
      setup do 
        @query_string = <<-GRAPHQL
          query {
            payments {
              payments {
                period
              }
              pages
            }
          }
        GRAPHQL
      end
      
      context 'when there arent payments' do 
        should 'return empty array' do 
          response = perform(@query_string, @context)
          assert_empty response["data"]["payments"]["payments"]
        end
      end

      context 'when there are payments' do
        setup do 
          create(:payment, sub_account: create(:sub_account, agency: @current_user.selected_agency))
          create(:payment, sub_account: create(:sub_account, agency: @current_user.selected_agency))
        end

        should 'return only 1 attr by payment' do 
          response = perform(@query_string, @context)
          payments = response["data"]["payments"]["payments"]
          assert payments[0]["period"]
          assert_nil payments[0]["id"]
          assert payments[1]["period"]
          assert_nil payments[1]["id"]
        end
      end
    end 

    context 'when use filters' do 
      setup do 
        @concept = "Rent"
        @query_string = <<-GRAPHQL
          query {
            payments(filters: { concept: "#{@concept}" } ) {
              payments {
                id
                paymentType
                period
                concept
                state
                dueDate
                amount
              }
              pages
            }
          }
        GRAPHQL
      end 

      context 'when payments dont match filter' do 
        setup do 
          create(:payment, concept: 'Tax', sub_account: create(:sub_account, agency: @current_user.selected_agency))
        end 

        should 'return empty array' do 
          response = perform(@query_string, @context)
          assert_empty response["data"]["payments"]["payments"]

        end
      end
    
      context 'when there are payments matching filter' do 
        setup do 
          create(:payment, concept: @concept, sub_account: create(:sub_account, agency: @current_user.selected_agency))
          create(:payment, concept: 'Tax', sub_account: create(:sub_account, agency: @current_user.selected_agency))
          create(:payment, concept: 'Something', sub_account: create(:sub_account, agency: @current_user.selected_agency))
        end 
        
        should 'return only correct payments' do 
          response = perform(@query_string, @context)
          payments = response["data"]["payments"]["payments"]

          assert payments
          assert_equal payments.length, 1
          assert payments[0]["id"]
          assert payments[0]["paymentType"]
          assert payments[0]["period"]
          assert payments[0]["state"]
          assert_equal payments[0]["concept"], @concept
          assert payments[0]["dueDate"]
          assert payments[0]["amount"]
        end
      end
    end
  end

  context 'when current user has admin permissions' do
    setup do 
      @current_user = create(:user_with_role_admin)
      create(:user_agency, user: @current_user, selected: true)
      @context = { 
        current_user: @current_user,
        current_ability: Ability.new(@current_user)
      }
    end 
      
    context 'when ask for multiples attrs' do 
      setup do 
        @query_string = <<-GRAPHQL
          query {
            payments {
              payments {

                id
                paymentType
                period
                concept
                state
                dueDate
                amount
                contract {
                  id
                  state
                  contractType
                  property {
                    id
                    propertyType
                    state
                    description
                  }
                }
                subAccount {
                  id
                  name
                  balance
                }
                referral {
                  ... on User {
                    id
                    email
                    profile {
                      name
                    }
                  }
                  ... on Agency {
                    id
                    name
                  }
                }
              }
              pages
            }
          }
        GRAPHQL
      end 

      context 'when there arent payments' do 
        should 'return empty array' do 
          response = perform(@query_string, @context)
          assert_empty response["data"]["payments"]["payments"]
        end
      end

      context 'when there are payments' do
        setup do 
          @payment1 = create(:payment, referral: create(:user))
          @payment2 = create(:payment, referral: create(:agency))
        end

        context 'when there arent payments related to user' do 
          should 'return empty array' do 
            response = perform(@query_string, @context)
            assert_empty response["data"]["payments"]["payments"]
          end
        end 

        context 'when there are payments related to user' do 
          setup do 
            @payment3 = create(:payment, referral: create(:agency), sub_account: create(:sub_account, agency: @current_user.selected_agency))
          end 
        
          should 'return only related payments' do 
            response = perform(@query_string, @context)
            payments = response["data"]["payments"]["payments"]
            assert payments
            assert_equal payments.length, 1
          end
        end 

        context 'when all payments are related to user' do 
          setup do 
            @payment3 = create(:payment, referral: create(:user), sub_account: create(:sub_account, agency: @current_user.selected_agency))
            @payment4 = create(:payment, referral: create(:agency), sub_account: create(:sub_account, agency: @current_user.selected_agency))
          end 
          
          should 'return multiple payments' do 
            response = perform(@query_string, @context)
            payments = response["data"]["payments"]["payments"]

            assert payments
            assert_operator payments.length, :>, 1
          end 

          should 'return attrs by payment' do 
            response = perform(@query_string, @context)
            payments = response["data"]["payments"]["payments"].sort_by { |x| x["id"].to_i }
            
            assert_equal payments[0]["id"].to_i, @payment3.id
            assert_equal payments[0]["paymentType"], @payment3.payment_type
            assert_equal payments[0]["period"], @payment3.period
            assert_equal payments[0]["concept"], @payment3.concept
            assert_equal payments[0]["state"], @payment3.state
            assert_equal payments[0]["dueDate"], @payment3.due_date.to_s
            assert_equal payments[0]["amount"], @payment3.amount
            assert_equal payments[0]["contract"]["id"].to_i, @payment3.contract.id
            assert_equal payments[0]["contract"]["state"], @payment3.contract.state
            assert_equal payments[0]["contract"]["contractType"], @payment3.contract.contract_type
            assert_equal payments[0]["contract"]["property"]["id"].to_i, @payment3.contract.property.id
            assert_equal payments[0]["contract"]["property"]["propertyType"], @payment3.contract.property.property_type
            assert_equal payments[0]["contract"]["property"]["state"], @payment3.contract.property.state
            assert_equal payments[0]["contract"]["property"]["description"], @payment3.contract.property.description
            assert_equal payments[0]["subAccount"]["id"].to_i, @payment3.sub_account.id
            assert_equal payments[0]["subAccount"]["name"], @payment3.sub_account.name
            assert_equal payments[0]["subAccount"]["balance"], @payment3.sub_account.balance
            assert_equal payments[0]["referral"]["id"].to_i, @payment3.referral.id
            assert_equal payments[0]["referral"]["profile"]["name"], @payment3.referral.profile.name
            assert_equal payments[0]["referral"]["email"], @payment3.referral.email
            
            assert_equal payments[1]["id"].to_i, @payment4.id
            assert_equal payments[1]["paymentType"], @payment4.payment_type
            assert_equal payments[1]["period"], @payment4.period
            assert_equal payments[1]["concept"], @payment4.concept
            assert_equal payments[1]["state"], @payment4.state
            assert_equal payments[1]["dueDate"], @payment4.due_date.to_s
            assert_equal payments[1]["amount"], @payment4.amount
            assert_equal payments[1]["contract"]["id"].to_i, @payment4.contract.id
            assert_equal payments[1]["contract"]["state"], @payment4.contract.state
            assert_equal payments[1]["contract"]["contractType"], @payment4.contract.contract_type
            assert_equal payments[1]["contract"]["property"]["id"].to_i, @payment4.contract.property.id
            assert_equal payments[1]["contract"]["property"]["propertyType"], @payment4.contract.property.property_type
            assert_equal payments[1]["contract"]["property"]["state"], @payment4.contract.property.state
            assert_equal payments[1]["contract"]["property"]["description"], @payment4.contract.property.description
            assert_equal payments[1]["subAccount"]["id"].to_i, @payment4.sub_account.id
            assert_equal payments[1]["subAccount"]["name"], @payment4.sub_account.name
            assert_equal payments[1]["subAccount"]["balance"], @payment4.sub_account.balance
            assert_equal payments[1]["referral"]["id"].to_i, @payment4.referral.id
            assert_equal payments[1]["referral"]["name"], @payment4.referral.name
            assert_nil payments[1]["referral"]["email"]
          end
        end
      end  
    end 

    context 'when ask for only 1 attr' do 
      setup do 
        @query_string = <<-GRAPHQL
          query {
            payments {
              payments {
                period
              }
              pages
            }
          }
        GRAPHQL
      end
      
      context 'when there arent payments' do 
        should 'return empty array' do 
          response = perform(@query_string, @context)
          assert_empty response["data"]["payments"]["payments"]

        end
      end

      context 'when there are payments related to user' do 
        setup do 
          create(:payment)
          @payment1 = create(:payment, sub_account: create(:sub_account, agency: @current_user.selected_agency))
          @payment2 = create(:payment, sub_account: create(:sub_account, agency: @current_user.selected_agency))
        end 
      
        should 'return only 1 attr by each related payment' do 
          response = perform(@query_string, @context)
          payments = response["data"]["payments"]["payments"]

          assert payments
          assert_equal payments.length, 2
          assert payments[0]["period"]
          assert_nil payments[0]["id"]
        end
      end 

      context 'when all payments are related to user' do 
        setup do 
          @payment1 = create(:payment, sub_account: create(:sub_account, agency: @current_user.selected_agency))
          @payment2 = create(:payment, sub_account: create(:sub_account, agency: @current_user.selected_agency))
        end 

        should 'return only 1 attr by payment' do 
          response = perform(@query_string, @context)
          payments = response["data"]["payments"]["payments"]

          assert payments[0]["period"]
          assert_nil payments[0]["id"]
          assert payments[1]["period"]
          assert_nil payments[1]["id"]
        end
      end
    end 

    context 'when use filters' do 
      setup do 
        @concept = "Rent"
        @query_string = <<-GRAPHQL
          query {
            payments(filters: { concept: "#{@concept}" } ) {
              payments {
                id
                paymentType
                period
                concept
                state
                dueDate
                amount
              }
              pages
            }
          }
        GRAPHQL
      end 

      context 'when payments dont match filter' do 
        setup do 
          create(:payment, concept: 'Tax', sub_account: create(:sub_account, agency: @current_user.selected_agency))
        end 

        should 'return empty array' do 
          response = perform(@query_string, @context)
          assert_empty response["data"]["payments"]["payments"]
        end
      end
    
      context 'when there are payments matching filter' do 
        setup do 
          @payment1 = create(:payment, concept: @concept)
          @payment2 = create(:payment, concept: 'Tax')
          @payment3 = create(:payment, concept: @concept)
        end 
        
        context 'when there arent related to user' do 
          should 'return empty array' do 
            response = perform(@query_string, @context)
            assert_empty response["data"]["payments"]["payments"]
          end
        end 

        context 'when there are payments related to user' do 
          setup do 
            @payment1.update(sub_account: create(:sub_account, agency: @current_user.selected_agency)) 
            @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
          end 
        
          should 'return only related and filtered payments' do 
            response = perform(@query_string, @context)
            payments = response["data"]["payments"]["payments"]

            assert payments
            assert_equal payments.length, 1
            assert payments[0]["id"]
            assert payments[0]["paymentType"]
            assert payments[0]["period"]
            assert payments[0]["state"]
            assert_equal payments[0]["concept"], @concept
            assert payments[0]["dueDate"]
            assert payments[0]["amount"]
          end
        end
      end
    end
  end
end