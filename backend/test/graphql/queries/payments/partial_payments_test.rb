require 'test_helper'

class Queries::PartialPaymentsTest < ActiveSupport::TestCase
  def perform(query, context = {})
    BackendSchema.execute(query, context: context).to_h
  end

  context 'when current user has gendev permissions' do
    setup do 
      @current_user = create(:user_with_role_gendev)
      @context = { 
        current_user: @current_user,
        current_ability: Ability.new(@current_user)
      }
    end 

    context 'when ask for multiples attrs' do 
      setup do 
        @query_string = <<-GRAPHQL
          query {
            partialPayments {
              id 
              value
              payment {
                id
                paymentType
                period
                concept
                state
                dueDate
                amount
                contract {
                  id
                  state
                  contractType
                  property {
                    id
                    propertyType
                    state
                    description
                  }
                }
                subAccount {
                  id
                  name
                  balance
                }
                referral {
                  ... on User {
                    id
                    email
                    profile { 
                      name
                    }
                  }
                  ... on Agency {
                    id
                    name
                  }
                }
              }
            }
          }
        GRAPHQL
      end 

      context 'when there arent partial payments' do 
        should 'return empty array' do 
          response = perform(@query_string, @context)
          assert_empty response["data"]["partialPayments"]
        end
      end
      
      context 'when there are partial payments' do
        setup do 
          @partial_payment1 = create(:partial_payment)
          @partial_payment2 = create(:partial_payment)
        end
        
        should 'return multiple partial payments' do 
          response = perform(@query_string, @context)
          assert response["data"]["partialPayments"]
          assert_operator response["data"]["partialPayments"].length, :>, 1
          assert_equal response["data"]["partialPayments"].length, PartialPayment.all.length
        end 

        should 'return attrs by payment' do 
          response = perform(@query_string, @context)
          assert_equal response["data"]["partialPayments"][0]["id"].to_i, @partial_payment1.id
          assert_equal response["data"]["partialPayments"][0]["value"].to_f, @partial_payment1.value.to_f
          assert_equal response["data"]["partialPayments"][0]["payment"]["id"].to_i, @partial_payment1.payment.id
          assert_equal response["data"]["partialPayments"][0]["payment"]["paymentType"], @partial_payment1.payment.payment_type
          assert_equal response["data"]["partialPayments"][0]["payment"]["period"], @partial_payment1.payment.period
          assert_equal response["data"]["partialPayments"][0]["payment"]["concept"], @partial_payment1.payment.concept
          assert_equal response["data"]["partialPayments"][0]["payment"]["state"], @partial_payment1.payment.state
          assert_equal response["data"]["partialPayments"][0]["payment"]["dueDate"], @partial_payment1.payment.due_date.to_s
          assert_equal response["data"]["partialPayments"][0]["payment"]["amount"], @partial_payment1.payment.amount
          assert_equal response["data"]["partialPayments"][0]["payment"]["contract"]["id"].to_i, @partial_payment1.payment.contract.id
          assert_equal response["data"]["partialPayments"][0]["payment"]["contract"]["state"], @partial_payment1.payment.contract.state
          assert_equal response["data"]["partialPayments"][0]["payment"]["contract"]["contractType"], @partial_payment1.payment.contract.contract_type
          assert_equal response["data"]["partialPayments"][0]["payment"]["contract"]["property"]["id"].to_i, @partial_payment1.payment.contract.property.id
          assert_equal response["data"]["partialPayments"][0]["payment"]["contract"]["property"]["propertyType"], @partial_payment1.payment.contract.property.property_type
          assert_equal response["data"]["partialPayments"][0]["payment"]["contract"]["property"]["state"], @partial_payment1.payment.contract.property.state
          assert_equal response["data"]["partialPayments"][0]["payment"]["contract"]["property"]["description"], @partial_payment1.payment.contract.property.description
          assert_equal response["data"]["partialPayments"][0]["payment"]["subAccount"]["id"].to_i, @partial_payment1.payment.sub_account.id
          assert_equal response["data"]["partialPayments"][0]["payment"]["subAccount"]["name"], @partial_payment1.payment.sub_account.name
          assert_equal response["data"]["partialPayments"][0]["payment"]["subAccount"]["balance"], @partial_payment1.payment.sub_account.balance
          assert_equal response["data"]["partialPayments"][0]["payment"]["referral"]["id"].to_i, @partial_payment1.payment.referral.id
          assert_equal response["data"]["partialPayments"][0]["payment"]["referral"]["profile"]["name"], @partial_payment1.payment.referral.profile.name
          
          assert_equal response["data"]["partialPayments"][1]["id"].to_i, @partial_payment2.id
          assert_equal response["data"]["partialPayments"][1]["value"].to_f, @partial_payment2.value.to_f
          assert_equal response["data"]["partialPayments"][1]["payment"]["id"].to_i, @partial_payment2.payment.id
          assert_equal response["data"]["partialPayments"][1]["payment"]["paymentType"], @partial_payment2.payment.payment_type
          assert_equal response["data"]["partialPayments"][1]["payment"]["period"], @partial_payment2.payment.period
          assert_equal response["data"]["partialPayments"][1]["payment"]["concept"], @partial_payment2.payment.concept
          assert_equal response["data"]["partialPayments"][1]["payment"]["state"], @partial_payment2.payment.state
          assert_equal response["data"]["partialPayments"][1]["payment"]["dueDate"], @partial_payment2.payment.due_date.to_s
          assert_equal response["data"]["partialPayments"][1]["payment"]["amount"], @partial_payment2.payment.amount
          assert_equal response["data"]["partialPayments"][1]["payment"]["contract"]["id"].to_i, @partial_payment2.payment.contract.id
          assert_equal response["data"]["partialPayments"][1]["payment"]["contract"]["state"], @partial_payment2.payment.contract.state
          assert_equal response["data"]["partialPayments"][1]["payment"]["contract"]["contractType"], @partial_payment2.payment.contract.contract_type
          assert_equal response["data"]["partialPayments"][1]["payment"]["contract"]["property"]["id"].to_i, @partial_payment2.payment.contract.property.id
          assert_equal response["data"]["partialPayments"][1]["payment"]["contract"]["property"]["propertyType"], @partial_payment2.payment.contract.property.property_type
          assert_equal response["data"]["partialPayments"][1]["payment"]["contract"]["property"]["state"], @partial_payment2.payment.contract.property.state
          assert_equal response["data"]["partialPayments"][1]["payment"]["contract"]["property"]["description"], @partial_payment2.payment.contract.property.description
          assert_equal response["data"]["partialPayments"][1]["payment"]["subAccount"]["id"].to_i, @partial_payment2.payment.sub_account.id
          assert_equal response["data"]["partialPayments"][1]["payment"]["subAccount"]["name"], @partial_payment2.payment.sub_account.name
          assert_equal response["data"]["partialPayments"][1]["payment"]["subAccount"]["balance"], @partial_payment2.payment.sub_account.balance
          assert_equal response["data"]["partialPayments"][1]["payment"]["referral"]["id"].to_i, @partial_payment2.payment.referral.id
          assert_equal response["data"]["partialPayments"][1]["payment"]["referral"]["profile"]["name"], @partial_payment2.payment.referral.profile.name
        end
      end  
    end 

    context 'when ask for only 1 attr' do 
      setup do 
        @query_string = <<-GRAPHQL
          query {
            partialPayments {
              value
            }
          }
        GRAPHQL
      end
      
      context 'when there arent partial payments' do 
        should 'return empty array' do 
          response = perform(@query_string, @context)
          assert_empty response["data"]["partialPayments"]
        end
      end
      
      context 'when there are partial payments' do
        setup do 
          create(:partial_payment)
          create(:partial_payment)
        end

        should 'return only 1 attr by partial payment' do 
          response = perform(@query_string, @context)
          assert response["data"]["partialPayments"][0]["value"]
          assert_nil response["data"]["partialPayments"][0]["id"]
          assert response["data"]["partialPayments"][1]["value"]
          assert_nil response["data"]["partialPayments"][1]["id"]
        end
      end
    end 

    context 'when use filters' do 
      setup do 
        @value = 1200
        @query_string = <<-GRAPHQL
          query {
            partialPayments(filters: { value: #{@value} } ) {
              id
              value
            }
          }
        GRAPHQL
      end 

      context 'when partial payments dont match filter' do 
        setup do 
          create(:partial_payment, value: 3600)
        end 

        should 'return empty array' do 
          response = perform(@query_string, @context)
          assert_empty response["data"]["partialPayments"]
        end
      end

      context 'when there are partial payments matching filter' do 
        setup do 
          create(:partial_payment, value: @value)
          create(:partial_payment, value: 2400)
          create(:partial_payment, value: 600)
        end 
        
        should 'return only correct partial payments' do 
          response = perform(@query_string, @context)
          assert response["data"]["partialPayments"]
          assert_equal response["data"]["partialPayments"].length, 1
          assert response["data"]["partialPayments"][0]["id"]
          assert_equal response["data"]["partialPayments"][0]["value"], @value
        end
      end
    end
  end

  context 'when current user has admin permissions' do
    setup do 
      @current_user = create(:user_with_role_admin)
      @context = { 
        current_user: @current_user,
        current_ability: Ability.new(@current_user)
      }
    end 

    context 'when ask for multiples attrs' do 
      setup do 
        @query_string = <<-GRAPHQL
          query {
            partialPayments {
              id 
              value
              payment {
                id
                paymentType
                period
                concept
                state
                dueDate
                amount
                contract {
                  id
                  state
                  contractType
                  property {
                    id
                    propertyType
                    state
                    description
                  }
                }
                subAccount {
                  id
                  name
                  balance
                }
                referral {
                  ... on User {
                    id
                    email
                    profile { 
                      name
                    }
                  }
                  ... on Agency {
                    id
                    name
                  }
                }
              }
            }
          }
        GRAPHQL
      end 

      context 'when there arent partial payments' do 
        should 'return empty array' do 
          response = perform(@query_string, @context)
          assert_empty response["data"]["partialPayments"]
        end
      end
      
      context 'when there are partial payments' do
        setup do 
          @partial_payment1 = create(:partial_payment)
          @partial_payment2 = create(:partial_payment)
        end
        
        context 'when there arent partial payments related to user' do 
          should 'return empty array' do 
            response = perform(@query_string, @context)
            assert_empty response["data"]["partialPayments"]
          end
        end 
      
        context 'when there are partial payments related to user' do 
          setup do 
            create(:user_agency, agency: @partial_payment1.payment.sub_account.agency, user: @current_user)
            @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
          end 
        
          should 'return only related partial payments' do 
            response = perform(@query_string, @context)
            assert response["data"]["partialPayments"]
            assert_equal response["data"]["partialPayments"].length, 1
          end
        end 
      
        context 'when all partial payments are related to user' do 
          setup do 
            create(:user_agency, agency: @partial_payment1.payment.sub_account.agency, user: @current_user)
            create(:user_agency, agency: @partial_payment2.payment.sub_account.agency, user: @current_user)
            @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
          end 
      
          should 'return multiple partial payments' do 
            response = perform(@query_string, @context)
            assert response["data"]["partialPayments"]
            assert_operator response["data"]["partialPayments"].length, :>, 1
            assert_equal response["data"]["partialPayments"].length, PartialPayment.all.length
          end 

          should 'return attrs by payment' do 
            response = perform(@query_string, @context)
            partial_payments = response["data"]["partialPayments"].sort_by { |x| x["id"].to_i }

            assert_equal partial_payments[0]["id"].to_i, @partial_payment1.id
            assert_equal partial_payments[0]["value"].to_f, @partial_payment1.value.to_f
            assert_equal partial_payments[0]["payment"]["id"].to_i, @partial_payment1.payment.id
            assert_equal partial_payments[0]["payment"]["paymentType"], @partial_payment1.payment.payment_type
            assert_equal partial_payments[0]["payment"]["period"], @partial_payment1.payment.period
            assert_equal partial_payments[0]["payment"]["concept"], @partial_payment1.payment.concept
            assert_equal partial_payments[0]["payment"]["state"], @partial_payment1.payment.state
            assert_equal partial_payments[0]["payment"]["dueDate"], @partial_payment1.payment.due_date.to_s
            assert_equal partial_payments[0]["payment"]["amount"], @partial_payment1.payment.amount
            assert_equal partial_payments[0]["payment"]["contract"]["id"].to_i, @partial_payment1.payment.contract.id
            assert_equal partial_payments[0]["payment"]["contract"]["state"], @partial_payment1.payment.contract.state
            assert_equal partial_payments[0]["payment"]["contract"]["contractType"], @partial_payment1.payment.contract.contract_type
            assert_equal partial_payments[0]["payment"]["contract"]["property"]["id"].to_i, @partial_payment1.payment.contract.property.id
            assert_equal partial_payments[0]["payment"]["contract"]["property"]["propertyType"], @partial_payment1.payment.contract.property.property_type
            assert_equal partial_payments[0]["payment"]["contract"]["property"]["state"], @partial_payment1.payment.contract.property.state
            assert_equal partial_payments[0]["payment"]["contract"]["property"]["description"], @partial_payment1.payment.contract.property.description
            assert_equal partial_payments[0]["payment"]["subAccount"]["id"].to_i, @partial_payment1.payment.sub_account.id
            assert_equal partial_payments[0]["payment"]["subAccount"]["name"], @partial_payment1.payment.sub_account.name
            assert_equal partial_payments[0]["payment"]["subAccount"]["balance"], @partial_payment1.payment.sub_account.balance
            assert_equal partial_payments[0]["payment"]["referral"]["id"].to_i, @partial_payment1.payment.referral.id
            assert_equal partial_payments[0]["payment"]["referral"]["profile"]["name"], @partial_payment1.payment.referral.profile.name
            
            assert_equal partial_payments[1]["id"].to_i, @partial_payment2.id
            assert_equal partial_payments[1]["value"].to_f, @partial_payment2.value.to_f
            assert_equal partial_payments[1]["payment"]["id"].to_i, @partial_payment2.payment.id
            assert_equal partial_payments[1]["payment"]["paymentType"], @partial_payment2.payment.payment_type
            assert_equal partial_payments[1]["payment"]["period"], @partial_payment2.payment.period
            assert_equal partial_payments[1]["payment"]["concept"], @partial_payment2.payment.concept
            assert_equal partial_payments[1]["payment"]["state"], @partial_payment2.payment.state
            assert_equal partial_payments[1]["payment"]["dueDate"], @partial_payment2.payment.due_date.to_s
            assert_equal partial_payments[1]["payment"]["amount"], @partial_payment2.payment.amount
            assert_equal partial_payments[1]["payment"]["contract"]["id"].to_i, @partial_payment2.payment.contract.id
            assert_equal partial_payments[1]["payment"]["contract"]["state"], @partial_payment2.payment.contract.state
            assert_equal partial_payments[1]["payment"]["contract"]["contractType"], @partial_payment2.payment.contract.contract_type
            assert_equal partial_payments[1]["payment"]["contract"]["property"]["id"].to_i, @partial_payment2.payment.contract.property.id
            assert_equal partial_payments[1]["payment"]["contract"]["property"]["propertyType"], @partial_payment2.payment.contract.property.property_type
            assert_equal partial_payments[1]["payment"]["contract"]["property"]["state"], @partial_payment2.payment.contract.property.state
            assert_equal partial_payments[1]["payment"]["contract"]["property"]["description"], @partial_payment2.payment.contract.property.description
            assert_equal partial_payments[1]["payment"]["subAccount"]["id"].to_i, @partial_payment2.payment.sub_account.id
            assert_equal partial_payments[1]["payment"]["subAccount"]["name"], @partial_payment2.payment.sub_account.name
            assert_equal partial_payments[1]["payment"]["subAccount"]["balance"], @partial_payment2.payment.sub_account.balance
            assert_equal partial_payments[1]["payment"]["referral"]["id"].to_i, @partial_payment2.payment.referral.id
            assert_equal partial_payments[1]["payment"]["referral"]["profile"]["name"], @partial_payment2.payment.referral.profile.name
          end
        end
      end  
    end 

    context 'when ask for only 1 attr' do 
      setup do 
        @query_string = <<-GRAPHQL
          query {
            partialPayments {
              value
            }
          }
        GRAPHQL
      end
      
      context 'when there arent partial payments' do 
        should 'return empty array' do 
          response = perform(@query_string, @context)
          assert_empty response["data"]["partialPayments"]
        end
      end
      
      context 'when there are partial payments' do
        setup do 
          @partial_payment1 = create(:partial_payment)
          @partial_payment2 = create(:partial_payment)
        end

        context 'when there are agencies related to user' do 
          setup do 
            create(:user_agency, agency: @partial_payment1.payment.sub_account.agency, user: @current_user)
            @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
          end 
        
          should 'return only 1 attr by each related payment' do 
            response = perform(@query_string, @context)
            assert response["data"]["partialPayments"]
            assert_equal response["data"]["partialPayments"].length, 1
            assert response["data"]["partialPayments"][0]["value"]
            assert_nil response["data"]["partialPayments"][0]["id"]
          end
        end 
  
        context 'when all agencies are related to user' do 
          setup do 
            create(:user_agency, agency: @partial_payment1.payment.sub_account.agency, user: @current_user)
            create(:user_agency, agency: @partial_payment2.payment.sub_account.agency, user: @current_user)
            @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
          end 

          should 'return only 1 attr by partial payment' do 
            response = perform(@query_string, @context)
            assert response["data"]["partialPayments"][0]["value"]
            assert_nil response["data"]["partialPayments"][0]["id"]
            assert response["data"]["partialPayments"][1]["value"]
            assert_nil response["data"]["partialPayments"][1]["id"]
          end
        end
      end
    end 

    context 'when use filters' do 
      setup do 
        @value = 1200
        @query_string = <<-GRAPHQL
          query {
            partialPayments(filters: { value: #{@value} } ) {
              id
              value
            }
          }
        GRAPHQL
      end 

      context 'when partial payments dont match filter' do 
        setup do 
          create(:partial_payment, value: 3600)
        end 

        should 'return empty array' do 
          response = perform(@query_string, @context)
          assert_empty response["data"]["partialPayments"]
        end
      end

      context 'when there are partial payments matching filter' do 
        setup do 
          @partial_payment1 = create(:partial_payment, value: @value)
          @partial_payment2 = create(:partial_payment, value: 2400)
          @partial_payment3 = create(:partial_payment, value: @value)
        end 

        context 'when there arent related to user' do 
          should 'return empty array' do 
            response = perform(@query_string, @context)
            assert_empty response["data"]["partialPayments"]
          end
        end 

        context 'when there are partial payments related to user' do 
          setup do 
            create(:user_agency, agency: @partial_payment1.payment.sub_account.agency, user: @current_user)
            @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
          end 
        
          should 'return only related and filtered partial payments' do 
            response = perform(@query_string, @context)
            assert response["data"]["partialPayments"]
            assert_equal response["data"]["partialPayments"].length, 1
            assert response["data"]["partialPayments"][0]["id"]
            assert_equal response["data"]["partialPayments"][0]["value"], @value
          end
        end
      end
    end
  end
end