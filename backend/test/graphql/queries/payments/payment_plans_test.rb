require 'test_helper'

class Queries::PaymentPlansTest < ActiveSupport::TestCase
  def perform(query, context = {})
    BackendSchema.execute(query, context: context).to_h
  end

  context 'when current user has gendev permissions' do
    setup do 
      @current_user = create(:user_with_role_gendev)
      @context = { 
        current_user: @current_user,
        current_ability: Ability.new(@current_user)
      }
    end 

    context 'when ask for multiples attrs' do 
      setup do 
        @query_string = <<-GRAPHQL
          query {
            paymentPlans {
              id
              paymentMethods
              contract {
                id
                state
                contractType
                property {
                  id
                  propertyType
                  state
                  description
                }
              }
            }
          }
        GRAPHQL
      end 

      context 'when there arent payment plans' do 
        should 'return empty array' do 
          response = perform(@query_string, @context)
          assert_empty response["data"]["paymentPlans"]
        end
      end
      
      context 'when there are payment plans' do
        setup do 
          @payment_plan1 = create(:payment_plan)
          @payment_plan2 = create(:payment_plan)
        end
        
        should 'return multiple payment plans' do 
          response = perform(@query_string, @context)
          assert response["data"]["paymentPlans"]
          assert_operator response["data"]["paymentPlans"].length, :>, 1
          assert_equal response["data"]["paymentPlans"].length, PaymentPlan.all.length
        end 

        should 'return attrs by payment plan' do 
          response = perform(@query_string, @context)
          assert_equal response["data"]["paymentPlans"][0]["id"].to_i, @payment_plan1.id
          assert_equal response["data"]["paymentPlans"][0]["paymentMethods"], @payment_plan1.payment_methods
          assert_equal response["data"]["paymentPlans"][0]["contract"]["id"].to_i, @payment_plan1.contract.id
          assert_equal response["data"]["paymentPlans"][0]["contract"]["state"], @payment_plan1.contract.state
          assert_equal response["data"]["paymentPlans"][0]["contract"]["contractType"], @payment_plan1.contract.contract_type
          assert_equal response["data"]["paymentPlans"][0]["contract"]["property"]["id"].to_i, @payment_plan1.contract.property.id
          assert_equal response["data"]["paymentPlans"][0]["contract"]["property"]["propertyType"], @payment_plan1.contract.property.property_type
          assert_equal response["data"]["paymentPlans"][0]["contract"]["property"]["state"], @payment_plan1.contract.property.state
          assert_equal response["data"]["paymentPlans"][0]["contract"]["property"]["description"], @payment_plan1.contract.property.description
          
          assert_equal response["data"]["paymentPlans"][1]["id"].to_i, @payment_plan2.id
          assert_equal response["data"]["paymentPlans"][1]["paymentMethods"], @payment_plan2.payment_methods
          assert_equal response["data"]["paymentPlans"][1]["contract"]["id"].to_i, @payment_plan2.contract.id
          assert_equal response["data"]["paymentPlans"][1]["contract"]["state"], @payment_plan2.contract.state
          assert_equal response["data"]["paymentPlans"][1]["contract"]["contractType"], @payment_plan2.contract.contract_type
          assert_equal response["data"]["paymentPlans"][1]["contract"]["property"]["id"].to_i, @payment_plan2.contract.property.id
          assert_equal response["data"]["paymentPlans"][1]["contract"]["property"]["propertyType"], @payment_plan2.contract.property.property_type
          assert_equal response["data"]["paymentPlans"][1]["contract"]["property"]["state"], @payment_plan2.contract.property.state
          assert_equal response["data"]["paymentPlans"][1]["contract"]["property"]["description"], @payment_plan2.contract.property.description
        end
      end  
    end 

    context 'when ask for only 1 attr' do 
      setup do 
        @query_string = <<-GRAPHQL
          query {
            paymentPlans {
              paymentMethods
            }
          }
        GRAPHQL
      end
      
      context 'when there arent payment plans' do 
        should 'return empty array' do 
          response = perform(@query_string, @context)
          assert_empty response["data"]["paymentPlans"]
        end
      end
      
      context 'when there are payment plans' do
        setup do 
          create(:payment_plan)
          create(:payment_plan)
        end

        should 'return only 1 attr by payment plan' do 
          response = perform(@query_string, @context)
          assert response["data"]["paymentPlans"][0]["paymentMethods"]
          assert_nil response["data"]["paymentPlans"][0]["id"]
          assert response["data"]["paymentPlans"][1]["paymentMethods"]
          assert_nil response["data"]["paymentPlans"][1]["id"]
        end
      end
    end 

    context 'when use filters' do 
      setup do 
        @payment_methods = ["Some PM"]
        @query_string = <<-GRAPHQL
          query {
            paymentPlans(filters: { paymentMethods: #{@payment_methods} } ) {
              id
              paymentMethods
            }
          }
        GRAPHQL
      end 

      context 'when payment plans dont match filter' do 
        setup do 
          create(:payment_plan, payment_methods: ['Asd'] )
        end 

        should 'return empty array' do 
          response = perform(@query_string, @context)
          assert_empty response["data"]["paymentPlans"]
        end
      end

      context 'when there are payment plans matching filter' do 
        setup do 
          create(:payment_plan, payment_methods: @payment_methods)
          create(:payment_plan, payment_methods: ["Subject 2"])
          create(:payment_plan, payment_methods: ["Subject 3"])
        end 
        
        should 'return only correct payment plans' do 
          response = perform(@query_string, @context)
          assert response["data"]["paymentPlans"]
          assert_equal response["data"]["paymentPlans"].length, 1
          assert response["data"]["paymentPlans"][0]["id"]
          assert_equal response["data"]["paymentPlans"][0]["paymentMethods"], @payment_methods
        end
      end
    end
  end

  context 'when current user has admin permissions' do
    setup do 
      @current_user = create(:user_with_role_admin)
      @context = { 
        current_user: @current_user,
        current_ability: Ability.new(@current_user)
      }
    end 

    context 'when ask for multiples attrs' do 
      setup do 
        @query_string = <<-GRAPHQL
          query {
            paymentPlans {
              id
              paymentMethods
              contract {
                id
                state
                contractType
                property {
                  id
                  propertyType
                  state
                  description
                }
              }
            }
          }
        GRAPHQL
      end 

      context 'when there arent payment plans' do 
        should 'return empty array' do 
          response = perform(@query_string, @context)
          assert_empty response["data"]["paymentPlans"]
        end
      end
      
      context 'when there are payment plans' do
        setup do 
          @payment_plan1 = create(:payment_plan)
          @payment_plan2 = create(:payment_plan)
        end

        context 'when there arent payment plans related to user' do 
          should 'return empty array' do 
            response = perform(@query_string, @context)
            assert_empty response["data"]["paymentPlans"]
          end
        end 

        context 'when there are payment plans related to user' do 
          setup do 
            create(:user_agency, agency: @payment_plan1.contract.property.agency, user: @current_user)
            @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
          end 

          should 'return only related payment plans' do 
            response = perform(@query_string, @context)
            assert response["data"]["paymentPlans"]
            assert_equal response["data"]["paymentPlans"].length, 1
          end
        end 

        context 'when all payment plans are related to user' do 
          setup do 
            create(:user_agency, agency: @payment_plan1.contract.property.agency, user: @current_user)
            create(:user_agency, agency: @payment_plan2.contract.property.agency, user: @current_user)
            @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
          end 

          should 'return multiple payment plans' do 
            response = perform(@query_string, @context)
            assert response["data"]["paymentPlans"]
            assert_operator response["data"]["paymentPlans"].length, :>, 1
            assert_equal response["data"]["paymentPlans"].length, PaymentPlan.all.length
          end 

          should 'return attrs by payment plan' do 
            response = perform(@query_string, @context)
            payment_plans = response["data"]["paymentPlans"].sort_by { |x| x["id"].to_i }

            assert_equal payment_plans[0]["id"].to_i, @payment_plan1.id
            assert_equal payment_plans[0]["paymentMethods"], @payment_plan1.payment_methods
            assert_equal payment_plans[0]["contract"]["id"].to_i, @payment_plan1.contract.id
            assert_equal payment_plans[0]["contract"]["state"], @payment_plan1.contract.state
            assert_equal payment_plans[0]["contract"]["contractType"], @payment_plan1.contract.contract_type
            assert_equal payment_plans[0]["contract"]["property"]["id"].to_i, @payment_plan1.contract.property.id
            assert_equal payment_plans[0]["contract"]["property"]["propertyType"], @payment_plan1.contract.property.property_type
            assert_equal payment_plans[0]["contract"]["property"]["state"], @payment_plan1.contract.property.state
            assert_equal payment_plans[0]["contract"]["property"]["description"], @payment_plan1.contract.property.description
            
            assert_equal payment_plans[1]["id"].to_i, @payment_plan2.id
            assert_equal payment_plans[1]["paymentMethods"], @payment_plan2.payment_methods
            assert_equal payment_plans[1]["contract"]["id"].to_i, @payment_plan2.contract.id
            assert_equal payment_plans[1]["contract"]["state"], @payment_plan2.contract.state
            assert_equal payment_plans[1]["contract"]["contractType"], @payment_plan2.contract.contract_type
            assert_equal payment_plans[1]["contract"]["property"]["id"].to_i, @payment_plan2.contract.property.id
            assert_equal payment_plans[1]["contract"]["property"]["propertyType"], @payment_plan2.contract.property.property_type
            assert_equal payment_plans[1]["contract"]["property"]["state"], @payment_plan2.contract.property.state
            assert_equal payment_plans[1]["contract"]["property"]["description"], @payment_plan2.contract.property.description
          end
        end
      end  
    end 

    context 'when ask for only 1 attr' do 
      setup do 
        @query_string = <<-GRAPHQL
          query {
            paymentPlans {
              paymentMethods
            }
          }
        GRAPHQL
      end
      
      context 'when there arent payment plans' do 
        should 'return empty array' do 
          response = perform(@query_string, @context)
          assert_empty response["data"]["paymentPlans"]
        end
      end
      
      context 'when there are payment plans' do
        setup do 
          @payment_plan1 = create(:payment_plan)
          @payment_plan2 = create(:payment_plan)
        end

        context 'when there are payment plans related to user' do 
          setup do 
            create(:user_agency, agency: @payment_plan1.contract.property.agency, user: @current_user)
            @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
          end 
        
          should 'return only 1 attr by each related payment' do 
            response = perform(@query_string, @context)
            assert response["data"]["paymentPlans"]
            assert_equal response["data"]["paymentPlans"].length, 1
            assert response["data"]["paymentPlans"][0]["paymentMethods"]
            assert_nil response["data"]["paymentPlans"][0]["id"]
          end
        end 
  
        context 'when all payment plans are related to user' do 
          setup do 
            create(:user_agency, agency: @payment_plan1.contract.property.agency, user: @current_user)
            create(:user_agency, agency: @payment_plan2.contract.property.agency, user: @current_user)
            @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
          end 

          should 'return only 1 attr by payment plan' do 
            response = perform(@query_string, @context)
            assert response["data"]["paymentPlans"][0]["paymentMethods"]
            assert_nil response["data"]["paymentPlans"][0]["id"]
            assert response["data"]["paymentPlans"][1]["paymentMethods"]
            assert_nil response["data"]["paymentPlans"][1]["id"]
          end
        end
      end
    end 

    context 'when use filters' do 
      setup do 
        @payment_methods = ["Some PM"]
        @query_string = <<-GRAPHQL
          query {
            paymentPlans(filters: { paymentMethods: #{@payment_methods} } ) {
              id
              paymentMethods
            }
          }
        GRAPHQL
      end 

      context 'when payment plans dont match filter' do 
        setup do 
          create(:payment_plan, payment_methods: ["Another PM"])
        end 

        should 'return empty array' do 
          response = perform(@query_string, @context)
          assert_empty response["data"]["paymentPlans"]
        end
      end

      context 'when there are payment plans matching filter' do 
        setup do 
          @payment_plan1 = create(:payment_plan, payment_methods: @payment_methods)
          @payment_plan2 = create(:payment_plan, payment_methods: ["PM 2"])
          @payment_plan3 = create(:payment_plan, payment_methods: @payment_methods)
        end 
        
        context 'when there arent related to user' do 
          should 'return empty array' do 
            response = perform(@query_string, @context)
            assert_empty response["data"]["paymentPlans"]
          end
        end 

        context 'when there are payment plans related to user' do 
          setup do 
            create(:user_agency, agency: @payment_plan1.contract.property.agency, user: @current_user)
            @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
          end 
        
          should 'return only related and filtered payment plans' do 
            response = perform(@query_string, @context)
            assert response["data"]["paymentPlans"]
            assert_equal response["data"]["paymentPlans"].length, 1
            assert response["data"]["paymentPlans"][0]["id"]
            assert_equal response["data"]["paymentPlans"][0]["paymentMethods"], @payment_methods
          end
        end
      end
    end
  end
end