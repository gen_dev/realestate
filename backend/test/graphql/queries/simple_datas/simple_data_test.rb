require 'test_helper'

class Queries::SimpleDataTest < ActiveSupport::TestCase
  def perform(query, context = {})
    BackendSchema.execute(query, context: context).to_h
  end

  context 'when current user has gendev permissions' do
    setup do 
      @current_user = create(:user_with_role_gendev)
      @context = { 
        current_user: @current_user,
        current_ability: Ability.new(@current_user)
      }
    end 

    context 'when there arent simple datas' do 
      setup do 
        @query_string = <<-GRAPHQL
          query {
            simpleData(id: 1) {
              name
              value
              category
            }
          }
        GRAPHQL
      end 

      should 'return nil' do 
        response = perform(@query_string, @context)
        assert_nil response["data"]["simpleData"]
      end
    end

    context 'when there are simple datas' do
      setup do 
        @simple_data1 = create(:simple_data, datable: create(:user))
      end  

      context 'when ask for multiple attrs' do 
        context 'when simple data doesnt exist' do 
          setup do 
            @query_string = <<-GRAPHQL
              query {
                simpleData(id: #{@simple_data1.id + 1}) {
                  id
                  name
                  value
                  category
                }
              }
            GRAPHQL
          end

          should 'return nil' do 
            response = perform(@query_string, @context)
            assert_nil response["data"]["simpleData"]
          end
        end

        context 'when simple data exists' do
          setup do 
            @query_string = <<-GRAPHQL
              query {
                simpleData(id: #{@simple_data1.id}) {
                  id
                  name
                  value
                  category
                  datable {
                    ... on User {
                      email
                    }
                  }
                }
              }
            GRAPHQL
          end

          should 'return only attrs by simple data' do 
            response = perform(@query_string, @context)
            assert_equal response["data"]["simpleData"]["id"].to_i, @simple_data1.id
            assert_equal response["data"]["simpleData"]["name"], @simple_data1.name
            assert_equal response["data"]["simpleData"]["value"], @simple_data1.value
            assert_equal response["data"]["simpleData"]["category"], @simple_data1.category
            assert_equal response["data"]["simpleData"]["datable"]["email"], @simple_data1.datable.email
          end
        end 
      end

      context 'when ask for only 1 attr' do 
        context 'when simple data doesnt exist' do 
          setup do 
            @query_string = <<-GRAPHQL
              query {
                simpleData(id: #{@simple_data1.id + 1}) {
                  id
                  name
                  value
                  category
                }
              }
            GRAPHQL
          end

          should 'return nil' do 
            response = perform(@query_string, @context)
            assert_nil response["data"]["simpleData"]
          end
        end

        context 'when simple data exists' do
          setup do 
            @query_string = <<-GRAPHQL
              query {
                simpleData(id: #{@simple_data1.id}) {
                  name
                }
              }
            GRAPHQL
          end

          should 'return only 1 attr for specific simple data' do 
            response = perform(@query_string, @context)
            assert_equal response["data"]["simpleData"]["name"], @simple_data1.name
            assert_nil response["data"]["simpleData"]["id"]
          end
        end 
      end 
    end
  end

  context 'when current user has admin permissions' do
    setup do 
      @current_user = create(:user_with_role_admin)
      @context = { 
        current_user: @current_user,
        current_ability: Ability.new(@current_user)
      }
    end 

    context 'when there arent simple datas' do 
      setup do 
        @query_string = <<-GRAPHQL
          query {
            simpleData(id: 1) {
              name
              value
              category
            }
          }
        GRAPHQL
      end 

      should 'return nil' do 
        response = perform(@query_string, @context)
        assert_nil response["data"]["simpleData"]
      end
    end

    context 'when there are simple datas' do
      setup do 
        @simple_data1 = create(:simple_data, datable: create(:user))
      end  

      context 'when ask for multiple attrs' do 
        context 'when simple data doesnt exist' do 
          setup do 
            @query_string = <<-GRAPHQL
              query {
                simpleData(id: #{@simple_data1.id + 1}) {
                  id
                  name
                  value
                  category
                }
              }
            GRAPHQL
          end

          should 'return nil' do 
            response = perform(@query_string, @context)
            assert_nil response["data"]["simpleData"]
          end
        end

        context 'when simple data exists' do
          setup do 
            @query_string = <<-GRAPHQL
              query {
                simpleData(id: #{@simple_data1.id}) {
                  id
                  name
                  value
                  category
                  datable {
                    ... on User {
                      email
                    }
                  }
                }
              }
            GRAPHQL
          end

          context 'when isnt related to user' do 
            should 'return "unauthorized" message' do 
              response = perform(@query_string, @context)
              assert_nil response['data']['simpleData']
              assert_equal response['errors'][0]['message'], 'user unauthorized'
            end
          end 
          
          context 'when is related to user' do 
            setup do 
              @simple_data1.datable = @current_user
              @simple_data1.save
              create(:user_agency, user: @current_user)
              @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
            end

            should 'return only attrs by simple data' do 
              response = perform(@query_string, @context)
              assert_equal response["data"]["simpleData"]["id"].to_i, @simple_data1.id
              assert_equal response["data"]["simpleData"]["name"], @simple_data1.name
              assert_equal response["data"]["simpleData"]["value"], @simple_data1.value
              assert_equal response["data"]["simpleData"]["category"], @simple_data1.category
              assert_equal response["data"]["simpleData"]["datable"]["email"], @simple_data1.datable.email
            end
          end
        end 
      end 

      context 'when ask for only 1 attr' do 
        context 'when simple data doesnt exist' do 
          setup do 
            @query_string = <<-GRAPHQL
              query {
                simpleData(id: #{@simple_data1.id + 1}) {
                  id
                  name
                  value
                  category
                }
              }
            GRAPHQL
          end

          should 'return nil' do 
            response = perform(@query_string, @context)
            assert_nil response["data"]["simpleData"]
          end
        end

        context 'when simple data exists' do
          setup do 
            @query_string = <<-GRAPHQL
              query {
                simpleData(id: #{@simple_data1.id}) {
                  name
                }
              }
            GRAPHQL
          end

          context 'when isnt related to user' do 
            should 'return "unauthorized" message' do 
              response = perform(@query_string, @context)
              assert_nil response['data']['simpleData']
              assert_equal response['errors'][0]['message'], 'user unauthorized'
            end
          end 
          
          context 'when is related to user' do 
            setup do 
              @simple_data1.datable = @current_user
              @simple_data1.save
              create(:user_agency, user: @current_user)
              @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
            end

            should 'return only 1 attr for specific simple data' do 
              response = perform(@query_string, @context)
              assert_equal response["data"]["simpleData"]["name"], @simple_data1.name
              assert_nil response["data"]["simpleData"]["id"]
            end
          end
        end 
      end 
    end
  end
end