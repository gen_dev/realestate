require 'test_helper'

class Queries::SimpleDatasTest < ActiveSupport::TestCase
  def perform(query, context = {})
    BackendSchema.execute(query, context: context).to_h
  end

  context 'when current user has gendev permissions' do
    setup do 
      @current_user = create(:user_with_role_gendev)
      @context = { 
        current_user: @current_user,
        current_ability: Ability.new(@current_user)
      }
    end 

    context 'when ask for multiples attrs' do 
      setup do 
        @query_string = <<-GRAPHQL
          query {
            simpleDatas {
              id
              name
              value
              category
              datable {
                ... on User {
                  email
                }
                ... on Agency {
                  name
                }
              }
            }
          }
        GRAPHQL
      end 

      context 'when there arent simple datas' do 
        should 'return empty array' do 
          response = perform(@query_string, @context)
          assert_empty response["data"]["simpleDatas"]
        end
      end
      
      context 'when there are multiple simple datas' do
        setup do 
          @simple_data1 = create(:simple_data, datable: create(:agency))
          @simple_data2 = create(:simple_data, datable: create(:user))
        end
        
        should 'return multiple simple datas' do 
          response = perform(@query_string, @context)
          assert response["data"]["simpleDatas"]
          assert_operator response["data"]["simpleDatas"].length, :>, 1
          assert_equal response["data"]["simpleDatas"].length, SimpleData.all.length
        end 

        should 'return attrs by simple data' do 
          response = perform(@query_string, @context)
          assert_equal response["data"]["simpleDatas"][0]["id"].to_i, @simple_data1.id
          assert_equal response["data"]["simpleDatas"][0]["name"], @simple_data1.name
          assert_equal response["data"]["simpleDatas"][0]["value"], @simple_data1.value
          assert_equal response["data"]["simpleDatas"][0]["category"], @simple_data1.category
          assert_equal response["data"]["simpleDatas"][0]["datable"]["name"], @simple_data1.datable.name
          
          assert_equal response["data"]["simpleDatas"][1]["id"].to_i, @simple_data2.id
          assert_equal response["data"]["simpleDatas"][1]["name"], @simple_data2.name
          assert_equal response["data"]["simpleDatas"][1]["value"], @simple_data2.value
          assert_equal response["data"]["simpleDatas"][1]["category"], @simple_data2.category
          assert_equal response["data"]["simpleDatas"][1]["datable"]["email"], @simple_data2.datable.email
        end
      end  
    end 

    context 'when ask for only 1 attr' do 
      setup do 
        @query_string = <<-GRAPHQL
          query {
            simpleDatas {
              name
            }
          }
        GRAPHQL
      end
      
      context 'when there arent simple datas' do 
        should 'return empty array' do 
          response = perform(@query_string, @context)
          assert_empty response["data"]["simpleDatas"]
        end
      end
      
      context 'when there are simple datas' do
        setup do 
          create(:simple_data)
          create(:simple_data)
        end

        should 'return only 1 attr by simple data' do 
          response = perform(@query_string, @context)
          assert response["data"]["simpleDatas"][0]["name"]
          assert_nil response["data"]["simpleDatas"][0]["id"]
          assert response["data"]["simpleDatas"][1]["name"]
          assert_nil response["data"]["simpleDatas"][1]["id"]
        end
      end
    end 

    context 'when use filters' do 
      setup do 
        @name = "Key 0"
        @query_string = <<-GRAPHQL
          query {
            simpleDatas(filters: { name: "#{@name}" } ) {
              id
              name
              value
              category
            }
          }
        GRAPHQL
      end 

      context 'when simple datas dont match filter' do 
        setup do 
          create(:simple_data, name: "Key 1")
        end 

        should 'return empty array' do 
          response = perform(@query_string, @context)
          assert_empty response["data"]["simpleDatas"]
        end
      end

      context 'when there are simple datas matching filter' do 
        setup do 
          create(:simple_data, name: @name)
          create(:simple_data, name: "Key 2")
          create(:simple_data, name: "Key 3")
        end 
        
        should 'return only correct simple datas' do 
          response = perform(@query_string, @context)
          assert response["data"]["simpleDatas"]
          assert_equal response["data"]["simpleDatas"].length, 1
          assert response["data"]["simpleDatas"][0]["id"]
          assert_equal response["data"]["simpleDatas"][0]["name"], @name
          assert response["data"]["simpleDatas"][0]["value"]
        end
      end
    end
  end

  context 'when current user has admin permissions' do
    setup do 
      @current_user = create(:user_with_role_admin)
      @context = { 
        current_user: @current_user,
        current_ability: Ability.new(@current_user)
      }
    end 

    context 'when ask for multiples attrs' do 
      setup do 
        @query_string = <<-GRAPHQL
          query {
            simpleDatas {
              id
              name
              value
              category
              datable {
                ... on User {
                  email
                }
                ... on Agency {
                  name
                }
              }
            }
          }
        GRAPHQL
      end 

      context 'when there arent simple datas' do 
        should 'return empty array' do 
          response = perform(@query_string, @context)
          assert_empty response["data"]["simpleDatas"]
        end
      end
      
      context 'when there are multiple simple datas' do
        setup do 
          @simple_data1 = create(:simple_data, datable: create(:agency))
          @simple_data2 = create(:simple_data, datable: create(:user))
        end
        
        context 'when there arent simple datas related to user' do 
          should 'return empty array' do 
            response = perform(@query_string, @context)
            assert_empty response["data"]["simpleDatas"]
          end
        end 
      
        context 'when there are simple datas related to user' do 
          setup do 
            @simple_data1.datable = create(:user_agency, user: @current_user).agency
            @simple_data1.save
            @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
          end 
        
          should 'return only related simple datas' do 
            response = perform(@query_string, @context)
            assert response["data"]["simpleDatas"]
            assert_equal response["data"]["simpleDatas"].length, 1
          end
        end 
      
        context 'when all simple datas are related to user' do 
          setup do 
            @simple_data1.datable = create(:user_agency, user: @current_user).agency
            @simple_data1.save
            @simple_data2.datable = create(:user_agency, user: @current_user).agency
            @simple_data2.save
            @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
          end 

          should 'return multiple simple datas' do 
            response = perform(@query_string, @context)
            assert response["data"]["simpleDatas"]
            assert_operator response["data"]["simpleDatas"].length, :>, 1
            assert_equal response["data"]["simpleDatas"].length, SimpleData.all.length
          end 

          should 'return attrs by simple data' do 
            response = perform(@query_string, @context)
            simple_datas = response["data"]["simpleDatas"].sort_by { |x| x["id"].to_i }

            assert_equal simple_datas[0]["id"].to_i, @simple_data1.id
            assert_equal simple_datas[0]["name"], @simple_data1.name
            assert_equal simple_datas[0]["value"], @simple_data1.value
            assert_equal simple_datas[0]["category"], @simple_data1.category
            assert_equal simple_datas[0]["datable"]["name"], @simple_data1.datable.name
            
            assert_equal simple_datas[1]["id"].to_i, @simple_data2.id
            assert_equal simple_datas[1]["name"], @simple_data2.name
            assert_equal simple_datas[1]["value"], @simple_data2.value
            assert_equal simple_datas[1]["category"], @simple_data2.category
            assert_equal simple_datas[1]["datable"]["name"], @simple_data2.datable.name
          end
        end
      end  
    end 

    context 'when ask for only 1 attr' do 
      setup do 
        @query_string = <<-GRAPHQL
          query {
            simpleDatas {
              name
            }
          }
        GRAPHQL
      end
      
      context 'when there arent simple datas' do 
        should 'return empty array' do 
          response = perform(@query_string, @context)
          assert_empty response["data"]["simpleDatas"]
        end
      end
      
      context 'when there are simple datas' do
        setup do 
          @simple_data1 = create(:simple_data)
          @simple_data2 = create(:simple_data)
        end

        context 'when there are simple datas related to user' do 
          setup do 
            @simple_data1.datable = create(:user_agency, user: @current_user).agency
            @simple_data1.save
            @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
          end 
        
          should 'return only 1 attr by each related simple data' do 
            response = perform(@query_string, @context)
            assert response["data"]["simpleDatas"]
            assert_equal response["data"]["simpleDatas"].length, 1
            assert response["data"]["simpleDatas"][0]["name"]
            assert_nil response["data"]["simpleDatas"][0]["id"]
          end
        end 
  
        context 'when all simple datas are related to user' do 
          setup do 
            @simple_data1.datable = create(:user_agency, user: @current_user).agency
            @simple_data1.save
            @simple_data2.datable = create(:user_agency, user: @current_user).agency
            @simple_data2.save
            @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
          end 

          should 'return only 1 attr by simple data' do 
            response = perform(@query_string, @context)
            assert response["data"]["simpleDatas"][0]["name"]
            assert_nil response["data"]["simpleDatas"][0]["id"]
            assert response["data"]["simpleDatas"][1]["name"]
            assert_nil response["data"]["simpleDatas"][1]["id"]
          end
        end
      end
    end 

    context 'when use filters' do 
      setup do 
        @name = "Key 0"
        @query_string = <<-GRAPHQL
          query {
            simpleDatas(filters: { name: "#{@name}" } ) {
              id
              name
              value
              category
            }
          }
        GRAPHQL
      end 

      context 'when simple datas dont match filter' do 
        setup do 
          create(:simple_data, name: "Key 1")
        end 

        should 'return empty array' do 
          response = perform(@query_string, @context)
          assert_empty response["data"]["simpleDatas"]
        end
      end

      context 'when there are simple datas matching filter' do 
        setup do 
          @simple_data1 = create(:simple_data, name: @name)
          @simple_data2 = create(:simple_data, name: "Key 2")
          @simple_data3 = create(:simple_data, name: @name)
        end 
        
        context 'when there arent related to user' do 
          should 'return empty array' do 
            response = perform(@query_string, @context)
            assert_empty response["data"]["simpleDatas"]
          end
        end 

        context 'when there are simple datas related to user' do 
          setup do 
            @simple_data1.datable = create(:user_agency, user: @current_user).agency
            @simple_data1.save
            @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
          end 
        
          should 'return only related and filtered simple datas' do 
            response = perform(@query_string, @context)
            assert response["data"]["simpleDatas"]
            assert_equal response["data"]["simpleDatas"].length, 1
            assert response["data"]["simpleDatas"][0]["id"]
            assert_equal response["data"]["simpleDatas"][0]["name"], @name
            assert response["data"]["simpleDatas"][0]["value"]
          end
        end
      end
    end
  end
end