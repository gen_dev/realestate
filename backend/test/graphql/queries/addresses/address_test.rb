require 'test_helper'

class Queries::AddressTest < ActiveSupport::TestCase
  def perform(query, context = {})
    BackendSchema.execute(query, context: context).to_h
  end

  context 'when current user has gendev permissions' do
    setup do 
      @current_user = create(:user_with_role_gendev)
      @context = { 
        current_user: @current_user,
        current_ability: Ability.new(@current_user)
      }
    end 

    context 'when there arent addresses' do 
      setup do 
        @query_string = <<-GRAPHQL
          query {
            address(id: 1) {
              id
              street
              number
              unit
              city
              state
              country
              neighborhood
            }
          }
        GRAPHQL
      end 

      should 'return nil' do 
        response = perform(@query_string, @context)
        assert_nil response["data"]["address"]
      end
    end


    context 'when there are addresses' do
      setup do 
        @address1 = create(:address)
      end  

      context 'when ask for multiple attrs' do 
        context 'when address doesnt exist' do 
          setup do 
            @query_string = <<-GRAPHQL
              query {
                address(id: #{@address1.id + 1}) {
                  id
                  street
                  number
                  unit
                  city
                  state
                  country
                  neighborhood
                }
              }
            GRAPHQL
          end

          should 'return nil' do 
            response = perform(@query_string, @context)
            assert_nil response["data"]["address"]
          end
        end

        context 'when address exists' do
          setup do 
            @query_string = <<-GRAPHQL
              query {
                address(id: #{@address1.id}) {
                  id
                  street
                  number
                  unit
                  city
                  state
                  country
                  neighborhood
                }
              }
            GRAPHQL
          end

          should 'return only 1 attr for specific address' do 
            response = perform(@query_string, @context)
            assert_equal response["data"]["address"]["id"].to_i, @address1.id
            assert_equal response["data"]["address"]["street"], @address1.street
            assert_equal response["data"]["address"]["number"], @address1.number
            assert_equal response["data"]["address"]["unit"], @address1.unit
            assert_equal response["data"]["address"]["city"], @address1.city
            assert_equal response["data"]["address"]["state"], @address1.state
            assert_equal response["data"]["address"]["country"], @address1.country
            assert_equal response["data"]["address"]["neighborhood"], @address1.neighborhood
          end
        end 

        context 'when ask for addressable as User' do 
          setup do 
            @user = create(:user)
            @user_address = create(:address, addressable: @user)
            @query_string = <<-GRAPHQL
              query {
                address(id: #{@user_address.id}) {
                  id
                  street
                  number
                  unit
                  city
                  state
                  country
                  neighborhood
                  addressable {
                    ... on User {
                      id
                      email
                    }
                  }
                }
              }
            GRAPHQL
          end

          should 'return user fields' do 
            response = perform(@query_string, @context)
            assert_equal response["data"]["address"]["id"].to_i, @user_address.id
            assert_equal response["data"]["address"]["street"], @user_address.street
            assert_equal response["data"]["address"]["number"], @user_address.number
            assert_equal response["data"]["address"]["unit"], @user_address.unit
            assert_equal response["data"]["address"]["city"], @user_address.city
            assert_equal response["data"]["address"]["state"], @user_address.state
            assert_equal response["data"]["address"]["country"], @user_address.country
            assert_equal response["data"]["address"]["neighborhood"], @user_address.neighborhood
            assert_equal response["data"]["address"]["addressable"]["id"].to_i, @user.id
            assert_equal response["data"]["address"]["addressable"]["email"], @user.email
          end
        end

        context 'when ask for addressable as Agency' do 
          setup do 
            @agency = create(:agency)
            @agency_address = create(:address, addressable: @agency)
            @query_string = <<-GRAPHQL
              query {
                address(id: #{@agency_address.id}) {
                  id
                  street
                  number
                  unit
                  city
                  state
                  country
                  neighborhood
                  addressable {
                    ... on Agency {
                      id
                      name
                    }
                  }
                }
              }
            GRAPHQL
          end

          should 'return agency fields' do 
            response = perform(@query_string, @context)
            assert_equal response["data"]["address"]["id"].to_i, @agency_address.id
            assert_equal response["data"]["address"]["street"], @agency_address.street
            assert_equal response["data"]["address"]["number"], @agency_address.number
            assert_equal response["data"]["address"]["unit"], @agency_address.unit
            assert_equal response["data"]["address"]["city"], @agency_address.city
            assert_equal response["data"]["address"]["state"], @agency_address.state
            assert_equal response["data"]["address"]["country"], @agency_address.country
            assert_equal response["data"]["address"]["neighborhood"], @agency_address.neighborhood
            assert_equal response["data"]["address"]["addressable"]["id"].to_i, @agency.id
            assert_equal response["data"]["address"]["addressable"]["name"], @agency.name
          end
        end

        context 'when ask for addressable as Property' do 
          setup do 
            @property = create(:property)
            @property_address = create(:address, addressable: @property)
            @query_string = <<-GRAPHQL
              query {
                address(id: #{@property_address.id}) {
                  id
                  street
                  number
                  unit
                  city
                  state
                  country
                  neighborhood
                  addressable {
                    ... on Property {
                      id
                      state
                      propertyType
                      description
                    }
                  }
                }
              }
            GRAPHQL
          end

          should 'return property fields' do 
            response = perform(@query_string, @context)
            assert_equal response["data"]["address"]["id"].to_i, @property_address.id
            assert_equal response["data"]["address"]["street"], @property_address.street
            assert_equal response["data"]["address"]["number"], @property_address.number
            assert_equal response["data"]["address"]["unit"], @property_address.unit
            assert_equal response["data"]["address"]["city"], @property_address.city
            assert_equal response["data"]["address"]["state"], @property_address.state
            assert_equal response["data"]["address"]["country"], @property_address.country
            assert_equal response["data"]["address"]["neighborhood"], @property_address.neighborhood
            assert_equal response["data"]["address"]["addressable"]["id"].to_i, @property.id
            assert_equal response["data"]["address"]["addressable"]["state"], @property.state
            assert_equal response["data"]["address"]["addressable"]["propertyType"], @property.property_type
            assert_equal response["data"]["address"]["addressable"]["description"], @property.description
          end
        end
      end 

      context 'when ask for only 1 attr' do 
        context 'when address doesnt exist' do 
          setup do 
            @query_string = <<-GRAPHQL
              query {
                address(id: #{@address1.id + 1}) {
                  street 
                }
              }
            GRAPHQL
          end

          should 'return nil' do 
            response = perform(@query_string, @context)
            assert_nil response["data"]["address"]
          end
        end

        context 'when address exists' do
          setup do 
            @query_string = <<-GRAPHQL
              query {
                address(id: #{@address1.id}) {
                  street 
                }
              }
            GRAPHQL
          end

          should 'return only 1 attr for specific address' do 
            response = perform(@query_string, @context)
            assert_equal response["data"]["address"]["street"], @address1.street
            assert_nil response["data"]["address"]["id"]
          end
        end 
      end 
    end
  end

  context 'when current user has admin permissions' do
    setup do 
      @current_user = create(:user_with_role_admin)
      @context = { 
        current_user: @current_user,
        current_ability: Ability.new(@current_user)
      }
    end 

    context 'when there arent addresses' do 
      setup do 
        @query_string = <<-GRAPHQL
          query {
            address(id: 1) {
              id
              street
              number
              unit
              city
              state
              country
              neighborhood
            }
          }
        GRAPHQL
      end 

      should 'return nil' do 
        response = perform(@query_string, @context)
        assert_nil response["data"]["address"]
      end
    end


    context 'when there are addresses' do
      setup do 
        @address1 = create(:address)
      end  

      context 'when ask for multiple attrs' do 
        context 'when address doesnt exist' do 
          setup do 
            @query_string = <<-GRAPHQL
              query {
                address(id: #{@address1.id + 1}) {
                  id
                  street
                  number
                  unit
                  city
                  state
                  country
                  neighborhood
                }
              }
            GRAPHQL
          end

          should 'return nil' do 
            response = perform(@query_string, @context)
            assert_nil response["data"]["address"]
          end
        end

        context 'when address exists' do
          setup do 
            @query_string = <<-GRAPHQL
              query {
                address(id: #{@address1.id}) {
                  id
                  street
                  number
                  unit
                  city
                  state
                  country
                  neighborhood
                }
              }
            GRAPHQL
          end

          context 'when isnt related to user' do 
            should 'return "unauthorized" message' do 
              response = perform(@query_string, @context)
              assert_nil response['data']['address']
              assert_equal response['errors'][0]['message'], 'user unauthorized'
            end
          end 

          context 'when is related to user' do 
            setup do 
              @current_user.address = @address1
              @current_user.save
            end 

            should 'return only 1 attr for specific address' do 
              response = perform(@query_string, @context)
              assert_equal response["data"]["address"]["id"].to_i, @address1.id
              assert_equal response["data"]["address"]["street"], @address1.street
              assert_equal response["data"]["address"]["number"], @address1.number
              assert_equal response["data"]["address"]["unit"], @address1.unit
              assert_equal response["data"]["address"]["city"], @address1.city
              assert_equal response["data"]["address"]["state"], @address1.state
              assert_equal response["data"]["address"]["country"], @address1.country
              assert_equal response["data"]["address"]["neighborhood"], @address1.neighborhood
            end
          end 
        end 

        context 'when ask for addressable as User' do 
          setup do 
            @user = create(:user)
            @user_address = create(:address, addressable: @user)
            @query_string = <<-GRAPHQL
              query {
                address(id: #{@user_address.id}) {
                  id
                  street
                  number
                  unit
                  city
                  state
                  country
                  neighborhood
                  addressable {
                    ... on User {
                      id
                      email
                    }
                  }
                }
              }
            GRAPHQL
          end

          context 'when isnt related to user' do 
            should 'return "unauthorized" message' do 
              response = perform(@query_string, @context)
              assert_nil response['data']['address']
              assert_equal response['errors'][0]['message'], 'user unauthorized'
            end
          end 

          context 'when User is related to current user' do 
            setup do 
              @agency = create(:agency)
              create(:user_agency, user: @current_user, agency: @agency)
              create(:user_agency, user: @user, agency: @agency)
            end 

            should 'return user fields' do 
              response = perform(@query_string, @context)
              assert_equal response["data"]["address"]["id"].to_i, @user_address.id
              assert_equal response["data"]["address"]["street"], @user_address.street
              assert_equal response["data"]["address"]["number"], @user_address.number
              assert_equal response["data"]["address"]["unit"], @user_address.unit
              assert_equal response["data"]["address"]["city"], @user_address.city
              assert_equal response["data"]["address"]["state"], @user_address.state
              assert_equal response["data"]["address"]["country"], @user_address.country
              assert_equal response["data"]["address"]["neighborhood"], @user_address.neighborhood
              assert_equal response["data"]["address"]["addressable"]["id"].to_i, @user.id
              assert_equal response["data"]["address"]["addressable"]["email"], @user.email
            end
          end
        end

        context 'when ask for addressable as Agency' do 
          setup do 
            @agency = create(:agency)
            @agency_address = create(:address, addressable: @agency)
            @query_string = <<-GRAPHQL
              query {
                address(id: #{@agency_address.id}) {
                  id
                  street
                  number
                  unit
                  city
                  state
                  country
                  neighborhood
                  addressable {
                    ... on Agency {
                      id
                      name
                    }
                  }
                }
              }
            GRAPHQL
          end

          context 'when isnt related to user' do 
            should 'return "unauthorized" message' do 
              response = perform(@query_string, @context)
              assert_nil response['data']['address']
              assert_equal response['errors'][0]['message'], 'user unauthorized'
            end
          end 

          context 'when Agency is related to current user' do 
            setup do 
              create(:user_agency, user: @current_user, agency: @agency)
            end 

            should 'return agency fields' do 
              response = perform(@query_string, @context)
              assert_equal response["data"]["address"]["id"].to_i, @agency_address.id
              assert_equal response["data"]["address"]["street"], @agency_address.street
              assert_equal response["data"]["address"]["number"], @agency_address.number
              assert_equal response["data"]["address"]["unit"], @agency_address.unit
              assert_equal response["data"]["address"]["city"], @agency_address.city
              assert_equal response["data"]["address"]["state"], @agency_address.state
              assert_equal response["data"]["address"]["country"], @agency_address.country
              assert_equal response["data"]["address"]["neighborhood"], @agency_address.neighborhood
              assert_equal response["data"]["address"]["addressable"]["id"].to_i, @agency.id
              assert_equal response["data"]["address"]["addressable"]["name"], @agency.name
            end
          end
        end

        context 'when ask for addressable as Property' do 
          setup do 
            @property = create(:property)
            @property_address = create(:address, addressable: @property)
            @query_string = <<-GRAPHQL
              query {
                address(id: #{@property_address.id}) {
                  id
                  street
                  number
                  unit
                  city
                  state
                  country
                  neighborhood
                  addressable {
                    ... on Property {
                      id
                      state
                      propertyType
                      description
                    }
                  }
                }
              }
            GRAPHQL
          end

          context 'when isnt related to user' do 
            should 'return "unauthorized" message' do 
              response = perform(@query_string, @context)
              assert_nil response['data']['address']
              assert_equal response['errors'][0]['message'], 'user unauthorized'
            end
          end 

          context 'when Property is related to current user' do 
            setup do 
              create(:user_agency, user: @current_user, agency: @property.agency)
            end 

            should 'return property fields' do 
              response = perform(@query_string, @context)
              assert_equal response["data"]["address"]["id"].to_i, @property_address.id
              assert_equal response["data"]["address"]["street"], @property_address.street
              assert_equal response["data"]["address"]["number"], @property_address.number
              assert_equal response["data"]["address"]["unit"], @property_address.unit
              assert_equal response["data"]["address"]["city"], @property_address.city
              assert_equal response["data"]["address"]["state"], @property_address.state
              assert_equal response["data"]["address"]["country"], @property_address.country
              assert_equal response["data"]["address"]["neighborhood"], @property_address.neighborhood
              assert_equal response["data"]["address"]["addressable"]["id"].to_i, @property.id
              assert_equal response["data"]["address"]["addressable"]["state"], @property.state
              assert_equal response["data"]["address"]["addressable"]["propertyType"], @property.property_type
              assert_equal response["data"]["address"]["addressable"]["description"], @property.description
            end
          end
        end
      end 

      context 'when ask for only 1 attr' do 
        context 'when address doesnt exist' do 
          setup do 
            @query_string = <<-GRAPHQL
              query {
                address(id: #{@address1.id + 1}) {
                  street 
                }
              }
            GRAPHQL
          end

          should 'return nil' do 
            response = perform(@query_string, @context)
            assert_nil response["data"]["address"]
          end
        end

        context 'when address exists' do
          setup do 
            @query_string = <<-GRAPHQL
              query {
                address(id: #{@address1.id}) {
                  street 
                }
              }
            GRAPHQL
          end

          context 'when isnt related to user' do 
            should 'return "unauthorized" message' do 
              response = perform(@query_string, @context)
              assert_nil response['data']['address']
              assert_equal response['errors'][0]['message'], 'user unauthorized'
            end
          end 

          context 'when is related to user' do 
            setup do 
              @current_user.address = @address1
              @current_user.save
            end 

            should 'return only 1 attr for specific address' do 
              response = perform(@query_string, @context)
              assert_equal response["data"]["address"]["street"], @address1.street
              assert_nil response["data"]["address"]["id"]
            end
          end
        end 
      end 
    end
  end
end
