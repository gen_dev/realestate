require 'test_helper'

class Queries::AddressesTest < ActiveSupport::TestCase
  def perform(query, context = {})
    BackendSchema.execute(query, context: context).to_h
  end

  context 'when current user has gendev permissions' do
    setup do 
      @current_user = create(:user_with_role_gendev)
      @context = { 
        current_user: @current_user,
        current_ability: Ability.new(@current_user)
      }
    end 

    context 'when ask for multiples attrs' do 
      setup do 
        @query_string = <<-GRAPHQL
          query {
            addresses {
              id
              street
              number
              unit
              city
              state
              country
              neighborhood
            }
          }
        GRAPHQL
      end 

      context 'when there arent addresses' do 
        should 'return empty array' do 
          response = perform(@query_string, @context)
          assert_empty response["data"]["addresses"]
        end
      end
      
      context 'when there are addresses' do
        setup do 
          @address1 = create(:address)
          @address2 = create(:address)
        end
        
        should 'return multiple addresses' do 
          response = perform(@query_string, @context)
          assert response["data"]["addresses"]
          assert_operator response["data"]["addresses"].length, :>, 1
          assert_equal response["data"]["addresses"].length, Address.all.length
        end 

        should 'return attrs by address' do 
          response = perform(@query_string, @context)
          assert_equal response["data"]["addresses"][0]["id"].to_i, @address1.id
          assert_equal response["data"]["addresses"][0]["street"], @address1.street
          assert_equal response["data"]["addresses"][0]["number"], @address1.number
          assert_equal response["data"]["addresses"][0]["unit"], @address1.unit
          assert_equal response["data"]["addresses"][0]["city"], @address1.city
          assert_equal response["data"]["addresses"][0]["state"], @address1.state
          assert_equal response["data"]["addresses"][0]["country"], @address1.country
          assert_equal response["data"]["addresses"][0]["neighborhood"], @address1.neighborhood

          assert_equal response["data"]["addresses"][1]["id"].to_i, @address2.id
          assert_equal response["data"]["addresses"][1]["street"], @address2.street
          assert_equal response["data"]["addresses"][1]["number"], @address2.number
          assert_equal response["data"]["addresses"][1]["unit"], @address2.unit
          assert_equal response["data"]["addresses"][1]["city"], @address2.city
          assert_equal response["data"]["addresses"][1]["state"], @address2.state
          assert_equal response["data"]["addresses"][1]["country"], @address2.country
          assert_equal response["data"]["addresses"][1]["neighborhood"], @address2.neighborhood
        end     
      end
    end 

    context 'when ask for only 1 attr' do 
      setup do 
        @query_string = <<-GRAPHQL
          query {
            addresses {
              street
            }
          }
        GRAPHQL
      end
      
      context 'when there arent addresses' do 
        should 'return empty array' do 
          response = perform(@query_string, @context)
          assert_empty response["data"]["addresses"]
        end
      end
      
      context 'when there are addresses' do
        setup do 
          create(:address)
          create(:address)
        end

        should 'return only 1 attr by address' do 
          response = perform(@query_string, @context)
          assert response["data"]["addresses"][0]["street"]
          assert_nil response["data"]["addresses"][0]["id"]
          assert response["data"]["addresses"][1]["street"]
          assert_nil response["data"]["addresses"][1]["id"]
        end
      end
    end 

    context 'when addressable is different for each address' do 
      setup do 
        @agency = create(:agency)
        @agency_address = create(:address, addressable: @agency)
        
        @property = create(:property)
        @property_address = create(:address, addressable: @property)

        @user = create(:user)
        @user_address = create(:address, addressable: @user)

        @query_string = <<-GRAPHQL
          query {
            addresses {
              id
              street
              number
              unit
              city
              state
              country
              neighborhood
              addressable {
                ... on Agency {
                  id
                  name
                }
                ... on Property {
                  id
                  state
                  propertyType
                  description
                }
                ... on User {
                  id
                  email
                }
              }
            }
          }
        GRAPHQL
      end

      # Test of polymorphic model addressable (can be Agency / Property / User)
      should 'return correct attributes for each addressable' do 
        response = perform(@query_string, @context)
        addresses = response["data"]["addresses"].sort_by { |x| x["id"].to_i }

        assert_equal addresses[0]["id"].to_i, @agency_address.id
        assert_equal addresses[0]["addressable"]["id"].to_i, @agency.id
        assert_equal addresses[0]["addressable"]["name"], @agency.name

        assert_equal addresses[1]["addressable"]["id"].to_i, @property.id
        assert_equal addresses[1]["addressable"]["state"], @property.state
        assert_equal addresses[1]["addressable"]["propertyType"], @property.property_type
        assert_equal addresses[1]["addressable"]["description"], @property.description

        assert_equal addresses[2]["id"].to_i, @user_address.id
        assert_equal addresses[2]["addressable"]["id"].to_i, @user.id
        assert_equal addresses[2]["addressable"]["email"], @user.email
      end
    end

    context 'when use filters' do 
      setup do 
        @number = '666'
        @query_string = <<-GRAPHQL
          query {
            addresses(filters: { number: "#{@number}" } ) {
                id
                street
                number
                unit
                city
                state
                country
                neighborhood
                addressable {
                  ... on Agency {
                    id
                    name
                  }
                  ... on Property {
                    id
                    state
                    propertyType
                    description
                  }
                  ... on User {
                    id
                    email
                  }
                }
              }
          }
        GRAPHQL
      end 

      context 'when addresses dont match filter' do 
        setup do 
          create(:address, number: "777")
        end 

        should 'return empty array' do 
          response = perform(@query_string, @context)
          assert_empty response["data"]["addresses"]
        end
      end

      context 'when there are addresses matching filter' do 
        setup do 
          create(:address, number: @number)
          create(:address, number: "123")
          create(:address, number: "345")
        end 
        
        should 'return only correct addresses' do 
          response = perform(@query_string, @context)
          assert response["data"]["addresses"]
          assert_equal response["data"]["addresses"].length, 1
          assert response["data"]["addresses"][0]["id"]
          assert response["data"]["addresses"][0]["street"]
          assert_equal response["data"]["addresses"][0]["number"], @number
          assert response["data"]["addresses"][0]["unit"]
          assert response["data"]["addresses"][0]["city"]
          assert response["data"]["addresses"][0]["state"]
          assert response["data"]["addresses"][0]["country"]
        end
      end
    end 
  end

  context 'when current user has admin permissions' do
    setup do 
      @current_user = create(:user_with_role_admin)
      @context = { 
        current_user: @current_user,
        current_ability: Ability.new(@current_user)
      }
    end 

    context 'when ask for multiples attrs' do 
      setup do 
        @query_string = <<-GRAPHQL
          query {
            addresses {
              id
              street
              number
              unit
              city
              state
              country
              neighborhood
            }
          }
        GRAPHQL
      end 

      context 'when there arent addresses' do 
        should 'return empty array' do 
          response = perform(@query_string, @context)
          assert_empty response["data"]["addresses"]
        end
      end
      
      context 'when there are addresses' do
        setup do 
          @address1 = create(:address)
          @address2 = create(:address)
        end

        context 'when there arent addresses related to user' do 
          should 'return empty array' do 
            response = perform(@query_string, @context)
            assert_empty response["data"]["addresses"]
          end
        end 

        context 'when there are addresses related to user' do 
          setup do 
            @current_user.address = @address1
            @current_user.save
          end 
        
          should 'return only related addresses' do 
            response = perform(@query_string, @context)
            assert response["data"]["addresses"]
            assert_equal response["data"]["addresses"].length, 1
          end
        end 

        context 'when all addresses are related to user' do 
          setup do 
            @agency = create(:agency)
            @address1.addressable = @agency
            @address1.save
            @address2.addressable = @agency
            @address2.save
            create(:user_agency, user: @current_user, agency: @agency)
            @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
          end 
          
          should 'return multiple addresses' do 
            response = perform(@query_string, @context)
            assert response["data"]["addresses"]
            assert_operator response["data"]["addresses"].length, :>, 1
            assert_equal response["data"]["addresses"].length, Address.all.length
          end 

          should 'return attrs by address' do 
            response = perform(@query_string, @context)
            addresses = response["data"]["addresses"].sort_by { |x| x["id"].to_i }

            assert_equal addresses[0]["id"].to_i, @address1.id
            assert_equal addresses[0]["street"], @address1.street
            assert_equal addresses[0]["number"], @address1.number
            assert_equal addresses[0]["unit"], @address1.unit
            assert_equal addresses[0]["city"], @address1.city
            assert_equal addresses[0]["state"], @address1.state
            assert_equal addresses[0]["country"], @address1.country
            assert_equal addresses[0]["neighborhood"], @address1.neighborhood

            assert_equal addresses[1]["id"].to_i, @address2.id
            assert_equal addresses[1]["street"], @address2.street
            assert_equal addresses[1]["number"], @address2.number
            assert_equal addresses[1]["unit"], @address2.unit
            assert_equal addresses[1]["city"], @address2.city
            assert_equal addresses[1]["state"], @address2.state
            assert_equal addresses[1]["country"], @address2.country
            assert_equal addresses[1]["neighborhood"], @address2.neighborhood
          end
        end     
      end
    end 

    context 'when ask for only 1 attr' do 
      setup do 
        @query_string = <<-GRAPHQL
          query {
            addresses {
              street
            }
          }
        GRAPHQL
      end
      
      context 'when there arent addresses' do 
        should 'return empty array' do 
          response = perform(@query_string, @context)
          assert_empty response["data"]["addresses"]
        end
      end
      
      context 'when there are addresses' do
        setup do 
          @address1 = create(:address)
          @address2 = create(:address)
        end

        context 'when there arent addresses related to user' do 
          should 'return empty array' do 
            response = perform(@query_string, @context)
            assert_empty response["data"]["addresses"]
          end
        end 

        context 'when there are addresses related to user' do 
          setup do 
            @current_user.address = @address1
            @current_user.save
          end 
        
          should 'return only 1 attr by each related address' do 
            response = perform(@query_string, @context)
            assert response["data"]["addresses"]
            assert_equal response["data"]["addresses"].length, 1
            assert response["data"]["addresses"][0]["street"]
            assert_nil response["data"]["addresses"][0]["id"]
          end
        end 

        context 'when all addresses are related to user' do 
          setup do 
            @agency = create(:agency)
            @address1.addressable = @agency
            @address1.save
            @address2.addressable = @agency
            @address2.save
            create(:user_agency, user: @current_user, agency: @agency)
            @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
          end 
            
          should 'return only 1 attr by address' do 
            response = perform(@query_string, @context)
            assert response["data"]["addresses"][0]["street"]
            assert_nil response["data"]["addresses"][0]["id"]
            assert response["data"]["addresses"][1]["street"]
            assert_nil response["data"]["addresses"][1]["id"]
          end
        end
      end
    end 

    context 'when addressable is different for each address' do 
      setup do 
        @agency = create(:agency)
        @agency_address = create(:address, addressable: @agency)
        
        @property = create(:property, agency: @agency)
        @property_address = create(:address, addressable: @property)

        @user_address = create(:address, addressable: @current_user)
        
        create(:user_agency, user: @current_user, agency: @agency)
        @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id

        @query_string = <<-GRAPHQL
          query {
            addresses {
              id
              street
              number
              unit
              city
              state
              country
              neighborhood
              addressable {
                ... on Agency {
                  id
                  name
                }
                ... on Property {
                  id
                  state
                  propertyType
                  description
                }
                ... on User {
                  id
                  email
                }
              }
            }
          }
        GRAPHQL
      end

      # Test of polymorphic model addressable (can be Agency / Property / User)
      should 'return correct attributes for each addressable' do 
        response = perform(@query_string, @context)
        addresses = response["data"]["addresses"].sort_by { |x| x["id"].to_i }

        assert_equal addresses[0]["id"].to_i, @agency_address.id
        assert_equal addresses[0]["addressable"]["id"].to_i, @agency.id
        assert_equal addresses[0]["addressable"]["name"], @agency.name

        assert_equal addresses[1]["addressable"]["id"].to_i, @property.id
        assert_equal addresses[1]["addressable"]["state"], @property.state
        assert_equal addresses[1]["addressable"]["propertyType"], @property.property_type
        assert_equal addresses[1]["addressable"]["description"], @property.description

        assert_equal addresses[2]["id"].to_i, @user_address.id
        assert_equal addresses[2]["addressable"]["id"].to_i, @current_user.id
        assert_equal addresses[2]["addressable"]["email"], @current_user.email
      end
    end

    context 'when use filters' do 
      setup do 
        @number = '666'
        @query_string = <<-GRAPHQL
          query {
            addresses(filters: { number: "#{@number}" } ) {
                id
                street
                number
                unit
                city
                state
                country
                neighborhood
                addressable {
                  ... on Agency {
                    id
                    name
                  }
                  ... on Property {
                    id
                    state
                    propertyType
                    description
                  }
                  ... on User {
                    id
                    email
                  }
                }
              }
          }
        GRAPHQL
      end 

      context 'when addresses dont match filter' do 
        setup do 
          create(:address, number: "777")
        end 

        should 'return empty array' do 
          response = perform(@query_string, @context)
          assert_empty response["data"]["addresses"]
        end
      end

      context 'when there are addresses matching filter' do 
        setup do 
          @address1 = create(:address, number: @number)
          @address2 = create(:address, number: @number)
          @address3 = create(:address, number: "345")
        end 

        context 'when there arent related to user' do 
          should 'return empty array' do 
            response = perform(@query_string, @context)
            assert_empty response["data"]["addresses"]
          end
        end 

        context 'when there are addresses related to user' do 
          setup do 
            @current_user.address = @address1
            @current_user.save
          end 
        
          should 'return only related and filtered addresses' do 
            response = perform(@query_string, @context)
            assert response["data"]["addresses"]
            assert_equal response["data"]["addresses"].length, 1
            assert response["data"]["addresses"][0]["id"]
            assert response["data"]["addresses"][0]["street"]
            assert_equal response["data"]["addresses"][0]["number"], @number
            assert response["data"]["addresses"][0]["unit"]
            assert response["data"]["addresses"][0]["city"]
            assert response["data"]["addresses"][0]["state"]
            assert response["data"]["addresses"][0]["country"]
          end
      end
      end
    end 
  end
end
