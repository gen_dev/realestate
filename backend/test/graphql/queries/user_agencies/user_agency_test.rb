require 'test_helper'

class Queries::UserAgencyTest < ActiveSupport::TestCase
  def perform(query, context = {})
    BackendSchema.execute(query, context: context).to_h
  end

  context 'when current user has gendev permissions' do
    setup do
      @current_user = create(:user_with_role_gendev)
      @context = {
        current_user: @current_user,
        current_ability: Ability.new(@current_user)
      }
    end

    context 'when there are user agencies' do
      setup do
        @user_agency1 = create(:user_agency)
      end

      context 'when ask for multiple attrs' do
        context 'when user agency doesnt exist' do
          setup do
            @query_string = <<-GRAPHQL
              query {
                userAgency(id: #{@user_agency1.id + 1}) {
                  id
                  selected
                  user {
                    id
                    email
                  }
                  agency {
                    id
                    name
                  }
                }
              }
            GRAPHQL
          end

          should 'return nil' do
            response = perform(@query_string, @context)
            assert_nil response["data"]["userAgency"]
          end
        end

        context 'when user agency exists' do
          setup do
            @query_string = <<-GRAPHQL
              query {
                userAgency(id: #{@user_agency1.id}) {
                  id
                  selected
                  user {
                    id
                    email
                  }
                  agency {
                    id
                    name
                  }
                }
              }
            GRAPHQL
          end

          should 'return attrs by user agency' do
            response = perform(@query_string, @context)
            assert_equal response["data"]["userAgency"]["selected"], @user_agency1.selected
            assert_equal response["data"]["userAgency"]["id"].to_i, @user_agency1.id
            assert_equal response["data"]["userAgency"]["user"]["id"].to_i, @user_agency1.user.id
            assert_equal response["data"]["userAgency"]["user"]["email"], @user_agency1.user.email
            assert_equal response["data"]["userAgency"]["agency"]["id"].to_i, @user_agency1.agency.id
            assert_equal response["data"]["userAgency"]["agency"]["name"], @user_agency1.agency.name
          end
        end
      end

      context 'when ask for only 1 attr' do
        context 'when user agency doesnt exist' do
          setup do
            @query_string = <<-GRAPHQL
              query {
                userAgency(id: #{@user_agency1.id + 1}) {
                  selected
                }
              }
            GRAPHQL
          end

          should 'return nil' do
            response = perform(@query_string, @context)
            assert_nil response["data"]["userAgency"]
          end
        end

        context 'when user user agency exists' do
          setup do
            @query_string = <<-GRAPHQL
              query {
                userAgency(id: #{@user_agency1.id}) {
                  selected
                }
              }
            GRAPHQL
          end

          should 'return only 1 attr for specific user agency' do
            response = perform(@query_string, @context)
            assert_equal response["data"]["userAgency"]["selected"], @user_agency1.selected
            assert_nil response["data"]["userAgency"]["id"]
          end
        end
      end
    end
  end

  context 'when current user has admin permissions' do
    setup do
      @current_user = create(:user_with_role_admin)
      @context = {
        current_user: @current_user,
        current_ability: Ability.new(@current_user)
      }
    end

    context 'when there are user agencies' do
      setup do
        @user_agency1 = create(:user_agency)
      end

      context 'when ask for multiple attrs' do
        context 'when user agency doesnt exist' do
          setup do
            @query_string = <<-GRAPHQL
              query {
                userAgency(id: #{@user_agency1.id + 1}) {
                  id
                  selected
                  user {
                    id
                    email
                  }
                  agency {
                    id
                    name
                  }
                }
              }
            GRAPHQL
          end

          should 'return nil' do
            response = perform(@query_string, @context)
            assert_nil response["data"]["userAgency"]
          end
        end

        context 'when user agency exists' do
          setup do
            @query_string = <<-GRAPHQL
              query {
                userAgency(id: #{@user_agency1.id}) {
                  id
                  selected
                  user {
                    id
                    email
                  }
                  agency {
                    id
                    name
                  }
                }
              }
            GRAPHQL
          end

          context 'when isnt related to user' do
            should 'return "unauthorized" message' do
              response = perform(@query_string, @context)
              assert_nil response['data']['userAgency']
              assert_equal response['errors'][0]['message'], 'user unauthorized'
            end
          end

          context 'when is related to user' do
            setup do
              create(:user_agency, agency: @user_agency1.agency, user: @current_user)
              @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
            end

            should 'return attrs by user agency' do
              response = perform(@query_string, @context)
              assert_equal response["data"]["userAgency"]["selected"], @user_agency1.selected
              assert_equal response["data"]["userAgency"]["id"].to_i, @user_agency1.id
              assert_equal response["data"]["userAgency"]["user"]["id"].to_i, @user_agency1.user.id
              assert_equal response["data"]["userAgency"]["user"]["email"], @user_agency1.user.email
              assert_equal response["data"]["userAgency"]["agency"]["id"].to_i, @user_agency1.agency.id
              assert_equal response["data"]["userAgency"]["agency"]["name"], @user_agency1.agency.name
            end
          end
        end
      end

      context 'when ask for only 1 attr' do
        context 'when user agency doesnt exist' do
          setup do
            @query_string = <<-GRAPHQL
              query {
                userAgency(id: #{@user_agency1.id + 1}) {
                  selected
                }
              }
            GRAPHQL
          end

          should 'return nil' do
            response = perform(@query_string, @context)
            assert_equal response['errors'][0]['message'], 'user unauthorized'
          end
        end

        context 'when user user agency exists' do
          setup do
            @query_string = <<-GRAPHQL
              query {
                userAgency(id: #{@user_agency1.id}) {
                  selected
                }
              }
            GRAPHQL
          end

          context 'when isnt related to user' do
            should 'return "unauthorized" message' do
              response = perform(@query_string, @context)
              assert_equal response['errors'][0]['message'], 'user unauthorized'
            end
          end

          context 'when is related to user' do
            setup do
              create(:user_agency, agency: @user_agency1.agency, user: @current_user)
              @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
            end

            should 'return only 1 attr for specific user agency' do
              response = perform(@query_string, @context)
              assert_equal response["data"]["userAgency"]["selected"], @user_agency1.selected
              assert_nil response["data"]["userAgency"]["id"]
            end
          end
        end
      end
    end
  end
end