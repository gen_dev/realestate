require 'test_helper'

class Queries::UserAgenciesTest < ActiveSupport::TestCase
  def perform(query, context = {})
    BackendSchema.execute(query, context: context).to_h
  end

  context 'when current user has gendev permissions' do
    setup do 
      @current_user = create(:user_with_role_gendev)
      @context = { 
        current_user: @current_user,
        current_ability: Ability.new(@current_user)
      }
    end 

    context 'when ask for multiples attrs' do 
      setup do 
        @query_string = <<-GRAPHQL
          query {
            userAgencies {
              id
              selected
              user {
                id
                email
              }
              agency {
                id
                name
              }
            }
          }
        GRAPHQL
      end 

      context 'when there are user agencies' do
        setup do 
          @user_agency1 = create(:user_agency, user: @current_user)
          @user_agency2 = create(:user_agency, user: @current_user)
        end
        
        should 'return multiple user agencies' do 
          response = perform(@query_string, @context)
          assert response["data"]["userAgencies"]
          assert_operator response["data"]["userAgencies"].length, :>, 1
          assert_equal response["data"]["userAgencies"].length, Agency.all.length
        end 

        should 'return attrs by user agency' do 
          response = perform(@query_string, @context)
          assert_equal response["data"]["userAgencies"][0]["selected"], @user_agency1.selected
          assert_equal response["data"]["userAgencies"][0]["id"].to_i, @user_agency1.id
          assert_equal response["data"]["userAgencies"][0]["user"]["id"].to_i, @user_agency1.user.id
          assert_equal response["data"]["userAgencies"][0]["user"]["email"], @user_agency1.user.email
          assert_equal response["data"]["userAgencies"][0]["agency"]["id"].to_i, @user_agency1.agency.id
          assert_equal response["data"]["userAgencies"][0]["agency"]["name"], @user_agency1.agency.name

          assert_equal response["data"]["userAgencies"][1]["selected"], @user_agency2.selected
          assert_equal response["data"]["userAgencies"][1]["id"].to_i, @user_agency2.id
          assert_equal response["data"]["userAgencies"][1]["user"]["id"].to_i, @user_agency2.user.id
          assert_equal response["data"]["userAgencies"][1]["user"]["email"], @user_agency2.user.email
          assert_equal response["data"]["userAgencies"][1]["agency"]["id"].to_i, @user_agency2.agency.id
          assert_equal response["data"]["userAgencies"][1]["agency"]["name"], @user_agency2.agency.name
        end
      end  
    end 

    context 'when ask for only 1 attr' do 
      setup do 
        @query_string = <<-GRAPHQL
          query {
            userAgencies {
              selected
            }
          }
        GRAPHQL
      end
 
      context 'when there are user agencies' do
        setup do 
          create(:user_agency, user: @current_user)
          create(:user_agency, user: @current_user)
        end

        should 'return only 1 attr by agency' do 
          response = perform(@query_string, @context)
          assert response["data"]["userAgencies"][0]["selected"]
          assert_nil response["data"]["userAgencies"][0]["id"]
          assert response["data"]["userAgencies"][1]["selected"]
          assert_nil response["data"]["userAgencies"][1]["id"]
        end
      end
    end 

    context 'when use filters' do 
      setup do 
        @selected = true
        @query_string = <<-GRAPHQL
          query {
            userAgencies(filters: { selected: #{@selected} } ) {
              id
              selected
            }
          }
        GRAPHQL
      end 

      context 'when user agencies dont match filter' do 
        setup do 
          create(:user_agency, selected: false, user: @current_user)
        end 

        should 'return empty array' do 
          response = perform(@query_string, @context)
          assert_empty response["data"]["userAgencies"]
        end
      end

      context 'when there are user agencies matching filter' do 
        setup do 
          create(:user_agency, selected: @selected, user: @current_user)
          create(:user_agency, selected: false, user: @current_user)
          create(:user_agency, selected: false, user: @current_user)
        end 
        
        should 'return only correct user agencies' do 
          response = perform(@query_string, @context)
          assert response["data"]["userAgencies"]
          assert_equal response["data"]["userAgencies"].length, 1
          assert response["data"]["userAgencies"][0]["id"]
          assert_equal response["data"]["userAgencies"][0]["selected"], @selected
        end
      end
    end
  end

  context 'when current user has admin permissions' do
    setup do 
      @current_user = create(:user_with_role_admin)
      @context = { 
        current_user: @current_user,
        current_ability: Ability.new(@current_user)
      }
    end 

    context 'when ask for multiples attrs' do 
      setup do 
        @query_string = <<-GRAPHQL
          query {
            userAgencies {
              id
              selected
              user {
                id
                email
              }
              agency {
                id
                name
              }
            }
          }
        GRAPHQL
      end 

      context 'when there are user agencies' do
        setup do 
          @user_agency1 = create(:user_agency)
          @user_agency2 = create(:user_agency)
        end
        
        context 'when there arent user agencies related to user' do 
          should 'return empty array' do 
            response = perform(@query_string, @context)
            assert_empty response["data"]["userAgencies"]
          end
        end 

        context 'when there are user agencies related to user' do 
          setup do 
            @user_agency1.update(user: @current_user)
            @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
          end 

          should 'return only related user agencies' do 
            response = perform(@query_string, @context)
            assert response["data"]["userAgencies"]
            assert_equal response["data"]["userAgencies"].length, 1
          end
        end 

        context 'when all user agencies are related to user' do 
          setup do 
            @user_agency1.update(user: @current_user)
            @user_agency2.update(user: @current_user)
            @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
          end 

          should 'return multiple user agencies' do 
            response = perform(@query_string, @context)
            assert response["data"]["userAgencies"]
            assert_operator response["data"]["userAgencies"].length, :>, 1
            assert_equal response["data"]["userAgencies"].length, UserAgency.all.length
          end 

          should 'return attrs by user agency' do 
            response = perform(@query_string, @context)
            user_agencies = response["data"]["userAgencies"].sort_by { |x| x["id"].to_i }

            assert_equal user_agencies[0]["selected"], @user_agency1.selected
            assert_equal user_agencies[0]["id"].to_i, @user_agency1.id
            assert_equal user_agencies[0]["user"]["id"].to_i, @user_agency1.user.id
            assert_equal user_agencies[0]["user"]["email"], @user_agency1.user.email
            assert_equal user_agencies[0]["agency"]["id"].to_i, @user_agency1.agency.id
            assert_equal user_agencies[0]["agency"]["name"], @user_agency1.agency.name

            assert_equal user_agencies[1]["selected"], @user_agency2.selected
            assert_equal user_agencies[1]["id"].to_i, @user_agency2.id
            assert_equal user_agencies[1]["user"]["id"].to_i, @user_agency2.user.id
            assert_equal user_agencies[1]["user"]["email"], @user_agency2.user.email
            assert_equal user_agencies[1]["agency"]["id"].to_i, @user_agency2.agency.id
            assert_equal user_agencies[1]["agency"]["name"], @user_agency2.agency.name
          end
        end
      end  
    end 

    context 'when ask for only 1 attr' do 
      setup do 
        @query_string = <<-GRAPHQL
          query {
            userAgencies {
              selected
            }
          }
        GRAPHQL
      end
 
      context 'when there are user agencies' do
        setup do 
          @user_agency1 = create(:user_agency)
          @user_agency2 = create(:user_agency)
        end

        context 'when there are user agencies related to user' do 
          setup do 
            @user_agency1.update(user: @current_user)
            @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
          end 
        
          should 'return only 1 attr by each related agency' do 
            response = perform(@query_string, @context)
            assert response["data"]["userAgencies"]
            assert_equal response["data"]["userAgencies"].length, 1
            assert response["data"]["userAgencies"][0]["selected"]
            assert_nil response["data"]["userAgencies"][0]["id"]
          end
        end 
  
        context 'when all user agencies are related to user' do 
          setup do 
            @user_agency1.update(user: @current_user)
            @user_agency2.update(user: @current_user)
            @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
          end 
  
          should 'return only 1 attr by user agency' do 
            response = perform(@query_string, @context)
            assert response["data"]["userAgencies"][0]["selected"]
            assert_nil response["data"]["userAgencies"][0]["id"]
            assert response["data"]["userAgencies"][1]["selected"]
            assert_nil response["data"]["userAgencies"][1]["id"]
          end
        end
      end
    end 

    context 'when use filters' do 
      setup do 
        @selected = true
        @query_string = <<-GRAPHQL
          query {
            userAgencies(filters: { selected: #{@selected} } ) {
              id
              selected
            }
          }
        GRAPHQL
      end 

      context 'when user agencies dont match filter' do 
        setup do 
          create(:user_agency, selected: false)
        end 

        should 'return empty array' do 
          response = perform(@query_string, @context)
          assert_empty response["data"]["userAgencies"]
        end
      end

      context 'when there are user agencies matching filter' do 
        setup do 
          @user_agency1 = create(:user_agency, selected: @selected)
          @user_agency2 = create(:user_agency, selected: false)
          @user_agency3 = create(:user_agency, selected: @selected)
        end 

        context 'when there arent related to user' do 
          should 'return empty array' do 
            response = perform(@query_string, @context)
            assert_empty response["data"]["userAgencies"]
          end
        end 

        context 'when there are agencies related to user' do 
          setup do 
            @user_agency1.update(user: @current_user)
            @user_agency2.update(user: @current_user)
            @user_agency3.update(user: @current_user)
            @context[:current_ability] = Ability.new(@current_user) # reload permissions w/agency id
          end 
        
          should 'return only related and filtered user agencies' do 
            response = perform(@query_string, @context)
            assert response["data"]["userAgencies"]
            assert_equal response["data"]["userAgencies"].length, 2 # there are 2 because of "setup" of this context
            assert response["data"]["userAgencies"][0]["id"]
            assert_equal response["data"]["userAgencies"][0]["selected"], @selected
          end
        end
      end
    end
  end
end