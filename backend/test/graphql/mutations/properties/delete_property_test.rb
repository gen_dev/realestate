require 'test_helper'

class Mutations::Properties::DeletePropertyTest < ActiveSupport::TestCase
  def perform(current_user, args = {})
    Mutations::Properties::DeleteProperty.new(object: nil, field: nil, context: { current_user: current_user }).resolve(args)
  end
  
  context 'when current user hasnt got permissions' do
    setup do 
      @current_user = create(:user)
      @property = create(:property)
    end
    
    should 'not delete any property' do
      response = perform(@current_user, { property_id: @property.id })
      assert_empty response
    end
  end
  
  context 'when current user has gendev permissions' do
    setup do 
      @current_user = create(:user_with_role_gendev)
      @property = create(:property)
    end 
    
    should 'delete any property' do
      response = perform(@current_user, { property_id: @property.id })
      assert response[:success]
    end
  end
  
  context 'when current user has admin permissions' do
    setup do 
      @current_user = create(:user_with_role_admin)
    end 
    
    context 'when property isnt related to user' do 
      setup do 
        @property = create(:property)
      end 
      
      should 'not delete it' do 
        response = perform(@current_user, { property_id: @property.id })
        assert_empty response
      end 
    end 
    
    context 'when property is related to user through payment agency' do 
      setup do 
        @property = create(:property, agency: create(:user_agency, user: @current_user).agency)
      end 

      should 'delete it' do 
        response = perform(@current_user, { property_id: @property.id })
        assert response[:success]
      end 
    end 
  end
end