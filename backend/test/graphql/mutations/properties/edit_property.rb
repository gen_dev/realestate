require 'test_helper'

class Mutations::Properties::EditPropertyTest < ActiveSupport::TestCase
  def perform(current_user, args = {})
    Mutations::Properties::EditProperty.new(object: nil, field: nil, context: { current_user: current_user }).resolve(args)
  end
  
  context 'when current user hasnt got permissions' do
    setup do 
      @current_user = create(:user)
      @property = create(:property_with_address)
      @attrs = {
        attributes: {
          property_type: 'Apartment',
          state: 'Rented',
          description: 'Lorem ipsum it su damae',
          price: 2000.20
        },
        property_id: @property.id 
      }
    end

    should 'not edit property' do
      assert_empty perform(@current_user, @attrs)
    end
  end

  context 'when current user has gendev permissions' do
    setup do 
      @current_user = create(:user_with_role_gendev)
      @property = create(:property_with_address)
      @attrs = {
        attributes: {
          property_type: 'Apartment',
          state: 'Rented',
          description: 'Lorem ipsum it su damae',
          price: 2000.20
        },
        property_id: @property.id 
      }
    end 
    
    should 'edit any property' do
      response = perform(@current_user, @attrs)
      assert response[:property]
      assert_equal response[:property].property_type, @attrs[:attributes][:property_type]
      assert_equal response[:property].state, @attrs[:attributes][:state]
      assert_equal response[:property].description, @attrs[:attributes][:description]
      assert_equal response[:property].price, sprintf('$%.2f', @attrs[:attributes][:price]).tr('.',',') 
    end
  end
  
  context 'when current user has admin permissions' do
    setup do 
      @current_user = create(:user_with_role_admin)
    end 
    
    context 'when property isnt related to user' do 
      setup do 
        @property = create(:property_with_address)
        @attrs = {
          attributes: {
            property_type: 'Apartment',
            state: 'Rented',
            description: 'Lorem ipsum it su damae',
            price: 2000.20
          },
          property_id: @property.id 
        }
      end 
      
      should 'not update it' do 
        response = perform(@current_user, @attrs)
        assert_empty response
      end 
    end 

    context 'when property is related to user through payment agency' do 
      setup do 
        @property = create(:property, agency: create(:user_agency, user: @current_user).agency)
        @attrs = {
          attributes: {
            property_type: 'Apartment',
            state: 'Rented',
            description: 'Lorem ipsum it su damae',
            price: 2000.20
          },
          property_id: @property.id 
        }
      end 

      should 'update it' do 
        response = perform(@current_user, @attrs)
        assert response[:property]
        assert_equal response[:property].property_type, @attrs[:attributes][:property_type]
        assert_equal response[:property].state, @attrs[:attributes][:state]
        assert_equal response[:property].description, @attrs[:attributes][:description]
        assert_equal response[:property].price, sprintf('$%.2f', @attrs[:attributes][:price]).tr('.',',') 
      end 
    end 
  end
end
