require 'test_helper'

class Mutations::Properties::CreatePropertyTest < ActiveSupport::TestCase
  def perform(current_user, args = {})
    Mutations::Properties::CreateProperty.new(object: nil, field: nil, context: { current_user: current_user }).resolve(args)
  end

  context 'when current user hasnt got permissions' do
    setup do 
      @current_user = create(:user)
      @owner = create(:user)
      @agency = create(:agency)
      @current_user.user_agencies.create! agency: @agency, selected: true
      @attrs = {
        attributes: {
          property_type: 'Apartment',
          state: 'Rented',
          description: 'Lorem ipsum it su damae',
          price: 2000.20
        },
        owner_id: @owner.id
      }
    end

    should 'not create property' do
      assert_empty perform(@current_user, @attrs)
    end
  end

  context 'when current user has gendev permissions' do
    setup do 
      @current_user = create(:user_with_role_gendev)
    end 

    context 'when required fields are missing' do 
      setup do 
        @attrs = {
          attributes: {
            property_type: nil,
            state: 'Rented',
            description: 'Lorem ipsum it su damae',
            price: 2000.20
          },
          owner_id: nil
        }
      end 

      should 'not create property' do
        assert_empty perform(@current_user, @attrs)
      end
    end 

    context 'when all required fields are filled' do 
      setup do 
        @owner = create(:user)
        @agency = create(:agency)
        @current_user.user_agencies.create! agency: @agency, selected: true
        @attrs = {
          attributes: {
            property_type: 'Apartment',
            state: 'Rented',
            description: 'Lorem ipsum it su damae',
            price: 2000.20
          },
          owner_id: @owner.id
        }
      end 

      should 'create property' do 
        response = perform(@current_user, @attrs)
        property = response[:property]
        assert property.persisted?
        assert_equal property.property_type, @attrs[:attributes][:property_type] 
        assert_equal property.state, @attrs[:attributes][:state] 
        assert_equal property.description, @attrs[:attributes][:description]
        assert_equal property.price, sprintf('$%.2f', @attrs[:attributes][:price]).tr('.',',') 
      end
    end
  end

  context 'when current user has admin permissions' do
    setup do 
      @current_user = create(:user_with_role_admin)
      @owner = create(:user)
    end 

    context 'when user belongs to agency' do 
      setup do 
        @user_agency = create(:user_agency, user: @current_user)
      end

      context 'when required fields are missing' do 
        setup do 
          @attrs = {
            attributes: {
              property_type: nil,
              state: 'Rented',
              description: 'Lorem ipsum it su damae',
              price: 2000.20
            },
            owner_id: @owner.id
          }
        end 

        should 'not create property' do
          assert_empty perform(@current_user, @attrs)
        end
      end 

      context 'when all required fields are filled' do 
        setup do 
          @attrs = {
            attributes: {
              property_type: 'Apartment',
              state: 'Rented',
              description: 'Lorem ipsum it su damae',
              price: 2000.20
            },
            owner_id: @owner.id
          }
        end 

        should 'create property' do 
          response = perform(@current_user, @attrs)
          property = response[:property]
          assert property.persisted?
          assert_equal property.property_type, @attrs[:attributes][:property_type] 
          assert_equal property.state, @attrs[:attributes][:state] 
          assert_equal property.description, @attrs[:attributes][:description]
          assert_equal property.price, sprintf('$%.2f', @attrs[:attributes][:price]).tr('.',',') 
        end
      end
    end

    context 'when user doesnt belongs to agency' do 
      setup do 
        @attrs = {
          attributes: {
            property_type: 'Apartment',
            state: 'Rented',
            description: 'Lorem ipsum it su damae',
            price: 2000.20
          },
          owner_id: @owner.id
        }
      end

      should 'not create property' do 
        assert_empty perform(@current_user, @attrs)
      end
    end
  end
end
