require 'test_helper'

class Mutations::SimpleDatas::DeleteSimpleDataTest < ActiveSupport::TestCase
  def perform(current_user, args = {})
    Mutations::SimpleDatas::DeleteSimpleData.new(object: nil, field: nil, context: { current_user: current_user }).resolve(args)
  end
  
  context 'when current user hasnt got permissions' do
    setup do 
      @current_user = create(:user)
      @simple_data = create(:simple_data)
    end
    
    should 'not delete any simple data' do
      response = perform(@current_user, { simple_data_id: @simple_data.id })
      assert_empty response
    end
  end
  
  context 'when current user has gendev permissions' do
    setup do 
      @current_user = create(:user_with_role_gendev)
      @simple_data = create(:simple_data)
    end 
    
    should 'delete any simple data' do
      response = perform(@current_user, { simple_data_id: @simple_data.id })
      assert response[:success]
    end
  end
  
  context 'when current user has admin permissions' do
    setup do 
      @current_user = create(:user_with_role_admin)
    end 
    
    context 'when simple data isnt related to user' do 
      setup do 
        @simple_data = create(:simple_data)
      end 
      
      should 'not delete it' do 
        response = perform(@current_user, { simple_data_id: @simple_data.id })
        assert_empty response
      end 
    end 
    
    context 'when simple data is related to user through datable' do 
      setup do 
        @simple_data = create(:simple_data, datable: create(:user_agency, user: @current_user).agency)
      end 

      should 'delete it' do 
        response = perform(@current_user, { simple_data_id: @simple_data.id })
        assert response[:success]
      end 
    end 
  end
end