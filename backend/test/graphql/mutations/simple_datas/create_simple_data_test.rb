require 'test_helper'

class Mutations::SimpleDatas::CreateSimpleDataTest < ActiveSupport::TestCase
  def perform(current_user, args = {})
    Mutations::SimpleDatas::CreateSimpleData.new(object: nil, field: nil, context: { current_user: current_user }).resolve(args)
  end

  context 'when current user hasnt got permissions' do
    setup do 
      @current_user = create(:user)
      @datable = create(:contract)
      @attrs = {
        attributes: {
          name: 'Mt2',
          value: '70',
          category: 'Studio Apartment'
        },
        datable_id: @datable.id, 
        datable_type: @datable.class.name
      }
    end

    should 'not create simple data' do
      assert_empty perform(@current_user, @attrs)
    end
  end

  context 'when current user has gendev permissions' do
    setup do 
      @current_user = create(:user_with_role_gendev)
      @datable = create(:contract)
    end

    context 'when required fields are missing' do 
      setup do 
        @attrs = {
          attributes: {
            name: 'Mt2',
            value: nil,
            category: 'Studio Apartment'
          },
          datable_id: @datable.id, 
          datable_type: @datable.class.name
        }
      end

      should 'not create simple data' do
        assert_empty perform(@current_user, @attrs)
      end
    end 

    context 'when all required fields are filled' do 
      setup do 
        @attrs = {
          attributes: {
            name: 'Mt2',
            value: '70',
            category: 'Studio Apartment'
          },
          datable_id: @datable.id, 
          datable_type: @datable.class.name
        }
      end

      should 'create simple data' do 
        response = perform(@current_user, @attrs)
        simple_data = response[:simple_data]
        assert simple_data.persisted?
        assert_equal simple_data.name, @attrs[:attributes][:name] 
        assert_equal simple_data.value, @attrs[:attributes][:value]
        assert_equal simple_data.category, @attrs[:attributes][:category]
        assert_equal simple_data.datable_id, @attrs[:datable_id]
        assert_equal simple_data.datable_type, @attrs[:datable_type]
      end
    end 
  end

  context 'when current user has admin permissions' do
    setup do 
      @current_user = create(:user_with_role_admin)
    end
    
    context 'when datable is related to user' do 
      setup do 
        @datable = create(:contract, property: create(:property, agency: create(:user_agency, user: @current_user).agency))
      end

      context 'when required fields are missing' do 
        setup do 
          @attrs = {
            attributes: {
              name: 'Mt2',
              value: nil,
              category: 'Studio Apartment'
            },
            datable_id: @datable.id, 
            datable_type: @datable.class.name
          }
        end
        
        should 'not create simple data' do
          assert_empty perform(@current_user, @attrs)
        end
      end 

      context 'when all required fields are filled' do 
        setup do 
          @attrs = {
            attributes: {
              name: 'Mt2',
              value: '70',
              category: 'Studio Apartment'
            },
            datable_id: @datable.id, 
            datable_type: @datable.class.name
          }
        end

        should 'create simple data' do 
          response = perform(@current_user, @attrs)
          simple_data = response[:simple_data]
          assert simple_data.persisted?
          assert_equal simple_data.name, @attrs[:attributes][:name] 
          assert_equal simple_data.value, @attrs[:attributes][:value]
          assert_equal simple_data.category, @attrs[:attributes][:category]
          assert_equal simple_data.datable_id, @attrs[:datable_id]
          assert_equal simple_data.datable_type, @attrs[:datable_type]
        end
      end 
    end 

    context 'when datable isnt related to user' do 
      setup do 
        @datable = create(:contract)
        @attrs = {
          attributes: {
            name: 'Mt2',
            value: '70',
            category: 'Studio Apartment'
          },
          datable_id: @datable.id, 
          datable_type: @datable.class.name
        }
      end

      should 'not create simple data' do
        assert_empty perform(@current_user, @attrs)
      end
    end
  end
end
