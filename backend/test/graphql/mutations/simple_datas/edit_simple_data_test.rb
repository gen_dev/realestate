require 'test_helper'

class Mutations::SimpleDatas::EditSimpleDataTest < ActiveSupport::TestCase
  def perform(current_user, args = {})
    Mutations::SimpleDatas::EditSimpleData.new(object: nil, field: nil, context: { current_user: current_user }).resolve(args)
  end
  
  context 'when current user hasnt got permissions' do
    setup do 
      @current_user = create(:user)
      @simple_data = create(:simple_data)
      @attrs = {
        attributes: {
          name: 'Mt2',
          value: '70',
          category: 'Studio Apartment'
        },
        simple_data_id: @simple_data.id 
      }
    end

    should 'not edit simple data' do
      assert_empty perform(@current_user, @attrs)
    end
  end

  context 'when current user has gendev permissions' do
    setup do 
      @current_user = create(:user_with_role_gendev)
      @simple_data = create(:simple_data)
      @attrs = {
        attributes: {
          name: 'Mt2',
          value: '70',
          category: 'Studio Apartment'
        },
        simple_data_id: @simple_data.id 
      }
    end 
    
    should 'edit any simple data' do
      response = perform(@current_user, @attrs)
      assert response[:simple_data]
      assert_equal response[:simple_data].name, @attrs[:attributes][:name]
      assert_equal response[:simple_data].value, @attrs[:attributes][:value]
      assert_equal response[:simple_data].category, @attrs[:attributes][:category]
    end
  end
  
  context 'when current user has admin permissions' do
    setup do 
      @current_user = create(:user_with_role_admin)
    end 
    
    context 'when simple data isnt related to user' do 
      setup do 
        @simple_data = create(:simple_data)
        @attrs = {
          attributes: {
            name: 'Mt2',
          value: '70',
          category: 'Studio Apartment'
          },
          simple_data_id: @simple_data.id 
        }
      end 
      
      should 'not update it' do 
        response = perform(@current_user, @attrs)
        assert_empty response
      end 
    end 

    context 'when simple data is related to user through datable' do 
      setup do 
        @simple_data = create(:simple_data, datable: create(:user_agency, user: @current_user).agency)
        @attrs = {
          attributes: {
            name: 'Mt2',
          value: '70',
          category: 'Studio Apartment'
          },
          simple_data_id: @simple_data.id 
        }
      end 

      should 'update it' do 
        response = perform(@current_user, @attrs)
        assert response[:simple_data]
        assert_equal response[:simple_data].name, @attrs[:attributes][:name]
        assert_equal response[:simple_data].value, @attrs[:attributes][:value]
        assert_equal response[:simple_data].category, @attrs[:attributes][:category]
      end 
    end 
  end
end
