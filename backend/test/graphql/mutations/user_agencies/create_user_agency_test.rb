require 'test_helper'

class Mutations::UserAgencies::CreateUserAgencyTest < ActiveSupport::TestCase
  def perform(current_user, args = {})
    Mutations::UserAgencies::CreateUserAgency.new(object: nil, field: nil, context: { current_user: current_user }).resolve(args)
  end

  context 'when current user hasnt got permissions' do
    setup do 
      @current_user = create(:user)
      @attrs = {
        selected: true,
        agency_id: 1,
        user_id: 1
      }
    end

    should 'not create user agency' do
      assert_empty perform(@current_user, @attrs)
    end
  end

  context 'when current user has gendev permissions' do
    setup do 
      @current_user = create(:user_with_role_gendev)
    end

    context 'when required fields are missing' do 
      setup do 
        @attrs = {
          selected: true,
          agency_id: nil,
          user_id:  @current_user.id
        }
      end 

      should 'not create user agency' do
        assert_empty perform(@current_user, @attrs)
      end
    end 

    context 'when all required fields are filled' do 
      context 'when user doesnt exist' do 
        setup do 
          @attrs = {
            selected: true,
            agency_id: create(:agency).id,
            user_id: 112312312
          }
        end 

        should 'create user agency for current user' do
          response = perform(@current_user, @attrs)
          user_agency = response[:user_agency]
        
          assert user_agency
          assert_equal user_agency.selected, @attrs[:selected]
          assert_equal user_agency.agency_id, @attrs[:agency_id]
          assert_equal user_agency.user_id, @current_user.id
        end
      end

      context 'when agency doesnt exist' do 
        setup do 
          @attrs = {
            selected: true,
            agency_id: 123423234,
            user_id: create(:user).id
          }
        end 

        should 'not create user agency' do
          assert_empty perform(@current_user, @attrs)
        end
      end

      context 'when agency and user exists' do 
        setup do 
          @attrs = {
            selected: true,
            agency_id: create(:agency).id,
            user_id: create(:user).id
          }
        end 

        should 'create user agency' do
          response = perform(@current_user, @attrs)
          user_agency = response[:user_agency]
        
          assert user_agency
          assert_equal user_agency.selected, @attrs[:selected]
          assert_equal user_agency.agency_id, @attrs[:agency_id]
          assert_equal user_agency.user_id, @attrs[:user_id]
        end
      end
    end
  end

  context 'when current user has admin permissions' do
    setup do 
      @current_user = create(:user_with_role_admin)
    end

    context 'when user agency is related to user' do 
      setup do 
        @agency = create(:agency)
        create(:user_agency, user: @current_user, agency: @agency)
      end 

      context 'when required fields are missing' do 
        setup do 
          @attrs = {
            selected: true,
            agency_id: nil,
            user_id: @current_user.id
          }
        end 

        should 'not create user agency' do
          assert_empty perform(@current_user, @attrs)
        end
      end 

      context 'when all required fields are filled' do 
        context 'when user doesnt exist' do 
          setup do 
            @attrs = {
              selected: true,
              agency_id: @agency.id,
              user_id: 112312312
            }
          end 

          should 'create user agency for current user' do
            response = perform(@current_user, @attrs)
            user_agency = response[:user_agency]
          
            assert user_agency
            assert_equal user_agency.selected, @attrs[:selected]
            assert_equal user_agency.agency_id, @attrs[:agency_id]
            assert_equal user_agency.user_id, @current_user.id
          end
        end

        context 'when user exists' do 
          setup do 
            @attrs = {
              selected: true,
              agency_id: @agency.id,
              user_id: create(:user).id
            }
          end 

          should 'create user agency' do
            response = perform(@current_user, @attrs)
            user_agency = response[:user_agency]
          
            assert user_agency
            assert_equal user_agency.selected, @attrs[:selected]
            assert_equal user_agency.agency_id, @attrs[:agency_id]
            assert_equal user_agency.user_id, @attrs[:user_id]
          end
        end
      end
    end

    context 'when user agency isnt related to user' do 
      setup do 
        @attrs = {
          selected: true,
          agency_id: create(:agency).id,
          user_id: create(:user).id
        }
      end 

      should 'not create user agency' do
        assert_empty perform(@current_user, @attrs)
      end
    end
  end
end