require 'test_helper'

class Mutations::UserAgencies::DeleteUserAgencyTest < ActiveSupport::TestCase
  def perform(current_user, args = {})
    Mutations::UserAgencies::DeleteUserAgency.new(object: nil, field: nil, context: { current_user: current_user }).resolve(args)
  end
  
  context 'when current user hasnt got permissions' do
    setup do 
      @current_user = create(:user)
      @user_agency = create(:user_agency)
    end
    
    should 'not delete any user agency' do
      response = perform(@current_user, { user_agency_id: @user_agency.id })
      assert_empty response
    end
  end
  
  context 'when current user has gendev permissions' do
    setup do 
      @current_user = create(:user_with_role_gendev)
      @user_agency = create(:user_agency)
    end 
    
    should 'delete any user agency' do
      response = perform(@current_user, { user_agency_id: @user_agency.id })
      assert response[:success]
    end
  end
  
  context 'when current user has admin permissions' do
    setup do 
      @current_user = create(:user_with_role_admin)
    end 
    
    context 'when user agency isnt related to user' do 
      setup do 
        @user_agency = create(:user_agency)
      end 
      
      should 'not delete it' do 
        response = perform(@current_user, { user_agency_id: @user_agency.id })
        assert_empty response
      end 
    end 
    
    context 'when user agency is related to user' do 
      setup do 
        @user_agency = create(:user_agency, user: @current_user)
      end 

      should 'delete it' do 
        response = perform(@current_user, { user_agency_id: @user_agency.id })
        assert response[:success]
      end 
    end 
  end
end