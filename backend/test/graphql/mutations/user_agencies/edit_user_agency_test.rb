require 'test_helper'

class Mutations::UserAgencies::EditUserAgencyTest < ActiveSupport::TestCase
  def perform(current_user, args = {})
    Mutations::UserAgencies::EditUserAgency.new(object: nil, field: nil, context: { current_user: current_user }).resolve(args)
  end
  
  context 'when current user hasnt got permissions' do
    setup do 
      @current_user = create(:user)
      @user_agency = create(:user_agency)
      @attrs = {
        attributes: {
          selected: true
        },
        user_agency_id: @user_agency.id 
      }
    end

    should 'not edit user agency' do
      assert_empty perform(@current_user, @attrs)
    end
  end

  context 'when current user has gendev permissions' do
    setup do 
      @current_user = create(:user_with_role_gendev)
      @user_agency = create(:user_agency)
      @attrs = {
        attributes: {
          selected: true
        },
        user_agency_id: @user_agency.id 
      }
    end 
    
    should 'edit any user agency' do
      response = perform(@current_user, @attrs)
      assert response[:user_agency]
      assert_equal response[:user_agency].selected, @attrs[:attributes][:selected]
    end
  end
  
  context 'when current user has admin permissions' do
    setup do 
      @current_user = create(:user_with_role_admin)
    end 
    
    context 'when user agency isnt related to user' do 
      setup do 
        @user_agency = create(:user_agency)
        @attrs = {
          attributes: {
            selected: true
          },
          user_agency_id: @user_agency.id 
        }
      end 
      
      should 'not update it' do 
        response = perform(@current_user, @attrs)
        assert_empty response
      end 
    end 

    context 'when user agency is related to user' do 
      setup do 
        @user_agency = create(:user_agency, user: @current_user)
        @attrs = {
          attributes: {
            selected: true
          },
          user_agency_id: @user_agency.id 
        }
      end 

      should 'update it' do 
        response = perform(@current_user, @attrs)
        assert response[:user_agency]
        assert_equal response[:user_agency].selected, @attrs[:attributes][:selected]
      end 
    end 
  end
end
