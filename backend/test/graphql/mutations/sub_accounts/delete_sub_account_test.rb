require 'test_helper'

class Mutations::SubAccounts::DeleteSubAccountTest < ActiveSupport::TestCase
  def perform(current_user, args = {})
    Mutations::SubAccounts::DeleteSubAccount.new(object: nil, field: nil, context: { current_user: current_user }).resolve(args)
  end
  
  context 'when current user hasnt got permissions' do
    setup do 
      @current_user = create(:user)
      @sub_account = create(:sub_account)
    end
    
    should 'not delete any sub_account' do
      response = perform(@current_user, { sub_account_id: @sub_account.id })
      assert_empty response
    end
  end
  
  context 'when current user has gendev permissions' do
    setup do 
      @current_user = create(:user_with_role_gendev)
      @sub_account = create(:sub_account)
    end 
    
    should 'delete any sub_account' do
      response = perform(@current_user, { sub_account_id: @sub_account.id })
      assert response[:success]
    end
  end
  
  context 'when current user has admin permissions' do
    setup do 
      @current_user = create(:user_with_role_admin)
    end 
    
    context 'when sub_account isnt related to user' do 
      setup do 
        @sub_account = create(:sub_account)
      end 
      
      should 'not delete it' do 
        response = perform(@current_user, { sub_account_id: @sub_account.id })
        assert_empty response
      end 
    end 
    
    context 'when sub_account is related to user through agency' do 
      setup do 
        @sub_account = create(:sub_account, agency_id: create(:user_agency, user: @current_user).agency_id)
      end 

      should 'delete it' do 
        response = perform(@current_user, { sub_account_id: @sub_account.id })
        assert response[:success]
      end 
    end 
  end
end