require 'test_helper'

class Mutations::SubAccounts::EditSubAccountTest < ActiveSupport::TestCase
  def perform(current_user, args = {})
    Mutations::SubAccounts::EditSubAccount.new(object: nil, field: nil, context: { current_user: current_user }).resolve(args)
  end
  
  context 'when current user hasnt got permissions' do
    setup do 
      @current_user = create(:user)
      @sub_account = create(:sub_account)
      @attrs = {
        attributes: {
          name: 'Some SubAccount',
          balance: 1000
        },
        sub_account_id: @sub_account.id, 
      }
    end

    should 'not edit sub_account' do
      assert_empty perform(@current_user, @attrs)
    end
  end

  context 'when current user has gendev permissions' do
    setup do 
      @current_user = create(:user_with_role_gendev)
      @sub_account = create(:sub_account)
      @attrs = {
        attributes: {
          name: 'Some SubAccount',
          balance: 1000
        },
        sub_account_id: @sub_account.id, 
      }
    end 
    
    should 'edit any sub_account' do
      response = perform(@current_user, @attrs)
      assert response[:sub_account]
      assert_equal response[:sub_account].name, @attrs[:attributes][:name]
      assert_equal response[:sub_account].balance, @attrs[:attributes][:balance]
    end
  end
  
  context 'when current user has admin permissions' do
    setup do 
      @current_user = create(:user_with_role_admin)
    end 
    
    context 'when sub_account isnt related to user' do 
      setup do 
        @sub_account = create(:sub_account)
        @attrs = {
          attributes: {
            name: 'Some SubAccount',
            balance: 1000
          },
          sub_account_id: @sub_account.id, 
        }
      end 
      
      should 'not update it' do 
        response = perform(@current_user, @attrs)
        assert_empty response
      end 
    end 
    
    context 'when sub_account is related to user' do 
      setup do 
        @sub_account = create(:sub_account, agency_id: create(:user_agency, user: @current_user).agency_id)
        @attrs = {
          attributes: {
            name: 'Some SubAccount',
            balance: 1000
          },
          sub_account_id: @sub_account.id, 
        }
      end 

      should 'update it' do 
        response = perform(@current_user, @attrs)
        assert response[:sub_account]
        assert_equal response[:sub_account].name, @attrs[:attributes][:name]
        assert_equal response[:sub_account].balance, @attrs[:attributes][:balance]
      end 
    end 
  end
end
