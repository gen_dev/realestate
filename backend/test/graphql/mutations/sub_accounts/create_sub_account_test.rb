require 'test_helper'

class Mutations::SubAccounts::CreateSubAccountTest < ActiveSupport::TestCase
  def perform(current_user, args = {})
    Mutations::SubAccounts::CreateSubAccount.new(object: nil, field: nil, context: { current_user: current_user }).resolve(args)
  end

  context 'when current user hasnt got permissions' do
    setup do 
      @current_user = create(:user)
      create(:user_agency, selected: true, user: @current_user)
      @attrs = {
        attributes: {
          name: 'Principal', 
          balance: 2000, 
        }
      }
    end

    should 'not create sub_account' do
      assert_empty perform(@current_user, @attrs)
    end
  end

  context 'when current user has gendev permissions' do
    setup do 
      @current_user = create(:user_with_role_gendev)
      create(:user_agency, selected: true, user: @current_user)
    end 

    context 'when required fields are missing' do 
      setup do 
        @attrs = {
          attributes: {
            name: 'Principal', 
            balance: nil, 
          } 
        }
      end 

      should 'not create sub_account' do
        assert_empty perform(@current_user, @attrs)
      end
    end

    context 'when all required fields are filled' do 
      setup do 
        @attrs = {
          attributes: {
            name: 'Principal', 
            balance: 2000, 
          } 
        }
      end 
      
      should 'always create sub_account' do
        response = perform(@current_user, @attrs)
        sub_account = response[:sub_account]
      
        assert sub_account.persisted?
        assert_equal sub_account.name, @attrs[:attributes][:name]
        assert_equal sub_account.balance, @attrs[:attributes][:balance]
        assert_equal sub_account.agency_id, @current_user.selected_agency.id
      end
    end
  end

  context 'when current user has admin permissions' do
    setup do 
      @current_user = create(:user_with_role_admin)
      create(:user_agency, selected: true, user: @current_user)
    end 

    context 'when user belongs to Agency' do
      setup do 
        @agency = create(:user_agency, user: @current_user).agency
      end
      
      context 'when required fields are missing' do 
        setup do 
          @attrs = {
            attributes: {
              name: 'Principal', 
              balance: nil, 
            } 
          }
        end 
  
        should 'not create sub_account' do
          assert_empty perform(@current_user, @attrs)
        end
      end

      context 'when all required fields are filled' do 
        setup do 
          @attrs = {
            attributes: {
              name: 'Principal', 
              balance: 2000, 
            } 
          }
        end 

        should 'create sub_account' do
          response = perform(@current_user, @attrs)
          sub_account = response[:sub_account]
        
          assert sub_account.persisted?
          assert_equal sub_account.name, @attrs[:attributes][:name]
          assert_equal sub_account.balance, @attrs[:attributes][:balance]
          assert_equal sub_account.agency_id, @current_user.selected_agency.id
        end
      end
    end

    context 'when user doesnt belong to Agency' do
      setup do 
        UserAgency.update_all selected: false
        @attrs = { 
          attributes: {
            name: 'Principal', 
            balance: 2000, 
          }
        }
      end

      should 'not create sub_account' do
        assert_empty perform(@current_user, @attrs)
      end
    end
  end
end
