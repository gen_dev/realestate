require 'test_helper'

class Mutations::Addresses::EditAddressTest < ActiveSupport::TestCase
  def perform(current_user, args = {})
    Mutations::Addresses::EditAddress.new(object: nil, field: nil, context: { current_user: current_user }).resolve(args)
  end
  
  context 'when current user hasnt got permissions' do
    setup do 
      @current_user = create(:user)
      @addressable = create(:user, email: 'john@doe.com', password: '[omitted]')
      @address = create(:address, addressable: @addressable)
      @attrs = {
        attributes: {
          street: 'Occasion Avenue',
          number: '666',
          unit: '3',
          city: 'New Jersey',
          state: 'New Jersey',
          country: 'USA',
          neighborhood: 'Hells Kitchen'
        },
        address_id: @address.id, 
      }
    end

    should 'not edit address' do
      assert_empty perform(@current_user, @attrs)
    end
  end

  context 'when current user has gendev permissions' do
    setup do 
      @current_user = create(:user_with_role_gendev)
      @address = create(:address)
      @attrs = {
        attributes: {
          street: 'Occasia Test Avenue',
          number: '666',
          unit: '3',
          city: 'New Jersey',
          state: 'New Jersey',
          country: 'USA',
          neighborhood: 'Hells Kitchen'
        },
        address_id: @address.id, 
      }
    end 
    
    should 'edit any address' do
      response = perform(@current_user, @attrs)
      assert response[:address]
      assert_equal response[:address].street, @attrs[:attributes][:street]
      assert_equal response[:address].number, @attrs[:attributes][:number]
      assert_equal response[:address].unit, @attrs[:attributes][:unit]
      assert_equal response[:address].city, @attrs[:attributes][:city]
      assert_equal response[:address].state, @attrs[:attributes][:state]
      assert_equal response[:address].country, @attrs[:attributes][:country]
      assert_equal response[:address].neighborhood, @attrs[:attributes][:neighborhood]
    end
  end
  
  context 'when current user has admin permissions' do
    setup do 
      @current_user = create(:user_with_role_admin)
    end 
    
    context 'when address isnt related to user' do 
      setup do 
        @address = create(:address)
        @attrs = {
          attributes: {
            street: 'Occasia Test Avenue',
            number: '666',
            unit: '3',
            city: 'New Jersey',
            state: 'New Jersey',
            country: 'USA',
            neighborhood: 'Hells Kitchen'
          },
          address_id: @address.id, 
        }
      end 
      
      should 'not update it' do 
        response = perform(@current_user, @attrs)
        assert_empty response
      end 
    end 
    
    context 'when address is related to user' do 
      setup do 
        @address = create(:address, addressable: @current_user)
        @attrs = {
          attributes: {
            street: 'Occasia Test Avenue',
            number: '666',
            unit: '3',
            city: 'New Jersey',
            state: 'New Jersey',
            country: 'USA',
            neighborhood: 'Hells Kitchen'
          },
          address_id: @address.id, 
        }
      end 

      should 'update it' do 
        response = perform(@current_user, @attrs)
        assert response[:address]
        assert_equal response[:address].street, @attrs[:attributes][:street]
        assert_equal response[:address].number, @attrs[:attributes][:number]
        assert_equal response[:address].unit, @attrs[:attributes][:unit]
        assert_equal response[:address].city, @attrs[:attributes][:city]
        assert_equal response[:address].state, @attrs[:attributes][:state]
        assert_equal response[:address].country, @attrs[:attributes][:country]
        assert_equal response[:address].neighborhood, @attrs[:attributes][:neighborhood]
      end 
    end 
  end
end
