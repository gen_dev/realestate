require 'test_helper'

class Mutations::Addresses::DeleteAddressTest < ActiveSupport::TestCase
  def perform(current_user, args = {})
    Mutations::Addresses::DeleteAddress.new(object: nil, field: nil, context: { current_user: current_user }).resolve(args)
  end
  
  context 'when current user hasnt got permissions' do
    setup do 
      @current_user = create(:user)
      @address = create(:address)
    end
    
    should 'not delete any address' do
      response = perform(@current_user, { address_id: @address.id })
      assert_empty response
    end
  end
  
  context 'when current user has gendev permissions' do
    setup do 
      @current_user = create(:user_with_role_gendev)
      @address = create(:address)
    end 
    
    should 'delete any address' do
      response = perform(@current_user, { address_id: @address.id })
      assert response[:success]
    end
  end
  
  context 'when current user has admin permissions' do
    setup do 
      @current_user = create(:user_with_role_admin)
    end 
    
    context 'when address isnt related to user' do 
      setup do 
        @address = create(:address)
      end 
      
      should 'not delete it' do 
        response = perform(@current_user, { address_id: @address.id })
        assert_empty response
      end 
    end 
    
    context 'when address is related to user' do 
      setup do 
        @address = create(:address, addressable: @current_user)
      end 

      should 'delete it' do 
        response = perform(@current_user, { address_id: @address.id })
        assert response[:success]
      end 
    end 
  end
end