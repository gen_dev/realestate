require 'test_helper'

class Mutations::Addresses::CreateAddressTest < ActiveSupport::TestCase
  def perform(current_user, args = {})
    Mutations::Addresses::CreateAddress.new(object: nil, field: nil, context: { current_user: current_user }).resolve(args)
  end
  
  context 'when current user hasnt got permissions' do
    setup do 
      @current_user = create(:user)
      @addressable = create(:user, email: 'john@doe.com', password: '[omitted]')
      @attrs = {
        attributes: {
          street: 'Occasion Avenue',
          number: '666',
          unit: '3',
          city: 'New Jersey',
          state: 'New Jersey',
          country: 'USA',
          neighborhood: 'Hells Kitchen'
        },
        addressable_id: @addressable.id, 
        addressable_type: @addressable.class.name
      }
    end

    should 'not create address' do
      assert_empty perform(@current_user, @attrs)
    end
  end

  context 'when current user has gendev permissions' do
    setup do 
      @current_user = create(:user_with_role_gendev)
    end 

    context 'when addressable_type == User' do 
      setup do 
        @addressable = create(:user, email: 'john@doe.com', password: '[omitted]')
        @attrs = {
          attributes: {
            street: 'Occasion Avenue',
            number: '666',
            unit: '3',
            city: 'New Jersey',
            state: 'New Jersey',
            country: 'USA',
            neighborhood: 'Hells Kitchen'
          },
          addressable_id: @addressable.id, 
          addressable_type: @addressable.class.name
        }
      end

      should 'create address' do 
        response = perform(@current_user, @attrs)
        address = response[:address]
        assert address.persisted?
        assert_equal address.street, @attrs[:attributes][:street] 
        assert_equal address.number, @attrs[:attributes][:number]
        assert_equal address.city, @attrs[:attributes][:city]
        assert_equal address.state, @attrs[:attributes][:state]
        assert_equal address.unit, @attrs[:attributes][:unit]
        assert_equal address.addressable_id, @attrs[:addressable_id]
        assert_equal address.addressable_type, @attrs[:addressable_type]
      end
    end

    context 'when addressable_type == Agency' do 
      setup do 
        @addressable = create(:agency, name: 'Test Agency S.A')
        @attrs = {
          attributes: {
            street: 'Occasion Avenue',
            number: '666',
            unit: '3',
            city: 'New Jersey',
            state: 'New Jersey',
            country: 'USA',
            neighborhood: 'Hells Kitchen'
          },
          addressable_id: @addressable.id, 
          addressable_type: @addressable.class.name
        }
      end

      should 'create address' do 
        response = perform(@current_user, @attrs)
        address = response[:address]
        assert address.persisted?
        assert_equal address.street, @attrs[:attributes][:street] 
        assert_equal address.number, @attrs[:attributes][:number]
        assert_equal address.city, @attrs[:attributes][:city]
        assert_equal address.state, @attrs[:attributes][:state]
        assert_equal address.unit, @attrs[:attributes][:unit]
        assert_equal address.addressable_id, @attrs[:addressable_id]
        assert_equal address.addressable_type, @attrs[:addressable_type]
      end
    end

    context 'when addressable_type == Property' do 
      setup do 
        @addressable = create(:property, property_type: 'Apartment', state: 'empty')
        @attrs = {
          attributes: {
            street: 'Occasion Avenue',
            number: '666',
            unit: '3',
            city: 'New Jersey',
            state: 'New Jersey',
            country: 'USA',
            neighborhood: 'Hells Kitchen'
          },
          addressable_id: @addressable.id, 
          addressable_type: @addressable.class.name
        }
      end

      context 'when address doesnt exists' do 
        should 'create address' do 
          response = perform(@current_user, @attrs)
          address = response[:address]
          assert address.persisted?
          assert_equal address.street, @attrs[:attributes][:street] 
          assert_equal address.number, @attrs[:attributes][:number]
          assert_equal address.city, @attrs[:attributes][:city]
          assert_equal address.state, @attrs[:attributes][:state]
          assert_equal address.unit, @attrs[:attributes][:unit]
          assert_equal address.addressable_id, @attrs[:addressable_id]
          assert_equal address.addressable_type, @attrs[:addressable_type]
        end
      end

      context 'when address already exist' do 
        setup do 
          perform(@current_user, @attrs)
        end 

        should 'not create address' do 
          assert_empty perform(@current_user, @attrs)
        end
      end
    end
  end

  context 'when current user has admin permissions' do
    setup do 
      @current_user = create(:user_with_role_admin)
    end 

    context 'when addressable_type == User' do 
      context 'when address isnt related to user' do 
        setup do 
          @addressable = create(:user, email: 'john@doe.com', password: '[omitted]')
          @attrs = {
            attributes: {
              street: 'Occasion Avenue',
              number: '666',
              unit: '3',
              city: 'New Jersey',
              state: 'New Jersey',
              country: 'USA',
              neighborhood: 'Hells Kitchen'
            },
            addressable_id: @addressable.id, 
            addressable_type: @addressable.class.name
          }
        end
        
        should 'not create address' do 
          assert_empty perform(@current_user, @attrs)
        end
      end

      context 'when address is related to user' do 
        setup do 
          @addressable = @current_user
        end

        context 'when required fields are missing' do 
          setup do 
            @attrs = {
              attributes: {
                street: nil,
                number: '666',
                unit: '3',
                city: 'New Jersey',
                state: 'New Jersey',
                country: 'USA',
                neighborhood: 'Hells Kitchen'
              },
              addressable_id: @addressable.id, 
              addressable_type: @addressable.class.name
            }
          end

          should 'not create address' do 
            assert_empty perform(@current_user, @attrs)
          end
        end 

        context 'when all required fields are filled' do
          setup do 
            @attrs = {
              attributes: {
                street: 'Occasion Avenue',
                number: '666',
                unit: '3',
                city: 'New Jersey',
                state: 'New Jersey',
                country: 'USA',
                neighborhood: 'Hells Kitchen'
              },
              addressable_id: @addressable.id, 
              addressable_type: @addressable.class.name
            }
          end

          should 'create address' do 
            response = perform(@current_user, @attrs)
            address = response[:address]
            assert address.persisted?
            assert_equal address.street, @attrs[:attributes][:street] 
            assert_equal address.number, @attrs[:attributes][:number]
            assert_equal address.city, @attrs[:attributes][:city]
            assert_equal address.state, @attrs[:attributes][:state]
            assert_equal address.unit, @attrs[:attributes][:unit]
            assert_equal address.addressable_id, @attrs[:addressable_id]
            assert_equal address.addressable_type, @attrs[:addressable_type]
          end
        end
      end
    end

    context 'when addressable_type == Agency' do 
      context 'when address isnt related to user' do 
        setup do 
          @addressable = create(:agency, name: 'Test Agency S.A')
          @attrs = {
            attributes: {
              street: 'Occasion Avenue',
              number: '666',
              unit: '3',
              city: 'New Jersey',
              state: 'New Jersey',
              country: 'USA',
              neighborhood: 'Hells Kitchen'
            },
            addressable_id: @addressable.id, 
            addressable_type: @addressable.class.name
          }
        end

        should 'not create address' do 
          assert_empty perform(@current_user, @attrs)
        end
      end

      context 'when address is related to user' do 
        setup do 
          @addressable = create(:agency, name: 'Test Agency S.A')
          create(:user_agency, agency: @addressable, user: @current_user)
        end

        context 'when required fields are missing' do 
          setup do 
            @attrs = {
              attributes: {
                street: nil,
                number: '666',
                unit: '3',
                city: 'New Jersey',
                state: 'New Jersey',
                country: 'USA',
                neighborhood: 'Hells Kitchen'
              },
              addressable_id: @addressable.id, 
              addressable_type: @addressable.class.name
            }
          end

          should 'not create address' do 
            assert_empty perform(@current_user, @attrs)
          end
        end 

        context 'when all required fields are filled' do
          setup do 
            @attrs = {
              attributes: {
                street: 'Occasion Avenue',
                number: '666',
                unit: '3',
                city: 'New Jersey',
                state: 'New Jersey',
                country: 'USA',
                neighborhood: 'Hells Kitchen'
              },
              addressable_id: @addressable.id, 
              addressable_type: @addressable.class.name
            }
          end

          should 'create address' do 
            response = perform(@current_user, @attrs)
            address = response[:address]
            assert address.persisted?
            assert_equal address.street, @attrs[:attributes][:street] 
            assert_equal address.number, @attrs[:attributes][:number]
            assert_equal address.city, @attrs[:attributes][:city]
            assert_equal address.state, @attrs[:attributes][:state]
            assert_equal address.unit, @attrs[:attributes][:unit]
            assert_equal address.addressable_id, @attrs[:addressable_id]
            assert_equal address.addressable_type, @attrs[:addressable_type]
          end
        end
      end
    end

    context 'when addressable_type == Property' do 
      context 'when address isnt related to user' do 
        setup do 
          @addressable = create(:property, property_type: 'Apartment', state: 'empty')
          @attrs = {
            attributes: {
              street: 'Occasion Avenue',
              number: '666',
              unit: '3',
              city: 'New Jersey',
              state: 'New Jersey',
              country: 'USA',
              neighborhood: 'Hells Kitchen'
            },
            addressable_id: @addressable.id, 
            addressable_type: @addressable.class.name
          }
        end

        should 'not create address' do 
          assert_empty perform(@current_user, @attrs)
        end
      end
      
      context 'when address is related to user' do 
        setup do 
          @addressable = create(:property, property_type: 'Apartment', state: 'empty')
          create(:user_agency, agency: @addressable.agency, user: @current_user)
        end

        context 'when required fields are missing' do 
          setup do 
            @attrs = {
              attributes: {
                street: nil,
                number: '666',
                unit: '3',
                city: 'New Jersey',
                state: 'New Jersey',
                country: 'USA',
                neighborhood: 'Hells Kitchen'
              },
              addressable_id: @addressable.id, 
              addressable_type: @addressable.class.name
            }
          end

          should 'not create address' do 
            assert_empty perform(@current_user, @attrs)
          end
        end 

        context 'when all required fields are filled' do
          setup do 
            @attrs = {
              attributes: {
                street: 'Occasion Avenue',
                number: '666',
                unit: '3',
                city: 'New Jersey',
                state: 'New Jersey',
                country: 'USA',
                neighborhood: 'Hells Kitchen'
              },
              addressable_id: @addressable.id, 
              addressable_type: @addressable.class.name
            }
          end

          context 'when address doesnt exists' do 
            should 'create address' do 
              response = perform(@current_user, @attrs)
              address = response[:address]
              assert address.persisted?
              assert_equal address.street, @attrs[:attributes][:street] 
              assert_equal address.number, @attrs[:attributes][:number]
              assert_equal address.city, @attrs[:attributes][:city]
              assert_equal address.state, @attrs[:attributes][:state]
              assert_equal address.unit, @attrs[:attributes][:unit]
              assert_equal address.addressable_id, @attrs[:addressable_id]
              assert_equal address.addressable_type, @attrs[:addressable_type]
            end
          end

          context 'when address already exist' do 
            setup do 
              perform(@current_user, @attrs)
            end 

            should 'not create address' do 
              assert_empty perform(@current_user, @attrs)
            end
          end
        end
      end
    end
  end
end
