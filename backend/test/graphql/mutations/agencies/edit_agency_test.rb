require 'test_helper'

class Mutations::Agencies::EditAgencyTest < ActiveSupport::TestCase
  def perform(current_user, args = {})
    Mutations::Agencies::EditAgency.new(object: nil, field: nil, context: { current_user: current_user }).resolve(args)
  end
  
  context 'when current user hasnt got permissions' do
    setup do 
      @current_user = create(:user)
      @agency = create(:agency)
      @attrs = {
        attributes: {
          name: 'Some Agency'
        },
        agency_id: @agency.id, 
      }
    end

    should 'not edit agency' do
      assert_empty perform(@current_user, @attrs)
    end
  end

  context 'when current user has gendev permissions' do
    setup do 
      @current_user = create(:user_with_role_gendev)
      @agency = create(:agency)
      @attrs = {
        attributes: {
          name: 'Some Agency'
        },
        agency_id: @agency.id, 
      }
    end 
    
    should 'edit any agency' do
      response = perform(@current_user, @attrs)
      assert response[:agency]
      assert_equal response[:agency].name, @attrs[:attributes][:name]
    end
  end
  
  context 'when current user has admin permissions' do
    setup do 
      @current_user = create(:user_with_role_admin)
    end 
    
    context 'when agency isnt related to user' do 
      setup do 
        @agency = create(:agency)
        @attrs = {
          attributes: {
            name: 'Some Agency'
          },
          agency_id: @agency.id, 
        }
      end 
      
      should 'not update it' do 
        response = perform(@current_user, @attrs)
        assert_empty response
      end 
    end 
    
    context 'when agency is related to user' do 
      setup do 
        @agency = create(:agency)
        create(:user_agency, user: @current_user, agency: @agency)
        @attrs = {
          attributes: {
            name: 'Some Agency'
          },
          agency_id: @agency.id, 
        }
      end 

      should 'update it' do 
        response = perform(@current_user, @attrs)
        assert response[:agency]
        assert_equal response[:agency].name, @attrs[:attributes][:name]
      end 
    end 
  end
end
