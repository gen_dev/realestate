require 'test_helper'

class Mutations::Agencies::DeleteAgencyTest < ActiveSupport::TestCase
  def perform(current_user, args = {})
    Mutations::Agencies::DeleteAgency.new(object: nil, field: nil, context: { current_user: current_user }).resolve(args)
  end
  
  context 'when current user hasnt got permissions' do
    setup do 
      @current_user = create(:user)
      @agency = create(:agency)
    end
    
    should 'not delete any agency' do
      response = perform(@current_user, { agency_id: @agency.id })
      assert_empty response
    end
  end
  
  context 'when current user has gendev permissions' do
    setup do 
      @current_user = create(:user_with_role_gendev)
      @agency = create(:agency)
    end 
    
    should 'delete any agency' do
      response = perform(@current_user, { agency_id: @agency.id })
      assert response[:success]
    end
  end
  
  context 'when current user has admin permissions' do
    setup do 
      @current_user = create(:user_with_role_admin)
    end 
    
    context 'when agency isnt related to user' do 
      setup do 
        @agency = create(:agency)
      end 
      
      should 'not delete it' do 
        response = perform(@current_user, { agency_id: @agency.id })
        assert_empty response
      end 
    end 
    
    context 'when agency is related to user' do 
      setup do 
        @agency = create(:agency)
        create(:user_agency, user: @current_user, agency: @agency)
      end 

      should 'delete it' do 
        response = perform(@current_user, { agency_id: @agency.id })
        assert response[:success]
      end 
    end 
  end
end