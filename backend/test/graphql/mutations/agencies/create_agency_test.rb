require 'test_helper'

class Mutations::Agencies::CreateAgencyTest < ActiveSupport::TestCase
  def perform(current_user, args = {})
    Mutations::Agencies::CreateAgency.new(object: nil, field: nil, context: { current_user: current_user }).resolve(args)
  end

  context 'when current user hasnt got permissions' do
    setup do 
      @current_user = create(:user)
      @attrs = { name: 'Test Agency S.A' }
    end

    should 'not create agency' do
      assert_empty perform(@current_user, @attrs)
    end
  end

  context 'when current user has gendev permissions' do
    setup do 
      @current_user = create(:user_with_role_gendev)
    end 

    context 'when required fields are missing' do 
      setup do 
        @attrs = { name: nil }
      end

      should 'not create agency' do
        assert_empty perform(@current_user, @attrs)
      end
    end

    context 'when all required fields are filled' do 
      setup do 
        @attrs = { name: 'Test Agency S.A' }
      end

      context 'when Agency name doesnt exists' do 
        should 'create Agency' do 
          response = perform(@current_user, @attrs)
          agency = response[:agency]
          assert agency.persisted?
          assert_equal agency.name, @attrs[:name]
        end
      end
      
      context 'when Agency name is already in use' do 
        setup do 
          perform(@current_user, @attrs)
        end
        
        should 'not create Agency' do 
          assert_empty perform(@current_user, @attrs)
        end
      end
    end
  end

  context 'when current user has admin permissions' do
    setup do 
      @current_user = create(:user_with_role_admin)
      @attrs = { name: 'Test Agency S.A' }
    end

    should 'not create agency' do
      assert_empty perform(@current_user, @attrs)
    end
  end
end
