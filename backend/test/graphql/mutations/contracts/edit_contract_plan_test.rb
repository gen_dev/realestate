require 'test_helper'

class Mutations::Contracts::EditContractPlanTest < ActiveSupport::TestCase
  def perform(current_user, args = {})
    Mutations::Contracts::EditContractPlan.new(object: nil, field: nil, context: { current_user: current_user }).resolve(args)
  end
  
  context 'when current user hasnt got permissions' do
    setup do 
      @current_user = create(:user)
      @contract_plan = create(:contract_plan)
      @attrs = {
        attributes: {
          duration: 24, 
          rent_update_period: 6, 
          rent_value: { 
            'first_period' => 1000, 
            'second_period' => 2000 
          }
        },
        contract_plan_id: @contract_plan.id, 
      }
    end

    should 'not edit contract plan' do
      assert_empty perform(@current_user, @attrs)
    end
  end

  context 'when current user has gendev permissions' do
    setup do 
      @current_user = create(:user_with_role_gendev)
      @contract_plan = create(:contract_plan)
      @attrs = {
        attributes: {
          duration: 24, 
          rent_update_period: 6, 
          rent_value: { 
            'first_period' => 1000, 
            'second_period' => 2000 
          }
        },
        contract_plan_id: @contract_plan.id, 
      }
    end 
    
    should 'edit any contract plan' do
      response = perform(@current_user, @attrs)
      assert response[:contract_plan]
      assert_equal response[:contract_plan].duration, @attrs[:attributes][:duration]
      assert_equal response[:contract_plan].rent_update_period, @attrs[:attributes][:rent_update_period]
      assert_equal response[:contract_plan].rent_value, @attrs[:attributes][:rent_value]
    end
  end
  
  context 'when current user has admin permissions' do
    setup do 
      @current_user = create(:user_with_role_admin)
    end 
    
    context 'when contract plan isnt related to user' do 
      setup do 
        @contract_plan = create(:contract_plan)
        @attrs = {
          attributes: {
            duration: 24, 
            rent_update_period: 6, 
            rent_value: { 
              'first_period' => 1000, 
              'second_period' => 2000 
            }
          },
          contract_plan_id: @contract_plan.id, 
        }
      end 
      
      should 'not update it' do 
        response = perform(@current_user, @attrs)
        assert_empty response
      end 
    end 
    
    context 'when contract plan is related to user through contractPlan property agency' do 
      setup do 
        property = create(:property, agency: create(:user_agency, user: @current_user).agency)
        @contract_plan = create(:contract_plan, property: property)
        @attrs = {
          attributes: {
            duration: 24, 
            rent_update_period: 6, 
            rent_value: { 
              'first_period' => 1000, 
              'second_period' => 2000 
            }
          },
          contract_plan_id: @contract_plan.id, 
        }
      end 

      should 'update it' do 
        response = perform(@current_user, @attrs)
        assert response[:contract_plan]
        assert_equal response[:contract_plan].duration, @attrs[:attributes][:duration]
        assert_equal response[:contract_plan].rent_update_period, @attrs[:attributes][:rent_update_period]
        assert_equal response[:contract_plan].rent_value, @attrs[:attributes][:rent_value]
      end 
    end 
  end
end
