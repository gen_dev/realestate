require 'test_helper'

class Mutations::Contracts::DeleteContractExpenseTest < ActiveSupport::TestCase
  def perform(current_user, args = {})
    Mutations::Contracts::DeleteContractExpense.new(object: nil, field: nil, context: { current_user: current_user }).resolve(args)
  end
  
  context 'when current user hasnt got permissions' do
    setup do 
      @current_user = create(:user)
      @contract_expense = create(:contract_expense)
    end
    
    should 'not delete any contract expense' do
      response = perform(@current_user, { contract_expense_id: @contract_expense.id })
      assert_empty response
    end
  end
  
  context 'when current user has gendev permissions' do
    setup do 
      @current_user = create(:user_with_role_gendev)
      @contract_expense = create(:contract_expense)
    end 
    
    should 'delete any contract expense' do
      response = perform(@current_user, { contract_expense_id: @contract_expense.id })
      assert response[:success]
    end
  end
  
  context 'when current user has admin permissions' do
    setup do 
      @current_user = create(:user_with_role_admin)
    end 
    
    context 'when contract expense isnt related to user' do 
      setup do 
        @contract_expense = create(:contract_expense)
      end 
      
      should 'not delete it' do 
        response = perform(@current_user, { contract_expense_id: @contract_expense.id })
        assert_empty response
      end 
    end 
    
    context 'when contract expense is related to user through contractPlan property agency' do 
      setup do 
        contract_plan = create(:contract_plan, property: create(:property, agency: create(:user_agency, user: @current_user).agency))
        @contract_expense = create(:contract_expense, contract_plan: contract_plan)
      end 

      should 'delete it' do 
        response = perform(@current_user, { contract_expense_id: @contract_expense.id })
        assert response[:success]
      end 
    end 
  end
end