require 'test_helper'

class Mutations::Contracts::CreateContractPlanTest < ActiveSupport::TestCase
  def perform(current_user, args = {})
    Mutations::Contracts::CreateContractPlan.new(object: nil, field: nil, context: { current_user: current_user }).resolve(args)
  end

  context 'when current user hasnt got permissions' do
    setup do 
      @current_user = create(:user)
      @property = create(:property)
      @attrs = {
        attributes: {
          duration: 12, 
          rent_update_period: 6, 
          rent_value: { 
            'first_period' => 1000, 
            'second_period' => 2000 
          }
        },
        property_id: @property.id,
        contract_id: create(:contract, property: @property).id
      }
    end

    should 'not create contract plan' do
      assert_empty perform(@current_user, @attrs)
    end
  end

  context 'when current user has gendev permissions' do
    setup do 
      @current_user = create(:user_with_role_gendev)
    end 

    context 'when required fields are missing' do
      setup do 
        @property = create(:property)
        @attrs = {
          attributes: {
            duration: nil, 
            rent_update_period: 6, 
            rent_value: { 
              'first_period' => 1000, 
              'second_period' => 2000 
            }
          },
          property_id: @property.id,
          contract_id: create(:contract, property: @property).id
        }
      end
      
      should 'not create contract plan' do
        assert_empty perform(@current_user, @attrs)
      end
    end
    
    context 'when all required fields are filled' do
      setup do 
        @property = create(:property)
        @attrs = {
          attributes: {
            duration: 12, 
            rent_update_period: 6, 
            rent_value: { 
              'first_period' => 1000, 
              'second_period' => 2000 
            }
          },
          property_id: @property.id,
          contract_id: create(:contract, property: @property).id
        }
      end

      should 'create contract plan' do
        response = perform(@current_user, @attrs)
        contract_plan = response[:contract_plan]
      
        assert contract_plan.persisted?
        assert_equal contract_plan.duration, @attrs[:attributes][:duration]
        assert_equal contract_plan.rent_update_period, @attrs[:attributes][:rent_update_period]
        assert_equal contract_plan.rent_value, @attrs[:attributes][:rent_value]
        assert_equal contract_plan.property_id, @attrs[:property_id]
        assert_equal contract_plan.contract_id, @attrs[:contract_id]
      end
    end
  end

  context 'when current user has admin permissions' do
    setup do 
      @current_user = create(:user_with_role_admin)
    end 

    context 'when contract plan is related to user' do 
      setup do 
        @property = create(:property, agency: create(:user_agency, user: @current_user).agency)
      end 
    
      context 'when required fields are missing' do
        setup do 
          @attrs = {
            attributes: {
              duration: nil, 
              rent_update_period: 6, 
              rent_value: { 
                'first_period' => 1000, 
                'second_period' => 2000 
              }
            },
            property_id: @property.id,
            contract_id: create(:contract, property: @property).id
          }
        end

        should 'not create contract_plan' do
          assert_empty perform(@current_user, @attrs)
        end
      end

      context 'when all required fields are filled' do
        setup do 
          @attrs = {
            attributes: {
              duration: 12, 
              rent_update_period: 6, 
              rent_value: { 
                'first_period' => 1000, 
                'second_period' => 2000 
              }
            },
            property_id: @property.id,
            contract_id: create(:contract, property: @property).id
          }
        end

        should 'create contract plan' do
          response = perform(@current_user, @attrs)
          contract_plan = response[:contract_plan]
        
          assert contract_plan.persisted?
          assert_equal contract_plan.duration, @attrs[:attributes][:duration]
          assert_equal contract_plan.rent_update_period, @attrs[:attributes][:rent_update_period]
          assert_equal contract_plan.rent_value, @attrs[:attributes][:rent_value]
          assert_equal contract_plan.property_id, @attrs[:property_id]
          assert_equal contract_plan.contract_id, @attrs[:contract_id]
        end
      end
    end

    context 'when contract plan isnt related to user' do 
      setup do 
        @property = create(:property)
        @attrs = {
          attributes: {
            duration: nil, 
            rent_update_period: 6, 
            rent_value: { 
              'first_period' => 1000, 
              'second_period' => 2000 
            }
          },
          property_id: @property.id,
          contract_id: create(:contract, property: @property).id
        }
      end

      should 'not create contract plan' do
        assert_empty perform(@current_user, @attrs)
      end
    end 
  end
end
