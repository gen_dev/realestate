require 'test_helper'

class Mutations::Contracts::DeleteContractPlanTest < ActiveSupport::TestCase
  def perform(current_user, args = {})
    Mutations::Contracts::DeleteContractPlan.new(object: nil, field: nil, context: { current_user: current_user }).resolve(args)
  end
  
  context 'when current user hasnt got permissions' do
    setup do 
      @current_user = create(:user)
      @contract_plan = create(:contract_plan)
    end
    
    should 'not delete any contract plan' do
      response = perform(@current_user, { contract_plan_id: @contract_plan.id })
      assert_empty response
    end
  end
  
  context 'when current user has gendev permissions' do
    setup do 
      @current_user = create(:user_with_role_gendev)
      @contract_plan = create(:contract_plan)
    end 
    
    should 'delete any contract plan' do
      response = perform(@current_user, { contract_plan_id: @contract_plan.id })
      assert response[:success]
    end
  end
  
  context 'when current user has admin permissions' do
    setup do 
      @current_user = create(:user_with_role_admin)
    end 
    
    context 'when contract plan isnt related to user' do 
      setup do 
        @contract_plan = create(:contract_plan)
      end 
      
      should 'not delete it' do 
        response = perform(@current_user, { contract_plan_id: @contract_plan.id })
        assert_empty response
      end 
    end 
    
    context 'when contract plan is related to user through contractPlan property agency' do 
      setup do 
        property = create(:property, agency: create(:user_agency, user: @current_user).agency)
        @contract_plan = create(:contract_plan, property: property)
      end 

      should 'delete it' do 
        response = perform(@current_user, { contract_plan_id: @contract_plan.id })
        assert response[:success]
      end 
    end 
  end
end