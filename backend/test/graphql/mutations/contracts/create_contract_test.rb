require 'test_helper'

class Mutations::Contracts::CreateContractTest < ActiveSupport::TestCase
  def perform(current_user, args = {})
    Mutations::Contracts::CreateContract.new(object: nil, field: nil, context: { current_user: current_user }).resolve(args)
  end

  context 'when current user hasnt got permissions' do
    setup do 
      @current_user = create(:user)
      @attrs = { 
        attributes: { 
          state: 'Ongoing', 
          contract_type: 'Renting'
        }, 
        property_id: create(:property).id 
      }
    end

    should 'not create contract' do
      assert_empty perform(@current_user, @attrs)
    end
  end

  context 'when current user has gendev permissions' do
    setup do 
      @current_user = create(:user_with_role_gendev)
    end 

    context 'when required fields are missing' do
      setup do 
        @attrs = { 
          attributes: { 
            state: nil,
            contract_type: 'Renting'
          }, 
          property_id: create(:property).id 
        }
      end

      should 'not create contract' do
        assert_empty perform(@current_user, @attrs)
      end
    end

    context 'when all required fields are filled' do
      context 'when property doesnt have owner' do 
        setup do 
          @attrs = { 
            attributes: { 
              state: 'Ongoing', 
              contract_type: 'Renting'
            }, 
            property_id: create(:property).id 
          }
        end

        should 'not create contract' do
          assert_empty perform(@current_user, @attrs)
        end
      end

      context 'when property have owner' do 
        setup do 
          @attrs = { 
            attributes: { 
              state: 'Ongoing', 
              contract_type: 'Renting'
            }, 
            property_id: create(:property_with_owner).id 
          }
        end

        should 'create contract' do
          response = perform(@current_user, @attrs)
          contract = response[:contract]
        
          assert contract.persisted?
          assert_equal contract.state, @attrs[:attributes][:state]
          assert_equal contract.contract_type, @attrs[:attributes][:contract_type]
          assert_equal contract.property_id, @attrs[:property_id]
        end
      end
    end
  end

  context 'when current user has admin permissions' do
    setup do 
      @current_user = create(:user_with_role_admin)
    end
    
    context 'when contract is related to user' do 
      setup do 
        @property = create(:property, agency: create(:user_agency, user: @current_user).agency)
        @property_with_owner = create(:property_with_owner, agency: create(:user_agency, user: @current_user).agency)
      end 

      context 'when required fields are missing' do
        setup do 
          @attrs = { 
            attributes: { 
              state: nil,
              contract_type: 'Renting'
            }, 
            property_id: @property.id 
          }
        end
  
        should 'not create contract' do
          assert_empty perform(@current_user, @attrs)
        end
      end
  
      context 'when all required fields are filled' do
        context 'when property doesnt have owner' do 
          setup do 
            @attrs = { 
              attributes: { 
                state: 'Ongoing', 
                contract_type: 'Renting'
              }, 
              property_id: @property.id 
            }
          end

          should 'not create contract' do
            assert_empty perform(@current_user, @attrs)
          end
        end
  
        context 'when property have owner' do 
          setup do 
            @attrs = { 
              attributes: { 
                state: 'Ongoing', 
                contract_type: 'Renting'
              }, 
              property_id: @property_with_owner.id 
            }
          end

          should 'create contract' do
            response = perform(@current_user, @attrs)
            contract = response[:contract]
          
            assert contract.persisted?
            assert_equal contract.state, @attrs[:attributes][:state]
            assert_equal contract.contract_type, @attrs[:attributes][:contract_type]
            assert_equal contract.property_id, @attrs[:property_id]
          end
        end
      end
    end

    context 'when contract isnt related to user' do 
      setup do 
        @attrs = { 
          attributes: { 
            state: nil,
            contract_type: 'Renting'
          }, 
          property_id: create(:property_with_owner).id 
        }
      end 

      should 'not create contract' do
        assert_empty perform(@current_user, @attrs)
      end
    end
  end
end
