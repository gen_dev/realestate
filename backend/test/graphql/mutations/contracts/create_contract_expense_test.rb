require 'test_helper'

class Mutations::Contracts::CreateContractExpenseTest < ActiveSupport::TestCase
  def perform(current_user, args = {})
    Mutations::Contracts::CreateContractExpense.new(object: nil, field: nil, context: { current_user: current_user }).resolve(args)
  end

  context 'when current user hasnt got permissions' do
    setup do 
      @current_user = create(:user)
      @attrs = { 
        attributes: { 
          name: 'Sealed', 
          value: 3000
        }, 
        contract_plan_id: create(:contract_plan).id 
      }
    end

    should 'not create contract expense' do
      assert_empty perform(@current_user, @attrs)
    end
  end

  context 'when current user has gendev permissions' do
    setup do 
      @current_user = create(:user_with_role_gendev)
    end 

    context 'when required fields are missing' do
      setup do 
        @attrs = { 
          attributes: {
            name: nil, 
            value: 2000 
          },
          contract_plan_id: create(:contract_plan).id 
        }
      end

      should 'not create contract expense' do
        assert_empty perform(@current_user, @attrs)
      end
    end

    context 'when all required fields are filled' do
      setup do 
        @attrs = { 
          attributes: { 
            name: 'Sealed', 
            value: 3000
          }, 
          contract_plan_id: create(:contract_plan).id 
        }
      end

      should 'create contract expense' do
        response = perform(@current_user, @attrs)
        contract_expense = response[:contract_expense]
      
        assert contract_expense.persisted?
        assert_equal contract_expense.name, @attrs[:attributes][:name]
        assert_equal contract_expense.value, @attrs[:attributes][:value]
        assert_equal contract_expense.contract_plan_id, @attrs[:contract_plan_id]
      end
    end
  end

  context 'when current user has admin permissions' do
    setup do 
      @current_user = create(:user_with_role_admin)
    end 

    context 'when contract expense is related to user through contract property agency' do 
      setup do
        @contract_plan = create(:contract_plan, property: create(:property, agency: create(:user_agency, user: @current_user).agency))
      end 

      context 'when required fields are missing' do
        setup do 
          @attrs = { 
            attributes: {
              name: nil, 
              value: 2000 
            },
            contract_plan_id: @contract_plan.id 
          }
        end
  
        should 'not create contract expense' do
          assert_empty perform(@current_user, @attrs)
        end
      end
  
      context 'when all required fields are filled' do
        setup do 
          @attrs = { 
            attributes: { 
              name: 'Sealed', 
              value: 3000
            }, 
            contract_plan_id: @contract_plan.id 
          }
        end
  
        should 'create contract expense' do
          response = perform(@current_user, @attrs)
          contract_expense = response[:contract_expense]
        
          assert contract_expense.persisted?
          assert_equal contract_expense.name, @attrs[:attributes][:name]
          assert_equal contract_expense.value, @attrs[:attributes][:value]
          assert_equal contract_expense.contract_plan_id, @attrs[:contract_plan_id]
        end
      end
    end 

    context 'when contract expense isnt related to user' do 
      setup do 
        @attrs = { 
          attributes: { 
            name: 'Sealed', 
            value: 3000
          }, 
          contract_plan_id: create(:contract_plan).id 
        }
      end

      should 'not create contract expense' do
        assert_empty perform(@current_user, @attrs)
      end
    end 
  end
end
