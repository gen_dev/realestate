require 'test_helper'

class Mutations::Contracts::EditContractTest < ActiveSupport::TestCase
  def perform(current_user, args = {})
    Mutations::Contracts::EditContract.new(object: nil, field: nil, context: { current_user: current_user }).resolve(args)
  end
  
  context 'when current user hasnt got permissions' do
    setup do 
      @current_user = create(:user)
      @contract = create(:contract)
      @attrs = {
        attributes: {
          state: 'Finished',
          contract_type: 'Renting'
        },
        contract_id: @contract.id, 
      }
    end

    should 'not edit contract' do
      assert_empty perform(@current_user, @attrs)
    end
  end

  context 'when current user has gendev permissions' do
    setup do 
      @current_user = create(:user_with_role_gendev)
      @contract = create(:contract)
      @attrs = {
        attributes: {
          state: 'Finished',
          contract_type: 'Renting'
        },
        contract_id: @contract.id, 
      }
    end 
    
    should 'edit any contract' do
      response = perform(@current_user, @attrs)
      assert response[:contract]
      assert_equal response[:contract].state, @attrs[:attributes][:state]
      assert_equal response[:contract].contract_type, @attrs[:attributes][:contract_type]
    end
  end
  
  context 'when current user has admin permissions' do
    setup do 
      @current_user = create(:user_with_role_admin)
    end 
    
    context 'when contract isnt related to user' do 
      setup do 
        @contract = create(:contract)
        @attrs = {
          attributes: {
            state: 'Finished',
            contract_type: 'Renting'
          },
          contract_id: @contract.id, 
        }
      end 
      
      should 'not update it' do 
        response = perform(@current_user, @attrs)
        assert_empty response
      end 
    end 
    
    context 'when contract is related to user' do 
      setup do 
        property = create(:property, agency: create(:user_agency, user: @current_user).agency)
        @contract = create(:contract, property: property)
        @attrs = {
          attributes: {
            state: 'Finished',
            contract_type: 'Renting'
          },
          contract_id: @contract.id, 
        }
      end 

      should 'update it' do 
        response = perform(@current_user, @attrs)
        assert response[:contract]
        assert_equal response[:contract].state, @attrs[:attributes][:state]
        assert_equal response[:contract].contract_type, @attrs[:attributes][:contract_type]
      end 
    end 
  end
end
