require 'test_helper'

class Mutations::Contracts::EditContractExpenseTest < ActiveSupport::TestCase
  def perform(current_user, args = {})
    Mutations::Contracts::EditContractExpense.new(object: nil, field: nil, context: { current_user: current_user }).resolve(args)
  end
  
  context 'when current user hasnt got permissions' do
    setup do 
      @current_user = create(:user)
      @contract_expense = create(:contract_expense)
      @attrs = {
        attributes: {
          name: 'Sealed',
          value: 200.30
        },
        contract_expense_id: @contract_expense.id, 
      }
    end

    should 'not edit contract expense' do
      assert_empty perform(@current_user, @attrs)
    end
  end

  context 'when current user has gendev permissions' do
    setup do 
      @current_user = create(:user_with_role_gendev)
      @contract_expense = create(:contract_expense)
      @attrs = {
        attributes: {
          name: 'Sealed',
          value: 200
        },
        contract_expense_id: @contract_expense.id, 
      }
    end 
    
    should 'edit any contract expense' do
      response = perform(@current_user, @attrs)
      assert response[:contract_expense]
      assert_equal response[:contract_expense].name, @attrs[:attributes][:name]
      assert_equal response[:contract_expense].value, @attrs[:attributes][:value]
    end
  end
  
  context 'when current user has admin permissions' do
    setup do 
      @current_user = create(:user_with_role_admin)
    end 
    
    context 'when contract expense isnt related to user' do 
      setup do 
        @contract_expense = create(:contract_expense)
        @attrs = {
          attributes: {
            name: 'Sealed',
            value: 200
          },
          contract_expense_id: @contract_expense.id, 
        }
      end 
      
      should 'not update it' do 
        response = perform(@current_user, @attrs)
        assert_empty response
      end 
    end 
    
    context 'when contract expense is related to user through contractPlan property agency' do 
      setup do 
        contract_plan = create(:contract_plan, property: create(:property, agency: create(:user_agency, user: @current_user).agency))
        @contract_expense = create(:contract_expense, contract_plan: contract_plan)
        @attrs = {
          attributes: {
            name: 'Sealed',
            value: 200
          },
          contract_expense_id: @contract_expense.id, 
        }
      end 

      should 'update it' do 
        response = perform(@current_user, @attrs)
        assert response[:contract_expense]
        assert_equal response[:contract_expense].name, @attrs[:attributes][:name]
        assert_equal response[:contract_expense].value, @attrs[:attributes][:value]
      end 
    end 
  end
end
