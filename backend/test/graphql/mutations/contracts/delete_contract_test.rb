require 'test_helper'

class Mutations::Contracts::DeleteContractTest < ActiveSupport::TestCase
  def perform(current_user, args = {})
    Mutations::Contracts::DeleteContract.new(object: nil, field: nil, context: { current_user: current_user }).resolve(args)
  end
  
  context 'when current user hasnt got permissions' do
    setup do 
      @current_user = create(:user)
      @contract = create(:contract)
    end
    
    should 'not delete any contract' do
      response = perform(@current_user, { contract_id: @contract.id })
      assert_empty response
    end
  end
  
  context 'when current user has gendev permissions' do
    setup do 
      @current_user = create(:user_with_role_gendev)
      @contract = create(:contract)
    end 
    
    should 'delete any contract' do
      response = perform(@current_user, { contract_id: @contract.id })
      assert response[:success]
    end
  end
  
  context 'when current user has admin permissions' do
    setup do 
      @current_user = create(:user_with_role_admin)
    end 
    
    context 'when contract isnt related to user' do 
      setup do 
        @contract = create(:contract)
      end 
      
      should 'not delete it' do 
        response = perform(@current_user, { contract_id: @contract.id })
        assert_empty response
      end 
    end 
    
    context 'when contract is related to user through property agency' do 
      setup do 
        property = create(:property, agency: create(:user_agency, user: @current_user).agency)
        @contract = create(:contract, property: property)
      end 

      should 'delete it' do 
        response = perform(@current_user, { contract_id: @contract.id })
        assert response[:success]
      end 
    end 
  end
end