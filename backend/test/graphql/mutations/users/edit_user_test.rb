require 'test_helper'

class Mutations::Users::EditUserTest < ActiveSupport::TestCase
  def perform(current_user, args = {})
    Mutations::Users::EditUser.new(object: nil, field: nil, context: { current_user: current_user }).resolve(args)
  end
  
  context 'when current user hasnt got permissions' do
    setup do 
      @current_user = create(:user)
      @user = create(:user)
      @attrs = {
        attributes: {
          email: 'paulst@kiss.com'
        },
        user_id: @user.id 
      }
    end

    should 'not edit user' do
      assert_empty perform(@current_user, @attrs)
    end
  end

  context 'when current user has gendev permissions' do
    setup do 
      @current_user = create(:user_with_role_gendev)
      @user = create(:user)
      @attrs = {
        attributes: {
          email: 'paulst@kiss.com'
        },
        user_id: @user.id 
      }
    end 
    
    should 'edit any user' do
      response = perform(@current_user, @attrs)
      assert response[:user]
      assert_equal response[:user].email, @attrs[:attributes][:email]
    end
  end
  
  context 'when current user has admin permissions' do
    setup do 
      @current_user = create(:user_with_role_admin)
    end 
    
    context 'when user isnt related to user' do 
      setup do 
        @user = create(:user)
        @attrs = {
          attributes: {
            email: 'paulst@kiss.com'
          },
          user_id: @user.id 
        }
      end 
      
      should 'not update it' do 
        response = perform(@current_user, @attrs)
        assert_empty response
      end 
    end 

    context 'when user is related to user through agency' do 
      setup do 
        @user = create(:user)
        create(:user_agency, user: @user, agency: create(:user_agency, user: @current_user).agency)
        @attrs = {
          attributes: {
            email: 'paulst@kiss.com'
          },
          user_id: @user.id 
        }
      end 

      should 'update it' do 
        response = perform(@current_user, @attrs)
        assert response[:user]
        assert_equal response[:user].email, @attrs[:attributes][:email]
      end 
    end 
  end
end
