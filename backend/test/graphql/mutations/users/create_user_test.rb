require 'test_helper'

class Mutations::Users::CreateUserTest < ActiveSupport::TestCase
  def perform(args = {})
    Mutations::Users::CreateUser.new(object: nil, field: nil, context: { }).resolve(args)
  end

  context 'when credentials are nil' do 
    should 'not create user' do 
      assert_empty perform
    end
  end 

  context 'when credentials are filled' do 
    setup do 
      @credentials = {
        email: 'email@example.com',
        password: '[omitted]'
      }
    end

    context 'when user email doesnt exist' do 
      setup do 
        @attrs = {
          credentials: @credentials
        }
      end

      should 'create user' do 
        response = perform(@attrs)
        user = response[:user]
      
        assert user.persisted?
        assert_equal user.email, @attrs[:credentials][:email]
      end
    end

    context 'when user email already exists' do 
      setup do 
        @attrs = {
          credentials: @credentials
        }
        perform(@attrs)
      end

      should 'not create user' do 
        assert_empty perform(@attrs)
      end
    end 
  end
end
