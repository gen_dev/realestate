require 'test_helper'

class Mutations::Users::DeleteUserTest < ActiveSupport::TestCase
  def perform(current_user, args = {})
    Mutations::Users::DeleteUser.new(object: nil, field: nil, context: { current_user: current_user }).resolve(args)
  end
  
  context 'when current user hasnt got permissions' do
    setup do 
      @current_user = create(:user)
      @user = create(:user)
    end
    
    should 'not delete any user' do
      response = perform(@current_user, { user_id: @user.id })
      assert_empty response
    end
  end
  
  context 'when current user has gendev permissions' do
    setup do 
      @current_user = create(:user_with_role_gendev)
      @user = create(:user)
    end 
    
    should 'delete any user' do
      response = perform(@current_user, { user_id: @user.id })
      assert response[:success]
    end
  end
  
  context 'when current user has admin permissions' do
    setup do 
      @current_user = create(:user_with_role_admin)
    end 
    
    context 'when user isnt related to user' do 
      setup do 
        @user = create(:user)
      end 
      
      should 'not delete it' do 
        response = perform(@current_user, { user_id: @user.id })
        assert_empty response
      end 
    end 
    
    context 'when user is related to user through agency' do 
      setup do 
        @user = create(:user)
        create(:user_agency, user: @user, agency: create(:user_agency, user: @current_user).agency)
      end 

      should 'delete it' do 
        response = perform(@current_user, { user_id: @user.id })
        assert response[:success]
      end 
    end 
  end
end