require 'test_helper'

class Mutations::Users::SignOutTest < ActiveSupport::TestCase
  def sign_in(args = {})
    Mutations::Users::SignInUser.new(object: nil, field: nil, context: { session: {} }).resolve(args)
  end

  def perform()
    Mutations::Users::SignOut.new(object: nil, field: nil, context: { session: { token: @session } }).resolve()
  end

  context 'when user is logged' do 
    setup do 
      user = create(:user)
      @credentials = { email: user.email, password: user.password }
    end 

    should 'logout' do 
      credentials = sign_in(credentials: @credentials)
      @session = credentials[:token]
      result = perform()
      assert result[:success]
    end
  end 
end

