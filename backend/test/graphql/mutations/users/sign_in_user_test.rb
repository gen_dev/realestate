require 'test_helper'

class Mutations::Users::SignInUserTest < ActiveSupport::TestCase
  def perform(args = {})
    Mutations::Users::SignInUser.new(object: nil, field: nil, context: { session: {} }).resolve(args)
  end

  context 'when credentials doesnt exist' do 
    should 'not sign in user' do 
      assert_nil perform
    end
  end 

  context 'when credentials exists' do 
    setup do 
      @user = create(:user)
    end 

    context 'when email format isnt valid' do 
      setup do 
        @credentials = { email: 'wrong' }
      end 
      
      should 'not sign in user' do 
        assert_empty perform(credentials: @credentials)
      end
    end 

    context 'when password is wrong' do 
      setup do
        @credentials = { email: @user.email, password: 'wrong' }
      end 

      should 'not sign in user' do
        assert_empty perform(credentials: @credentials)
      end
    end 

    context 'when credentials are ok' do 
      setup do
        @credentials = { email: @user.email, password: @user.password }
      end 

      should 'sign in user' do 
        result = perform(credentials: @credentials) 
        assert result[:token].present?
        assert_equal result[:user], @user
      end
    end 
  end
end

