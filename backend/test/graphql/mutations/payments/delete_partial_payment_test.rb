require 'test_helper'

class Mutations::Payments::DeletePartialPaymentTest < ActiveSupport::TestCase
  def perform(current_user, args = {})
    Mutations::Payments::DeletePartialPayment.new(object: nil, field: nil, context: { current_user: current_user }).resolve(args)
  end
  
  context 'when current user hasnt got permissions' do
    setup do 
      @current_user = create(:user)
      @partial_payment = create(:partial_payment)
    end
    
    should 'not delete any partial payment' do
      response = perform(@current_user, { partial_payment_id: @partial_payment.id })
      assert_empty response
    end
  end
  
  context 'when current user has gendev permissions' do
    setup do 
      @current_user = create(:user_with_role_gendev)
      @partial_payment = create(:partial_payment)
    end 
    
    should 'delete any partial payment' do
      response = perform(@current_user, { partial_payment_id: @partial_payment.id })
      assert response[:success]
    end
  end
  
  context 'when current user has admin permissions' do
    setup do 
      @current_user = create(:user_with_role_admin)
    end 
    
    context 'when partial payment isnt related to user' do 
      setup do 
        @partial_payment = create(:partial_payment)
      end 
      
      should 'not delete it' do 
        response = perform(@current_user, { partial_payment_id: @partial_payment.id })
        assert_empty response
      end 
    end 
    
    context 'when partial payment is related to user through payment sub_account agency' do 
      setup do 
        @partial_payment = create(:partial_payment, payment: create(:payment, sub_account: create(:sub_account, agency: create(:user_agency, user: @current_user).agency)))
      end 

      should 'delete it' do 
        response = perform(@current_user, { partial_payment_id: @partial_payment.id })
        assert response[:success]
      end 
    end 
  end
end