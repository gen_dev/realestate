require 'test_helper'

class Mutations::Payments::EditPartialPaymentTest < ActiveSupport::TestCase
  def perform(current_user, args = {})
    Mutations::Payments::EditPartialPayment.new(object: nil, field: nil, context: { current_user: current_user }).resolve(args)
  end
  
  context 'when current user hasnt got permissions' do
    setup do 
      @current_user = create(:user)
      @partial_payment = create(:partial_payment)
      @attrs = {
        attributes: {
          value: 2000
        },
        partial_payment_id: @partial_payment.id 
      }
    end

    should 'not edit partial payment' do
      assert_empty perform(@current_user, @attrs)
    end
  end

  context 'when current user has gendev permissions' do
    setup do 
      @current_user = create(:user_with_role_gendev)
      @partial_payment = create(:partial_payment)
      @attrs = {
        attributes: {
          value: 2000
        },
        partial_payment_id: @partial_payment.id 
      }
    end 
    
    should 'edit any partial payment' do
      response = perform(@current_user, @attrs)
      assert response[:partial_payment]
      assert_equal response[:partial_payment].value, @attrs[:attributes][:value]
    end
  end
  
  context 'when current user has admin permissions' do
    setup do 
      @current_user = create(:user_with_role_admin)
    end 
    
    context 'when partial payment isnt related to user' do 
      setup do 
        @partial_payment = create(:partial_payment)
        @attrs = {
          attributes: {
            value: 2000
          },
          partial_payment_id: @partial_payment.id 
        }
      end 
      
      should 'not update it' do 
        response = perform(@current_user, @attrs)
        assert_empty response
      end 
    end 

    context 'when partial payment is related to user through payment sub_account agency' do 
      setup do 
        @partial_payment = create(:partial_payment, payment: create(:payment, sub_account: create(:sub_account, agency: create(:user_agency, user: @current_user).agency)))
        @attrs = {
          attributes: {
            value: 2000
          },
          partial_payment_id: @partial_payment.id 
        }
      end 

      should 'update it' do 
        response = perform(@current_user, @attrs)
        assert response[:partial_payment]
        assert_equal response[:partial_payment].value, @attrs[:attributes][:value]
      end 
    end 
  end
end
