require 'test_helper'

class Mutations::Payments::DeletePaymentTest < ActiveSupport::TestCase
  def perform(current_user, args = {})
    Mutations::Payments::DeletePayment.new(object: nil, field: nil, context: { current_user: current_user }).resolve(args)
  end
  
  context 'when current user hasnt got permissions' do
    setup do 
      @current_user = create(:user)
      @payment = create(:payment)
    end
    
    should 'not delete any payment' do
      response = perform(@current_user, { payment_id: @payment.id })
      assert_empty response
    end
  end
  
  context 'when current user has gendev permissions' do
    setup do 
      @current_user = create(:user_with_role_gendev)
      @payment = create(:payment)
    end 
    
    should 'delete any payment' do
      response = perform(@current_user, { payment_id: @payment.id })
      assert response[:success]
    end
  end
  
  context 'when current user has admin permissions' do
    setup do 
      @current_user = create(:user_with_role_admin)
    end 
    
    context 'when payment isnt related to user' do 
      setup do 
        @payment = create(:payment)
      end 
      
      should 'not delete it' do 
        response = perform(@current_user, { payment_id: @payment.id })
        assert_empty response
      end 
    end 
    
    context 'when payment is related to user through contractPlan property agency' do 
      setup do 
        @payment = create(:payment, sub_account: create(:sub_account, agency: create(:user_agency, user: @current_user).agency))
      end 

      should 'delete it' do 
        response = perform(@current_user, { payment_id: @payment.id })
        assert response[:success]
      end 
    end 
  end
end