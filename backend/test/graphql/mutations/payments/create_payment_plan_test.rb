require 'test_helper'

class Mutations::Payments::CreatePaymentPlanTest < ActiveSupport::TestCase
  def perform(current_user, args = {})
    Mutations::Payments::CreatePaymentPlan.new(object: nil, field: nil, context: { current_user: current_user }).resolve(args)
  end

  context 'when current user hasnt got permissions' do
    setup do 
      @current_user = create(:user)
      @contract = create(:contract)
      @attrs = {
        attributes: {
          payment_methods: ['Prueba']
        },
        contract_id: @contract.id
      }
    end

    should 'not create payment plan' do
      assert_empty perform(@current_user, @attrs)
    end
  end

  context 'when current user has gendev permissions' do
    setup do 
      @current_user = create(:user_with_role_gendev)
    end 

    context 'when required fields are missing' do 
      setup do 
        @attrs = {
          attributes: {
            payment_methods: ['Prueba']
          },
          contract_id: nil
        }
      end 

      should 'not create payment plan' do
        assert_empty perform(@current_user, @attrs)
      end
    end

    context 'when all required fields are filled' do 
      setup do 
        @attrs = {
          attributes: {
            payment_methods: ['Prueba']
          },
          contract_id: create(:contract).id
        }
      end 

      should 'create payment plan' do 
        response = perform(@current_user, @attrs)
        payment_plan = response[:payment_plan]
        assert payment_plan.persisted?
        assert_equal payment_plan.contract_id, @attrs[:contract_id]
      end
    end
  end

  context 'when current user has admin permissions' do
    setup do 
      @current_user = create(:user_with_role_admin)
    end 

    context 'when payment plan is related to user' do 
      setup do 
        @property = create(:property, agency: create(:user_agency, user: @current_user).agency) 
      end 

      context 'when required fields are missing' do 
        setup do 
          @attrs = {
            attributes: {
              payment_methods: ['Prueba']
            },
            contract_id: nil
          }
        end 

        should 'not create payment plan' do
          assert_empty perform(@current_user, @attrs)
        end
      end

      context 'when all required fields are filled' do 
        setup do 
          @attrs = {
            attributes: {
              payment_methods: ['Prueba']
            },
            contract_id: create(:contract, property: @property).id
          }
        end 

        should 'create payment plan' do 
          response = perform(@current_user, @attrs)
          payment_plan = response[:payment_plan]
          assert payment_plan.persisted?
          assert_equal payment_plan.contract_id, @attrs[:contract_id]
        end
      end
    end

    context 'when payment plan isnt related to user' do 
      setup do 
        @attrs = {
          attributes: {
            payment_methods: ['Prueba']
          },
          contract_id: create(:contract)
        }
      end 
      
      should 'not create payment plan' do
        assert_empty perform(@current_user, @attrs)
      end
    end 
  end
end
