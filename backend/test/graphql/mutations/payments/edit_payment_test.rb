require 'test_helper'

class Mutations::Payments::EditPaymentTest < ActiveSupport::TestCase
  def perform(current_user, args = {})
    Mutations::Payments::EditPayment.new(object: nil, field: nil, context: { current_user: current_user }).resolve(args)
  end
  
  context 'when current user hasnt got permissions' do
    setup do 
      @current_user = create(:user)
      @payment = create(:payment)
      @attrs = {
        attributes: {
          payment_type: "Check", 
          period: "1/12", 
          concept: "Water", 
          state: "pending", 
          amount: 2300.30, 
          due_date: "10/11/2020", 
          observations: "Lorem ipsum"
        },
        payment_id: @payment.id 
      }
    end

    should 'not edit payment' do
      assert_empty perform(@current_user, @attrs)
    end
  end

  context 'when current user has gendev permissions' do
    setup do 
      @current_user = create(:user_with_role_gendev)
      @payment = create(:payment)
      @attrs = {
        attributes: {
          payment_type: "Check", 
          period: "1/12", 
          concept: "Water", 
          state: "pending", 
          amount: 2300.30, 
          due_date: "10/11/2020", 
          observations: "Lorem ipsum"
        },
        payment_id: @payment.id 
      }
    end 
    
    should 'edit any payment' do
      response = perform(@current_user, @attrs)
      assert response[:payment]
      assert_equal response[:payment].payment_type, @attrs[:attributes][:payment_type]
      assert_equal response[:payment].period, @attrs[:attributes][:period]
      assert_equal response[:payment].concept, @attrs[:attributes][:concept]
      assert_equal response[:payment].amount, @attrs[:attributes][:amount]
      assert_equal response[:payment].due_date, @attrs[:attributes][:due_date].to_date
      assert_equal response[:payment].observations, @attrs[:attributes][:observations]
    end
  end
  
  context 'when current user has admin permissions' do
    setup do 
      @current_user = create(:user_with_role_admin)
    end 
    
    context 'when payment isnt related to user' do 
      setup do 
        @payment = create(:payment)
        @attrs = {
          attributes: {
            payment_type: "Check", 
            period: "1/12", 
            concept: "Water", 
            state: "pending", 
            amount: 2300.30, 
            due_date: "10/11/2020", 
            observations: "Lorem ipsum"
          },
          payment_id: @payment.id 
        }
      end 
      
      should 'not update it' do 
        response = perform(@current_user, @attrs)
        assert_empty response
      end 
    end 
    
    context 'when payment is related to user through contractPlan property agency' do 
      setup do 
        @payment = create(:payment, sub_account: create(:sub_account, agency: create(:user_agency, user: @current_user).agency))
        @attrs = {
          attributes: {
            payment_type: "Check", 
            period: "1/12", 
            concept: "Water", 
            state: "pending", 
            amount: 2300.30, 
            due_date: "10/11/2020", 
            observations: "Lorem ipsum"
          },
          payment_id: @payment.id 
        }
      end 

      should 'update it' do 
        response = perform(@current_user, @attrs)
        assert response[:payment]
        assert_equal response[:payment].payment_type, @attrs[:attributes][:payment_type]
        assert_equal response[:payment].period, @attrs[:attributes][:period]
        assert_equal response[:payment].concept, @attrs[:attributes][:concept]
        assert_equal response[:payment].amount, @attrs[:attributes][:amount]
        assert_equal response[:payment].due_date, @attrs[:attributes][:due_date].to_date
        assert_equal response[:payment].observations, @attrs[:attributes][:observations]
      end 
    end 
  end
end
