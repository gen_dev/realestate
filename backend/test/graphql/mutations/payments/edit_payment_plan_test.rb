require 'test_helper'

class Mutations::Payments::EditPaymentPlanTest < ActiveSupport::TestCase
  def perform(current_user, args = {})
    Mutations::Payments::EditPaymentPlan.new(object: nil, field: nil, context: { current_user: current_user }).resolve(args)
  end
  
  context 'when current user hasnt got permissions' do
    setup do 
      @current_user = create(:user)
      @payment_plan = create(:payment_plan)
      @attrs = {
        attributes: {
          payment_methods: ['Cash','Check','Card']
        },
        payment_plan_id: @payment_plan.id 
      }
    end

    should 'not edit payment plan' do
      assert_empty perform(@current_user, @attrs)
    end
  end

  context 'when current user has gendev permissions' do
    setup do 
      @current_user = create(:user_with_role_gendev)
      @payment_plan = create(:payment_plan)
      @attrs = {
        attributes: {
          payment_methods: ['Cash','Check','Card']
        },
        payment_plan_id: @payment_plan.id 
      }
    end 
    
    should 'edit any payment plan' do
      response = perform(@current_user, @attrs)
      assert response[:payment_plan]
      assert_equal response[:payment_plan].payment_methods, @attrs[:attributes][:payment_methods]
    end
  end
  
  context 'when current user has admin permissions' do
    setup do 
      @current_user = create(:user_with_role_admin)
    end 
    
    context 'when payment plan isnt related to user' do 
      setup do 
        @payment_plan = create(:payment_plan)
        @attrs = {
          attributes: {
            payment_methods: ['Cash','Check','Card']
          },
          payment_plan_id: @payment_plan.id 
        }
      end 
      
      should 'not update it' do 
        response = perform(@current_user, @attrs)
        assert_empty response
      end 
    end 
    
    context 'when payment plan is related to user through contract property agency' do 
      setup do 
        @payment_plan = create(:payment_plan, contract: create(:contract, property: create(:property, agency: create(:user_agency, user: @current_user).agency)))
        @attrs = {
          attributes: {
            payment_methods: ['Cash','Check','Card']
          },
          payment_plan_id: @payment_plan.id 
        }
      end 

      should 'update it' do 
        response = perform(@current_user, @attrs)
        assert response[:payment_plan]
        assert_equal response[:payment_plan].payment_methods, @attrs[:attributes][:payment_methods]
      end 
    end 
  end
end
