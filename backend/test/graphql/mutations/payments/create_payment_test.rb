require 'test_helper'

class Mutations::Payments::CreatePaymentTest < ActiveSupport::TestCase
  def perform(current_user, args = {})
    Mutations::Payments::CreatePayment.new(object: nil, field: nil, context: { current_user: current_user }).resolve(args)
  end

  context 'when current user hasnt got permissions' do
    setup do 
      @current_user = create(:user)
      @attrs = {
        attributes: {
          payment_type: 'Check',
          period: '5/12',
          concept: 'Water',
          state: 'pending',
          due_date: Time.now.days_ago(-2).to_date.to_s,
          amount: 3000,
          observations: 'Lorem ipsum it su damae',
          referral_id: create(:user).id, 
          referral_type: create(:user).class.name,
          sub_account_id: create(:sub_account).id
        }
      }
    end

    should 'not create payment' do
      assert_empty perform(@current_user, @attrs)
    end
  end

  context 'when current user has gendev permissions' do
    setup do 
      @current_user = create(:user_with_role_gendev)
    end 

    context 'when referral_type == User' do 
      setup do 
        @referral = create(:user)
      end

      context 'when required fields are missing' do 
        setup do 
          @attrs = {
            attributes: {
              payment_type: 'Check',
              period: '5/12',
              concept: 'Water',
              state: nil,
              due_date: Time.now.days_ago(-2).to_date.to_s,
              amount: 3000,
              observations: 'Lorem ipsum it su damae',
              referral_id: @referral.id, 
              referral_type: @referral.class.name,
              sub_account_id: create(:sub_account).id
            }
          }
        end

        should 'not create payment' do
          assert_empty perform(@current_user, @attrs)
        end
      end 

      context 'when all required fields are filled' do 
        setup do 
          @attrs = {
            attributes: {
              payment_type: 'Check',
              period: '5/12',
              concept: 'Water',
              state: 'pending',
              due_date: Time.now.days_ago(-2).to_date.to_s,
              amount: 3000,
              observations: 'Lorem ipsum it su damae',
              referral_id: @referral.id, 
              referral_type: @referral.class.name,
              sub_account_id: create(:sub_account).id
            }
          }
        end
     
        should 'create payment' do 
          response = perform(@current_user, @attrs)
          payment = response[:payment]
          assert payment.persisted?
          assert_equal payment.payment_type, @attrs[:attributes][:payment_type] 
          assert_equal payment.period, @attrs[:attributes][:period] 
          assert_equal payment.concept, @attrs[:attributes][:concept]
  
          assert_equal payment.due_date.to_s, @attrs[:attributes][:due_date]
  
          assert_equal payment.state, @attrs[:attributes][:state]
          assert_equal payment.amount.to_f, @attrs[:attributes][:amount].to_f
          assert_equal payment.observations, @attrs[:attributes][:observations]
          assert_equal payment.referral_id, @attrs[:attributes][:referral_id]
          assert_equal payment.referral_type, @attrs[:attributes][:referral_type]
        end
      end 
    end
  
    context 'when referral_type == Agency' do 
      setup do 
        @referral = create(:agency)
      end
      
      context 'when required fields are missing' do 
        setup do 
          @attrs = {
            attributes: {
              payment_type: 'Check',
              period: '5/12',
              concept: 'Water',
              state: nil,
              due_date: Time.now.days_ago(-2).to_date.to_s,
              amount: 3000,
              observations: 'Lorem ipsum it su damae',
              referral_id: @referral.id, 
              referral_type: @referral.class.name,
              sub_account_id: create(:sub_account).id

            },
          }
        end

        should 'not create payment' do
          assert_empty perform(@current_user, @attrs)
        end
      end 

      context 'when all required fields are filled' do 
        setup do 
          @attrs = {
            attributes: {
              payment_type: 'Check',
              period: '5/12',
              concept: 'Water',
              state: 'pending',
              due_date: Time.now.days_ago(-2).to_date.to_s,
              amount: 3000,
              observations: 'Lorem ipsum it su damae',
              referral_id: @referral.id, 
              referral_type: @referral.class.name,
              sub_account_id: create(:sub_account).id
            },
          }
        end
     
        should 'create payment' do 
          response = perform(@current_user, @attrs)
          payment = response[:payment]
          assert payment.persisted?
          assert_equal payment.payment_type, @attrs[:attributes][:payment_type] 
          assert_equal payment.period, @attrs[:attributes][:period] 
          assert_equal payment.concept, @attrs[:attributes][:concept]
  
          assert_equal payment.due_date.to_s, @attrs[:attributes][:due_date]
  
          assert_equal payment.state, @attrs[:attributes][:state]
          assert_equal payment.amount.to_f, @attrs[:attributes][:amount].to_f
          assert_equal payment.observations, @attrs[:attributes][:observations]
          assert_equal payment.referral_id, @attrs[:attributes][:referral_id]
          assert_equal payment.referral_type, @attrs[:attributes][:referral_type]
        end
      end 
    end
  end

  context 'when current user has admin permissions' do
    setup do 
      @current_user = create(:user_with_role_admin)
    end 
      
    context 'when payment is related to user' do 
      setup do 
        @sub_account = create(:sub_account, agency: create(:user_agency, selected: true, user: @current_user).agency)
      end

      context 'when referral_type == User' do 
        setup do 
          @referral = create(:user)
        end

        context 'when required fields are missing' do 
          setup do 
            @attrs = {
              attributes: {
                payment_type: 'Check',
                period: '5/12',
                concept: 'Water',
                state: nil,
                due_date: Time.now.days_ago(-2).to_date.to_s,
                amount: 3000,
                observations: 'Lorem ipsum it su damae',
                referral_id: @referral.id, 
                referral_type: @referral.class.name,
                sub_account_id: @sub_account.id
              },
            }
          end

          should 'not create payment' do
            assert_empty perform(@current_user, @attrs)
          end
        end 

        context 'when all required fields are filled' do 
          setup do 
            @attrs = {
              attributes: {
                payment_type: 'Check',
                period: '5/12',
                concept: 'Water',
                state: 'pending',
                due_date: Time.now.days_ago(-2).to_date.to_s,
                amount: 3000,
                observations: 'Lorem ipsum it su damae',
                referral_id: @referral.id, 
                referral_type: @referral.class.name,
                sub_account_id: @sub_account.id
              },
            }
          end
      
          should 'create payment' do 
            response = perform(@current_user, @attrs)
            payment = response[:payment]
            assert payment.persisted?
            assert_equal payment.payment_type, @attrs[:attributes][:payment_type] 
            assert_equal payment.period, @attrs[:attributes][:period] 
            assert_equal payment.concept, @attrs[:attributes][:concept]
    
            assert_equal payment.due_date.to_s, @attrs[:attributes][:due_date]
    
            assert_equal payment.state, @attrs[:attributes][:state]
            assert_equal payment.amount.to_f, @attrs[:attributes][:amount].to_f
            assert_equal payment.observations, @attrs[:attributes][:observations]
            assert_equal payment.referral_id, @attrs[:attributes][:referral_id]
            assert_equal payment.referral_type, @attrs[:attributes][:referral_type]
          end
        end 
      end

      context 'when referral_type == Agency' do 
        setup do 
          @referral = create(:agency)
        end
        
        context 'when required fields are missing' do 
          setup do 
            @attrs = {
              attributes: {
                payment_type: 'Check',
                period: '5/12',
                concept: 'Water',
                state: nil,
                due_date: Time.now.days_ago(-2).to_date.to_s,
                amount: 3000,
                observations: 'Lorem ipsum it su damae',
                referral_id: @referral.id, 
                referral_type: @referral.class.name,
                sub_account_id: @sub_account.id
              },
            }
          end

          should 'not create payment' do
            assert_empty perform(@current_user, @attrs)
          end
        end 

        context 'when all required fields are filled' do 
          setup do 
            @attrs = {
              attributes: {
                payment_type: 'Check',
                period: '5/12',
                concept: 'Water',
                state: 'pending',
                due_date: Time.now.days_ago(-2).to_date.to_s,
                amount: 3000,
                observations: 'Lorem ipsum it su damae',
                referral_id: @referral.id, 
                referral_type: @referral.class.name,
                sub_account_id: @sub_account.id
              },
            }
          end
      
          should 'create payment' do 
            response = perform(@current_user, @attrs)
            payment = response[:payment]
            assert payment.persisted?
            assert_equal payment.payment_type, @attrs[:attributes][:payment_type] 
            assert_equal payment.period, @attrs[:attributes][:period] 
            assert_equal payment.concept, @attrs[:attributes][:concept]
    
            assert_equal payment.due_date.to_s, @attrs[:attributes][:due_date]
    
            assert_equal payment.state, @attrs[:attributes][:state]
            assert_equal payment.amount.to_f, @attrs[:attributes][:amount].to_f
            assert_equal payment.observations, @attrs[:attributes][:observations]
            assert_equal payment.referral_id, @attrs[:attributes][:referral_id]
            assert_equal payment.referral_type, @attrs[:attributes][:referral_type]
          end
        end 
      end
    end

    context 'when payment isnt related to user' do 
      setup do 
        @attrs = {
          attributes: {
            payment_type: 'Check',
            period: '5/12',
            concept: 'Water',
            state: 'pending',
            due_date: Time.now.days_ago(-2).to_date.to_s,
            amount: 3000,
            observations: 'Lorem ipsum it su damae',
            referral_id: create(:agency).id, 
            referral_type: create(:agency).class.name,
            sub_account_id: create(:sub_account).id
          },
        }
      end

      should 'not create payment' do
        assert_empty perform(@current_user, @attrs)
      end
    end
  end
end
