require 'test_helper'

class Mutations::Payments::DeletePaymentPlanTest < ActiveSupport::TestCase
  def perform(current_user, args = {})
    Mutations::Payments::DeletePaymentPlan.new(object: nil, field: nil, context: { current_user: current_user }).resolve(args)
  end
  
  context 'when current user hasnt got permissions' do
    setup do 
      @current_user = create(:user)
      @payment_plan = create(:payment_plan)
    end
    
    should 'not delete any payment_plan' do
      response = perform(@current_user, { payment_plan_id: @payment_plan.id })
      assert_empty response
    end
  end
  
  context 'when current user has gendev permissions' do
    setup do 
      @current_user = create(:user_with_role_gendev)
      @payment_plan = create(:payment_plan)
    end 
    
    should 'delete any payment_plan' do
      response = perform(@current_user, { payment_plan_id: @payment_plan.id })
      assert response[:success]
    end
  end
  
  context 'when current user has admin permissions' do
    setup do 
      @current_user = create(:user_with_role_admin)
    end 
    
    context 'when payment plan isnt related to user' do 
      setup do 
        @payment_plan = create(:payment_plan)
      end 
      
      should 'not delete it' do 
        response = perform(@current_user, { payment_plan_id: @payment_plan.id })
        assert_empty response
      end 
    end 
    
    context 'when payment plan is related to user through contract property agency' do 
      setup do 
        @payment_plan = create(:payment_plan, contract: create(:contract, property: create(:property, agency: create(:user_agency, user: @current_user).agency)))
      end 

      should 'delete it' do 
        response = perform(@current_user, { payment_plan_id: @payment_plan.id })
        assert response[:success]
      end 
    end 
  end
end