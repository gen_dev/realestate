require 'test_helper'

class Mutations::Payments::CreatePartialPaymentTest < ActiveSupport::TestCase
  def perform(current_user, args = {})
    Mutations::Payments::CreatePartialPayment.new(object: nil, field: nil, context: { current_user: current_user }).resolve(args)
  end

  context 'when current user hasnt got permissions' do
    setup do 
      @current_user = create(:user)
      @agency = create(:agency)
      create(:user_agency, user: @current_user, agency: @agency)
      @sub_account = create(:sub_account, agency: @agency)
      @payment = create(:payment, sub_account: @sub_account)
      @attrs = {
        value: 200,
        payment_id: @payment.id
      }
    end

    should 'not create partial payment' do
      assert_empty perform(@current_user, @attrs)
    end
  end

  context 'when current user has gendev permissions' do
    setup do 
      @current_user = create(:user_with_role_gendev)
    end 

    context 'when required fields are missing' do 
      setup do 
        @payment = create(:payment)
        @attrs = {
          value: nil,
          payment_id: @payment.id
        }
      end

      should 'not create partial payment' do
        assert_empty perform(@current_user, @attrs)
      end
    end

    context 'when required fields are filled' do 
      setup do 
        @payment = create(:payment)
        @attrs = {
          value: 200,
          payment_id: @payment.id
        }
      end

      should 'create partial payment' do 
        response = perform(@current_user, @attrs)
        partial_payment = response[:partial_payment]
        assert partial_payment.persisted?
        assert_equal partial_payment.value.to_f, @attrs[:value].to_f 
      end
    end
  end

  context 'when current user has admin permissions' do 
    setup do 
      @current_user = create(:user_with_role_admin)
    end

    context 'when belongs to agency' do 
      setup do 
        @agency = create(:agency)
        create(:user_agency, user: @current_user, agency: @agency)
        @sub_account = create(:sub_account, agency: @agency)
        @payment = create(:payment, sub_account: @sub_account)
      end

      context 'when required fields are missing' do 
        setup do 
          @attrs = {
            value: nil,
            payment_id: @payment.id
          }
        end
  
        should 'not create partial payment' do
          assert_empty perform(@current_user, @attrs)
        end
      end
      
      context 'when all required fields are filled' do 
        setup do 
          @attrs = {
            value: 200,
            payment_id: @payment.id
          }
        end 

        should 'create partial payment' do 
          response = perform(@current_user, @attrs)
          partial_payment = response[:partial_payment]
          assert partial_payment.persisted?
          assert_equal partial_payment.value.to_f, @attrs[:value].to_f 
        end
      end
    end 

    context 'when doesnt belongs to agency' do 
      setup do 
        @payment = create(:payment)
        @attrs = {
          value: 200,
          payment_id: @payment.id
        }
      end

      should 'not create partial payment' do
        assert_empty perform(@current_user, @attrs)
      end
    end 
  end
end
