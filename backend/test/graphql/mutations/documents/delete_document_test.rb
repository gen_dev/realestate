require 'test_helper'

class Mutations::Documents::DeleteDocumentTest < ActiveSupport::TestCase
  def perform(current_user, args = {})
    Mutations::Documents::DeleteDocument.new(object: nil, field: nil, context: { current_user: current_user }).resolve(args)
  end
  
  context 'when current user hasnt got permissions' do
    setup do 
      @current_user = create(:user)
      @document = create(:document)
    end
    
    should 'not delete any document' do
      response = perform(@current_user, { document_id: @document.id })
      assert_empty response
    end
  end
  
  context 'when current user has gendev permissions' do
    setup do 
      @current_user = create(:user_with_role_gendev)
      @document = create(:document)
    end 
    
    should 'delete any document' do
      response = perform(@current_user, { document_id: @document.id })
      assert response[:success]
    end
  end
  
  context 'when current user has admin permissions' do
    setup do 
      @current_user = create(:user_with_role_admin)
    end 
    
    context 'when document isnt related to user' do 
      setup do 
        @document = create(:document)
      end 
      
      should 'not delete it' do 
        response = perform(@current_user, { document_id: @document.id })
        assert_empty response
      end 
    end 
    
    context 'when document is related to user through datable' do 
      setup do 
        @document = create(:document, documentable: create(:contract, property: create(:property, agency: create(:user_agency, user: @current_user).agency)))
      end 

      should 'delete it' do 
        response = perform(@current_user, { document_id: @document.id })
        assert response[:success]
      end 
    end 
  end
end