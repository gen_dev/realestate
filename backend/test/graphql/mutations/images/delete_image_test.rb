require 'test_helper'

class Mutations::Images::DeleteImageTest < ActiveSupport::TestCase
  def perform(current_user, args = {})
    Mutations::Images::DeleteImage.new(object: nil, field: nil, context: { current_user: current_user }).resolve(args)
  end
  
  context 'when current user hasnt got permissions' do
    setup do 
      @current_user = create(:user)
      @image = create(:image)
    end
    
    should 'not delete any image' do
      response = perform(@current_user, { image_id: @image.id })
      assert_empty response
    end
  end
  
  context 'when current user has gendev permissions' do
    setup do 
      @current_user = create(:user_with_role_gendev)
      @image = create(:image)
    end 
    
    should 'delete any image' do
      response = perform(@current_user, { image_id: @image.id })
      assert response[:success]
    end
  end
  
  context 'when current user has admin permissions' do
    setup do 
      @current_user = create(:user_with_role_admin)
    end 
    
    context 'when image isnt related to user' do 
      setup do 
        @image = create(:image)
      end 
      
      should 'not delete it' do 
        response = perform(@current_user, { image_id: @image.id })
        assert_empty response
      end 
    end 
    
    context 'when image is related to user through datable' do 
      setup do 
        @image = create(:image, imageable: create(:property, agency: create(:user_agency, user: @current_user).agency))
      end 

      should 'delete it' do 
        response = perform(@current_user, { image_id: @image.id })
        assert response[:success]
      end 
    end 
  end
end