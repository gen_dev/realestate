require 'test_helper'

class Mutations::Taxes::EditTaxTest < ActiveSupport::TestCase
  def perform(current_user, args = {})
    Mutations::Taxes::EditTax.new(object: nil, field: nil, context: { current_user: current_user }).resolve(args)
  end
  
  context 'when current user hasnt got permissions' do
    setup do 
      @current_user = create(:user)
      @tax = create(:tax)
      @attrs = {
        attributes: {
          name: 'EPE',
          state: 'pending',
          interval: 2,
          duration: Time.now
        },
        tax_id: @tax.id 
      }
    end

    should 'not edit tax' do
      assert_empty perform(@current_user, @attrs)
    end
  end

  context 'when current user has gendev permissions' do
    setup do 
      @current_user = create(:user_with_role_gendev)
      @tax = create(:tax)
      @attrs = {
        attributes: {
          name: 'EPE',
          state: 'pending',
          interval: 2,
          duration: Time.now
        },
        tax_id: @tax.id 
      }
    end 
    
    should 'edit any tax' do
      response = perform(@current_user, @attrs)
      assert response[:tax]
      assert_equal response[:tax].name, @attrs[:attributes][:name]
      assert_equal response[:tax].state, @attrs[:attributes][:state]
      assert_equal response[:tax].interval, @attrs[:attributes][:interval]
      assert_equal response[:tax].duration, @attrs[:attributes][:duration].to_date
    end
  end
  
  context 'when current user has admin permissions' do
    setup do 
      @current_user = create(:user_with_role_admin)
    end 
    
    context 'when tax isnt related to user' do 
      setup do 
        @tax = create(:tax)
        @attrs = {
          attributes: {
            name: 'EPE',
            state: 'pending',
            interval: 2,
            duration: Time.now
          },
          tax_id: @tax.id 
        }
      end 
      
      should 'not update it' do 
        response = perform(@current_user, @attrs)
        assert_empty response
      end 
    end 

    context 'when tax is related to user through contract property agency' do 
      setup do 
        @tax = create(:tax, taxable: create(:contract, property: create(:property, agency: create(:user_agency, user: @current_user).agency)))
        @attrs = {
          attributes: {
            name: 'EPE',
            state: 'pending',
            interval: 2,
            duration: Time.now          
          },
          tax_id: @tax.id 
        }
      end 

      should 'update it' do 
        response = perform(@current_user, @attrs)
        assert response[:tax]
        assert_equal response[:tax].name, @attrs[:attributes][:name]
        assert_equal response[:tax].state, @attrs[:attributes][:state]
        assert_equal response[:tax].interval, @attrs[:attributes][:interval]
        assert_equal response[:tax].duration, @attrs[:attributes][:duration].to_date
      end 
    end 
  end
end
