require 'test_helper'

class Mutations::Taxes::DeleteTaxTest < ActiveSupport::TestCase
  def perform(current_user, args = {})
    Mutations::Taxes::DeleteTax.new(object: nil, field: nil, context: { current_user: current_user }).resolve(args)
  end
  
  context 'when current user hasnt got permissions' do
    setup do 
      @current_user = create(:user)
      @tax = create(:tax)
    end
    
    should 'not delete any tax' do
      response = perform(@current_user, { tax_id: @tax.id })
      assert_empty response
    end
  end
  
  context 'when current user has gendev permissions' do
    setup do 
      @current_user = create(:user_with_role_gendev)
      @tax = create(:tax)
    end 
    
    should 'delete any tax' do
      response = perform(@current_user, { tax_id: @tax.id })
      assert response[:success]
    end
  end
  
  context 'when current user has admin permissions' do
    setup do 
      @current_user = create(:user_with_role_admin)
    end 
    
    context 'when tax isnt related to user' do 
      setup do 
        @tax = create(:tax)
      end 
      
      should 'not delete it' do 
        response = perform(@current_user, { tax_id: @tax.id })
        assert_empty response
      end 
    end 
    
    context 'when tax is related to user through contract property agency' do 
      setup do 
        @tax = create(:tax, taxable: create(:contract, property: create(:property, agency: create(:user_agency, user: @current_user).agency)))
      end 

      should 'delete it' do 
        response = perform(@current_user, { tax_id: @tax.id })
        assert response[:success]
      end 
    end 
  end
end