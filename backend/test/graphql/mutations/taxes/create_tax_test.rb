require 'test_helper'

class Mutations::Taxes::CreateTaxTest < ActiveSupport::TestCase
  def perform(current_user, args = {})
    Mutations::Taxes::CreateTax.new(object: nil, field: nil, context: { current_user: current_user }).resolve(args)
  end
  
  context 'when current user hasnt got permissions' do
    setup do 
      @current_user = create(:user)
      @taxable = create(:contract)
      @debtor = create(:user, email: 'john@doe.com', password: '[omitted]')
      @attrs = {
        attributes: {
          name: 'EPE',
          state: 'pending',
          interval: 2,
          duration: Time.now,
          debtor_id: @debtor.id, 
          debtor_type: @debtor.class.name,
          taxable_id: @taxable.id,
          taxable_type: @taxable.class.name
        },
      }
    end

    should 'not create tax' do
      assert_empty perform(@current_user, @attrs)
    end
  end

  context 'when current user has gendev permissions' do
    setup do 
      @current_user = create(:user_with_role_gendev)
    end 

    context 'when debtor_type == User' do 
      setup do 
        @taxable = create(:contract)
        @debtor = create(:user, email: 'john@doe.com', password: '[omitted]')
      end

      context 'when required fields are missing' do 
        setup do 
          @attrs = {
            attributes: {
              name: nil,
              state: 'pending',
              interval: 2,
              duration: Time.now,
              debtor_id: @debtor.id, 
              debtor_type: @debtor.class.name,
              taxable_id: nil,
              taxable_type: @taxable.class.name
            },
          }
        end 

        should 'not create tax' do
          assert_empty perform(@current_user, @attrs)
        end
      end 

      context 'when required fields are filled' do 
        setup do 
          @attrs = {
            attributes: {
              name: 'EPE',
              state: 'pending',
              interval: 2,
              duration: Time.now,
              debtor_id: @debtor.id, 
              debtor_type: @debtor.class.name,
              taxable_id: @taxable.id,
              taxable_type: @taxable.class.name
            },
          }
        end

        should 'create tax' do 
          response = perform(@current_user, @attrs)
          tax = response[:tax]
          assert tax.persisted?
          assert_equal tax.name, @attrs[:attributes][:name] 
          assert_equal tax.state, @attrs[:attributes][:state]
          assert_equal tax.interval, @attrs[:attributes][:interval]
          assert_equal tax.taxable_id, @attrs[:attributes][:taxable_id]
          assert_equal tax.taxable_type, @attrs[:attributes][:taxable_type]
          assert_equal tax.debtor_id, @attrs[:attributes][:debtor_id]
          assert_equal tax.debtor_type, @attrs[:attributes][:debtor_type]
        end 
      end
    end

    context 'when debtor_type == Agency' do 
      setup do 
        @taxable = create(:contract)
        @debtor = create(:agency, name: 'Test Agency S.A')
      end

      context 'when required fields are missing' do 
        setup do 
          @attrs = {
            attributes: {
              name: nil,
              state: 'pending',
              interval: 2,
              duration: Time.now,
              debtor_id: @debtor.id, 
              debtor_type: @debtor.class.name,
              taxable_id: nil,
              taxable_type: @taxable.class.name
            },
          }
        end 
        
        should 'not create tax' do
          assert_empty perform(@current_user, @attrs)
        end
      end 

      context 'when required fields are filled' do 
        setup do 
          @attrs = {
            attributes: {
              name: 'EPE',
              state: 'pending',
              interval: 2,
              duration: Time.now,
              debtor_id: @debtor.id, 
              debtor_type: @debtor.class.name,
              taxable_id: @taxable.id,
              taxable_type: @taxable.class.name
            },
          }
        end

        should 'create tax' do 
          response = perform(@current_user, @attrs)
          tax = response[:tax]
          assert tax.persisted?
          assert_equal tax.name, @attrs[:attributes][:name] 
          assert_equal tax.state, @attrs[:attributes][:state]
          assert_equal tax.interval, @attrs[:attributes][:interval]
          assert_equal tax.taxable_id, @attrs[:attributes][:taxable_id]
          assert_equal tax.taxable_type, @attrs[:attributes][:taxable_type]
          assert_equal tax.debtor_id, @attrs[:attributes][:debtor_id]
          assert_equal tax.debtor_type, @attrs[:attributes][:debtor_type]
        end
      end
    end
  end

  context 'when current user has admin permissions' do
    setup do 
      @current_user = create(:user_with_role_admin)
    end 

    context 'when tax is related to user' do 
      setup do
        @taxable = create(:contract, property: create(:property, agency: create(:user_agency, user: @current_user).agency))
      end 

      context 'when debtor_type == User' do 
        setup do 
          @debtor = create(:user, email: 'john@doe.com', password: '[omitted]')
        end

        context 'when required fields are missing' do 
          setup do 
            @attrs = {
              attributes: {
                name: nil,
                state: 'pending',
                interval: 2,
                duration: Time.now,
                debtor_id: @debtor.id, 
                debtor_type: @debtor.class.name,
                taxable_id: nil,
                taxable_type: @taxable.class.name
              },
            }
          end 

          should 'not create tax' do
            assert_empty perform(@current_user, @attrs)
          end
        end 

        context 'when required fields are filled' do 
          setup do 
            @attrs = {
              attributes: {
                name: 'EPE',
                state: 'pending',
                interval: 2,
                duration: Time.now,
                debtor_id: @debtor.id, 
                debtor_type: @debtor.class.name,
                taxable_id: @taxable.id,
                taxable_type: @taxable.class.name
              },
            }
          end

          should 'create tax' do 
            response = perform(@current_user, @attrs)
            tax = response[:tax]
            assert tax.persisted?
            assert_equal tax.name, @attrs[:attributes][:name] 
            assert_equal tax.state, @attrs[:attributes][:state]
            assert_equal tax.interval, @attrs[:attributes][:interval]
            assert_equal tax.taxable_id, @attrs[:attributes][:taxable_id]
            assert_equal tax.taxable_type, @attrs[:attributes][:taxable_type]
            assert_equal tax.debtor_id, @attrs[:attributes][:debtor_id]
            assert_equal tax.debtor_type, @attrs[:attributes][:debtor_type]
          end 
        end
      end

      context 'when debtor_type == Agency' do 
        setup do 
          @debtor = create(:agency, name: 'Test Agency S.A')
        end

        context 'when required fields are missing' do 
          setup do 
            @attrs = {
              attributes: {
                name: nil,
                state: 'pending',
                interval: 2,
                duration: Time.now,
                debtor_id: @debtor.id, 
                debtor_type: @debtor.class.name,
                taxable_id: nil,
                taxable_type: @taxable.class.name
              },
            }
          end 
          
          should 'not create tax' do
            assert_empty perform(@current_user, @attrs)
          end
        end 

        context 'when required fields are filled' do 
          setup do 
            @attrs = {
              attributes: {
                name: 'EPE',
                state: 'pending',
                interval: 2,
                duration: Time.now,
                debtor_id: @debtor.id, 
                debtor_type: @debtor.class.name,
                taxable_id: @taxable.id,
                taxable_type: @taxable.class.name
              },
            }
          end

          should 'create tax' do 
            response = perform(@current_user, @attrs)
            tax = response[:tax]
            assert tax.persisted?
            assert_equal tax.name, @attrs[:attributes][:name] 
            assert_equal tax.state, @attrs[:attributes][:state]
            assert_equal tax.interval, @attrs[:attributes][:interval]
            assert_equal tax.taxable_id, @attrs[:attributes][:taxable_id]
            assert_equal tax.taxable_type, @attrs[:attributes][:taxable_type]
            assert_equal tax.debtor_id, @attrs[:attributes][:debtor_id]
            assert_equal tax.debtor_type, @attrs[:attributes][:debtor_type]
          end
        end
      end
    end

    context 'when tax isnt related to user' do 
      setup do
        @taxable = create(:contract)
        @debtor = create(:user)
        @attrs = {
          attributes: {
            name: 'EPE',
            state: 'pending',
            interval: 2,
            duration: Time.now,
            debtor_id: @debtor.id, 
            debtor_type: @debtor.class.name,
            taxable_id: @taxable.id,
            taxable_type: @taxable.class.name
          },
        }
      end 
      
      should 'not create tax' do
        assert_empty perform(@current_user, @attrs)
      end
    end 
  end
end