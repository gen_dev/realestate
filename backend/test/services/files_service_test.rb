require 'test_helper'

class FilesServiceTest < ActiveSupport::TestCase
  context '#initialize' do 
    context 'when params are nil' do 
      setup do 
        @params = nil
      end

      should 'raise ArgumentError' do 
        response = assert_raise ArgumentError do
          FilesService.new(@params)
        end
        assert_match /This params are required: files, object_class, object_id/, response.message
      end
    end

    context 'when missing params :object_class' do 
      setup do 
        @params = {
          files: [fixture_file_upload('document.pdf')],
          object_id: 1
        }
      end

      should 'raise ArgumentError' do 
        response = assert_raise ArgumentError do
          FilesService.new(@params)
        end
        assert_match /This params are required: files, object_class, object_id/, response.message
      end
    end
  
    context 'when missing params :object_id' do 
      setup do 
        @params = {
          files: [fixture_file_upload('document.pdf')],
          object_class: 'Contract'
        }
      end

      should 'raise ArgumentError' do 
        response = assert_raise ArgumentError do
          FilesService.new(@params)
        end
        assert_match /This params are required: files, object_class, object_id/, response.message
      end
    end

    context 'when all required params exists' do 
      setup do 
        @params = {
          files: [fixture_file_upload('document.pdf')],
          object_class: 'Contract',
          object_id: 1
        }
      end

      context 'when object class/id doesnt match any record in db' do 
        should 'raise StandardError' do 
          response = assert_raise StandardError do
            FilesService.new(@params)
          end
          assert_match /Couldn't find Contract with 'id'=1/, response.message
        end
      end

      context 'when object class/id does match a record in db' do 
        setup do 
          @params[:object_id] = create(:contract).id
        end 

        should 'not raise StandardError' do 
          assert FilesService.new(@params)
        end
      end
    end
  end 

  context '#upload' do 
    context 'when object is a Contract' do 
      setup do 
        @object = create(:contract)
      end

      context 'when file doesnt exists' do 
        setup do 
          @params = {
            files: ['prueba'],
            object_class: @object.class.name,
            object_id: @object.id
          }
        end

        should 'raise NoMethodError' do 
          response = assert_raise NoMethodError do
            FilesService.new(@params).upload
          end
          assert_match /undefined method `original_filename' for "prueba"/, response.message
          assert_empty @object.reload.documents
        end
      end

      context 'when file exists' do 
        setup do 
          @params = {
            files: [fixture_file_upload('document.pdf')],
            object_class: @object.class.name,
            object_id: @object.id
          }
        end

        should 'add files to object' do 
          assert FilesService.new(@params).upload
          documents = @object.reload.documents
          assert documents
          assert_equal documents.count, 1
          assert_equal documents[0].class, Document
          assert_equal documents[0].document.url, "/uploads/#{@object.class.to_s.downcase}/document/#{@object.id}/document.pdf"
        end
      end 

      context 'when multiple file exists' do 
        setup do 
          @params = {
            files: [fixture_file_upload('document.pdf'), fixture_file_upload('image.jpeg')],
            object_class: @object.class.name,
            object_id: @object.id
          }
        end

        should 'add multiple files to object' do 
          assert FilesService.new(@params).upload
          documents = @object.reload.documents.sort
          assert documents
          assert_equal documents.count, 2
          assert_equal documents[0].class, Document
          assert_equal documents[1].class, Document
          assert_equal documents[0].document.url, "/uploads/#{@object.class.to_s.downcase}/document/#{@object.id}/document.pdf"
          assert_equal documents[1].document.url, "/uploads/#{@object.class.to_s.downcase}/document/#{@object.id}/image.jpeg"
        end
      end 
    end
    
    context 'when object is a Property' do 
      setup do 
        @object = create(:property)
      end

      context 'when file doesnt exists' do 
        setup do 
          @params = {
            files: ['prueba'],
            object_class: @object.class.name,
            object_id: @object.id
          }
        end

        should 'raise NoMethodError' do 
          response = assert_raise NoMethodError do
            FilesService.new(@params).upload
          end
          assert_match /undefined method `original_filename' for "prueba"/, response.message
          assert_empty @object.reload.images
        end
      end

      context 'when file exists' do 
        setup do 
          @params = {
            files: [fixture_file_upload('image.jpeg')],
            object_class: @object.class.name,
            object_id: @object.id
          }
        end

        should 'add files to object' do 
          assert FilesService.new(@params).upload
          images = @object.reload.images
          assert images
          assert_equal images.count, 1
          assert_equal images[0].class, Image
          assert_equal images[0].image.url, "/uploads/#{@object.class.to_s.downcase}/image/#{@object.id}/image.jpeg"
        end
      end 

      context 'when multiple file exists' do 
        setup do 
          @params = {
            files: [fixture_file_upload('image.jpeg'), fixture_file_upload('image.jpeg')],
            object_class: @object.class.name,
            object_id: @object.id
          }
        end

        should 'add multiple files to object' do 
          assert FilesService.new(@params).upload
          images = @object.reload.images.sort
          assert images
          assert_equal images.count, 2
          assert_equal images[0].class, Image
          assert_equal images[1].class, Image
          assert_equal images[0].image.url, "/uploads/#{@object.class.to_s.downcase}/image/#{@object.id}/image.jpeg"
          assert_equal images[1].image.url, "/uploads/#{@object.class.to_s.downcase}/image/#{@object.id}/image.jpeg"
        end
      end 
    end
  end
end