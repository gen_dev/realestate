require 'test_helper'

class ImageUploaderTest < ActiveSupport::TestCase
  context 'when class is nil' do 
    setup do 
      @uploader = ImageUploader.new
    end 

    should 'raise NoMethodError' do 
      assert_raise NoMethodError do 
        @uploader.store!(fixture_file_upload('image.jpeg'))
      end 
    end
  end 
  
  context 'when class hasnt got uploader' do 
    setup do 
      @account = create(:account)
      @uploader = ImageUploader.new(@account)
    end 
    
    should 'raise NoMethodError' do 
      assert_raise NoMethodError do 
        @uploader.store!(fixture_file_upload('image.jpeg'))
      end
    end 
  end 

  context 'when class has uploader mounted' do 
    setup do 
      @image = create(:image_without_attachment)
      @uploader = ImageUploader.new(@image)
    end 

    context 'when file format is wrong' do 
      setup do 
        @file = fixture_file_upload('document.pdf')
      end 
      
      context 'when #store!' do 
        should 'raise CarrierWave::IntegrityError' do 
          response = assert_raise CarrierWave::IntegrityError do 
            assert @uploader.store!(@file)
          end 
          assert response.message, "You are not allowed to upload application/pdf files, allowed types: (?-mix:image\\/)"
        end
      end
    end

    context 'when file format is correct' do 
      setup do 
        @file = fixture_file_upload('image.jpeg')
      end 

      context 'when #store!' do 
        should 'not raise NoMethodError' do 
          assert @uploader.store!(@file)
        end
      end

      context 'when model#images' do 
        should 'create image' do 
          @uploader.store!(@file)
          assert @image.image
        end
      end 
    end 
  end 
end