require 'test_helper'

class DocumentUploaderTest < ActiveSupport::TestCase
  context 'when class is nil' do 
    setup do 
      @uploader = DocumentUploader.new
    end 

    should 'raise NoMethodError' do 
      assert_raise NoMethodError do 
        @uploader.store!(fixture_file_upload('document.pdf'))
      end 
    end
  end 
  
  context 'when class hasnt got uploader' do 
    setup do 
      @account = create(:account)
      @uploader = DocumentUploader.new(@account)
    end 
    
    should 'raise NoMethodError' do 
      assert_raise NoMethodError do 
        @uploader.store!(fixture_file_upload('document.pdf'))
      end
    end 
  end 

  context 'when class has uploader mounted' do 
    setup do 
      @document = create(:document_without_attachment)
      @uploader = DocumentUploader.new(@document)
    end 

    context 'when #store!' do 
      should 'not raise NoMethodError' do 
        assert @uploader.store!(fixture_file_upload('document.pdf'))
      end
    end

    context 'when model#documents' do 
      should 'create document' do 
        @uploader.store!(fixture_file_upload('document.pdf'))
        assert @document.document
      end
    end 
  end 
end