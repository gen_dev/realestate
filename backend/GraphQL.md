| Index |
| ----- |
| **Queries**<br> 1. [Address](#address-queries)<br> 2. [Agency](#agency-queries)<br> 3. [Cash](#cash-queries)<br> 4. [Contract](#contract-queries)<br> 5. [Contract Expense](#contract-expense-queries)<br> 6. [Contract Plan](#contract-plan-queries)<br> 7. [Image](#image-queries)<br>8. [Document](#document-queries)<br>9. [Payment](#payment-queries)<br> 10. [Payment Plan](#payment-plan-queries)<br>11. [Partial Payment](#partial-payment-queries)<br> 12. [Property](#property-queries)<br> 13. [Simple Data](#simple-data-queries)<br>14. [Tax](#tax-queries)<br> 15. [User](#user-queries)<br> 16. [User Agency](#user-agency-queries) |
| **Mutations**<br> 1. [Address](#address-mutations)<br> 2. [Agency](#agency-mutations)<br> 3. [Cash](#cash-mutations)<br> 4. [Contract](#contract-mutations)<br> 5. [Contract Expense](#contract-expense-mutations)<br> 6. [Contract Plan](#contract-plan-mutations)<br> 7. [Payment](#payment-mutations)<br> 8. [Payment Plan](#payment-plan-mutations)<br>9. [Partial Payment](#partial-payment-mutations)<br> 10. [Property](#property-mutations)<br>11. [Simple Data](#simple-data-mutations)<br>12. [Tax](#tax-mutations)<br> 13. [User](#user-mutations)<br> 14. [User Agency](#user-agency-mutations) |

# Example Queries

## Address Queries

**Get All Addresses with Polymorphic Addressable**  

```graphql 
query addresses {
  addresses {
    id
    street
    number
    unit
    city
    state
    country
    neighborhood
    addressable {
      ... on User {
        id
        email
        name
      }
      ... on Agency {
        id
        name
      }
      ... on Property {
        id
        propertyType
        state
        description
        price
        images {
          id
          name 
          size
          url
        }
      }
    }
  }
}
```

**Get Address By ID with Polymorphic Addressable**  

```graphql 
query addresses {
  address(id: 1) {
    street
    number
    unit
    city
    state
    country
    neighborhood
    addressable {
      ... on User {
        id
        email
        name
      }
      ... on Agency {
        id
        name
      }
      ... on Property {
        id
        propertyType
        state
        description
        price
        images {
          id
          name 
          size
          url
        }
      }
    }
  }
}
```

**Get All Addresses w/some filter**

```graphql
query addresses {
  addresses(filters: { number: "666" } ) {
    id
    street
    number
    unit
    city
    state
    country
    neighborhood
    addressable {
      ... on Agency {
        id
        name
      }
      ... on Property {
        id
        state
        propertyType
        description
      }
      ... on User {
        id
        email
        name
      }
    }
  }
}
```

## Agency Queries 

**Get All Agencies**  

```graphql 
query agencies {
  agencies {
    id
    name
    address {
      id
      street
      number
      unit
      city
      state
      country
      neighborhood
    }
    properties {
      id
      propertyType
      state
      description
      price
      images {
          id
          name 
          size
          url
        }
    }
    users {
      id
      name 
      email
    }
    userAgencies {
      id
      selected
    }
  }
}
```

**Get Agency by ID**  

```graphql 
query agency {
  agency(id: 1) {
    name
    address {
      id
      street
      number
      unit
      city
      state
      country
      neighborhood
    }
    properties {
      id
      propertyType
      state
      description
      price
      images {
          id
          name 
          size
          url
        }
    }
    users {
      id
      name 
      email
    }
    userAgencies {
      id
      selected
    }
  }
}
```

**Get All Agencies w/some filter**

```graphql
query agencies { 
  agencies(filters: { name: "GenDev" } ) {
    id
    name
    address {
      id
      street
      number
      unit
      city
      state
      country
      neighborhood
    }
    properties {
      id
      propertyType
      state
      description
      price
      images {
          id
          name 
          size
          url
        }
    }
    users {
      id
      name 
      email
    }
    userAgencies {
      id
      selected
    }
  }
}
```

## Cash Queries 

**Get All Cashes**  

```graphql 
query cashes {
  cashes {
    id
    name
    balance
    payments {
      id
      paymentType
      period
      concept
      state
      dueDate
      amount
      contract {
        id
        state
        contractType
        documents {
          id
          name
          size
          url
        }
        property {
          id
          propertyType
          state
          description
          price
          images {
          id
          name 
          size
          url
        }
        }
      }
      referral {
        ... on User {
          id
          email
          name
        }
        ... on Agency {
          id
          name
        }
      }
    }
  }
}
```

**Get Cash by ID**  

```graphql 
query cash {
  cash(id: 1) {
    name
    balance
    payments {
      id
      paymentType
      period
      concept
      state
      dueDate
      amount
      contract {
        id
        state
        contractType
        documents {
          id
          name
          size
          url
        }
        property {
          id
          propertyType
          state
          description
          price
          images {
          id
          name 
          size
          url
        }
        }
      }
      referral {
        ... on User {
          id
          email
          name
        }
        ... on Agency {
          id
          name
        }
      }
    }
  }
}
```

**Get All Cashes w/some filter**

```graphql
query cashes {
  cashes(filters: { balance: 3000 } ) {
    id
    name
    balance
    payments {
      id
      paymentType
      period
      concept
      state
      dueDate
      amount
      contract {
        id
        state
        contractType
        property {
          id
          propertyType
          state
          description
        }
      }
      referral {
        ... on User {
          id
          email
          name
        }
        ... on Agency {
          id
          name
        }
      }
    }
  }
}
```

## Contract Queries 

**Get All Contracts**

```graphql
query contracts {
  contracts {
    id
    state
    contractType
    documents {
      id
      name
      size
      url
    }
    property {
      id
      propertyType
      state
      description
      price
      images {
          id
          name 
          size
          url
        }
    }
    contractPlan {
      duration
      rentUpdatePeriod
      rentValue
      property {
        id
        propertyType
        state
        description
        price
        images {
          id
          name 
          size
          url
        }
      }
    }
    paymentPlan {
      owner
      tenant
      paymentMethods
    }
    taxes {
      name
      state
      interval
      lastPaymentDate
      debtor {
        ... on User {
          id
          email
          name
        }
        ... on Agency {
          id
          name
        }
      }
    }
    payments {
      period
      concept
      state
      dueDate
      paymentType
      amount
      cash {
        id
        name
        balance
      }
      referral {
        ... on User {
          id
          email
          name
        }
        ... on Agency {
          id
          name
        }
      }
    }
    owner {
      id 
      email 
      name
    }
    tenant {
    	id 
      email 
      name
    }
    overduePayments {
     	period
      concept
      state
      dueDate
      paymentType
      amount
      cash {
        id
        name
        balance
      }
      referral {
        ... on User {
          id
          email
          name
        }
        ... on Agency {
          id
          name
        }
      } 
    }
  }
}
```

**Get Contract by ID**

```graphql 
query contract {
  contract(id: 1) {
    id
    state
    contractType
    documents {
      id
      name
      size
      url
    }
    property {
      id
      propertyType
      state
      description
      price
      images {
          id
          name 
          size
          url
        }
    }
    contractPlan {
      duration
      rentUpdatePeriod
      rentValue
      property {
        id
        propertyType
        state
        description
        price
        images {
          id
          name 
          size
          url
        }
      }
    }
    paymentPlan {
      owner
      tenant
      paymentMethods
    }
    taxes {
      name
      state
      interval
      lastPaymentDate
      debtor {
        ... on User {
          id
          email
          name
        }
        ... on Agency {
          id
          name
        }
      }
    }
    payments {
      period
      concept
      state
      dueDate
      paymentType
      amount
      cash {
        id
        name
        balance
      }
      referral {
        ... on User {
          id
          email
          name
        }
        ... on Agency {
          id
          name
        }
      }
    }
    owner {
      id 
      email 
      name
    }
    tenant {
    	id 
      email 
      name
    }
    overduePayments {
     	period
      concept
      state
      dueDate
      paymentType
      amount
      cash {
        id
        name
        balance
      }
      referral {
        ... on User {
          id
          email
          name
        }
        ... on Agency {
          id
          name
        }
      } 
    }
  }
}
```

**Get All Contracts w/some filter**

```graphql
query contracts {
  contracts(filters: { state: "activo" } ) {
    id
    state
    contractType
    property {
      id
      propertyType
      state
      description
    }
    contractPlan {
      duration
      rentUpdatePeriod
      rentValue
      property {
        id
        propertyType
        state
        description
      }
    }
    paymentPlan {
      owner
      tenant
      paymentMethods
    }
    taxes {
      name
      state
      interval
      lastPaymentDate
      debtor {
        ... on User {
          id
          email
          name
        }
        ... on Agency {
          id
          name
        }
      }
    }
    payments {
      period
      concept
      state
      dueDate
      paymentType
      amount
      cash {
        id
        name
        balance
      }
      referral {
        ... on User {
          id
          email
          name
        }
        ... on Agency {
          id
          name
        }
      }
    }
    owner {
      id 
      email 
      name
    }
    tenant {
    	id 
      email 
      name
    }
    overduePayments {
     	period
      concept
      state
      dueDate
      paymentType
      amount
      cash {
        id
        name
        balance
      }
      referral {
        ... on User {
          id
          email
          name
        }
        ... on Agency {
          id
          name
        }
      } 
    }
  }
}
```

## Contract Expense Queries 

**Get All Contract Expenses**

```graphql
query contractExpenses {
  contractExpenses {
    id
    name
    value
    contractPlan {
      id
    	duration
      rentUpdatePeriod
      rentValue
      property {
        id
        propertyType
        state
        description
        price
        images {
          id
          name 
          size
          url
        }
      }
    }
  }
}
```

**Get Contract Expense by ID**

```graphql
query contractExpense {
  contractExpense(id: 1) {
    name
    value
    contractPlan {
      id
    	duration
      rentUpdatePeriod
      rentValue
      property {
        id
        propertyType
        state
        description
        price
        images {
          id
          name 
          size
          url
        }
      }
    }
  }
}
```

**Get All Contract Expenses w/some filter**

```graphql
query contractExpenses {
  contractExpenses(filters: { name: "Sellado" } ) {
    id
    name
    value
    contractPlan {
      id
      duration
      rentUpdatePeriod
      rentValue
      property {
        id
        propertyType
        state
        description
      }
    }
  }
}
```

## Contract Plan Queries 

**Get All Contract Plans**

```graphql
query contractPlans {
  contractPlans {
    id
    duration
    rentUpdatePeriod
    rentValue
    property {
      id
      propertyType
      state
      description
      price
      images {
          id
          name 
          size
          url
        }
    }
    contractExpenses {
      id
      name
      value
    }
    totalAmount
  }
}
```

**Get Contract Plan by ID**

```graphql
query contractPlan {
  contractPlan(id: 1) {
    duration
    rentUpdatePeriod
    rentValue
    property {
      id
      propertyType
      state
      description
      price
      images {
          id
          name 
          size
          url
        }
    }
    contractExpenses {
      id
      name
      value
    }
    totalAmount
  }
}
```

**Get All Contract Plans w/some filter**

```graphql
query contractPlans {
  contractPlans(filters: { duration: 12 } ) {
    duration
    rentUpdatePeriod
    rentValue
    property {
      id
      propertyType
      state
      description
    }
    contractExpenses {
      id
      name
      value
    }
    totalAmount
  }
}
```

## Image Queries 

**Get All Images**

```graphql
query images {
  images {
    id
    name
    size
    url
    imageable{
      ... on Property {
        id
        propertyType
        state
        description
        price
      }
    }
  }
}
```

**Get Image by ID**

```graphql
query image {
  image(id: 1) {
    id
    name
    size
    url
    imageable{
      ... on Property {
        id
        propertyType
        state
        description
        price
      }
    }
  }
}
```

**Get All Images w/some filter**

```graphql
query images {
  images(filters: { name: "Somefilename.jpg" }) {
    id
    name
    size
    url
    imageable{
      ... on Property {
        id
        propertyType
        state
        description
        price
      }
    }
  }
}
```

## Document Queries 

**Get All Documents**

```graphql
query documents {
  documents {
    id
    name
    size
    url
    documentable {
      ... on Contract {
        id
        state
        contractType
      }
    }
  }
}
```

**Get Document by ID**

```graphql
query document {
  document(id: 1) {
    id
    name
    size
    url
    documentable {
      ... on Contract {
        id
        state
        contractType
      }
    }
  }
}
```

**Get All Documents w/some filter**

```graphql
query images {
  images(filters: { name: "Somefilename.pdf" }) {
    id
    name
    size
    url
    imageable{
      ... on Property {
        id
        propertyType
        state
        description
        price
      }
    }
  }
}
```

## Payment Queries 

**Get All Payments with Polymorphic Referral**

```graphql
query payments {
  payments {
    id
    paymentType
    period
    concept
    state
    dueDate
    amount
    contract {
      id
      state
      contractType
      documents {
        id
        name
        size
        url
      }
      property {
        id
        propertyType
        state
        description
        price
        images {
          id
          name 
          size
          url
        }
      }
    }
    cash {
      id
      name
      balance
    }
    referral {
      ... on User {
        id
        email
        name
      }
      ... on Agency {
        id
        name
      }
    }
    partialPayments {
      id 
      value
    }
  }
}
```

**Get Payment by ID with Polymorphic Referral**

```graphql
query payment {
  payment(id: 1) {
    period
    concept
    state
    dueDate
    paymentType
    amount
    contract {
      id
      state
      contractType
      documents {
        id
        name
        size
        url
      }
      property {
        id
        propertyType
        state
        description
        price
        images {
          id
          name 
          size
          url
        }
      }
    }
    cash {
      id
      name
      balance
    }
    referral {
      ... on User {
        id
        email
        name
      }
      ... on Agency {
        id
        name
      }
    }
    partialPayments {
      id 
      value
    }
  }
}
```

**Get All Payments w/some filter**

```graphql
query payments {
  payments(filters: { concept: "Water" } ) {
    id
    paymentType
    period
    concept
    state
    dueDate
    amount
    contract {
      id
      state
      contractType
      property {
        id
        propertyType
        state
        description
      }
    }
    cash {
      id
      name
      balance
    }
    referral {
      ... on User {
        id
        email
        name
      }
      ... on Agency {
        id
        name
      }
    }
    partialPayments {
      id 
      value
    }
  }
}
```

## Payment Plan Queries 

**Get All Payment Plans**

```graphql
query paymentPlans {
  paymentPlans {
    owner
    tenant
    paymentMethods
    contract {
      id
      state
      contractType
      documents {
        id
        name
        size
        url
      }
      property {
        id
        propertyType
        state
        description
        price
        images {
          id
          name 
          size
          url
        }
      }
    }
  }
}
```

**Get Payment Plan by ID**

```graphql
query paymentPlan {
  paymentPlan(id: 1) {
    owner
    tenant
    paymentMethods
    contract {
      id
      state
      contractType
      documents {
        id
        name
        size
        url
      }
      property {
        id
        propertyType
        state
        description
        price
        images {
          id
          name 
          size
          url
        }
      }
    }
  }
}
```

**Get All Payment Plans w/some filter**

```graphql
query paymentPlans {
  paymentPlans(filters: { owner: "John" }) {
    owner
    tenant
    paymentMethods
    contract {
      id
      state
      contractType
      documents {
        id
        name
        size
        url
      }
      property {
        id
        propertyType
        state
        description
        price
        images {
          id
          name 
          size
          url
        }
      }
    }
  }
}
```

## Partial Payment Queries 

**Get All Partial Payments**

```graphql
query partialPayments {
  partialPayments {
    id 
    value
    payment {
      id
      paymentType
      period
      concept
      state
      dueDate
      amount
      contract {
        id
        state
        contractType
        documents {
          id
          name
          size
          url
        }
        property {
          id
          propertyType
          state
          description
          price
          images {
          id
          name 
          size
          url
        }
        }
      }
      cash {
        id
        name
        balance
      }
      referral {
        ... on User {
          id
          email
          name
        }
        ... on Agency {
          id
          name
        }
      }
    }
  }
}
```

**Get Partial Payment by ID**

```graphql
query partialPayment {
  partialPayment(id: 1) {
    value
    payment {
      id
      paymentType
      period
      concept
      state
      dueDate
      amount
      contract {
        id
        state
        contractType
        documents {
          id
          name
          size
          url
        }
        property {
          id
          propertyType
          state
          description
          price
          images {
          id
          name 
          size
          url
        }
        }
      }
      cash {
        id
        name
        balance
      }
      referral {
        ... on User {
          id
          email
          name
        }
        ... on Agency {
          id
          name
        }
      }
    }
  }
}
```

**Get All Partial Payments w/some filter**

```graphql
query partialPayments {
  partialPayments(filters: { value: 69 } ) {
    value
    payment {
      id
      paymentType
      period
      concept
      state
      dueDate
      amount
      contract {
        id
        state
        contractType
        property {
          id
          propertyType
          state
          description
        }
      }
      cash {
        id
        name
        balance
      }
      referral {
        ... on User {
          id
          email
          name
        }
        ... on Agency {
          id
          name
        }
      }
    }
  }
}
```

## Property Queries 

**Get All Properties**

```graphql
query properties {
  properties {
    id
    propertyType
    state
    description
    price
    images {
          id
          name 
          size
          url
        }
    activeTenant {
      id
      name
      email
    }
    activeContracts {
      id 
      state
      contractType
      documents {
        id
        name
        size
        url
      }
    }
    address {
      id 
      street
      number
      unit
      city
      state
      country
      neighborhood
    }
    contracts {
      id
      state
      contractType
      documents {
        id
        name
        size
        url
      }
    }
    owner {
      id
      name 
      email
    }
    characteristics {
      id
      name 
      value
      category
    }
  }
}
```

**Get Property by ID**

```graphql
query property {
  property(id: 1) {
    propertyType
    state
    description
    price
    images {
          id
          name 
          size
          url
        }
    activeTenant {
      id
      name
      email
    }
    activeContracts {
      id 
      state
      contractType
      documents {
        id
        name
        size
        url
      }
    }
    address {
      id 
      street
      number
      unit
      city
      state
      country
      neighborhood
    }
    contracts {
      id
      state
      contractType
      documents {
        id
        name
        size
        url
      }
    }
    owner {
      id
      name 
      email
    }
    characteristics {
      id
      name 
      value
      category
    }
  }
}
```

**Get All Properties w/some filter**

```graphql
query properties {
  properties(filters: { propertyType: "Apartment" } ) {
    id
    propertyType
    state
    description
    images {
          id
          name 
          size
          url
        }
    activeTenant {
      id
      name
      email
    }
    activeContracts {
      id 
      state
      contractType
    }
    address {
      id 
      street
      number 
      city
      state
      unit
    }
    contracts {
      id
      state
      contractType
    }
    owner {
      id
      name 
      email
    }
    characteristics {
      id
      name 
      value
      category
    }
  }
}
```

## Simple Data Queries 

**Get All Simple Datas with Polymorphic Datable**

```graphql
query simpleDatas {
  simpleDatas {
    id
    name
    value
    category
    datable {
      ... on Address {
        id
        street
        number
        unit
        city
        state
        country
        neighborhood
        addressable {
          ... on User {
            id
            email
            name
          }
          ... on Agency {
            id
            name
          }
          ... on Property {
            id
            state
            propertyType
            description
            images {
              id
              name 
              size
              url
            }
          }
        }
      }
      ... on Agency {
        id
        name
      }
      ... on Cash {
        id
        name
        balance
      }
      ... on Contract {
        id
        state
        contractType
        documents {
          id
          name
          size
          url
        }
        property {
          id
          propertyType
          state
          description
          price
          images {
            id
            name 
            size
            url
          }
        }
      }
      ... on ContractExpense {
        id
        name
        value
        contractPlan {
          id
          duration
          rentUpdatePeriod
          rentValue
          property {
            id
            propertyType
            state
            description
            price
            images {
              id
              name 
              size
              url
            }
          }
        }
      }
      ... on ContractPlan {
        duration
        rentUpdatePeriod
        rentValue
        property {
          id
          propertyType
          state
          description
          price
          images {
            id
            name 
            size
            url
          }
        }
      }
      ... on Payment {
        id
        paymentType
        period
        concept
        state
        dueDate
        amount
        contract {
          id
          state
          contractType
          documents {
            id
            name
            size
            url
          }
          property {
            id
            propertyType
            state
            description
            price
            images {
              id
              name 
              size
              url
            }
          }
        }
        cash {
          id
          name
          balance
        }
        referral {
          ... on User {
            id
            email
            name
          }
          ... on Agency {
            id
            name
          }
        }
      }
      ... on PaymentPlan {
        owner
        tenant
        paymentMethods
        contract {
          id
          state
          contractType
          documents {
            id
            name
            size
            url
          }
          property {
            id
            propertyType
            state
            description
            price
            images {
              id
              name 
              size
              url
            }
          }
        }
      }
      ... on PartialPayment {
        id 
        value
        payment {
          id
          paymentType
          period
          concept
          state
          dueDate
          amount
          contract {
            id
            state
            contractType
            documents {
              id
              name
              size
              url
            }
            property {
              id
              propertyType
              state
              description
              price
              images {
                id
                name 
                size
                url
              }
            }
          }
          cash {
            id
            name
            balance
          }
          referral {
            ... on User {
              id
              email
              name
            }
            ... on Agency {
              id
              name
            }
          }
        }
      }
      ... on Property {
        id
        propertyType
        state
        description
        price
        images {
          id
          name 
          size
          url
        }
      }
      ... on Tax {
        id
        name
        state
        interval
        lastPaymentDate
        debtor {
          ... on User {
            id
            email
            name
          }
          ... on Agency {
            id
            name
          }
        }
      }
      ... on User {
        id
        email
        name
      }
    }
  }
}
```

**Get Simple Data by ID with Polymorphic Datable**

```graphql
query simpleData {
  simpleData(id: 1) {
    name
    value
    category
    datable {
      ... on Address {
        id
        street
        number
        unit
        city
        state
        country
        neighborhood
        addressable {
          ... on User {
            id
            email
            name
          }
          ... on Agency {
            id
            name
          }
          ... on Property {
            id
            state
            propertyType
            description
            price
            images {
              id
              name 
              size
              url
            }
          }
        }
      }
      ... on Agency {
        id
        name
      }
      ... on Cash {
        id
        name
        balance
      }
      ... on Contract {
        id
        state
        contractType
        documents {
          id
          name
          size
          url
        }
        property {
          id
          propertyType
          state
          description
          price
          images {
            id
            name 
            size
            url
          }
        }
      }
      ... on ContractExpense {
        id
        name
        value
        contractPlan {
          id
          duration
          rentUpdatePeriod
          rentValue
          property {
            id
            propertyType
            state
            description
            price
            images {
              id
              name 
              size
              url
            }
          }
        }
      }
      ... on ContractPlan {
        duration
        rentUpdatePeriod
        rentValue
        property {
          id
          propertyType
          state
          description
          price
          images {
            id
            name 
            size
            url
          }
        }
      }
      ... on Payment {
        id
        paymentType
        period
        concept
        state
        dueDate
        amount
        contract {
          id
          state
          contractType
          documents {
            id
            name
            size
            url
          }
          property {
            id
            propertyType
            state
            description
            price
            images {
              id
              name 
              size
              url
            }
          }
        }
        cash {
          id
          name
          balance
        }
        referral {
          ... on User {
            id
            email
            name
          }
          ... on Agency {
            id
            name
          }
        }
      }
      ... on PaymentPlan {
        owner
        tenant
        paymentMethods
        contract {
          id
          state
          contractType
          documents {
            id
            name
            size
            url
          }
          property {
            id
            propertyType
            state
            description
            price
            images {
              id
              name 
              size
              url
            }
          }
        }
      }
      ... on PartialPayment {
        id 
        value
        payment {
          id
          paymentType
          period
          concept
          state
          dueDate
          amount
          contract {
            id
            state
            contractType
            documents {
              id
              name
              size
              url
            }
            property {
              id
              propertyType
              state
              description
              price
              images {
                id
                name 
                size
                url
              }
            }
          }
          cash {
            id
            name
            balance
          }
          referral {
            ... on User {
              id
              email
              name
            }
            ... on Agency {
              id
              name
            }
          }
        }
      }
      ... on Property {
        id
        propertyType
        state
        description
        price
        images {
          id
          name 
          size
          url
        }
      }
      ... on Tax {
        id
        name
        state
        interval
        lastPaymentDate
        debtor {
          ... on User {
            id
            email
            name
          }
          ... on Agency {
            id
            name
          }
        }
      }
      ... on User {
        id
        email
        name
      }
    }
  }
}
```

**Get All Simple Datas w/some filter**

```graphql
query simpleDatas {
  simpleDatas(filters: { name: "mt2" } ) {
    id
    name
    value
    category
    datable {
      ... on Address {
        street
        number
        city
        state
        unit
        addressable {
          ... on User {
            id
            email
            name
          }
          ... on Agency {
            id
            name
          }
          ... on Property {
            id
            state
            propertyType
            description
          }
        }
      }
      ... on Agency {
        id
        name
      }
      ... on Cash {
        id
        name
        balance
      }
      ... on Contract {
        id
        state
        contractType
        property {
          id
          propertyType
          state
          description
        }
      }
      ... on ContractExpense {
        id
        name
        value
        contractPlan {
          id
          duration
          rentUpdatePeriod
          rentValue
          property {
            id
            propertyType
            state
            description
          }
        }
      }
      ... on ContractPlan {
        duration
        rentUpdatePeriod
        rentValue
        property {
          id
          propertyType
          state
          description
        }
      }
      ... on Payment {
        id
        paymentType
        period
        concept
        state
        dueDate
        amount
        contract {
          id
          state
          contractType
          property {
            id
            propertyType
            state
            description
          }
        }
        cash {
          id
          name
          balance
        }
        referral {
          ... on User {
            id
            email
            name
          }
          ... on Agency {
            id
            name
          }
        }
      }
      ... on PaymentPlan {
        owner
        tenant
        paymentMethods
        contract {
          id
          state
          contractType
          property {
            id
            propertyType
            state
            description
          }
        }
      }
      ... on PartialPayment {
        id 
        value
        payment {
          id
          paymentType
          period
          concept
          state
          dueDate
          amount
          contract {
            id
            state
            contractType
            property {
              id
              propertyType
              state
              description
            }
          }
          cash {
            id
            name
            balance
          }
          referral {
            ... on User {
              id
              email
              name
            }
            ... on Agency {
              id
              name
            }
          }
        }
      }
      ... on Property {
        id
        propertyType
        state
        description
      }
      ... on Tax {
        id
        name
        state
        interval
        lastPaymentDate
        debtor {
          ... on User {
            id
            email
            name
          }
          ... on Agency {
            id
            name
          }
        }
      }
      ... on User {
        id
        email
        name
      }
    }
  }
}
```

## Tax Queries

**Get All Taxes with Polymorphic Debtor**  

```graphql 
query taxes {
  taxes {
    id
    name 
    state
    interval
    lastPaymentDate
    debtor {
      ... on User {
        id
        email
        name
      }
      ... on Agency {
        id
        name
      }
    }
  }
}
```

**Get Tax By ID with Polymorphic Debtor**  

```graphql 
query tax {
  tax(id: 1) {
    id
    name 
    state
    interval
    lastPaymentDate
    debtor {
      ... on User {
        id
        email
        name
      }
      ... on Agency {
        id
        name
      }
    }
  }
}
```

**Get All Taxes w/some filter**  

```graphql
query taxes {
  taxes(filters: { name: "Water" } ) {
    id
    name
    state
    interval
    lastPaymentDate
    debtor {
      ... on User {
        id
        email
        name
      }
      ... on Agency {
        id
        name
      }
    }
  }
}
```

## User Queries

**Get All Users**  

```graphql 
query users {
  users {
    id
    name
    email
    address {
      id
      street
      number
      unit
      city
      state
      country
      neighborhood
    }
    userAgencies {
      id
      selected
    }
    agencies {
      id
      name
    }
    activeContracts {
      id
      state
      contractType
      documents {
        id
        name
        size
        url
      }
      property {
        id
        propertyType
        state
        description
        price
        images {
          id
          name 
          size
          url
        }
      }
    }
    properties {
      id
      propertyType
      state
      description
      price
      images {
        id
        name 
        size
        url
      }
    }
    permissions
  }
}
```

**Get All Users w/some filter**
 
```graphql
query users {
  users(filters: { name: "Jack" }) {
    id
    name
    email
    address {
      id
      street
      number
      unit
      city
      state
      country
      neighborhood
    }
    userAgencies {
      id
      selected
    }
    agencies {
      id
      name
    }
    activeContracts {
      id
      state
      contractType
      documents {
        id
        name
        size
        url
      }
      property {
        id
        propertyType
        state
        description
        price
        images {
          id
          name 
          size
          url
        }
      }
    }
    properties {
      id
      propertyType
      state
      description
      price
      images {
        id
        name 
        size
        url
      }
    }
    permissions
  }
}
```

**Get User By ID**  

```graphql 
query user {
  user(id: 2) {
    name
    email
    address {
      id
      street
      number
      unit
      city
      state
      country
      neighborhood
    }
    userAgencies {
      id
      selected
    }
    agencies {
      id
      name
    }
    activeContracts {
      id
      state
      contractType
      documents {
        id
        name
        size
        url
      }
      property {
        id
        propertyType
        state
        description
        price
        images {
          id
          name 
          size
          url
        }
      }
    }
    properties {
      id
      propertyType
      state
      description
      price
      images {
        id
        name 
        size
        url
      }
    }
    permissions
  }
}
```

## User Agency Queries

**Get All User Agencies**  
```graphql 
query userAgencies {
  userAgencies {
    id 
    selected
    user {
      id
      email
      name
    }
    agency {
      id
      name
    }
  }
}
```

**Get User Agency By ID**  

```graphql 
query userAgency {
  userAgency(id: 1) {
    selected
    user {
      id
      email
      name
    }
    agency {
      id
      name
    }
  }
}
```

**Get All User Agencies w/some filters**

```graphql
query userAgencies {
  userAgencies(filters: { selected: true }) {
  	id
  	selected
		agency {
      id
      name
    }
    user {
      id
      name 
      email
    }
  }
}
```

# Example Mutations

## Address Mutations

**Create Address with Polymorphic Addressable**  

```graphql 
mutation createAddress {
  createAddress(input: { 
    attributes: { 
      street: "Occasion Avenue", 
      number: "666",
      unit: "3",
      city: "New Jersey",
      state: "New Jersey",
      country: "USA",
      neighborhood: "Hells kitchen"
    }, 
    addressableId: 1, 
    addressableType: "Property" 
  }) {
    address {
      id
      street
    number
      unit
      city
      state
      country
      neighborhood
      addressable {
        ... on User {
          id
          email
          name
        }
        ... on Agency {
          id
          name
        }
        ... on Property {
          id
          state
          propertyType
          description
          price
          images {
            id
            name 
            size
            url
          }
        }
      }
    }
  }
}
```

**Delete Address**

```graphql 
mutation deleteAddress {
  deleteAddress(input: { addressId: 24 }) {
    success
	}
}
```

**Edit Address**

```graphql 
mutation editAddress {
  editAddress(input: {attributes: { street: "Occasion Avenue", number: "666", city: "New Jersey", state: "New Jersey", country: "USA", unit: "3" }, addressId: 1 }) {
    address {
      street
      number
      city
      state
      unit
      addressable {
        ... on User {
          id
          email
          name
        }
        ... on Agency {
          id
          name
        }
        ... on Property {
          id
          state
          propertyType
          description
        }
      }
    }
  }
}
```

## Agency Mutations

**Create Agency**  

```graphql 
mutation createAgency {
  createAgency(input: { name: "GenDev" }) {
    agency {
      id
      name
    }
  }
}
```

**Edit Agency**

```graphql 
mutation deleteAgency {
  deleteAgency(input: {agencyId: 2 }) {
    success
  }
}
```

**Edit Agency**

```graphql 
mutation editAgency {
  editAgency(input: { attributes: { name: "GenDev"}, agencyId: 1 }) {
    agency {
      id
      name
    }
  }
}
```

## Cash Mutations

**Create Cash**  

```graphql 
mutation createCash {
  createCash(input: { attributes: { name: "GenDev", balance: 3000 }, agencyId: 1 }) {
    cash {
      id
      name
      balance
      agency {
        id 
        name
      }
    }
  }
}
```

**Delete Cash**

```graphql
mutation deleteCash {
  deleteCash(input: {cashId: 3 }) {
    success
  }
}
```

**Edit Cash**

```graphql
mutation editCash {
  editCash(input: { attributes: { name: "GenDev", balance: 3000 }, cashId: 1 }) {
    cash {
      id
      name
      balance
      agency {
        id 
        name
      }
    }
  }
}
```

## Contract Mutations 

**Create Contract**

```graphql 
mutation createContract {
  createContract(input: { attributes: { state: "ongoing", contractType: "Renting" }, propertyId: 1 }) {
    contract {
      id
      state
      contractType
      documents {
        id
        name
        size
        url
      }
      property {
        id
        propertyType
        state
        description
        price
        images {
          id
          name 
          size
          url
        }
      }
    }
  }
}
```

**Delete Contract**

```graphql 
mutation deleteContract {
  deleteContract(input: {contractId: 3 }) {
    success
  }
}
```

**Edit Contract**

```graphql 
mutation editContract {
  editContract(input: { attributes: { state: "finished", contractType: "Renting" }, contractId: 2} ) {
    contract {
      id
      state
      contractType
      property {
        id
        propertyType
        state
        description
      }
    }
  }
}
```

## Contract Expense Mutations 

**Create Contract Expense**

```graphql 
mutation createContractExpense {
  createContractExpense(input: { attributes: { name: "Sellado", value: 1.0 }, contractPlanId: 1 }) {
    contractExpense {
      id
      name 
      value
      contractPlan {
        id
        duration
        rentUpdatePeriod
        rentValue
        property {
          id
          propertyType
          state
          description
          price
          images {
            id
            name 
            size
            url
          }
        }
      }  
    }
  }
}
```

**Delete Contract Expense**

```graphql
mutation deleteContractExpense {
  deleteContractExpense(input: { contractExpenseId: 3 }) {
    success
  }
}
```

**Edit Contract Expense**

```graphql
mutation editContractExpense {
  editContractExpense(input: { attributes: { name: "Sellado", value: 5 }, contractExpenseId: 2} ) {
    contractExpense {
      id
      name
      value
      contractPlan {
        id
        duration
        rentUpdatePeriod
        rentValue
        property {
          id
          propertyType
          state
          description
        }
      }
    }
  }
}
```

## Contract Plan Mutations 

**Create Contract Plan**

```graphql 
mutation createContractPlan {
  createContractPlan(input: {attributes: {duration: 12, rentUpdatePeriod: 6, rentValue: { first_period: 1000, second_period: 2000 }}, propertyId: 1, contractId: 1}) {
    contractPlan {
      duration
      rentUpdatePeriod
      rentValue
      property {
        id
        propertyType
        state
        description
        price
        images {
          id
          name 
          size
          url
        }
      }
    }
  }
}
```

**Delete Contract Plan**

```graphql 
mutation deleteContractPlan {
  deleteContractPlan(input: { contractPlanId: 4 }) {
    success
  }
}
```

**Edit Contract Plan**

```graphql 
mutation editContractPlan {
  editContractPlan(input: { attributes: {duration: 12, rentUpdatePeriod: 6, rentValue: { first_period: 1000, second_period: 2000 } }, contractPlanId: 2} ) {
     contractPlan {
      duration
      rentUpdatePeriod
      rentValue
      property {
        id
        propertyType
        state
        description
      }
    }
  }
}
```

## Payment Mutations 

**Create Payment with Polymorphic Referral**

```graphql
mutation createPayment {
  createPayment(input: {
    attributes: { 
      paymentType: "Check", 
      period: "1/12", 
      concept:"Water", 
      state: "pending",
      amount: 2300.30,
      dueDate: "10/11/2020",
      observations: "Lorem ipsum"
    }, 
    cashId: 1, 
    contractId: 1, 
    referralType: "User", 
    referralId: 1
  }) {
    payment {
      id
      paymentType
      period
      concept
      state
      dueDate
      amount
      contract {
        id
        state
        contractType
        documents {
          id
          name
          size
          url
        }
        property {
          id
          propertyType
          state
          description
          price
          images {
            id
            name 
            size
            url
          }
        }
      }
      cash {
        id
        name
        balance
      }
      referral {
        ... on User {
          id
          email
          name
        }
        ... on Agency {
          id
          name
        }
      }
    }
  }
}
```

**Delete Payment** 

```graphql
mutation deletePayment {
  deletePayment(input: { paymentId: 6 }) {
    success
  }
}
```

**Edit Payment**

```graphql
mutation editPayment {
  editPayment(input: {
    attributes: {
      paymentType: "Check", 
      period: "1/12", 
      concept: "Water", 
      state: "pending", 
      amount: 2300.30, 
      dueDate: "10/11/2020", 
      observations: "Lorem ipsum"
    }, 
    paymentId: 5
  }) {
    payment {
      id
      paymentType
      period
      concept
      state
      dueDate
      amount
      contract {
        id
        state
        contractType
        property {
          id
          propertyType
          state
          description
        }
      }
      cash {
        id
        name
        balance
      }
      referral {
        ... on User {
          id
          email
          name
        }
        ... on Agency {
          id
          name
        }
      }
    }
  }
}
```

# Payment Plan Mutations 

**Create Payment Plan**

```graphql
mutation createPaymentPlan {
  createPaymentPlan(input: {attributes: {owner: "Somebody", tenant: "Another", paymentMethods: ["Check", "Cash"]}, contractId: 1 }) {
    paymentPlan {
      id
      owner
      tenant
      paymentMethods
      contract {
        id
        state
        contractType
        documents {
          id
          name
          size
          url
        }
        property {
          id
          propertyType
          state
          description
          price
          images {
            id
            name 
            size
            url
          }
        }
      }
    }
  }
}
```

**Delete Payment Plan**

```graphql
mutation deletePaymentPlan {
  deletePaymentPlan(input: { paymentPlanId: 6 }) {
    success
  }
}
```

**Edit Payment Plan**

```graphql
mutation editPaymentPlan {
  editPaymentPlan(input: {
    attributes: {
      owner: "ASd", 
      tenant: "Qwe", 
      paymentMethods: ["Cash", "Check"]
    }, 
    paymentPlanId: 5
  }) {
    paymentPlan {
      id
      owner
      tenant
      paymentMethods
      contract {
        id
        state
        contractType
        property {
          id
          propertyType
          state
          description
        }
      }
    }
  }
}
```

## Partial Payment Mutations 

**Create Partial Payment**

```graphql
mutation createPartialPayment {
  createPartialPayment(input: { value: 200, paymentId: 1 }) {
    partialPayment {
      value
      payment {
        id
        paymentType
        period
        concept
        state
        dueDate
        amount
        contract {
          id
          state
          contractType
          documents {
            id
            name
            size
            url
          }
          property {
            id
            propertyType
            state
            description
            price
            images {
              id
              name 
              size
              url
            }
          }
        }
        cash {
          id
          name
          balance
        }
        referral {
          ... on User {
            id
            email
            name
          }
          ... on Agency {
            id
            name
          }
        }
      }
    }
  }
}
```

**Delete Partial Payment**

```graphql
mutation deletePartialPayment {
  deletePartialPayment(input: { partialPaymentId: 1 }) {
    success
  }
}
```

**Edit Partial Payment**

```graphql
mutation editPartialPayment {
  editPartialPayment(input: { attributes: { value: 89 }, partialPaymentId: 2  }) {
    partialPayment {
      value
      payment {
        id
        paymentType
        period
        concept
        state
        dueDate
        amount
        contract {
          id
          state
          contractType
          property {
            id
            propertyType
            state
            description
          }
        }
        cash {
          id
          name
          balance
        }
        referral {
          ... on User {
            id
            email
            name
          }
          ... on Agency {
            id
            name
          }
        }
      }
    }
  }
}
```

## Property Mutations 

**Create Property**

```graphql
mutation createProperty {
  createProperty(input: { attributes: { propertyType: "Apartment", state: "Rented", description: "Lorem ipsum", features: { operationType:{ rent: true } }, addressAttributes: { street: "asd", number: "213", unit: "s", city:"Ros", state:"asd", country: "ARG" } }, ownerId: 1 }) {
    property { 
      propertyType
    	state
    	description
    }
  }
}
```

**Delete Property**

```graphql
mutation deleteProperty {
  deleteProperty(input: { propertyId: 4 }) {
    success
  }
}
```

**Edit Property**

```graphql
mutation editProperty {
  editProperty(input: { attributes: { propertyType: "Apartment", state: "Rented", description: "Lorem ipsum", features: { operationType: { rent: true } }, addressAttributes: { id: 26, street: "addsd", number: "213", unit: "s", city:"Ros", state:"asd", country: "ARG"} }, propertyId: 5 }) {
     property { 
      propertyType
    	state
    	description
    }
  }
}
```

# Simple Data Mutations 

**Create Simple Data with Polymorphic Datable**

```graphql
mutation createSimpleData {
  createSimpleData(input: {attributes: {name: "mt2", value: "66", category: "Studio Apartment"}, datableId: 1, datableType: "Property"}) {
    simpleData{
      id
      name
      value 
      category
      datable {
        ... on Address {
          id
          street
          number
          unit
          city
          state
          country
          neighborhood
          addressable {
            ... on User {
              id
              email
              name
            }
            ... on Agency {
              id
              name
            }
            ... on Property {
              id
              state
              propertyType
              description
              price
              images {
                id
                name 
                size
                url
              }
            }
          }
        }
        ... on Agency {
          id
          name
        }
        ... on Cash {
          id
          name
          balance
        }
        ... on Contract {
          id
          state
          contractType
          documents {
            id
            name
            size
            url
          }
          property {
            id
            propertyType
            state
            description
            price
            images {
              id
              name 
              size
              url
            }
          }
        }
        ... on ContractExpense {
          id
          name
          value
          contractPlan {
            id
            duration
            rentUpdatePeriod
            rentValue
            property {
              id
              propertyType
              state
              description
              price
              images {
                id
                name 
                size
                url
              }
            }
          }
        }
        ... on ContractPlan {
          duration
          rentUpdatePeriod
          rentValue
          property {
            id
            propertyType
            state
            description
            price
            images {
              id
              name 
              size
              url
            }
          }
        }
        ... on Payment {
          id
          paymentType
          period
          concept
          state
          dueDate
          amount
          contract {
            id
            state
            contractType
            documents {
              id
              name
              size
              url
            }
            property {
              id
              propertyType
              state
              description
              price
              images {
                id
                name 
                size
                url
              }
            }
          }
          cash {
            id
            name
            balance
          }
          referral {
            ... on User {
              id
              email
              name
            }
            ... on Agency {
              id
              name
            }
          }
        }
        ... on PaymentPlan {
          owner
          tenant
          paymentMethods
          contract {
            id
            state
            contractType
            documents {
              id
              name
              size
              url
            }
            property {
              id
              propertyType
              state
              description
              price
              images {
                id
                name 
                size
                url
              }
            }
          }
        }
        ... on PartialPayment {
          id 
          value
          payment {
            id
            paymentType
            period
            concept
            state
            dueDate
            amount
            contract {
              id
              state
              contractType
              documents {
                id
                name
                size
                url
              }
              property {
                id
                propertyType
                state
                description
                price
                images {
                  id
                  name 
                  size
                  url
                }
              }
            }
            cash {
              id
              name
              balance
            }
            referral {
              ... on User {
                id
                email
                name
              }
              ... on Agency {
                id
                name
              }
            }
          }
        }
        ... on Property {
          id
          propertyType
          state
          description
          price
          images {
            id
            name 
            size
            url
          }
        }
        ... on Tax {
          id
          name
          state
          interval
          lastPaymentDate
          debtor {
            ... on User {
              id
              email
              name
            }
            ... on Agency {
              id
              name
            }
          }
        }
        ... on User {
          id
          email
          name
        }
      }
    }
  }
}   
```

**Delete Simple Data**

```graphql
mutation deleteSimpleData {
  deleteSimpleData(input: { simpleDataId: 1 }) {
    success
  }
}
```

**Edit Simple Data**

```graphql
mutation editSimpleData {
  editSimpleData(input: { attributes: { name: "Apartment", value: "Rented", category: "Lorem ipsum" }, simpleDataId: 3 }) {
     simpleData { 
      name
    	value
    	category
      datable {
        ... on Address {
          street
          number
          city
          state
          unit
          addressable {
            ... on User {
              id
              email
              name
            }
            ... on Agency {
              id
              name
            }
            ... on Property {
              id
              state
              propertyType
              description
            }
          }
        }
        ... on Agency {
          id
          name
        }
        ... on Cash {
          id
          name
          balance
        }
        ... on Contract {
          id
          state
          contractType
          property {
            id
            propertyType
            state
            description
          }
        }
        ... on ContractExpense {
          id
          name
          value
          contractPlan {
            id
            duration
            rentUpdatePeriod
            rentValue
            property {
              id
              propertyType
              state
              description
            }
          }
        }
        ... on ContractPlan {
          duration
          rentUpdatePeriod
          rentValue
          property {
            id
            propertyType
            state
            description
          }
        }
        ... on Payment {
          id
          paymentType
          period
          concept
          state
          dueDate
          amount
          contract {
            id
            state
            contractType
            property {
              id
              propertyType
              state
              description
            }
          }
          cash {
            id
            name
            balance
          }
          referral {
            ... on User {
              id
              email
              name
            }
            ... on Agency {
              id
              name
            }
          }
        }
        ... on PaymentPlan {
          owner
          tenant
          paymentMethods
          contract {
            id
            state
            contractType
            property {
              id
              propertyType
              state
              description
            }
          }
        }
        ... on PartialPayment {
          id 
          value
          payment {
            id
            paymentType
            period
            concept
            state
            dueDate
            amount
            contract {
              id
              state
              contractType
              property {
                id
                propertyType
                state
                description
              }
            }
            cash {
              id
              name
              balance
            }
            referral {
              ... on User {
                id
                email
                name
              }
              ... on Agency {
                id
                name
              }
            }
          }
        }
        ... on Property {
          id
          propertyType
          state
          description
        }
        ... on Tax {
          id
          name
          state
          interval
          lastPaymentDate
          debtor {
            ... on User {
              id
              email
              name
            }
            ... on Agency {
              id
              name
            }
          }
        }
        ... on User {
          id
          email
          name
        }
      }
    }
  }
}
```

## Tax Mutations 

**Create Tax with Polymorphic Debtor**

```graphql
mutation createTax {
  createTax(input: { attributes: { name: "Water", interval: 2, state: "pending" }, contractId: 1, debtorType: "User", debtorId: 1 }) {
    tax { 
      id
      name 
      state
      interval
      lastPaymentDate
      debtor {
        ... on User {
          id
          email
          name
        }
        ... on Agency {
          id
          name
        }
      }
    }
  }
}
```

**Delete Tax**

```graphql
mutation deleteTax {
  deleteTax(input: { taxId: 1 }) {
    success
  }
}
```

**Edit Tax**

```graphql
mutation editTax {
  editTax(input: { attributes: {name: "Waterr", interval: 2, state: "pending"}, taxId: 2 }) {
     tax {
      id
      name
      state
      interval
      lastPaymentDate
      debtor {
        ... on User {
          id
          email
          name
        }
        ... on Agency {
          id
          name
        }
      }
    }
  }
}
```

## User Mutations

**Create User**  

```graphql 
mutation createUser {
  createUser(input: {name: "Example User", credentials: { email: "mail@example.com", password: "[ommited]"} }) {
    user {
      id
      name
      email
    }
  }
}
```

**Delete User**

```graphql
mutation deleteUser {
  deleteUser(input: { userId: 2 }) {
    success
  }
}
```

**Edit User**

```graphql
mutation editUser {
  editUser(input: { attributes: { name: "Le pepiu", email: "pepito@2gmail.com" }, userId: 2 }) {
     user {
      id
      name
      email
    }
  }
}
```

**SignIn User**  

```graphql
mutation signInUser {
  signIn(input: {credentials: {email: "martinmb96@gmail.com", password: "qwewqewq"}}) {
    token
    user {
      id
      name
    }
  }
}
```

**SignOut User**  

```graphql
mutation signOut {
  signOut(input: {}) {
    success
  }
}
```

## User Agency Mutations

**Create UserAgency**  

```graphql
mutation createUserAgency {
  createUserAgency(input: { selected: true, agencyId: 1, userId: 1 }) {
    userAgency {
      id
      selected 
      agency {
        id
        name 
      }
      user {
        id 
        name
        email
      }
    }
  }
}
```

**Delete User Agency**

```graphql
mutation deleteUserAgency {
  deleteUserAgency(input: { userAgencyId: 6 }) {
    success
  }
}
```

**Edit User Agency**

```graphql
mutation editUserAgency {
  editUserAgency(input: { attributes: { selected: true }, userAgencyId: 2 }) {
     userAgency {
      selected
      agency {
        id
        name
      }
      user {
        id
        name
        email
      }
    }
  }
}
```