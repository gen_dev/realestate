class Concept < ApplicationRecord
  belongs_to :sub_account

  has_many :payments, dependent: :destroy

  default_scope { order(name: :asc) }

  def balance
    return 0 if payments.blank?
    pending_receipts = Receipt.where(state: 'pending').pluck(:id)

    if sub_account.account.period.blank?
      pmts = payments.current.where('state = ? AND receipt_id IS NULL OR NOT receipt_id IN (?)', 'paid', pending_receipts)
    else
      initial = "#{sub_account.account.period.first}-01".to_date
      latest = "#{sub_account.account.period.last}-01".to_date

      pmts = payments.where(due_date: initial.beginning_of_month..latest.end_of_month).or(payments.where(paid_at: initial.beginning_of_month..latest.end_of_month))
      pmts = pmts.where('state = ? AND receipt_id IS NULL OR NOT receipt_id IN (?)', 'paid', pending_receipts)
    end

    fp = pmts.where(state: 'paid').pluck(:amount).sum
    lp = pmts.where(state: 'pending').joins(:partial_payments).pluck('partial_payments.value').sum
    fp + lp
  end

  def parsed_balance
    "#{balance.abs.to_i.to_s(:delimited, delimiter: '.')}"
  end
end
