class SearchResult < ApplicationRecord
  default_scope { order(dropped: :asc) }

  belongs_to :search
  belongs_to :property
  belongs_to :user, optional: true

  has_one :agency, through: :search
end
