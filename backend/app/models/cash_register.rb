class CashRegister < ApplicationRecord
  belongs_to :agency

  def payments
    sa = ::SubAccount.where(agency_id: agency&.id)
    payments = ::Payment.where(sub_account_id: sa.pluck(:id)).where(paid_at: self.created_at.beginning_of_day..self.created_at.end_of_day)

    payments = payments.includes(:partial_payments, :chargebacks, :price, sub_account: :account, contract: [property: :address])

    payments = payments.distinct

    payments
  end
end
