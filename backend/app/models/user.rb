class User < ApplicationRecord
  rolify
  has_secure_password

  include Userable

  has_many :agencies, through: :user_agencies
  has_many :guarantees, class_name: 'Contracts::Guarantee', foreign_key: :guarantor_id
  has_many :payments, as: :referral
  has_many :fixes, class_name: 'Properties::Fix', as: :payer
  has_many :taxes, as: :taxable
  has_many :receipts, -> { order(id: :desc) }, dependent: :destroy

  has_many :inquiries
  has_many :comments, class_name: "Inquiries::Comment"

  has_many :searches, dependent: :destroy
  has_many :search_results, dependent: :destroy

  has_one :address, as: :addressable, dependent: :destroy
  has_one :profile, class_name: 'Users::Profile'
  has_one :staff

  belongs_to :organization

  belongs_to :advisor, class_name: "Staff", optional: true

  validates :email, presence: true, uniqueness: true

  delegate :can?, :cannot?, to: :ability

  accepts_nested_attributes_for :profile

  before_create :create_organization

  def has_debts
    payments = Payment.
      includes(children: :chargebacks).
      where('(receiver_type = ? AND receiver_id = ?) OR (referral_type = ? AND referral_id = ?)', "User", self.id, "User", self.id).
      where('state = ? AND due_date < ?', 'pending', Date.today.beginning_of_month)

    payments.any?
  end

  def owed_money
    owed = {
      "ARS" => 0,
      "USD" => 0,
    }

    payments = Payment.includes(children: :chargebacks).where(receiver: self).where('state = ? AND due_date < ?', 'pending', Date.today)

    payments.each do |payment|
      owed[payment.currency] += payment.lasting_amount
    end

    owed
  end

  def owed_money_by_us
    owed = {
      "ARS" => 0,
      "USD" => 0,
    }

    payments = Payment.includes(:chargebacks).where(referral: self).where('state = ? AND due_date < ?', 'pending', Date.today)

    payments.each do |payment|
      owed[payment.currency] += payment.lasting_amount
    end

    owed
  end

  def active_contracts
    # List ongoing contracts
    Contract.where(state: 'ongoing').with_roles([:tenant, :owner], self)
  end

  def active_owned_contracts
    Contract.where(state: 'ongoing').with_roles([:owner], self)
  end

  def owned_contracts
    Contract.with_roles([:owner], self)
  end

  def active_rented_contract
    Contract.where(state: 'ongoing').with_roles([:tenant], self)
  end

  def owned_payments
    ids = active_owned_contracts.pluck(:id)
    Payment.where(concept: 'Alquiler', contract_id: ids, referral_type: 'Agency')
  end

  def display_roles
    roles.pluck(:name).uniq
  end

  def selected_agency
    # Agency where selected is true 
    selected = user_agencies.find_by(selected: true)
    return nil unless selected
    selected.agency
  end

  def properties
    # List properties from user
    Property.with_role([:owner, :tenant], self)
  end
  
  def permissions
    # A hash with the permissions a User has over a certain object. For example: { property: { can_delete: true } }
    ability.permissions
  end

  def overdue_payments_amount
    payments.current.where(state: 'pending').map(&:lasting_amount).sum
  end

  def next_rent_expiration
    # Show the closest payment by due date.
  end

  def up_to_date
    # Returns a hash with payments and its state. For example: { rent: true }
  end

  private

    def ability
      @ability ||= Ability.new(self)
    end

    def create_organization
      self.organization = Organization.create
    end
end
