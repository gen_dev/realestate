class Building < ApplicationRecord
  belongs_to :agency
  
  has_many :properties
  has_many :taxes, as: :taxable
  has_one :address, as: :addressable, dependent: :destroy

  accepts_nested_attributes_for :address
end
