class Contract < ApplicationRecord
  after_update :update_taxes
  after_update :set_taxes

  before_destroy :change_property_state

  resourcify
  belongs_to :property
  
  has_one :contract_plan, dependent: :destroy
  has_one :payment_plan, dependent: :destroy
  has_one :rescission
  
  has_many :taxes, as: :taxable, dependent: :destroy
  has_many :payments, dependent: :destroy
  
  validates :contract_type, presence: true
  validates :state, presence: true # Ongoing / Draft / Finalizado
  validate :check_existing_contract

  has_many :documents, class_name: 'Document', foreign_key: :documentable_id, as: :documentable, dependent: :destroy
  has_many :guarantees, class_name: 'Contracts::Guarantee'
  has_many :guarantors, through: :guarantees

  has_many :tickets, dependent: :destroy

  accepts_nested_attributes_for :guarantees, allow_destroy: true
  accepts_nested_attributes_for :contract_plan
  accepts_nested_attributes_for :payment_plan

  def full_address
    address = property.address
    return "#{address.street} #{address.number}#{(' ' + address.unit) if address.unit}, #{address.city}, #{address.state}"
  end

  def owner
    User.preload(:roles).with_role(:owner, self)
  end

  def tenant
    User.preload(:roles).with_role(:tenant, self)
  end

  def duration
    return 0 unless payment_plan
    rent = payment_plan.taxes.find_by(name: 'Alquiler')
    return 0 unless rent
    return 0 unless rent.ends_at
    (rent.ends_at.year * 12 + rent.ends_at.month) - (Time.now.year * 12 + Time.now.month)
  end

  def rent
    return nil unless payment_plan
    payment_plan.taxes.find_by(name: 'Alquiler')
  end

  def total_rent_price
    return 0 unless rent
    rent.prices.pluck(:amount).sum
  end

  def current_payments
    payments.current
  end

  def current_taxes
    ids = current_payments.pluck(:tax_id)

    ::Tax.where(id: ids).order(:name)
  end

  def overdue_payments
    # Return payments where state: pending and due_date < Date.today
    payments.where('state = ? AND due_date < ?', 'pending', Date.today)
  end

  def owner_status
    cpayments = current_payments.where('referral_type = ?', 'Agency')
    paid = cpayments.where(state: 'paid').map(&:lasting_amount).sum
    total = cpayments.map(&:lasting_amount).sum
    (paid.abs - total.abs).abs
  end

  def tenant_status
    return nil unless tenant.last
    tenant_id = tenant.last.id
    cpayments = current_payments.where('referral_type = ? AND referral_id = ?', 'User', tenant_id)
    paid = cpayments.where(state: 'paid').map(&:lasting_amount).sum
    total = cpayments.map(&:lasting_amount).sum
    (paid.abs - total.abs).abs
  end

  def change_property_state
    property.update state: 'published'
  end

  private

    def update_taxes
      return unless state == 'ongoing'
      return nil unless payment_plan
      taxes.all.map(&:create_payment)
      payment_plan.taxes.all.map(&:create_payment)
    end

    def set_taxes
      return unless state == 'ongoing'
      return unless self.tenant.any? && self.taxes.empty?
      return if property.taxes.empty?
      existing_taxes = property.taxes

      existing_taxes.each do |tax|
        ntax = tax.deep_dup
        ntax.starts_at = self.starts_at
        ntax.ends_at = self.ends_at

        tax.prices.each do |price|
          nprice = price.deep_dup
          if price.referral_id.nil?
            nprice.referral_id = self.tenant.last.id
          end
          ntax.prices.push(nprice)
        end

        self.taxes.push(ntax)
      end

      save!
    end

    def check_existing_contract
      contract = Contract.find_by("contracts.property_id = ? AND contracts.state = ? AND NOT contracts.id = ?", self.property_id, "ongoing", self.id)

      errors.add(:property_id, "should only be assign to one active contract") if contract
    end
end
