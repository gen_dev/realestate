class Document < ApplicationRecord
  include Fileable
  belongs_to :documentable, polymorphic: true
  mount_uploader :document, DocumentUploader

  def url 
    document.url
  end
end
