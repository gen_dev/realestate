class PartialPayment < ApplicationRecord
  belongs_to :payment
  
  validates :value, presence: true
end
