class Staff < ApplicationRecord
  mount_uploader :avatar, LogoUploader

  belongs_to :organization
  belongs_to :user

  has_many :inquiries, through: :user
  has_many :comments, through: :user

  has_many :properties_staff, dependent: :destroy
  has_many :properties, through: :properties_staff

  has_many :clients, class_name: "User", foreign_key: "advisor_id"
  has_many :searches, through: :clients
end
