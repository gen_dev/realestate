module Fileable
  extend ActiveSupport::Concern

  included do
    validates :name, presence: true
    validates :size, presence: true
  end
end
