module Userable
  extend ActiveSupport::Concern

  included do
    has_many :user_agencies
  end
end
