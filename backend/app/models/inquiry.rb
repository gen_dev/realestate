class Inquiry < ApplicationRecord
  belongs_to :property, optional: true
  belongs_to :user, optional: true
  belongs_to :contact, class_name: 'User', optional: true

  has_many :comments, class_name: 'Inquiries::Comment'
  has_one :search

  before_create :link_with_contact
  before_update :link_with_contact

  def link_with_contact
    return if self.contact_id
    user = Users::Profile.find_by("TRIM(UNACCENT(UPPER(name))) = UNACCENT(?) OR email = ?", self.name.upcase.strip, (self.email || "").downcase)

    return unless user

    self.contact = user.user
  end
end
