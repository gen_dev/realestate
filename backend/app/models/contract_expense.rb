class ContractExpense < ApplicationRecord
  belongs_to :contract_plan
  validates :name, presence: true
  validates :value, presence: true
end
