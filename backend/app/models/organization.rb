class Organization < ApplicationRecord
  has_many :agencies
  has_many :users

  has_many :staff
end
