class Ability
  include CanCan::Ability

  def initialize(user)
    @user = user
    if @user.blank?
      self.public
    else
      self.private
    end
  end

  def public
    # There arent public permissions yet (might be in future versions)
  end

  def private
    case 
    when @user.has_role?(:gendev)
      can :manage, :all
    when @user.has_role?(:admin)
      can :manage, :all
    when @user.has_role?(:owner)
      apply_owner_permissions
    when @user.has_role?(:properties)
      apply_property_permissions
    when @user.has_role?(:tenant)
      apply_tenant_permissions
    end
  end
  def apply_property_permissions
    apply_address_permissions
    apply_agency_permissions
    apply_document_permissions
    apply_image_permissions
    apply_user_agency_permissions
    apply_user_permissions
  end

  def apply_admin_permissions
    apply_address_permissions
    apply_agency_permissions
    apply_cash_permissions
    apply_contract_permissions
    apply_contract_expense_permissions
    apply_contract_plan_permissions
    apply_document_permissions
    apply_image_permissions
    apply_partial_payment_permissions
    apply_payment_permissions
    apply_payment_plan_permissions
    apply_property_permissions
    apply_simple_data_permissions
    apply_tax_permissions
    apply_user_agency_permissions
    apply_user_permissions
  end

  def apply_owner_permissions
    # There arent owner permissions yet (might be in future versions)
  end
  
  def apply_tenant_permissions
    # There arent tenant permissions yet (might be in future versions)
  end

  private

  # SQL
  
  def agency_ids
    @agency_ids ||= @user.agencies.ids
  end

  def property_ids_by_agencies
    @property_ids_by_agencies ||= Property.where('agency_id IN (?)', agency_ids).ids
  end

  def contract_ids_by_agencies
    @contract_ids_by_agencies ||= Contract.joins(:property).where('properties.agency_id IN (?)', agency_ids).ids 
  end

  def user_ids_by_user_agencies
    @user_ids_by_user_agencies ||= UserAgency.where('agency_id IN (?)', agency_ids).pluck(:user_id)
  end

  # Permissions

  def apply_address_permissions
    can :manage, Address, addressable_type: 'User', addressable_id: @user.id
    can :manage, Address, addressable_type: 'User', addressable_id: user_ids_by_user_agencies
    can :manage, Address, addressable_type: 'Agency', addressable_id: agency_ids
    can :manage, Address, addressable_type: 'Property', addressable_id: property_ids_by_agencies
  end

  def apply_agency_permissions
    can :manage, Agency, id: agency_ids
  end

  def apply_cash_permissions
    can :manage, Account, agency: { id: agency_ids } 
    can :manage, SubAccount, agency: { id: agency_ids } 
  end
  
  def apply_contract_permissions
    can :manage, Contract, property: { agency: { id: agency_ids } } 
  end

  def apply_contract_expense_permissions
    can :manage, ContractExpense, contract_plan: { property: { agency: { id: agency_ids } } }
  end
  
  def apply_contract_plan_permissions
    can :manage, ContractPlan, property: { agency: { id: agency_ids } } 
  end

  def apply_document_permissions
    can :manage, Document, documentable_type: 'Contract', documentable_id: contract_ids_by_agencies 
  end

  def apply_image_permissions
    can :manage, Image, imageable_type: 'Property', imageable_id: property_ids_by_agencies  
  end

  def apply_partial_payment_permissions
    can :manage, PartialPayment, payment: { sub_account: { agency: { id: agency_ids } } }
  end

  def apply_payment_permissions
    can :manage, Payment, sub_account: { agency: { id: agency_ids } } 
  end

  def apply_payment_plan_permissions
    can :manage, PaymentPlan, contract: { property: { agency: { id: agency_ids } } }
  end

  def apply_property_permissions
    can :manage, Property, agency: { id: agency_ids }
  end

  def apply_simple_data_permissions
    can :manage, SimpleData, datable_type: 'Agency', datable_id: agency_ids
    can :manage, SimpleData, datable_type: 'Contract', datable_id: contract_ids_by_agencies 
    can :manage, SimpleData, datable_type: 'Property', datable_id: property_ids_by_agencies  
    can :manage, SimpleData, datable_type: 'User', datable_id: user_ids_by_user_agencies
  end

  def apply_tax_permissions
    can :manage, Tax, taxable_type: 'Contract', taxable_id: contract_ids_by_agencies 
  end

  def apply_user_agency_permissions
    can :manage, UserAgency, agency: { id: agency_ids } 
  end

  def apply_user_permissions
    can :manage, User, user_agencies: { agency: { id: agency_ids } }
  end
end
