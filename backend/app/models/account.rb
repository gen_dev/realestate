class Account < ApplicationRecord
  belongs_to :agency
  has_many :sub_accounts, dependent: :destroy

  def balance
    amount = sub_accounts.preload(:payments).map(&:tmp_balance).sum

    amount
  end
end
