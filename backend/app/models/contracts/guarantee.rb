class Contracts::Guarantee < ApplicationRecord
  belongs_to :guarantor, class_name: 'User'
  belongs_to :contract

  has_many :documents, class_name: 'Document', foreign_key: :documentable_id, as: :documentable, dependent: :destroy
end
