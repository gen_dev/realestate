class Image < ApplicationRecord
  include Fileable
  belongs_to :imageable, polymorphic: true
  mount_uploader :image, ImageUploader

  def url 
    image.url
  end

  def thumb_url
    image.thumb.url
  end

  def small_url
    image.small.url
  end
end
