class SubAccount < ApplicationRecord
  belongs_to :agency
  belongs_to :account, optional: true
  has_many :payments, -> { where(third_party_transfer: false).where.not(state: :deleted) }, dependent: :destroy
  has_many :concepts, dependent: :destroy 

  default_scope { order('name ASC') }

  def tmp_balance
    return 0 if payments.blank?
    pending_receipts = Receipt.where(state: 'pending').pluck(:id)
    # filter according to new variable, i.e. period
    if account.period.blank?
      pmts = payments
        .where(paid_at: Date.today.beginning_of_month..)
        .where(
          'currency = ? AND state = ? AND receipt_id IS NULL OR NOT receipt_id IN (?)',
          'ARS',
          'paid',
          pending_receipts
        )
    else
      initial = "#{account.period.first}-01".to_date
      latest = "#{account.period.last}-01".to_date

      pmts = payments.where(
        paid_at: initial.beginning_of_month..latest.end_of_month
      )

      pmts = pmts.where(
        'currency = ? AND state = ? AND receipt_id IS NULL OR NOT receipt_id IN (?)',
        "ARS",
        'paid',
        pending_receipts
      )
    end

    fp = pmts.where(state: 'paid').pluck(:amount).sum
    lp = pmts.where(state: 'pending').joins(:partial_payments).pluck('partial_payments.value').sum
    fp + lp
  end

  def parsed_balance
    "#{tmp_balance.abs.to_i.to_s(:delimited, delimiter: '.')}"
  end

  def update_balance(amount, action)
    # update balance where action can be :+ or :-
    update!(balance: balance.send(action, amount))
  end
end
