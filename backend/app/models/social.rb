class Social < ApplicationRecord
  mount_uploader :image, LogoUploader

  belongs_to :agency
end
