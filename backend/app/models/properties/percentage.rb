class Properties::Percentage < ApplicationRecord
  belongs_to :property
  belongs_to :tax
end
