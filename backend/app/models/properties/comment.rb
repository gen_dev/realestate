class Properties::Comment < ApplicationRecord
  belongs_to :observation, class_name: "Properties::Observation"
  belongs_to :user
end
