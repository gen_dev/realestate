class Properties::Observation < ApplicationRecord
  belongs_to :property
  belongs_to :user

  has_many :comments, class_name: "Properties::Comment"
end
