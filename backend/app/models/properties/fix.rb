class Properties::Fix < ApplicationRecord
  after_create :update_ticket

  belongs_to :property
  belongs_to :contract, optional: true
  belongs_to :ticket, optional: true
  belongs_to :payer, polymorphic: true

  has_many :payments, -> { order(id: :desc) }, as: :payable, dependent: :destroy

  accepts_nested_attributes_for :payments

  def sub_account_id
    property.agency.sub_accounts.find_by(name: 'Reparaciones').id
  end

  private
    def update_ticket
      return unless ticket
      ticket.update(state: 'watching')
    end
end
