class Rescission < ApplicationRecord
  belongs_to :contract
  has_one :receipt

  accepts_nested_attributes_for :receipt

  def create_payments
    rent = self.rent.to_f - self.bonification.to_f

    debtor_id = self.contract.tenant[0].id
    debtor_type = self.contract.tenant[0].class.to_s

    tax = self.contract.rent

    (self.quota.to_i || 1).times do |date|
      amount = rent / (self.quota.to_i || 1)

      due_date = date.months.ago.end_of_month

      Payment.create({
        tax: tax,
        contract: contract,
        referral: contract.property.agency, 
        receiver: contract.property.owner[0], 
        amount: amount * -1,
        sub_account: sub_account(contract.property.agency)["Alquiler"],
        concept: 'Resolución anticipada',
        state: 'pending',
        due_date: due_date,
      })

      Payment.create({
        tax: tax,
        contract: contract,
        referral: contract.property.owner[0], 
        receiver: contract.property.agency, 
        amount: amount * (tax.commission / 100),
        sub_account: sub_account(contract.property.agency)["Comision"],
        concept: 'Comisión',
        state: 'pending',
        due_date: due_date,
      })

      rented = Payment.create({
        tax: tax,
        contract: contract, 
        referral_id: debtor_id, 
        referral_type: debtor_type,
        receiver: contract.property.agency, 
        amount: amount,
        sub_account: sub_account(contract.property.agency)['Alquiler'],
        concept: 'Resolución anticipada',
        state: 'pending',
        due_date: due_date,
        paid_at: date == 0 ? Date.today.end_of_day : nil
      })

      return if date > 0

      self.receipt.payments.push(rented)
      self.receipt.save!
    end
  end

  private
    def sub_account(agency)
      {
        'Alquiler' => SubAccount.find_by(name: 'Alquileres', agency: agency),
        'Honorarios' => SubAccount.find_by(name: 'Honorarios alquiler inquilino', agency: agency),
        'Honorarios propietario' => SubAccount.find_by(name: 'Honorarios alquiler propietario', agency: agency),
        # 'Rendicion' => SubAccount.find_by(name: 'Alquileres pagados', agency: agency),
        'Comision' => SubAccount.find_by(name: 'Honorarios administración alquiler', agency: agency),
      }
    end
end
