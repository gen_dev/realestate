class PaymentPlan < ApplicationRecord
  belongs_to :contract
  has_many :taxes, -> { order('id ASC') }, as: :taxable, inverse_of: :taxable

  accepts_nested_attributes_for :taxes, allow_destroy: true
end
