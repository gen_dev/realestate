class Receipt < ApplicationRecord
  before_create :set_number

  has_many :payments
  has_many :payment_types

  belongs_to :agency
  belongs_to :user

  belongs_to :rescission, optional: true
  belongs_to :contract, optional: true

  accepts_nested_attributes_for :payments

  def set_payment_types
    self.payment_types = []

    self.payments.each do |payment|
      payment_type = self.payment_types.find_or_create_by(name: payment.payment_type, currency: payment.currency)

      payment_type.amount = (payment_type.amount || 0) + payment.chargeback_amount

      payment_type.save
    end
  end

  private
    def set_number
      self.number = agency.receipts.where(receipt_type: self.receipt_type).count + 1
    end
end
