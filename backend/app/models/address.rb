class Address < ApplicationRecord
  belongs_to :addressable, polymorphic: true

  #validates :street, presence: true
  #validates :number, presence: true
  #validates :city, presence: true
  #validates :state, presence: true
  #validates :country, presence: true

  before_validation :is_unique, on: :create

  private

  def is_unique
    # The full address should be unique by Property
    if self.addressable_type == 'Property' && Address.where(self.attributes.except('id','addressable_id','created_at','updated_at')).any? 
      errors.add(:address, 'already exists for that Property.')
    end
  end
end
