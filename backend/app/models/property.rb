class Property < ApplicationRecord
  resourcify

  has_many :properties_staff
  has_many :staff, through: :properties_staff
  has_many :contracts
  has_many :fixes, class_name: 'Properties::Fix'
  has_many :taxes, as: :taxable
  has_many :characteristics, class_name: 'SimpleData', foreign_key: :datable_id, as: :datable

  has_many :observations, class_name: 'Properties::Observation'

  has_one :percentage, class_name: 'Properties::Percentage', dependent: :destroy
  has_one :address, as: :addressable, dependent: :destroy
  belongs_to :agency
  belongs_to :building, optional: true

  validates :property_type, presence: true
  validates :state, presence: true

  mount_uploader :blueprint, LogoUploader

  has_many :images, 
    -> { order('lft ASC') },
    class_name: 'Image', foreign_key: :imageable_id, as: :imageable, dependent: :destroy

  has_many :search_results,
    dependent: :destroy

  has_many :searches, through: :search_results

  before_update :update_search_results, if: :disabled_changed?
  before_update :search_for_results, if: :state_changed?
  before_update :update_staff

  accepts_nested_attributes_for :address, :characteristics, :images

  def tokko_mapping
    PropertyMapper.new(self.to_json(include: [:address])).map
  end

  def blueprint_url
    blueprint.url
  end

  def seller
    staff.find_by(rol: "Ventas")
  end

  def last_result
    search_results.last
  end

  def unseen_results
    search_results.where.not('(?) = ANY (seen_by)', agency_id).count
  end

  def useful_results
    search_results.where.not(user_id: nil)
  end

  def operation_types
    self.features["operation_type"].to_a.map{|x| x[0] if x[1]}.compact
  end

  def basic_info
    { 
      bedrooms: characteristics.find_by(name: 'Dormitorios').value,
      bathrooms: characteristics.find_by(name: 'Baños').value,
      rooms: characteristics.find_by(name: 'Ambientes').value,
    }
  end

  def full_address
    return "#{address.street} #{address.number}#{(' ' + address.unit) if address.unit}, #{address.city}, #{address.state}"
  end

  def address_first_line
    return "#{address.street} #{address.number}#{(' ' + address.unit) if address.unit}"
  end

  def address_second_line
    return "#{address.city}, #{address.state}"
  end

  def active_tenant
    # Users under contract
    return nil if active_contract.blank?
    User.with_role(:tenant, active_contract).last
  end
  
  def active_contract
    # List ongoing contract
    contracts.find_by(state: 'ongoing')
  end

  def owner
    # Find the user where the role is owner for this (self) property.
    User.with_role(:owner, self)
  end

  def price
    # NOTE: This is a reference price, the final price will be Contract#total_amount
    # This method format and return price as $xxxx,xx
    price_attr = read_attribute(:price)
    price = price_attr ? price_attr : 0
    "#{currency == 'ARS' ? '$' : currency } #{price.to_i.to_s(:delimited, delimiter: '.')}"
  end

  def sell_price
    # NOTE: This is a reference price, the final price will be Contract#total_amount
    # This method format and return price as $xxxx,xx
    price_attr = read_attribute(:sell_price)
    price = price_attr ? price_attr : 0
    "#{sell_currency } #{price.to_i.to_s(:delimited, delimiter: '.')}"
  end

  def fixes_sub_account_id
    agency.sub_accounts.find_by(name: 'Reparaciones').id
  end

  def update_search_results
    if disabled
      search_results.update_all(active: false)
    else
      search_results.update_all(active: ['published', 'booked'].include?(self.state))
    end
  end

  def related_properties
    related = self.searches.joins(:search_results).group('searches.id, search_results.dropped').having('count(search_results.search_id) > 1').sample

    ids = related.results.where(active: true).pluck(:property_id)

    Property.where(id: ids).limit(3)
  end

  def search_for_results
    update_search_results

    Thread.new do |t|
      ::Search.all.map(&:search_for_results)
    end
  end

  def update_staff
    self.staff = []
    full_staff = agency.organization.staff
    aux = full_staff.find_by("? = ANY(property_types) AND operation_type IN (?) AND (city = ? OR city IS NULL)", self.property_type, self.operation_types, self.address.city)

    if aux
      self.staff.push(aux)
    else
      aux = full_staff.where("operation_type IN (?)", self.operation_types)

      if aux.any?
        full_staff = aux
      end

      aux = full_staff.where("city = ?", self.address.city)

      self.staff = aux
    end
  end
end
