class SimpleData < ApplicationRecord
  self.table_name = "simple_datas"
  belongs_to :datable, polymorphic: true
  
  validates :name, presence: true
end
