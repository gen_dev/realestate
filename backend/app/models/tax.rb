class Tax < ApplicationRecord
  belongs_to :taxable, polymorphic: true
  belongs_to :debtor, polymorphic: true, optional: true
  belongs_to :concept, optional: true

  after_create :create_payment
  after_create :duplicate_in_contract
  after_update :create_payment

  before_destroy :destroy_pending_payments

  # Price may not have ends_at date, so query will be something like:
  # prices.where('starts_at > ? AND (ends_at = null OR ends_at < ?)', Date.now, Date.now)
  # uwu
  # This will be useful for taxes like Rent, API, TGI, and so
  # which have a static value over a period
  # For cases like: electricity, water and such, this will not be needed.
  has_many :prices, -> { reorder(starts_at: :asc) }, class_name: "Price::Period", as: :priceable, dependent: :destroy
  has_many :payments, -> { current.order(id: :desc) }
  has_many :percentages, class_name: 'Properties::Percentage'

  belongs_to :parent, class_name: 'Tax', optional: true
  has_many :children, class_name: 'Tax', foreign_key: :parent_id

  # validates :name, presence: true, uniqueness: true
  # validates :state, presence: true
  # validates :interval, presence: true

  accepts_nested_attributes_for :prices, allow_destroy: true
  accepts_nested_attributes_for :payments, allow_destroy: true
  accepts_nested_attributes_for :percentages, allow_destroy: true

  def sub_account_id
    return unless concept
    concept.sub_account_id 
  end

  def account_id
    return unless concept
    concept.sub_account.account_id
  end

  def editable
    self.taxable_type != 'PaymentPlan'
  end

  def prices_attributes
    prices
  end

  def current_prices(arr = [-1, 0, 1])
    pcs = []
    contract = taxable_type == 'PaymentPlan' ? taxable&.contract : taxable
    return unless contract

    if self.name == 'Honorarios'
      prices.update_all(starts_at: nil)
    end

    arr.each do |time|
      next if time.month.from_now < contract.starts_at
      current_prices = prices.where(starts_at: time.month.from_now.beginning_of_month..time.month.from_now.end_of_month)

      unless current_prices.any?
        current_prices = prices.where('starts_at <= ? AND ends_at >= ? OR (starts_at IS NULL AND ends_at IS NULL) OR (starts_at <= ? AND ends_at IS NULL)', time.month.from_now, time.month.from_now, time.month.from_now)
      end

      next unless current_prices.any?

      current_prices.each do |price|
        period = name == 'Alquiler' ? 1 : (interval || 1)
        payable = false
        m = 0
        for x in (0..price.months) do
          m = x * period
          payable = price.starts_at ? (price.starts_at + m.month).month == time.month.from_now.month : true
          break if payable
        end

        next unless payable

        if ("#{contract.starts_at.month}_#{contract.starts_at.year}" == "#{time.month.from_now.month}_#{time.month.from_now.year}") && name == 'Alquiler'
          now = (contract.starts_at).day
          if now > 1 
            eom = contract.starts_at.end_of_month.day
            diff = eom - now
            price.amount = ((diff * price.amount) / eom)
          end
        end

        pcs << { price: price, period: m, idx: time }
      end
    end

    pcs
  end

  def create_payment(arr = [-1, 0, 1])
    # when cron is executed
    # create a new payment once the interval edge is reached
    return nil unless taxable
    return single_user_create(taxable, arr) if taxable_type == 'User'
    return building_create(arr) if taxable_type == 'Building'
    contract = taxable_type == 'PaymentPlan' ? taxable.contract : taxable
    return nil unless contract && contract.state == 'ongoing'
    return nil unless contract.property
    return nil if contract.rescission
    return nil if contract.tenant.blank?
    return single_create(contract, arr) if self.prices.empty?
    c_prices = current_prices(arr)
    return nil if c_prices.blank?

    c_prices.each do |price|
      price[:price].amount ||= 0
      if self.name == 'Honorarios'
        fix_starts_at(price, contract)
        next unless price[:price].amount.abs > 0
        due_date = price[:price].starts_at.beginning_of_month + (price[:price].due_at ? price[:price].due_at - 1 : 9).days
        next if due_date < 1.month.ago.beginning_of_month
      else
        due_date = ((price[:idx].month.from_now.beginning_of_month + (price[:price].due_at ? price[:price].due_at - 1 : 9).days))
      end
      # due_date += 1.month if Date.today > due_date

      if price[:price].referral
        debtor_id = price[:price].referral_id
        debtor_type = price[:price].referral_type
      else
        debtor_id = contract ? contract.tenant[0].id : self.debtor_id
        debtor_type = contract ? contract.tenant[0].class.to_s : self.debtor_type
      end

      currency = "ARS"

      if self.name == 'Alquiler'
        currency = contract.dollarized ? "USD" : "ARS"

        if payment = Payment.find_by({
            tax_id: self.id,
            contract_id: contract.id,
            referral: contract.property.agency, 
            receiver: contract.property.owner[0], 
            period: price[:period],
            sub_account: sub_account(contract.property.agency)["Alquiler"],
            concept_id: self.concept_id,
            concept: name,
            due_date: due_date.beginning_of_month..due_date.end_of_month,
        })
          if payment.state != 'paid'
            payment.update({
              tax: self,
              contract: contract,
              referral: contract.property.agency, 
              receiver: contract.property.owner[0], 
              period: price[:period],
              amount: (price[:price].amount * -1),
              currency: currency,
              sub_account: sub_account(contract.property.agency)["Alquiler"],
              concept_id: self.concept_id,
              concept: name,
              due_date: due_date,
              third_party_transfer: contract.third_party_transfer,
              price: price[:price]
            })
          end
        else
          Payment.create({
            tax: self,
            contract: contract,
            referral: contract.property.agency, 
            receiver: contract.property.owner[0], 
            period: price[:period],
            amount: (price[:price].amount * -1),
            sub_account: sub_account(contract.property.agency)["Alquiler"],
            concept_id: self.concept_id,
            concept: name,
            currency: currency,
            state: 'pending',
            due_date: due_date,
            third_party_transfer: contract.third_party_transfer,
            price: price[:price]
          })
        end
      end

      if !self.commission.zero? && contract
        if payment = Payment.find_by({
            tax_id: self.id,
            contract_id: contract.id,
            referral: contract.property.owner[0], 
            receiver: contract.property.agency, 
            period: price[:period],
            sub_account: sub_account(contract.property.agency)["Comision"],
            concept: name,
            concept_id: self.concept_id,
            due_date: due_date.beginning_of_month..due_date.end_of_month,
        })
          if payment.state != 'paid'
            payment.update({
              tax: self,
              contract: contract,
              referral: contract.property.owner[0], 
              receiver: contract.property.agency, 
              period: price[:period],
              amount: price[:price].amount * (self.commission / 100),
              sub_account: sub_account(contract.property.agency)["Comision"],
              concept: name,
              concept_id: self.concept_id,
              state: 'pending',
              currency: currency,
              due_date: due_date,
              price: price[:price]
            })
          end
        else
          Payment.create({
            tax: self,
            contract: contract,
            referral: contract.property.owner[0], 
            receiver: contract.property.agency, 
            period: price[:period],
            amount: price[:price].amount * (self.commission / 100),
            sub_account: sub_account(contract.property.agency)["Comision"],
            concept: name,
            concept_id: self.concept_id,
            state: 'pending',
            currency: currency,
            due_date: due_date,
            price: price[:price]
          })
        end
      end

      fee_price = 0

      if self.name == 'Honorarios' && self.owner_fee.to_f > 0
        fee_price = price[:price].amount * self.owner_fee / 100
        if payment = Payment.find_by({
            tax_id: self.id,
            contract_id: contract.id,
            referral: contract.owner[0], 
            receiver: contract.property.agency, 
            period: price[:period],
            sub_account: sub_account(contract.property.agency)['Honorarios propietario'],
            concept_id: self.concept_id,
            concept: name,
            due_date: due_date.beginning_of_month..due_date.end_of_month,
        })
          if payment.state != 'paid'
            payment.update({
              tax: self,
              contract: contract, 
              referral: contract.owner[0], 
              receiver: contract.property.agency, 
              period: price[:period],
              amount: fee_price,
              sub_account: sub_account(contract.property.agency)['Honorarios propietario'],
              concept_id: self.concept_id,
              concept: name,
              state: 'pending',
              price: price[:price],
              due_date: due_date
            })
          end
        else
          Payment.create({
            tax: self,
            contract: contract, 
            referral: contract.owner[0], 
            receiver: contract.property.agency, 
            period: price[:period],
            amount: fee_price,
            sub_account: sub_account(contract.property.agency)['Honorarios propietario'],
            concept_id: self.concept_id,
            concept: name,
            state: 'pending',
            price: price[:price],
            due_date: due_date
          })
        end
      end

      s_name = 'Impuestos y servicios'

      if payment = Payment.find_by({
        tax_id: self.id,
        contract_id: contract.id,
        referral_id: debtor_id, 
        referral_type: debtor_type,
        receiver: price[:price].receiver, 
        period: price[:period],
        sub_account: sub_account(contract.property.agency)[name] || SubAccount.find_by(name: s_name, agency: contract.property.agency),
        concept_id: self.concept_id,
        concept: name,
        due_date: due_date.beginning_of_month..due_date.end_of_month,
      })
        if payment.state != 'paid'
          payment.update({
            contract: contract, 
            referral_id: debtor_id, 
            referral_type: debtor_type,
            receiver: price[:price].receiver, 
            period: price[:period],
            amount: price[:price].amount - fee_price - price[:price].differential,
            sub_account: sub_account(contract.property.agency)[name] || SubAccount.find_by(name: s_name, agency: contract.property.agency),
            concept_id: self.concept_id,
            concept: name,
            currency: currency,
            third_party_transfer: contract.third_party_transfer && name == 'Alquiler',
            state: 'pending',
            price: price[:price],
            differential: !price[:price].differential.zero?,
            due_date: due_date
          })
        end
      else
        Payment.create({
          tax: self,
          contract: contract, 
          referral_id: debtor_id, 
          referral_type: debtor_type,
          receiver: price[:price].receiver, 
          period: price[:period],
          amount: price[:price].amount - fee_price - price[:price].differential,
          sub_account: sub_account(contract.property.agency)[name] || SubAccount.find_by(name: s_name, agency: contract.property.agency),
          concept_id: self.concept_id,
          concept: name,
          state: 'pending',
          currency: currency,
          third_party_transfer: contract.third_party_transfer,
          price: price[:price],
          differential: !price[:price].differential.zero?,
          due_date: due_date
        })
      end
=begin
      if payment.sub_account.name == 'Impuestos y servicios cobrados' && taxable_type == 'PaymentPlan'
        payment = payments.find_or_create_by(concept: name, period: price[:period], referral: contract.property.agency, contract: contract)
        payment.update!({
          amount: ((payment.amount || 0).abs + price[:price].amount.abs) * -1,
          sub_account: SubAccount.find_by(name: 'Impuestos y servicios pagados', agency: contract.property.agency),
          state: 'pending',
          price: price[:price],
          due_date: due_date
        })
      end
=end
    end
  end

  def create_payments
    return nil unless taxable
    return nil if taxable_type == 'Property'
    contract = taxable_type == 'PaymentPlan' ? taxable.contract : taxable
    times = (contract.starts_at..contract.ends_at).to_a.group_by(&:month).keys.length
    times.times do |t|
      create_payment(t)
    end
  end

  def set_payment_date
    # when payment state transitions to paid set payment date
    update(last_payment_date: Time.now)
  end

  def single_create(contract, arr, amount = nil, property_id = nil)
    amount ||= self.amount

    if self.taxable_type == 'Building'
      if arr == [-1, 0, 1]
        Payment.where('due_date >= ?', 1.month.ago.beginning_of_month).where(contract_id: contract.id, tax_id: self.id, state: 'pending').destroy_all 
      elsif arr.length == 1
        Payment.where('due_date >= ?', arr.first.month.from_now.beginning_of_month).where(contract_id: contract.id, tax_id: self.id, state: 'pending').destroy_all 
      end
    end

    arr.each do |x|
      next if contract.starts_at > x.month.from_now.end_of_month
      next unless !self.ends_at || x.month.from_now <= self.ends_at
      # next if ::Payment.find_by('tax_id = ? AND due_date >= ? AND due_date <= ?', self.id, x.month.from_now.beginning_of_month, x.month.from_now.end_of_month)

      if self.interval > 1 
        months = (x.month.from_now.year * 12 + self.starts_at.month) - (x.month.from_now.year * 12 + self.starts_at.month)
        next unless (months != 1)
        next unless (months % self.interval) == 0
      end

      if self.taxable_type == 'Building'
        if self.unequal_split
          amount = (self.amount || 0) * self.percentages.find_by(property_id: property_id).amount / 100
        else
          if self.applies_when_empty
            amount = (self.amount || 0) / taxable.properties.length
          else
            contracts = taxable.properties.joins(:contracts).distinct.where('contracts.state = ? AND contracts.starts_at <= ? AND contracts.ends_at >= ?', 'ongoing', x.month.from_now.beginning_of_month, x.month.from_now.end_of_month)
            next if contracts.blank?
            amount = (self.amount || 0) / contracts.length
          end
        end
      end

      if (self.owner_action && self.owner_action != 'none')
        band = self.owner_action == 'add'
        multiplier = band ? 1 : -1

        concept = Concept.find(self.concept_id)

        if payment = Payment.find_by({
            tax: self,
            contract: contract, 
            referral: band ? contract.owner.last : contract.property.agency, 
            receiver: band ? contract.property.agency : contract.owner.last, 
            sub_account_id: concept.sub_account.id,
            concept_id: self.concept_id,
            concept: concept.name,
            due_date: x.month.from_now.beginning_of_month..x.month.from_now.end_of_month
        })
          if payment.state != 'paid' && (payment.due_date >= Date.today.beginning_of_month)
            payment.update({
              tax: self,
              contract: contract, 
              referral: band ? contract.owner.last : contract.property.agency, 
              receiver: band ? contract.property.agency : contract.owner.last, 
              amount: ((amount || 0) * multiplier),
              sub_account_id: concept.sub_account.id,
              concept_id: self.concept_id,
              concept: concept.name,
              state: 'pending',
              due_date: x.month.from_now.beginning_of_month + ((self.due_at || 10) - 1).days
            })
          end
        else
          begin
          Payment.create!({
            tax: self,
            contract: contract, 
            referral: band ? contract.owner.last : contract.property.agency, 
            receiver: band ? contract.property.agency : contract.owner.last, 
            amount: ((amount || 0) * multiplier),
            sub_account_id: concept.sub_account.id,
            concept_id: self.concept_id,
            concept: concept.name,
            state: 'pending',
            due_date: x.month.from_now.beginning_of_month + ((self.due_at || 10) - 1).days
          })
          rescue
            debugger
          end
        end
      end

      if (self.tenant_action && self.tenant_action != 'none')
        band = self.tenant_action == 'add'
        multiplier = band ? 1 : -1

        concept = Concept.find(self.concept_id)

        if payment = Payment.find_by({
            tax: self,
            contract: contract, 
            referral: band ? contract.tenant.last : contract.property.agency, 
            receiver: band ? contract.property.agency : contract.tenant.last, 
            sub_account_id: concept.sub_account.id,
            concept_id: self.concept_id,
            concept: concept.name,
            due_date: x.month.from_now.beginning_of_month..x.month.from_now.end_of_month
        })
          if payment.state != 'paid'
            payment.update({
              tax: self,
              contract: contract, 
              referral: band ? contract.tenant.last : contract.property.agency, 
              receiver: band ? contract.property.agency : contract.tenant.last, 
              amount: ((amount || 0) * multiplier),
              sub_account_id: concept.sub_account.id,
              concept_id: self.concept_id,
              concept: concept.name,
              state: 'pending',
              due_date: x.month.from_now.beginning_of_month + ((self.due_at || 10) - 1).days
            })
          end
        else
          Payment.create!({
            tax: self,
            contract: contract, 
            referral: band ? contract.tenant.last : contract.property.agency, 
            receiver: band ? contract.property.agency : contract.tenant.last, 
            amount: ((amount || 0) * multiplier),
            sub_account_id: concept.sub_account.id,
            concept_id: self.concept_id,
            concept: concept.name,
            state: 'pending',
            due_date: x.month.from_now.beginning_of_month + ((self.due_at || 10) - 1).days
          })
        end
      end

      if (self.agency_action && self.agency_action != 'none')
        band = self.agency_action == 'add'
        multiplier = band ? 1 : -1

        concept = Concept.find(self.concept_id)

        if payment = Payment.find_by({
          tax: self,
          contract: contract, 
          referral: contract.property.agency, 
          sub_account_id: concept.sub_account.id,
          concept_id: self.concept_id,
          concept: concept.name,
          due_date: x.month.from_now.beginning_of_month..x.month.from_now.end_of_month
        })
          if payment.state != 'paid'
            payment.update({
              tax: self,
              contract: contract, 
              referral: contract.property.agency, 
              amount: ((amount || 0) * multiplier),
              sub_account_id: concept.sub_account.id,
              concept_id: self.concept_id,
              concept: concept.name,
              state: 'pending',
              due_date: x.month.from_now.beginning_of_month + ((self.due_at || 10) - 1).days
            })
          end
        else
          Payment.create!({
            tax: self,
            contract: contract, 
            referral: contract.property.agency, 
            amount: ((amount || 0) * multiplier),
            sub_account_id: concept.sub_account.id,
            concept_id: self.concept_id,
            concept: concept.name,
            state: 'pending',
            due_date: x.month.from_now.beginning_of_month + ((self.due_at || 10) - 1).days
          })
        end
      end
    end
  end

  def fix_single_user_payments
    return unless self.taxable_type == 'User'
    concept = Concept.find_by(id: self.concept_id)

    return unless concept

    payments = ::Payment.where(tax_id: self.id)

    band = self.agency_action == 'add'
    multiplier = band ? -1 : 1

    payments.each do |payment|
      payment.update(
        referral: !band ? taxable : concept.sub_account.agency,
        receiver: !band ? concept.sub_account.agency : taxable,
        amount: payment.amount.abs * multiplier
      )
    end
  end

  def single_user_create(user, arr, amount = nil, property_id = nil)
    # Payment.where('due_date >= ?', 1.month.ago.beginning_of_month).where(tax_id: self.id, state: 'pending', contract_id: nil).destroy_all

    amount ||= self.amount

    arr.each do |x|
      if self.taxable_type == 'Building'
        # Only gets here if applies_when_empty
        if self.unequal_split
          amount = (self.amount || 0) * self.percentages.find_by(property_id: property_id).amount / 100
        else
          amount = (self.amount || 0) / taxable.properties.length
        end
      end

      next if self.ends_at && x.month.from_now > self.ends_at
      payment = ::Payment.find_by('tax_id = ? AND due_date >= ? AND due_date <= ?', self.id, x.month.from_now.beginning_of_month, x.month.from_now.end_of_month)
      next if payment && payment.state == 'paid'

      if self.interval > 1 
        months = (x.month.from_now.year * 12 + self.starts_at.month) - (x.month.from_now.year * 12 + self.starts_at.month)
        next unless (months != 1)
        next unless (months % self.interval) == 0
      end

      band = self.agency_action == 'add'
      multiplier = band ? -1 : 1

      concept = Concept.find_by(id: self.concept_id)
      
      next unless concept

      if payment
        next if payment.state == 'paid'
        payment.update({
          referral: !band ? (user || taxable) : concept.sub_account.agency,
          receiver: !band ? concept.sub_account.agency : (user || taxable),
          amount: ((amount || 0) * multiplier),
          sub_account_id: concept.sub_account.id,
          concept_id: self.concept_id,
          concept: concept.name,
          state: 'pending',
          due_date: x.month.from_now.beginning_of_month + ((self.due_at || 10) - 1).days
        })
      else
        payment = Payment.create!({
          tax: self,
          referral: !band ? (user || taxable) : concept.sub_account.agency,
          receiver: !band ? concept.sub_account.agency : (user || taxable),
          amount: ((amount || 0) * multiplier),
          sub_account_id: concept.sub_account.id,
          concept_id: self.concept_id,
          concept: concept.name,
          state: 'pending',
          due_date: x.month.from_now.beginning_of_month + ((self.due_at || 10) - 1).days
        })
      end
    end
  end

  def building_create(arr)
    if !self.starts_at
      update starts_at: Date.today.beginning_of_month
    end

    taxable.properties.each do |property|
      if contract = property.active_contract
        single_create(contract, arr, nil, property.id)
      else
        # Cuando está vacío, se cobra igual
        if applies_when_empty
          next if property.owner.blank?
          single_user_create(property.owner[0], arr, nil, property.id)
        end
      end
    end
  end

  private
    def sub_account(agency)
      {
        'Alquiler' => SubAccount.find_by(name: 'Alquileres', agency: agency),
        'Honorarios' => SubAccount.find_by(name: 'Honorarios alquiler inquilino', agency: agency),
        'Honorarios propietario' => SubAccount.find_by(name: 'Honorarios alquiler propietario', agency: agency),
        # 'Rendicion' => SubAccount.find_by(name: 'Alquileres pagados', agency: agency),
        'Comision' => SubAccount.find_by(name: 'Honorarios administración alquiler', agency: agency),
      }
    end

    def duplicate_in_contract
      return unless taxable_type == 'Property'
      return unless contract = taxable.contracts.find_by(state: 'ongoing')

      ntax = self.deep_dup
      ntax.starts_at = contract.starts_at
      ntax.ends_at = contract.ends_at

      self.prices.each do |price|
        nprice = price.deep_dup
        if price.referral_id.nil?
          nprice.referral_id = contract.tenant.last.id
        end
        ntax.prices.push(nprice)
      end

      contract.taxes.push(ntax)
    end

    def fix_starts_at(price, contract)
      idx = self.prices.index(price[:price])
      starts_at = (contract.starts_at + idx.month).beginning_of_month

      price[:price].update(starts_at: starts_at)
      price[:price].reload
    end

    def destroy_pending_payments
      Payment.where(state: :pending, tax_id: self.id).destroy_all
    end
end
