class Agency < ApplicationRecord
  resourcify
  mount_uploader :logo, LogoUploader

  after_create :set_accounts
  
  include Userable

  belongs_to :organization

  has_many :properties
  has_many :contracts, through: :properties
  has_many :users, through: :user_agencies
  has_many :payments, as: :referral
  has_many :sub_accounts, dependent: :destroy
  has_many :accounts, dependent: :destroy
  has_many :receipts, dependent: :destroy
  has_many :buildings, dependent: :destroy

  has_many :searches
  has_many :cash_registers

  has_many :socials

  validates :name, presence: true, uniqueness: true

  def unseen_properties_results
    properties.joins(:search_results).where.not('(?) = ANY (search_results.seen_by)', id).count
  end

  def unseen_results
    searches.joins(:search_results).where.not('(?) = ANY (search_results.seen_by)', id).count
  end

  def unseen_inquiries
    Inquiry.where(read: false).count
  end

  def set_accounts
    fr = accounts.find_or_create_by(name: 'Flujo operativo')
    ["Alquileres cobrados", "Alquileres pagados", "Impuestos y servicios cobrados", "Impuestos y servicios pagados", "Reparaciones"].each do |name|
      fr.sub_accounts.find_or_create_by(name: name, agency_id: id)
    end

    lt = accounts.find_or_create_by(name: "Resultado inmobiliario")
    ["Honorarios alquiler inquilino", "Honorarios alquiler propietario", "Honorarios venta", "Honorarios administración alquiler"].each do |name|
      lt.sub_accounts.find_or_create_by(name: name, agency_id: id)
    end
  end
end
