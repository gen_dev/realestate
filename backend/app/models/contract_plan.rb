class ContractPlan < ApplicationRecord
  belongs_to :property
  belongs_to :contract
  has_many :contract_expenses, dependent: :destroy

  validates :duration, presence: true
  validates :rent_update_period, presence: true
  validates :rent_value, presence: true

  def current_period
    # get total of months between start date and due date
    total_months = (due_date.year * 12 + due_date.month) - (start_date.year * 12 + start_date.month)

    # get total of periods
    periods = total_months / rent_update_period

    # iterate through periods to find if date is included on some period range
    periods.times do |x| 
      @date1 = @date1.nil? ? start_date : @date1.months_ago(-rent_update_period)
      @date2 = @date1.months_ago(-rent_update_period)
      
      if Time.now > @date1 && Time.now < @date2
        @period = x.to_s 
        break
      end
    end 
    
    @period 
  end

  def current_period_value
    rent_value[current_period]
  end

  def total_amount
    # multiply each period total amount by total of periods
    rent_value.values.map { |x| rent_update_period * x }.sum
  end
end