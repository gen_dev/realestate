class Ticket < ApplicationRecord
  belongs_to :contract
  has_many :descriptions, class_name: 'Tickets::Description', dependent: :destroy
  has_one :fix, class_name: 'Properties::Fix', dependent: :destroy
  belongs_to :opener, polymorphic: true, optional: true

  accepts_nested_attributes_for :descriptions, allow_destroy: true
  accepts_nested_attributes_for :fix, allow_destroy: true

  default_scope { order('updated_at DESC') }
end
