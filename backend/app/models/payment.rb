class Payment < ApplicationRecord
  belongs_to :counterpart, class_name: 'Payment', foreign_key: :counterpart_id, optional: true

  belongs_to :contract, optional: true
  belongs_to :tax, optional: true
  belongs_to :receipt, optional: true
  belongs_to :sub_account
  belongs_to :sub_account_concept, class_name: 'Concept', foreign_key: 'concept_id', optional: true

  # If nil then belongs to future tenant
  belongs_to :referral, polymorphic: true, optional: true
  belongs_to :receiver, polymorphic: true, optional: true
  belongs_to :payable, polymorphic: true, optional: true
  belongs_to :price, class_name: 'Price::Period', optional: true

  # Deprecated
  has_many :partial_payments
  has_many :chargebacks, class_name: 'Payments::Chargeback'

  belongs_to :parent, class_name: 'Payment', optional: true
  has_many :children, class_name: 'Payment', foreign_key: :parent_id

#  validates :period, presence: true
  validates :concept, presence: true
  validates :state, presence: true # Realizado / Pendiente / Vencido
  validates :due_date, presence: true
  validates :amount, presence: true

  after_update :update_sibling

  before_save :update_tag
  before_save :update_commissioned_amount
  before_save :update_fixes_amount
  before_save :update_counterpart

  before_destroy :can_destroy?, prepend: true

  def can_destroy?
    if children.length > 0
      self.errors.add(:base, "Can't be destroy payment with children")
      throw :abort
    end
  end

  def self.current(period = 'current')
    if period == 'current'
      date = Date.today
      where(due_date: date.beginning_of_month..date.end_of_month).or(where(paid_at: date.beginning_of_month..date.end_of_month))
    elsif period == 'current_exclusive'
      date = Date.today
      where(due_date: date.beginning_of_month..date.end_of_month)
    elsif period == 'past'
      date = Date.today
      where('(due_date < ? AND state = ?) OR (paid_at BETWEEN ? AND ? AND due_date < ?)', date.beginning_of_month, 'pending', date.beginning_of_month, date.end_of_month, date.beginning_of_month)
    else
      number = period == 'next' ? 1 : period.to_i
      date = (number).month.from_now
      where(due_date: date.beginning_of_month..date.end_of_month)
    end

    # where(due_date: 2.week.ago.beginning_of_month..1.month.from_now.end_of_month).or(where(paid_at: now.beginning_of_month..now.end_of_month))
  end

  def self.pending
    where(state: 'pending')
  end

  def payable_name
    return payable.name if payable
  end

  def name
    if referral_type == 'User'
      referral.profile.name
    else
      'Inmobiliaria Sartor'
    end
  end

  def receiver_name
    if receiver
      if receiver_type == 'User'
        receiver.profile.name
      else
        receiver.name
      end
    else
      'Externo'
    end
  end

  def expired
    return false if state == 'paid'
    Date.today > due_date
  end

  def manual
    # !sub_account.account
    true
  end

  def can_edit
    return true
    manual && state == 'pending'
  end

  def is_advanced
    return false unless tax

    if (concept == "Alquiler")
      advanced && tax.payments.where('state = ? AND NOT id = ? AND referral_type = ?', 'pending', id, 'User').any?
    else
      advanced && tax.payments.where('state = ? AND NOT id = ?', 'paid', id).blank?
    end
  end

  def can_advance
    return false unless tax
    return false if (state == 'paid' && !advanced)
    true
  end

  def user_type
    return 'Inmobiliaria' if referral_type == 'Agency'
    referral.has_role?(:tenant, contract) ? 'Locatario' : 'Locador'
  end

  def lasting_amount
    sign = amount.positive? ? '-' : '+'

    amount = total.send(sign, children.pluck(:amount).sum)

    amount
  end 

  def partial_amount
    if children.blank?
      amount
    else
      children.pluck(:amount).sum
    end
  end

  def chargeback_amount
    sign = amount.positive?
    chargeback_amounts = chargebacks.where(active: true).pluck(:amount).sum

    partial_amount + chargeback_amounts
  end

  def commission_amount
    return 0 unless tax
    return 0 if tax.commission.zero?
    comission = tax.payments.includes(:sub_account).references(:sub_account).where('sub_accounts.name = ?', 'Honorarios administración alquiler').last
    return 0 unless comission
    comission.lasting_amount.abs
  end

  def total
    amount + chargebacks.where(active: true).pluck(:amount).sum
  end

  def siblings
    return nil unless tax
    Payment.where(tax_id: tax.id).includes(:sub_account).references(:sub_account).where('NOT payments.id = ? AND payments.due_date BETWEEN ? AND ?', self.id, due_date.beginning_of_month, due_date.end_of_month)
  end

  private

  def update_counterpart
    return nil if siblings.blank?
    sibling = siblings.find_by('sub_accounts.id = ?', sub_account.id)

    self.counterpart = sibling
  end

  def update_commissioned_amount
    return self.commissioned_amount = self.amount unless contract
    return self.commissioned_amount = self.amount unless contract.property
    return self.commissioned_amount = self.amount if contract.owner.blank?
    return self.commissioned_amount = self.amount unless tax
    commission = tax.commission
    return self.commissioned_amount = self.amount if commission.zero?
    ids = contract.property.fixes.pluck(:id)
    payments = Payment.current.where('payable_type = ? AND payable_id IN (?) AND referral_type = ? AND referral_id = ?', 'Properties::Fix', ids, 'User', contract.owner.last.id)
    commissioned = (self.amount * commission) / 100

    aux = self.amount - commissioned + payments.pluck(:amount).sum

    self.commissioned_amount = aux
  end

  def update_fixes_amount
    return self.fixes_amount = 0 unless contract
    return self.fixes_amount = 0 unless contract.property
    return self.fixes_amount = 0 unless contract.property.fixes.any?

    aux = 0

    if (concept == "Alquiler" && sub_account.name != 'Honorarios administración alquiler')
      return self.fixes_amount = 0 if referral_id == contract.owner.last.id && referral_type == 'User'
      ids = contract.property.fixes.pluck(:id)
      current = Payment.current.preload(:payable, :referral)
      if (referral_type == 'Agency')
        payments = current.where('payable_type = ? AND payable_id IN (?) AND referral_type = ? AND referral_id = ?', 'Properties::Fix', ids, 'User', contract.owner.last.id)
        return self.fixes_amount = 0 unless payments.any?
        return self.fixes_amount = payments.pluck(:amount).sum
      end
      payments = current.where('payable_type = ? AND payable_id IN (?) AND referral_type = ? AND referral_id = ?', 'Properties::Fix', ids, referral_type, referral_id)
      return self.fixes_amount = 0 unless payments.any?
      aux = payments.pluck(:amount).sum
    end

    self.fixes_amount = aux
  end

  def create_movement
    # callback
    # Once a payment is updated we need to create a movement regarding the transaction
  end

  def update_cash
    # callback
    # Once a payment is updated, it should update the parent Cash balance.
  end

  def update_sibling
    return if parent
    return unless tax
    return unless siblings

    aux = siblings.where('sub_accounts.name = ?', 'Honorarios administración alquiler')

    if aux.count > 1
      sibling = aux.where(state: self.state)
      lasting = aux.where.not(state: self.state)
      lasting.delete_all
      if sibling.count > 1
        aux = sibling.first
        sibling = sibling.last
        aux.destroy
      end
    else
      sibling = aux.first
    end

    return unless sibling
    return if (self.amount.abs * (self.tax.commission / 100)) == (sibling.amount.abs * (sibling.tax.commission / 100))
    sibling.update(amount: self.amount.abs * (self.tax.commission / 100))
  rescue
    nil
  end


  def update_tag
    self.taxable_tag = tax ? tax.taxable_type : nil
    return unless self.contract
    self.contract_tag = contract.full_address
  end
end
