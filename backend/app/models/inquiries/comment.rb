class Inquiries::Comment < ApplicationRecord
  belongs_to :inquiry
  belongs_to :user
end
