class Price::Period < ApplicationRecord
  default_scope { order(id: :asc) }

  before_validation :change_sign

  after_create :update_parent
  after_save :update_parent

  belongs_to :priceable, polymorphic: true, optional: true
  belongs_to :referral, polymorphic: true, optional: true
  belongs_to :receiver, polymorphic: true, optional: true

  has_many :payments, foreign_key: :price_id

  def months
    return nil unless ends_at && starts_at
    ((ends_at - starts_at).to_f / 365 * 12).round
  end

  def contract
    return unless priceable
    return unless priceable.taxable
    if priceable.taxable_type == 'PaymentPlan'
      priceable.taxable&.contract
    else
      priceable.taxable
    end
  end

  def amount
    super || 0
  end

  private

    def change_sign
      return if self.amount.blank? || self.sign.blank?
      tmp = self.amount.to_s.gsub(/\-/, '')
      tmp = self.sign + tmp
      self.amount = tmp
    end

    def update_parent
      if (self.priceable_type == 'Tax' && !self.saved_change_to_starts_at? && !self.saved_change_to_ends_at?)
        self.priceable.create_payment
      end
    end
end
