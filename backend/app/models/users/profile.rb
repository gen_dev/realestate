class Users::Profile < ApplicationRecord
  belongs_to :user
  has_many :documents, class_name: 'Document', as: :documentable

  validates_presence_of :name
end
