class Search < ApplicationRecord
  belongs_to :agency, optional: true
  belongs_to :user, optional: true
  belongs_to :inquiry, optional: true

  has_many :search_results, 
    dependent: :destroy

  after_create :search_for_results
  after_update :search_for_results

  after_create :assign_to_inquiry

  def results 
    search_results.where(active: true)
  end

  def results_count
    results.count
  end

  def unseen_results
    return 0 unless agency

    results.where.not('(?) = ANY (search_results.seen_by)', agency_id).count
  end

  # Remaining public for now
  def search_for_results
    searching_results = ::SearchService.match(self)
    ids = []

    searching_results.each_pair do |id, score|
      ids.push id
      result = self.search_results.find_or_create_by(property_id: id)
      result.update score: score, user_id: user_id, active: true
    end

    search_results.where.not(property_id: ids).destroy_all
  end

  def assign_to_inquiry
    return if self.inquiry_id
    return unless self.user

    inquiry = Inquiry.create!(
      contact_id: self.user.id,
      user_id: self.user.advisor_id,
      name: self.user.profile.name,
      phone: self.user.profile.phone,
      email: self.user.profile.email,
      topic: "Búsqueda de propiedades - #{self.user.profile.name}",
      created_at: self.created_at,
      state: 'pending',
      message: 'Consulta automática creada a través de una búsqueda.'
    )

    self.update inquiry_id: inquiry.id
  end
end
