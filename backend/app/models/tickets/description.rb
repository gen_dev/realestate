class Tickets::Description < ApplicationRecord
  belongs_to :ticket, touch: true
end
