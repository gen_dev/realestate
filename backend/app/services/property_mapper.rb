class PropertyMapper
  PROPERTY_TYPE_MAPPING = {
    'apartment' => 2,   # Departamento
    'farm' => 9,        # Countryside
    'house' => 3,       # Casa
    'terrain' => 1,     # Land
    'salon' => 7,       # Business Premises
    'shed' => 24,       # Galpón
    'parking' => 10,    # Garage
    'warehouse' => 14   # Storage
  }.freeze

  def initialize(input_json)
    @input = JSON.parse(input_json)
  end

  def map
    {
      updated_at: @input['updated_at'],
      reference_code: @input['id'],
      producer_user: nil, # No mapping available
      contacts: [],
      status: map_status(@input['state']),
      property_type: map_property_type(@input['property_type']),
      services: [],
      street: @input.dig('address', 'street'),
      number: @input.dig('address', 'number'),
      floor: nil, # No mapping available
      apartment: @input.dig('address', 'unit'),
      geo_lat: @input.dig('address', 'lat'),
      geo_long: @input.dig('address', 'lng'),
      description: @input['description'],
      internal_comments: nil, # No mapping available
      publication_title: nil, # No mapping available
      publication_title_en: nil, # No mapping available
      commission: nil, # No mapping available
      operations: map_operations(@input),
      pictures: [],
      videos: []
    }
  end

  private

  def map_property_type(property_type)
    PROPERTY_TYPE_MAPPING[property_type] || 3 # Default to 3 (House) if not found
  end

  def map_status(state)
    return 2 if state == 'published'
    return 3 if state == 'booked'

    4
  end

  def map_attributes(additional_features)
    additional_features.map do |feature|
      {
        code: feature['name'].downcase.gsub(' ', '_'),
        value: feature['values']
      }
    end
  end

  def map_location(address)
    [address['city'], address['state'], address['country']].compact.join(', ')
  end

  def map_operations(input)
    operation_types = input.dig('features', 'operation_type')
    operations = []

    if operation_types['rent']
      operations << {
        type: 1, # Rent type
        prices: [
          {
            currency: input['currency'],
            price: input['price'].to_s.gsub('$', '').strip.to_i
          }
        ]
      }
    end

    if operation_types['sell']
      operations << {
        type: 2, # Sell type
        prices: [
          {
            currency: input['sell_currency'],
            price: input['sell_price'].to_s.gsub('ARS', '').strip.to_i
          }
        ]
      }
    end

    operations
  end

  def map_pictures(blueprint_url)
    return [] unless blueprint_url

    [
      {
        url: blueprint_url,
        description: 'Blueprint',
        is_blueprint: true
      }
    ]
  end
end
