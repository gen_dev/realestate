require 'byebug'

class FilesService
  def initialize(params)
    raise ArgumentError, 'This params are required: files, object_class, object_id' unless params

    @files = params[:files]
    return nil unless @files
    @object_class = params[:object_class]
    object_id = params[:object_id]

    raise ArgumentError, 'This params are required: files, object_class, object_id' unless @files && @object_class && object_id

    @object = @object_class.constantize.find(object_id)
  end

  def upload
    case 
    when @object.is_a?(Property)
      @object.images << create_images
      @object.save
    when @object.is_a?(Contract) || @object.is_a?(Contracts::Guarantee) || @object.is_a?(Users::Profile)
      @object.documents << create_documents 
      @object.save
    end
  end

  private

  # create Image objects with image attachment
  # return Array of Image's
  def create_images 
    @files.map { |file| Image.create!(image: file, size: file.size, name: file.original_filename, imageable: @object) }
  end 
  
  # create Document objects with document attachment
  # return Array of Document's
  def create_documents 
    @files.map { |file| Document.create!(document: file, size: file.size, name: file.original_filename, documentable: @object) }
  end 
end
