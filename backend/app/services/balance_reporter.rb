class BalanceReporter
  require "i18n"

  def self.generate(account, range)
    timestamp = Time.now.strftime("%Y%m%d")
    file_name = "reporte_balances_#{timestamp}.xlsx"
    dirname = Rails.root + 'tmp/reports/'

    unless File.directory?(dirname)
      FileUtils.mkdir_p(dirname)
    end

    file_name = dirname + file_name

    ::Axlsx::Package.new do |p|
      account.sub_accounts.each do |sub_account|
        p.workbook.add_worksheet(:name => I18n.transliterate(sub_account.name.truncate(31).gsub("/", "-"))) do |sheet|
          sheet.add_row ['Reporte:', I18n.transliterate(account.name)]

          headers = [
            "ID",
            "Concepto",
            "Monto",
            "Fecha asiento",
            "Observaciones",
            "Contrato",
            "A nombre de"
          ]

          style = sheet.styles.add_style(:bg_color => "1f3864", :fg_color => "FFFFFF")

          sheet.add_row headers, style: style

          pending_receipts = Receipt.where(state: 'pending').pluck(:id)

          if range.blank?
            payments = sub_acount.payments
              .where(paid_at: Date.today.beginning_of_month..)
              .where('state = ? AND receipt_id IS NULL OR NOT receipt_id IN (?)', 'paid', pending_receipts)
          else
            initial = range[0].to_date
            latest = range[-1].to_date

            payments = sub_account.payments.where(paid_at: initial.beginning_of_month..latest.end_of_month)
            payments = payments.where('state = ? AND receipt_id IS NULL OR NOT receipt_id IN (?)', 'paid', pending_receipts)
          end

          payments.each do |payment|
            aux = [
              payment.id,
              payment.concept,
              "#{payment.amount.to_i}",
              (payment.paid_at || (payment.receipt ? payment.receipt.updated_at : nil) || payment.updated_at).strftime("%d-%m-%Y"),
              payment.detail,
              payment.contract_tag,
              payment.name
            ]

            sheet.add_row aux
          end

          sheet.auto_filter = "A2:E#{sheet.rows.count}"
        end
      end

      s = p.to_stream()
      File.open(file_name, 'w') { |f| f.write(s.read.force_encoding("UTF-8"))}
    end

    file_name
  end

  def self.translations(field)
    status = {
      assigned: 'Asignado',
      installing: 'Instalación',
      installed: 'Instalado',
      pre_approved: 'Doc. enviada',
      approved: 'Aprobado',
      cancelled: 'Cancelado',
      partial: 'Enviado Fac. parcial',
      partial_billed: 'Fac. parcial',
      total: 'Enviado Fac. total',
      closed: 'Cerrado',
    }
    return status[field.to_sym]
  end
end
