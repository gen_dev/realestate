class ResponseService
  def initialize(location, context)
    @location = location
    @context = context
  end

  def success(hash)
    { 
    }.merge(hash)
  end

  def failure(status, errors = [])
    build_errors(errors, status) unless errors.blank?

    { }
  end

  private
    def build_errors(errors, status = 500)
      errors.map do |attr, message|
        display_message = "#{attr} #{message}" 
        @context.try(:add_error, GraphQL::ExecutionError.new(display_message, extensions: { 
          code: 'objectInputError', 
          status: status,
          location: @location,
          field: attr,
          message: message.parameterize(separator: '_') 
        }))
      end
    end
end
