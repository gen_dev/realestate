class SearchService
  def initialize(options)
  end

  def self.match(search)
    results = {}
    max_score = 0

    @search = search
    base_properties = initial_property_scope

    base_properties = filter_by_address(base_properties, search.address) if search.address
    base_properties = filter_by_operation_type(base_properties, search.operation_type)
    base_properties = filter_by_property_type(base_properties, search.property_type) unless search.property_type.blank?
    base_properties = filter_by_price(base_properties, search)

    properties = base_properties.pluck(:id)
    results = calculate_scores(properties, results, 20)
    max_score += 20

    characteristics_filters.each do |filter|
      properties = send("filter_by_#{filter}", base_properties, search)
      results = calculate_scores(properties.pluck(:id), results, 10) if properties.exists?
      max_score += 10
    end

    normalize_results(results, max_score)
  end

  private

  def self.initial_property_scope
    if @search.operation_type == "sell"
      ::Property.where("properties.disabled = ? AND properties.state IN (?) AND properties.features->'operation_type'->>'sell' = ?", false, ['published', 'booked', 'rented'], "true")
    else
      ::Property
        .where(disabled: false, state: ['published', 'booked'])
    end
  end

  def self.filter_by_address(scope, address)
    city, state = address.split(', ')
    scope.joins(:address).where('addresses.city = ? AND addresses.state = ?', city, state)
  end

  def self.filter_by_operation_type(scope, operation_type)
    return scope unless operation_type

    scope.where("features->'operation_type' IS NOT NULL")
         .where("features->'operation_type'->>'#{operation_type.downcase}' = ?", "true")
  end

  def self.filter_by_property_type(scope, property_type)
    scope.where(property_type: property_type)
  end

  def self.filter_by_price(scope, search)
    if search.min_price.to_f > 0 || search.max_price.to_f > 0
      if search.currency
        currency_filter = search.operation_type == "rent" ? { currency: search.currency } : { sell_currency: search.currency }
      else
        currency_filter = {}
      end

      if search.min_price.to_f > 0 && search.max_price.to_f > 0
        price_range = search.min_price.to_f..search.max_price.to_f
      elsif search.min_price.to_f > 0
        price_range = search.min_price.to_f..
      else
        price_range = ..search.max_price.to_f
      end

      if search.operation_type == "rent"
        scope.where(currency_filter).where(price: price_range)
      elsif search.operation_type == "sell"
        scope.where(currency_filter).where(sell_price: price_range)
      else
        scope.where(price: price_range).or(scope.where(sell_price: price_range))
      end
    else
      scope
    end
  end

  def self.characteristics_filters
    %w[brooms baths surface covered_surface]
  end

  def self.filter_by_brooms(scope, search)
    return scope unless search.min_brooms.to_f > 0 || search.max_brooms.to_f > 0

    min_brooms = search.min_brooms.to_f
    max_brooms = search.max_brooms.to_f if search.max_brooms.to_f > 0

    scope.joins(:characteristics)
         .where('simple_datas.name = ?', 'Dormitorios')
         .where('simple_datas.value': min_brooms..max_brooms)
  end

  def self.filter_by_baths(scope, search)
    return scope unless search.min_baths.to_f > 0 || search.max_baths.to_f > 0

    min_baths = search.min_baths.to_f
    max_baths = search.max_baths.to_f if search.max_baths.to_f > 0

    scope.joins(:characteristics)
         .where('simple_datas.name = ?', 'Baños')
         .where('simple_datas.value': min_baths..max_baths)
  end

  def self.filter_by_surface(scope, search)
    return scope unless search.min_surface.to_f > 0 || search.max_surface.to_f > 0

    min_surface = search.min_surface.to_f
    max_surface = search.max_surface.to_f if search.max_surface.to_f > 0

    scope.joins(:characteristics)
         .where('simple_datas.name = ?', 'Superficie total')
         .where('CAST(simple_datas.value as FLOAT) >= ? AND CAST(simple_datas.value as FLOAT) <= ?', min_surface, max_surface)
  end

  def self.filter_by_covered_surface(scope, search)
    return scope unless search.min_covered_surface.to_f > 0 || search.max_covered_surface.to_f > 0

    min_covered_surface = search.min_covered_surface.to_f
    max_covered_surface = search.max_covered_surface.to_f if search.max_covered_surface.to_f > 0

    scope.joins(:characteristics)
         .where('simple_datas.name = ?', 'Superficie útil')
         .where('CAST(simple_datas.value as FLOAT) >= ? AND CAST(simple_datas.value as FLOAT) <= ?', min_covered_surface, max_covered_surface)
  end

  def self.calculate_scores(properties, results, score)
    properties.each do |property|
      results[property] = (results[property] || 0) + score
    end
    results
  end

  def self.normalize_results(results, max_score)
    results.transform_values! { |v| (v.to_f / max_score * 100.0).round(2) }
           .sort_by { |_k, v| -v }
           .to_h
  end
end
