module Mutations
  module Concepts
    class DeleteConcept < BaseMutation
      argument :id, ID, required: true

      field :success, Boolean, null: false

      def resolve(id:)
        current_user = context[:current_user]
        
        return initialize_response.failure(403, [['user', 'is required']]) unless current_user

        concept = ::Concept.find_by(id: id)

        return initialize_response.failure(404, [['concept', 'not found']]) unless concept

        return initialize_response.success({ success: true }) if concept.destroy
        initialize_response.failure(500, concept.errors)
      end

      private

      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
