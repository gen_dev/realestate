module Mutations
  module Concepts
    class CreateConcept < BaseMutation
      argument :attributes, Types::Concepts::ConceptInputType, required: true

      field :success, Boolean, null: true

      def resolve(attributes:)
        current_user = context[:current_user]

        return initialize_response.failure(403, [['user', 'is required']]) unless current_user

        concept = ::Concept.new(attributes.to_h)

        if concept.save
          initialize_response.success({ success: true })
        else
          initialize_response.failure(500, sub_account.errors)
        end
      end

      private
      
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
