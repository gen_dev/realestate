module Mutations
  module Images
    class DeleteImage < BaseMutation
      argument :image_id, ID, required: true

      field :success, Boolean, null: false

      def resolve(image_id:)
        current_user = context[:current_user]
        
        return initialize_response.failure(403, [['user', 'is required']]) unless current_user

        image = Image.find_by(id: image_id)

        return initialize_response.failure(404, [['image', 'not found']]) unless image
        return initialize_response.failure(401, [['user', 'unauthorized']]) if current_user.cannot?(:destroy, image)

        return initialize_response.success({ success: true }) if image.destroy
        initialize_response.failure(500, image.errors)
      end

      private

      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
