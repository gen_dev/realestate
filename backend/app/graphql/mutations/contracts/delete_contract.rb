module Mutations
  module Contracts
    class DeleteContract < BaseMutation
      argument :contract_id, ID, required: true

      field :success, Boolean, null: false

      def resolve(contract_id:)
        current_user = context[:current_user]
        
        return initialize_response.failure(403, [['user', 'is required']]) unless current_user

        contract = Contract.find_by(id: contract_id)

        return initialize_response.failure(404, [['contract', 'not found']]) unless contract
        return initialize_response.failure(401, [['user', 'unauthorized']]) if current_user.cannot?(:destroy, contract)

        return initialize_response.success({ success: true }) if contract.destroy
        initialize_response.failure(500, contract.errors)
      end

      private

      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
