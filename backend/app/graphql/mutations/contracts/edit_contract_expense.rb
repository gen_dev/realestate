module Mutations
  module Contracts
    class EditContractExpense < BaseMutation
      argument :attributes, Types::Contracts::ContractExpenseInputType, required: true
      argument :contract_expense_id, ID, required: true

      field :contract_expense, Types::Contracts::ContractExpenseType, null: true
      
      def resolve(attributes: nil, contract_expense_id:)
        return initialize_response.failure(500, [['attributes', 'is required']]) unless attributes

        contract_expense = ContractExpense.find_by(id: contract_expense_id)

        current_user = context[:current_user]
        return initialize_response.failure(403, [['user', 'is required']]) unless current_user
        return initialize_response.failure(404, [['contract expense', 'not found']]) unless contract_expense
        return initialize_response.failure(401, [['user', 'unauthorized']]) if current_user.cannot?(:update, contract_expense)

        contract_expense.update(attributes.to_h)

        return initialize_response.success({ contract_expense: contract_expense }) if contract_expense.valid?
        initialize_response.failure(500, contract_expense.errors)
      end

      private
    
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
