module Mutations
  module Contracts
    class EditContractPlan < BaseMutation
      argument :attributes, Types::Contracts::ContractPlanInputType, required: true
      argument :contract_plan_id, ID, required: true

      field :contract_plan, Types::Contracts::ContractPlanType, null: true
      
      def resolve(attributes: nil, contract_plan_id:)
        return initialize_response.failure(500, [['attributes', 'is required']]) unless attributes

        contract_plan = ContractPlan.find_by(id: contract_plan_id)

        current_user = context[:current_user]
        return initialize_response.failure(403, [['user', 'is required']]) unless current_user
        return initialize_response.failure(404, [['contract plan', 'not found']]) unless contract_plan
        return initialize_response.failure(401, [['user', 'unauthorized']]) if current_user.cannot?(:update, contract_plan)

        contract_plan.update(attributes.to_h)

        return initialize_response.success({ contract_plan: contract_plan }) if contract_plan.valid?
        initialize_response.failure(500, contract_plan.errors)
      end

      private
    
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
