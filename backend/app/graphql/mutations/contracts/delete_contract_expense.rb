module Mutations
  module Contracts
    class DeleteContractExpense < BaseMutation
      argument :contract_expense_id, ID, required: true

      field :success, Boolean, null: false

      def resolve(contract_expense_id:)
        current_user = context[:current_user]
        
        return initialize_response.failure(403, [['user', 'is required']]) unless current_user

        contract_expense = ContractExpense.find_by(id: contract_expense_id)

        return initialize_response.failure(404, [['contract expense', 'not found']]) unless contract_expense
        return initialize_response.failure(401, [['user', 'unauthorized']]) if current_user.cannot?(:destroy, contract_expense)

        return initialize_response.success({ success: true }) if contract_expense.destroy
        initialize_response.failure(500, contract_expense.errors)
      end

      private

      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
