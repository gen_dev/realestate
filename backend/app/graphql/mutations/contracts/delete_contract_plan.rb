module Mutations
  module Contracts
    class DeleteContractPlan < BaseMutation
      argument :contract_plan_id, ID, required: true

      field :success, Boolean, null: false

      def resolve(contract_plan_id:)
        current_user = context[:current_user]
        
        return initialize_response.failure(403, [['user', 'is required']]) unless current_user

        contract_plan = ContractPlan.find_by(id: contract_plan_id)

        return initialize_response.failure(404, [['contract plan', 'not found']]) unless contract_plan
        return initialize_response.failure(401, [['user', 'unauthorized']]) if current_user.cannot?(:destroy, contract_plan)

        return initialize_response.success({ success: true }) if contract_plan.destroy
        initialize_response.failure(500, contract_plan.errors)
      end

      private

      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
