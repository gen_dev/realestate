module Mutations
  module Contracts
    class CreateContractPlan < BaseMutation
      argument :attributes, Types::Contracts::ContractPlanInputType, required: true
      argument :contract_id, ID, required: true
      argument :property_id, ID, required: true

      field :contract_plan, Types::Contracts::ContractPlanType, null: true

      def resolve(attributes: nil, contract_id:, property_id:)
        contract = Contract.find_by(id: contract_id)
        property = Property.find_by(id: property_id)

        current_user = context[:current_user]

        return initialize_response.failure(403, [['user', 'is required']]) unless current_user
        return initialize_response.failure(404, [['contract', 'not found']]) unless contract
        return initialize_response.failure(404, [['property', 'not found']]) unless property
        
        contract_plan = ContractPlan.new(attributes.to_h.merge(contract: contract, property: property))
        return initialize_response.failure(500, [['user', 'cant create ContractPlan for this Agency']]) if current_user.cannot?(:create, contract_plan)

        if contract_plan.save
          initialize_response.success({ contract_plan: contract_plan })
        else
          initialize_response.failure(500, contract_plan.errors)
        end
      end

      private
      
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
