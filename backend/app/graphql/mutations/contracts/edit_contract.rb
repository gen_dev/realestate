module Mutations
  module Contracts
    class EditContract < BaseMutation
      argument :attributes, Types::Contracts::ContractEditInputType, required: true
      argument :contract_id, ID, required: true
      argument :tenant_id, ID, required: false

      field :contract, Types::Contracts::ContractType, null: true
      
      def resolve(attributes: nil, contract_id:, tenant_id: nil)
        return initialize_response.failure(500, [['attributes', 'is required']]) unless attributes

        contract = Contract.includes(:payment_plan, :property).find_by(id: contract_id)

        current_user = context[:current_user]
        return initialize_response.failure(403, [['user', 'is required']]) unless current_user
        return initialize_response.failure(404, [['contract', 'not found']]) unless contract
        return initialize_response.failure(401, [['user', 'unauthorized']]) if current_user.cannot?(:update, contract)

        contract.update(attributes.to_h)
        
        if contract.valid? && tenant_id
          contract.tenant.map do |t|
            t.remove_role :tenant, contract
            t.save
          end
          tenant = User.find_by(id: tenant_id)
          tenant.add_role :tenant, contract
          tenant.save
        end

        contract.save

        contract.property.update(state: 'rented') if contract.valid? && contract.state == 'ongoing'

        return initialize_response.success({ contract: contract }) if contract.valid?
        initialize_response.failure(500, contract.errors)
      end

      private
    
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
