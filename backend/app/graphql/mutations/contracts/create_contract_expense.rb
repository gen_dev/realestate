module Mutations
  module Contracts
    class CreateContractExpense < BaseMutation
      argument :attributes, Types::Contracts::ContractExpenseInputType, required: true
      argument :contract_plan_id, ID, required: true

      field :contract_expense, Types::Contracts::ContractExpenseType, null: true

      def resolve(attributes: nil, contract_plan_id:)
        contract_plan = ContractPlan.find_by(id: contract_plan_id)

        current_user = context[:current_user]

        return initialize_response.failure(403, [['user', 'is required']]) unless current_user
        return initialize_response.failure(404, [['contract_plan', 'not found']]) unless contract_plan
        
        contract_expense = ContractExpense.new(attributes.to_h.merge(contract_plan: contract_plan))
        return initialize_response.failure(500, [['user', 'cant create ContractExpense for this Agency']]) if current_user.cannot?(:create, contract_expense)

        if contract_expense.save
          initialize_response.success({ contract_expense: contract_expense })
        else
          initialize_response.failure(500, contract_expense.errors)
        end
      end

      private
      
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
