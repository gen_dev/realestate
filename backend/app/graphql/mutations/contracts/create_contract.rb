module Mutations
  module Contracts
    class CreateContract < BaseMutation
      argument :attributes, Types::Contracts::ContractInputType, required: true
      argument :property_id, ID, required: true 

      field :contract, Types::Contracts::ContractType, null: true

      def resolve(attributes: nil, property_id:)
        property = Property.find_by(id: property_id)

        current_user = context[:current_user]

        return initialize_response.failure(403, [['user', 'is required']]) unless current_user
        return initialize_response.failure(404, [['property', 'not found']]) unless property
        return initialize_response.failure(404, [['property owner', 'not found']]) unless property.owner.last

        contract = Contract.new(attributes.to_h.merge(property: property))
        return initialize_response.failure(500, [['user', 'cant create Contract for this Property']]) if current_user.cannot?(:create, contract)

        if contract.save
          owners = property.owner
          owners.each do |owner|
            owner.add_role :owner, contract
          end
          initialize_response.success({ contract: contract })
        else
          initialize_response.failure(500, contract.errors)
        end
      end

      private
      
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
