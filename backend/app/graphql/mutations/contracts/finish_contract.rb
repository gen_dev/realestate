module Mutations
  module Contracts
    class FinishContract < BaseMutation
      argument :id, ID, required: true

      field :success, Boolean, null: true
      
      def resolve(id:)
        current_user = context[:current_user]
        return initialize_response.failure(403, [['user', 'is required']]) unless current_user

        contract = Contract.find_by(id: id)

        return initialize_response.failure(404, [['contract', 'not found']]) unless contract

        contract.update(state: 'finished')

        payments = Payment.pending.where(contract_id: id, due_date: 1.month.from_now.beginning_of_month..)
        payments.destroy_all

        contract.property.update(state: 'published')

        return initialize_response.success({ contract: contract }) if contract.valid?
        initialize_response.failure(500, contract.errors)
      end

      private
    
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
