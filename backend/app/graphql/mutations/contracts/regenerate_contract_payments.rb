module Mutations
  module Contracts
    class RegenerateContractPayments < BaseMutation
      argument :id, ID, required: true

      field :success, Boolean, null: true
      
      def resolve(id:)
        current_user = context[:current_user]
        return initialize_response.failure(403, [['user', 'is required']]) unless current_user

        contract = ::Contract.find_by(id: id)

        return initialize_response.failure(404, [['contract', 'not found']]) unless contract

        payments = contract.payments.where(state: 'pending')
        payments.destroy_all

        contract.taxes.map(&:create_payment)
        contract.property.building.taxes.map(&:create_payment) if contract.property.building
        contract.property.taxes.map(&:create_payment)
        contract.payment_plan.taxes.map(&:create_payment)

        initialize_response.success({ success: true })
      end

      private
    
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
