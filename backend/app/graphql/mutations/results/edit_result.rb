module Mutations
  module Results
    class EditResult < BaseMutation
      argument :attributes, Types::Searches::SearchResultInputType, required: true
      argument :id, ID, required: true

      field :success, Boolean, null: true

      def resolve(attributes:, id:)
        current_user = context[:current_user]
        return initialize_response.failure(403, [['user', 'is required']]) unless current_user

        agency = current_user.selected_agency
        return initialize_response.failure(404, [['agency', 'not found']]) unless agency

        result = ::SearchResult.find_by(id: id)
        result.update attributes.to_h

        if result.valid?
          initialize_response.success({ success: true })
        else
          initialize_response.failure(500, result.errors)
        end
      end

      private
      
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
