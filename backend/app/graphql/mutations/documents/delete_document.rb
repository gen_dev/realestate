module Mutations
  module Documents
    class DeleteDocument < BaseMutation
      argument :document_id, ID, required: true

      field :success, Boolean, null: false

      def resolve(document_id:)
        current_user = context[:current_user]
        
        return initialize_response.failure(403, [['user', 'is required']]) unless current_user

        document = Document.find_by(id: document_id)

        return initialize_response.failure(404, [['document', 'not found']]) unless document
        return initialize_response.failure(401, [['user', 'unauthorized']]) if current_user.cannot?(:destroy, document)

        return initialize_response.success({ success: true }) if document.destroy
        initialize_response.failure(500, document.errors)
      end

      private

      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
