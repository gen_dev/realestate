module Mutations
  module Inquiries
    class UpdateInquiryState < BaseMutation
      argument :id, ID, required: true
      argument :state, String, required: true

      field :success, Boolean, null: true

      def resolve(state:, id:)
        inquiry = ::Inquiry.find_by(id: id)

        if inquiry.update(state: state)
          initialize_response.success({ success: true })
        else
          initialize_response.failure(500, user.errors)
        end
      end

      private
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
