module Mutations
  module Inquiries
    class CreateComment < BaseMutation
      argument :attributes, Types::Comments::CommentInputType, required: true

      field :success, Boolean, null: true

      def resolve(attributes:)
        comment = ::Inquiries::Comment.new(attributes.to_h)
        comment.user = context[:current_user]

        if comment.save
          initialize_response.success({ success: true })
        else
          initialize_response.failure(500, comment.errors)
        end
      end

      private
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
