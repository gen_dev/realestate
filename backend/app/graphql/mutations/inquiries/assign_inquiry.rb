module Mutations
  module Inquiries
    class AssignInquiry < BaseMutation
      argument :id, ID, required: true
      argument :email, String, required: true

      field :success, Boolean, null: true

      def resolve(email:, id:)
        inquiry = ::Inquiry.find_by(id: id)

        user = ::User.find_by(email: email)

        if inquiry.update(user: user)
          initialize_response.success({ success: true })
        else
          initialize_response.failure(500, user.errors)
        end
      end

      private
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
