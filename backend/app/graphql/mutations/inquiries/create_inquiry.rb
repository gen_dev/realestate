module Mutations
  module Inquiries
    class CreateInquiry < BaseMutation
      argument :attributes, Types::Inquiries::InquiryInputType, required: true

      field :success, Boolean, null: true

      def resolve(attributes:)
        inquiry = Inquiry.new(attributes.to_h)

        if user = context[:current_user]
          inquiry.user = context[:current_user]
          inquiry.manual = true
        end

        if inquiry.save
          inquiry.update(state: :pending)
          initialize_response.success({ success: true })
        else
          initialize_response.failure(500, inquiry.errors)
        end
      end

      private
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
