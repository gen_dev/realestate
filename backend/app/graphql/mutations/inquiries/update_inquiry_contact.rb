module Mutations
  module Inquiries
    class UpdateInquiryContact < BaseMutation
      argument :id, ID, required: true
      argument :contact_id, ID, required: true

      field :success, Boolean, null: true

      def resolve(contact_id:, id:)
        inquiry = ::Inquiry.find_by(id: id)

        user = ::User.find_by(id: contact_id)

        if inquiry.update(contact: user)
          initialize_response.success({ success: true })
        else
          initialize_response.failure(500, user.errors)
        end
      end

      private
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
