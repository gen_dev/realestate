module Mutations
  module Inquiries
    class DeleteInquiry < BaseMutation
      argument :id, ID, required: true

      field :success, Boolean, null: true

      def resolve(id:)
        inquiry = ::Inquiry.find_by(id: id)

        if inquiry.destroy
          initialize_response.success({ success: true })
        else
          initialize_response.failure(500, inquiry.errors)
        end
      end

      private
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
