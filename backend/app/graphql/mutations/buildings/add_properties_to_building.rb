module Mutations
  module Buildings
    class AddPropertiesToBuilding < BaseMutation
      argument :id, ID, required: true
      argument :property_ids, [ID], required: true

      field :success, Boolean, null: true

      def resolve(id:, property_ids:)
        current_user = context[:current_user]
        return initialize_response.failure(403, [['user', 'is required']]) unless current_user

        building = ::Building.find_by(id: id)

        properties = ::Property.where(id: property_ids)

        properties.update_all(building_id: id)

        building.common_expense.create_payment

        initialize_response.success({ success: true })
      end

      private
      
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
