module Mutations
  module Buildings
    class RemovePropertyFromBuilding < BaseMutation
      argument :id, ID, required: true

      field :success, Boolean, null: true

      def resolve(id:)
        current_user = context[:current_user]
        return initialize_response.failure(403, [['user', 'is required']]) unless current_user

        property = ::Property.find_by(id: id)

        property.update(building_id: nil)

        initialize_response.success({ success: true })
      end

      private
      
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
