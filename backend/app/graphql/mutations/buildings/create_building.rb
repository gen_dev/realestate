module Mutations
  module Buildings
    class CreateBuilding < BaseMutation
      argument :attributes, Types::Buildings::BuildingInputType, required: true

      field :success, Boolean, null: true

      def resolve(attributes:)
        current_user = context[:current_user]
        return initialize_response.failure(403, [['user', 'is required']]) unless current_user

        building = current_user.selected_agency.buildings.create(attributes.to_h)

        if building.save
          initialize_response.success({ success: true })
        else
          initialize_response.failure(500, building.errors)
        end
      end

      private
      
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
