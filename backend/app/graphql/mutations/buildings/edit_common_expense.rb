module Mutations
  module Buildings
    class EditCommonExpense < BaseMutation
      argument :attributes, Types::CommonExpenses::CommonExpenseInputType, required: true
      argument :id, ID, required: true

      field :success, Boolean, null: true
      
      def resolve(attributes:, id:)
        current_user = context[:current_user]

        return initialize_response.failure(403, [['user', 'is required']]) unless current_user

        common_expense = ::Buildings::CommonExpense.find_by(id: id)

        common_expense.update(attributes.to_h)

        return initialize_response.success({ success: true }) if common_expense.valid?
        initialize_response.failure(500, common_expense.errors)
      end

      private
    
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
