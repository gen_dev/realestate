module Mutations
  module Staff
    class EditStaff < BaseMutation
      argument :attributes, Types::Staff::StaffInputType, required: true
      argument :id, ID, required: true

      field :success, Boolean, null: true
      
      def resolve(attributes:, id:)
        return initialize_response.failure(500, [['attributes', 'is required']]) unless attributes

        staff = ::Staff.find_by(id: id)

        current_user = context[:current_user]
        return initialize_response.failure(403, [['user', 'is required']]) unless current_user
        return initialize_response.failure(404, [['staff', 'not found']]) unless staff

        staff.update(attributes.to_h)

        return initialize_response.success({ success: true }) if staff.valid?
        initialize_response.failure(500, staff.errors)
      end

      private
    
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
