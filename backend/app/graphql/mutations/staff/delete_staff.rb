module Mutations
  module Staff
    class DeleteStaff < BaseMutation
      argument :id, ID, required: true

      field :success, Boolean, null: false

      def resolve(id:)
        current_user = context[:current_user]
        
        return initialize_response.failure(403, [['user', 'is required']]) unless current_user

        staff = ::Staff.find_by(id: id)

        return initialize_response.failure(404, [['tax', 'not found']]) unless staff

        if staff.destroy
          staff.clients.update_all(advisor_id: nil)
          staff.inquiries.update_all(user_id: nil)
          staff.user.update deleted_at: Time.now

          return initialize_response.success({ success: true })
        end
        initialize_response.failure(500, staff.errors)
      end

      private

      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
