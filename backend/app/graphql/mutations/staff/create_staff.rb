module Mutations
  module Staff
    class CreateStaff < BaseMutation
      argument :attributes, Types::Staff::StaffInputType, required: true
      argument :user_attributes, Types::Users::UserInputType, required: true
      argument :role, String, required: true

      field :success, Boolean, null: true
      
      def resolve(attributes:, user_attributes:, role:)
        current_user = context[:current_user]
        return initialize_response.failure(403, [['user', 'is required']]) unless current_user
        
        agency = current_user.selected_agency

        user = agency.organization.users.create(user_attributes.to_h)
        user.add_role role

        agency.organization.agencies.each do |ag|
          user.agencies.push(ag)
        end

        user.user_agencies.first.update selected: true

        staff = user.build_staff(attributes.to_h)
        staff.organization = agency.organization
        
        if staff.save
          initialize_response.success({ success: true })
        else
          initialize_response.failure(500, staff.errors)
        end
      end

      private
      
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
