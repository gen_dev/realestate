module Mutations
  module Staff
    class AttachStaff < BaseMutation
      argument :id, ID, required: true
      argument :property_id, ID, required: false
      argument :user_id, ID, required: false

      field :success, Boolean, null: false

      def resolve(id:, property_id: nil, user_id: nil)
        current_user = context[:current_user]
        
        return initialize_response.failure(403, [['user', 'is required']]) unless current_user

        staff = ::Staff.find_by(id: id)

        return initialize_response.failure(404, [['staff', 'not found']]) unless staff
        
        if property_id
          property = ::Property.find_by(id: property_id)
          return initialize_response.failure(404, [['property', 'not found']]) unless property

          return initialize_response.success({ success: true }) if property.staff.push(staff)

          return initialize_response.failure(500, property.errors)
        end

        contact = ::User.find_by(id: user_id)
        return initialize_response.failure(404, [['property', 'not found']]) unless contact

        return initialize_response.success({ success: true }) if staff.clients.push(contact)
        initialize_response.failure(500, staff.errors)
      end

      private

      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
