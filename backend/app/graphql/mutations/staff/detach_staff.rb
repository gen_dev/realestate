module Mutations
  module Staff
    class DetachStaff < BaseMutation
      argument :id, ID, required: true
      argument :property_id, ID, required: true

      field :success, Boolean, null: false

      def resolve(id:, property_id:)
        current_user = context[:current_user]
        
        return initialize_response.failure(403, [['user', 'is required']]) unless current_user

        staff = ::Staff.find_by(id: id)

        return initialize_response.failure(404, [['staff', 'not found']]) unless staff
        
        property = ::Property.find_by(id: property_id)
        return initialize_response.failure(404, [['property', 'not found']]) unless property

        return initialize_response.success({ success: true }) if property.staff.delete(staff)
        initialize_response.failure(500, property.errors)
      end

      private

      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
