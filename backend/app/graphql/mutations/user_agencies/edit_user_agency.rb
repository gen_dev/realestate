module Mutations
  module UserAgencies
    class EditUserAgency < BaseMutation
      argument :attributes, Types::UserAgencies::UserAgencyInputType, required: true
      argument :user_agency_id, ID, required: true

      field :user_agency, Types::UserAgencies::UserAgencyType, null: true
      
      def resolve(attributes: nil, user_agency_id:)
        return initialize_response.failure(500, [['attributes', 'is required']]) unless attributes

        user_agency = UserAgency.find_by(id: user_agency_id)

        current_user = context[:current_user]
        return initialize_response.failure(403, [['user', 'is required']]) unless current_user
        return initialize_response.failure(404, [['user agency', 'not found']]) unless user_agency
        return initialize_response.failure(401, [['user', 'unauthorized']]) if current_user.cannot?(:update, user_agency)

        user_agency.update(attributes.to_h)

        return initialize_response.success({ user_agency: user_agency }) if user_agency.valid?
        initialize_response.failure(500, user_agency.errors)
      end

      private
    
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
