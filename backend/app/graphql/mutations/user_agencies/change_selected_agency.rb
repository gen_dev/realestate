module Mutations
  module UserAgencies
    class ChangeSelectedAgency < BaseMutation
      argument :id, ID, required: true

      field :success, Boolean, null: true

      def resolve(id: nil)
        current_user = context[:current_user]

        return initialize_response.failure(403, [['current_user', 'is required']]) unless current_user
        
        user_agency = current_user.user_agencies.find_by id: id

        return initialize_response.failure(404, [['user_agency', 'not found']]) unless user_agency

        current_user.user_agencies.update_all(selected: false)
        user_agency.update(selected: true)

        { success: true }
      end

      private
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
