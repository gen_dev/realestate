module Mutations
  module UserAgencies
    class CreateUserAgency < BaseMutation
      argument :selected, Boolean, required: false
      argument :agency_id, ID, required: true
      argument :user_id, ID, required: false

      field :user_agency, Types::UserAgencies::UserAgencyType, null: true

      def resolve(selected: true, agency_id:, user_id: nil)
        agency = Agency.find_by(id: agency_id)
        current_user = context[:current_user]

        user = User.find_by(id: user_id) || current_user

        return initialize_response.failure(403, [['current_user', 'is required']]) unless current_user
        return initialize_response.failure(404, [['user', 'not found']]) unless user
        return initialize_response.failure(404, [['agency', 'not found']]) unless agency
        
        user_agency = UserAgency.new(selected: selected, agency: agency, user: user)
        return initialize_response.failure(500, [['current_user', 'cant create UserAgency for this Agency']]) if current_user.cannot?(:create, user_agency)

        if user_agency.save
          user.add_role :admin, agency
          initialize_response.success({ user_agency: user_agency })
        else
          initialize_response.failure(500, user_agency.errors)
        end
      end

      private
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
