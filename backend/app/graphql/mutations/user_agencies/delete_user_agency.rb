module Mutations
  module UserAgencies
    class DeleteUserAgency < BaseMutation
      argument :user_agency_id, ID, required: true

      field :success, Boolean, null: false

      def resolve(user_agency_id:)
        current_user = context[:current_user]
        
        return initialize_response.failure(403, [['user', 'is required']]) unless current_user

        user_agency = UserAgency.find_by(id: user_agency_id)

        return initialize_response.failure(404, [['user agency', 'not found']]) unless user_agency
        return initialize_response.failure(401, [['user', 'unauthorized']]) if current_user.cannot?(:destroy, user_agency)

        return initialize_response.success({ success: true }) if user_agency.destroy
        initialize_response.failure(500, user_agency.errors)
      end

      private

      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
