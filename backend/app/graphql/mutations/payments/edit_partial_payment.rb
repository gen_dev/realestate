module Mutations
  module Payments
    class EditPartialPayment < BaseMutation
      argument :attributes, Types::Payments::PartialPaymentInputType, required: true
      argument :partial_payment_id, ID, required: true

      field :partial_payment, Types::Payments::PartialPaymentType, null: true
      
      def resolve(attributes: nil, partial_payment_id:)
        return initialize_response.failure(500, [['attributes', 'is required']]) unless attributes

        partial_payment = PartialPayment.find_by(id: partial_payment_id)

        current_user = context[:current_user]
        return initialize_response.failure(403, [['user', 'is required']]) unless current_user
        return initialize_response.failure(404, [['partial payment', 'not found']]) unless partial_payment
        return initialize_response.failure(401, [['user', 'unauthorized']]) if current_user.cannot?(:update, partial_payment)

        partial_payment.update(attributes.to_h)

        return initialize_response.success({ partial_payment: partial_payment }) if partial_payment.valid?
        initialize_response.failure(500, partial_payment.errors)
      end

      private
    
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
