module Mutations
  module Payments
    class CreatePayment < BaseMutation
      argument :attributes, Types::Payments::PaymentInputType, required: true

      field :payment, Types::Payments::PaymentType, null: true

      def resolve(attributes: nil)
        current_user = context[:current_user]

        return initialize_response.failure(403, [['user', 'is required']]) unless current_user

        attributes = attributes.to_h

        attributes[:referral_id] ||= current_user.selected_agency.id

        payment = Payment.new(attributes)

        return initialize_response.failure(500, [['user', 'cant create Payment for this SubAccount or Referral']]) if current_user.cannot?(:create, payment)

        if payment.save
          payment.update(paid_at: payment.paid_at + 3.hours)
          initialize_response.success({ payment: payment })
        else
          initialize_response.failure(500, payment.errors)
        end
      end

      private
      
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
