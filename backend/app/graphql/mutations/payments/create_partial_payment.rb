module Mutations
  module Payments
    class CreatePartialPayment < BaseMutation
      argument :value, ID, required: true
      argument :payment_id, ID, required: true
      argument :receipt_type, String, required: false

      field :partial_payment, Types::Payments::PartialPaymentType, null: true

      def resolve(value:, payment_id:, receipt_type: 'Owner')
        payment = Payment.find_by(id: payment_id)

        current_user = context[:current_user]
        return initialize_response.failure(403, [['user', 'is required']]) unless current_user
        return initialize_response.failure(404, [['payment', 'not found']]) unless payment

        partial_payment = payment.children.new(payment.dup.attributes)
        return initialize_response.failure(500, [['user', 'cant create PartialPayment for this Payment']]) if current_user.cannot?(:create, partial_payment)

        time = (Time.now - 3.hours)

        partial_payment.amount = value
        partial_payment.parent = payment
        partial_payment.state = 'paid'
        partial_payment.paid_at = time

        if partial_payment.save
          if payment.lasting_amount.zero?
            new_state = 'paid'
            time = new_state == 'paid' ? (Time.now - 3.hours) : nil
            payment.update(state: new_state, paid_at: time)

            if payment.sub_account.name == 'Alquileres pagados'
              tax = payment.tax
              update_sibling(payment, payment.due_date, tax, new_state, receipt_type)
            end
          end

          set_receipt_id(partial_payment, receipt_type) if payment.contract

          initialize_response.success({ partial_payment: partial_payment })
        else
          initialize_response.failure(500, partial_payment.errors)
        end
      end

      private
      
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end

      def update_sibling(payment, due_date, tax, new_state, receipt_type)
        time = new_state == 'paid' ? (Time.now - 3.hours) : nil
        sibling = tax.payments.joins(:sub_account).where.not(id: payment.id).find_by('sub_accounts.name = ? AND due_date BETWEEN ? AND ?', 'Honorarios administración alquiler', due_date.beginning_of_month, due_date.end_of_month)
        return unless sibling
        sibling.update(state: new_state, paid_at: time)
        set_receipt_id(sibling, receipt_type) if sibling.contract
      end

      def set_receipt_id(payment, receipt_type, user = nil)
        if payment.state == 'paid'
          stamp = "#{payment.paid_at.year}#{payment.paid_at.month}"
          agency = context[:current_user].selected_agency
          last_receipt = agency.receipts.find_or_create_by(state: 'pending', user_id: user || payment.contract.send(receipt_type.downcase).last.id, receipt_type: receipt_type, stamp: stamp)
          payment.receipt = last_receipt

          last_receipt.set_payment_types
        else
          payment.receipt = nil
        end
        payment.save
      end
    end
  end
end
