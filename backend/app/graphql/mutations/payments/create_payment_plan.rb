module Mutations
  module Payments
    class CreatePaymentPlan < BaseMutation
      argument :attributes, Types::Payments::PaymentPlanInputType, required: true
      argument :contract_id, ID, required: false

      field :payment_plan, Types::Payments::PaymentPlanType, null: true

      def resolve(attributes: nil, contract_id:)
        contract = Contract.find_by(id: contract_id)

        current_user = context[:current_user]
        return initialize_response.failure(403, [['user', 'is required']]) unless current_user
        return initialize_response.failure(404, [['contract', 'not found']]) unless contract

        payment_plan = PaymentPlan.new(attributes.to_h.merge(contract: contract))
        return initialize_response.failure(500, [['user', 'cant create PaymentPlan for this Contract']]) if current_user.cannot?(:create, payment_plan)

        if payment_plan.save
          initialize_response.success({ payment_plan: payment_plan })
        else
          initialize_response.failure(500, payment_plan.errors)
        end
      end

      private
      
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
