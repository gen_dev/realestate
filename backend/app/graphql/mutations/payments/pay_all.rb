module Mutations
  module Payments
    class PayAll < BaseMutation
      argument :payment_ids, [ID], required: true
      argument :set_receipt, Boolean, required: false
      argument :receipt_type, String, required: false

      field :success, Boolean, null: true

      def resolve(payment_ids:, set_receipt: false, receipt_type: 'Owner')
        payments = Payment.where(id: payment_ids)

        current_user = context[:current_user]
        return initialize_response.failure(403, [['user', 'is required']]) unless current_user
        return initialize_response.failure(404, [['payment', 'not found']]) unless payments.any?

        payments.each do |payment|
          new_state = payment.state == 'paid' ? 'pending' : 'paid'

          if new_state == 'paid' && payment.partial_payments.any?
            partial_payment = payment.children.new(payment.dup.attributes)
            partial_payment.amount = payment.lasting_amount.abs
            partial_payment.payment = payment
            partial_payment.save
          elsif new_state == 'pending' && payment.partial_payments.any?
            payment.partial_payments.last.delete
          end

          if payment.advanced && new_state == 'pending'
            payment.update(advanced: false)
          end

          time = new_state == 'paid' ? (Time.now - 3.hours) : nil
          payment.update(state: new_state, paid_at: time)

          if payment.valid?
            if payment.sub_account.name == 'Alquileres pagados'
              tax = payment.tax
              update_sibling(payment.due_date, tax, new_state, set_receipt, receipt_type)
            end

            if payment.contract
              if (payment.contract.tenant.include?(payment.referral) || payment.contract.tenant.include?(payment.receiver))
                receipt_type = 'tenant'
              elsif payment.contract.owner.include?(payment.referral) || payment.contract.owner.include?(payment.receiver)
                receipt_type = 'owner'
              else
                next
              end
            end

            set_receipt_id(payment, receipt_type) 
          end
        end

        return initialize_response.success({ success: true }) 
      end

      private
    
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end

      def update_sibling(due_date, tax, new_state, set_receipt, receipt_type)
        time = new_state == 'paid' ? (Time.now - 3.hours) : nil
        sibling = tax.payments.joins(:sub_account).find_by('sub_accounts.name = ? AND due_date BETWEEN ? AND ?', 'Honorarios administración alquiler', due_date.beginning_of_month, due_date.end_of_month)
        return unless sibling
        sibling.update(state: new_state, paid_at: time)
        set_receipt_id(sibling, receipt_type) if sibling.contract
      end

      def set_receipt_id(payment, receipt_type)
        stamp = "#{payment.due_date.year}#{payment.due_date.month}"
        if payment.state == 'paid'
          agency = context[:current_user].selected_agency
          if (payment.tax && payment.tax.taxable_type == 'User')
            last_receipt = agency.receipts.find_or_create_by(state: 'pending', user_id: payment.tax.taxable_id, receipt_type: receipt_type, stamp: stamp)
          else
            last_receipt = agency.receipts.find_or_create_by(state: 'pending', user_id: payment.contract.send(receipt_type.downcase).last.id, receipt_type: receipt_type, stamp: stamp)
          end
          payment.receipt = last_receipt

          last_receipt.set_payment_types
        else
          payment.receipt = nil
        end
        payment.save
      end
    end
  end
end
