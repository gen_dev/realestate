module Mutations
  module Payments
    class DeletePaymentPlan < BaseMutation
      argument :payment_plan_id, ID, required: true

      field :success, Boolean, null: false

      def resolve(payment_plan_id:)
        current_user = context[:current_user]
        
        return initialize_response.failure(403, [['user', 'is required']]) unless current_user

        payment_plan = PaymentPlan.find_by(id: payment_plan_id)

        return initialize_response.failure(404, [['payment plan', 'not found']]) unless payment_plan
        return initialize_response.failure(401, [['user', 'unauthorized']]) if current_user.cannot?(:destroy, payment_plan)

        return initialize_response.success({ success: true }) if payment_plan.destroy
        initialize_response.failure(500, payment_plan.errors)
      end

      private

      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
