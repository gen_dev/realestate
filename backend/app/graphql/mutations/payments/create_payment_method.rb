module Mutations
  module Payments
    class CreatePaymentMethod < BaseMutation
      argument :receipt_id, ID, required: true
      argument :name, String, required: true
      argument :currency, String, required: false
      argument :amount, String, required: true

      field :success, Boolean, null: true

      def resolve(receipt_id:, name:, amount:, currency: "ARS")
        current_user = context[:current_user]
        return initialize_response.failure(403, [['user', 'is required']]) unless current_user

        receipt = ::Receipt.find_by(id: receipt_id)
        return initialize_response.failure(404, [['receipt', 'not found']]) unless receipt

        payment_type = receipt.payment_types.find_or_create_by(name: name, currency: currency)

        payment_type.update(amount: amount)

        if payment_type.save
          initialize_response.success({ success: true })
        else
          initialize_response.failure(500, payment_type.errors)
        end
      end

      private
      
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
