module Mutations
  module Payments
    class DeletePayment < BaseMutation
      argument :payment_id, ID, required: true

      field :success, Boolean, null: false

      def resolve(payment_id:)
        current_user = context[:current_user]
        
        return initialize_response.failure(403, [['user', 'is required']]) unless current_user

        payment = Payment.find_by(id: payment_id)

        return initialize_response.failure(404, [['payment', 'not found']]) unless payment
        return initialize_response.failure(401, [['user', 'unauthorized']]) if current_user.cannot?(:destroy, payment)

        return initialize_response.success({ success: true }) if payment.destroy
        initialize_response.failure(500, payment.errors)
      end

      private

      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
