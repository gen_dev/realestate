module Mutations
  module Payments
    class Pay < BaseMutation
      argument :payment_id, ID, required: true
      argument :set_receipt, Boolean, required: false
      argument :receipt_type, String, required: false

      field :payment, Types::Payments::PaymentType, null: true
      
      def resolve(payment_id:, set_receipt: false, receipt_type: 'Owner')
        payment = Payment.find_by(id: payment_id)

        current_user = context[:current_user]
        return initialize_response.failure(403, [['user', 'is required']]) unless current_user
        return initialize_response.failure(404, [['payment', 'not found']]) unless payment
        return initialize_response.failure(401, [['user', 'unauthorized']]) if current_user.cannot?(:update, payment)

        new_state = payment.state == 'paid' ? 'pending' : 'paid'

        if new_state == 'paid' && payment.children.any?
          time = new_state == 'paid' ? (Time.now - 3.hours) : nil

          partial_payment = payment.children.new(payment.dup.attributes)
          partial_payment.amount = payment.lasting_amount.abs
          partial_payment.parent = payment
          partial_payment.state = 'paid'
          partial_payment.paid_at = time
          partial_payment.save
        elsif new_state == 'pending' && payment.children.any?
          payment.children.last.destroy
        end

        if payment.advanced && new_state == 'pending'
          payment.update(advanced: false)
        end

        time = new_state == 'paid' ? (Time.now - 3.hours) : nil
        payment.update(state: new_state, paid_at: time)

        if payment.valid?
          if payment.sub_account.name == 'Alquileres pagados'
            tax = payment.tax
            update_sibling(payment.due_date, tax, new_state, set_receipt, receipt_type)
          end

          if payment.contract
            if (payment.contract.tenant.include?(payment.referral) || payment.contract.tenant.include?(payment.receiver))
              receipt_type = 'tenant'
            elsif payment.contract.owner.include?(payment.referral) || payment.contract.owner.include?(payment.receiver)
              receipt_type = 'owner'
            else
              return initialize_response.success({ payment: payment }) 
            end

            set_receipt_id(partial_payment || payment, receipt_type) 
          end

          if payment.tax.taxable_type == 'User'
            set_receipt_id(partial_payment || payment, 'owner', payment.tax.taxable) 
          end

          return initialize_response.success({ payment: payment }) 
        end

        initialize_response.failure(500, payment.errors)
      end

      private
    
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end

      def update_sibling(due_date, tax, new_state, set_receipt, receipt_type)
        time = new_state == 'paid' ? (Time.now - 3.hours) : nil
        sibling = tax.payments.joins(:sub_account).find_by('sub_accounts.name = ? AND due_date BETWEEN ? AND ?', 'Honorarios administración alquiler', due_date.beginning_of_month, due_date.end_of_month)
        return unless sibling
        sibling.update(state: new_state, paid_at: time)
        set_receipt_id(sibling, receipt_type) if sibling.contract
      end

      def set_receipt_id(payment, receipt_type, user = nil)
        if payment.state == 'paid'
          stamp = "#{payment.paid_at.year}#{payment.paid_at.month}"
          agency = context[:current_user].selected_agency
          last_receipt = agency.receipts.find_or_create_by(state: 'pending', user_id: user ? user.id : payment.contract.send(receipt_type.downcase).last.id, receipt_type: receipt_type, stamp: stamp)
          payment.receipt = last_receipt

          last_receipt.set_payment_types
        else
          payment.receipt = nil
        end
        payment.save
      end
    end
  end
end
