module Mutations
  module Payments
    class EditPayment < BaseMutation
      argument :attributes, Types::Payments::PaymentInputType, required: true
      argument :id, ID, required: true

      field :success, Boolean, null: true
      
      def resolve(attributes:, id:)
        payment = Payment.find_by(id: id)

        current_user = context[:current_user]

        return initialize_response.failure(403, [['user', 'is required']]) unless current_user
        return initialize_response.failure(404, [['payment', 'not found']]) unless payment
        return initialize_response.failure(401, [['user', 'unauthorized']]) if current_user.cannot?(:update, payment)

        payment.update(attributes.to_h)

        return initialize_response.success({ success: true }) if payment.valid?
        initialize_response.failure(500, payment.errors)
      end

      private
    
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
