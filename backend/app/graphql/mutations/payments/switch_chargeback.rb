module Mutations
  module Payments
    class SwitchChargeback < BaseMutation
      argument :id, ID, required: true

      field :success, Boolean, null: true
      
      def resolve(id:)
        current_user = context[:current_user]

        return initialize_response.failure(403, [['user', 'is required']]) unless current_user

        charge = ::Payments::Chargeback.find_by(id: id)

        return initialize_response.failure(404, [['chargeback', 'not_found']]) unless charge

        charge.active = !charge.active

        payment = charge.payment
        tax = payment.tax

        if tax
          sibling = tax.payments.joins(:sub_account).find_by('sub_accounts.name = ? AND NOT payments.id = ? AND due_date BETWEEN ? AND ?', 'Honorarios administración alquiler', payment.id, payment.due_date.beginning_of_month, payment.due_date.end_of_month)
          if sibling
            sibling_chargeback = sibling.chargebacks.last
            if sibling_chargeback
              sibling_chargeback.active = charge.active
              sibling_chargeback.save
            end
          end
        end

        { success: charge.save }
      end
    end
  end
end
