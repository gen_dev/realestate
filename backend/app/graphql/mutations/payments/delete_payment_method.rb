module Mutations
  module Payments
    class DeletePaymentMethod < BaseMutation
      argument :id, ID, required: true

      field :success, Boolean, null: true

      def resolve(id:)
        current_user = context[:current_user]
        return initialize_response.failure(403, [['user', 'is required']]) unless current_user

        payment_type = ::PaymentType.find(id)

        if payment_type.destroy
          initialize_response.success({ success: true })
        else
          initialize_response.failure(500, payment_type.errors)
        end
      end

      private
      
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
