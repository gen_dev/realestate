module Mutations
  module Payments
    class DeletePartialPayment < BaseMutation
      argument :partial_payment_id, ID, required: true

      field :success, Boolean, null: false

      def resolve(partial_payment_id:)
        current_user = context[:current_user]
        
        return initialize_response.failure(403, [['user', 'is required']]) unless current_user

        partial_payment = ::Payment.unscoped.find_by(id: partial_payment_id)

        payment = partial_payment.parent

        return initialize_response.failure(404, [['partial payment', 'not found']]) unless partial_payment
        return initialize_response.failure(401, [['user', 'unauthorized']]) if current_user.cannot?(:destroy, partial_payment)

        if partial_payment.destroy
          payment.update state: 'pending', paid_at: nil

          if payment.sub_account.name == 'Alquileres pagados'
            tax = payment.tax
            update_sibling(payment.paid_at, tax, 'pending', receipt_type)
          end

          return initialize_response.success({ success: true }) 
        end
        initialize_response.failure(500, partial_payment.errors)
      end

      private

      def initialize_response
        ResponseService.new(self.field&.name, context)
      end

      def update_sibling(due_date, tax, new_state, receipt_type)
        time = new_state == 'paid' ? (Time.now - 3.hours) : nil
        sibling = tax.payments.joins(:sub_account).find_by('sub_accounts.name = ? AND due_date BETWEEN ? AND ?', 'Honorarios administración alquiler', due_date.beginning_of_month, due_date.end_of_month)
        return unless sibling
        sibling.update(state: new_state, paid_at: time)
        set_receipt_id(sibling, receipt_type) if sibling.contract
      end

      def set_receipt_id(payment, receipt_type)
        if payment.state == 'paid'
          stamp = "#{payment.paid_at.year}#{payment.paid_at.month}"
          agency = context[:current_user].selected_agency
          last_receipt = agency.receipts.find_or_create_by(state: 'pending', user_id: user || payment.contract.send(receipt_type.downcase).last.id, receipt_type: receipt_type, stamp: stamp)
          payment.receipt = last_receipt
        else
          payment.receipt = nil
        end
        payment.save
      end
    end
  end
end
