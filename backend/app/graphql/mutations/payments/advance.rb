module Mutations
  module Payments
    class Advance < BaseMutation
      argument :payment_id, ID, required: true
      argument :set_receipt, Boolean, required: false
      argument :receipt_type, String, required: false

      field :payment, Types::Payments::PaymentType, null: true
      
      def resolve(payment_id:, set_receipt: false, receipt_type: 'Owner')
        payment = Payment.find_by(id: payment_id)

        current_user = context[:current_user]
        return initialize_response.failure(403, [['user', 'is required']]) unless current_user
        return initialize_response.failure(404, [['payment', 'not found']]) unless payment
        return initialize_response.failure(401, [['user', 'unauthorized']]) if current_user.cannot?(:update, payment)

        payment.update(advanced: !payment.advanced)
        new_state = payment.advanced ? 'paid' : 'pending'

        if new_state == 'paid' && payment.partial_payments.any?
          payment.partial_payments.create!(value: payment.lasting_amount.abs)
        elsif new_state == 'pending' && payment.partial_payments.any?
          payment.partial_payments.last.delete
        end

        if payment.advanced && new_state == 'pending'
          payment.update(advanced: false)
        end

        time = new_state == 'paid' ? Time.now : nil
        payment.update(state: new_state, paid_at: time)

        if payment.valid?
          if payment.sub_account.name == 'Alquileres pagados'
            tax = payment.tax
            update_sibling(tax, new_state, set_receipt, receipt_type)
          end

          set_receipt_id(payment, receipt_type) if set_receipt && payment.contract
          return initialize_response.success({ payment: payment }) 
        end

        initialize_response.failure(500, payment.errors)
      end

      private
    
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end

      def update_sibling(tax, new_state, set_receipt, receipt_type)
        time = new_state == 'paid' ? (Time.now - 3.hours) : nil
        sibling = tax.payments.joins(:sub_account).find_by('sub_accounts.name = ?', 'Honorarios administración alquiler')
        return unless sibling
        sibling.update(state: new_state, paid_at: time)
        set_receipt_id(sibling, receipt_type) if sibling.contract
      end

      def set_receipt_id(payment, receipt_type)
        if payment.state == 'paid'
          agency = context[:current_user].selected_agency
          last_receipt = agency.receipts.find_or_create_by(state: 'pending', user_id: payment.contract.send(receipt_type).last.id, receipt_type: receipt_type)
          payment.receipt = last_receipt
        else
          payment.receipt = nil
        end
        payment.save
      end
    end
  end
end
