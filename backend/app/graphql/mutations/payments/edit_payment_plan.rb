module Mutations
  module Payments
    class EditPaymentPlan < BaseMutation
      argument :attributes, Types::Payments::PaymentPlanInputType, required: true
      argument :payment_plan_id, ID, required: true

      field :payment_plan, Types::Payments::PaymentPlanType, null: true
      
      def resolve(attributes: nil, payment_plan_id:)
        return initialize_response.failure(500, [['attributes', 'is required']]) unless attributes

        payment_plan = PaymentPlan.find_by(id: payment_plan_id)

        current_user = context[:current_user]
        return initialize_response.failure(403, [['user', 'is required']]) unless current_user
        return initialize_response.failure(404, [['payment plan', 'not found']]) unless payment_plan
        return initialize_response.failure(401, [['user', 'unauthorized']]) if current_user.cannot?(:update, payment_plan)

        payment_plan.update(attributes.to_h)

        return initialize_response.success({ payment_plan: payment_plan }) if payment_plan.valid?
        initialize_response.failure(500, payment_plan.errors)
      end

      private
    
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
