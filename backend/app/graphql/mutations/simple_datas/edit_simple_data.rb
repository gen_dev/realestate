module Mutations
  module SimpleDatas
    class EditSimpleData < BaseMutation
      argument :attributes, Types::SimpleDatas::SimpleDataInputType, required: true
      argument :simple_data_id, ID, required: true

      field :simple_data, Types::SimpleDatas::SimpleDataType, null: true
      
      def resolve(attributes: nil, simple_data_id:)
        return initialize_response.failure(500, [['attributes', 'is required']]) unless attributes

        simple_data = SimpleData.find_by(id: simple_data_id)

        current_user = context[:current_user]
        return initialize_response.failure(403, [['user', 'is required']]) unless current_user
        return initialize_response.failure(404, [['simple data', 'not found']]) unless simple_data
        return initialize_response.failure(401, [['user', 'unauthorized']]) if current_user.cannot?(:manage, simple_data.datable)

        simple_data.update(attributes.to_h)

        return initialize_response.success({ simple_data: simple_data }) if simple_data.valid?
        initialize_response.failure(500, simple_data.errors)
      end

      private
    
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
