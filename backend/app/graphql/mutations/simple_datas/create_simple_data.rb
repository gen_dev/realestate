module Mutations
  module SimpleDatas
    class CreateSimpleData < BaseMutation
      argument :attributes, Types::SimpleDatas::SimpleDataInputType, required: true
      argument :datable_id, ID, required: true
      argument :datable_type, String, required: true

      field :simple_data, Types::SimpleDatas::SimpleDataType, null: true
      
      def resolve(attributes: nil, datable_id:, datable_type:)
        datable = datable_type.constantize.find_by(id: datable_id)

        current_user = context[:current_user]
        return initialize_response.failure(403, [['user', 'is required']]) unless current_user
        return initialize_response.failure(404, [['datable', 'not found']]) unless datable
        
        simple_data = SimpleData.new(attributes.to_h.merge(datable: datable))
        return initialize_response.failure(500, [['user', "cant create simple data for this #{datable_type}"]]) if current_user.cannot?(:manage, datable)
        
        if simple_data.save
          initialize_response.success({ simple_data: simple_data })
        else
          initialize_response.failure(500, simple_data.errors)
        end
      end

      private
      
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
