module Mutations
  module SimpleDatas
    class DeleteSimpleData < BaseMutation
      argument :simple_data_id, ID, required: true

      field :success, Boolean, null: false

      def resolve(simple_data_id:)
        current_user = context[:current_user]
        
        return initialize_response.failure(403, [['user', 'is required']]) unless current_user

        simple_data = SimpleData.find_by(id: simple_data_id)

        return initialize_response.failure(404, [['simple data', 'not found']]) unless simple_data
        return initialize_response.failure(401, [['user', 'unauthorized']]) if current_user.cannot?(:manage, simple_data.datable)

        return initialize_response.success({ success: true }) if simple_data.destroy
        initialize_response.failure(500, simple_data.errors)
      end

      private

      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
