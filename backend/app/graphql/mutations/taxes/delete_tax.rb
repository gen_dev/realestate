module Mutations
  module Taxes
    class DeleteTax < BaseMutation
      argument :id, ID, required: true

      field :success, Boolean, null: false

      def resolve(id:)
        current_user = context[:current_user]
        
        return initialize_response.failure(403, [['user', 'is required']]) unless current_user

        tax = Tax.find_by(id: id)

        return initialize_response.failure(404, [['tax', 'not found']]) unless tax

        return initialize_response.success({ success: true }) if tax.destroy
        initialize_response.failure(500, tax.errors)
      end

      private

      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
