module Mutations
  module Taxes
    class EditTax < BaseMutation
      argument :attributes, Types::Taxes::TaxInputType, required: true
      argument :tax_id, ID, required: true

      field :tax, Types::Taxes::TaxType, null: true
      
      def resolve(attributes: nil, tax_id:)
        return initialize_response.failure(500, [['attributes', 'is required']]) unless attributes

        tax = Tax.find_by(id: tax_id)

        current_user = context[:current_user]
        return initialize_response.failure(403, [['user', 'is required']]) unless current_user
        return initialize_response.failure(404, [['tax', 'not found']]) unless tax
        return initialize_response.failure(401, [['user', 'unauthorized']]) if current_user.cannot?(:update, tax)

        tax.update(attributes.to_h)

        return initialize_response.success({ tax: tax }) if tax.valid?
        initialize_response.failure(500, tax.errors)
      end

      private
    
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
