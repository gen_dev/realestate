module Mutations
  module Taxes
    class CreateTax < BaseMutation
      argument :attributes, Types::Taxes::TaxInputType, required: true

      field :tax, Types::Taxes::TaxType, null: true
      
      def resolve(attributes: nil)
        current_user = context[:current_user]
        return initialize_response.failure(403, [['user', 'is required']]) unless current_user
        
        tax = Tax.new(attributes.to_h)
        return initialize_response.failure(500, [['user', 'cant create Tax']]) unless current_user.can?(:create, tax)
        
        if tax.save
          initialize_response.success({ tax: tax })
        else
          initialize_response.failure(500, tax.errors)
        end
      end

      private
      
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
