module Mutations
  module Tickets
    class DeleteTicket < BaseMutation
      argument :id, ID, required: true

      field :success, Boolean, null: false

      def resolve(id:)
        current_user = context[:current_user]

        return initialize_response.failure(403, [['current_user', 'is required']]) unless current_user
        return initialize_response.failure(403, [['selected_agency', 'is required']]) unless current_user.selected_agency

        ticket = ::Ticket.find_by(id: id)

        return initialize_response.failure(404, [['ticket', 'not found']]) unless ticket

        if ticket.delete
          initialize_response.success({ success: true })
        else
          initialize_response.failure(500, ticket.errors)
        end
      end

      private
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
