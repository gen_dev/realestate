module Mutations
  module Tickets
    class EditTicket < BaseMutation
      argument :id, ID, required: true
      argument :attributes, Types::Tickets::TicketInputType, required: true

      field :ticket, Types::Tickets::TicketType, null: true

      def resolve(attributes:, id:)
        current_user = context[:current_user]

        return initialize_response.failure(403, [['current_user', 'is required']]) unless current_user
        return initialize_response.failure(403, [['selected_agency', 'is required']]) unless current_user.selected_agency

        ticket = ::Ticket.find_by(id: id)

        return initialize_response.failure(404, [['ticket', 'not found']]) unless ticket

        ticket.update(attributes.to_h)

        if ticket.save
          initialize_response.success({ ticket: ticket })
        else
          initialize_response.failure(500, ticket.errors)
        end
      end

      private
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
