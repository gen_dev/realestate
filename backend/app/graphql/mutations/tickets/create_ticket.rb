module Mutations
  module Tickets
    class CreateTicket < BaseMutation
      argument :attributes, Types::Tickets::TicketInputType, required: true

      field :ticket, Types::Tickets::TicketType, null: true

      def resolve(attributes:)
        current_user = context[:current_user]

        return initialize_response.failure(403, [['current_user', 'is required']]) unless current_user
        return initialize_response.failure(403, [['selected_agency', 'is required']]) unless current_user.selected_agency

        ticket = ::Ticket.new(attributes.to_h)

        if ticket.save
          initialize_response.success({ ticket: ticket })
        else
          initialize_response.failure(500, ticket.errors)
        end
      end

      private
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
