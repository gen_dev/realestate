module Mutations
  module Rescissions
    class EditRescission < BaseMutation
      argument :id, ID, required: true
      argument :attributes, Types::Rescissions::RescissionInputType, required: true

      field :success, Boolean, null: true

      def resolve(attributes:, id:)
        current_user = context[:current_user]

        return initialize_response.failure(403, [['current_user', 'is required']]) unless current_user
        return initialize_response.failure(403, [['selected_agency', 'is required']]) unless current_user.selected_agency

        rescission = ::Rescission.find_by(id: id)
        rescission.update(attributes.to_h)
        rescission.receipt.agency = current_user.selected_agency

        if rescission.rent && rescission.state == "confirmed"
          rescission.create_payments

          contract = rescission.contract

          payments = Payment.pending.where(contract_id: id, due_date: 1.month.from_now.beginning_of_month..)

          payments.destroy_all

          contract.update(state: 'finished')
          contract.property.update(state: 'published')
        end

        if rescission.receipt.save
          rescission.receipt.payments.push(rented)

          { success: true }
        else
          initialize_response.failure(500, rescission.errors)
        end
      rescue
      end

      private
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
