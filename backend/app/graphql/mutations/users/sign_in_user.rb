module Mutations
  module Users
    class SignInUser < BaseMutation
      null true

      argument :credentials, Types::AuthProviderCredentialsInput, required: false
      argument :token, String, required: false

      field :token, String, null: true
      field :user, Types::Users::UserType, null: true

      def resolve(credentials: nil, token: nil)
        return sign_in_with_token(token) if token
        return unless credentials

        user = User.find_by email: credentials[:email]

        return initialize_response.failure(404, [['user', 'not found']]) unless user
        return initialize_response.failure(404, [['user', 'deleted']]) if user.deleted_at
        return initialize_response.failure(401, [['password', 'did not match']]) unless user.authenticate(credentials[:password])

        crypt = ActiveSupport::MessageEncryptor.new(Rails.application.credentials.secret_key_base.byteslice(0..31))
        token = crypt.encrypt_and_sign("user-id:#{ user.id }")

        context[:session][:token] = token

        Thread.new do |t|
          sleep 90

          user.update(changelog_read: true)
        end

        initialize_response.success({ user: user, token: token })
      end

      private
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end

      def sign_in_with_token(token)
        crypt = ActiveSupport::MessageEncryptor.new(Rails.application.credentials.secret_key_base.byteslice(0..31))
        decrypted_token = crypt.decrypt_and_verify token
        user_id = decrypted_token.gsub('user-id:', '').to_i
        user = User.find_by id: user_id

        context[:session][:token] = token

        Thread.new do |t|
          sleep 1

          user.update!(changelog_read: true)
        end

        initialize_response.success({ user: user, token: token })
      rescue ActiveSupport::MessageEncryptor::InvalidMessage
        nil
      end

    end
  end
end
