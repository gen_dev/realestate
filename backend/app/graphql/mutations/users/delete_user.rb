module Mutations
  module Users
    class DeleteUser < BaseMutation
      argument :user_id, ID, required: true

      field :success, Boolean, null: false

      def resolve(user_id:)
        current_user = context[:current_user]
        
        return initialize_response.failure(403, [['current user', 'is required']]) unless current_user

        user = User.find_by(id: user_id)

        return initialize_response.failure(404, [['user', 'not found']]) unless user
        return initialize_response.failure(401, [['current user', 'unauthorized']]) if current_user.cannot?(:destroy, user)

        return initialize_response.success({ success: true }) if user.destroy
        initialize_response.failure(500, user.errors)
      end

      private

      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
