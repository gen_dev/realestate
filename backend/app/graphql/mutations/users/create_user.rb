module Mutations
  module Users
    class CreateUser < BaseMutation
      argument :credentials, Types::AuthProviderCredentialsInput, required: false

      field :user, Types::Users::UserType, null: true

      def resolve(credentials: nil)
        return initialize_response.failure(500, [['credentials', 'required']]) unless credentials 
        
        user = ::User.create(
          email: credentials&.[](:email),
          password: credentials&.[](:password)
        )

        if user.save
          initialize_response.success({ user: user })
        else
          initialize_response.failure(500, user.errors)
        end
      end

      private
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
