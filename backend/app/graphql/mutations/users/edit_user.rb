module Mutations
  module Users
    class EditUser < BaseMutation
      argument :attributes, Types::Users::UserInputType, required: true
      argument :id, ID, required: true
      argument :password, String, required: false

      field :success, Boolean, null: true
      
      def resolve(attributes:, id:, password: nil)
        user = User.find_by(id: id)

        current_user = context[:current_user]
        return initialize_response.failure(403, [['current user', 'is required']]) unless current_user
        return initialize_response.failure(404, [['user', 'not found']]) unless user
        return initialize_response.failure(401, [['current user', 'unauthorized']]) if current_user.cannot?(:update, user) 
        return initialize_response.failure(401, [['password', 'did not match']]) if password && !current_user.authenticate(password)

        user.update(attributes.to_h)

        return initialize_response.success({ user: user }) if user.valid?
        initialize_response.failure(500, user.errors)
      end

      private
    
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
