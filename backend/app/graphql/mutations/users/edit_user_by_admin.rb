module Mutations
  module Users
    class EditUserByAdmin < BaseMutation
      argument :attributes, Types::Users::UserInputType, required: true
      argument :role, String, required: true
      argument :id, ID, required: true

      field :success, Boolean, null: true
      
      def resolve(attributes:, id:, role:)
        user = User.find_by(id: id)

        current_user = context[:current_user]
        return initialize_response.failure(403, [['current user', 'is required']]) unless current_user
        return initialize_response.failure(404, [['user', 'not found']]) unless user

        attributes = attributes.to_h

        user.update(attributes.compact)

        user.remove_role user.roles.first.name
        user.add_role role

        return initialize_response.success({ user: user }) if user.valid?
        initialize_response.failure(500, user.errors)
      end

      private
    
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
