module Mutations
  module Users
    class SignOut < BaseMutation
      field :success, Boolean, null: true
      # field :response, Types::ResponseStatusType, null: false

      def resolve()
        context[:session][:token] = nil

        initialize_response.success({ success: true })
      end

      private
        def initialize_response
          ResponseService.new(self.field&.name, context)
        end
    end
  end
end
