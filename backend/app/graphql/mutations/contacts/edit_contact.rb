module Mutations
  module Contacts
    class EditContact < BaseMutation
      argument :user_id, ID, required: true
      argument :contact_attributes, Types::Profiles::ProfileInputType, required: true

      field :user, Types::Users::UserType, null: true

      def resolve(contact_attributes:, user_id:)
        current_user = context[:current_user]

        return initialize_response.failure(403, [['current_user', 'is required']]) unless current_user
        return initialize_response.failure(403, [['selected_agency', 'is required']]) unless current_user.selected_agency

        user = ::User.find_by(id: user_id)

        return initialize_response.failure(404, [['user', 'not found']]) unless user

        user.profile.update(contact_attributes.to_h)

        if user.save
          initialize_response.success({ user: user })
        else
          initialize_response.failure(500, user.errors)
        end
      end

      private
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
