module Mutations
  module Contacts
    class CreateContact < BaseMutation
      argument :contact_attributes, Types::Profiles::ProfileInputType, required: true

      field :user, Types::Users::UserType, null: true

      def resolve(contact_attributes:)
        current_user = context[:current_user]

        return initialize_response.failure(403, [['current_user', 'is required']]) unless current_user
        return initialize_response.failure(403, [['selected_agency', 'is required']]) unless current_user.selected_agency

        email = "#{SecureRandom.hex(16)}@temp.com"
        password = SecureRandom.hex(16)
        user = ::User.create(
          email: email,
          password: password,
          profile_attributes: contact_attributes.to_h,
          organization: current_user.selected_agency.organization
        )

        if user.save
          user.add_role 'contact', current_user.selected_agency
          initialize_response.success({ user: user })
        else
          initialize_response.failure(500, user.errors)
        end
      end

      private
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
