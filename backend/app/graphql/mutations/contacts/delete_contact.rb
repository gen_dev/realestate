module Mutations
  module Contacts
    class DeleteContact < BaseMutation
      argument :user_id, ID, required: true

      field :success, Boolean, null: false

      def resolve(user_id:)
        current_user = context[:current_user]

        return initialize_response.failure(403, [['current_user', 'is required']]) unless current_user
        return initialize_response.failure(403, [['selected_agency', 'is required']]) unless current_user.selected_agency

        user = ::User.find_by(id: user_id)

        return initialize_response.failure(404, [['user', 'not found']]) unless user

        if user.delete
          initialize_response.success({ success: true })
        else
          initialize_response.failure(500, user.errors)
        end
      end

      private
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
