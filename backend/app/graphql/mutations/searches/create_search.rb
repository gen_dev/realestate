module Mutations
  module Searches
    class CreateSearch < BaseMutation
      argument :attributes, Types::Searches::SearchInputType, required: true
      argument :user_id, ID, required: true
      argument :inquiry_id, ID, required: false

      field :success, Boolean, null: true

      def resolve(attributes:, user_id:, inquiry_id: nil)
        current_user = context[:current_user]
        return initialize_response.failure(403, [['user', 'is required']]) unless current_user

        agency = current_user.selected_agency
        return initialize_response.failure(404, [['agency', 'not found']]) unless agency

        search = agency.searches.new(attributes.to_h)
        search.user_id = user_id
        search.inquiry_id = inquiry_id

        if search.save
          initialize_response.success({ success: true })
        else
          initialize_response.failure(500, search.errors)
        end
      end

      private
      
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
