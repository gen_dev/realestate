module Mutations
  module Searches
    class EditSearch < BaseMutation
      argument :attributes, Types::Searches::SearchInputType, required: true
      argument :id, ID, required: true

      field :success, Boolean, null: true

      def resolve(attributes:, id:)
        current_user = context[:current_user]
        return initialize_response.failure(403, [['user', 'is required']]) unless current_user

        agency = current_user.selected_agency
        return initialize_response.failure(404, [['agency', 'not found']]) unless agency

        search = agency.searches.find_by(id: id)
        search.update attributes.to_h

        if search.valid?
          initialize_response.success({ success: true })
        else
          initialize_response.failure(500, search.errors)
        end
      end

      private
      
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
