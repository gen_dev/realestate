module Mutations
  module Properties
    class CreateProperty < BaseMutation
      argument :attributes, Types::Properties::PropertyInputType, required: true
      argument :owner_ids, [ID], required: true

      field :property, Types::Properties::PropertyType, null: true

      def resolve(attributes:, owner_ids:)
        current_user = context[:current_user]
        return initialize_response.failure(403, [['user', 'is required']]) unless current_user
        agency = current_user.selected_agency
        return initialize_response.failure(404, [['agency', 'not found']]) unless agency

        property = Property.new(attributes.to_h.merge(agency: agency))
        return initialize_response.failure(500, [['user', 'cant create Property for this Agency']]) if current_user.cannot?(:create, property)

        owners = ::User.where(id: owner_ids)
        return initialize_response.failure(404, [['owner', 'not found']]) unless owners.length

        if property.save
          owners.each do |owner|
            owner.add_role :owner, property
            owner.save
          end
          initialize_response.success({ property: property })
        else
          initialize_response.failure(500, property.errors)
        end
      end

      private
      
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
