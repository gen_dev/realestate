module Mutations
  module Properties
    class EditProperty < BaseMutation
      argument :attributes, Types::Properties::PropertyEditInputType, required: true
      argument :property_id, ID, required: true
      argument :owner_ids, [ID], required: false

      field :property, Types::Properties::PropertyType, null: true
      
      def resolve(attributes: nil, property_id:, owner_ids: nil)
        return initialize_response.failure(500, [['attributes', 'is required']]) unless attributes

        property = Property.find_by(id: property_id)

        current_user = context[:current_user]
        return initialize_response.failure(403, [['user', 'is required']]) unless current_user
        return initialize_response.failure(404, [['property', 'not found']]) unless property
        return initialize_response.failure(401, [['user', 'unauthorized']]) if current_user.cannot?(:update, property)

        if owner_ids
          owners = ::User.where(id: owner_ids)
          return initialize_response.failure(404, [['owner', 'not found']]) unless owners.length

          old_owners = property.owner

          if owners != old_owners
            old_owners.each do |old_owner|
              old_owner.remove_role :owner, property
              old_owner.remove_role :owner, property.active_contract if property.active_contract
            end

            owners.each do |owner|
              owner.add_role :owner, property
              owner.add_role :owner, property.active_contract if property.active_contract
            end
          end
        end

        property.update(attributes.to_h)

        return initialize_response.success({ property: property }) if property.valid?
        initialize_response.failure(500, property.errors)
      end

      private
    
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
