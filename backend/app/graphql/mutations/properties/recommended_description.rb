module Mutations
  module Properties
    class RecommendedDescription < BaseMutation
      argument :id, ID, required: true
      argument :description, ID, required: false

      field :description, String, null: true
      
      def resolve(id:, description: nil)
        current_user = context[:current_user]

        return initialize_response.failure(403, [['user', 'is required']]) unless current_user

        property = Property.find_by(id: id)

        return initialize_response.failure(404, [['property', 'not found']]) unless property

        client = OpenAI::Client.new

        basic_info = property.characteristics.where.not(value: [0, nil, ""]).map {|x| "#{x.value} #{x.name}"}

        if description
          message = description
        else
          message = "Genera una descripción para una publicación de red social sobre #{translations[property.operation_types[0].to_sym]} de #{translations[property.property_type.to_sym]} ubicado en #{property.full_address} que cuenta con #{basic_info.join(', ')}, usando saltos de línea y negrita en palabras clave, formato HTML" 
        end

        response = client.chat(
          parameters: {
            model: "gpt-3.5-turbo", # Required.
            messages: [{ role: "user", content: message}], # Required.
          })

        description = response.dig("choices", 0, "message", "content")

        { description: description }
      end

      private

      def translations
        {
          sell: 'la venta',
          rent: 'el alquiler',
          apartment: 'un departamento',
          country_house: 'una quinta',
          farm: 'un campo',
          house: 'una casa',
          terrain: 'un terreno',
          warehouse: 'un depósito',
          parking: 'una cochera',
          shed: 'un galpón',
          salon: 'un salón',
          bathroom: 'baños',
          bedroom: 'dormitorios',
          rooms: 'ambientes'
        }
      end
    
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
