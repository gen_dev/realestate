module Mutations
  module Properties
    class DeleteProperty < BaseMutation
      argument :id, ID, required: true

      field :success, Boolean, null: false

      def resolve(id:)
        current_user = context[:current_user]
        
        return initialize_response.failure(403, [['user', 'is required']]) unless current_user

        property = Property.find_by(id: id)

        return initialize_response.failure(404, [['property', 'not found']]) unless property
        return initialize_response.failure(401, [['user', 'unauthorized']]) if current_user.cannot?(:destroy, property)

        return initialize_response.success({ success: true }) if property.destroy
        initialize_response.failure(500, property.errors)
      end

      private

      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
