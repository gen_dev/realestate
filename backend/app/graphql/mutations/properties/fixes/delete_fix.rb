module Mutations
  module Properties
    module Fixes
      class DeleteFix < BaseMutation
        argument :id, ID, required: true

        field :success, Boolean, null: true

        def resolve(id:)
          current_user = context[:current_user]
          return initialize_response.failure(403, [['user', 'is required']]) unless current_user
          agency = current_user.selected_agency
          return initialize_response.failure(404, [['agency', 'not found']]) unless agency

          fix = ::Properties::Fix.find(id)

          if fix.destroy
            initialize_response.success({ success: true })
          else
            initialize_response.failure(500, fix.errors)
          end
        end

        private

        def initialize_response
          ResponseService.new(self.field&.name, context)
        end
      end
    end
  end
end
