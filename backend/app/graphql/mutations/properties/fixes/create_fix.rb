module Mutations
  module Properties
    module Fixes
      class CreateFix < BaseMutation
        argument :attributes, Types::Properties::FixInputType, required: true

        field :fix, Types::Properties::FixType, null: true

        def resolve(attributes:)
          current_user = context[:current_user]
          return initialize_response.failure(403, [['user', 'is required']]) unless current_user
          agency = current_user.selected_agency
          return initialize_response.failure(404, [['agency', 'not found']]) unless agency

          fix = ::Properties::Fix.create!(attributes.to_h)
          return initialize_response.failure(500, [['user', 'cant create Fix for this Agency']]) if current_user.cannot?(:create, fix)

          if fix.save
            initialize_response.success({ fix: fix })
          else
            initialize_response.failure(500, fix.errors)
          end
        end

        private

        def initialize_response
          ResponseService.new(self.field&.name, context)
        end
      end
    end
  end
end
