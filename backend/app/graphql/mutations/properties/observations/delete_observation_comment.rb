module Mutations
  module Properties
    module Observations
      class DeleteObservationComment < BaseMutation
        argument :id, ID, required: true

        field :success, Boolean, null: true

        def resolve(id:)
          comment = ::Properties::Comment.find(id)

          if comment.destroy
            initialize_response.success({ success: true })
          else
            initialize_response.failure(500, comment.errors)
          end
        end

        private
        def initialize_response
          ResponseService.new(self.field&.name, context)
        end
      end
    end
  end
end
