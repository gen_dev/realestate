module Mutations
  module Properties
    module Observations
      class CreateObservation < BaseMutation
        argument :attributes, Types::Properties::Observations::ObservationInputType, required: true

        field :success, Boolean, null: true

        def resolve(attributes:)
          observation = ::Properties::Observation.new(attributes.to_h)

          if user = context[:current_user]
            observation.user = user
          end

          if observation.save
            initialize_response.success({ success: true })
          else
            initialize_response.failure(500, inquiry.errors)
          end
        end

        private
        def initialize_response
          ResponseService.new(self.field&.name, context)
        end
      end
    end
  end
end
