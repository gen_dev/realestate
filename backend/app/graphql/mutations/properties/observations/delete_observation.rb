module Mutations
  module Properties
    module Observations
      class DeleteObservation < BaseMutation
        argument :id, ID, required: true

        field :success, Boolean, null: true

        def resolve(id:)
          observation = ::Properties::Observation.find(id)

          if observation.destroy
            initialize_response.success({ success: true })
          else
            initialize_response.failure(500, observation.errors)
          end
        end

        private
        def initialize_response
          ResponseService.new(self.field&.name, context)
        end
      end
    end
  end
end
