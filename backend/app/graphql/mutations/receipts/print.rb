module Mutations
  module Receipts
    class Print < BaseMutation
      argument :id, ID, required: true

      field :success, Boolean, null: true

      def resolve(id:)
        current_user = context[:current_user]

        return initialize_response.failure(403, [['current_user', 'is required']]) unless current_user
        return initialize_response.failure(403, [['selected_agency', 'is required']]) unless current_user.selected_agency

        receipt = ::Receipt.find_by(id: id)

        return initialize_response.failure(404, [['receipt', 'not found']]) unless receipt

        receipt.state = 'printed'

        if receipt.save
          initialize_response.success({ success: true })
        else
          initialize_response.success({ success: false })
        end
      rescue
        initialize_response.failure(500, receipt.errors)
      end

      private
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
