module Mutations
  module Receipts
    class CreateReceipt < BaseMutation
      argument :rent, String, required: false
      argument :contract_id, ID, required: true
      argument :attributes, Types::Receipts::ReceiptInputType, required: true

      field :receipt, Types::Receipts::ReceiptType, null: true

      def resolve(attributes:, rent: nil, contract_id:)
        current_user = context[:current_user]

        return initialize_response.failure(403, [['current_user', 'is required']]) unless current_user
        return initialize_response.failure(403, [['selected_agency', 'is required']]) unless current_user.selected_agency

        receipt = ::Receipt.new(attributes.to_h)

        receipt.agency = current_user.selected_agency

        if rent
          rent = rent.to_f
          contract = ::Contract.find_by(id: contract_id)

          debtor_id = contract ? contract.tenant[0].id : self.debtor_id
          debtor_type = contract ? contract.tenant[0].class.to_s : self.debtor_type

          tax = contract.rent

          payment = Payment.create({
            tax: tax,
            contract: contract,
            referral: contract.property.agency, 
            receiver: contract.property.owner[0], 
            amount: rent,
            sub_account: sub_account(contract.property.agency)["Alquiler"],
            concept: 'Alquiler',
            state: 'pending',
            due_date: Date.today.end_of_month,
          })

          commission = Payment.create({
            tax: tax,
            contract: contract,
            referral: contract.property.owner[0], 
            receiver: contract.property.agency, 
            amount: rent * (tax.commission / 100),
            sub_account: sub_account(contract.property.agency)["Comision"],
            concept: 'Alquiler',
            state: 'pending',
            due_date: Date.today.end_of_month,
          })

          rented = Payment.create({
            tax: tax,
            contract: contract, 
            referral_id: debtor_id, 
            referral_type: debtor_type,
            receiver: contract.property.agency, 
            amount: rent,
            sub_account: sub_account(contract.property.agency)['Alquiler'],
            concept: 'Alquiler',
            state: 'paid',
            due_date: Date.today.end_of_month,
          })
        end

        if receipt.save
          receipt.payments.push(rented)

          { receipt: receipt }
        else
          initialize_response.failure(500, receipt.errors)
        end
      rescue
      end

      private
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end

      def sub_account(agency)
        {
          'Alquiler' => SubAccount.find_by(name: 'Alquileres', agency: agency),
          'Honorarios' => SubAccount.find_by(name: 'Honorarios alquiler inquilino', agency: agency),
          'Honorarios propietario' => SubAccount.find_by(name: 'Honorarios alquiler propietario', agency: agency),
          # 'Rendicion' => SubAccount.find_by(name: 'Alquileres pagados', agency: agency),
          'Comision' => SubAccount.find_by(name: 'Honorarios administración alquiler', agency: agency),
        }
      end
    end
  end
end
