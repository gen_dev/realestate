module Mutations
  module Addresses
    class CreateAddress < BaseMutation
      argument :attributes, Types::Addresses::AddressInputType, required: true
      argument :addressable_id, ID, required: true
      argument :addressable_type, String, required: true

      field :address, Types::Addresses::AddressType, null: true
      
      def resolve(attributes: nil, addressable_id:, addressable_type:)
        addressable = addressable_type.constantize.find_by(id: addressable_id)

        current_user = context[:current_user]
        return initialize_response.failure(403, [['user', 'is required']]) unless current_user
        return initialize_response.failure(404, [['addressable', 'not found']]) unless addressable
        
        address = Address.new(attributes.to_h.merge(addressable: addressable))
        return initialize_response.failure(500, [['user', "cant create address for this #{addressable_type}"]]) if current_user.cannot?(:create, address)
        
        if address.save
          initialize_response.success({ address: address })
        else
          initialize_response.failure(500, address.errors)
        end
      end

      private
      
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
