module Mutations
  module Addresses
    class EditAddress < BaseMutation
      argument :attributes, Types::Addresses::AddressInputType, required: true
      argument :address_id, ID, required: true

      field :address, Types::Addresses::AddressType, null: true
      
      def resolve(attributes: nil, address_id:)
        return initialize_response.failure(500, [['attributes', 'is required']]) unless attributes

        address = Address.find_by(id: address_id)

        current_user = context[:current_user]
        return initialize_response.failure(403, [['user', 'is required']]) unless current_user
        return initialize_response.failure(404, [['address', 'not found']]) unless address
        return initialize_response.failure(401, [['user', 'unauthorized']]) if current_user.cannot?(:update, address)

        address.update(attributes.to_h)

        return initialize_response.success({ address: address }) if address.valid?
        initialize_response.failure(500, address.errors)
      end

      private
    
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
