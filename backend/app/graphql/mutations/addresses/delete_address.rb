module Mutations
  module Addresses
    class DeleteAddress < BaseMutation
      argument :address_id, ID, required: true

      field :success, Boolean, null: false

      def resolve(address_id:)
        current_user = context[:current_user]
        
        return initialize_response.failure(403, [['user', 'is required']]) unless current_user

        address = Address.find_by(id: address_id)

        return initialize_response.failure(404, [['address', 'not found']]) unless address
        return initialize_response.failure(401, [['user', 'unauthorized']]) if current_user.cannot?(:destroy, address)

        return initialize_response.success({ success: true }) if address.destroy
        initialize_response.failure(500, address.errors)
      end

      private
      
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
