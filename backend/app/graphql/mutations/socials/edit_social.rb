module Mutations
  module Socials
    class EditSocial < BaseMutation
      argument :attributes, Types::Agencies::AgencyInputType, required: true
      argument :id, ID, required: true

      field :success, Boolean, null: true
      
      def resolve(attributes:, id:)
        current_user = context[:current_user]
        return initialize_response.failure(403, [['user', 'is required']]) unless current_user

        agency = current_user.selected_agency
        return initialize_response.failure(404, [['agency', 'not found']]) unless agency

        social = agency.socials.find(id)

        social.update(attributes.to_h)

        return initialize_response.success({ success: true }) if social.valid?
        initialize_response.failure(500, social.errors)
      end

      private
    
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
