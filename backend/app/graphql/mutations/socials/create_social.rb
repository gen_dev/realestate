module Mutations
  module Socials
    class CreateSocial < BaseMutation
      argument :attributes, Types::Socials::SocialInputType, required: true

      field :success, Boolean, null: true

      def resolve(attributes:)
        current_user = context[:current_user]
        return initialize_response.failure(403, [['user', 'is required']]) unless current_user

        agency = current_user.selected_agency

        social = agency.socials.create(attributes.to_h)

        if social.valid?
          initialize_response.success({ success: true })
        else
          initialize_response.failure(500, social.errors)
        end
      end

      private
      
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
