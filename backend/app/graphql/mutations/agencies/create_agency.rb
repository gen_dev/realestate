module Mutations
  module Agencies
    class CreateAgency < BaseMutation
      argument :name, String, required: true

      field :agency, Types::Agencies::AgencyType, null: true

      def resolve(name: nil)
        agency = Agency.new(name: name)

        current_user = context[:current_user]
        return initialize_response.failure(403, [['user', 'is required']]) unless current_user
        return initialize_response.failure(500, [['user', "cant create Agencies"]]) unless current_user.can?(:create, agency)

        if agency.save
          initialize_response.success({ agency: agency })
        else
          initialize_response.failure(500, agency.errors)
        end
      end

      private
      
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
