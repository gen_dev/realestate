module Mutations
  module Agencies
    class EditAgency < BaseMutation
      argument :attributes, Types::Agencies::AgencyInputType, required: true
      argument :id, ID, required: true

      field :success, Boolean, null: true
      
      def resolve(attributes:, id:)
        agency = Agency.find_by(id: id)

        current_user = context[:current_user]
        return initialize_response.failure(403, [['user', 'is required']]) unless current_user
        return initialize_response.failure(404, [['agency', 'not found']]) unless agency
        return initialize_response.failure(401, [['user', 'unauthorized']]) if current_user.cannot?(:update, agency)

        agency.update(attributes.to_h)

        return initialize_response.success({ success: true }) if agency.valid?
        initialize_response.failure(500, agency.errors)
      end

      private
    
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
