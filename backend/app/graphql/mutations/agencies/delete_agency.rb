module Mutations
  module Agencies
    class DeleteAgency < BaseMutation
      argument :agency_id, ID, required: true

      field :success, Boolean, null: false

      def resolve(agency_id:)
        current_user = context[:current_user]
        
        return initialize_response.failure(403, [['user', 'is required']]) unless current_user

        agency = Agency.find_by(id: agency_id)

        return initialize_response.failure(404, [['agency', 'not found']]) unless agency
        return initialize_response.failure(401, [['user', 'unauthorized']]) if current_user.cannot?(:destroy, agency)

        return initialize_response.success({ success: true }) if agency.destroy
        initialize_response.failure(500, agency.errors)
      end

      private

      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
