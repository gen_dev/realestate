module Mutations
  module CashRegisters
    class EditCashRegister < BaseMutation
      argument :attributes, Types::CashRegisters::CashRegisterInputType, required: true
      argument :id, ID, required: true

      field :success, Boolean, null: true
      
      def resolve(attributes:, id:)
        current_user = context[:current_user]

        return initialize_response.failure(403, [['user', 'is required']]) unless current_user

        cash_register = ::CashRegister.find_by(id: id)

        cash_register.update(attributes.to_h)

        return initialize_response.success({ success: true }) if cash_register.valid?
        initialize_response.failure(500, cash_register.errors)
      end

      private
    
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
