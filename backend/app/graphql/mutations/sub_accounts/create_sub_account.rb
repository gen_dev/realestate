module Mutations
  module SubAccounts
    class CreateSubAccount < BaseMutation
      argument :attributes, Types::SubAccounts::SubAccountInputType, required: true

      field :success, Boolean, null: true

      def resolve(attributes: nil)
        current_user = context[:current_user]

        return initialize_response.failure(403, [['user', 'is required']]) unless current_user

        agency = current_user.selected_agency
        
        sub_account = SubAccount.new(attributes.to_h.merge(agency: agency))

        if sub_account.save
          initialize_response.success({ success: true })
        else
          initialize_response.failure(500, sub_account.errors)
        end
      end

      private
      
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
