module Mutations
  module SubAccounts
    class DeleteSubAccount < BaseMutation
      argument :sub_account_id, ID, required: true

      field :success, Boolean, null: false

      def resolve(sub_account_id:)
        current_user = context[:current_user]
        
        return initialize_response.failure(403, [['user', 'is required']]) unless current_user

        sub_account = SubAccount.find_by(id: sub_account_id)

        return initialize_response.failure(404, [['sub_account', 'not found']]) unless sub_account
        return initialize_response.failure(401, [['user', 'unauthorized']]) if current_user.cannot?(:destroy, sub_account)

        return initialize_response.success({ success: true }) if sub_account.destroy
        initialize_response.failure(500, sub_account.errors)
      end

      private

      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
