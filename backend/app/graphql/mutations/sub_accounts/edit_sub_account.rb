module Mutations
  module SubAccounts
    class EditSubAccount < BaseMutation
      argument :attributes, Types::SubAccounts::SubAccountInputType, required: true
      argument :sub_account_id, ID, required: true

      field :sub_account, Types::SubAccounts::SubAccountType, null: true
      
      def resolve(attributes: nil, sub_account_id:)
        return initialize_response.failure(500, [['attributes', 'is required']]) unless attributes

        sub_account = SubAccount.find_by(id: sub_account_id)

        current_user = context[:current_user]
        return initialize_response.failure(403, [['user', 'is required']]) unless current_user
        return initialize_response.failure(404, [['sub_account', 'not found']]) unless sub_account
        return initialize_response.failure(401, [['user', 'unauthorized']]) if current_user.cannot?(:update, sub_account)

        sub_account.update(attributes.to_h)

        return initialize_response.success({ sub_account: sub_account }) if sub_account.valid?
        initialize_response.failure(500, sub_account.errors)
      end

      private
    
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
