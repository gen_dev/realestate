module Queries
  module CashRegisters
    class CurrentCashRegister < Queries::BaseQuery
      description 'Find all Agencies'

      type Types::CashRegisters::CashRegisterType, null: true

      def resolve
        current_user = context[:current_user]
        return initialize_response.failure(403, [['user', 'is required']]) unless current_user

        agency = current_user.selected_agency

        cash_register = agency.cash_registers.find_or_create_by(created_at: Date.today.beginning_of_day..Date.today.end_of_day)

        cash_register
      end

      private
      
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
