module Queries
  module CashRegisters
    class CashRegister < Queries::BaseQuery
      description 'Find all Agencies'

      argument :id, ID, required: true

      type Types::CashRegisters::CashRegisterType, null: true

      def resolve(id:)
        current_user = context[:current_user]
        return initialize_response.failure(403, [['user', 'is required']]) unless current_user

        agency = current_user.selected_agency

        agency.cash_registers.find_by(id: id)
      end

      private
      
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
