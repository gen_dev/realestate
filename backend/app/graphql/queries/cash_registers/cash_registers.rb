module Queries
  module CashRegisters
    class CashRegisters < Queries::BaseQuery
      description 'Find all Agencies'

      type [Types::CashRegisters::CashRegisterType], null: true

      def resolve
        current_user = context[:current_user]
        return initialize_response.failure(403, [['user', 'is required']]) unless current_user

        agency = current_user.selected_agency

        agency.cash_registers.order('id DESC')
      end

      private
      
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
