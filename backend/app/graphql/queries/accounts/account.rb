module Queries
  module Accounts
    class Account < Queries::BaseQuery
      description 'Find a Accounts by ID'

      argument :id, ID, required: true

      type Types::Accounts::AccountType, null: true

      def resolve(id:)
        current_user = context[:current_user]
        return initialize_response.failure(403, [['user', 'is required']]) unless current_user

        account = ::Account.find_by(id: id)
        return initialize_response.failure(500, [['user', 'unauthorized']]) if current_user.cannot?(:manage, account)

        account
      end

      private

      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
