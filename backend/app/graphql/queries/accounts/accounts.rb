module Queries
  module Accounts
    class Accounts < Queries::BaseQuery
      description 'Find all Account'

      argument :period, [String], required: false
      argument :filters, Types::Accounts::AccountFilterType, required: false

      type [Types::Accounts::AccountType], null: true

      def resolve(filters: nil, period: nil)
        current_user = context[:current_user]

        return initialize_response.failure(403, [['user', 'is required']]) unless current_user

        accounts = current_user.selected_agency.accounts
        accounts = accounts.where(filters.to_h) if filters 

        unless period.blank?
          accounts.map { |x| x.period = period }
        end

        accounts.uniq
      end

      private
      
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
