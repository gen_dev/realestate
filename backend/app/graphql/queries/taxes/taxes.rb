module Queries
  module Taxes
    class Taxes < Queries::BaseQuery
      description 'Find all Taxes'
      
      argument :id, ID, required: true
      
      type [Types::Taxes::TaxType], null: true
      
      def resolve(id:)
        current_ability = context[:current_ability]
        return initialize_response.failure(403, [['user', 'is required']]) unless current_ability

        contract = ::Contract.find_by(id: id)

        taxes = contract.taxes

        taxes
      end

      private
      
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
