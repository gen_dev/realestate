module Queries
  module Taxes
    class Tax < Queries::BaseQuery
      description 'Find a Tax by ID'

      argument :id, ID, required: true

      type Types::Taxes::TaxType, null: true

      def resolve(id:)
        current_user = context[:current_user]
        return initialize_response.failure(403, [['user', 'is required']]) unless current_user

        tax = ::Tax.find_by(id: id)
        return initialize_response.failure(500, [['user', 'unauthorized']]) if current_user.cannot?(:manage, tax)

        tax
      end

      private

      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
