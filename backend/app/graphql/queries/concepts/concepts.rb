module Queries
  module Concepts
    class Concepts < Queries::BaseQuery
      description 'Find all SubAccount'

      argument :sub_account_id, ID, required: true

      type [Types::Concepts::ConceptType], null: true

      def resolve(sub_account_id:)
        current_ability = context[:current_ability]
        current_user = context[:current_user]
        return initialize_response.failure(403, [['user', 'is required']]) unless current_ability

        sub_account = current_user.selected_agency.sub_accounts.find_by(id: sub_account_id)
        
        sub_account.concepts.order('name ASC')
      end

      private
      
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
