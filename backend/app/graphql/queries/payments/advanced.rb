module Queries
  module Payments
    class Advanced < Queries::BaseQuery
      description 'Find an Owner by ID'

      type [Types::Payments::PaymentType], null: true

      def resolve()
        current_user = context[:current_user]
        return initialize_response.failure(403, [['current_user', 'is required']]) unless current_user

        agency = current_user.selected_agency
        sa = SubAccount.where(agency_id: agency.id)
        payments = ::Payment.where(sub_account_id: sa.pluck(:id))

        payments = payments.current

        payments = payments.where(advanced: true)

        payments = payments.group_by(&:concept)

        result = []

        payments.each_pair do |k, v|
          valid = v.select { |e| e.is_advanced }
          total = valid.map(&:amount).sum

          result << { name: k, amount: total }
        end

        result
      end

      private

      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
