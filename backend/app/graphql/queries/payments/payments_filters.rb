module Queries
  module Payments
    class PaymentsFilters < Queries::BaseQuery
      description 'Find all Payments'

      argument :subaccounts, [String], required: false
      argument :from_agency, Boolean, required: false
      
      type Types::Payments::PaymentListType, null: true

      def resolve(subaccounts: nil, from_agency: false)
        current_ability = context[:current_ability]
        current_user = context[:current_user]

        return initialize_response.failure(403, [['user', 'is required']]) unless current_ability

        agency = current_user.selected_agency
        sa = SubAccount.where(agency_id: agency&.id)
        payments = ::Payment.where(sub_account_id: sa.pluck(:id))

        payments = payments.current

        if subaccounts 
          sids = agency.sub_accounts.where(name: subaccounts).pluck(:id)
          payments = payments.where(sub_account_id: sids)
        end

        if from_agency
          payments = payments.preload('referral').where(referral_id: agency.id, referral_type: 'Agency')
        end

        payments = payments.distinct.order('id desc').page(1).per(payments.count || 10)

        { payments: payments, pages: payments.total_pages }
      end

      private
      
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
