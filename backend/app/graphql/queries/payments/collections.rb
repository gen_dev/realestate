module Queries
  module Payments
    class Collections < Queries::BaseQuery
      description 'Find an Owner by ID'

      argument :id, ID, required: true
      argument :period, String, required: true

      type Types::Payments::AccountabilityType, null: true

      def resolve(id:, period:)
        current_user = context[:current_user]
        return initialize_response.failure(403, [['current_user', 'is required']]) unless current_user

        user = ::User.find_by(id: id)

        return initialize_response.failure(500, [['user', 'unauthorized']]) if current_user.cannot?(:manage, user)

        property_ids = current_user.selected_agency.properties.pluck(:id)

        contract_ids = ::Contract.where(property_id: property_ids).to_a

        if (period.to_i > 1)
          payments = ::Payment.
            where(parent_id: nil, referral_id: id, referral_type: 'User').
            or(
              ::Payment.
              where(parent_id: nil, receiver_type: 'User', receiver_id: id)
            ).
            where('contract_id IN (?) OR contract_id IS NULL', contract_ids).
            where.not(state: :deleted)

          ids = payments.pluck(:tax_id)

          ::Tax.where(id: ids).map{|x| x.create_payment([period.to_i])}
        end

        payments = ::Payment.
          where(parent_id: nil, referral_id: id, referral_type: 'User').
          or(
            ::Payment.
            where(parent_id: nil, receiver_type: 'User', receiver_id: id)
          ).
          where('contract_id IN (?) OR contract_id IS NULL', contract_ids).
          where.not(state: :deleted).
          order('id asc')

        paid = []
        pending = payments.current(period).reorder('due_date ASC')
        chargebacks = ::Payments::Chargeback.where(active: true, payment_id: pending.pluck(:id)).pluck(:amount).sum

        { paid: paid, pending: pending, chargebacks: chargebacks }
      end

      private

      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
