module Queries
  module Payments
    class Payments < Queries::BaseQuery
      description 'Find all Payments'

      argument :filters, Types::Payments::PaymentFilterType, required: false
      argument :subaccounts, [String], required: false
      argument :from_agency, Boolean, required: false
      argument :contract_id, ID, required: false
      argument :search, String, required: false
      argument :date, [String], required: false
      argument :page, Integer, required: false
      argument :per_page, Integer, required: false
      
      type Types::Payments::PaymentListType, null: true

      def resolve(search: nil, filters: nil, subaccounts: nil, page: 1, per_page: 10, from_agency: false, contract_id: nil, date: nil)
        current_ability = context[:current_ability]
        current_user = context[:current_user]

        return initialize_response.failure(403, [['user', 'is required']]) unless current_ability

        agency = current_user.selected_agency
        sa = ::SubAccount.where(agency_id: agency&.id)
        payments = ::Payment.where(sub_account_id: sa.pluck(:id)).where.not(paid_at: nil, state: :deleted)

        if !date.blank?
          date = date.sort

          if date.length == 1
            payments = payments.where(paid_at: date[0].to_date.beginning_of_day..date[0].to_date.end_of_day)
          else
            payments = payments.where(paid_at: date[0].to_date.beginning_of_day..date[1].to_date.end_of_day)
          end
        else
          payments = payments.current
        end

        payments = payments.includes(:referral, :partial_payments, :chargebacks, :price, sub_account: :account, contract: [property: :address])

        if subaccounts 
          sids = agency.sub_accounts.where(name: subaccounts).pluck(:id)
          payments = payments.where(sub_account_id: sids)
        end

=begin
        if filters
          nfilters = { concept: filters[:concept], state: filters[:state] }.delete_if { |k, v| v.blank? } if filters
          
          if filters[:userType].present?
            # TODO: Improve this horrible select to SQL 
            payments = ::Payment.where(id: payments.select { |x| filters[:userType].include?(x.user_type) }.map(&:id))
          end

          if filters[:subAccount].present?
            payments = payments.left_joins(:sub_account).
            where('(LOWER(unaccent(sub_accounts.name)) LIKE LOWER(unaccent(?)))', filters[:subAccount])
          end
          
          if filters[:account].present?
            payments = payments.left_joins(sub_account: :account).
            where('(LOWER(unaccent(accounts.name)) LIKE LOWER(unaccent(?)))', filters[:account])
          end
          
          payments = payments.where(nfilters) if nfilters.any? 
        end
=end
        if search.present?
          users_id = ::Users::Profile.where(
            '(LOWER(unaccent(users_profiles.name)) LIKE LOWER(unaccent(:q)))',
            q: "%#{search}%"
          ).pluck(:user_id)

          payments = payments.left_joins(sub_account: :account).
            where(
              '(LOWER(unaccent(sub_accounts.name)) LIKE LOWER(unaccent(:q))) 
              OR (LOWER(unaccent(accounts.name)) LIKE LOWER(unaccent(:q)))
              OR (LOWER(unaccent(payments.concept)) LIKE LOWER(unaccent(:q)))
              OR (LOWER(unaccent(payments.contract_tag)) LIKE LOWER(unaccent(:q)))
              OR (payments.receiver_type = :ut AND payments.receiver_id IN (:uid))
              OR (payments.referral_type = :ut AND payments.referral_id IN (:uid))',
              q: "%#{search}%",
              uid: users_id,
              ut: "User"
            ) 
        end

        if from_agency
          payments = payments.where(referral_id: agency.id, referral_type: 'Agency')
        elsif !from_agency && subaccounts
          payments = payments.where.not(referral_id: agency.id, referral_type: 'Agency')
        end

        if contract_id
          payments = payments.where(contract_id: contract_id)
        else
          pending_receipts = Receipt.where(state: 'pending').pluck(:id)
          payments = payments.where.not(receipt_id: pending_receipts).or(payments.where(receipt_id: nil))
        end

        payments = payments.distinct.order('paid_at desc').page(page).per(per_page)

        { payments: payments, pages: payments.total_pages }
      end

      private
      
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
