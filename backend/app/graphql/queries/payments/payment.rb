module Queries
  module Payments
    class Payment < Queries::BaseQuery
      description 'Find a Payment by ID'

      argument :id, ID, required: true

      type Types::Payments::PaymentType, null: true

      def resolve(id:)
        current_user = context[:current_user]
        return initialize_response.failure(403, [['user', 'is required']]) unless current_user

        payment = ::Payment.find_by(id: id)
        return initialize_response.failure(500, [['user', 'unauthorized']]) if current_user.cannot?(:manage, payment)

        payment
      end
      
      private
      
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
