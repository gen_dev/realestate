module Queries
  module Payments
    class Today < Queries::BaseQuery
      description 'Find all Payments'

      type [Types::Payments::PaymentType], null: true

      def resolve
        current_ability = context[:current_ability]
        current_user = context[:current_user]

        return initialize_response.failure(403, [['user', 'is required']]) unless current_ability

        agency = current_user.selected_agency

        sa = ::SubAccount.where(agency_id: agency&.id)
        payments = ::Payment.where(sub_account_id: sa.pluck(:id)).where(paid_at: Date.today.beginning_of_day..Date.today.end_of_day)

        payments = payments.includes(:referral, :partial_payments, :chargebacks, :price, sub_account: :account, contract: [property: :address])

        payments = payments.distinct.order('id desc')

        payments
      end

      private
      
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
