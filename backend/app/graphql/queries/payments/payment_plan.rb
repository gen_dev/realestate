module Queries
  module Payments
    class PaymentPlan < Queries::BaseQuery
      description 'Find a Payment Plan by ID'

      argument :id, ID, required: true

      type Types::Payments::PaymentPlanType, null: true

      def resolve(id:)
        ::PaymentPlan.find_by(id: id)

        current_user = context[:current_user]
        return initialize_response.failure(403, [['user', 'is required']]) unless current_user

        payment_plan = ::PaymentPlan.find_by(id: id)
        return initialize_response.failure(500, [['user', 'unauthorized']]) if current_user.cannot?(:manage, payment_plan)

        payment_plan
      end

      private

      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
