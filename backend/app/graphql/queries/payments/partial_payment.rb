module Queries
  module Payments
    class PartialPayment < Queries::BaseQuery
      description 'Find a Partial Payment by ID'

      argument :id, ID, required: true

      type Types::Payments::PartialPaymentType, null: true

      def resolve(id:)
        current_user = context[:current_user]
        return initialize_response.failure(403, [['user', 'is required']]) unless current_user

        partial_payment = ::PartialPayment.find_by(id: id)
        return initialize_response.failure(500, [['user', 'unauthorized']]) if current_user.cannot?(:manage, partial_payment)

        partial_payment
      end

      private

      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
