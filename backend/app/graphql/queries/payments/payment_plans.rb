module Queries
  module Payments
    class PaymentPlans < Queries::BaseQuery
      description 'Find all Payments Plans'

      argument :filters, Types::Payments::PaymentPlanFilterType, required: false
      
      type [Types::Payments::PaymentPlanType], null: true

      def resolve(filters: nil)
        current_ability = context[:current_ability]
        return initialize_response.failure(403, [['user', 'is required']]) unless current_ability

        payment_plans = ::PaymentPlan.accessible_by(current_ability)
        payment_plans = payment_plans.where(filters.to_h) if filters 

        payment_plans.uniq
      end

      private
      
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
