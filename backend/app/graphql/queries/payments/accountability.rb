module Queries
  module Payments
    class Accountability < Queries::BaseQuery
      description 'Find an Owner by ID'

      argument :id, ID, required: true
      argument :period, String, required: false

      type Types::Payments::AccountabilityType, null: true

      def resolve(id:, period: 'current')
        current_user = context[:current_user]
        return initialize_response.failure(403, [['current_user', 'is required']]) unless current_user

        property_ids = current_user.selected_agency.properties.pluck(:id)

        user = ::User.find_by(id: id)

        contract_ids = user.owned_contracts.where(property_id: property_ids).pluck(:id)

        if (period.to_i > 1)
          payments = ::Payment.current.preload(:referral).where(parent_id: nil, contract_id: contract_ids, concept: 'Alquiler').where.not(state: :deleted)
          ids = payments.pluck(:tax_id)
          ::Tax.where(id: ids).map{|x| x.create_payment([period.to_i])}

          payments = ::Payment.current.preload(:referral).where(parent_id: nil, referral: user).where('contract_id IN (?) OR contract_id IS NULL', contract_ids).where.not(state: :deleted)
          ids = payments.pluck(:tax_id)
          ::Tax.where(id: ids).map{|x| x.create_payment([period.to_i])}

          payments = ::Payment.current.where(parent_id: nil, payable_type: 'Properties::Fix', referral_id: id, referral_type: 'User').where.not(state: :deleted)
          ids = payments.pluck(:tax_id)
          ::Tax.where(id: ids).map{|x| x.create_payment([period.to_i])}

          payments = ::Payment.current.where(parent_id: nil).where('receiver_id = ? OR (contract_id IN (?) AND referral_id = ? AND referral_type = ? AND NOT concept = ?)', id, contract_ids, id, 'User', 'Alquiler').where.not(state: :deleted)
          ids = payments.pluck(:tax_id)
          ::Tax.where(id: ids).map{|x| x.create_payment([period.to_i])}
        end

        payments = ::Payment.current(period).preload(:referral).where(parent_id: nil, contract_id: contract_ids, concept: 'Alquiler').where.not(state: :deleted)

        owned = ::Payment.current(period).preload(:referral).where(parent_id: nil, referral: user).where('contract_id IN (?) OR contract_id IS NULL', contract_ids).where.not(state: :deleted)

        pending_ids = owned.pluck(:id)

        payments = ::Payment.current(period).where(parent_id: nil, payable_type: 'Properties::Fix', referral_id: id, referral_type: 'User').where.not(state: :deleted)

        pending_ids += payments.pluck(:id)

       #payments.group_by(&:tax_id).values.map do |gr|
       #  owned = gr.select { |payment| payment.referral_type == 'Agency' }
       #  owned.each_with_index do |aux, idx|
       #    next unless aux
       #    if aux.state == 'pending' 
       #      pending << aux
       #    else
       #      paid << aux
       #    end
       #  end
       #end

        paid_ids = []

        paid = ::Payment.where(id: paid_ids)

        payments = ::Payment.current(period).where(parent_id: nil).where('receiver_id = ? OR (contract_id IN (?) AND referral_id = ? AND referral_type = ? AND NOT concept = ?)', id, contract_ids, id, 'User', 'Alquiler').where.not(state: :deleted)

        pending_ids += payments.pluck(:id)

        pending = ::Payment.includes(:children, :sub_account, :chargebacks).where(id: pending_ids.uniq).order('payments.contract_tag ASC')

        chargebacks = ::Payments::Chargeback.where(active: true, payment_id: paid.pluck(:id) + pending.pluck(:id)).pluck(:amount).sum

        { paid: paid, pending: pending, chargebacks: chargebacks }
      end

      private

      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
