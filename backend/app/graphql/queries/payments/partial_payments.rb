module Queries
  module Payments
    class PartialPayments < Queries::BaseQuery
      description 'Find all Partial Payments'

      argument :filters, Types::Payments::PartialPaymentFilterType, required: false
      
      type [Types::Payments::PartialPaymentType], null: true

      def resolve(filters: nil)
        current_ability = context[:current_ability]
        return initialize_response.failure(403, [['user', 'is required']]) unless current_ability

        partial_payments = ::PartialPayment.accessible_by(current_ability)
        partial_payments = partial_payments.where(filters.to_h) if filters 

        partial_payments.uniq
      end

      private
      
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
