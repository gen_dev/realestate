module Queries
  module SimpleDatas
    class SimpleData < Queries::BaseQuery
      description 'Find a Simple Data by ID'

      argument :id, ID, required: true

      type Types::SimpleDatas::SimpleDataType, null: true

      def resolve(id:)
        current_user = context[:current_user]
        return initialize_response.failure(403, [['user', 'is required']]) unless current_user

        simple_data = ::SimpleData.find_by(id: id)
        return initialize_response.failure(500, [['user', 'unauthorized']]) if current_user.cannot?(:manage, simple_data)

        simple_data
      end

      private

      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
