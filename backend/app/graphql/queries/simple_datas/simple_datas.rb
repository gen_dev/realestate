module Queries
  module SimpleDatas
    class SimpleDatas < Queries::BaseQuery
      description 'Find all Simple Datas'

      argument :filters, Types::SimpleDatas::SimpleDataFilterType, required: false
      
      type [Types::SimpleDatas::SimpleDataType], null: true
      
      def resolve(filters: nil)
        current_ability = context[:current_ability]
        return initialize_response.failure(403, [['user', 'is required']]) unless current_ability

        simple_datas = ::SimpleData.accessible_by(current_ability)
        simple_datas = simple_datas.where(filters.to_h) if filters 

        simple_datas.uniq
      end

      private
      
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
