module Queries
  module Searches
    class UserResults < Queries::BaseQuery
      type [Types::Searches::SearchType], null: true

      def resolve
        current_user = context[:current_user]

        return initialize_response.failure(403, [['user', 'is required']]) unless current_user

        searches = current_user.selected_agency.searches

        searches = searches.joins(:search_results).group(:id).order('created_at DESC').uniq

        Thread.new do
          sleep(0.5)
          SearchResult.
            where(search_id: searches.pluck(:id)).
            where.not('(?) = ANY (seen_by)', current_user.selected_agency.id).
            update_all(['seen_by = array_append(seen_by, ?)', current_user.selected_agency.id])
        end

        searches
      end

      private
      
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
