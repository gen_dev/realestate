module Queries
  module Searches
    class AgencyResults < Queries::BaseQuery
      argument :page, ID, required: false

      type Types::Properties::PropertyListType, null: true

      def resolve(page: 1)
        current_user = context[:current_user]

        return initialize_response.failure(403, [['user', 'is required']]) unless current_user

        properties = (current_user.staff || current_user.selected_agency).properties.where(disabled: false, state: 'published').joins(:search_results).where('search_results.score > ?', 80).where.not('search_results.user_id IS NULL').distinct

        Thread.new do
          sleep(0.5)
          SearchResult.where(property_id: properties.pluck(:id)).update_all(['seen_by = array_append(seen_by, ?)', current_user.selected_agency.id])
        end

        properties = properties.page(page).per(10)

        { properties: properties, pages: properties.total_pages }
      end

      private
      
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
