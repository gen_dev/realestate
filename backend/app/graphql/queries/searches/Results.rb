module Queries
  module Searches
    class Results < Queries::BaseQuery
      argument :search_id, ID, required: true

      type [Types::Searches::SearchResultType], null: true

      def resolve(search_id:)
        current_user = context[:current_user]

        return initialize_response.failure(403, [['user', 'is required']]) unless current_user

        search = ::Search.find_by(id: search_id)

        results = search.results.where('score > ?', 80)

        results.order('score DESC')
      end

      private
      
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
