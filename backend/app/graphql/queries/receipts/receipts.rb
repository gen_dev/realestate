module Queries
  module Receipts
    class Receipts < Queries::BaseQuery
      description 'Find an Agency by ID'

      argument :id, ID, required: true
      argument :date, String, required: false

      type [Types::Receipts::ReceiptType], null: true

      def resolve(id:, date: nil)
        current_user = context[:current_user]
        return initialize_response.failure(403, [['user', 'is required']]) unless current_user

        user = ::User.find_by(id: id)

        receipts = user.receipts.preload(payments: [:price, :tax, :partial_payments, :sub_account])

        if date
          date = date.split("-").map(&:to_i).map(&:to_s).join

          receipts = receipts.where(stamp: date)
        end

        receipts
      end

      private

      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
