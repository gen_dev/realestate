module Queries
  module Receipts
    class Receipt < Queries::BaseQuery
      description 'Find an Agency by ID'

      argument :id, ID, required: true

      type Types::Receipts::ReceiptType, null: true

      def resolve(id:)
        current_user = context[:current_user]
        return initialize_response.failure(403, [['user', 'is required']]) unless current_user

        receipt = ::Receipt.find_by(id: id)
        return initialize_response.failure(500, [['user', 'unauthorized']]) if current_user.cannot?(:manage, receipt)

        receipt
      end

      private

      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
