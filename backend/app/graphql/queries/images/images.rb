module Queries
  module Images
    class Images < Queries::BaseQuery
      description 'Find all Images'

      argument :filters, Types::Images::ImageFilterType, required: false
      
      type [Types::Images::ImageType], null: true
      
      def resolve(filters: nil)
        current_ability = context[:current_ability]
        return initialize_response.failure(403, [['user', 'is required']]) unless current_ability

        images = ::Image.accessible_by(current_ability)
        images = images.where(filters.to_h) if filters 

        images.uniq
      end

      private
      
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
