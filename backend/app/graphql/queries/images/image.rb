module Queries
  module Images
    class Image < Queries::BaseQuery
      description 'Find a Image by ID'

      argument :id, ID, required: true

      type Types::Images::ImageType, null: true

      def resolve(id:)
        current_user = context[:current_user]
        return initialize_response.failure(403, [['user', 'is required']]) unless current_user

        image = ::Image.find_by(id: id)
        return initialize_response.failure(500, [['user', 'unauthorized']]) if current_user.cannot?(:manage, image)

        image
      end

      private
      
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
