module Queries
  module Agencies
    class Agencies < Queries::BaseQuery
      description 'Find all Agencies'

      argument :filters, Types::Agencies::AgencyFilterType, required: false

      type [Types::Agencies::AgencyType], null: true

      def resolve(filters: nil)
        current_ability = context[:current_ability]
        return initialize_response.failure(403, [['user', 'is required']]) unless current_ability

        agencies = ::Agency.accessible_by(current_ability)
        agencies = agencies.where(filters.to_h) if filters 

        agencies.uniq
      end

      private
      
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
