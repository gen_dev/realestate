module Queries
  module Agencies
    class Agency < Queries::BaseQuery
      description 'Find an Agency by ID'

      argument :id, ID, required: true

      type Types::Agencies::AgencyType, null: true

      def resolve(id:)
        agency = ::Agency.find_by(id: id)

        agency
      end

      private

      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
