module Queries
  module Agencies
    class Dashboard < Queries::BaseQuery
      description 'Find an Agency by ID'

      argument :date, [String], required: false

      type Types::Agencies::DashboardType, null: true

      def resolve(date: nil)
        current_user = context[:current_user] 
        return initialize_response.failure(403, [['current_user', 'is required']]) unless current_user

        if date
        else
          date = [Date.today.beginning_of_month, Date.today]
        end

        agency = current_user.selected_agency

        contracts = agency.contracts

        owners = agency.organization.users.joins(:roles).where('roles.resource_type = ? AND roles.resource_id IN (?)', 'Contract', contracts.pluck(:id))

        pending_to_pay = calculate_pending_to_pay(owners)
        average_payday = calculate_average_payday(owners, date)

        { pending_to_pay: pending_to_pay, average_payday: average_payday }
      end

      private

      def calculate_pending_to_pay(owners)
        payments = ::Payment.where(receiver_id: owners.pluck(:id), paid_at: nil).where('due_date < ?', Date.today)
        payments.count
      end

      def calculate_average_payday(owners, date)
        payments = ::Payment.where(receiver_id: owners.pluck(:id)).where(paid_at: date[0]..date[1])
        avg = payments.group_by{|payment| payment.paid_at.to_date}.transform_values(&:count)

        avg.sort.to_h
      end

      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
