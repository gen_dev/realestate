module Queries
  module Buildings
    class Building < Queries::BaseQuery
      description 'Find an Agency by ID'

      argument :id, ID, required: true

      type Types::Buildings::BuildingType, null: true

      def resolve(id:)
        current_user = context[:current_user]
        return initialize_response.failure(403, [['user', 'is required']]) unless current_user

        agency = current_user.selected_agency

        building = agency.buildings.find_by(id: id)

        building
      end

      private

      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
