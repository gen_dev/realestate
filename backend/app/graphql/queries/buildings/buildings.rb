module Queries
  module Buildings
    class Buildings < Queries::BaseQuery
      description 'Find all Agencies'

      type [Types::Buildings::BuildingType], null: true

      def resolve
        current_user = context[:current_user]
        return initialize_response.failure(403, [['user', 'is required']]) unless current_user

        agency = current_user.selected_agency

        agency.buildings.order('id DESC')
      end

      private
      
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
