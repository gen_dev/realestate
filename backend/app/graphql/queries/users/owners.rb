module Queries
  module Users
    class Owners < Queries::BaseQuery
      description 'Find all owners'

      argument :filters, Types::Users::UserFilterType, required: false

      type [Types::Users::UserType], null: true

      def resolve(filters: nil)
        current_user = context[:current_user]

        return initialize_response.failure(403, [['user', 'is required']]) unless current_user

        properties = ::Property.where(agency_id: current_user.agencies.pluck(:id)).pluck(:id)
        contracts = ::Contract.where(property_id: properties, state: 'ongoing').pluck(:id)

        users = ::User.joins(:roles).where('roles.name = ? AND roles.resource_type = ? AND resource_id IN (?)', 'owner', 'Contract', contracts).distinct
        users = users.where(filters.to_h) if filters 

        users.uniq
      end

      private
      
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
