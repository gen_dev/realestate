module Queries
  module Users
    class Users < Queries::BaseQuery
      description 'Find all Users'

      argument :filters, Types::Users::UserFilterType, required: false

      type [Types::Users::UserType], null: true

      def resolve(filters: nil)
        current_user = context[:current_user]

        return initialize_response.failure(403, [['user', 'is required']]) unless current_user

        users = ::User.joins(:roles).where('roles.resource_id = ? AND roles.resource_type = ?', current_user.selected_agency.id, 'Agency').distinct
        users = users.where(filters.to_h) if filters 

        users.uniq
      end

      private
      
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
