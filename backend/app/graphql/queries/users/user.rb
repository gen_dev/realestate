module Queries
  module Users
    class User < Queries::BaseQuery
      description 'Find an User by ID'

      argument :id, ID, required: true

      type Types::Users::UserType, null: true

      def resolve(id:)
        current_user = context[:current_user]
        return initialize_response.failure(403, [['current_user', 'is required']]) unless current_user

        user = ::User.find_by(id: id)
        return initialize_response.failure(500, [['user', 'unauthorized']]) if current_user.cannot?(:manage, user)

        user
      end

      private

      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
