module Queries
  module Users
    class UserTaxes < Queries::BaseQuery
      description 'Find all owners'

      argument :id, ID, required: true

      type [Types::Taxes::TaxType], null: true

      def resolve(id:)
        current_user = context[:current_user]

        return initialize_response.failure(403, [['user', 'is required']]) unless current_user

        user = ::User.find_by(id: id)

        user.taxes
      end

      private
      
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
