module Queries
  module Staff
    class Staff < Queries::BaseQuery
      description 'Find all Taxes'
      
      type [Types::Staff::StaffType], null: true
      
      def resolve
        current_user = context[:current_user]
        return initialize_response.failure(403, [['user', 'is required']]) unless current_user

        agency = current_user.selected_agency

        staff = agency.organization.staff

        staff.order("name asc")
      end

      private
      
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
