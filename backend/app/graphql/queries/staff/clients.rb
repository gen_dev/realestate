module Queries
  module Staff
    class Clients < Queries::BaseQuery
      description 'Find all Properties by owner'

      argument :id, ID, required: true

      type Types::Users::ContactListType, null: true

      def resolve(id:, page: 1, per_page: 10, no_paginate: true)
        current_user = context[:current_user]
        return initialize_response.failure(403, [['user', 'is required']]) unless current_user
        staff = ::Staff.find_by(id: id)

        clients = staff.clients

        clients = clients.order('id DESC')

        unless no_paginate
          clients = clients.distinct.page(page).per(per_page)
        end

        { contacts: clients, pages: clients.try(:total_pages) || 1 }
      end

      private
      
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
