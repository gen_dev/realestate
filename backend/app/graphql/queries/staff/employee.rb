module Queries
  module Staff
    class Employee < Queries::BaseQuery
      description 'Find a Tax by ID'

      argument :id, ID, required: true

      type Types::Staff::StaffType, null: true

      def resolve(id:)
        current_user = context[:current_user]
        return initialize_response.failure(403, [['user', 'is required']]) unless current_user

        staff = ::Staff.find_by(id: id)

        staff
      end

      private

      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
