module Queries
  module Staff
    class AssignedProperties < Queries::BaseQuery
      description 'Find all Properties by owner'

      argument :id, ID, required: true

      type Types::Properties::PropertyListType, null: true

      def resolve(id:, page: 1, per_page: 10, no_paginate: true)
        current_user = context[:current_user]
        return initialize_response.failure(403, [['user', 'is required']]) unless current_user
        agency = current_user.agencies.pluck(:id)

        properties = ::Property.where(agency_id: agency)

        staff = ::Staff.find_by(id: id)

        properties = staff.properties

        properties = properties.order('id DESC')

        unless no_paginate
          properties = properties.distinct.page(page).per(per_page)
        end

        { properties: properties, pages: properties.try(:total_pages) || 1 }
      end

      private
      
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
