module Queries
  module Socials
    class Socials < Queries::BaseQuery
      description 'Find all social links on Agency'

      type [Types::Socials::SocialType], null: true

      def resolve
        current_user = context[:current_user]
        return initialize_response.failure(403, [['user', 'is required']]) unless current_user

        current_user.selected_agency.socials
      end

      private
      
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
