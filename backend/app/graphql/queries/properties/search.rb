module Queries
  module Properties
    class Search < Queries::BaseQuery
      description 'Find all Properties'

      argument :id, ID, required: false
      argument :page, ID, required: false
      argument :attributes, Types::Searches::SearchInputType, required: false

      type Types::Searches::SearchListResultType, null: true

      def resolve(attributes: nil, id: nil, page: 1)
        # Falta baths, incorporar size, common_expenses y features

        if id
          search = ::Search.find_by(id: id)
        else
          search = ::Search.find_or_create_by(attributes.to_h)
        end

        results = search.results.where(active: true).joins(:property)

        min_score = id ? 80 : 0

        splitted_results = results.order('score DESC').group_by { |result| result.score > min_score}
        splitted_results[:id] = search.id
        splitted_results[:results] = splitted_results.delete true
        splitted_results[:recommended] = splitted_results.delete false

        splitted_results
      end

      private
      
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
