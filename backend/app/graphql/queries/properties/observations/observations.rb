module Queries
  module Properties
    module Observations
      class Observations < Queries::BaseQuery
        description 'Find a Property by ID'

        argument :id, ID, required: true

        type [Types::Properties::Observations::ObservationType], null: true

        def resolve(id:)
          current_user = context[:current_user]

          property = ::Property.find_by(id: id)

          property.observations.order('created_at DESC')
        end

        private

        def initialize_response
          ResponseService.new(self.field&.name, context)
        end
      end
    end
  end
end
