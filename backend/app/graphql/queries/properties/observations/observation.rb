module Queries
  module Properties
    module Observations
      class Observation < Queries::BaseQuery
        description 'Find a Property by ID'

        argument :id, ID, required: true

        type Types::Properties::Observations::ObservationType, null: true

        def resolve(id:)
          current_user = context[:current_user]

          observation = ::Properties::Observation.find_by(id: id)

          observation
        end

        private

        def initialize_response
          ResponseService.new(self.field&.name, context)
        end
      end
    end
  end
end
