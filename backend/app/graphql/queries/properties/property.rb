module Queries
  module Properties
    class Property < Queries::BaseQuery
      description 'Find a Property by ID'

      argument :id, ID, required: true

      type Types::Properties::PropertyType, null: true

      def resolve(id:)
        current_user = context[:current_user]

        property = ::Property.find_by(id: id)

        property
      end

      private
      
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
