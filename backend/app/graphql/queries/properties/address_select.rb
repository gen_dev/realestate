module Queries
  module Properties
    class AddressSelect < Queries::BaseQuery
      description 'Find all Properties::Fix'

      argument :agency_ids, [ID], required: true

      type [String], null: true

      def resolve(agency_ids:)
        properties = ::Property.where(agency_id: agency_ids).joins(:address)

        hs = []

        address = properties.map(&:address_second_line)

        address
      end

      private
      
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
