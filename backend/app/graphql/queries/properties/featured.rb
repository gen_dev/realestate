module Queries
  module Properties
    class Featured < Queries::BaseQuery
      description 'Find all Properties'

      argument :agency_ids, [ID], required: true

      type [Types::Properties::PropertyType], null: true

      def resolve(agency_ids:, operation_type: nil)
        properties = ::Property.where(agency_id: agency_ids, disabled: false, featured: true)

        properties = properties.where(state: 'published')

        properties.reorder('created_at DESC')
      end

      private
      
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
