module Queries
  module Properties
    class Cities < Queries::BaseQuery
      description 'Find all Properties'

      argument :operation_type, String, required: false

      type [Types::SimpleDatas::SimpleDataType], null: true

      def resolve(operation_type: nil)
        properties = ::Property.all.where(disabled: false, state: 'published')

        properties = properties.where("features->'operation_type'->>'#{operation_type.downcase}' = ?", "true") if operation_type 

        ids = properties.pluck(:id)

        address = ::Address.where(addressable_type: 'Property', addressable_id: ids)

        arr = address.pluck(:city, :state).map { |address| address.join(', ')}

        arr = arr.map do |el|
          {
            name: el
          }
        end

        arr
      end

      private
      
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
