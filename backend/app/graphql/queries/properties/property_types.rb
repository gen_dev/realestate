module Queries
  module Properties
    class PropertyTypes < Queries::BaseQuery
      description 'Find all Properties'

      argument :agency_ids, [ID], required: true
      argument :operation_type, String, required: false

      type [Types::SimpleDatas::SimpleDataType], null: true

      def resolve(agency_ids:, operation_type: nil)
        properties = ::Property.where(agency_id: agency_ids)

        properties = properties.where("features->'operation_type'->>'#{operation_type.downcase}' = ?", "true") if operation_type 

        contracts = Contract.where('state = ?', 'ongoing')
        ids = contracts.pluck(:property_id)
        properties = properties.where.not(id: ids, state: 'draft')

        arr = properties.pluck(:property_type)

        arr = arr.map do |el|
          {
            name: el
          }
        end

        arr
      end

      private
      
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
