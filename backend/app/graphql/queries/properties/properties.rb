module Queries
  module Properties
    class Properties < Queries::BaseQuery
      description 'Find all Properties'

      argument :filters, Types::Properties::PropertyFilterType, required: false
      argument :search, String, required: false
      argument :free, Boolean, required: false
      argument :contract_id, ID, required: false
      argument :operation_type, String, required: false
      argument :page, Integer, required: false
      argument :per_page, Integer, required: false
      argument :no_paginate, Boolean, required: false

      type Types::Properties::PropertyListType, null: true

      def resolve(search: nil, filters: nil, operation_type: nil, free: nil, contract_id: nil, page: 1, per_page: 10, no_paginate: false)
        current_user = context[:current_user]
        return initialize_response.failure(403, [['user', 'is required']]) unless current_user
        agency = current_user.agencies.pluck(:id)

        properties = ::Property.where(agency_id: agency)

        if filters
          properties = properties.where(price: (filters[:price_range].min..filters[:price_range].max)) if filters[:price_range]
          nfilters = { property_type: filters[:property_type], state: filters[:state], disabled: filters[:disabled] }.delete_if { |k, v| v.blank? } if filters
          properties = properties.where(nfilters) if nfilters.any? 
        end

        properties = properties.where("features->'operation_type'->>'#{operation_type.downcase}' = ?", "true") if operation_type 

        if search.present?
          properties = properties.left_joins(:address).
            where('(LOWER(unaccent(properties.description)) LIKE :q)
            OR (LOWER(unaccent(addresses.street)) LIKE :q)
            OR (LOWER(unaccent(addresses.number)) LIKE :q)
            OR (LOWER(unaccent(addresses.unit)) LIKE :q)', q: "%#{I18n.transliterate(search.downcase)}%")
        end 

        if false
          contracts = Contract.where('state = ?', 'ongoing')
          contracts = contracts.where.not(id: contract_id) if contract_id
          ids = contracts.pluck(:property_id)
          properties = properties.where.not(id: ids)
        end

        properties = properties.order('id DESC')

        unless no_paginate
          properties = properties.distinct.page(page).per(per_page)
        end

        { properties: properties, pages: properties.try(:total_pages) || 1 }
      end

      private
      
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
