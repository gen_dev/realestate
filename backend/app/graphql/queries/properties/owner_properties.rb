module Queries
  module Properties
    class OwnerProperties < Queries::BaseQuery
      description 'Find all Properties by owner'

      argument :owner_id, ID, required: true

      type Types::Properties::PropertyListType, null: true

      def resolve(owner_id:, page: 1, per_page: 10, no_paginate: true)
        current_user = context[:current_user]
        return initialize_response.failure(403, [['user', 'is required']]) unless current_user
        agency = current_user.agencies.pluck(:id)

        properties = ::Property.where(agency_id: agency)

        owner = ::User.find_by(id: owner_id)

        ids = owner.roles.where(resource_type: "Property").pluck(:resource_id)

        properties = properties.where(id: ids)

        properties = properties.order('id DESC')

        unless no_paginate
          properties = properties.distinct.page(page).per(per_page)
        end

        { properties: properties, pages: properties.try(:total_pages) || 1 }
      end

      private
      
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
