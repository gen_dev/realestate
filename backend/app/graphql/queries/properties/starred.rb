module Queries
  module Properties
    class Starred < Queries::BaseQuery
      description 'Find all Properties'

      argument :agency_ids, [ID], required: true
      argument :operation_type, String, required: false

      type [Types::Properties::PropertyType], null: true

      def resolve(agency_ids:, operation_type: nil)
        properties = ::Property.where(agency_id: agency_ids, disabled: false)

        properties = properties.where(state: 'published')

        properties = properties.where("features->'operation_type'->>'#{operation_type.downcase}' = ?", "true") if operation_type 

        properties.reorder('updated_at DESC').limit(6)
      end

      private
      
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
