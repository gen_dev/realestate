module Queries
  module Properties
    class Fixes < Queries::BaseQuery
      description 'Find all Properties::Fix'

      argument :property_id, ID, required: true

      argument :filters, Types::Properties::PropertyFilterType, required: false
      argument :search, String, required: false
      argument :free, Boolean, required: false
      argument :contract_id, ID, required: false
      argument :operation_type, String, required: false
      argument :page, Integer, required: false
      argument :per_page, Integer, required: false

      type [Types::Properties::FixType], null: true

      def resolve(search: nil, filters: nil, operation_type: nil, free: nil, contract_id: nil, page: 1, per_page: 10, property_id:)
        current_user = context[:current_user]
        return initialize_response.failure(403, [['user', 'is required']]) unless current_user
        agency = current_user.agencies.pluck(:id)

        property = ::Property.find_by(id: property_id)

        fixes = property.fixes

        fixes
      end

      private
      
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
