module Queries
  module SubAccounts
    class SubAccounts < Queries::BaseQuery
      description 'Find all SubAccount'

      argument :filters, Types::SubAccounts::SubAccountFilterType, required: false
      argument :isolated, Boolean, required: false
      argument :account_id, ID, required: false

      type [Types::SubAccounts::SubAccountType], null: true

      def resolve(filters: nil, isolated: true, account_id: nil)
        current_ability = context[:current_ability]
        current_user = context[:current_user]
        return initialize_response.failure(403, [['user', 'is required']]) unless current_ability

        sub_accounts = current_user.selected_agency.sub_accounts.accessible_by(current_ability)
        sub_accounts = sub_accounts.where(account_id: account_id) if account_id
        sub_accounts = sub_accounts.where(filters.to_h) if filters 

        sub_accounts = sub_accounts.where(account_id: nil) if (isolated)

        sub_accounts.uniq
      end

      private
      
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
