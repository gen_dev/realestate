module Queries
  module Documents
    class Document < Queries::BaseQuery
      description 'Find a Document by ID'

      argument :id, ID, required: true

      type Types::Documents::DocumentType, null: true

      def resolve(id:)
        current_user = context[:current_user]
        return initialize_response.failure(403, [['user', 'is required']]) unless current_user

        document = ::Document.find_by(id: id)
        return initialize_response.failure(500, [['user', 'unauthorized']]) if current_user.cannot?(:manage, document)

        document
      end

      private
      
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
