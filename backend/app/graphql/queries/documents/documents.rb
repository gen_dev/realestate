module Queries
  module Documents
    class Documents < Queries::BaseQuery
      description 'Find all Documents'

      argument :filters, Types::Documents::DocumentFilterType, required: false
      
      type [Types::Documents::DocumentType], null: true
      
      def resolve(filters: nil)
        current_ability = context[:current_ability]
        return initialize_response.failure(403, [['user', 'is required']]) unless current_ability

        documents = ::Document.accessible_by(current_ability)
        documents = documents.where(filters.to_h) if filters 

        documents.uniq
      end

      private
      
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
