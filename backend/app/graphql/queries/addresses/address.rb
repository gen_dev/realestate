module Queries
  module Addresses
    class Address < Queries::BaseQuery
      description 'Find an Address by ID'

      argument :id, ID, required: true

      type Types::Addresses::AddressType, null: true

      def resolve(id:)
        current_user = context[:current_user]
        return initialize_response.failure(403, [['user', 'is required']]) unless current_user

        address = ::Address.find_by(id: id)
        return initialize_response.failure(500, [['user', 'unauthorized']]) if current_user.cannot?(:manage, address)

        address
      end

      private

      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
