module Queries
  module Addresses
    class Addresses < Queries::BaseQuery
      description 'Find all Addresses'

      argument :filters, Types::Addresses::AddressFilterType, required: false

      type [Types::Addresses::AddressType], null: true

      def resolve(filters: nil)
        current_ability = context[:current_ability]
        return initialize_response.failure(403, [['user', 'is required']]) unless current_ability

        addresses = ::Address.accessible_by(current_ability)
        addresses = addresses.where(filters.to_h) if filters 

        addresses.uniq
      end

      private
      
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
