module Queries
  module Inquiries
    class Comments < Queries::BaseQuery
      description 'Find all Contacts'

      argument :id, ID, required: true
      
      type [Types::Comments::CommentType], null: true

      def resolve(id:)
        current_user = context[:current_user] 
        return initialize_response.failure(403, [['current_user', 'is required']]) unless current_user
        # return initialize_response.failure(403, [['user', 'unauthorized']]) unless current_user.display_roles.include?('shibui')

        inquiry = ::Inquiry.find_by(id: id)

        return initialize_response.failure(404, [['inquiry', 'not found']]) unless inquiry

        comments = inquiry.comments

        comments
      end

      private
      
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
