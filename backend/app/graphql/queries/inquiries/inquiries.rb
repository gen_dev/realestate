module Queries
  module Inquiries
    class Inquiries < Queries::BaseQuery
      description 'Find all Contacts'

      argument :page, ID, required: false
      argument :per_page, ID, required: false
      argument :type, String, required: false
      argument :mine, Boolean, required: false
      argument :manual, Boolean, required: false
      argument :owner_id, ID, required: false
      argument :user_id, ID, required: false
      argument :status, [String], required: false
      
      type Types::Inquiries::InquiriesListType, null: true

      def resolve(page: 1, per_page: 10, type: nil, mine: false, owner_id: nil, user_id: nil, status: nil, manual: false)
        current_user = context[:current_user] 
        return initialize_response.failure(403, [['current_user', 'is required']]) unless current_user
        # return initialize_response.failure(403, [['user', 'unauthorized']]) unless current_user.display_roles.include?('shibui')

        if type == "global"
          inquiries = ::Inquiry.where(property_id: nil, investment_type: nil)
        elsif type == "investments"
          inquiries = ::Inquiry.where.not(investment_type: nil)
        elsif type == "all"
          inquiries = ::Inquiry.all
        else
          inquiries = ::Inquiry.where.not(property_id: nil)
        end

        if mine
          inquiries = inquiries.where(user_id: current_user.id)
        end

        if user_id
          staff = ::Staff.find(user_id)
          inquiries = staff.inquiries
        end

        unless status.blank?
          inquiries = inquiries.where(state: status)
        end

        if owner_id
          inquiries = inquiries.where(contact_id: owner_id)
        else
          inquiries = inquiries.where(manual: manual)
        end

        inquiries = inquiries.order('created_at DESC').page(page).per(per_page)

        Thread.new do
          sleep 3
          inquiries.update_all(read: true)
        end

        { inquiries: inquiries, pages: inquiries.try(:total_pages) || 1 }
      end

      private
      
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
