module Queries
  module UserAgencies
    class UserAgency < Queries::BaseQuery
      description 'Find an User by ID'

      argument :id, ID, required: true

      type Types::UserAgencies::UserAgencyType, null: true

      def resolve(id:)
        current_user = context[:current_user]
        return initialize_response.failure(403, [['user', 'is required']]) unless current_user

        user_agency = ::UserAgency.find_by(id: id)
        return initialize_response.failure(500, [['user', 'unauthorized']]) if current_user.cannot?(:manage, user_agency)

        user_agency
      end

      private

      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
