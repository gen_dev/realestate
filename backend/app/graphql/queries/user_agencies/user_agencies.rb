module Queries
  module UserAgencies
    class UserAgencies < Queries::BaseQuery
      description 'Find all User Agencies'
      
      argument :filters, Types::UserAgencies::UserAgencyFilterType, required: false
      
      type [Types::UserAgencies::UserAgencyType], null: true

      def resolve(filters: nil)
        current_user = context[:current_user]
        return initialize_response.failure(403, [['user', 'is required']]) unless current_user

        user_agencies = current_user.user_agencies
        user_agencies = user_agencies.where(filters.to_h) if filters 

        user_agencies.uniq
      end

      private
      
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
