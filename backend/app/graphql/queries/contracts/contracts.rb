module Queries
  module Contracts
    class Contracts < Queries::BaseQuery
      description 'Find all Contracts'
      
      argument :filters, Types::Contracts::ContractFilterType, required: false
      argument :search, String, required: false
      argument :operation_type, String, required: false
      argument :page, Integer, required: false
      argument :per_page, Integer, required: false

      type Types::Contracts::ContractListType, null: true

      def resolve(search: nil, filters: nil, operation_type: nil, page: 1, per_page: 10)
        current_user = context[:current_user]
        return initialize_response.failure(403, [['user', 'is required']]) unless current_user
        agency = current_user.selected_agency

        properties = agency.properties

        contracts = ::Contract.where(property_id: properties.pluck(:id))
        contracts = contracts.where(contract_type: operation_type) if operation_type 

        if filters
          if filters[:owner] || filters[:tenant]
            contract_ids_1 = User.joins(:profile, :roles).where('users.id = ? AND roles.name = ? AND roles.resource_type = ?', filters[:owner], 'owner', 'Contract').pluck('roles.resource_id') if filters[:owner]
            contract_ids_2 = User.joins(:profile, :roles).where('users.id = ? AND roles.name = ? AND roles.resource_type = ?', filters[:tenant], 'tenant', 'Contract').pluck('roles.resource_id') if filters[:tenant]

            if contract_ids_1.present? && contract_ids_2.present?
              contract_ids = (contract_ids_1 & contract_ids_2).flatten.uniq
            elsif contract_ids_1.present? && contract_ids_2.blank? 
              contract_ids = contract_ids_1 
            elsif contract_ids_2.present? && contract_ids_1.blank? 
              contract_ids = contract_ids_2
            end            

            if contract_ids
              contract_ids = contract_ids.flatten.uniq 
            end

            contracts = contracts.where(id: contract_ids)  

            return { contracts: contracts, pages: contracts.total_pages } unless contracts
          end

          nfilters = { state: filters[:state] }.delete_if { |k, v| v.blank? } if filters
          contracts = contracts.where(nfilters) if nfilters.any? 

          return { contracts: contracts, pages: contracts.total_pages } unless contracts
        end

        if search.present?
          
          # keywords = I18n.transliterate(search.downcase).split # convert search term to keywords without accents or capital letters
          # keywords = keywords.map { |x| "%#{x}%" } # add % to search keywords to find "%keyword%" instead of "keyword"
          keywords = ["%#{search}%"]
          contracts = contracts.left_joins(property: :address).
            where('(LOWER(unaccent(addresses.street)) LIKE ANY (array[:q])) 
            OR (LOWER(unaccent(addresses.number)) LIKE ANY (array[:q])) 
            OR (LOWER(unaccent(addresses.unit)) LIKE ANY (array[:q]))', q: keywords) 
        end 

        contracts = contracts.order('id desc', 'state desc')

        contracts = contracts.distinct.page(page).per(per_page)

        { contracts: contracts, pages: contracts.total_pages, total_count: contracts.total_count }
      end

      private
      
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
