module Queries
  module Contracts
    class ContractExpense < Queries::BaseQuery
      description 'Find a Contract Expense by ID'

      argument :id, ID, required: true

      type Types::Contracts::ContractExpenseType, null: true

      def resolve(id:)
        current_user = context[:current_user]
        return initialize_response.failure(403, [['user', 'is required']]) unless current_user

        contract_expense = ::ContractExpense.find_by(id: id)
        return initialize_response.failure(500, [['user', 'unauthorized']]) if current_user.cannot?(:manage, contract_expense)

        contract_expense
      end

      private

      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end