module Queries
  module Contracts
    class Contract < Queries::BaseQuery
      description 'Find a Contract by ID'

      argument :id, ID, required: true

      type Types::Contracts::ContractType, null: true

      def resolve(id:)
        current_user = context[:current_user]
        return initialize_response.failure(403, [['user', 'is required']]) unless current_user

        contract = ::Contract.includes(payment_plan: [taxes: :payments], taxes: :payments).find_by(id: id)
        return initialize_response.failure(500, [['user', 'unauthorized']]) if current_user.cannot?(:manage, contract)

        contract
      end

      private

      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
