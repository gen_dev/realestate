module Queries
  module Contracts
    class OutdatedPrices < Queries::BaseQuery
      type [Types::Prices::PeriodPriceType], null: true

      def resolve
        current_user = context[:current_user]
        return initialize_response.failure(403, [['user', 'is required']]) unless current_user

        current_agency = current_user.selected_agency

        property_ids = ::Property.where(agency_id: current_agency.id)

        contract_ids = ::Contract.where(state: "ongoing", property: property_ids).pluck(:id)
        payment_plan_ids = ::PaymentPlan.where(contract_id: contract_ids)

        taxes = ::Tax.joins(:prices).where(name: 'Alquiler', taxable_type: 'PaymentPlan', taxable_id: payment_plan_ids)
        prices = ::Price::Period.where('amount = ? AND priceable_type = ? AND priceable_id IN (?) AND starts_at <= ? AND ends_at >= ?', 0, 'Tax', taxes.pluck(:id), 0.month.from_now, 0.month.from_now)

        prices
      end

      private

      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
