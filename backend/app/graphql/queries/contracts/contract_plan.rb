module Queries
  module Contracts
    class ContractPlan < Queries::BaseQuery
      description 'Find a Contract Plan by ID'

      argument :id, ID, required: true

      type Types::Contracts::ContractPlanType, null: true

      def resolve(id:)
        current_user = context[:current_user]
        return initialize_response.failure(403, [['user', 'is required']]) unless current_user

        contract_plan = ::ContractPlan.find_by(id: id)
        return initialize_response.failure(500, [['user', 'unauthorized']]) if current_user.cannot?(:manage, contract_plan)

        contract_plan
      end

      private

      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end