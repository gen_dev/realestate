module Queries
  module Contracts
    class AllContracts < Queries::BaseQuery
      description 'Find all Contracts'
      
      type [Types::Contracts::ContractType], null: true

      def resolve()
        current_user = context[:current_user]
        return initialize_response.failure(403, [['user', 'is required']]) unless current_user
        agency = current_user.selected_agency

        properties = agency.properties

        contracts = ::Contract.where(property_id: properties.pluck(:id), state: 'ongoing')

        contracts
      end

      private
      
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
