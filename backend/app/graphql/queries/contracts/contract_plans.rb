module Queries
  module Contracts
    class ContractPlans < Queries::BaseQuery
      description 'Find all ContractPlans'
      
      argument :filters, Types::Contracts::ContractPlanFilterType, required: false
      
      type [Types::Contracts::ContractPlanType], null: true

      def resolve(filters: nil)    
        current_ability = context[:current_ability]
        return initialize_response.failure(403, [['user', 'is required']]) unless current_ability

        contract_plans = ::ContractPlan.accessible_by(current_ability)
        contract_plans = contract_plans.where(filters.to_h) if filters 

        contract_plans.uniq
      end

      private
      
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
