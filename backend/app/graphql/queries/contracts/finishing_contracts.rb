module Queries
  module Contracts
    class FinishingContracts < Queries::BaseQuery
      type [Types::Contracts::ContractType], null: true

      def resolve
        current_user = context[:current_user]
        return initialize_response.failure(403, [['user', 'is required']]) unless current_user
        current_agency = current_user.selected_agency

        property_ids = ::Property.where(agency_id: current_agency.id)

        rescission_ids = ::Rescission.where(state: ["draft", "confirmed"]).pluck(:contract_id)

        contracts = ::Contract.
          where(state: 'ongoing', property_id: property_ids).
          where('ends_at < ?', 1.month.from_now.end_of_month).
          or(::Contract.where(state: 'ongoing', id: rescission_ids))

        contracts
      end

      private

      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
