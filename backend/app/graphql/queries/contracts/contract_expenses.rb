module Queries
  module Contracts
    class ContractExpenses < Queries::BaseQuery
      description 'Find all ContractExpenses'

      argument :filters, Types::Contracts::ContractExpenseFilterType, required: false

      type [Types::Contracts::ContractExpenseType], null: true

      def resolve(filters: nil)
        current_ability = context[:current_ability]
        return initialize_response.failure(403, [['user', 'is required']]) unless current_ability

        contract_expenses = ::ContractExpense.accessible_by(current_ability)
        contract_expenses = contract_expenses.where(filters.to_h) if filters 

        contract_expenses.uniq
      end

      private
      
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
