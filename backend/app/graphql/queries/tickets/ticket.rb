module Queries
  module Tickets
    class Ticket < Queries::BaseQuery
      description 'Find all Properties::Fix'

      argument :ticket_id, ID, required: true

      type Types::Tickets::TicketType, null: true

      def resolve(ticket_id:)
        current_user = context[:current_user]
        return initialize_response.failure(403, [['user', 'is required']]) unless current_user
        agency = current_user.agencies.pluck(:id)

        ticket = ::Ticket.find_by(id: ticket_id)

        ticket
      end

      private
      
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
