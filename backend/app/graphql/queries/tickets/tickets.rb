module Queries
  module Tickets
    class Tickets < Queries::BaseQuery
      description 'Find all Properties::Fix'

      argument :contract_id, ID, required: false
      argument :states, [String], required: false

      type [Types::Tickets::TicketType], null: true

      def resolve(contract_id: nil, states: nil)
        current_user = context[:current_user]
        return initialize_response.failure(403, [['user', 'is required']]) unless current_user
        agency = current_user.selected_agency

        tickets = contract_id ? ::Ticket.where(contract_id: contract_id) : ::Ticket.where(contract_id: agency.contracts.pluck(:id))

        unless states.blank?
          tickets = tickets.where(state: states)
        end

        tickets
      end

      private
      
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
