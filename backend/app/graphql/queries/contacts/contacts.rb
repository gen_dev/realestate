module Queries
  module Contacts
    class Contacts < Queries::BaseQuery
      description 'Find all Contacts'

      argument :filters, Types::Users::ProfileFilterType, required: false
      argument :search, String, required: false
      argument :roles, [String], required: false
      argument :not_roles, [String], required: false
      argument :no_paginate, Boolean, required: false
      argument :user_id, ID, required: false
      argument :page, Integer, required: false
      argument :per_page, Integer, required: false
      
      type Types::Users::ContactListType, null: true

      def resolve(search: nil, roles: [], filters: nil, not_roles: [], user_id: nil, page: 1, per_page: 10, no_paginate: false)
        current_user = context[:current_user] 
        return initialize_response.failure(403, [['current_user', 'is required']]) unless current_user
        agency = current_user.agencies.pluck(:id)
        return initialize_response.failure(404, [['agency', 'not found']]) unless agency

        roles = roles.blank? ? ['owner', 'tenant', 'buyer', 'seller', 'guarantor', 'contact'] : roles
        properties = ::Property.where(agency_id: agency)

        if not_roles.present?
          finished_ids = ::Contract.where('state = ? AND property_id IN (?)', 'finished', properties.pluck(:id)).pluck(:id)
          not_roles_users = User.joins(:roles).where('roles.name IN (?) AND NOT roles.resource_id IN (?) AND roles.resource_type = ?', not_roles, finished_ids, 'Contract') # e.g: non tenant users
          not_roles_users = not_roles_users.where.not(id: user_id) if user_id
          not_roles_users_ids = not_roles_users.pluck(:id)

          contacts = User.joins(:roles, :profile).where.not(id: not_roles_users_ids).where('roles.name IN (?) AND ((roles.resource_type = ? AND roles.resource_id IN (?)) OR (roles.resource_type = ? AND roles.resource_id IN (?)) OR (roles.resource_type = ? AND roles.resource_id IN (?)) )', roles, "Agency", agency, "Property", properties.pluck(:id), "Contract", Contract.joins(property: :agency).where('agencies.id IN (?)', agency).ids)
        else
          contacts = ::User.joins(:roles, :profile).where('roles.name IN (?) AND ((roles.resource_type = ? AND roles.resource_id IN (?)) OR (roles.resource_type = ? AND roles.resource_id IN (?)) OR (roles.resource_type = ? AND roles.resource_id IN (?)) )', roles, "Agency", agency, "Property", properties.pluck(:id), "Contract", Contract.joins(property: :agency).where('agencies.id IN (?)', agency).ids)
        end

        contacts = contacts.where('LOWER(users_profiles.name) LIKE LOWER(unaccent(:q)) 
        OR LOWER(users_profiles.email) LIKE LOWER(unaccent(:q)) 
        OR LOWER(users_profiles.phone) LIKE LOWER(unaccent(:q)) 
        OR LOWER(users_profiles.address) LIKE LOWER(unaccent(:q))', q: "%#{search.split(" ").join("%")}%") if search

        unless no_paginate
          contacts = contacts.distinct.page(page).per(per_page)
        end

        { contacts: contacts.includes(:roles, :profile), pages: contacts.try(:total_pages) || 1 }
      end

      private
      
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
