module Queries
  module Contacts
    class Contact < Queries::BaseQuery
      description 'Find a Contact by ID'

      argument :contact_id, ID, required: true
      
      type Types::Users::UserType, null: true

      def resolve(contact_id:)
        current_user = context[:current_user] 
        return initialize_response.failure(403, [['current_user', 'is required']]) unless current_user
        agencies = current_user.agencies.pluck(:id)
        return initialize_response.failure(404, [['agency', 'not found']]) if agencies.blank?
        contact = User.left_joins(:roles).where('roles.name IN (?) AND roles.resource_type = ? AND roles.resource_id IN (?) AND users.id = ?', ['owner', 'tenant', 'buyer', 'seller', 'guarantor', 'contact'], "Agency", agencies, contact_id).first

        contact
      end

      private
      
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
