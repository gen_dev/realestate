module Queries
  module Contacts
    class ListContacts < Queries::BaseQuery
      description 'Find all Contacts'

      argument :roles, [String], required: false
      argument :not_roles, [String], required: false
      argument :pending_rent, Boolean, required: false
      
      type [Types::Users::UserType], null: true

      def resolve(roles: [], not_roles: [], pending_rent: false)
        current_user = context[:current_user] 
        return initialize_response.failure(403, [['current_user', 'is required']]) unless current_user
        agency = current_user.selected_agency
        return initialize_response.failure(404, [['agency', 'not found']]) unless agency

        roles = roles.empty? ? ['owner', 'tenant', 'buyer', 'seller', 'guarantor', 'contact'] : roles
        properties = agency.properties

        if not_roles.present?
          finished_ids = agency.contracts.where(state: 'finished').pluck(:id)
          not_roles_users = User.joins(:roles).where('roles.name IN (?) AND NOT roles.resource_id IN (?) AND roles.resource_type = ?', not_roles, finished_ids, 'Contract') # e.g: non tenant users
          not_roles_users = not_roles_users.where.not(id: user_id) if user_id
          not_roles_users_ids = not_roles_users.pluck(:id)

          contacts = User.joins(:roles, :profile).where.not(id: not_roles_users_ids).where('roles.name IN (?) AND ((roles.resource_type = ? AND roles.resource_id = ?) OR (roles.resource_type = ? AND roles.resource_id IN (?)) OR (roles.resource_type = ? AND roles.resource_id IN (?)) )', roles, "Agency", agency.id, "Property", properties.pluck(:id), "Contract", Contract.joins(property: :agency).where('agencies.id = ?', agency.id).ids)
        else
          contacts = User.joins(:roles, :profile).where('roles.name IN (?) AND ((roles.resource_type = ? AND roles.resource_id = ?) OR (roles.resource_type = ? AND roles.resource_id IN (?)) OR (roles.resource_type = ? AND roles.resource_id IN (?)) )', roles, "Agency", agency.id, "Property", properties.pluck(:id), "Contract", Contract.joins(property: :agency).where('agencies.id = ?', agency.id).ids)
        end

        if pending_rent
          eom = Date.today.end_of_month
          bom = Date.today.beginning_of_month

          payments = ::Payment.where(concept: "Alquiler", referral_type: "User", due_date: bom..eom, state: 'pending')

          ids = payments.pluck(:referral_id)

          contacts = contacts.where(id: ids)
        end

        contacts.includes(:roles, :profile).order('users_profiles.name ASC')
      end

      private
      
      def initialize_response
        ResponseService.new(self.field&.name, context)
      end
    end
  end
end
