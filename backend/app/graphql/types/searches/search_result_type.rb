module Types
  module Searches
    class SearchResultType < Types::BaseObject
      field :id, ID, null: false
      field :created_at, GraphQL::Types::ISO8601Date, null: false
      field :seen_by, [ID], null: false
      field :property, Types::Properties::PropertyType, null: true
      field :agency, Types::Agencies::AgencyType, null: false
      field :search, Types::Searches::SearchType, null: false
      field :user, Types::Users::UserType, null: false
      field :score, Float, null: false

      field :state, String, null: true
      field :dropped, Boolean, null: true
      field :shared, Boolean, null: true
    end
  end
end
