module Types
  module Searches
    class SearchInputType < Types::BaseInputObject
      argument :agency_id, ID, required: false
      argument :user_id, ID, required: false
      argument :state, String, required: false
      argument :currency, String, required: false
      argument :min_brooms, ID, required: false
      argument :max_brooms, ID, required: false
      argument :min_rooms, ID, required: false
      argument :max_rooms, ID, required: false
      argument :min_baths, ID, required: false
      argument :max_baths, ID, required: false
      argument :min_price, ID, required: false
      argument :max_price, ID, required: false
      argument :min_covered_surface, ID, required: false
      argument :max_covered_surface, ID, required: false
      argument :min_surface, ID, required: false
      argument :max_surface, ID, required: false
      argument :min_common_expenses, ID, required: false
      argument :max_common_expenses, ID, required: false
      argument :operation_type, String, required: false
      argument :address, String, required: false
      argument :property_type, [String], required: false
      argument :features, GraphQL::Types::JSON, required: false
    end
  end
end
