module Types
  module Searches
    class SearchListResultType < Types::BaseObject
      field :id, ID, null: true
      field :results, [Types::Searches::SearchResultType], null: true
      field :recommended, [Types::Searches::SearchResultType], null: true
    end
  end
end
