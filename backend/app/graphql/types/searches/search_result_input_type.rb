module Types
  module Searches
    class SearchResultInputType < Types::BaseInputObject
      argument :dropped, Boolean, required: false
      argument :shared, Boolean, required: false
      argument :state, String, required: false
    end
  end
end
