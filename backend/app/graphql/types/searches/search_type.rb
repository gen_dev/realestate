module Types
  module Searches
    class SearchType < Types::BaseObject
      field :id, ID, null: false
      field :created_at, GraphQL::Types::ISO8601Date, null: false
      field :state, String, null: true
      field :results_count, Integer, null: true
      field :currency, String, null: true
      field :agency_id, ID, null: true
      field :unseen_results, Integer, null: true
      field :user_id, ID, null: true
      field :min_brooms, ID, null: true
      field :max_brooms, ID, null: true
      field :min_rooms, ID, null: true
      field :max_rooms, ID, null: true
      field :min_baths, ID, null: true
      field :max_baths, ID, null: true
      field :min_price, ID, null: true
      field :max_price, ID, null: true
      field :min_common_expenses, ID, null: true
      field :max_common_expenses, ID, null: true
      field :operation_type, String, null: true
      field :address, String, null: true
      field :property_type, [String], null: true
      field :features, GraphQL::Types::JSON, null: true
      field :results, [Types::Searches::SearchResultType], null: true
      field :user, Types::Users::UserType, null: true
      field :agency, Types::Agencies::AgencyType, null: true
    end
  end
end
