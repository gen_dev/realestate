module Types
  module Properties
    class FixType < Types::BaseObject
      field :id, ID, null: false
      field :sub_account_id, ID, null: false
      field :name, String, null: false
      field :amount, ID, null: false
      field :description, String, null: true
      field :state, String, null: true
      field :fixed_at, GraphQL::Types::ISO8601Date, null: true
      field :payments, [Types::Payments::PaymentType], null: true
      field :property, Types::Properties::PropertyType, null: false
    end
  end
end
