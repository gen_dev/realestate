module Types
  module Properties
    class PercentageInputType < Types::BaseInputObject
      argument :id, ID, required: false
      argument :property_id, ID, required: true
      argument :tax_id, ID, required: false
      argument :amount, ID, required: true
    end
  end
end
