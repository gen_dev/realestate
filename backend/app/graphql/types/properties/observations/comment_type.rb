module Types
  module Properties
    module Observations
      class CommentType < Types::BaseObject
        field :id, ID, null: false
        field :text, String, null: false
        field :created_at, GraphQL::Types::ISO8601Date, null: true
        field :user, Types::Users::UserType, null: false
      end
    end
  end
end
