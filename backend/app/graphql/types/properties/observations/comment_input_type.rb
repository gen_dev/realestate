module Types
  module Properties
    module Observations
      class CommentInputType < Types::BaseInputObject
        argument :text, String, required: false
        argument :observation_id, ID, required: false
      end
    end
  end
end
