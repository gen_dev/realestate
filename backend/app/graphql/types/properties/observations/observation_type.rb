module Types
  module Properties
    module Observations
      class ObservationType < Types::BaseObject
        field :id, ID, null: false
        field :message, String, null: false
        field :created_at, GraphQL::Types::ISO8601Date, null: true
        field :user, Types::Users::UserType, null: false
        field :comments, [Types::Properties::Observations::CommentType], null: true
      end
    end
  end
end
