module Types
  module Properties
    module Observations
      class ObservationInputType < Types::BaseInputObject
        argument :id, ID, required: false
        argument :property_id, ID, required: true
        argument :message, String, required: false
      end
    end
  end
end
