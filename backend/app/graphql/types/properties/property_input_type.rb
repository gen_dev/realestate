module Types
  module Properties
    class PropertyInputType < Types::BaseInputObject
      class FeatureInputType < Types::BaseInputObject
        class OperationInputType < Types::BaseInputObject
          argument :sell, Boolean, required: false
          argument :rent, Boolean, required: false
        end

        argument :operation_type, OperationInputType, required: false, camelize: false
      end

      argument :property_type, String, required: false
      argument :state, String, required: false
      argument :featured, String, required: false
      argument :currency, String, required: false
      argument :disabled, Boolean, required: false
      argument :description, String, required: false
      argument :price, ID, required: false
      argument :features, FeatureInputType, required: false
      argument :address_attributes, Types::Addresses::AddressInputType, required: false
    end
  end
end
