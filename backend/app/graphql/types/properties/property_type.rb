module Types
  module Properties
    class PropertyType < Types::BaseObject
      field :id, ID, null: false
      field :agency_id, ID, null: false
      field :property_type, String, null: false
      field :state, String, null: false
      field :full_address, String, null: false
      field :address_first_line, String, null: false
      field :address_second_line, String, null: false
      field :description, String, null: true
      field :price, String, null: true
      field :currency, String, null: true
      field :sell_price, String, null: true
      field :sell_currency, String, null: true
      field :disabled, Boolean, null: true
      field :featured, Boolean, null: true
      field :metadata, GraphQL::Types::JSON, null: true
      field :features, GraphQL::Types::JSON, null: true
      field :basic_info, GraphQL::Types::JSON, null: true
      field :fixes_sub_account_id, ID, null: true
      field :unseen_results, Integer, null: false
      field :operation_types, [String], null: true
      field :blueprint_url, String, null: true
      field :agency, Types::Agencies::AgencyType, null: false
      field :images, [Types::Images::ImageType], null: true
      field :taxes, [Types::Taxes::TaxType], null: true
      field :active_tenant, Types::Users::UserType, null: true
      field :active_contract, Types::Contracts::ContractType, null: true
      field :address, Types::Addresses::AddressType, null: true
      field :staff, [Types::Staff::StaffType], null: true
      field :seller, Types::Staff::StaffType, null: true
      field :contracts, [Types::Contracts::ContractType], null: true
      field :owner, [Types::Users::UserType], null: true
      field :characteristics, [Types::SimpleDatas::SimpleDataType], null: true
      field :related_properties, [Types::Properties::PropertyType], null: true

      field :last_result, Types::Searches::SearchResultType, null: true
      field :search_results, [Types::Searches::SearchResultType], null: false
      field :useful_results, [Types::Searches::SearchResultType], null: false
    end
  end
end
