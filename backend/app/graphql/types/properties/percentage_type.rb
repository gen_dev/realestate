module Types
  module Properties
    class PercentageType < Types::BaseObject
      field :id, ID, null: false
      field :property_id, ID, null: false
      field :amount, ID, null: false
    end
  end
end
