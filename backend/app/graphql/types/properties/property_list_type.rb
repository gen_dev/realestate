module Types
  module Properties
    class PropertyListType < Types::BaseObject
      field :properties, [Types::Properties::PropertyType], null: true
      field :pages, Integer, null: true
    end
  end
end
