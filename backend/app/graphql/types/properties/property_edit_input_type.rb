module Types
  module Properties
    class PropertyEditInputType < Types::BaseInputObject
      class EditFeatureInputType < Types::BaseInputObject
        class EditOperationInputType < Types::BaseInputObject
          argument :sell, Boolean, required: false
          argument :rent, Boolean, required: false
        end

        class EditAdditionalFeatureInputType < Types::BaseInputObject
          argument :name, String, required: true
          argument :values, [String], required: false
        end

        argument :operation_type, EditOperationInputType, required: false, camelize: false
        argument :additional_features, [EditAdditionalFeatureInputType], required: false, camelize: false
      end

      class EditMetadataInputType < Types::BaseInputObject
        argument :hide_sell_price, Boolean, required: false, camelize: false
        argument :hide_rent_price, Boolean, required: false, camelize: false
      end

      argument :property_type, String, required: false
      argument :disabled, Boolean, required: false
      argument :featured, Boolean, required: false
      argument :state, String, required: false
      argument :currency, String, required: false
      argument :description, String, required: false
      argument :price, ID, required: false
      argument :sell_price, ID, required: false
      argument :sell_currency, String, required: false
      argument :common_expenses, ID, required: false
      argument :metadata, EditMetadataInputType, required: false
      argument :features, EditFeatureInputType, required: false
      argument :blueprint, ::ApolloUploadServer::Upload, required: false
      argument :address_attributes, Types::Addresses::AddressInputType, required: false
      argument :images_attributes, [Types::Images::ImageInputType], required: false
      argument :characteristics_attributes, [Types::SimpleDatas::SimpleDataInputType], required: false
    end
  end
end
