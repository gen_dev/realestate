module Types
  module Properties
    class FixInputType < Types::BaseInputObject
      argument :id, ID, required: false
      argument :property_id, ID, required: true
      argument :contract_id, ID, required: false
      argument :ticket_id, ID, required: false
      argument :payer_id, ID, required: false
      argument :payer_type, String, required: false
      argument :amount, ID, required: true
      argument :name, String, required: true
      argument :description, String, required: false
      argument :state, String, required: false
      argument :fixed_at, GraphQL::Types::ISO8601Date, required: false
      argument :payments_attributes, [Types::Payments::PaymentInputType], required: true
    end
  end
end
