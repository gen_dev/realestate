module Types
  module Properties
    class PropertyFilterType < Types::BaseInputObject
      argument :property_type, [String], required: false
      argument :disabled, Boolean, required: false
      argument :state, [String], required: false
      argument :description, String, required: false
      argument :antiqueness, Float, required: false
      argument :price_range, [Float], required: false
    end
  end
end
