module Types
  module Accounts
    class AccountInputType < Types::BaseInputObject
      argument :name, String, required: true
      argument :balance, Float, required: true
    end
  end
end
