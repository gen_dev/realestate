module Types
  module Accounts
    class AccountType < Types::BaseObject
      field :id, ID, null: false
      field :name, String, null: false
      field :balance, ID, null: false
      field :sub_accounts, [Types::SubAccounts::SubAccountType], null: true
    end
  end
end
