module Types
  module Accounts
    class AccountFilterType < Types::BaseInputObject
      argument :name, String, required: false
      argument :balance, Float, required: false
    end
  end
end
