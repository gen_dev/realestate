module Types
  module Staff
    class StaffInputType < Types::BaseInputObject
      argument :name, String, required: false
      argument :rol, String, required: false
      argument :property_types, [String], required: false
      argument :number, String, required: false
      argument :city, String, required: false
      argument :operation_type, String, required: false

      argument :avatar, ::ApolloUploadServer::Upload, required: false
    end
  end
end
