module Types
  module Staff
    class StaffType < Types::BaseObject
      field :id, ID, null: false
      field :name, String, null: false
      field :rol, String, null: true
      field :property_types, [String], null: true
      field :number, String, null: false
      field :city, String, null: true
      field :operation_type, String, null: false

      field :user, Types::Users::UserType, null: true

      field :avatar_url, String, null: true
    end
  end
end
