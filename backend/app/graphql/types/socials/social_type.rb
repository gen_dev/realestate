module Types
  module Socials
    class SocialType < Types::BaseObject
      field :id, ID, null: false
      field :name, String, null: false
      field :description, String, null: true
      field :link, String, null: true
      field :image_url, String, null: true
      field :button_tag, String, null: true
    end
  end
end
