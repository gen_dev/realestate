module Types
  module SimpleDatas
    class SimpleDataFilterType < Types::BaseInputObject
      argument :name, String, required: false
      argument :value, String, required: false
      argument :category, String, required: false
    end
  end
end