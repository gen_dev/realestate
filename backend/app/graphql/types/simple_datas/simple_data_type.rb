module Types
  module SimpleDatas
    class SimpleDataType < Types::BaseObject
      field :id, ID, null: false
      field :name, String, null: false
      field :value, String, null: false
      field :category, String, null: true
      field :datable, Types::Datables::DatableType, null: false
    end
  end
end