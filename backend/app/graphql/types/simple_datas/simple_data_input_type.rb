module Types
  module SimpleDatas
    class SimpleDataInputType < Types::BaseInputObject
      argument :id, ID, required: false
      argument :name, String, required: true
      argument :value, String, required: false
      argument :category, String, required: false
    end
  end
end
