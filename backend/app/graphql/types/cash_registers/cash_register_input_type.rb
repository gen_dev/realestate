module Types
  module CashRegisters
    class CashRegisterInputType < Types::BaseInputObject
      argument :amount, String, required: false
      argument :state, String, required: false
    end
  end
end
