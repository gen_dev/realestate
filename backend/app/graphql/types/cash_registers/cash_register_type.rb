module Types
  module CashRegisters
    class CashRegisterType < Types::BaseObject
      field :id, ID, null: false
      field :amount, Float, null: true
      field :state, String, null: true
      field :payments, [Types::Payments::PaymentType], null: true
      field :created_at, GraphQL::Types::ISO8601Date, null: false
    end
  end
end
