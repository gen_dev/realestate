module Types
  module UserAgencies
    class UserAgencyFilterType < Types::BaseInputObject
      argument :selected, Boolean, required: false
    end
  end
end
