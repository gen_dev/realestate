module Types
  module UserAgencies
    class UserAgencyType < Types::BaseObject
      field :id, ID, null: false
      field :selected, Boolean, null: true
      field :user, Types::Users::UserType, null: false
      field :agency, Types::Agencies::AgencyType, null: false
    end
  end
end
