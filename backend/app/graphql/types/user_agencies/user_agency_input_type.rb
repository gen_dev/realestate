module Types
  module UserAgencies
    class UserAgencyInputType < Types::BaseInputObject
      argument :selected, Boolean, required: false
    end
  end
end
