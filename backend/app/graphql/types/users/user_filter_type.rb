module Types
  module Users
    class UserFilterType < Types::BaseInputObject
      argument :email, String, required: false
    end
  end
end
