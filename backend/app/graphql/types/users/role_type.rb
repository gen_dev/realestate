module Types
  module Users
    class RoleType < Types::BaseObject
      field :name, String, null: false
    end
  end
end
