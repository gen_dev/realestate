module Types
  module Users
    class UserType < Types::BaseObject
      field :id, ID, null: false
      field :email, String, null: false
      field :name, String, null: true
      field :search_observations, String, null: true
      field :changelog_read, Boolean, null: true
      field :owed_money, GraphQL::Types::JSON, null: true
      field :owed_money_by_us, GraphQL::Types::JSON, null: true
      field :has_debts, Boolean, null: true
      field :overdue_payments_amount, Float, null: true
      field :advisor_id, ID, null: true
      field :advisor, Types::Staff::StaffType, null: true
      field :address, Types::Addresses::AddressType, null: true
      field :profile, Types::Profiles::ProfileType, null: true
      field :user_agencies, [Types::UserAgencies::UserAgencyType], null: true
      field :agencies, [Types::Agencies::AgencyType], null: true
      field :receipts, [Types::Receipts::ReceiptType], null: true
      field :active_contracts, [Types::Contracts::ContractType], null: true
      field :selected_agency, Types::Agencies::AgencyType, null: true
      field :properties, [Types::Properties::PropertyType], null: true
      field :permissions, GraphQL::Types::JSON, null: true
      field :roles, [Types::Users::RoleType], null: true
      field :display_roles, [String], null: true
    end
  end
end
