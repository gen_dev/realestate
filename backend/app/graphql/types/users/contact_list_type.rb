module Types
  module Users
    class ContactListType < Types::BaseObject
      field :contacts, [Types::Users::UserType], null: true
      field :pages, Integer, null: true
    end
  end
end
