module Types
  module Users
    class UserInputType < Types::BaseInputObject
      argument :email, String, required: false
      argument :password, String, required: false

      argument :search_observations, String, required: false
    end
  end
end
