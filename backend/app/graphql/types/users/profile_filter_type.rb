module Types
  module Users
    class ProfileFilterType < Types::BaseInputObject
      argument :name, String, required: false
      argument :email, String, required: false
      argument :address, String, required: false
      argument :phone, String, required: false
    end
  end
end
