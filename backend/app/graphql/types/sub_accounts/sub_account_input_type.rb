module Types
  module SubAccounts
    class SubAccountInputType < Types::BaseInputObject
      argument :account_id, ID, required: false
      argument :name, String, required: false
      argument :balance, ID, required: false
    end
  end
end
