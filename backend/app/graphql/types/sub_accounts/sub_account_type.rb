module Types
  module SubAccounts
    class SubAccountType < Types::BaseObject
      field :id, ID, null: false
      field :agency_id, ID, null: false
      field :name, String, null: false
      field :balance, Float, null: false
      field :tmp_balance, ID, null: false
      field :parsed_balance, ID, null: false
      field :agency, Types::Agencies::AgencyType, null: false
      field :account, Types::Accounts::AccountType, null: false
      field :payments, [Types::Payments::PaymentType], null: true
      field :concepts, [Types::Concepts::ConceptType], null: true
    end
  end
end
