module Types
  module SubAccounts
    class SubAccountFilterType < Types::BaseInputObject
      argument :name, String, required: false
      argument :balance, Float, required: false
    end
  end
end
