module Types
  module Datables
    class DatableType < Types::BaseUnion
      description "Properties of Datable"
    
      possible_types Types::Addresses::AddressType,
                     Types::Agencies::AgencyType,
                     Types::SubAccounts::SubAccountType,
                     Types::Contracts::ContractType,
                     Types::Contracts::ContractExpenseType,
                     Types::Contracts::ContractPlanType,
                     Types::Payments::PaymentType,
                     Types::Payments::PaymentPlanType,
                     Types::Payments::PartialPaymentType,
                     Types::Properties::PropertyType,
                     Types::Taxes::TaxType,
                     Types::Users::UserType
                     
    
      def self.resolve_type(object, context)
        if object.is_a?(Address)
          Types::Addresses::AddressType
        elsif object.is_a?(Agency)
          Types::Agencies::AgencyType
        elsif object.is_a?(SubAccount)
          Types::SubAccounts::SubAccountType
        elsif object.is_a?(Contract)
          Types::Contracts::ContractType
        elsif object.is_a?(ContractExpense)
          Types::Contracts::ContractExpenseType
        elsif object.is_a?(ContractPlan)
          Types::Contracts::ContractPlanType
        elsif object.is_a?(Payment)
          Types::Payments::PaymentType
        elsif object.is_a?(PaymentPlan)
          Types::Payments::PaymentPlanType
        elsif object.is_a?(PartialPayment)
          Types::Payments::PartialPaymentType
        elsif object.is_a?(Property)
          Types::Properties::PropertyType
        elsif object.is_a?(Tax)
          Types::Taxes::TaxType
        elsif object.is_a?(User)
          Types::Users::UserType
        end
      end
    end
  end
end
