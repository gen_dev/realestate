module Types
  module Rescissions
    class RescissionInputType < Types::BaseInputObject
      argument :due_at, GraphQL::Types::ISO8601Date, required: false
      argument :rent, ID, required: false
      argument :quota, ID, required: false
      argument :bonification, ID, required: false
      argument :state, String, required: false
      argument :receipt_attributes, Types::Receipts::ReceiptInputType, required: false
      argument :contract_id, ID, required: true
    end
  end
end
