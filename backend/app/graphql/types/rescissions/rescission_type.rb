module Types
  module Rescissions
    class RescissionType < Types::BaseObject
      field :id, ID, null: false
      field :contract_id, ID, null: false
      field :rent, String, null: true
      field :quota, String, null: true
      field :bonification, String, null: true
      field :state, String, null: true
      field :due_at, GraphQL::Types::ISO8601Date, null: true
      field :contract, Types::Contracts::ContractType, null: false
      field :receipt, Types::Receipts::ReceiptType, null: true
    end
  end
end
