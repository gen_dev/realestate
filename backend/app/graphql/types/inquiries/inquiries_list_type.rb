module Types
  module Inquiries
    class InquiriesListType < Types::BaseObject
      field :pages, Integer, null: true
      field :inquiries, [Types::Inquiries::InquiryType], null: true
    end
  end
end
