module Types
  module Inquiries
    class InquiryType < Types::BaseObject
      field :id, ID, null: false
      field :name, String, null: false
      field :phone, String, null: true
      field :email, String, null: true
      field :read, Boolean, null: true
      field :investment_type, String, null: true
      field :operation_type, String, null: true
      field :location, String, null: true
      field :topic, String, null: true
      field :message, String, null: true
      field :state, String, null: true
      field :created_at, GraphQL::Types::ISO8601Date, null: true
      field :property, Types::Properties::PropertyType, null: true
      field :user, Types::Users::UserType, null: true
      field :contact, Types::Users::UserType, null: true
      field :search, Types::Searches::SearchType, null: true

      field :characteristics, GraphQL::Types::JSON, null: true
    end
  end
end
