module Types
  module Inquiries
    class InquiryInputType < Types::BaseInputObject
      argument :name, String, required: false
      argument :email, String, required: false
      argument :phone, String, required: false
      argument :topic, String, required: false
      argument :investment_type, String, required: false
      argument :operation_type, String, required: false
      argument :location, String, required: false
      argument :message, String, required: false
      argument :property_id, ID, required: false
      argument :contact_id, ID, required: false

      argument :characteristics, GraphQL::Types::JSON, required: false
    end
  end
end
