module Types
  module Taxes
    class TaxInputType < Types::BaseInputObject
      argument :id, ID, required: false
      argument :amount, Float, required: false
      argument :tenant_action, String, required: false
      argument :owner_action, String, required: false
      argument :agency_action, String, required: false
      argument :empty_action, String, required: false
      argument :applies_when_empty, Boolean, required: false
      argument :unequal_split, Boolean, required: false
      argument :name, String, required: true
      argument :state, String, required: false
      argument :chargeback, ID, required: false
      argument :owner_fee, ID, required: false
      argument :concept_id, ID, required: false
      argument :taxable_id, ID, required: false
      argument :taxable_type, String, required: false
      argument :debtor_id, ID, required: false
      argument :debtor_type, String, required: false
      argument :due_at, ID, required: false
      argument :billed, Boolean, required: false
      argument :_destroy, Boolean, required: false
      argument :starts_at, GraphQL::Types::ISO8601Date, required: false
      argument :ends_at, GraphQL::Types::ISO8601Date, required: false
      argument :prices_attributes, [Types::Prices::PeriodPriceInputType], required: false
      argument :percentages_attributes, [Types::Properties::PercentageInputType], required: false
      argument :duration, Integer, required: false
      argument :interval, Integer, required: false
      argument :commission, ID, required: false
    end
  end
end
