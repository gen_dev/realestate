module Types
  module Taxes
    class TaxType < Types::BaseObject
      field :id, ID, null: false
      field :commission, ID, null: true
      field :chargeback, ID, null: true
      field :owner_fee, ID, null: true
      field :name, String, null: false
      field :tenant_action, String, null: true
      field :owner_action, String, null: true
      field :agency_action, String, null: true
      field :empty_action, String, null: true
      field :applies_when_empty, Boolean, null: true
      field :unequal_split, Boolean, null: true
      field :amount, Float, null: true
      field :state, String, null: false
      field :account_id, Integer, null: true
      field :sub_account_id, Integer, null: true
      field :concept_id, Integer, null: true
      field :taxable_type, String, null: true
      field :taxable_id, Integer, null: true
      field :due_at, Integer, null: true
      field :interval, Integer, null: true
      field :duration, Integer, null: true
      field :editable, Boolean, null: false
      field :billed, Boolean, null: true
      field :starts_at, GraphQL::Types::ISO8601Date, null: true
      field :ends_at, GraphQL::Types::ISO8601Date, null: true
      field :last_payment_date, GraphQL::Types::ISO8601Date, null: true
      field :concept, Types::Concepts::ConceptType, null: true
      field :contract, Types::Contracts::ContractType, null: false
      field :debtor, Types::Debtors::DebtorType, null: false
      field :prices, [Types::Prices::PeriodPriceType], null: true
      field :prices_attributes, [Types::Prices::PeriodPriceType], null: true
      field :payments, [Types::Payments::PaymentType], null: true
      field :percentages, [Types::Properties::PercentageType], null: true
    end
  end
end
