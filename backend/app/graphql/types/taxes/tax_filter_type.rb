module Types
  module Taxes
    class TaxFilterType < Types::BaseInputObject
      argument :name, String, required: false
      argument :state, String, required: false
      argument :interval, Integer, required: false
      argument :duration, Integer, required: false
    end
  end
end
