module Types
    module Documentables
      class DocumentableType < Types::BaseUnion
        description "Properties of Documentable"
      
        possible_types Types::Contracts::ContractType
      
        def self.resolve_type(object, context)
          if object.is_a?(Contract)
            Types::Contracts::ContractType
          end
        end
      end
    end
  end
  