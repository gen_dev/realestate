module Types
    module Imageables
      class ImageableType < Types::BaseUnion
        description "Properties of Imageable"
      
        possible_types Types::Properties::PropertyType
      
        def self.resolve_type(object, context)
          if object.is_a?(Property)
            Types::Properties::PropertyType
          end
        end
      end
    end
  end
  