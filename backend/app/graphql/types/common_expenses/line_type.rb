module Types
  module CommonExpenses
    class LineType < Types::BaseObject
      field :id, ID, null: false
      field :amount, Float, null: true
      field :description, String, null: true
    end
  end
end
