module Types
  module CommonExpenses
    class CommonExpenseType < Types::BaseObject
      field :id, ID, null: false
      field :amount, Float, null: true

      field :lines, [Types::CommonExpenses::LineType], null: true
    end
  end
end
