module Types
  module CommonExpenses
    class CommonExpenseInputType < Types::BaseInputObject
      argument :id, ID, required: false
      argument :amount, Float, required: false
      argument :lines_attributes, [Types::CommonExpenses::LineInputType], required: false
    end
  end
end
