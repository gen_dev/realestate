module Types
  module CommonExpenses
    class LineInputType < Types::BaseInputObject
      argument :id, ID, required: false
      argument :description, String, required: false
      argument :amount, Float, required: false
      argument :_destroy, ID, required: false
    end
  end
end
