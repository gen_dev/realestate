module Types
  module Debtors
    class DebtorType < Types::BaseUnion
      description "Properties of Debtor"
    
      possible_types Types::Users::UserType,
                     Types::Agencies::AgencyType
    
      def self.resolve_type(object, context)
        if object.is_a?(User)
          Types::Users::UserType
        elsif object.is_a?(Agency)
          Types::Agencies::AgencyType
        end
      end
    end
  end
end
