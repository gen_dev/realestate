module Types
  module Payments
    class PaymentListType < Types::BaseObject
      field :payments, [Types::Payments::PaymentType], null: true
      field :pages, Integer, null: true
    end
  end
end
