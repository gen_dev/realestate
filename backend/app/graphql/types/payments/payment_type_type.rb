module Types
  module Payments
    class PaymentTypeType < Types::BaseObject
      field :id, ID, null: false
      field :name, String, null: true
      field :currency, String, null: true
      field :amount, String, null: true
    end
  end
end
