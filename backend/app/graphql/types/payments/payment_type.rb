module Types
  module Payments
    class PaymentType < Types::BaseObject
      field :id, ID, null: false
      field :name, String, null: true
      field :differential, Boolean, null: true
      field :taxable_tag, String, null: true
      field :contract_tag, String, null: true
      field :contract_id, ID, null: true
      field :parent_id, ID, null: true
      field :user_type, String, null: true
      field :currency, String, null: true
      field :third_party_transfer, Boolean, null: true
      field :payment_type, String, null: true
      field :detail, String, null: true
      field :period, String, null: true
      field :concept, String, null: false
      field :state, String, null: false
      field :payable_name, String, null: true
      field :created_at, GraphQL::Types::ISO8601Date, null: false
      field :due_date, GraphQL::Types::ISO8601Date, null: false
      field :paid_at, GraphQL::Types::ISO8601DateTime, null: true
      field :is_advanced, Boolean, null: false
      field :can_advance, Boolean, null: false
      field :can_edit, Boolean, null: false
      field :payable, Boolean, null: false
      field :amount, Float, null: false
      field :total, Float, null: false
      field :lasting_amount, Float, null: false
      field :fixes_amount, Float, null: false
      field :commission_amount, Float, null: false
      field :chargeback_amount, Float, null: false
      field :commissioned_amount, Float, null: false
      field :partial_amount, Float, null: false
      field :observations, String, null: true
      field :sub_account_id, ID, null: false
      field :referral_type, String, null: false
      field :referral_id, ID, null: false
      field :receiver_name, ID, null: true
      field :receiver_type, String, null: true
      field :receiver_id, ID, null: true
      field :expired, Boolean, null: false
      field :chargebacks, [GraphQL::Types::JSON], null: true
      field :contract, Types::Contracts::ContractType, null: true
      field :tax, Types::Taxes::TaxType, null: true
      field :price, Types::Prices::PeriodPriceType, null: true
      field :referral, Types::Referrals::ReferralType, null: false
      field :sub_account, Types::SubAccounts::SubAccountType, null: false
      field :children, [Types::Payments::PaymentType], null: true
      field :counterpart, Types::Payments::PaymentType, null: true
    end
  end
end
