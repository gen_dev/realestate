module Types
  module Payments
    class PartialPaymentFilterType < Types::BaseInputObject
      argument :value, Float, required: false
    end
  end
end