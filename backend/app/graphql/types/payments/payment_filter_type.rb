module Types
  module Payments
    class PaymentFilterType < Types::BaseInputObject
      argument :payment_type, String, required: false
      argument :period, String, required: false
      argument :state, [String], required: false
      argument :due_date, String, required: false
      argument :amount, Float, required: false
      argument :observations, String, required: false
      argument :concept, [String], required: false
      
      argument :user_type, [String], required: false
      argument :sub_account, [String], required: false
      argument :account, [String], required: false
    end
  end
end
