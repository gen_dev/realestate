module Types
  module Payments
    class PaymentPlanInputType < Types::BaseInputObject
      argument :id, ID, required: false
      argument :payment_methods, [String], required: false
      argument :taxes_attributes, [Types::Taxes::TaxInputType], required: false
    end
  end
end
