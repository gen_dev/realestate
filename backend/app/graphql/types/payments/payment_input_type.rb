module Types
  module Payments
    class PaymentInputType < Types::BaseInputObject
      argument :id, ID, required: false
      argument :payment_type, String, required: false
      argument :period, String, required: false
      argument :concept, String, required: false
      argument :state, String, required: false
      argument :detail, String, required: false
      argument :due_date, String, required: false
      argument :paid_at, String, required: false
      argument :amount, ID, required: false
      argument :observations, String, required: false
      argument :advanced, Boolean, required: false
      argument :referral_type, String, required: false
      argument :referral_id, ID, required: false
      argument :receiver_type, String, required: false
      argument :receiver_id, ID, required: false
      argument :sub_account_id, ID, required: false
      argument :concept_id, ID, required: false
      argument :contract_id, ID, required: false
    end
  end
end
