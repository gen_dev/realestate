module Types
  module Payments
    class AdvancedType < Types::BaseObject
      field :name, String, null: true
      field :amount, Float, null: true
    end
  end
end
