module Types
  module Payments
    class PartialPaymentInputType < Types::BaseInputObject
      argument :value, Float, required: true
    end
  end
end