module Types
  module Payments
    class PaymentPlanType < Types::BaseObject
      field :id, ID, null: false
      field :tenant, String, null: false
      field :owner, String, null: false
      field :payment_methods, GraphQL::Types::JSON, null: false
      field :contract, Types::Contracts::ContractType, null: false
      field :taxes, [Types::Taxes::TaxType], null: true
    end
  end
end
