module Types
  module Payments
    class AccountabilityType < Types::BaseObject
      field :paid,    [Types::Payments::PaymentType], null: true
      field :pending, [Types::Payments::PaymentType], null: true
      field :chargebacks, Float, null: true
    end
  end
end
