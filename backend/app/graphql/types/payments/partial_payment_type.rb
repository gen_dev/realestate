module Types
  module Payments
    class PartialPaymentType < Types::BaseObject
      field :id, ID, null: false
      field :value, Float, null: false
      field :created_at, GraphQL::Types::ISO8601Date, null: false
      field :payment, Types::Payments::PaymentType, null: false
    end
  end
end
