module Types
  module Payments
    class PaymentPlanFilterType < Types::BaseInputObject
      argument :tenant, String, required: false
      argument :owner, String, required: false
      argument :payment_methods, GraphQL::Types::JSON, required: false
    end
  end
end
