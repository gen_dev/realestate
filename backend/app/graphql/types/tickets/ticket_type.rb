module Types
  module Tickets
    class TicketType < Types::BaseObject
      field :id, ID, null: false
      field :title, String, null: false
      field :state, String, null: false
      field :created_at, GraphQL::Types::ISO8601Date, null: false
      field :updated_at, GraphQL::Types::ISO8601Date, null: false
      field :descriptions, [Types::Tickets::TicketDescriptionType], null: true
      field :fix, Types::Properties::FixType, null: true
      field :contract, Types::Contracts::ContractType, null: true
    end
  end
end
