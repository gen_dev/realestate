module Types
  module Tickets
    class TicketInputType < Types::BaseInputObject
      argument :id, ID, required: false
      argument :title, String, required: false
      argument :state, String, required: false
      argument :contract_id, ID, required: false
      argument :descriptions_attributes, [Types::Tickets::DescriptionInputType], required: false
    end
  end
end
