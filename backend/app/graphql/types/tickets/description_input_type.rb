module Types
  module Tickets
    class DescriptionInputType < Types::BaseInputObject
      argument :id, ID, required: false
      argument :description, String, required: true
    end
  end
end
