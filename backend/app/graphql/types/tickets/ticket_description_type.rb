module Types
  module Tickets
    class TicketDescriptionType < Types::BaseObject
      field :id, ID, null: false
      field :description, String, null: false
      field :created_at, GraphQL::Types::ISO8601Date, null: false
      field :updated_at, GraphQL::Types::ISO8601Date, null: false
    end
  end
end
