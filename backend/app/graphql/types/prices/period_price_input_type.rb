module Types
  module Prices
    class PeriodPriceInputType < Types::BaseInputObject
      argument :id, ID, required: false
      argument :amount, ID, required: false
      argument :differential, ID, required: false
      argument :_destroy, Boolean, required: false
      argument :referral_type, String, required: false
      argument :referral_id, ID, required: false
      argument :receiver_type, String, required: false
      argument :receiver_id, ID, required: false
      argument :sign, String, required: false
      argument :due_at, ID, required: false
      argument :starts_at, GraphQL::Types::ISO8601Date, required: false
      argument :ends_at, GraphQL::Types::ISO8601Date, required: false
    end
  end
end
