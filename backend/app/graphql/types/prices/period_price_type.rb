module Types
  module Prices
    class PeriodPriceType < Types::BaseObject
      field :id, ID, null: false
      field :amount, ID, null: true
      field :sign, String, null: true
      field :differential, ID, null: true
      field :due_at, ID, null: true
      field :referral_type, String, null: true
      field :referral_id, String, null: true
      field :receiver_type, String, null: true
      field :receiver_id, String, null: true
      field :starts_at, GraphQL::Types::ISO8601Date, null: true
      field :ends_at, GraphQL::Types::ISO8601Date, null: true
      field :contract, Types::Contracts::ContractType, null: true
    end
  end
end
