module Types
  class ResponseStatusType < Types::BaseObject
    field :status, Integer, null: false
    field :location, String, null: false
    field :messages, [String], null: true
  end
end
