module Types
  module Contracts
    class ContractType < Types::BaseObject
      field :id, ID, null: false
      field :state, String, null: false
      field :name, String, null: true
      field :full_address, String, null: true
      field :contract_type, String, null: false
      field :keys, Integer, null: true
      field :duration, Integer, null: true
      field :total_rent_price, Float, null: true
      field :new_law, Boolean, null: true
      field :dollarized, Boolean, null: true
      field :third_party_transfer, Boolean, null: true
      field :tenant_status, Float, null: true
      field :owner_status, Float, null: true
      field :rescission, Types::Rescissions::RescissionType, null: true
      field :rent, Types::Taxes::TaxType, null: true
      field :starts_at, GraphQL::Types::ISO8601Date, null: true
      field :ends_at, GraphQL::Types::ISO8601Date, null: true
      field :documents, [Types::Documents::DocumentType], null: true
      field :property, Types::Properties::PropertyType, null: false
      field :contract_plan, Types::Contracts::ContractPlanType, null: true
      field :payment_plan, Types::Payments::PaymentPlanType, null: true
      field :taxes, [Types::Taxes::TaxType], null: true
      field :current_taxes, [Types::Taxes::TaxType], null: true
      field :payments, [Types::Payments::PaymentType], null: true
      field :owner, [Types::Users::UserType], null: true
      field :tenant, [Types::Users::UserType], null: true
      field :overdue_payments, [Types::Payments::PaymentType], null: true
      field :current_payments, [Types::Payments::PaymentType], null: true
      field :guarantees, [Types::Contracts::GuaranteeType], null: true
    end
  end
end
