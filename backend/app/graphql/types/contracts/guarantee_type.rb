module Types
  module Contracts
    class GuaranteeType < Types::BaseObject
      field :id, ID, null: false
      field :guarantee_type, String, null: true
      field :guarantor_id, ID, null: true
      field :guarantor, Types::Users::UserType, null: false
      field :documents, [Types::Documents::DocumentType], null: true
    end
  end
end
