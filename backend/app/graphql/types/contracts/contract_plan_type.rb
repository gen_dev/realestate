module Types
  module Contracts
    class ContractPlanType < Types::BaseObject
      field :id, ID, null: false
      field :duration, Integer, null: false
      field :rent_update_period, Integer, null: false
      field :rent_value, GraphQL::Types::JSON, null: false
      field :property, Types::Properties::PropertyType, null: false
      field :contract, Types::Contracts::ContractType, null: false
      field :contract_expenses, [Types::Contracts::ContractExpenseType], null: true
      field :total_amount, Float, null: true
    end
  end
end
