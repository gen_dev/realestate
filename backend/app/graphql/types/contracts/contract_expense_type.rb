module Types
  module Contracts
    class ContractExpenseType < Types::BaseObject
      field :id, ID, null: false
      field :name, String, null: false
      field :value, Float, null: false
      field :contract_plan, Types::Contracts::ContractPlanType, null: false
    end
  end
end
