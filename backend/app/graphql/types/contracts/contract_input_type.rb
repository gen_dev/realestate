module Types
  module Contracts
    class ContractInputType < Types::BaseInputObject
      argument :id, ID, required: false
      argument :state, String, required: true
      argument :contract_type, String, required: true
      argument :property_id, ID, required: true
      argument :name, String, required: false
      argument :new_law, Boolean, required: false
      argument :dollarized, Boolean, required: false
      argument :third_party_transfer, Boolean, required: false
      argument :keys, ID, required: false
    end
  end
end
