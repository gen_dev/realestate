module Types
  module Contracts
    class ContractListType < Types::BaseObject
      field :contracts, [Types::Contracts::ContractType], null: true
      field :pages, Integer, null: true
      field :total_count, Integer, null: true
    end
  end
end
