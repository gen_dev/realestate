module Types
  module Contracts
    class ContractPlanFilterType < Types::BaseInputObject
      argument :duration, Integer, required: false
      argument :rent_update_period, Integer, required: false
      argument :rent_value, GraphQL::Types::JSON, required: false
    end
  end
end
