module Types
  module Contracts
    class ContractEditInputType < Types::BaseInputObject
      argument :id, ID, required: false
      argument :state, String, required: false
      argument :contract_type, String, required: false
      argument :property_id, ID, required: false
      argument :name, String, required: false
      argument :keys, ID, required: false
      argument :new_law, Boolean, required: false
      argument :dollarized, Boolean, required: false
      argument :third_party_transfer, Boolean, required: false
      argument :starts_at, GraphQL::Types::ISO8601Date, required: false
      argument :ends_at, GraphQL::Types::ISO8601Date, required: false
      argument :payment_plan_attributes, Types::Payments::PaymentPlanInputType, required: false
      argument :guarantees_attributes, [Types::Contracts::GuaranteeInputType], required: false
    end
  end
end
