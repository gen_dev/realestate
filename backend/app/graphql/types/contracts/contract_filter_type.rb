module Types
  module Contracts
    class ContractFilterType < Types::BaseInputObject
      argument :state, [String], required: false
      argument :owner, String, required: false
      argument :tenant, String, required: false
      argument :contract_type, String, required: false
    end
  end
end
