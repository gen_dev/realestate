module Types
  module Contracts
    class GuaranteeInputType < Types::BaseInputObject
      argument :id, ID, required: false
      argument :guarantee_type, String, required: false
      argument :guarantor_id, ID, required: false
    end
  end
end
