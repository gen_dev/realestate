module Types
  module Contracts
    class ContractExpenseInputType < Types::BaseInputObject
      argument :name, String, required: true
      argument :value, Float, required: true
    end
  end
end
