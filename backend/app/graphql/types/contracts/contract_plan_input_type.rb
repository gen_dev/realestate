module Types
  module Contracts
    class ContractPlanInputType < Types::BaseInputObject
      argument :duration, Integer, required: true
      argument :rent_update_period, Integer, required: true
      argument :rent_value, GraphQL::Types::JSON, required: true
    end
  end
end
