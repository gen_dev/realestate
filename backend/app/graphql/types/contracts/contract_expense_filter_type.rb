module Types
  module Contracts
    class ContractExpenseFilterType < Types::BaseInputObject
      argument :name, String, required: false
      argument :value, Float, required: false
    end
  end
end
