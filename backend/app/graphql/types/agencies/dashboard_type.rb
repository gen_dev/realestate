module Types
  module Agencies
    class DashboardType < Types::BaseObject
      field :pending_to_pay, ID, null: true
      field :average_payday, GraphQL::Types::JSON, null: true
    end
  end
end
