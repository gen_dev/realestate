module Types
  module Agencies
    class AgencyType < Types::BaseObject
      field :id, ID, null: false
      field :name, String, null: false
      field :address, String, null: true
      field :email, String, null: true
      field :phone, String, null: true
      field :website, String, null: true
      field :logo_url, String, null: true

      field :unseen_properties_results, Integer, null: false
      field :unseen_results, Integer, null: false
      field :unseen_inquiries, Integer, null: false

      field :properties, [Types::Properties::PropertyType], null: true
      field :socials, [Types::Socials::SocialType], null: true
      field :users, [Types::Users::UserType], null: true
      field :user_agencies, [Types::UserAgencies::UserAgencyType], null: true
    end
  end
end
