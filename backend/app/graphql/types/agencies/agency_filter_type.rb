module Types
  module Agencies
    class AgencyFilterType < Types::BaseInputObject
      argument :name, String, required: false
    end
  end
end
