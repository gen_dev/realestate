module Types
  module Agencies
    class AgencyInputType < Types::BaseInputObject
      argument :name, String, required: true
      argument :address, String, required: false
      argument :email, String, required: false
      argument :phone, String, required: false
      argument :website, String, required: false
      argument :logo, ::ApolloUploadServer::Upload, required: false
    end
  end
end
