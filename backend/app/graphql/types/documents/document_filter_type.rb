module Types
  module Documents
    class DocumentFilterType < Types::BaseInputObject
      argument :name, String, required: false
      argument :size, Integer, required: false
    end
  end
end
