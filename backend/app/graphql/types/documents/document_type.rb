module Types
    module Documents
      class DocumentType < Types::BaseObject
        field :id, ID, null: false
        field :name, String, null: false
        field :size, Integer, null: false
        field :url, String, null: false
        field :documentable, Types::Documentables::DocumentableType, null: false
      end
    end
  end
  