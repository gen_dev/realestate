module Types
  module Images
    class ImageFilterType < Types::BaseInputObject
      argument :name, String, required: false
      argument :size, Integer, required: false
    end
  end
end
