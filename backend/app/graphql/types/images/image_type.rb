module Types
    module Images
      class ImageType < Types::BaseObject
        field :id, ID, null: false
        field :name, String, null: false
        field :size, Integer, null: false
        field :url, String, null: false
        field :thumb_url, String, null: false
        field :small_url, String, null: false
        field :imageable, Types::Imageables::ImageableType, null: false
      end
    end
  end
  
