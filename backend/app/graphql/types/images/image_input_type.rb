module Types                                                       
  module Images
    class ImageInputType < Types::BaseInputObject
      argument :id, ID, required: false
      argument :name, String, required: false
      argument :lft, Integer, required: false
      argument :size, Integer, required: false
      argument :image, ::ApolloUploadServer::Upload, required: false
    end
  end
end
