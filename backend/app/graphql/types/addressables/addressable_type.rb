module Types
  module Addressables
    class AddressableType < Types::BaseUnion
      description "Properties of Addressable"
    
      possible_types Types::Users::UserType,
                     Types::Agencies::AgencyType,
                     Types::Properties::PropertyType
    
      def self.resolve_type(object, context)
        if object.is_a?(User)
          Types::Users::UserType
        elsif object.is_a?(Agency)
          Types::Agencies::AgencyType
        elsif object.is_a?(Property)
          Types::Properties::PropertyType
        end
      end
    end
  end
end
