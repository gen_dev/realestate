module Types
  module Buildings
    class BuildingInputType < Types::BaseInputObject
      argument :full_address, String, required: false
    end
  end
end
