module Types
  module Buildings
    class BuildingType < Types::BaseObject
      field :id, ID, null: false
      field :full_address, String, null: true
      field :properties, [Types::Properties::PropertyType], null: true
      field :taxes, [Types::Taxes::TaxType], null: true
    end
  end
end
