module Types
  module Referrals
    class ReferralType < Types::BaseUnion
      description "Properties of Referral"
    
      possible_types Types::Users::UserType,
                     Types::Agencies::AgencyType
    
      def self.resolve_type(object, context)
        if object.is_a?(User)
          Types::Users::UserType
        elsif object.is_a?(Agency)
          Types::Agencies::AgencyType
        end
      end
    end
  end
end
