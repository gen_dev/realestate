module Types
  class MutationType < Types::BaseObject
    # Addresses
    field :create_address, mutation: Mutations::Addresses::CreateAddress
    field :delete_address, mutation: Mutations::Addresses::DeleteAddress
    field :edit_address,   mutation: Mutations::Addresses::EditAddress

    # Agencies
    field :create_agency, mutation: Mutations::Agencies::CreateAgency
    field :delete_agency, mutation: Mutations::Agencies::DeleteAgency
    field :edit_agency,   mutation: Mutations::Agencies::EditAgency

    # Staff
    field :create_staff, mutation: Mutations::Staff::CreateStaff
    field :delete_staff, mutation: Mutations::Staff::DeleteStaff
    field :edit_staff,   mutation: Mutations::Staff::EditStaff

    # Buildings
    field :create_building, mutation: Mutations::Buildings::CreateBuilding
    field :delete_building, mutation: Mutations::Buildings::DeleteBuilding
    field :edit_building,   mutation: Mutations::Buildings::EditBuilding
    field :edit_common_expense,   mutation: Mutations::Buildings::EditCommonExpense
    field :add_properties_to_building, mutation: Mutations::Buildings::AddPropertiesToBuilding
    field :remove_property_from_building, mutation: Mutations::Buildings::RemovePropertyFromBuilding

    # SubAccounts
    field :create_sub_account, mutation: Mutations::SubAccounts::CreateSubAccount
    field :delete_sub_account, mutation: Mutations::SubAccounts::DeleteSubAccount
    field :edit_sub_account,   mutation: Mutations::SubAccounts::EditSubAccount

    # CashRegister
    field :edit_cash_register, mutation: Mutations::CashRegisters::EditCashRegister

    # Concepts
    field :create_concept, mutation: Mutations::Concepts::CreateConcept
    field :delete_concept, mutation: Mutations::Concepts::DeleteConcept

    # Contacts
    field :create_contact,  mutation: Mutations::Contacts::CreateContact
    field :edit_contact,    mutation: Mutations::Contacts::EditContact
    field :delete_contact,  mutation: Mutations::Contacts::DeleteContact

    # Contracts
    field :create_contract, mutation: Mutations::Contracts::CreateContract
    field :delete_contract, mutation: Mutations::Contracts::DeleteContract
    field :edit_contract,   mutation: Mutations::Contracts::EditContract
    field :finish_contract,   mutation: Mutations::Contracts::FinishContract
    field :regenerate_payments,   mutation: Mutations::Contracts::RegenerateContractPayments

    # Contract Expenses
    field :create_contract_expense, mutation: Mutations::Contracts::CreateContractExpense
    field :delete_contract_expense, mutation: Mutations::Contracts::DeleteContractExpense
    field :edit_contract_expense,   mutation: Mutations::Contracts::EditContractExpense

    # Contract Plans
    field :create_contract_plan, mutation: Mutations::Contracts::CreateContractPlan
    field :delete_contract_plan, mutation: Mutations::Contracts::DeleteContractPlan
    field :edit_contract_plan,   mutation: Mutations::Contracts::EditContractPlan
    
    # Documents
    field :delete_document, mutation: Mutations::Documents::DeleteDocument

    # Images
    field :delete_image, mutation: Mutations::Images::DeleteImage

    # Inquiries
    field :create_inquiry,  mutation: Mutations::Inquiries::CreateInquiry
    field :delete_inquiry,  mutation: Mutations::Inquiries::DeleteInquiry
    field :assign_inquiry,  mutation: Mutations::Inquiries::AssignInquiry
    field :create_comment,  mutation: Mutations::Inquiries::CreateComment
    field :update_inquiry_contact,  mutation: Mutations::Inquiries::UpdateInquiryContact
    field :update_inquiry_state,  mutation: Mutations::Inquiries::UpdateInquiryState

    # Receipts
    field :create_payment_method, mutation: Mutations::Payments::CreatePaymentMethod
    field :delete_payment_method, mutation: Mutations::Payments::DeletePaymentMethod
    field :print, mutation: Mutations::Receipts::Print
    field :create_receipt, mutation: Mutations::Receipts::CreateReceipt

    # Rescissions
    field :create_rescission, mutation: Mutations::Rescissions::CreateRescission
    field :edit_rescission, mutation: Mutations::Rescissions::EditRescission

    # Payments
    field :advance,         mutation: Mutations::Payments::Advance
    field :pay,             mutation: Mutations::Payments::Pay
    field :pay_all,             mutation: Mutations::Payments::PayAll
    field :create_payment,  mutation: Mutations::Payments::CreatePayment
    field :delete_payment,  mutation: Mutations::Payments::DeletePayment
    field :edit_payment,    mutation: Mutations::Payments::EditPayment

    field :switch_chargeback, mutation: Mutations::Payments::SwitchChargeback

    # Payment Plans
    field :create_payment_plan, mutation: Mutations::Payments::CreatePaymentPlan
    field :delete_payment_plan, mutation: Mutations::Payments::DeletePaymentPlan
    field :edit_payment_plan,   mutation: Mutations::Payments::EditPaymentPlan

    # Partial Payments
    field :create_partial_payment, mutation: Mutations::Payments::CreatePartialPayment
    field :delete_partial_payment, mutation: Mutations::Payments::DeletePartialPayment
    field :edit_partial_payment,   mutation: Mutations::Payments::EditPartialPayment

    # Properties
    field :create_fix, mutation: Mutations::Properties::Fixes::CreateFix
    field :delete_fix, mutation: Mutations::Properties::Fixes::DeleteFix
    field :create_property, mutation: Mutations::Properties::CreateProperty
    field :delete_property, mutation: Mutations::Properties::DeleteProperty
    field :edit_property,   mutation: Mutations::Properties::EditProperty

    field :create_observation,  mutation: Mutations::Properties::Observations::CreateObservation
    field :delete_observation,  mutation: Mutations::Properties::Observations::DeleteObservation
    field :create_observation_comment,  mutation: Mutations::Properties::Observations::CreateObservationComment
    field :delete_observation_comment,  mutation: Mutations::Properties::Observations::DeleteObservationComment

    # Simple Data
    field :create_simple_data, mutation: Mutations::SimpleDatas::CreateSimpleData
    field :delete_simple_data, mutation: Mutations::SimpleDatas::DeleteSimpleData
    field :edit_simple_data,   mutation: Mutations::SimpleDatas::EditSimpleData

    # Searches
    field :create_search, mutation: Mutations::Searches::CreateSearch
    field :edit_search, mutation: Mutations::Searches::EditSearch
    field :delete_search, mutation: Mutations::Searches::DeleteSearch

    # Results
    field :edit_result, mutation: Mutations::Results::EditResult

    # Taxes
    field :create_tax, mutation: Mutations::Taxes::CreateTax
    field :delete_tax, mutation: Mutations::Taxes::DeleteTax
    field :edit_tax,   mutation: Mutations::Taxes::EditTax

    # Tickets
    field :create_ticket, mutation: Mutations::Tickets::CreateTicket
    field :delete_ticket, mutation: Mutations::Tickets::DeleteTicket
    field :edit_ticket,   mutation: Mutations::Tickets::EditTicket

    # Users
    field :create_user,         mutation: Mutations::Users::CreateUser
    field :delete_user,         mutation: Mutations::Users::DeleteUser
    field :edit_user,           mutation: Mutations::Users::EditUser
    field :edit_user_by_admin,  mutation: Mutations::Users::EditUserByAdmin
    field :sign_in,             mutation: Mutations::Users::SignInUser
    field :sign_out,            mutation: Mutations::Users::SignOut

    # User Agencies
    field :create_user_agency, mutation: Mutations::UserAgencies::CreateUserAgency
    field :delete_user_agency, mutation: Mutations::UserAgencies::DeleteUserAgency
    field :edit_user_agency,   mutation: Mutations::UserAgencies::EditUserAgency

    field :switch_selected_agency,   mutation: Mutations::UserAgencies::ChangeSelectedAgency

    # Staff
    field :attach_staff,   mutation: Mutations::Staff::AttachStaff
    field :detach_staff,   mutation: Mutations::Staff::DetachStaff

    # Recommended Description
    field :recommended_description, mutation: Mutations::Properties::RecommendedDescription

    # Socials
    field :create_social, mutation: Mutations::Socials::CreateSocial
    field :delete_social, mutation: Mutations::Socials::DeleteSocial
    field :edit_social,   mutation: Mutations::Socials::EditSocial
  end
end
