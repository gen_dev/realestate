module Types
  module Concepts
    class ConceptInputType < Types::BaseInputObject
      argument :id, ID, required: false
      argument :sub_account_id, ID, required: false
      argument :concept_id, ID, required: false
      argument :name, String, required: false
      argument :balance, ID, required: false
      argument :_destroy, ID, required: false
    end
  end
end
