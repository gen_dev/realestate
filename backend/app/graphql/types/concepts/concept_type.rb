module Types
  module Concepts
    class ConceptType < Types::BaseObject
      field :id, ID, null: false
      field :name, String, null: false
      field :balance, Float, null: false
      field :parsed_balance, String, null: false
      field :sub_account_id, ID, null: false
      field :concept_id, ID, null: false
      field :payments, [Types::Payments::PaymentType], null: true
    end
  end
end
