module Types
  module Concepts
    class ConceptFilterType < Types::BaseInputObject
      argument :name, String, required: false
    end
  end
end
