module Types
  class QueryType < Types::BaseObject
    field :address, resolver: Queries::Addresses::Address
    field :addresses, resolver: Queries::Addresses::Addresses

    field :agency, resolver: Queries::Agencies::Agency
    field :agencies, resolver: Queries::Agencies::Agencies
    field :dashboard, resolver: Queries::Agencies::Dashboard

    field :employee, resolver: Queries::Staff::Employee
    field :staff, resolver: Queries::Staff::Staff
    field :assigned_properties, resolver: Queries::Staff::AssignedProperties
    field :clients, resolver: Queries::Staff::Clients

    field :account, resolver: Queries::Accounts::Account
    field :accounts, resolver: Queries::Accounts::Accounts
    field :sub_account, resolver: Queries::SubAccounts::SubAccount
    field :sub_accounts, resolver: Queries::SubAccounts::SubAccounts

    field :cash_register, resolver: Queries::CashRegisters::CashRegister
    field :current_cash_register, resolver: Queries::CashRegisters::CurrentCashRegister
    field :cash_registers, resolver: Queries::CashRegisters::CashRegisters

    field :building, resolver: Queries::Buildings::Building
    field :buildings, resolver: Queries::Buildings::Buildings
    field :free_properties, resolver: Queries::Buildings::FreeProperties

    field :concepts, resolver: Queries::Concepts::Concepts

    field :contacts, resolver: Queries::Contacts::Contacts
    field :contact, resolver: Queries::Contacts::Contact
    field :list_contacts, resolver: Queries::Contacts::ListContacts

    field :contract, resolver: Queries::Contracts::Contract
    field :contracts, resolver: Queries::Contracts::Contracts
    field :all_contracts, resolver: Queries::Contracts::AllContracts
    field :outdated_prices, resolver: Queries::Contracts::OutdatedPrices
    field :finishing_contracts, resolver: Queries::Contracts::FinishingContracts

    field :contractExpense, resolver: Queries::Contracts::ContractExpense
    field :contractExpenses, resolver: Queries::Contracts::ContractExpenses

    field :contractPlan, resolver: Queries::Contracts::ContractPlan
    field :contractPlans, resolver: Queries::Contracts::ContractPlans

    field :document, resolver: Queries::Documents::Document
    field :documents, resolver: Queries::Documents::Documents

    field :image, resolver: Queries::Images::Image
    field :images, resolver: Queries::Images::Images

    field :inquiries, resolver: Queries::Inquiries::Inquiries
    field :comments, resolver: Queries::Inquiries::Comments

    field :receipt, resolver: Queries::Receipts::Receipt
    field :receipts, resolver: Queries::Receipts::Receipts

    field :payment, resolver: Queries::Payments::Payment
    field :payments, resolver: Queries::Payments::Payments
    field :today, resolver: Queries::Payments::Today
    field :payments_filters, resolver: Queries::Payments::PaymentsFilters
    field :accountability, resolver: Queries::Payments::Accountability
    field :collections, resolver: Queries::Payments::Collections
    field :advanced, resolver: Queries::Payments::Advanced

    field :paymentPlan, resolver: Queries::Payments::PaymentPlan
    field :paymentPlans, resolver: Queries::Payments::PaymentPlans

    field :partialPayment, resolver: Queries::Payments::PartialPayment
    field :partialPayments, resolver: Queries::Payments::PartialPayments

    field :property, resolver: Queries::Properties::Property
    field :property_types, resolver: Queries::Properties::PropertyTypes
    field :properties, resolver: Queries::Properties::Properties
    field :owner_properties, resolver: Queries::Properties::OwnerProperties
    field :fixes, resolver: Queries::Properties::Fixes
    field :starred, resolver: Queries::Properties::Starred
    field :featured, resolver: Queries::Properties::Featured
    field :search, resolver: Queries::Properties::Search
    field :cities, resolver: Queries::Properties::Cities

    field :observations, resolver: Queries::Properties::Observations::Observations

    field :simpleData, resolver: Queries::SimpleDatas::SimpleData
    field :simpleDatas, resolver: Queries::SimpleDatas::SimpleDatas

    # Socials
    field :socials, resolver: Queries::Socials::Socials

    # Searches
    field :searches, resolver: Queries::Searches::Searches
    field :results, resolver: Queries::Searches::Results
    field :agency_results, resolver: Queries::Searches::AgencyResults
    field :user_results, resolver: Queries::Searches::UserResults

    field :tax, resolver: Queries::Taxes::Tax
    field :taxes, resolver: Queries::Taxes::Taxes
    field :user_taxes, resolver: Queries::Users::UserTaxes

    field :ticket, resolver: Queries::Tickets::Ticket
    field :tickets, resolver: Queries::Tickets::Tickets

    field :user, resolver: Queries::Users::User
    field :users, resolver: Queries::Users::Users
    field :owners, resolver: Queries::Users::Owners
    field :tenants, resolver: Queries::Users::Tenants

    field :user_agency, resolver: Queries::UserAgencies::UserAgency
    field :user_agencies, resolver: Queries::UserAgencies::UserAgencies
  end
end
