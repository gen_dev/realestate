module Types
  module Receipts
    class ReceiptInputType < Types::BaseInputObject
      argument :id, ID, required: false
      argument :number, String, required: false
      argument :state, String, required: false
      argument :receipt_type, String, required: false
      argument :user_id, ID, required: false
      argument :contract_id, ID, required: false
      argument :payments_attributes, [Types::Payments::PaymentInputType], required: false
    end
  end
end
