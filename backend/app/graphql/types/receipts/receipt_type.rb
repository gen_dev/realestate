module Types
  module Receipts
    class ReceiptType < Types::BaseObject
      field :id, ID, null: false
      field :user_id, ID, null: false
      field :contract_id, ID, null: false
      field :number, String, null: false
      field :state, String, null: false
      field :receipt_type, String, null: false
      field :created_at, GraphQL::Types::ISO8601DateTime, null: true
      field :payments, [Types::Payments::PaymentType], null: true
      field :payment_types, [Types::Payments::PaymentTypeType], null: true
      field :contract, Types::Contracts::ContractType, null: true
    end
  end
end
