module Types
  module Receipts
    class ReceiptFilterType < Types::BaseInputObject
      argument :number, String, required: false
    end
  end
end
