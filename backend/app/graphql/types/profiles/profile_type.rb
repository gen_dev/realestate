module Types
  module Profiles
    class ProfileType < Types::BaseObject
      field :id, ID, null: false
      field :name, String, null: false
      field :email, String, null: true
      field :phone, String, null: true
      field :address, String, null: true
      field :dni, String, null: true
      field :documents, [Types::Documents::DocumentType], null: true
    end
  end
end
