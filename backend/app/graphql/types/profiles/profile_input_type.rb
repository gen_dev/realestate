module Types
  module Profiles
    class ProfileInputType < Types::BaseInputObject
      argument :name, String, required: true
      argument :email, String, required: false
      argument :address, String, required: false
      argument :phone, String, required: false
      argument :dni, String, required: false
    end
  end
end
