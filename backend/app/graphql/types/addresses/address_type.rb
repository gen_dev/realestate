module Types
  module Addresses
    class AddressType < Types::BaseObject
      field :id, ID, null: false
      field :lat, Float, null: true
      field :lng, Float, null: true
      field :street, String, null: false
      field :number, String, null: false
      field :unit, String, null: true
      field :city, String, null: false
      field :state, String, null: false
      field :country, String, null: false
      field :neighborhood, String, null: true
      field :full_address, String, null: true
      field :addressable, Types::Addressables::AddressableType, null: false
    end
  end
end
