module Types
  module Addresses
    class AddressInputType < Types::BaseInputObject
      argument :id, ID, required: false
      argument :lat, Float, required: false
      argument :lng, Float, required: false
      argument :street, String, required: true
      argument :number, String, required: true
      argument :unit, String, required: false
      argument :city, String, required: true
      argument :state, String, required: true
      argument :country, String, required: true
      argument :full_address, String, required: false
      argument :neighborhood, String, required: false
    end
  end
end
