module Types
  module Addresses
    class AddressFilterType < Types::BaseInputObject
      argument :street, String, required: false
      argument :number, String, required: false
      argument :unit, String, required: false
      argument :city, String, required: false
      argument :state, String, required: false
      argument :country, String, required: false
      argument :neighborhood, String, required: false
    end
  end
end
