module Types
  module Comments
    class CommentInputType < Types::BaseInputObject
      argument :text, String, required: false
      argument :inquiry_id, ID, required: false
    end
  end
end
