class FilesController < ActionController::Base
  before_action :set_paper_trail_whodunnit
  skip_before_action :verify_authenticity_token

  def upload
    res = FilesService.new(files_params).upload
    render json: res
  end 
  
  def test
    render :test
  end

  private 
  
  def files_params
    params.permit(:object_class, :object_id, :files => [])
  end
end
