class ReportsController < ApplicationController
  def balance
    account = Account.find_by(id: params["id"])

    range = []

    if (params["init"])
      range = ["#{params['init']}-01", "#{params['end']}-01"]
    end

    name = BalanceReporter.generate(account, range)

    send_file File.open(name)
  end
end
