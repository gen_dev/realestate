# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2024_11_12_140546) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "unaccent"

  create_table "accounts", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "agency_id"
    t.string "period", default: [], array: true
  end

  create_table "addresses", force: :cascade do |t|
    t.string "street"
    t.string "number"
    t.string "city"
    t.string "state"
    t.string "unit"
    t.string "addressable_type"
    t.bigint "addressable_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "neighborhood"
    t.string "country"
    t.float "lat"
    t.float "lng"
    t.string "full_address"
    t.index ["addressable_type", "addressable_id"], name: "index_addresses_on_addressable_type_and_addressable_id"
  end

  create_table "agencies", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.text "address"
    t.text "website"
    t.string "phone"
    t.string "email"
    t.string "logo"
    t.string "domain"
    t.integer "organization_id"
    t.boolean "approved", default: true
  end

  create_table "building_common_expenses", force: :cascade do |t|
    t.integer "building_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "buildings", force: :cascade do |t|
    t.integer "agency_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "full_address"
    t.index ["agency_id"], name: "index_buildings_on_agency_id"
  end

  create_table "buildings_common_expense_lines", force: :cascade do |t|
    t.integer "common_expense_id"
    t.string "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.float "amount"
  end

  create_table "buildings_common_expenses", force: :cascade do |t|
    t.integer "building_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.float "amount", default: 0.0
  end

  create_table "cash_registers", force: :cascade do |t|
    t.integer "amount", default: 0
    t.string "state", default: "pending"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "agency_id"
  end

  create_table "concepts", force: :cascade do |t|
    t.string "name"
    t.integer "sub_account_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "contract_expenses", force: :cascade do |t|
    t.string "name"
    t.decimal "value", precision: 8, scale: 2
    t.bigint "contract_plan_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["contract_plan_id"], name: "index_contract_expenses_on_contract_plan_id"
  end

  create_table "contract_plans", force: :cascade do |t|
    t.date "start_date"
    t.date "due_date"
    t.integer "duration"
    t.integer "rent_update_period"
    t.jsonb "rent_value"
    t.bigint "contract_id"
    t.bigint "property_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["contract_id"], name: "index_contract_plans_on_contract_id"
    t.index ["property_id"], name: "index_contract_plans_on_property_id"
  end

  create_table "contracts", force: :cascade do |t|
    t.string "state"
    t.string "contract_type"
    t.bigint "property_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "name"
    t.date "starts_at"
    t.date "ends_at"
    t.integer "keys"
    t.boolean "new_law"
    t.boolean "dollarized", default: false
    t.boolean "third_party_transfer", default: false
    t.index ["property_id"], name: "index_contracts_on_property_id"
  end

  create_table "contracts_guarantees", force: :cascade do |t|
    t.integer "guarantor_id"
    t.integer "contract_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "guarantee_type"
  end

  create_table "documents", force: :cascade do |t|
    t.string "name"
    t.integer "size"
    t.string "documentable_type"
    t.bigint "documentable_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "document"
    t.index ["documentable_type", "documentable_id"], name: "index_documents_on_documentable_type_and_documentable_id"
  end

  create_table "images", force: :cascade do |t|
    t.string "name"
    t.integer "size"
    t.string "imageable_type"
    t.bigint "imageable_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "image"
    t.integer "lft", default: 0
    t.index ["imageable_type", "imageable_id"], name: "index_images_on_imageable_type_and_imageable_id"
  end

  create_table "inquiries", force: :cascade do |t|
    t.string "name"
    t.string "topic"
    t.string "email"
    t.text "message"
    t.string "dni"
    t.integer "property_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "phone"
    t.integer "user_id"
    t.text "state"
    t.integer "contact_id"
    t.string "location"
    t.string "investment_type"
    t.boolean "manual", default: false
    t.boolean "read"
    t.string "operation_type"
    t.json "characteristics", default: {}
  end

  create_table "inquiries_comments", force: :cascade do |t|
    t.string "text"
    t.integer "user_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "inquiry_id"
  end

  create_table "organizations", force: :cascade do |t|
    t.string "domain"
    t.string "slug"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "partial_payments", force: :cascade do |t|
    t.decimal "value", precision: 8, scale: 2
    t.bigint "payment_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["payment_id"], name: "index_partial_payments_on_payment_id"
  end

  create_table "payment_plans", force: :cascade do |t|
    t.string "payment_methods", default: [], array: true
    t.bigint "contract_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["contract_id"], name: "index_payment_plans_on_contract_id"
  end

  create_table "payment_types", force: :cascade do |t|
    t.integer "receipt_id"
    t.decimal "amount", precision: 10, scale: 2
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "currency", default: "ARS"
  end

  create_table "payments", force: :cascade do |t|
    t.string "payment_type", default: "Efectivo"
    t.string "period"
    t.string "concept"
    t.string "state"
    t.date "due_date"
    t.text "observations"
    t.bigint "amount"
    t.bigint "contract_id"
    t.string "referral_type"
    t.bigint "referral_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "sub_account_id"
    t.integer "tax_id"
    t.boolean "advanced", default: false
    t.datetime "paid_at"
    t.integer "price_id"
    t.string "payable_type"
    t.integer "payable_id"
    t.integer "receipt_id"
    t.integer "receiver_id"
    t.string "receiver_type"
    t.string "contract_tag"
    t.integer "concept_id"
    t.integer "parent_id"
    t.boolean "differential", default: false
    t.string "taxable_tag"
    t.string "detail"
    t.integer "counterpart_id"
    t.float "commissioned_amount"
    t.float "fixes_amount"
    t.string "currency", default: "ARS"
    t.boolean "third_party_transfer", default: false
    t.index ["contract_id"], name: "index_payments_on_contract_id"
    t.index ["referral_type", "referral_id"], name: "index_payments_on_referral_type_and_referral_id"
  end

  create_table "payments_chargebacks", force: :cascade do |t|
    t.integer "payment_id"
    t.float "amount"
    t.boolean "active", default: true
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "charged_at"
    t.index ["charged_at"], name: "index_payments_chargebacks_on_charged_at"
  end

  create_table "price_periods", force: :cascade do |t|
    t.float "amount"
    t.date "starts_at"
    t.date "ends_at"
    t.integer "priceable_id"
    t.string "priceable_type"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "referral_type"
    t.integer "referral_id"
    t.string "sign"
    t.integer "due_at"
    t.integer "receiver_id"
    t.string "receiver_type"
    t.integer "differential", default: 0
  end

  create_table "properties", force: :cascade do |t|
    t.string "property_type"
    t.string "state"
    t.text "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "agency_id"
    t.float "price"
    t.jsonb "features"
    t.boolean "disabled", default: false
    t.string "currency", default: "ARS"
    t.integer "building_id"
    t.string "approved", default: "pending"
    t.float "sell_price"
    t.string "sell_currency"
    t.float "common_expenses"
    t.boolean "featured"
    t.string "blueprint"
    t.jsonb "metadata", default: {"hide_rent_price"=>false, "hide_sell_price"=>false}
    t.index ["agency_id"], name: "index_properties_on_agency_id"
    t.index ["building_id"], name: "index_properties_on_building_id"
  end

  create_table "properties_comments", force: :cascade do |t|
    t.integer "observation_id"
    t.string "text"
    t.integer "user_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "properties_fixes", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.integer "property_id"
    t.integer "contract_id"
    t.date "fixed_at"
    t.string "state"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.float "amount"
    t.integer "payer_id"
    t.string "payer_type"
    t.integer "ticket_id"
  end

  create_table "properties_observations", force: :cascade do |t|
    t.integer "property_id"
    t.string "message"
    t.integer "user_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "properties_percentages", force: :cascade do |t|
    t.integer "property_id"
    t.integer "building_id"
    t.integer "tax_id"
    t.integer "amount"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "properties_staffs", force: :cascade do |t|
    t.integer "property_id"
    t.integer "staff_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "receipts", force: :cascade do |t|
    t.integer "agency_id"
    t.integer "number"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "state", default: "pending"
    t.integer "user_id"
    t.string "receipt_type"
    t.string "stamp"
    t.integer "contract_id"
    t.integer "rescission_id"
  end

  create_table "rescissions", force: :cascade do |t|
    t.integer "contract_id"
    t.date "due_at"
    t.string "state", default: "draft"
    t.string "rent"
    t.string "bonification"
    t.string "quota"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "roles", force: :cascade do |t|
    t.string "name"
    t.string "resource_type"
    t.bigint "resource_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id"
    t.index ["resource_type", "resource_id"], name: "index_roles_on_resource_type_and_resource_id"
  end

  create_table "search_results", force: :cascade do |t|
    t.integer "search_id"
    t.integer "property_id"
    t.float "score", default: 0.0
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "user_id"
    t.boolean "active", default: true
    t.integer "seen_by", default: [], array: true
    t.boolean "dropped", default: false
    t.boolean "shared", default: false
    t.string "state", default: "Pendiente"
    t.index ["property_id"], name: "index_search_results_on_property_id"
    t.index ["search_id"], name: "index_search_results_on_search_id"
    t.index ["user_id"], name: "index_search_results_on_user_id"
  end

  create_table "searches", force: :cascade do |t|
    t.integer "user_id"
    t.integer "agency_id"
    t.text "property_type", default: [], array: true
    t.jsonb "features", default: {"operation_type"=>{"rent"=>false, "sell"=>false}, "additional_features"=>[{"name"=>"Servicios", "values"=>[]}, {"name"=>"Acceso", "values"=>[]}, {"name"=>"Comodidades y amenities", "values"=>[]}, {"name"=>"Características adicionales", "values"=>[]}, {"name"=>"Ambientes", "values"=>[]}]}
    t.float "max_price", default: 0.0
    t.float "min_price", default: 0.0
    t.float "max_common_expenses", default: 0.0
    t.float "min_common_expenses", default: 0.0
    t.integer "max_brooms", default: 0
    t.integer "min_brooms", default: 0
    t.integer "max_baths", default: 0
    t.integer "min_baths", default: 0
    t.integer "max_rooms", default: 0
    t.integer "min_rooms", default: 0
    t.string "address"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "operation_type", default: "rent"
    t.string "currency"
    t.float "max_surface"
    t.float "min_surface"
    t.float "max_covered_surface"
    t.float "min_covered_surface"
    t.string "state", default: "Pendiente"
    t.integer "inquiry_id"
    t.index ["agency_id"], name: "index_searches_on_agency_id"
    t.index ["user_id"], name: "index_searches_on_user_id"
  end

  create_table "simple_datas", force: :cascade do |t|
    t.string "name"
    t.string "value"
    t.string "category"
    t.string "datable_type"
    t.bigint "datable_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["datable_id"], name: "index_simple_datas_on_datable_id"
    t.index ["datable_type", "datable_id"], name: "index_simple_datas_on_datable_type_and_datable_id"
  end

  create_table "socials", force: :cascade do |t|
    t.integer "agency_id"
    t.string "name"
    t.string "link"
    t.string "image"
    t.string "button_tag"
    t.string "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "staffs", force: :cascade do |t|
    t.integer "organization_id"
    t.string "name"
    t.string "rol"
    t.string "property_types", default: [], array: true
    t.string "number"
    t.string "city"
    t.string "operation_type"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "user_id"
    t.text "avatar"
  end

  create_table "sub_accounts", force: :cascade do |t|
    t.string "name"
    t.decimal "balance", precision: 8, scale: 2
    t.bigint "agency_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "account_id"
    t.index ["agency_id"], name: "index_sub_accounts_on_agency_id"
  end

  create_table "taxes", force: :cascade do |t|
    t.string "name"
    t.date "duration"
    t.string "state"
    t.integer "interval", default: 0
    t.date "last_payment_date"
    t.string "debtor_type"
    t.bigint "debtor_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.date "starts_at"
    t.date "ends_at"
    t.integer "taxable_id"
    t.string "taxable_type"
    t.float "commission", default: 0.0
    t.float "chargeback", default: 0.0
    t.float "owner_fee", default: 0.0
    t.integer "concept_id"
    t.float "amount"
    t.string "tenant_action"
    t.string "owner_action"
    t.string "agency_action"
    t.boolean "billed", default: false
    t.integer "due_at"
    t.integer "parent_id"
    t.string "empty_action", default: "none"
    t.boolean "applies_when_empty", default: false
    t.boolean "unequal_split"
    t.index ["debtor_type", "debtor_id"], name: "index_taxes_on_debtor_type_and_debtor_id"
  end

  create_table "tickets", force: :cascade do |t|
    t.integer "contract_id"
    t.string "title"
    t.string "state"
    t.integer "opener_id"
    t.string "opener_type"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "tickets_descriptions", force: :cascade do |t|
    t.integer "ticket_id"
    t.text "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "user_agencies", force: :cascade do |t|
    t.boolean "selected"
    t.bigint "user_id"
    t.bigint "agency_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["agency_id"], name: "index_user_agencies_on_agency_id"
    t.index ["user_id"], name: "index_user_agencies_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email"
    t.string "password_digest"
    t.string "phone"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "organization_id"
    t.boolean "changelog_read", default: false
    t.integer "advisor_id"
    t.string "search_observations"
    t.datetime "deleted_at"
  end

  create_table "users_profiles", force: :cascade do |t|
    t.integer "user_id"
    t.string "name"
    t.string "address"
    t.string "phone"
    t.string "email"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "dni"
  end

  create_table "users_roles", id: false, force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "role_id"
    t.index ["role_id"], name: "index_users_roles_on_role_id"
    t.index ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id"
    t.index ["user_id"], name: "index_users_roles_on_user_id"
  end

end
