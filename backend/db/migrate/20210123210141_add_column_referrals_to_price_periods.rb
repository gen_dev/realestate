class AddColumnReferralsToPricePeriods < ActiveRecord::Migration[6.0]
  def change
    add_column :price_periods, :referral_type, :string
    add_column :price_periods, :referral_id, :integer
  end
end
