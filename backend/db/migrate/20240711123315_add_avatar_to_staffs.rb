class AddAvatarToStaffs < ActiveRecord::Migration[6.0]
  def change
    add_column :staffs, :avatar, :text
  end
end
