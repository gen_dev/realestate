class AddColumnDueAtToPricePeriods < ActiveRecord::Migration[6.0]
  def change
    add_column :price_periods, :due_at, :integer
  end
end
