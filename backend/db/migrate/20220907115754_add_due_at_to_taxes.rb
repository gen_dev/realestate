class AddDueAtToTaxes < ActiveRecord::Migration[6.0]
  def change
    add_column :taxes, :due_at, :integer
  end
end
