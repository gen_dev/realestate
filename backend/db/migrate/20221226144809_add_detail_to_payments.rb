class AddDetailToPayments < ActiveRecord::Migration[6.0]
  def change
    add_column :payments, :detail, :string
  end
end
