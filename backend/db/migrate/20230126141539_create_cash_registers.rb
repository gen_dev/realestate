class CreateCashRegisters < ActiveRecord::Migration[6.0]
  def change
    create_table :cash_registers do |t|
      t.integer :amount, default: 0
      t.string :state, default: 'pending'

      t.timestamps
    end
  end
end
