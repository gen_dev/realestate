class AddOwnerFeeToTaxes < ActiveRecord::Migration[6.0]
  def change
    add_column :taxes, :owner_fee, :float, default: 0
  end
end
