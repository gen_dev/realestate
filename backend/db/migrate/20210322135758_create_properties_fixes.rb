class CreatePropertiesFixes < ActiveRecord::Migration[6.0]
  def change
    create_table :properties_fixes do |t|
      t.string :name
      t.text :description
      t.integer :property_id
      t.integer :contract_id
      t.date :fixed_at
      t.string :state

      t.timestamps
    end
  end
end
