class CreateUserAgencies < ActiveRecord::Migration[6.0]
  def change
    create_table :user_agencies do |t|
      t.boolean :selected
      t.references :user
      t.references :agency

      t.timestamps
    end
  end
end
