class CreateTaxes < ActiveRecord::Migration[6.0]
  def change
    create_table :taxes do |t|
      t.string :name, unique: true
      t.date :duration
      t.string :state
      t.integer :interval, default: 0
      t.date :last_payment_date
      t.references :contract
      t.references :debtor, polymorphic: true

      t.timestamps
    end
  end
end
