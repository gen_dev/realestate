class AddAmountToPropertiesFixes < ActiveRecord::Migration[6.0]
  def change
    add_column :properties_fixes, :amount, :float
  end
end
