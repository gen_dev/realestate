class CreatePayments < ActiveRecord::Migration[6.0]
  def change
    create_table :payments do |t|
      t.string :payment_type
      t.string :period
      t.string :concept
      t.string :state
      t.date :due_date
      t.text :observations
      t.decimal :amount, default: nil, precision: 8, scale: 2
      t.references :contract
      t.references :cash
      t.references :referral, polymorphic: true

      t.timestamps
    end
  end
end
