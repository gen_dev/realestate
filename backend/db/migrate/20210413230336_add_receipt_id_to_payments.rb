class AddReceiptIdToPayments < ActiveRecord::Migration[6.0]
  def change
    add_column :payments, :receipt_id, :integer
  end
end
