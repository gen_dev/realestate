class AddCommissionToTaxes < ActiveRecord::Migration[6.0]
  def change
    add_column :taxes, :commission, :float, default: 0
  end
end
