class AddChangelogReadToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :changelog_read, :boolean, default: false
  end
end
