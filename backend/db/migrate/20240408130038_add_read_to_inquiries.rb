class AddReadToInquiries < ActiveRecord::Migration[6.0]
  def change
    add_column :inquiries, :read, :boolean
  end
end
