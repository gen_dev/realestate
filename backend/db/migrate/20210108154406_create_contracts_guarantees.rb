class CreateContractsGuarantees < ActiveRecord::Migration[6.0]
  def change
    create_table :contracts_guarantees do |t|
      t.integer :guarantor_id
      t.integer :contract_id

      t.timestamps
    end
  end
end
