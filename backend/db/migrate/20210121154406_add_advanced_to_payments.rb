class AddAdvancedToPayments < ActiveRecord::Migration[6.0]
  def change
    add_column :payments, :advanced, :boolean, default: false
  end
end
