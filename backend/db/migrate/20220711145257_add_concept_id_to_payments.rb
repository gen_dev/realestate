class AddConceptIdToPayments < ActiveRecord::Migration[6.0]
  def change
    add_column :payments, :concept_id, :integer
  end
end
