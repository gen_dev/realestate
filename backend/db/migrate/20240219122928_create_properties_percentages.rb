class CreatePropertiesPercentages < ActiveRecord::Migration[6.0]
  def change
    create_table :properties_percentages do |t|
      t.integer :property_id
      t.integer :building_id
      t.integer :tax_id
      t.integer :amount

      t.timestamps
    end
  end
end
