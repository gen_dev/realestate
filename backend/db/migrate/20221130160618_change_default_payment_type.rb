class ChangeDefaultPaymentType < ActiveRecord::Migration[6.0]
  def change
    change_column_default :payments, :payment_type, 'Efectivo'
  end
end
