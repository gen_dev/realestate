class AddFullAddressToBuildings < ActiveRecord::Migration[6.0]
  def change
    add_column :buildings, :full_address, :string
  end
end
