class AddAgencyReferenceToProperties < ActiveRecord::Migration[6.0]
  def change
    add_reference :properties, :agency, index: true
  end
end
