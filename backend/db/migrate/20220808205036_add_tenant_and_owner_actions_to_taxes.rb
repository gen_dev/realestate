class AddTenantAndOwnerActionsToTaxes < ActiveRecord::Migration[6.0]
  def change
    add_column :taxes, :tenant_action, :string
    add_column :taxes, :owner_action, :string
  end
end
