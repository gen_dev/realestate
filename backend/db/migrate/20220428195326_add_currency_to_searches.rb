class AddCurrencyToSearches < ActiveRecord::Migration[6.0]
  def change
    add_column :searches, :currency, :string
  end
end
