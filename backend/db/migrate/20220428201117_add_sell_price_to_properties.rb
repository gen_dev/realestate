class AddSellPriceToProperties < ActiveRecord::Migration[6.0]
  def change
    add_column :properties, :sell_price, :float
    add_column :properties, :sell_currency, :string
  end
end
