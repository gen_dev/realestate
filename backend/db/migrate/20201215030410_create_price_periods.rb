class CreatePricePeriods < ActiveRecord::Migration[6.0]
  def change
    create_table :price_periods do |t|
      t.float :amount
      t.date :starts_at
      t.date :ends_at
      t.integer :priceable_id
      t.string :priceable_type

      t.timestamps
    end
  end
end
