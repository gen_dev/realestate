class AddNewLawToContracts < ActiveRecord::Migration[6.0]
  def change
    add_column :contracts, :new_law, :boolean
  end
end
