class AddInfoToAgency < ActiveRecord::Migration[6.0]
  def change
    add_column :agencies, :address, :text
    add_column :agencies, :website, :text
    add_column :agencies, :phone, :string
    add_column :agencies, :email, :string
  end
end
