class CreateUsersProfiles < ActiveRecord::Migration[6.0]
  def change
    create_table :users_profiles do |t|
      t.integer :user_id
      t.string :name
      t.string :address
      t.string :phone
      t.string :email

      t.timestamps
    end
  end
end
