class AddStateToSearches < ActiveRecord::Migration[6.0]
  def change
    add_column :searches, :state, :string, default: "Pendiente"
  end
end
