class ChangeCashIdToSubAccountFromPayments < ActiveRecord::Migration[6.0]
  def change
    remove_column :payments, :cash_id, :integer
    add_column :payments, :sub_account_id, :integer
  end
end
