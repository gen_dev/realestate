class AddUserIdToSearchResults < ActiveRecord::Migration[6.0]
  def change
    add_column :search_results, :user_id, :integer
  end
end
