class AddAccountIdToSubAccounts < ActiveRecord::Migration[6.0]
  def change
    add_column :sub_accounts, :account_id, :integer
  end
end
