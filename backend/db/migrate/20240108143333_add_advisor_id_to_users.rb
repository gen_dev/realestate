class AddAdvisorIdToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :advisor_id, :integer
  end
end
