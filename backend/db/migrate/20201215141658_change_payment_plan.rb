class ChangePaymentPlan < ActiveRecord::Migration[6.0]
  def change
    remove_column :taxes, :contract_id, :integer
    add_column :taxes, :taxable_id, :integer    
    add_column :taxes, :taxable_type, :string

    remove_column :payment_plans, :tenant, :boolean
    remove_column :payment_plans, :owner, :boolean
  end
end
