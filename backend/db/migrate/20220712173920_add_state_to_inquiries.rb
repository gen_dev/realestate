class AddStateToInquiries < ActiveRecord::Migration[6.0]
  def change
    add_column :inquiries, :state, :text
  end
end
