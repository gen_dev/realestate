class ChangeTableNameToCash < ActiveRecord::Migration[6.0]
  def change
    rename_table :cashes, :sub_accounts
  end
end
