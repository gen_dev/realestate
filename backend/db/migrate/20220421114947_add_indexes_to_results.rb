class AddIndexesToResults < ActiveRecord::Migration[6.0]
  def change
    add_index :search_results, :search_id
    add_index :search_results, :user_id
    add_index :search_results, :property_id
    add_index :searches, :agency_id
    add_index :searches, :user_id
  end
end
