class ChangePrecisionOfPrices < ActiveRecord::Migration[6.0]
  def change
    change_column :payments, :amount, :integer, limit: 8, scale: 2
  end
end
