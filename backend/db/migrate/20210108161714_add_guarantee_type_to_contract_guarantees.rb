class AddGuaranteeTypeToContractGuarantees < ActiveRecord::Migration[6.0]
  def change
    add_column :contracts_guarantees, :guarantee_type, :string
  end
end
