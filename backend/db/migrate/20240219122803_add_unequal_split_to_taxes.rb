class AddUnequalSplitToTaxes < ActiveRecord::Migration[6.0]
  def change
    add_column :taxes, :unequal_split, :boolean
  end
end
