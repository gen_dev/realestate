class AddDifferentialToPayments < ActiveRecord::Migration[6.0]
  def change
    add_column :payments, :differential, :boolean, default: false
  end
end
