class AddSeenByToSearchResults < ActiveRecord::Migration[6.0]
  def change
    add_column :search_results, :seen_by, :integer, array: true, default: []
  end
end
