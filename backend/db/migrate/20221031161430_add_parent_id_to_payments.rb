class AddParentIdToPayments < ActiveRecord::Migration[6.0]
  def change
    add_column :payments, :parent_id, :integer
  end
end
