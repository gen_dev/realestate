class AddLogoToAgencies < ActiveRecord::Migration[6.0]
  def change
    add_column :agencies, :logo, :string
  end
end
