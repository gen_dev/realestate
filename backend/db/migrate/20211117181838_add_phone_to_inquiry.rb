class AddPhoneToInquiry < ActiveRecord::Migration[6.0]
  def change
    add_column :inquiries, :phone, :string
  end
end
