class CreateSimpleDatas < ActiveRecord::Migration[6.0]
  def change
    create_table :simple_datas do |t|
      t.string :name
      t.string :value
      t.string :category
      t.references :datable, polymorphic: true
      
      t.timestamps
    end
  end
end
