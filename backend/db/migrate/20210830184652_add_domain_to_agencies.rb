class AddDomainToAgencies < ActiveRecord::Migration[6.0]
  def change
    add_column :agencies, :domain, :string
  end
end
