class AddAgencyIdToCashRegisters < ActiveRecord::Migration[6.0]
  def change
    add_column :cash_registers, :agency_id, :integer
  end
end
