class AddOrganizationIdToAgencies < ActiveRecord::Migration[6.0]
  def change
    add_column :agencies, :organization_id, :integer
  end
end
