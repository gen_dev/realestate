class AddPeriodToAccounts < ActiveRecord::Migration[6.0]
  def change
    add_column :accounts, :period, :string, array: true, default: []
  end
end
