class CreateSearchResults < ActiveRecord::Migration[6.0]
  def change
    create_table :search_results do |t|
      t.integer :search_id
      t.integer :property_id
      t.float :score, default: 0.0

      t.timestamps
    end
  end
end
