class CreateRescissions < ActiveRecord::Migration[6.0]
  def change
    create_table :rescissions do |t|
      t.integer :contract_id
      t.date :due_at
      t.string :state, default: "draft"
      t.string :rent
      t.string :bonification
      t.string :quota

      t.timestamps
    end

    add_column :receipts, :rescission_id, :integer
  end
end
