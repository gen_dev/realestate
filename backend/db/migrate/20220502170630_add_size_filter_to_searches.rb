class AddSizeFilterToSearches < ActiveRecord::Migration[6.0]
  def change
    add_column :searches, :max_surface, :float
    add_column :searches, :min_surface, :float
    add_column :searches, :max_covered_surface, :float
    add_column :searches, :min_covered_surface, :float
  end
end
