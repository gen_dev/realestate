class AddChargebackToTaxes < ActiveRecord::Migration[6.0]
  def change
    add_column :taxes, :chargeback, :float, default: 0
  end
end
