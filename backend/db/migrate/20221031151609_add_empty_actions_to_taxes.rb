class AddEmptyActionsToTaxes < ActiveRecord::Migration[6.0]
  def change
    add_column :taxes, :empty_action, :string, default: 'none'
    add_column :taxes, :applies_when_empty, :boolean, default: false
  end
end
