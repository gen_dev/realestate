class CreatePropertiesStaffs < ActiveRecord::Migration[6.0]
  def change
    create_table :properties_staffs do |t|
      t.integer :property_id
      t.integer :staff_id

      t.timestamps
    end
  end
end
