class AddAgencyActionToTaxes < ActiveRecord::Migration[6.0]
  def change
    add_column :taxes, :agency_action, :string
  end
end
