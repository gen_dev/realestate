class AddChargedAtToPaymentsChargebacks < ActiveRecord::Migration[6.0]
  def change
    add_column :payments_chargebacks, :charged_at, :datetime
    add_index :payments_chargebacks, :charged_at
  end
end
