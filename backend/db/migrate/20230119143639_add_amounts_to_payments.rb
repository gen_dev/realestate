class AddAmountsToPayments < ActiveRecord::Migration[6.0]
  def change
    add_column :payments, :commissioned_amount, :float
    add_column :payments, :fixes_amount, :float
  end
end
