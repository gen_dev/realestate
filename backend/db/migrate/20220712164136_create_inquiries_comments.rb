class CreateInquiriesComments < ActiveRecord::Migration[6.0]
  def change
    create_table :inquiries_comments do |t|
      t.string :text
      t.integer :user_id

      t.timestamps
    end
  end
end
