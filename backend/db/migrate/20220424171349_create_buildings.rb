class CreateBuildings < ActiveRecord::Migration[6.0]
  def change
    create_table :buildings do |t|
      t.integer :agency_id

      t.timestamps
    end

    add_column :properties, :building_id, :integer

    add_index :buildings, :agency_id
    add_index :properties, :building_id
  end
end
