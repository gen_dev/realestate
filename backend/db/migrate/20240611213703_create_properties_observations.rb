class CreatePropertiesObservations < ActiveRecord::Migration[6.0]
  def change
    create_table :properties_observations do |t|
      t.integer :property_id
      t.string :message
      t.integer :user_id

      t.timestamps
    end
  end
end
