class AddKeysToContracts < ActiveRecord::Migration[6.0]
  def change
    add_column :contracts, :keys, :integer
  end
end
