class AddCurrencyToPayments < ActiveRecord::Migration[6.0]
  def change
    add_column :payments, :currency, :string, default: "ARS"
  end
end
