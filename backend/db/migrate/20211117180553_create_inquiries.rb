class CreateInquiries < ActiveRecord::Migration[6.0]
  def change
    create_table :inquiries do |t|
      t.string :name
      t.string :topic
      t.string :email
      t.text :message
      t.string :dni
      t.integer :property_id

      t.timestamps
    end
  end
end
