class CreatePaymentTypes < ActiveRecord::Migration[6.0]
  def change
    create_table :payment_types do |t|
      t.integer :receipt_id
      t.decimal :amount, precision: 10, scale: 2
      t.string :name

      t.timestamps
    end
  end
end
