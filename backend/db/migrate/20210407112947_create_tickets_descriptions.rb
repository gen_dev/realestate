class CreateTicketsDescriptions < ActiveRecord::Migration[6.0]
  def change
    create_table :tickets_descriptions do |t|
      t.integer :ticket_id
      t.text :description

      t.timestamps
    end
  end
end
