class AddDocumentFieldToDocuments < ActiveRecord::Migration[6.0]
  def change
    add_column :documents, :document, :string
  end
end
