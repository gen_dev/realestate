class AddColumnPriceIdToPayments < ActiveRecord::Migration[6.0]
  def change
    add_column :payments, :price_id, :integer
  end
end
