class AddNewFieldsToInquiries < ActiveRecord::Migration[6.0]
  def change
    add_column :inquiries, :location, :string
    add_column :inquiries, :investment_type, :string
  end
end
