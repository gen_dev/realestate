class AddParentIdToTaxes < ActiveRecord::Migration[6.0]
  def change
    add_column :taxes, :parent_id, :integer
  end
end
