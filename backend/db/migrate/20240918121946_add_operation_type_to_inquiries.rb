class AddOperationTypeToInquiries < ActiveRecord::Migration[6.0]
  def change
    add_column :inquiries, :operation_type, :string
  end
end
