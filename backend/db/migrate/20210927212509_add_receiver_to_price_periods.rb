class AddReceiverToPricePeriods < ActiveRecord::Migration[6.0]
  def change
    add_column :price_periods, :receiver_id, :integer
    add_column :price_periods, :receiver_type, :string
  end
end
