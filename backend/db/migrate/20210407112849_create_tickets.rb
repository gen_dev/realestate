class CreateTickets < ActiveRecord::Migration[6.0]
  def change
    create_table :tickets do |t|
      t.integer :contract_id
      t.string :title
      t.string :state
      t.integer :opener_id
      t.string :opener_type

      t.timestamps
    end
  end
end
