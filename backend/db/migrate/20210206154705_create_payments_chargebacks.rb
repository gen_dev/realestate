class CreatePaymentsChargebacks < ActiveRecord::Migration[6.0]
  def change
    create_table :payments_chargebacks do |t|
      t.integer :payment_id
      t.float :amount
      t.boolean :active, default: true

      t.timestamps
    end
  end
end
