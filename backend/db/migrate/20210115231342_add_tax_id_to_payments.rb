class AddTaxIdToPayments < ActiveRecord::Migration[6.0]
  def change
    add_column :payments, :tax_id, :integer
  end
end
