class CreateStaffs < ActiveRecord::Migration[6.0]
  def change
    create_table :staffs do |t|
      t.integer :organization_id
      t.string :name
      t.string :rol
      t.string :property_types, array: true, default: []
      t.string :number
      t.string :city
      t.string :operation_type

      t.timestamps
    end
  end
end
