class AddCounterpartIdToPayments < ActiveRecord::Migration[6.0]
  def change
    add_column :payments, :counterpart_id, :integer
  end
end
