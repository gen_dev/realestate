class AddContactIdToInquiries < ActiveRecord::Migration[6.0]
  def change
    add_column :inquiries, :contact_id, :integer
  end
end
