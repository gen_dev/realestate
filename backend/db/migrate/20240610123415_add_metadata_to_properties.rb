class AddMetadataToProperties < ActiveRecord::Migration[6.0]
  def change
    add_column :properties, :metadata, :jsonb, default: { hide_sell_price: false, hide_rent_price: false }
  end
end
