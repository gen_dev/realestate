class AddTicketIdToPropertiesFixes < ActiveRecord::Migration[6.0]
  def change
    add_column :properties_fixes, :ticket_id, :integer
  end
end
