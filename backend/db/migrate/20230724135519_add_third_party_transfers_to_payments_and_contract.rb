class AddThirdPartyTransfersToPaymentsAndContract < ActiveRecord::Migration[6.0]
  def change
    add_column :payments, :third_party_transfer, :boolean, default: false
    add_column :contracts, :third_party_transfer, :boolean, default: false
  end
end
