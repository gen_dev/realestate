class AddDollarizedToContracts < ActiveRecord::Migration[6.0]
  def change
    add_column :contracts, :dollarized, :boolean, default: false
  end
end
