class CreatePropertiesComments < ActiveRecord::Migration[6.0]
  def change
    create_table :properties_comments do |t|
      t.integer :observation_id
      t.string :text
      t.integer :user_id

      t.timestamps
    end
  end
end
