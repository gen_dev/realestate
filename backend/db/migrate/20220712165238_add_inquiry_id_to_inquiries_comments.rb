class AddInquiryIdToInquiriesComments < ActiveRecord::Migration[6.0]
  def change
    add_column :inquiries_comments, :inquiry_id, :integer
  end
end
