class CreateSearches < ActiveRecord::Migration[6.0]
  def change
    create_table :searches do |t|
      # both optionals
      t.integer :user_id
      t.integer :agency_id

      t.text :property_type, array: true, default: []

      t.jsonb :features, default: {
        "operation_type"=>{"rent"=>false, "sell"=>false},
        "additional_features"=>[
          {"name"=>"Servicios", "values"=>[]},
          {"name"=>"Acceso", "values"=>[]},
          {"name"=>"Comodidades y amenities", "values"=>[]},
          {"name"=>"Características adicionales", "values"=>[]},
          {"name"=>"Ambientes", "values"=>[]}
        ]
      }

      t.float :max_price, default: 0
      t.float :min_price, default: 0

      t.float :max_common_expenses, default: 0
      t.float :min_common_expenses, default: 0

      t.integer :max_brooms, default: 0
      t.integer :min_brooms, default: 0

      t.integer :max_baths, default: 0
      t.integer :min_baths, default: 0

      t.integer :max_rooms, default: 0
      t.integer :min_rooms, default: 0

      t.string :address

      t.timestamps
    end
  end
end
