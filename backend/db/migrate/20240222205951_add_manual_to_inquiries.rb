class AddManualToInquiries < ActiveRecord::Migration[6.0]
  def change
    add_column :inquiries, :manual, :boolean, default: false
  end
end
