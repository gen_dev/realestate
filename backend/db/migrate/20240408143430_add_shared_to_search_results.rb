class AddSharedToSearchResults < ActiveRecord::Migration[6.0]
  def change
    add_column :search_results, :shared, :boolean, default: false
  end
end
