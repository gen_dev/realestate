class AddStateToReceipts < ActiveRecord::Migration[6.0]
  def change
    add_column :receipts, :state, :string, default: 'pending'
  end
end
