class CreateProperties < ActiveRecord::Migration[6.0]
  def change
    create_table :properties do |t|
      t.string :property_type
      t.string :state
      t.text :description
      
      t.timestamps
    end
  end
end
