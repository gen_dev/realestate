class AddInquiryIdToSearches < ActiveRecord::Migration[6.0]
  def change
    add_column :searches, :inquiry_id, :integer
  end
end
