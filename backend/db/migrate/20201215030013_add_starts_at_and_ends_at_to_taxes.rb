class AddStartsAtAndEndsAtToTaxes < ActiveRecord::Migration[6.0]
  def change
    add_column :taxes, :starts_at, :date
    add_column :taxes, :ends_at, :date
  end
end
