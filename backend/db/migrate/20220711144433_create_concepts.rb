class CreateConcepts < ActiveRecord::Migration[6.0]
  def change
    create_table :concepts do |t|
      t.string :name
      t.integer :sub_account_id

      t.timestamps
    end
  end
end
