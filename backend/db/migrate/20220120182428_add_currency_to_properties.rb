class AddCurrencyToProperties < ActiveRecord::Migration[6.0]
  def change
    add_column :properties, :currency, :string, default: 'ARS'
  end
end
