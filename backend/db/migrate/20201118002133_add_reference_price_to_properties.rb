class AddReferencePriceToProperties < ActiveRecord::Migration[6.0]
  def change
    add_column :properties, :price, :decimal, default: nil, precision: 8, scale: 2
  end
end
