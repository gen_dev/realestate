class CreatePartialPayments < ActiveRecord::Migration[6.0]
  def change
    create_table :partial_payments do |t|
      t.decimal :value, default: nil, precision: 8, scale: 2
      t.references :payment 

      t.timestamps
    end
  end
end
