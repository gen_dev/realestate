class AddTaxableTagToPayments < ActiveRecord::Migration[6.0]
  def change
    add_column :payments, :taxable_tag, :string
  end
end
