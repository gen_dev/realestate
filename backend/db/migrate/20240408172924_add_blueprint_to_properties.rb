class AddBlueprintToProperties < ActiveRecord::Migration[6.0]
  def change
    add_column :properties, :blueprint, :string
  end
end
