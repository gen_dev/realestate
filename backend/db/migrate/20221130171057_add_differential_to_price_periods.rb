class AddDifferentialToPricePeriods < ActiveRecord::Migration[6.0]
  def change
    add_column :price_periods, :differential, :integer, default: 0
  end
end
