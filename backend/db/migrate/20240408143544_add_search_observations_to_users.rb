class AddSearchObservationsToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :search_observations, :string
  end
end
