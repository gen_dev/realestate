class AddCurrencyToPaymentTypes < ActiveRecord::Migration[6.0]
  def change
    add_column :payment_types, :currency, :string, default: "ARS"
  end
end
