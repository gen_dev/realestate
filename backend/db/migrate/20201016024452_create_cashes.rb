class CreateCashes < ActiveRecord::Migration[6.0]
  def change
    create_table :cashes do |t|
      t.string :name
      t.decimal :balance, default: nil, precision: 8, scale: 2
      t.references :agency
      
      t.timestamps
    end
  end
end
