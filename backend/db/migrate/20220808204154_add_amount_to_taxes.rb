class AddAmountToTaxes < ActiveRecord::Migration[6.0]
  def change
    add_column :taxes, :amount, :float
  end
end
