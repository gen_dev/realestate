class AddStampToReceipts < ActiveRecord::Migration[6.0]
  def change
    add_column :receipts, :stamp, :string
  end
end
