class AddDroppedToSearchResults < ActiveRecord::Migration[6.0]
  def change
    add_column :search_results, :dropped, :boolean, default: false
  end
end
