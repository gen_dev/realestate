class AddConceptIdToTaxes < ActiveRecord::Migration[6.0]
  def change
    add_column :taxes, :concept_id, :integer
  end
end
