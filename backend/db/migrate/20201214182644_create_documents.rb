class CreateDocuments < ActiveRecord::Migration[6.0]
  def change
    create_table :documents do |t|
      t.string :name
      t.integer :size
      t.references :documentable, polymorphic: true

      t.timestamps
    end
  end
end
