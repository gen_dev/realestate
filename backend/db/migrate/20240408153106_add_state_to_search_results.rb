class AddStateToSearchResults < ActiveRecord::Migration[6.0]
  def change
    add_column :search_results, :state, :string, default: "Pendiente"
  end
end
