class AddActiveToSearchResults < ActiveRecord::Migration[6.0]
  def change
    add_column :search_results, :active, :boolean, default: true
  end
end
