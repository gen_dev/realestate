class CreateBuildingsCommonExpenses < ActiveRecord::Migration[6.0]
  def change
    create_table :buildings_common_expenses do |t|
      t.integer :building_id

      t.timestamps
    end
  end
end
