class AddAmountToBuildingsCommonExpenseLines < ActiveRecord::Migration[6.0]
  def change
    add_column :buildings_common_expense_lines, :amount, :float
  end
end
