class AddLftToImages < ActiveRecord::Migration[6.0]
  def change
    add_column :images, :lft, :integer, default: 0
  end
end
