class AddTagToPayments < ActiveRecord::Migration[6.0]
  def change
    add_column :payments, :contract_tag, :string
  end
end
