class AddContractIdToReceipts < ActiveRecord::Migration[6.0]
  def change
    add_column :receipts, :contract_id, :integer
  end
end
