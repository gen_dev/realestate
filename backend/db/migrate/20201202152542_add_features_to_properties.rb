class AddFeaturesToProperties < ActiveRecord::Migration[6.0]
  def change
    add_column :properties, :features, :json
  end
end
