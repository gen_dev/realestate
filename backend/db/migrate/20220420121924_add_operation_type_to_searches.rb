class AddOperationTypeToSearches < ActiveRecord::Migration[6.0]
  def change
    add_column :searches, :operation_type, :string, default: 'rent'
  end
end
