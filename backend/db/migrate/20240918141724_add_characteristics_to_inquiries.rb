class AddCharacteristicsToInquiries < ActiveRecord::Migration[6.0]
  def change
    add_column :inquiries, :characteristics, :json, default: {}
  end
end
