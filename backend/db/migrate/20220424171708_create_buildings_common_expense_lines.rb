class CreateBuildingsCommonExpenseLines < ActiveRecord::Migration[6.0]
  def change
    create_table :buildings_common_expense_lines do |t|
      t.integer :common_expense_id
      t.string :description

      t.timestamps
    end
  end
end
