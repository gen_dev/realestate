class AddActiveToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :agencies, :approved, :boolean, default: true
    add_column :properties, :approved, :string, default: 'pending'
  end
end
