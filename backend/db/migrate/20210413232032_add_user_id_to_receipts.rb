class AddUserIdToReceipts < ActiveRecord::Migration[6.0]
  def change
    add_column :receipts, :user_id, :integer
  end
end
