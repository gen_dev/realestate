class AddColumnSignToPricePeriods < ActiveRecord::Migration[6.0]
  def change
    add_column :price_periods, :sign, :string
  end
end
