class CreateContractExpense < ActiveRecord::Migration[6.0]
  def change
    create_table :contract_expenses do |t|
      t.string :name
      t.decimal :value, default: nil, precision: 8, scale: 2
      t.references :contract_plan

      t.timestamps
    end
  end
end
