class AddCommonExpensesToProperties < ActiveRecord::Migration[6.0]
  def change
    add_column :properties, :common_expenses, :float
  end
end
