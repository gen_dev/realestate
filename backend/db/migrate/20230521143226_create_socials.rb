class CreateSocials < ActiveRecord::Migration[6.0]
  def change
    create_table :socials do |t|
      t.integer :agency_id
      t.string :name
      t.string :link
      t.string :image
      t.string :button_tag
      t.string :description

      t.timestamps
    end
  end
end
