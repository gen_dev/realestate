class CreateReceipts < ActiveRecord::Migration[6.0]
  def change
    create_table :receipts do |t|
      t.integer :agency_id
      t.integer :number

      t.timestamps
    end
  end
end
