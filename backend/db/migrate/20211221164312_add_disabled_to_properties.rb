class AddDisabledToProperties < ActiveRecord::Migration[6.0]
  def change
    add_column :properties, :disabled, :boolean, default: false
  end
end
