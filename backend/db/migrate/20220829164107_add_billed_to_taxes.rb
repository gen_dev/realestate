class AddBilledToTaxes < ActiveRecord::Migration[6.0]
  def change
    add_column :taxes, :billed, :boolean, default: false
  end
end
