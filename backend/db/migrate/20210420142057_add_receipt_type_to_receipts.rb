class AddReceiptTypeToReceipts < ActiveRecord::Migration[6.0]
  def change
    add_column :receipts, :receipt_type, :string
  end
end
