class AddStartsAtAndEndsAtToContracts < ActiveRecord::Migration[6.0]
  def change
    add_column :contracts, :starts_at, :date
    add_column :contracts, :ends_at, :date
  end
end
