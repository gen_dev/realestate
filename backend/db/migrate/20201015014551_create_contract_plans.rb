class CreateContractPlans < ActiveRecord::Migration[6.0]
  def change
    create_table :contract_plans do |t|
      t.date :start_date
      t.date :due_date
      t.integer :duration
      t.integer :rent_update_period
      t.jsonb :rent_value
      t.references :contract
      t.references :property

      t.timestamps
    end
  end
end
