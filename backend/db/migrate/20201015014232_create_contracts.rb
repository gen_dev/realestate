class CreateContracts < ActiveRecord::Migration[6.0]
  def change
    create_table :contracts do |t|
      t.string :state
      t.string :contract_type
      t.references :property

      t.timestamps
    end
  end
end
