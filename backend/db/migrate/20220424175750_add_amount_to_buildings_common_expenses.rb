class AddAmountToBuildingsCommonExpenses < ActiveRecord::Migration[6.0]
  def change
    add_column :buildings_common_expenses, :amount, :float, default: 0
  end
end
