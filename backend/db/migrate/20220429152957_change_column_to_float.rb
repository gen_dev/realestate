class ChangeColumnToFloat < ActiveRecord::Migration[6.0]
  def change
    change_column :properties, :price, :float
  end
end
