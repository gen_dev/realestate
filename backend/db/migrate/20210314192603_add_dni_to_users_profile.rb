class AddDniToUsersProfile < ActiveRecord::Migration[6.0]
  def change
    add_column :users_profiles, :dni, :string
  end
end
