class ChangePropertiesFeaturesToJsonb < ActiveRecord::Migration[6.0]
  def change
    change_column :properties, :features, :jsonb
  end
end
