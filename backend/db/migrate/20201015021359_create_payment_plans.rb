class CreatePaymentPlans < ActiveRecord::Migration[6.0]
  def change
    create_table :payment_plans do |t|
      t.string :tenant
      t.string :owner
      t.string :payment_methods, array: true, default: []
      t.references :contract

      t.timestamps
    end
  end
end
