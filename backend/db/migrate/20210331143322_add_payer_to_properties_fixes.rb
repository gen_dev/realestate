class AddPayerToPropertiesFixes < ActiveRecord::Migration[6.0]
  def change
    add_column :properties_fixes, :payer_id, :integer
    add_column :properties_fixes, :payer_type, :string
  end
end
