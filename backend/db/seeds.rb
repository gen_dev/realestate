# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).

# User
agency = Agency.create(name: 'Sartor Rqta.')
agency2 = Agency.create(name: 'Sartor Avda.')
["inmobiliariasartor@yahoo.com.ar", "buseghinv@gmail.com", "lorenaberlanda@yahoo.com.ar", "franco_sartor@hotmail.com", "ramirosartor@hotmail.com"].each do |email|
  password = email.split("@")[0] + "_psw"
  puts password
  user = User.create(email: email, password: password)
  user.add_role :gendev 

  # Agency
  UserAgency.create(selected: true, agency: agency, user: user)
  UserAgency.create(selected: false, agency: agency2, user: user)
end
