Rails.application.routes.draw do
  post "/graphql", to: "graphql#execute"
  post "/files", to: "files#upload"
  get "/files", to: "files#test"
  get "/balance_report/:id", to: "reports#balance"
  get "/properties", to: "properties#properties"
  post "/tokko_import", to: "properties#tokko"

  if Rails.env.development?
    mount GraphiQL::Rails::Engine, at: "/graphiql", graphql_path: "/graphql"
  end

  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  mount ActionCable.server => '/cable'
end
