# README

| Model            | Spanish Name                      |
| ------           | ------                            |
| Address          | Dirección                         |
| Agency           | Inmobiliaria                      |
| Cash             | Caja                              |
| Contract Expense | Gastos de contrato (ej: sellado)  |
| Contract Plan    | Plan del contrato                 |
| Contract         | Contrato                          |
| Partial Payment  | Pago parcial                      |
| Payment Plan     | Plan de pago                      |
| Payment          | Pago ("Asiento")                  |
| Property         | Inmueble                          |
| Simple Data      | Características (ej: de Inmueble) |
| Role             | Rol                               |
| Tax              | Impuesto                          |
| User Agency      | Relacion User/Inmobiliaria        |
| User             | Usuario                           |

## Posibles estados 
- Contrato: Ongoing / Draft / Finalizado
- Propietario e Inquilino: Al día / Moroso
- Pago: Realizado / Pendiente / Vencido

## Queries / Mutations  / etc

Read [this](GraphQL.md).

## Image / Documents Upload

- File uploads are handled by REST (not graphQL) because it works better.
- Files can be uploaded doing a POST request to /files with the params: files (Array: 1 or multiple files), object_class (String: "Contract" / "Property"), object_id (Integer: ID of object).
- File uploads can be tested from localhost:3000/files (there's a form already configured to post files).