export default {
  action: {
    add: 'Añadir',
    add_tax: 'Añadir impuesto',
    add_fix: 'Añadir reparación',
    cancel: 'Cancelar',
    click_here: 'click aquí',
    close: 'Cerrar',
    close_and_continue_later: 'Cerrar y continuar luego',
    confirm: 'Confirmar',
    create: 'Crear',
    delete: 'Borrar',
    edit: 'Editar',
    next: 'Siguiente',
    publish: 'Publicar',
    save: 'Guardar',
    search: 'Buscar',
    signin: 'Iniciar sesión',
    signup: 'Crear usuario',
    signout: 'Cerrar sesión',
  },
  investmentTypes: {
    residential: "Residencial",
    commercial: "Comercial",
    office: "Oficina",
  },
  tenantActions: {
    none: 'Sin efecto',
    add: 'Sumar al pago',
    substract: 'Descontar al pago',
  },
  ownerActions: {
    none: 'Sin efecto',
    substract: 'Sumar a lo que cobra',
    add: 'Descontar de lo que cobra',
  },
  agencyActions: {
    none: 'Sin efecto',
    substract: 'Egreso',
    add: 'Ingreso',
  },

  create: {
    contract: 'Nuevo contrato',
    property: 'Nuevo inmueble',
    payment: 'Nuevo asiento'
  },
  cash: {
    list: {
      title: 'Caja'
    }
  },
  contact: {
    list: {
      headers: {
        address: 'DIRECCIÓN',
        contact: 'CONTACTO',
        name: 'NOMBRE',
        role: 'ROL',
      },
      filters: {
        role: 'Rol',
        search: 'Buscar',
      },
      no_contacts_found: 'No se encontraron resultados. Presione "Nuevo contacto" si desea añadir un nuevo contacto',
      title: 'Agenda',
    },
    new: {
      fields: {
        address: 'Dirección',
        email: 'Email',
        phone: 'Teléfono',
        role: 'Rol',
      },
      title: 'Nuevo contacto'
    },
    show: {
      delete_modal: {
        confirm_message: '¿Está seguro que desea eliminar el contacto {name}?',
        title: 'Eliminar contacto'
      },
      fields: {
        address: 'Dirección',
        email: 'Email',
        phone: 'Teléfono',
        role: 'Rol',
      }
    }
  },
  payment: {
    list: {
      title: 'Listado de asientos',
      tabs: {
        general: 'General',
        accountability: 'Rendiciones',
        collections: 'Cobranzas',
        agency: 'Inmobiliaria'
      },
      filters: {
        concept: 'Concepto',
        search: 'Buscar',
        state: 'Estado',
        type: 'Tipo de asiento',
        user_type: 'Responsable'
      },
      state: {
        draft: 'Borrador',
        paid: 'Pagado',
        pending: 'Pendiente',
        rejected: 'Rechazado',
        aproved: 'Aprobado',
      },
    }
  },
  states: {
    pending: 'Pendiente',
    success: 'Completado',
    ongoing: 'En curso',
    ongoing: 'En curso',
    on_hold: 'Esperando respuesta',
    watching: 'En observación'
  },
  contract: {
    list: {
      headers: {
        address: 'DIRECCIÓN',
        date: 'FECHA',
        number: 'Nº',
        owners: 'LOCADOR(ES)',
        state: 'ESTADO',
        tenant: 'LOCATARIO',
      },
      filters: {
        location: 'Ubicación',
        owners: 'Locador(es)',
        role: 'Rol',
        search: 'Buscar',
        state: 'Estado',
        tenant: 'Locatario',
      },
      state: {
        draft: 'Borrador',
        published: 'En curso',
        finished: 'Finalizado',
        ongoing: 'En curso'
      },
      no_contracts_found: 'No se encontraron contratos de este tipo. Puede crear uno haciendo ',
      tabs: {
        rent: 'Alquiler',
        sell: 'Venta'
      },
      title: 'Listado de Contratos',
    },
    show: {
      delete_modal: {
        confirm_message: '¿Está seguro que desea eliminar el contrato?',
        title: 'Eliminar contrato'
      },
      headers: {
        rent: {
          owners: 'Locador(es): {name}',
        },
        sell: {
          owners: 'Propietario(s): {name}'
        },
        owners: 'Locador(es): {name}',
        tenant: 'Locatario: {name}',
      },
      main_panel: {
        filters: {
          search: 'Buscar'
        },
        headers: {
          balance: 'Balance:',
          owners: 'Locador(es): {name}',
          payments: 'Pagos',
          tenant: 'Locatario: {name}',
        },
        payments: {
          settings: 'Configuración',
        }
      },
      panel_one: {
        items: {
          keys: 'Llaves',
          owners: 'Locador(es)',
          period: 'Período {n}',
          tenant: 'Locatario',
          term: 'Duración',
          total_contract_price: 'Valor total del contrato',
          months: {
            one: '1 mes',
            n: '{n} meses',
          },
          from: 'Desde',
          to: 'Hasta',
        },
        title: 'Plan de contrato',
      },
      panel_two: {
        items: {
          fee: 'Honorarios',
          payment_methods: 'Formas de pago',
        },
        title: 'Plan de pago',
      },
      panel_three: {
        items: {
          guarantee: 'Garantes',
        },
        title: 'Documentación',
      }
    }
  },
  error: {
    password: {
      did_not_match: 'Contraseña incorrecta'
    },
    address: {
      address: {
        already_exists_for_that_property: 'La dirección ingresada ya se encuentra en uso.'
      }
    },
    user: {
      not_found: 'Usuario no encontrado'
    },
    has_already_been_taken: 'Ya se encuentra en uso'
  },
  field: {
    email: 'Email',
    password: 'Contraseña',
    username: 'Nombre de usuario'
  },
  home: 'Inicio',
  property: {
    type: {
      apartment: 'Departamento',
      farm: 'Campo',
      house: 'Casa',
      terrain: 'Terreno',
      salon: 'Salón',
      shed: 'Galpón',
      parking: 'Cochera',
      warehouse: 'Depósito'
    },
    create: {
      description: {
        title: 'Descripción',
        instructions: 'Describí el inmueble',
        fields: {
          access: {
            title: 'Acceso',
            dirt: 'Tierra',
            enhanced_dirt: 'Tierra mejorado o arenado',
            gravel: 'Ripio',
            pavement: 'Pavimento',
          },
          services: {
            title: 'Servicios',
            drinking_water_network: 'Red de agua potable',
            water_drilling: 'Perforación agua',
            gas_network: 'Red de gas',
            sewer: 'Red de saneamiento (cloaca)',
            electric_netowrk: 'Red eléctrica',
            street_lighting: 'Alumbrado público',
          },
          additional_characteristics: {
            title: 'Características adicionales',
            water: 'Agua corriente',
            furnished: 'Amoblado',
            boiler: 'Caldera',
            gas: 'Gas Natural',
            forested: 'Forestado',
            sprinkler_irrigation: 'Riego por asperción',
            masonry_perimeter_enclosure: 'Cerramiento perimetral mamposteria',
            green_perimeter_enclosure: 'Cerramiento perimetral verde',
          },
          additional_features: { 
            title: 'Especificaciones adicionales',
            antiquity: 'Antigüedad',
            antiquity_suffix: 'años',
            floors_quantity: 'Cantidad de pisos',
            apartments_by_floor: 'Departamentos por piso',
            floor_number: 'Número de piso',
          },
          comforts_and_amenities: { 
            title: 'Comodidades y amenities',
            internet: 'Acceso a internet',
            air_conditioning: 'Aire acondicionado',
            childrens_play_area: 'Área de juegos infantiles',
            elevator: 'Ascensor',
            business_center: 'Business center',
            heating: 'Calefacción',
            tennis_court: 'Cancha de tenis',
            fireplace: 'Chimenea',
            tank: 'Cisterna',
            visitor_parking: 'Estacionamiento para visitantes', 
            gym: 'Gimnasio', 
            jacuzzi: 'Jacuzzi', 
            laundry: 'Laundry', 
            pets: 'Mascotas', 
            pool: 'Pileta', 
            reception: 'Recepción', 
            party_room: 'Salón de fiestas', 
            multiple_uses_room: 'Salón de usos múltiples', 
            security: 'Seguridad 24 horas',
          },
          environments: { 
            title: 'Ambientes',
            balcony: 'Balcón',
            grill: 'Parrilla',
            kitchen: 'Cocina',
            courtyard: 'Patio',
            dinning_room: 'Comedor',
            placards: 'Placards',
            service_unit: 'Dependencia de servicio',
            playroom: 'Playroom',
            breakfast_nook: 'Desayunador',
            roof_garden: 'Roof Garden',
            suite_bedroom: 'Dormitorio en suite',
            terrace: 'Terraza',
            studio: 'Estudio',
            toilette: 'Toilette',
            garden: 'Jardín',
            dressing_room: 'Vestidor',
            laundry_room: 'Lavadero',
            living_room: 'Living',
          },  
          features: { 
            title: 'Especificaciones adicionales',
            location: 'Ubicación en manzana',
            garage: 'Cochera',
            bathrooms: 'Baños',
            bedrooms: 'Dormitorios',
            covered_surface: 'Superficie cubierta',
            covered_surface_suffix: 'm2',
            mid_covered_surface: 'Superficie semi-cubierta',
            mid_covered_surface_suffix: 'm2',
            total_surface: 'Superficie total',
            total_surface_suffix: 'm2',
            facing: 'Orientación',
            terrain_width: 'Ancho del terreno',
            terrain_depth: 'Largo del terreno',
          },    
        }
      },
      pictures: {
        title: 'Fotos',
        instructions: 'Subí fotos del inmueble',
        fields: {
          file: 'Archivos a cargar'
        },
        image_process: {
          completed: 'Completado',
          image_already_uploaded: 'Imagen ya cargada previamente.',
          unknown_size: 'Tamaño desconocido',
          could_not_upload_file: 'No se pudo subir el archivo: {file_name}'
        }
      },
      price: {
        title: 'Precio',
        instructions: 'Ingresá los precios',
        fields: {
          amount: 'Monto',
          currency: 'ARS'
        }
      },
      title: 'NUEVO INMUEBLE',
      temporarily_disabled: 'Temporalmente desactivado.',
      type: {
        title: 'Tipo de publicación',
        instructions: 'Seleccioná el tipo de publicación',
        fields: {
          address: 'Dirección',
          street: 'Calle',
          lat: 'Latitud',
          lng: 'Longitud',
          number: 'Altura',
          city: 'Ciudad',
          state: 'Provincia',
          country: 'País',
          apartment_unit: 'Departamento / Unidad',
          apartment_unit_hint: 'Ejemplo: Piso 7 Departamento 4',
          operation_type: 'Tipo de publicación',
          owners: 'Locador(es)',
          property_type: 'Tipo de propiedad',
          property_types: {
            apartment: 'Departamento',
            country_house: 'Quinta',
            farm: 'Campo',
            house: 'Casa',
            terrain: 'Terreno',
            warehouse: 'Depósito',
            parking: 'Cochera',
            shed: 'Galpón',
            salon: 'Salón'
          },
          rent: 'Alquiler',
          schedule: 'Agregar a agenda',
          sell: 'Venta',
        }
      },
    },
    detail: {
      show_contract: 'Ver contrato',
      tabs: {
        contract: 'Contrato',
        characteristics: 'Características',
        taxes: 'Impuestos',
        fixes: 'Reparaciones',
      }
    },
    headers: {
      fixes: 'Reparaciones',
    },
    list: { 
      characteristics: {
        antiquity: 'Antigüedad',
        bathrooms: 'Baños',
        bedrooms: 'Dormitorios',
        environments: 'Ambientes',
        total_surface: 'Superficie total',
      },
      delete_modal: {
        confirm_message: '¿Está seguro que desea eliminar el inmueble?',
        title: 'Eliminar inmueble'
      },
      filters: {
        antiquity: 'Antigüedad',
        price: 'Precio',
        price_label: '{a} hasta {b}',
        property_type: 'Tipo de propiedad',
        state: 'Estado',
        search: 'Buscar'
      },
      headers: {
        price: 'PRECIO',
        property: 'PROPIEDAD',
        state: 'ESTADO',
      },
      notifications: {
        updated_with_success: '¡Inmueble actualizado con éxito!'
      },
      no_properties_found: 'No se encontraron propiedades de este tipo. Puede crear una haciendo ',
      tabs: {
        rent: 'Alquiler',
        sell: 'Venta'
      },
      title: 'Listado de Inmuebles'
    },
    operation_type: {
      rent: 'Alquiler',
      sell: 'Venta'
    },
    state: {
      draft: 'Borrador',
      published: 'Publicado',
      rented: 'Alquilado',
      booked: 'Reservado',
    },
    type: {
      apartment: 'Departamento',
      farm: 'Campo',
      house: 'Casa',
      terrain: 'Terreno',
      salon: 'Salón',
      shed: 'Galpón',
      parking: 'Cochera',
      warehouse: 'Depósito'
    }
  },
  required_fields_caption: '* Información obligatoria',
  required_fields_hint: '* Obligatorio',
  roles: {
    sell: {
      owners: 'Propietario(s)'
    },
    rent: {
      owners: 'Locador(es)',
      tenant: 'Locatario'
    },
    buyer: 'Comprador',
    contact: 'Contacto',
    guarantor: 'Garante',
    owners: 'Locador(es)',
    owner: 'Locador',
    seller: 'Vendedor',
    tenant: 'Locatario',
    gendev: 'Admin Técnico'
  },
  success: {
    user_creation: 'Usuario creado con éxito'
  },
  validation: {
    is_required: 'Campo obligatorio',
    must_be_a_valid_email: 'Debe ser un email válido',
    must_be_gte_to_6_chars: 'Debe ser mayor o igual a 6 caracteres',
    must_be_gte_to_8_chars: 'Debe ser mayor o igual a 8 caracteres',
    must_be_gte_to_n_chars: 'Debe ser mayor o igual a {n} caracteres' // TODO: use this and remove 6 and 8
  },
  filters: {
    maxPrice: 'Precio máximo',
    minPrice: 'Precio mínimo',
    maxBrooms: 'Max. dormitorios',
    minBrooms: 'Min. dormitorios',
    maxBaths: 'Max. baños',
    minBaths: 'Min. baños',
    minRooms: 'Max. salas',
    maxRooms: 'Min. salas',
    minCoveredSurface: 'Min. superficie útil',
    maxCoveredSurface: 'Max. superficie útil',
    minSurface: 'Min. superficie total',
    maxSurface: 'Max. superficie total',
    minCommonExpenses: 'Max. gastos comunes',
    maxCommonExpenses: 'Min. gastos comunes'
  },
  operation_type: {
    rent: 'Alquiler',
    sell: 'Venta'
  },
}
