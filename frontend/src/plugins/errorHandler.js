class ErrorHandler {
  // TODO: Add handler for other errors
  getFormErrors (errors) {
    if (!errors) { return {} }
    if (!errors[0].extensions) { return {} }
    let arr = {}
    errors.forEach(error => { Object.assign(arr, { [error.extensions.field]: error.extensions.message }) })
    return arr
  }
}

export default {
  install(Vue, options) {
    Vue.prototype.$errorHandler = new ErrorHandler
  }
}