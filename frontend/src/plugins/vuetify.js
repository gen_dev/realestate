import Vue from 'vue';
import Vuetify from 'vuetify/lib';
import "vuetify/dist/vuetify.min.css";

Vue.use(Vuetify);

export default new Vuetify({
 theme: {
  themes: {
   light: {
    primary: '#293448',
    secondary: '#B4C4AE',
    accent: '#8c9eff',
    background: '#f8f9fb',
    succes: '#6c8762',
    pills: '#ebebeb',
    error: '#db2b39',
    bars: "#EEE8E8",
    filters_bg: "DFDFDF"
   },
  },
 },
});
