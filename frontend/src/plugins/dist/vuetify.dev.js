"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _vue = _interopRequireDefault(require("vue"));

var _vuetify = _interopRequireDefault(require("vuetify"));

require("vuetify/dist/vuetify.min.css");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

_vue["default"].use(_vuetify["default"]);

var _default = new _vuetify["default"]({
  theme: {
    themes: {
      light: {
        primary: '#293448',
        secondary: '#3b4a67',
        accent: '#8c9eff',
        background: '#f8f9fb',
        succes: '#6c8762',
        pending: '#969125',
        error: '#db2b39',
        highlight: "#67318F"
      },
      dark: {
        primary: "#67318F",
        secondary: "#C73024",
        highlight: "#67318F"
      }
    }
  }
});

exports["default"] = _default;