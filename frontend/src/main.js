import Vue from 'vue'
import '@mdi/font/css/materialdesignicons.css'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify'
import * as VueGoogleMaps from "vue2-google-maps";
import VuetifyGoogleAutocomplete from 'vuetify-google-autocomplete';
import { apolloProvider } from './graphql/apollo'
import "@/style/application.scss"
import errorHandler from './plugins/errorHandler'
import VueI18n from 'vue-i18n'
import messages from './i18n'
import wysiwyg from "vue-wysiwyg"

Vue.config.productionTip = false
Vue.use(errorHandler)
Vue.use(VueI18n)

const i18n = new VueI18n({
  locale: 'es', // TODO: dynamize
  messages
})

Vue.use(VueGoogleMaps, {
  load: {
    key: "AIzaSyB3RTiKtL9Wx4PT1gwp9FsRGsDu9qJittc",
    libraries: "places", // necessary for places input
    region: "ar",
    language: "es"
  }
})

Vue.use(wysiwyg, {
  hideModules: {
    "image": true,
    link: true,
    code: true,
    table: true,
    separator: true
  },
  locale: 'es'
})

Vue.use(VuetifyGoogleAutocomplete, {
  vueGoogleMapsCompatibility: true,
});

new Vue({
  router,
  store,
  vuetify,
  i18n,
  apolloProvider,
  render: function (h) { return h(App) }
}).$mount('#app')
