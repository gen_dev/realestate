import http from "../http-common";

class UploadFilesService {
  upload(files, objectClass, objectId, onUploadProgress) {
    let formData = new FormData();

    if (files && files.length) {
      files.forEach( file => {
        if (file != null) {
          formData.append("files[]", file);
        } else {
        }
      })
    } else {
    }

    formData.append("object_class", objectClass);
    formData.append("object_id", objectId);

    return http.post("/files", formData, {
      headers: {
        "Content-Type": "multipart/form-data"
      },
      onUploadProgress
    });
  }
}

export default new UploadFilesService();
