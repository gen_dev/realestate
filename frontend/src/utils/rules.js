import messages from '../i18n'
import { LocaleService } from '../services/storageService'

const locale = LocaleService.getLocale()

function getMsg (code) {
  let lcl = (Object.keys(messages).includes(locale)) ? locale : 'es'
  return messages[lcl].validation[code]
}

export const Required = [
  v => !!v || getMsg('is_required')
]

export const Email = [
  ...Required,
  v => {
    const pattern = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return pattern.test(v) || getMsg('must_be_a_valid_email')
  }
]

export const Password = [
  ...Required,
  v => (!!v && v.length >= 6) || getMsg('must_be_gte_to_6_chars'),
]

export const Username = [
  ...Required,
  v => v.length >= 8 || getMsg('must_be_gte_to_8_chars'),
]
