import Vue from "vue";
import VueRouter from "vue-router";
import SignIn from "../views/SignIn.vue";
import error404 from "../views/errors/404.vue";
import { TokenService } from "../services/storageService";
import Directory from "@/views/Directory";
import Properties from "@/views/properties/Properties";
import MainTickets from "@/views/tickets/Tickets";
import Tickets from "@/views/contracts/Tickets";
import Ticket from "@/views/contracts/Ticket";
import Contracts from "@/views/contracts/Contracts";
import Contract from "@/views/contracts/Contract";
import CreateContract from "@/views/contracts/Create";
import CreateProperty from "@/views/properties/Create";
import CreateTax from "@/views/taxes/Create";

Vue.use(VueRouter);

const routes = [
  {
    path: "/agenda",
    name: "Home",
    component: Directory
  },
  {
    path: "/",
    name: "Analytics",
    component: () => {
      return import(
        /* webpackChunkName: "analytics" */ "../views/Analytics.vue"
      );
    }
  },
  {
    path: "/staff",
    name: "staff",
    component: () => {
      return import(/* webpackChunkName: "staff" */ "../views/Staff.vue");
    }
  },
  {
    path: "/signin",
    name: "signin",
    component: SignIn,
    meta: {
      public: true, // Allow access to even if not logged in
      onlyWhenLoggedOut: true
    }
  },
  {
    path: "/balances",
    name: "balances",
    component: () => {
      return import(
        /* webpackChunkName: "balances" */ "../views/balances/Balances.vue"
      );
    }
  },
  {
    path: "/payments",
    name: "payments",
    component: () => {
      return import(
        /* webpackChunkName: "payments" */ "../views/payments/Payments.vue"
      );
    }
  },
  {
    path: "/inquiries",
    name: "inquiries",
    component: () => {
      return import(
        /* webpackChunkName: "inquiries" */ "../views/inquiries/Inquiries.vue"
      );
    }
  },
  {
    path: "/buildings",
    name: "buildings",
    component: () => {
      return import(
        /* webpackChunkName: "buildings" */ "../views/buildings/Buildings.vue"
      );
    }
  },
  {
    path: "/buildings/:id",
    name: "show_building",
    component: () => {
      return import(
        /* webpackChunkName: "buildings" */ "../views/buildings/Building.vue"
      );
    }
  },
  {
    path: "/a/:slug",
    name: "agency",
    component: function() {
      return import(
        /* webpackChunkName: "properties" */ "../views/agencies/Home.vue"
      );
    }
  },
  {
    path: "/matches",
    name: "matches",
    props: true,
    component: function() {
      return import(
        /* webpackChunkName: "notifications" */ "../views/Matches.vue"
      );
    }
  },
  {
    path: "/notifications",
    name: "Notifications",
    props: true,
    component: function() {
      return import(
        /* webpackChunkName: "notifications" */ "../views/notifications/Notifications.vue"
      );
    }
  },
  {
    path: "/properties",
    name: "Properties",
    props: true,
    component: function() {
      return import(
        /* webpackChunkName: "properties" */ "../views/properties/Properties.vue"
      );
    }
  },
  {
    path: "/property/:id",
    name: "Property",
    component: function() {
      return import(
        /* webpackChunkName: "property" */ "../views/properties/Property.vue"
      );
    }
  },
  {
    path: "/properties/create",
    name: "CreateProperty",
    props: true,
    component: function() {
      return import(
        /* webpackChunkName: "CreateProperty" */ "../views/properties/Create.vue"
      );
    },
    meta: {
      noSidebar: true,
      navbar: true
    }
  },
  {
    path: "/properties/edit/:id",
    name: "EditProperty",
    component: function() {
      return import(
        /* webpackChunkName: "CreateProperty" */ "../views/properties/Create.vue"
      );
    },
    meta: {
      noSidebar: true,
      navbar: true
    }
  },
  {
    path: "/contracts",
    name: "Contracts",
    component: Contracts
  },
  {
    path: "/tickets",
    name: "MainTickets",
    component: MainTickets
  },
  {
    path: "/contract/:id",
    name: "Contract",
    props: true,
    component: Contract
  },
  {
    path: "/contract/:id/tickets",
    name: "Tickets",
    component: Tickets
  },
  {
    path: "/contract/:id/ticket/:ticket_id",
    name: "ShowTicket",
    component: Ticket
  },
  {
    path: "/contracts/create",
    name: "CreateContract",
    props: true,
    component: CreateContract,
    meta: {
      noSidebar: true,
      navbar: true
    }
  },
  {
    path: "/contracts/edit/:id",
    name: "EditContract",
    component: CreateContract,
    meta: {
      noSidebar: true,
      navbar: true
    }
  },
  {
    path: "/contract/:id/taxes/create",
    name: "CreateTax",
    props: true,
    component: CreateTax,
    meta: {
      noSidebar: true,
      navbar: true
    }
  },
  {
    path: "/property/:id/taxes/create",
    name: "CreatePropertyTax",
    props: true,
    component: CreateTax,
    meta: {
      noSidebar: true,
      navbar: true
    }
  },
  {
    path: "/buildings/:id/taxes/create",
    name: "CreateBuildingTax",
    props: true,
    component: CreateTax,
    meta: {
      noSidebar: true,
      navbar: true
    }
  },
  {
    path: "/directory/:id/taxes/create",
    name: "CreateUserTax",
    props: true,
    component: CreateTax,
    meta: {
      noSidebar: true,
      navbar: true
    }
  },
  {
    path: "/contract/:id/taxes/edit/:tid",
    name: "EditTax",
    component: CreateTax,
    meta: {
      noSidebar: true,
      navbar: true
    }
  },
  {
    path: "/property/:id/taxes/edit/:tid",
    name: "EditPropertyTax",
    component: CreateTax,
    meta: {
      noSidebar: true,
      navbar: true
    }
  },
  {
    path: "/directory",
    name: "Directory",
    component: function() {
      return import(
        /* webpackChunkName: "directory" */ "../views/Directory.vue"
      );
    }
  },
  {
    path: "/directory/:id/detail",
    name: "admin_user_detail",
    component: function() {
      return import(
        /* webpackChunkName: "directory" */ "../views/users/Detail.vue"
      );
    },
    meta: {
      contained: true
    }
  },
  {
    path: "/directory/:id/taxes",
    name: "user_taxes",
    component: () => {
      return import(
        /* webpackChunkName: "balances" */ "../views/users/Taxes.vue"
      );
    }
  },
  {
    path: "/settings",
    name: "Settings",
    component: function() {
      return import(/* webpackChunkName: "settings" */ "../views/Settings.vue");
    }
  },
  {
    path: "/payments/rent/:user_id/receipt/:id",
    name: "TenantReceipt",
    props: true,
    component: function() {
      return import(
        /* webpackChunkName: "receipts" */ "../views/payments/receipts/Tenant.vue"
      );
    },
    meta: {
      noSidebar: true
    }
  },
  {
    path: "/payments/owner/:user_id/receipt/:id",
    name: "OwnerReceipt",
    props: true,
    component: function() {
      return import(
        /* webpackChunkName: "receipts" */ "../views/payments/receipts/Owner.vue"
      );
    },
    meta: {
      noSidebar: true
    }
  },
  //This route should go last since it's the 404 page.
  {
    path: "/404",
    name: "404",
    component: error404
  },
  { path: "*", redirect: "/404" }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

router.beforeEach((to, from, next) => {
  const isPublic = to.matched.some(record => record.meta.public);
  const onlyWhenLoggedOut = to.matched.some(
    record => record.meta.onlyWhenLoggedOut
  );
  const loggedIn = !!TokenService.getToken();

  if (!isPublic && !loggedIn) {
    return next({
      path: "/signin",
      query: { redirect: to.fullPath } // Store the full path to redirect the user to after login
    });
  }

  // Do not allow user to visit login page or register page if they are logged in
  if (loggedIn && onlyWhenLoggedOut) {
    return next("/");
  }

  next();
});

export default router;
