import gql from "graphql-tag";

export const Searches = gql`
  query searches($userId: ID, $staffId: ID) {
    searches(userId: $userId, staffId: $staffId) {
      id
      state
      createdAt
      resultsCount
      currency
      minBrooms
      maxBrooms
      minRooms
      maxRooms
      minBaths
      maxBaths
      minPrice
      maxPrice
      minCommonExpenses
      maxCommonExpenses
      operationType
      address
      propertyType
      features
      unseenResults
    }
  }
`;

export const StaffSearches = gql`
  query searches($userId: ID, $staffId: ID) {
    searches(userId: $userId, staffId: $staffId) {
      id
      state
      createdAt
      resultsCount
      currency
      minBrooms
      maxBrooms
      minRooms
      maxRooms
      minBaths
      maxBaths
      minPrice
      maxPrice
      minCommonExpenses
      maxCommonExpenses
      operationType
      address
      propertyType
      features
      unseenResults
      user {
        id
        profile {
          name
          email
          address
          phone
        }
      }
    }
  }
`;

export const UserResults = gql`
  query userResults {
    userResults {
      id
      state
      createdAt
      resultsCount
      currency
      minBrooms
      maxBrooms
      minRooms
      maxRooms
      minBaths
      maxBaths
      minPrice
      maxPrice
      minCommonExpenses
      maxCommonExpenses
      operationType
      address
      propertyType
      features
      unseenResults
      user {
        id
        profile {
          name
          email
          address
          phone
        }
      }
    }
  }
`;

export const AgencyResults = gql`
  query agencyResults($page: ID) {
    agencyResults(page: $page) {
      pages
      properties {
        id
        propertyType
        state
        currency
        price
        disabled
        fullAddress
        unseenResults
        seller {
          id
          name
        }
        lastResult {
          id
          createdAt
        }
        usefulResults {
          id
          score
          seenBy
          dropped
          state
          search {
            id
            state
            createdAt
            user {
              id
              profile {
                name
                email
                address
                phone
              }
            }
            minBrooms
            maxBrooms
            minRooms
            maxRooms
            minBaths
            maxBaths
            minPrice
            maxPrice
            minCommonExpenses
            maxCommonExpenses
            operationType
            address
            propertyType
            features
          }
        }
        images {
          id
          name
          size
          url
          thumbUrl
          smallUrl
        }
      }
    }
  }
`;

export const Results = gql`
  query results($searchId: ID!) {
    results(searchId: $searchId) {
      id
      score
      seenBy
      state
      property {
        id
        propertyType
        state
        description
        currency
        price
        disabled
        fullAddress
        images {
          id
          name
          size
          url
          thumbUrl
          smallUrl
        }
      }
    }
  }
`;
