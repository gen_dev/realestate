import gql from 'graphql-tag'

export const Receipt = gql`
  query receipt($id: ID!) {
    receipt(id: $id) {
      id
      number
      state
      createdAt
      paymentTypes {
        id
        name
        currency
        amount
      }
      contract {
        id
        property {
          address {
            id
            street 
            number
            unit
            city
            state 
            country
          }
        }
      }
      payments {
        id
        chargebacks
        total
        currency
        thirdPartyTransfer
        parentId
        period
        concept
        state
        dueDate
        paidAt
        paymentType
        payableName
        taxableTag
        amount
        lastingAmount
        partialAmount
        chargebackAmount
        commissionAmount
        commissionedAmount
        fixesAmount
        isAdvanced
        canAdvance
        name
        userType
        canEdit
        referralType
        referralId
        receiverType
        receiverId
        expired
        price {
          sign
        }
        tax {
          name
        }
        contract {
          id
          property {
            address {
              id
              street 
              number
              unit
              city
              state 
              country
            }
          }
        }
        children {
          id
          amount
        }
        subAccount {
          id
          name
        }
      }
    }
  }
`

export const Receipts = gql`
  query receipts($id: ID!, $date: String) {
    receipts(id: $id, date: $date) {
      id
      number
      state
      createdAt
      payments {
        id
        chargebacks
        contractTag
        total
        currency
        thirdPartyTransfer
        parentId
        period
        concept
        state
        dueDate
        paidAt
        paymentType
        payableName
        amount
        taxableTag
        lastingAmount
        chargebackAmount
        commissionAmount
        commissionedAmount
        fixesAmount
        isAdvanced
        canAdvance
        name
        userType
        canEdit
        referralType
        referralId
        receiverType
        receiverId
        expired
        price {
          sign
        }
        tax {
          name
        }
        children {
          id
          amount
        }
        subAccount {
          id
          name
        }
      }
    }
  }
`
