import gql from "graphql-tag";

export const Property = gql`
  query property($id: ID!) {
    property(id: $id) {
      id
      agencyId
      propertyType
      operationTypes
      state
      description
      currency
      price
      disabled
      features
      basicInfo
      fullAddress
      sellPrice
      sellCurrency
      metadata
      agency {
        id
        name
        email
        address
        phone
      }
      images {
        id
        name
        size
        url
        thumbUrl
        smallUrl
      }
      address {
        id
        lat
        lng
        street
        number
        unit
        city
        state
        country
        neighborhood
      }
      features
      characteristics {
        id
        name
        value
        category
      }
    }
  }
`;

export const Starred = gql`
  query starred($agencyIds: [ID!]!, $operationType: String) {
    starred(agencyIds: $agencyIds, operationType: $operationType) {
      id
      propertyType
      price
      features
      state
      sellPrice
      operationTypes
      metadata
      images {
        id
        name
        size
        url
        thumbUrl
        smallUrl
      }
      characteristics {
        id
        name
        value
        category
      }
      address {
        id
        lat
        lng
      }
      fullAddress
    }
  }
`;

export const Search = gql`
  query search($attributes: SearchInput) {
    search(attributes: $attributes) {
      results {
        score
        property {
          id
          propertyType
          price
          features
          state
          sellPrice
          operationTypes
          metadata
          images {
            id
            name
            size
            url
            thumbUrl
            smallUrl
          }
          characteristics {
            id
            name
            value
            category
          }
          address {
            id
            lat
            lng
          }
          fullAddress
        }
      }
      recommended {
        score
        property {
          id
          propertyType
          price
          features
          state
          operationTypes
          metadata
          images {
            id
            name
            size
            url
            thumbUrl
            smallUrl
          }
          characteristics {
            id
            name
            value
            category
          }
          address {
            id
            lat
            lng
          }
          fullAddress
        }
      }
    }
  }
`;

export const PropertyTypes = gql`
  query propertyTypes($agencyIds: [ID!]!) {
    propertyTypes(agencyIds: $agencyIds) {
      name
    }
  }
`;

export const Observations = gql`
  query observations($id: ID!) {
    observations(id: $id) {
      id
      message
      createdAt
      user {
        id
        email
        profile {
          name
          email
          address
          phone
        }
      }
      comments {
        id
        text
        createdAt
        user {
          id
          email
          profile {
            name
            email
            address
            phone
          }
        }
      }
    }
  }
`;
