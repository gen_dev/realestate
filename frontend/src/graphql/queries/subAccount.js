import gql from 'graphql-tag'

export const SubAccounts = gql`
  query subAccounts($isolated: Boolean, $accountId: ID) {
    subAccounts(isolated: $isolated, accountId: $accountId) {
      id
      agencyId
      name
      tmpBalance
    }
  }
`
