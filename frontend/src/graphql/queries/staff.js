import gql from "graphql-tag";

export const Staff = gql`
  query staff {
    staff {
      id
      name
      operationType
      rol
      propertyTypes
      city
      number
      avatarUrl
      user {
        id
        email
        displayRoles
      }
    }
  }
`;

export const Clients = gql`
  query clients($id: ID!) {
    clients(id: $id) {
      contacts {
        id
        profile {
          name
          address
          phone
          email
          dni
        }
        roles {
          name
        }
        displayRoles
      }
      pages
    }
  }
`;

export const AssignedProperties = gql`
  query assignedProperties($id: ID!) {
    assignedProperties(id: $id) {
      properties {
        id
        propertyType
        state
        description
        currency
        price
        sellPrice
        sellCurrency
        disabled
        featured
        operationTypes
        metadata
        images {
          id
          name
          size
          url
          thumbUrl
          smallUrl
        }
        address {
          id
          street
          number
          unit
          city
          state
          country
          neighborhood
        }
        features
        owner {
          id
          email
          profile {
            name
          }
        }
      }
      pages
    }
  }
`;
