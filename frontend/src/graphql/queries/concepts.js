import gql from 'graphql-tag'

export const Concepts = gql`
  query concepts($subAccountId: ID!) {
    concepts(subAccountId: $subAccountId) {
      id
      name
      subAccountId
    }
  }
`
