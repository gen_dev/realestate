import gql from "graphql-tag";

export const Properties = gql`
  query properties(
    $noPaginate: Boolean
    $search: String
    $filters: PropertyFilter
    $operationType: String
    $free: Boolean
    $contractId: ID
    $page: Int
    $perPage: Int
  ) {
    properties(
      noPaginate: $noPaginate
      search: $search
      filters: $filters
      operationType: $operationType
      free: $free
      contractId: $contractId
      page: $page
      perPage: $perPage
    ) {
      properties {
        id
        propertyType
        state
        description
        currency
        price
        sellPrice
        sellCurrency
        disabled
        featured
        operationTypes
        blueprintUrl
        metadata
        images {
          id
          name
          size
          url
          thumbUrl
          smallUrl
        }
        address {
          id
          street
          number
          unit
          city
          state
          country
          neighborhood
        }
        features
        owner {
          id
          email
          profile {
            name
          }
        }
      }
      pages
    }
  }
`;

export const OwnerProperties = gql`
  query ownerProperties($ownerId: ID!) {
    ownerProperties(ownerId: $ownerId) {
      properties {
        id
        propertyType
        state
        description
        currency
        price
        sellPrice
        sellCurrency
        disabled
        featured
        operationTypes
        blueprintUrl
        metadata
        images {
          id
          name
          size
          url
          thumbUrl
          smallUrl
        }
        address {
          id
          street
          number
          unit
          city
          state
          country
          neighborhood
        }
        features
        owner {
          id
          email
          profile {
            name
          }
        }
      }
      pages
    }
  }
`;

export const PropertiesList = gql`
  query properties(
    $noPaginate: Boolean
    $search: String
    $filters: PropertyFilter
    $operationType: String
    $free: Boolean
    $contractId: ID
    $page: Int
    $perPage: Int
  ) {
    properties(
      noPaginate: $noPaginate
      search: $search
      filters: $filters
      operationType: $operationType
      free: $free
      contractId: $contractId
      page: $page
      perPage: $perPage
    ) {
      properties {
        id
        address {
          id
          street
          number
          unit
          city
          state
          country
          neighborhood
        }
      }
      pages
    }
  }
`;

export const Property = gql`
  query property($id: ID!) {
    property(id: $id) {
      id
      agencyId
      propertyType
      fixesSubAccountId
      state
      description
      currency
      price
      disabled
      featured
      features
      sellPrice
      sellCurrency
      operationTypes
      blueprintUrl
      metadata
      staff {
        id
        name
        operationType
        rol
        propertyTypes
        city
        number
      }
      images {
        id
        name
        size
        url
        thumbUrl
        smallUrl
      }
      activeTenant {
        id
        email
        profile {
          name
        }
      }
      activeContract {
        id
        state
        contractType
        duration
        keys
        totalRentPrice
        rent {
          id
          name
          interval
          prices {
            id
            startsAt
            endsAt
            amount
          }
        }
        guarantees {
          id
          guaranteeType
          guarantorId
          guarantor {
            profile {
              name
            }
          }
          documents {
            id
            name
            size
            url
          }
        }
        documents {
          id
          name
          size
          url
        }
        property {
          id
          propertyType
          state
          description
          currency
          price
          images {
            id
            name
            size
            url
            thumbUrl
            smallUrl
          }
          address {
            id
            lat
            lng
            street
            number
            unit
            city
            state
            country
          }
        }
        contractPlan {
          id
          duration
          rentUpdatePeriod
          rentValue
          totalAmount
          property {
            id
            propertyType
            state
            description
            currency
            price
            images {
              id
              name
              size
              url
              thumbUrl
              smallUrl
            }
            address {
              id
              street
              number
              unit
              city
              state
              country
            }
          }
          contractExpenses {
            id
            name
            value
          }
        }
        paymentPlan {
          id
          paymentMethods
          taxes {
            id
            name
            interval
            prices {
              id
              startsAt
              endsAt
              amount
            }
          }
        }
        taxes {
          id
          name
          state
          interval
          lastPaymentDate
        }
        payments {
          id
          period
          concept
          state
          dueDate
          paymentType
          amount
        }
        owner {
          id
          email
          profile {
            id
            name
            address
            phone
            email
          }
        }
        tenant {
          id
          email
          profile {
            id
            name
            address
            phone
            email
          }
        }
        overduePayments {
          id
          period
          concept
          state
          dueDate
          paymentType
          amount
        }
      }
      address {
        id
        lat
        lng
        street
        number
        unit
        city
        state
        country
        neighborhood
      }
      contracts {
        id
        state
        name
        contractType
        startsAt
        endsAt
        documents {
          id
          name
          size
          url
        }
      }
      features
      owner {
        id
        email
        profile {
          name
        }
      }
      characteristics {
        id
        name
        value
        category
      }
      taxes {
        id
        name
        chargeback
        editable
      }
    }
  }
`;

export const Fixes = gql`
  query fixes($propertyId: ID!) {
    fixes(propertyId: $propertyId) {
      id
      name
      description
      state
      fixedAt
      subAccountId
      payments {
        id
        period
        concept
        state
        dueDate
        paidAt
        paymentType
        amount
        name
        userType
        lastingAmount
        chargebackAmount
        chargebacks
        isAdvanced
        canAdvance
        payable
        subAccountId
        canEdit
        referralType
        referralId
        expired
        partialPayments {
          id
          value
          createdAt
        }
        contract {
          id
          property {
            address {
              id
              street
              number
              unit
              city
              state
              country
            }
          }
        }
        subAccount {
          id
          name
        }
      }
    }
  }
`;
