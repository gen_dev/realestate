import gql from 'graphql-tag'

export const Tickets = gql`
  query tickets($contractId: ID, $states: [String!]) {
    tickets(contractId: $contractId, states: $states) {
      id
      title
      state
      createdAt
      updatedAt
      contract {
        id
        fullAddress
      }
    }
  }
`

export const Ticket = gql`
  query ticket($ticketId: ID!) {
    ticket(ticketId: $ticketId) {
      id
      title
      state
      createdAt
      updatedAt
      descriptions {
        id
        description
        createdAt
      }
      contract {
        id
        fullAddress
        property {
          id
          agencyId
          fixesSubAccountId
          activeTenant {
            id
          }
          owner {
            id
          }
        }
      }
      fix {
        id
        name
        description
        state
        fixedAt
        subAccountId
        payments {
          id
          period
          concept
          state
          dueDate
          paidAt
          createdAt
          paymentType
          amount
          name
          userType
          lastingAmount
          chargebackAmount
          chargebacks
          isAdvanced
          canAdvance
          payable
          subAccountId
          canEdit
          referralType
          referralId
          expired
          children {
            id
            amount
          }
          contract {
            id
            property {
              address {
                id
                street 
                number
                unit
                city
                state 
                country
              }
            }
          }
          subAccount {
            id
            name
          }
        }
      }
    }
  }
`
