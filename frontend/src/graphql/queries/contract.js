import gql from "graphql-tag";

export const AllContracts = gql`
  query allContracts {
    allContracts {
      id
      fullAddress
      rescission {
        id
      }
    }
  }
`;

export const Contracts = gql`
  query contracts(
    $search: String
    $filters: ContractFilter
    $operationType: String
    $page: Int
    $perPage: Int
  ) {
    contracts(
      search: $search
      filters: $filters
      operationType: $operationType
      page: $page
      perPage: $perPage
    ) {
      contracts {
        id
        state
        contractType
        duration
        keys
        startsAt
        endsAt
        rescission {
          id
        }
        property {
          id
          fullAddress
        }
        owner {
          id
          email
          profile {
            id
            name
            address
            phone
            email
          }
        }
        tenant {
          id
          email
          profile {
            id
            name
            address
            phone
            email
          }
        }
      }
      pages
      totalCount
    }
  }
`;

export const Contract = gql`
  query contract($id: ID!) {
    contract(id: $id) {
      id
      state
      contractType
      duration
      keys
      totalRentPrice
      tenantStatus
      ownerStatus
      startsAt
      endsAt
      rescission {
        id
        dueAt
        rent
        contractId
        state
        bonification
        quota
        receipt {
          id
          state
          receiptType
          userId
          contractId
          payments {
            id
            concept
            paymentType
            amount
          }
        }
      }
      rent {
        id
        name
        interval
        prices {
          id
          startsAt
          endsAt
          amount
        }
      }
      guarantees {
        id
        guaranteeType
        guarantorId
        guarantor {
          profile {
            id
            name
            documents {
              id
              name
              size
              url
            }
          }
        }
        documents {
          id
          name
          size
          url
        }
      }
      documents {
        id
        name
        size
        url
      }
      property {
        id
        propertyType
        state
        description
        price
        agencyId
        address {
          id
          street
          number
          unit
          city
          state
          country
        }
      }
      paymentPlan {
        id
        paymentMethods
        taxes {
          id
          name
          interval
          chargeback
          prices {
            id
            startsAt
            endsAt
            differential
            amount
          }
        }
      }
      owner {
        id
        email
        profile {
          id
          name
          address
          phone
          email
        }
      }
      tenant {
        id
        email
        profile {
          id
          name
          address
          phone
          email
        }
      }
      currentTaxes {
        id
        name
        chargeback
        editable
        interval
        payments {
          id
          contractTag
          contractId
          concept
          detail
          paidAt
          total
          currency
          thirdPartyTransfer
          name
          state
          subAccount {
            id
            name
            account {
              id
              name
            }
          }
        }
      }
    }
  }
`;

export const OutdatedPrices = gql`
  query outdatedPrices {
    outdatedPrices {
      id
      contract {
        id
        fullAddress
      }
    }
  }
`;

export const OutdatedPricesList = gql`
  query outdatedPrices {
    outdatedPrices {
      id
    }
  }
`;

export const FinishingContracts = gql`
  query finishingContracts {
    finishingContracts {
      id
      fullAddress
      rescission {
        id
      }
    }
  }
`;

export const FinishingContractsList = gql`
  query finishingContracts {
    finishingContracts {
      id
    }
  }
`;
