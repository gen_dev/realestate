import gql from "graphql-tag";

export const Inquiries = gql`
  query inquiries(
    $page: ID
    $perPage: ID
    $type: String
    $mine: Boolean
    $manual: Boolean
    $ownerId: ID
    $userId: ID
    $status: [String!]
  ) {
    inquiries(
      page: $page
      perPage: $perPage
      type: $type
      mine: $mine
      manual: $manual
      ownerId: $ownerId
      userId: $userId
      status: $status
    ) {
      pages
      inquiries {
        id
        name
        email
        topic
        message
        phone
        createdAt
        state
        read
        investmentType
        operationType
        location
        characteristics
        search {
          id
          state
          createdAt
          resultsCount
          currency
          minBrooms
          maxBrooms
          minRooms
          maxRooms
          minBaths
          maxBaths
          minPrice
          maxPrice
          minCommonExpenses
          maxCommonExpenses
          operationType
          address
          propertyType
          features
          unseenResults
          results {
            id
            score
            seenBy
            state
            property {
              id
              propertyType
              state
              description
              currency
              price
              disabled
              fullAddress
              images {
                id
                name
                size
                url
                thumbUrl
                smallUrl
              }
            }
          }
          user {
            id
            profile {
              name
              email
              address
              phone
            }
          }
        }
        user {
          id
          email
        }
        contact {
          id
        }
        property {
          id
          fullAddress
        }
      }
    }
  }
`;

export const Comments = gql`
  query comments($id: ID!) {
    comments(id: $id) {
      id
      text
      createdAt
      user {
        id
        email
      }
    }
  }
`;
