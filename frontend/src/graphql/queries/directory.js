import gql from "graphql-tag";

export const Contacts = gql`
  query contacts(
    $noPaginate: Boolean
    $search: String
    $filters: ProfileFilter
    $roles: [String!]
    $notRoles: [String!]
    $userId: ID
    $page: Int
    $perPage: Int
  ) {
    contacts(
      noPaginate: $noPaginate
      search: $search
      filters: $filters
      roles: $roles
      notRoles: $notRoles
      userId: $userId
      page: $page
      perPage: $perPage
    ) {
      contacts {
        id
        profile {
          name
          address
          phone
          email
          dni
        }
        roles {
          name
        }
        displayRoles
      }
      pages
    }
  }
`;

export const ListContacts = gql`
  query listContacts(
    $roles: [String!]
    $notRoles: [String!]
    $pendingRent: Boolean
  ) {
    listContacts(
      roles: $roles
      notRoles: $notRoles
      pendingRent: $pendingRent
    ) {
      id
      profile {
        name
        address
        phone
        email
        dni
      }
    }
  }
`;

export const Contact = gql`
  query contact($contactId: ID!) {
    contact(contactId: $contactId) {
      id
      searchObservations
      overduePaymentsAmount
      owedMoney
      owedMoneyByUs
      hasDebts
      advisorId
      advisor {
        id
        name
        number
        city
        rol
      }
      profile {
        name
        address
        phone
        email
        dni
      }
      roles {
        name
      }
      activeContracts {
        property {
          address {
            id
            street
            number
            unit
            city
            state
            country
          }
        }
      }
    }
  }
`;

export const Owners = gql`
  query owners {
    owners {
      id
      receipts {
        id
        number
        state
        payments {
          id
          chargebacks
          total
          period
          concept
          state
          dueDate
          paidAt
          paymentType
          payableName
          amount
          lastingAmount
          chargebackAmount
          commissionAmount
          commissionedAmount
          fixesAmount
          isAdvanced
          canAdvance
          name
          userType
          canEdit
          referralType
          referralId
          expired
          price {
            sign
          }
          tax {
            name
          }
          contract {
            id
            property {
              address {
                id
                street
                number
                unit
                city
                state
                country
              }
            }
          }
          partialPayments {
            id
          }
          subAccount {
            id
            name
          }
        }
      }
      profile {
        name
      }
    }
  }
`;

export const Tenants = gql`
  query tenants {
    tenants {
      id
      receipts {
        id
        number
        state
        payments {
          id
          chargebacks
          total
          period
          concept
          state
          dueDate
          paidAt
          paymentType
          payableName
          amount
          lastingAmount
          chargebackAmount
          commissionAmount
          commissionedAmount
          fixesAmount
          isAdvanced
          canAdvance
          name
          userType
          canEdit
          referralType
          referralId
          expired
          price {
            sign
          }
          tax {
            name
          }
          contract {
            id
            property {
              address {
                id
                street
                number
                unit
                city
                state
                country
              }
            }
          }
          partialPayments {
            id
          }
          subAccount {
            id
            name
          }
        }
      }
      profile {
        name
      }
    }
  }
`;

export const UserTaxes = gql`
  query userTaxes($id: ID!) {
    userTaxes(id: $id) {
      id
      name
      state
      interval
      lastPaymentDate
      taxableType
      taxableId
    }
  }
`;
