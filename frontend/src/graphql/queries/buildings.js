import gql from 'graphql-tag'

export const Buildings = gql`
  query buildings {
    buildings {
      id
      fullAddress
    }
  }
`

export const FreeProperties = gql`
  query freeProperties {
    freeProperties {
      id
      fullAddress
    }
  }
`

export const Building = gql`
  query building($id: ID!) {
    building(id: $id) {
      id
      fullAddress
      properties {
        id
        fullAddress
      }
      taxes {
        id
        name
        chargeback
        editable
        taxableId
        taxableType
        appliesWhenEmpty
        emptyAction
        ownerAction
      }
    }
  }
`
