import gql from "graphql-tag";

export const Tax = gql`
  query tax($id: ID!) {
    tax(id: $id) {
      id
      amount
      name
      interval
      commission
      startsAt
      endsAt
      dueAt
      chargeback
      accountId
      subAccountId
      conceptId
      agencyAction
      ownerAction
      tenantAction
      appliesWhenEmpty
      unequalSplit
      emptyAction
      concept {
        id
        name
        subAccountId
      }
      percentages {
        id
        propertyId
        amount
      }
      pricesAttributes {
        id
        dueAt
        startsAt
        endsAt
        amount
        sign
        referralType
        referralId
        receiverType
        receiverId
      }
    }
  }
`;

export const Taxes = gql`
  query taxes($id: ID!) {
    taxes(id: $id) {
      id
      taxableId
      name
      chargeback
      editable
      interval
    }
  }
`;
