import gql from 'graphql-tag'

export const Accounts = gql`
  query accounts($filters: AccountFilter, $period: [String!]) {
    accounts(filters: $filters, period: $period) {
      id
      name
      balance
      subAccounts {
        id
        name
        tmpBalance
        parsedBalance
        concepts {
          id
          name
          balance
          parsedBalance
        }
      }
    }
  }
`
