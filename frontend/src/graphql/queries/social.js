import gql from 'graphql-tag'

export const Socials = gql`
  query socials {
    socials {
      id
      name
      link
      description
      buttonTag
      imageUrl
    }
  }
`
