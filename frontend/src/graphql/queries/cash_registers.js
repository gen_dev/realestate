import gql from 'graphql-tag'

export const CurrentCashRegister = gql`
  query currentCashRegister {
    currentCashRegister {
      id
      amount
      state
      createdAt
      payments {
        id
        contractTag
        concept
        detail
        paidAt
        total
        lastingAmount
        name
        currency
        subAccount {
          id
          name
          account {
            id
            name
          }
        }
      }
    }
  }
`

export const CashRegisters = gql`
  query cashRegisters {
    cashRegisters {
      id
      state
      amount
      createdAt
    }
  }
`

export const CashRegister = gql`
  query cashRegister($id: ID!) {
    cashRegister(id: $id) {
      id
      amount
      state
      createdAt
      payments {
        id
        contractTag
        concept
        detail
        paidAt
        total
        lastingAmount
        name
        currency
        subAccount {
          id
          name
          account {
            id
            name
          }
        }
      }
    }
  }
`
