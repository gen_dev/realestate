import gql from 'graphql-tag'

export const Collections = gql`
  query collections($id: ID!, $period: String!) {
    collections(id: $id, period: $period) {
      chargebacks
      paid {
        id
        contractTag
        taxableTag
        period
        concept
        state
        detail
        dueDate
        detail
        paidAt
        paymentType
        payableName
        amount
        differential
        total
        isAdvanced
        canAdvance
        lastingAmount
        currency
        thirdPartyTransfer
        partialAmount
        chargebackAmount
        commissionAmount
        commissionedAmount
        fixesAmount
        name
        userType
        canEdit
        referralType
        referralId
        receiverName
        receiverType
        receiverId
        expired
        chargebacks
        price {
          sign
        }
        tax {
          name
        }
        contract {
          id
          property {
            address {
              id
              street 
              number
              unit
              city
              state 
              country
            }
          }
        }
        children {
          id
          amount
        }
        subAccount {
          id
          name
        }
      }
      pending {
        id
        contractTag
        taxableTag
        period
        concept
        state
        detail
        paidAt
        dueDate
        detail
        paymentType
        payableName
        amount
        differential
        total
        partialAmount
        chargebacks
        fixesAmount
        isAdvanced
        canAdvance
        lastingAmount
        currency
        thirdPartyTransfer
        chargebackAmount
        commissionAmount
        commissionedAmount
        name
        userType
        canEdit
        referralType
        referralId
        receiverName
        receiverType
        receiverId
        expired
        price {
          sign
        }
        tax {
          name
        }
        contract {
          id
          property {
            address {
              id
              street 
              number
              unit
              city
              state 
              country
            }
          }
        }
        children {
          id
          amount
        }
        subAccount {
          id
          name
        }
      }
    }
  }
`
export const Accountability = gql`
  query accountability($id: ID!, $period: String) {
    accountability(id: $id, period: $period) {
      chargebacks
      pending {
        id
        contractTag
        taxableTag
        chargebacks
        concept
        state
        dueDate
        name
        paidAt
        total
        lastingAmount
        currency
        thirdPartyTransfer
        referralType
        referralId
        counterpart {
          id
          paidAt
        }
        children {
          id
          amount
        }
      }
    }
  }
`

export const Payments = gql`
  query payments($filters: PaymentFilter, $date: [String!], $search: String, $page: Int, $perPage: Int, $subaccounts: [String!], $fromAgency: Boolean, $contractId: ID) {
    payments(filters: $filters, search: $search, page: $page, perPage: $perPage, subaccounts: $subaccounts, fromAgency: $fromAgency, contractId: $contractId, date: $date) {
      pages
      payments {
        id
        contractTag
        concept
        detail
        paidAt
        total
        currency
        thirdPartyTransfer
        name
        subAccount {
          id
          name
          account {
            id
            name
          }
        }
      }
    }
  }
`

export const Today = gql`
  query today {
    today {
      id
      contractTag
      concept
      detail
      paidAt
      total
      name
      subAccount {
        id
        name
        account {
          id
          name
        }
      }
    }
  }
`

export const Advanced = gql`
  query advanced {
    advanced {
      name
      amount
      differential
    }
  }
`

export const Payment = gql`
  query payment($id: ID!) {
    payment(id: $id) {
      id
      contractTag
      taxableTag
      period
      concept
      state
      dueDate
      detail
      paidAt
      paymentType
      payableName
      amount
      differential
      total
      partialAmount
      name
      userType
      lastingAmount
      currency
      thirdPartyTransfer
      chargebackAmount
      fixesAmount
      chargebacks
      isAdvanced
      canAdvance
      subAccountId
      canEdit
      referralType
      referralId
      receiverType
      receiverId
      receiverName
      expired
      paymentType
      price {
        sign
      }
      tax {
        name
      }
      children {
        id
        amount
      }
      contract {
        id
        property {
          address {
            id
            street 
            number
            unit
            city
            state 
            country
          }
        }
      }
      subAccount {
        id
        name
      }
    }
  }
`

export const PaymentsFilters = gql`
  query paymentsFilters($subaccounts: [String!], $fromAgency: Boolean) {
    paymentsFilters(subaccounts: $subaccounts, fromAgency: $fromAgency) {
      payments {
        id
        period
        concept
        state
        dueDate
        detail
        paidAt
        paymentType
        amount
        differential
        payableName
        total
        partialAmount
        lastingAmount
        currency
        thirdPartyTransfer
        chargebackAmount
        fixesAmount
        chargebacks
        isAdvanced
        canAdvance
        name
        userType
        canEdit
        referralType
        referralId
        receiverName
        receiverType
        receiverId
        expired
        tax {
          name
        }
        price {
          sign
        }
        contract {
          id
          property {
            address {
              id
              street 
              number
              unit
              city
              state 
              country
            }
          }
        }
        children {
          id
          amount
        }
        subAccount {
          id
          name
        }
      }
    }
  }
`
