import gql from 'graphql-tag'

export const Tax = gql`
  query userAgencies {
    userAgencies {
      selected
      id
      agency {
        name
      }
    }
  }
`
