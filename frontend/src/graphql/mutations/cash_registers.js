import gql from 'graphql-tag'

export const EditCashRegister = gql`
  mutation editCashRegister($input: EditCashRegisterInput!) {
    editCashRegister(input: $input) {
      success
    }
  }
`
