import gql from "graphql-tag";

export const EditResult = gql`
  mutation editResult($input: EditResultInput!) {
    editResult(input: $input) {
      clientMutationId
      success
    }
  }
`;
