import gql from 'graphql-tag'

export const CreateContract = gql`
  mutation createContract($input: CreateContractInput!) {
    createContract(input: $input) {
      clientMutationId
      contract {
        id
        keys
        startsAt
        endsAt
        contractType
        documents {
          id
          name
          size
          url
        }
        contractPlan {
          id
        }
        paymentPlan {
          id
          paymentMethods
          taxes {
            id
            name
            interval
            chargeback
            ownerFee
            prices {
              id
              startsAt
              referralId
              endsAt
              dueAt
              amount
            }
          }
        }
        property {
          id
          owner {
            id
            profile {
              name
            }
          }
        }
        state
        taxes {
          id
        }
        tenant {
          id
          profile {
            name
          }
        }
        owner {
          id
          profile {
            name
          }
        }
      }
    }
  }
`

export const EditContract = gql`
  mutation editContract($input: EditContractInput!) {
    editContract(input: $input) {
      clientMutationId
      contract {
        id
        keys
        newLaw
        dollarized
        thirdPartyTransfer
        startsAt
        endsAt
        contractType
        duration
        totalRentPrice
        rent {
          id
          name
          interval
          prices {
            id
            startsAt
            referralId
            endsAt
            amount
            dueAt
            differential
          }
        }
        guarantees {
          id
          guaranteeType
          guarantorId
          guarantor {
            profile {
              name
              documents {
                id
                name
                size
                url
              }
            }
          }
          documents {
            id
            name
            size
            url
          }
        }
        documents {
          id
          name
          size
          url
        }
        contractPlan {
          id
        }
        paymentPlan {
          id
          paymentMethods
          taxes {
            id
            name
            interval
            commission
            chargeback
            ownerFee
            prices {
              id
              startsAt
              referralId
              endsAt
              amount
              dueAt
              differential
            }
          }
        }
        property {
          id
          owner {
            id
            profile {
              name
            }
          }
        }
        state
        taxes {
          id
        }
        tenant {
          id
          profile {
            name
          }
        }
        owner {
          id
          profile {
            name
          }
        }
      }
    }
  }
`

export const DeleteContract = gql`
  mutation deleteContract($input: DeleteContractInput!) {
    deleteContract(input: $input) {
      success 
    }
  }
`

export const FinishContract = gql`
  mutation finishContract($input: FinishContractInput!) {
    finishContract(input: $input) {
      success 
    }
  }
`

export const RegeneratePayments = gql`
  mutation regeneratePayments($input: RegenerateContractPaymentsInput!) {
    regeneratePayments(input: $input) {
      success 
    }
  }
`
