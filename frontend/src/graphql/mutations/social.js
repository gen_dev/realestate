import gql from 'graphql-tag'

export const CreateSocial = gql`
  mutation createSocial($input: CreateSocialInput!) {
    createSocial(input: $input) {
      success
    }
  }
`

export const EditSocial = gql`
  mutation editSocial($input: EditSocialInput!) {
    editSocial(input: $input) {
      success
    }
  }
`

export const DeleteSocial = gql`
  mutation deleteSocial($input: DeleteSocialInput!) {
    deleteSocial(input: $input) {
      success
    }
  }
`
