import gql from 'graphql-tag'

export const Print = gql`
  mutation print($input: PrintInput!) {
    print(input: $input) {
      success
    }
  }
`

export const CreatePaymentType = gql`
  mutation createPaymentMethod($input: CreatePaymentMethodInput!) {
    createPaymentMethod(input: $input) {
      success
    }
  }
`

export const DeletePaymentType = gql`
  mutation deletePaymentMethod($input: DeletePaymentMethodInput!) {
    deletePaymentMethod(input: $input) {
      success
    }
  }
`

export const Create = gql`
  mutation createReceipt($input: CreateReceiptInput!) {
    createReceipt(input: $input) {
      receipt {
        id
      }
    }
  }
`
