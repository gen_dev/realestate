import gql from "graphql-tag";

export const CreateInquiry = gql`
  mutation createInquiry($input: CreateInquiryInput!) {
    createInquiry(input: $input) {
      success
    }
  }
`;

export const DeleteInquiry = gql`
  mutation deleteInquiry($input: DeleteInquiryInput!) {
    deleteInquiry(input: $input) {
      success
    }
  }
`;

export const CreateComment = gql`
  mutation createComment($input: CreateCommentInput!) {
    createComment(input: $input) {
      success
    }
  }
`;

export const AssignInquiry = gql`
  mutation assignInquiry($input: AssignInquiryInput!) {
    assignInquiry(input: $input) {
      success
    }
  }
`;

export const UpdateInquiryContact = gql`
  mutation updateInquiryContact($input: UpdateInquiryContactInput!) {
    updateInquiryContact(input: $input) {
      success
    }
  }
`;

export const UpdateInquiryState = gql`
  mutation updateInquiryState($input: UpdateInquiryStateInput!) {
    updateInquiryState(input: $input) {
      success
    }
  }
`;
