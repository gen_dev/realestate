import gql from 'graphql-tag'

export const CreateStaff = gql`
  mutation createStaff($input: CreateStaffInput!) {
    createStaff(input: $input) {
      clientMutationId
      success
    }
  }
`

export const EditStaff = gql`
  mutation editStaff($input: EditStaffInput!) {
    editStaff(input: $input) {
      clientMutationId
      success
    }
  }
`


export const DeleteStaff = gql`
  mutation deleteStaff($input: DeleteStaffInput!) {
    deleteStaff(input: $input) {
      clientMutationId
      success
    }
  }
`

export const AttachStaff = gql`
  mutation attachStaff($input: AttachStaffInput!) {
    attachStaff(input: $input) {
      clientMutationId
      success
    }
  }
`

export const DetachStaff = gql`
  mutation detachStaff($input: DetachStaffInput!) {
    detachStaff(input: $input) {
      clientMutationId
      success
    }
  }
`
