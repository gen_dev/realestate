import gql from 'graphql-tag'

export const CreateConcept = gql`
  mutation createConcept($input: CreateConceptInput!) {
    createConcept(input: $input) {
      success
    }
  }
`

export const CreateSubAccount = gql`
  mutation createSubAccount($input: CreateSubAccountInput!) {
    createSubAccount(input: $input) {
      success
    }
  }
`

export const DeleteSubAccount = gql`
  mutation deleteSubAccount($input: DeleteSubAccountInput!) {
    deleteSubAccount(input: $input) {
      success
    }
  }
`

export const DeleteConcept = gql`
  mutation deleteConcept($input: DeleteConceptInput!) {
    deleteConcept(input: $input) {
      success
    }
  }
`
