import gql from 'graphql-tag'

export const CreateContact = gql`
  mutation createContact($input: CreateContactInput!) {
    createContact(input: $input) {
      user {
        id
        profile {
          name
          address
          phone
          email
          dni
        }
        roles {
          name
        }
      }
    }
  }
`

export const EditContact = gql`
  mutation editContact($input: EditContactInput!) {
    editContact(input: $input) {
      user {
        id
        profile {
          name
          address
          phone
          email
          dni
        }
        roles {
          name
        }
      }
    }
  }
`

export const DeleteContact = gql`
  mutation deleteContact($input: DeleteContactInput!) {
    deleteContact(input: $input) {
      success 
    }
  }
`
