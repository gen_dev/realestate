import gql from "graphql-tag";

export const Create = gql`
  mutation createRescission($input: CreateRescissionInput!) {
    createRescission(input: $input) {
      success
    }
  }
`;

export const EditRescission = gql`
  mutation editRescission($input: EditRescissionInput!) {
    editRescission(input: $input) {
      success
    }
  }
`;
