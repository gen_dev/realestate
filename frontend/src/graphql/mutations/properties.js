import gql from "graphql-tag";

export const CreateProperty = gql`
  mutation createProperty($input: CreatePropertyInput!) {
    createProperty(input: $input) {
      clientMutationId
      property {
        id
        propertyType
        state
        description
        currency
        price
        sellPrice
        sellCurrency
        operationTypes
        metadata
        images {
          id
          name
          size
          url
        }
        activeTenant {
          id
          email
          profile {
            name
          }
        }
        activeContract {
          id
          state
          contractType
          documents {
            id
            name
            size
            url
          }
        }
        address {
          id
          street
          number
          unit
          city
          state
          country
          neighborhood
        }
        contracts {
          id
          state
          contractType
          documents {
            id
            name
            size
            url
          }
        }
        features
        owner {
          id
          email
          profile {
            name
          }
        }
        characteristics {
          id
          name
          value
          category
        }
      }
    }
  }
`;

export const EditProperty = gql`
  mutation editProperty($input: EditPropertyInput!) {
    editProperty(input: $input) {
      clientMutationId
      property {
        id
        propertyType
        state
        description
        currency
        price
        sellPrice
        sellCurrency
        operationTypes
        metadata
        images {
          id
          name
          size
          url
        }
        activeTenant {
          id
          email
          profile {
            name
          }
        }
        activeContract {
          id
          state
          contractType
          documents {
            id
            name
            size
            url
          }
        }
        address {
          id
          street
          number
          unit
          city
          state
          country
          neighborhood
        }
        contracts {
          id
          state
          contractType
          documents {
            id
            name
            size
            url
          }
        }
        features
        owner {
          id
          email
          profile {
            name
          }
        }
        characteristics {
          id
          name
          value
          category
        }
      }
    }
  }
`;

export const DeleteProperty = gql`
  mutation deleteProperty($input: DeletePropertyInput!) {
    deleteProperty(input: $input) {
      success
    }
  }
`;

export const CreateFix = gql`
  mutation createFix($input: CreateFixInput!) {
    createFix(input: $input) {
      fix {
        id
      }
    }
  }
`;

export const DeleteFix = gql`
  mutation deleteFix($input: DeleteFixInput!) {
    deleteFix(input: $input) {
      success
    }
  }
`;

export const RecommendedDescription = gql`
  mutation recommendedDescription($input: RecommendedDescriptionInput!) {
    recommendedDescription(input: $input) {
      description
    }
  }
`;

export const CreateObservation = gql`
  mutation createObservation($input: CreateObservationInput!) {
    createObservation(input: $input) {
      success
    }
  }
`;

export const CreateObservationComment = gql`
  mutation createObservationComment($input: CreateObservationCommentInput!) {
    createObservationComment(input: $input) {
      success
    }
  }
`;

export const DeleteObservation = gql`
  mutation deleteObservation($input: DeleteObservationInput!) {
    deleteObservation(input: $input) {
      success
    }
  }
`;

export const DeleteObservationComment = gql`
  mutation deleteObservationComment($input: DeleteObservationCommentInput!) {
    deleteObservationComment(input: $input) {
      success
    }
  }
`;
