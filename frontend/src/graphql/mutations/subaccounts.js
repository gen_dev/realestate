import gql from 'graphql-tag'

export const CreateSubAccount = gql`
  mutation createSubAccount($input: CreateSubAccountInput!) {
    createSubAccount(input: $input) {
      subAccount {
        id
      }
    }
  }
`

export const DeleteSubAccount = gql`
  mutation deleteSubAccount($input: DeleteSubAccountInput!) {
    deleteSubAccount(input: $input) {
      success
    }
  }
`
