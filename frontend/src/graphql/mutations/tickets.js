import gql from 'graphql-tag'

export const CreateTicket = gql`
  mutation createTicket($input: CreateTicketInput!) {
    createTicket(input: $input) {
      ticket {
				id
      }
    }
  }
`

export const EditTicket = gql`
  mutation editTicket($input: EditTicketInput!) {
    editTicket(input: $input) {
      ticket {
				id
      }
    }
  }
`

export const DeleteTicket = gql`
  mutation deleteTicket($input: DeleteTicketInput!) {
    deleteTicket(input: $input) {
      success
    }
  }
`
