import gql from 'graphql-tag'

export const EditAgency = gql`
  mutation editAgency($input: EditAgencyInput!) {
    editAgency(input: $input) {
      success
    }
  }
`
