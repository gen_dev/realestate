import gql from 'graphql-tag'

export const DeleteImage = gql`
  mutation deleteImage($input: DeleteImageInput!) {
    deleteImage(input: $input) {
      success
    }
  }
`
