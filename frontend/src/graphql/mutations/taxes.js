import gql from 'graphql-tag'

export const CreateTax = gql`
  mutation createTax($input: CreateTaxInput!) {
    createTax(input: $input) {
      tax {
				id
				name
      }
    }
  }
`

export const EditTax = gql`
  mutation editTax($input: EditTaxInput!) {
    editTax(input: $input) {
      tax {
				id
				name
      }
    }
  }
`

export const DeleteTax = gql`
  mutation deleteTax($input: DeleteTaxInput!) {
    deleteTax(input: $input) {
      success
    }
  }
`
