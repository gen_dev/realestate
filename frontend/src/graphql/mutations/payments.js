import gql from 'graphql-tag'

export const SwitchChargeback = gql`
  mutation switchChargeback($input: SwitchChargebackInput!) {
    switchChargeback(input: $input) {
      success
    }
  }
`

export const PayAll = gql`
  mutation payAll($input: PayAllInput!) {
    payAll(input: $input) {
      success
    }
  }
`

export const Pay = gql`
  mutation pay($input: PayInput!) {
    pay(input: $input) {
      payment {
        id
        period
        concept
        state
        detail
        dueDate
        paymentType
        amount
        name
        userType
        contract {
          id
        }
        subAccount {
          id
          name
        }
      }
    }
  }
`

export const Advance = gql`
  mutation advance($input: AdvanceInput!) {
    advance(input: $input) {
      payment {
        id
        period
        concept
        state
        dueDate
        paymentType
        amount
        name
        userType
        contract {
          id
        }
        subAccount {
          id
          name
        }
      }
    }
  }
`

export const CreatePayment = gql`
  mutation createPayment($input: CreatePaymentInput!) {
    createPayment(input: $input) {
      payment {
        id
      }
    }
  }
`

export const EditPayment = gql`
  mutation editPayment($input: EditPaymentInput!) {
    editPayment(input: $input) {
      success
    }
  }
`

export const PartialPay = gql`
  mutation createPartialPayment($input: CreatePartialPaymentInput!) {
    createPartialPayment(input: $input) {
      partialPayment {
        id
      }
    }
  }
`

export const DeletePartialPayment = gql`
  mutation deletePartialPayment($input: DeletePartialPaymentInput!) {
    deletePartialPayment(input: $input) {
      success
    }
  }
`

export const DeletePayment = gql`
  mutation deletePayment($input: DeletePaymentInput!) {
    deletePayment(input: $input) {
      success
    }
  }
`
