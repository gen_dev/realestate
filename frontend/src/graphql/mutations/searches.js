import gql from 'graphql-tag'

export const CreateSearch = gql`
  mutation createSearch($input: CreateSearchInput!) {
    createSearch(input: $input) {
      clientMutationId
      success
    }
  }
`

export const EditSearch = gql`
  mutation editSearch($input: EditSearchInput!) {
    editSearch(input: $input) {
      clientMutationId
      success
    }
  }
`


export const DeleteSearch = gql`
  mutation deleteSearch($input: DeleteSearchInput!) {
    deleteSearch(input: $input) {
      clientMutationId
      success
    }
  }
`
