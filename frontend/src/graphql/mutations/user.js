import gql from "graphql-tag";

export const CreateUser = gql`
  mutation createUser($input: CreateUserInput!) {
    createUser(input: $input) {
      clientMutationId
      user {
        id
        name
        email
      }
    }
  }
`;

export const EditUser = gql`
  mutation editUser($input: EditUserInput!) {
    editUser(input: $input) {
      success
    }
  }
`;

export const EditUserByAdmin = gql`
  mutation editUserByAdmin($input: EditUserByAdminInput!) {
    editUserByAdmin(input: $input) {
      success
    }
  }
`;

export const SigninUser = gql`
  mutation signIn($input: SignInUserInput!) {
    signIn(input: $input) {
      clientMutationId
      token
      user {
        email
        id
        displayRoles
        changelogRead
        selectedAgency {
          id
          name
          address
          email
          website
          phone
          logoUrl
          unseenInquiries
        }
        userAgencies {
          id
          selected
          agency {
            name
          }
        }
      }
    }
  }
`;

export const SignoutUser = gql`
  mutation signOut($input: SignOutInput!) {
    signOut(input: $input) {
      success
    }
  }
`;

export const SwitchSelectedAgency = gql`
  mutation switchSelectedAgency($input: ChangeSelectedAgencyInput!) {
    switchSelectedAgency(input: $input) {
      success
    }
  }
`;
