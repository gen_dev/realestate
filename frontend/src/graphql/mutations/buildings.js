import gql from 'graphql-tag'

export const CreateBuilding = gql`
  mutation createBuilding($input: CreateBuildingInput!) {
    createBuilding(input: $input) {
      success
    }
  }
`

export const EditBuilding = gql`
  mutation editBuilding($input: EditBuildingInput!) {
    editBuilding(input: $input) {
      success
    }
  }
`

export const DeleteBuilding = gql`
  mutation deleteBuilding($input: DeleteBuildingInput!) {
    deleteBuilding(input: $input) {
      success 
    }
  }
`

export const EditCommonExpense = gql`
  mutation editCommonExpense($input: EditCommonExpenseInput!) {
    editCommonExpense(input: $input) {
      success
    }
  }
`

export const AddPropertiesToBuilding = gql`
  mutation addPropertiesToBuilding($input: AddPropertiesToBuildingInput!) {
    addPropertiesToBuilding(input: $input) {
      success
    }
  }
`

export const RemovePropertyFromBuilding = gql`
  mutation removePropertyFromBuilding($input: RemovePropertyFromBuildingInput!) {
    removePropertyFromBuilding(input: $input) {
      success
    }
  }
`
