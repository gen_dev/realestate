module.exports = {
  "devServer": {
    "progress": false
  },
  "pwa": {
    "workboxPluginMode": "GenerateSW",
    "workboxOptions": {
      "skipWaiting": false
    }
  },
  "transpileDependencies": [
    "vuetify"
  ]
}